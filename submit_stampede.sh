#!/bin/bash
#SBATCH -p normal          
#SBATCH -n 512                 
#SBATCH -t 01:00:00             
#SBATCH -J driveBeta1
#SBATCH -o std.o%j 
#SBATCH --mail-user=kaylanb@berkeley.edu
#SBATCH --mail-type=end    # email me when the job finishes

export MYEXEC=/home1/03048/kaylanb/ORION2_intel/myProb/Burleighetal/drive/unitsGeq1/drive
export CH_OUTPUT_INTERVAL=512
ibrun ${MYEXEC}/orion2
