'''
fits Lee2014 Mdot \parallel or \perp equations to my 64 sink steady accretion rate data.

Example: presented fits at 11/10/2015 group meeting by
1) cd /Users/kburleigh/research/meeting_prog/10_13_2015/final_mdot_stdout_data/sinkPickle_and_plots/sd2
2) python Draft_figs_allBeta.py -dirs b100/lev4 b10/lev4 b1/lev5 b0.1/lev5 b0.01/lev5 -labels 100 10 1 0.1 0.01 -cs 1 -mach 5 -rho0 1e-2 -mass0 0.40625 -nsinks 64 -Geq1 1 -steady_wid 2.5 2.5 2. 3. 1. -steady_end 21.5 21.5 17. 13. 3. -xlim 0 23
2) run this script on the resulting beta_*_indivSteadyMdots.pickle files from #2 above
'''

import matplotlib
#matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from scipy.optimize import leastsq,curve_fit
import pickle
import argparse

from modules.sink.data_types import mdotBH_interp_x

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-root_dir",action="store",required=False,help='path to directory containing b100,b10,b1,b0.1,b0.01 directories with the steady mdot data')
parser.add_argument("-plot_fits",action="store",required=False,help='set to plot best-fit lines')
parser.add_argument("-plot_sams",action="store",required=False,help='set to plot semi analytic model median,mean vs. beta')
parser.add_argument("-stat_type",choices=['linear','log'],action="store",required=True,help='Log: semi analytic pickle file contains median(np.log(mdots)) like values, Linear: it contains median(mdots) like values')
args = parser.parse_args()

def lee_mach_bh(rmsV,cs):
    lam=np.exp(1.5)/4
    return np.power( 1.+np.power(rmsV/cs,4), 1./3)/np.power( 1.+np.power(rmsV/cs/lam,2), 1./6)

def lee_parallel(x,beta_ch,n):
    beta=x.copy()
    rmsV=5.
    cs=1.
    mach_bh= lee_mach_bh(rmsV,cs)
    return np.log( mach_bh**-2*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n) )

def mdot_magnetic_neq8(x,beta_ch):
    beta=x.copy()
    n=8.
    rmsV=5.
    cs=1.
    mach_bh= lee_mach_bh(rmsV,cs)
    return mach_bh**-2*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n)

def magnetic_bh_neq8(x,beta_ch):
	vrms= 5.
	rho= 1.e-2
	mdot= np.power(mdotBH_interp_x(vrms,rho)**-2+np.power(mdot_magnetic_neq8(x,beta_ch),-2), -0.5)
	return np.log(mdot)

def lee_parallel_3(x,A,beta_ch,n):
    beta=x.copy()
    rmsV=5.
    cs=1.
    mach_bh= lee_mach_bh(rmsV,cs)
    return A+np.log(mach_bh**-2*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n) )

def lee_parallel_neq100(x,beta_ch):
    beta=x.copy()
    n=100.
    rmsV=5.
    cs=1.
    mach_bh= lee_mach_bh(rmsV,cs)
    return np.log( mach_bh**-2*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n) )

def lee_parallel_neq8(x,beta_ch):
    beta=x.copy()
    n=8.
    rmsV=5.
    cs=1.
    mach_bh= lee_mach_bh(rmsV,cs)
    return np.log( mach_bh**-2*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n) )


def lee_parallel_betachEq11(x,n):
    beta=x.copy()
    beta_ch=11.6
    rmsV=5.
    cs=1.
    mach_bh= lee_mach_bh(rmsV,cs)
    return np.log( mach_bh**-2*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n) )

def add_fits_to_plot(ax, beta_rms,mdots):
	beta_cont= np.logspace(-3,1,num=1000)
	p0 = [19.8,1.]
	opt, pcov = curve_fit(lee_parallel,beta_rms,mdots,p0=p0)
	lab=r'$\dot{M}_{\parallel}: \beta_{ch}= %.1f, n = %.1f$' % (opt[0],opt[1])
	#ax.plot(beta_cont,np.exp(lee_parallel(beta_cont,opt[0],opt[1])),'b-',lw=2,label=lab)
	#
	p0 = [19.8]
	opt, pcov = curve_fit(lee_parallel_neq100,beta_rms,mdots,p0=p0)
	lab=r'$\dot{M}_{\parallel}: \beta_{ch}= %.1f, n \equiv %.1f$' % (opt[0],100)
	#ax.plot(beta_cont,np.exp(lee_parallel_neq100(beta_cont,opt[0])),'r-',lw=2,label=lab)
	#
	p0 = [19.8]
	opt, pcov = curve_fit(lee_parallel_neq8,beta_rms,mdots,p0=p0)
	#lab=r'$\dot{M}_{\parallel}: \beta_{ch}= %.2f, n \equiv %d$' % (opt[0],8)
	lab=r'$\dot{M}_{\rm{Magn.}}: \beta_{ch}= %.1f, n \equiv %d$' % (opt[0],8)
	ax.plot(beta_cont,np.exp(lee_parallel_neq8(beta_cont,opt[0])),'b-',lw=4,label=lab)
	#
	lab=r'$\dot{M}_{\parallel}: \beta_{ch}=19.8, n=1$ (Lee 2014)'
	#ax.plot(beta_cont,np.exp(lee_parallel(beta_cont,19.8,1.)),'g-',lw=2,label=lab)
	#p0 = [19.8]
	#opt, pcov = curve_fit(lee_parallel_neq100,beta_rms,mu,p0=p0)
	#lab=r'$\beta_{ch}= %.1f$' % (opt[0])
	#ax[0].plot(beta_cont,lee_parallel_neq100(beta_cont,opt[0]),'b-',lw=2,label=lab)
	#	p0 = [1,19.8,
	#	p0 = [1,19.8,1] 
	#	for i in range(3):
	#		fit = leastsq(residuals_par, p0, args=(log_mean, log_beta))
	#		p0=lsq_mean[0]
	#		plt.plot(log_beta, lee_parallel(log_beta,lsq_mean[0][0],lsq_mean[0][1],lsq_mean[0][2]),c='b',ls='-',label=r'$\parallel$ mean'
	p0 = [11.1]
	opt, pcov = curve_fit(magnetic_bh_neq8,beta_rms,mdots,p0=p0)
	lab=r'$\dot{M}_{\rm{fit,bh}}: \beta_{ch}= %.1f, n= 8' % (opt[0])
	ax.plot(beta_cont,np.exp(magnetic_bh_neq8(beta_cont,opt[0])),'r-',lw=4,label=lab)
	#)

def add_sams_to_plot(ax, stat,beta_rms,sam):
	for model,lab,color in zip(['w','fit','fit_w'],['Vorticity','Magnetic','Magn.+Vort.'],['g','r','b']):
		ax.plot(beta_rms,sam[model][stat],c=color,ls='-',lw=2,label=lab)


#get 64 sink data
mean=np.zeros(5)-1
median=mean.copy()
var=mean.copy()
#
beta= np.array(['0.01','0.1','1','10','100'])
for cnt,b in enumerate(beta):
    fin=open(os.path.join(args.root_dir,"b%s/" % b,"beta_%s_indivSteadyMdots.pickle" % b),'r')
    a=pickle.load(fin)
    fin.close()
#     mu[cnt]= np.mean(np.log(a))
    mean[cnt]= np.mean(np.log(a))
    median[cnt]= np.median(np.log(a))
    var[cnt]=np.var(np.log(a))
va_rms=np.array([14.18,5.15,3.52,2.12,1.10])
beta_rms= 2/va_rms**2
for b,me,md,sig in zip(beta_rms,mean,median,var): print "beta_rms,mean,median,var= ",b,me,md,sig
#get semi-analytic-model data
sam={}
for model in ['w','fit','fit_w']: 
	sam[model]= {}
	for stat in ['mean','median','std']: sam[model][stat]= np.zeros(5)-1
beta= np.array(['0.01','0.1','1','10','100'])
for cnt,b in enumerate(beta):
	fin=open(os.path.join(args.root_dir,"b%s/" % b,"cellMdots_stats_"+args.stat_type+".pickle"),'r')
	(me,md,sig)=pickle.load(fin)
	fin.close()
	sam['w']['mean'][cnt]= me['w']
	sam['w']['median'][cnt]= md['w']
	sam['fit']['mean'][cnt]= me['fit']
	sam['fit']['median'][cnt]= md['fit']
	sam['fit_w']['mean'][cnt]= me['fit_w']
	sam['fit_w']['median'][cnt]= md['fit_w']
if args.stat_type == 'log':
	for model in ['w','fit','fit_w']: 
		for stat in ['mean','median','std']: sam[model][stat]= np.exp(sam[model][stat])
####
##plot
kwargs= dict(labargs=dict(fontsize=22,fontweight='bold'),\
			tickargs=dict(fontsize=20),\
			legargs=dict(fontsize=20),\
			annargs=dict(fontsize=22,fontweight='bold'))
matplotlib.rcParams['xtick.labelsize'] = kwargs['tickargs']['fontsize']
matplotlib.rcParams['ytick.labelsize'] = kwargs['tickargs']['fontsize']

fig,axes=plt.subplots(1,2) #,sharey=True)
fig.set_size_inches(14, 8)
plt.subplots_adjust(wspace=0)
ax=axes.flatten()
#pts to fit to
area=600
ax[0].scatter(beta_rms,np.exp(median),s=area,marker='o',label=r'Simulation',edgecolors='black',facecolors='none',linewidths=4)
ax[1].scatter(beta_rms,np.exp(mean),s=area,marker='o',label=r'Simulation',edgecolors='black',facecolors='none',linewidths=4)
##annotate
for b0,brms,mdot in zip(['0.01','1','100'],beta_rms[[0,2,4]],np.exp(mean)[[0,2,4]]):
	if b0 == '0.01': ax[1].annotate(r'$\beta_0$ = %s' % b0, xy=(brms, mdot/1.075), xytext=(brms*1.5, mdot/1.75),
				arrowprops=dict(facecolor='black', shrink=0.05),ha='center',**kwargs['annargs'])
	else: ax[1].annotate(r'$\beta_0$ = %s' % b0, xy=(brms, mdot/1.075), xytext=(brms/1.05, mdot/1.75),
				arrowprops=dict(facecolor='black', shrink=0.05),ha='center',**kwargs['annargs'])
#
if args.plot_fits:
	add_fits_to_plot(ax[0], beta_rms,median)
	add_fits_to_plot(ax[1], beta_rms,mean)

if args.plot_sams:
	add_sams_to_plot(ax[0], 'median',beta_rms,sam)
	add_sams_to_plot(ax[1], 'mean',beta_rms,sam)

ax[0].set_ylabel(r'$\dot{M}/\dot{M}_B$',**kwargs['labargs'])
#ax[1].set_ylabel(r'Median ln($\dot{M}/\dot{M}_B$)',fontsize='medium')
#ax[1].set_xlim(-0.1,2)
for i,title in enumerate(['Median','Mean']): ax[i].set_title(title,**kwargs['labargs'])
for i in range(2): 
	ax[i].set_xscale('log')
	ax[i].set_xlabel(r'$\beta_{rms}$',**kwargs['labargs'])
	ax[i].legend(loc=2,numpoints=1,scatterpoints=1,**kwargs['legargs'])
	ax[i].set_xlim(1e-3,1e1)
	ax[i].set_ylim(4e-4,2e-2) #np.exp(-9.),np.exp(-3.))
	ax[i].set_yscale('log')
#xticks=np.logspace(-3,1,5)
ax[0].set_xticks(np.logspace(-3,0,4))
ax[1].set_yticklabels([])
plt.savefig('lsqr_fit_lee14_'+args.stat_type+'.png')
plt.close()

