%Run: /Applications/MATLAB_R2014b.app/bin/matlab -nodisplay -nojvm -nosplash -nodesktop -r vel2curl
%use: read in velocities from python, use matlab gradient func to calc curl **CANNOT use curl() b/c working with pythonic indexed arrays
%assumes dx = 1, so correct w is returned
load vels.mat
[py, junk, pz] = gradient(vx);
[junk, qx, qz] = gradient(vy);
[ry, rx, junk] = gradient(vz);
curlx = ry-qz;  
curly = pz-rx;
curlz = qx-py;
mag=sqrt(curlx.^2 + curly.^2 + curlz.^2);
save('saved_curlmag.mat','mag')
