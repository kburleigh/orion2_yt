import sys
Lib_path = "/global/home/users/kaylanb/myCode/ytScripts/Lib"
sys.path.append(Lib_path)
import deriv_fields
import perturbation_lib as pl
import init_cond as mic

path= "/clusterfs/henyey/kaylanb/Test_Prob/Turb/bondiTurb/quick/driving_t0_only/HD/lev2/"

p= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
ic = mic.calcIC(p)
ic.prout()

#pl.perturbation(64,1,2,0.667,2.)

#pl.plot_Pk_and_Hist_rmsV_all_simData(path, ic.v0, ic.v0/8., ic.tCr)
pl.hist_Rho_all_data(path,2, ic.rho0, ic.tCr)

