'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and plot vs. time'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-cs",action="store",help='adiabatic sound speed in cm/sec')
args = parser.parse_args()
if args.cs: 
    cs = float(args.cs)
else: raise ValueError

from yt.mods import *
import numpy as np
import glob

f_hdf5= glob.glob("./data.*.3d.hdf5")
f_hdf5.sort()

fout=open("AvgQs.txt","w")
fout.write("#rho.mean() vrms/cs time datafile\n")
fout.close()
for i in range(0,len(f_hdf5)-1,2):
    pf=load(f_hdf5[i])
    lev=0
    cube= pf.h.covering_grid(level=lev,\
                      left_edge=pf.domain_left_edge,
                      dims=pf.domain_dimensions)
    rho = np.array(cube["density"])
    vx= np.array(cube["X-momentum"])/rho
    vy= np.array(cube["Y-momentum"])/rho
    vz= np.array(cube["Z-momentum"])/rho
    vrms2= (vx**2+vy**2+vz**2)
    vrms2_MassWt= np.average(vrms2,weights=rho)
    vrms_3D= vrms2_MassWt**0.5
    stuff="covering_grid: %.5e %.5e %.15e %s\n" % (rho.mean(),vrms_3D/cs,pf.current_time,pf.basename)
    fout=open("AvgQs.txt","a")
    fout.write(stuff)
    fout.close()
    #all levels, uses entire box
    ad= pf.h.all_data()
    rho = np.array(ad["density"])
    vx= np.array(ad["X-momentum"])/rho
    vy= np.array(ad["Y-momentum"])/rho
    vz= np.array(ad["Z-momentum"])/rho
    vrms2= (vx**2+vy**2+vz**2)
    vrms2_MassWt= np.average(vrms2,weights=rho)
    vrms_3D= vrms2_MassWt**0.5
    stuff="all_data(): %.5e %.5e %.15e %s\n" % (rho.mean(),vrms_3D/cs,pf.current_time,pf.basename)
    fout=open("AvgQs.txt","a")
    fout.write(stuff)
    fout.close()
print 'done'
