import yt
import argparse
import os
import sys
import re
import numpy as np
import scipy
import scipy.integrate as integrate
import scipy.interpolate as interpolate
import scipy.optimize as optimize
import matplotlib as mpl
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-data_hdf5",action="store",help='wildcard for hdf5 files',required=True)
args = parser.parse_args()

def _KaysVx(field, data):
    return data["X-momentum"] / data["density"]
def _KaysVy(field, data):
    return data["Y-momentum"] / data["density"]
def _KaysVz(field, data):
    return data["Z-momentum"] / data["density"]
def _KaysBx(field, data):
    rt4pi= 3.5449
    return data["X-magnfield"]*rt4pi
def _KaysBy(field, data):
    rt4pi= 3.5449
    return data["Y-magnfield"]*rt4pi
def _KaysBz(field, data):
    rt4pi= 3.5449
    return data["Z-magnfield"]*rt4pi

ds = yt.load(args.data_hdf5) # load data
ds.add_field("KaysVx", function=_KaysVx, units="cm/s")
ds.add_field("KaysVy", function=_KaysVy, units="cm/s")
ds.add_field("KaysVz", function=_KaysVz, units="cm/s")
ds.add_field("KaysBx", function=_KaysBx, units="gauss")
ds.add_field("KaysBy", function=_KaysBy, units="gauss")
ds.add_field("KaysBz", function=_KaysBz, units="gauss")

rho0=1.e-2
center = [0,0,0]
width = (float(ds.domain_width[0]), 'cm') 
Npx= 1000
res = [Npx, Npx] # create an image with 1000x1000 pixels
cut_dir='z'
proj = ds.slice(axis=cut_dir,coord=0.)
frb = proj.to_frb(width=width, resolution=res, center=center)
img= np.array(frb['density'])[::-1]/rho0
bx2d,by2d= np.array(frb['KaysBx'])[::-1], np.array(frb['KaysBy'])[::-1]
#X, Y = np.meshgrid(np.arange(0,Npx), np.arange(0,Npx))

xmin= 0.#np.array([0.488,0.488,0.488])*rb_x-rc
xmax= float(Npx-1) #np.array([0.512,0.512,0.512])*rb_x-rc
dx=[1.,1.,1.]

# pick some footpoints
nf=20
# z=0 plane
xfline = (np.arange(nf,dtype=np.float)/nf+0.5/nf)*(xmax-xmin)+xmin
yfline = np.zeros_like(xfline)-0.5
# circle
nf=10
r = 0.125*min(xmax-xmin,xmax-xmin)
center = [0.5*(xmax+xmin),0.5*(xmax+xmin)]
xfcircle = r*np.cos(2*np.pi*(np.linspace(0,1,nf,endpoint=False)+0.5/nf))+center[0]
yfcircle = r*np.sin(2*np.pi*(np.linspace(0,1,nf,endpoint=False)+0.5/nf))+center[1]
#
xf = np.concatenate((xfline,xfcircle))
yf = np.concatenate((yfline,yfcircle))
# integrate some field lines
nstep=3*np.max(bx2d.shape)
tend=np.max(xmax-xmin)
parametric_t=np.linspace(0,tend,nstep)

def bfunc(x,t):
	i = int(np.floor((x[0]-xmin)/(xmax-xmin) * bx2d.shape[0]))
	i = min(max(i,0),bx2d.shape[0]-1)
	j = int(np.floor((x[1]-xmin)/(xmax-xmin) * bx2d.shape[1]))
	j = min(max(j,0),bx2d.shape[1]-1)

	return np.array([bx2d[i,j],by2d[i,j]])

def jac(x,t):
	i = int(np.floor((x[0]-xmin)/(xmax-xmin) * bx2d.shape[0]))
	i = min(max(i,1),bx2d.shape[0]-2)
	j = int(np.floor((x[1]-xmin)/(xmax-xmin) * bx2d.shape[1]))
	j = min(max(j,1),bx2d.shape[1]-2)
	dbx_dxL = (bx2d[i  ,j  ]-bx2d[i-1,j  ])/dx[1]
	dbx_dyL = (bx2d[i  ,j  ]-bx2d[i  ,j-1])/dx[2]
	dby_dxL = (by2d[i  ,j  ]-bx2d[i-1,j  ])/dx[1]
	dby_dyL = (by2d[i  ,j  ]-bx2d[i  ,j-1])/dx[2]
	dbx_dxR = (bx2d[i+1,j  ]-bx2d[i  ,j  ])/dx[1]
	dbx_dyR = (bx2d[i  ,j+1]-bx2d[i  ,j  ])/dx[2]
	dby_dxR = (by2d[i+1,j  ]-bx2d[i  ,j  ])/dx[1]
	dby_dyR = (by2d[i  ,j+1]-bx2d[i  ,j  ])/dx[2]

	dbx_dx = 0.5*(dbx_dxL+dbx_dxR)
	dbx_dy = 0.5*(dbx_dyL+dbx_dyR)
	dby_dx = 0.5*(dby_dxL+dby_dxR)
	dby_dy = 0.5*(dby_dyL+dby_dyR)

	return np.array([[dbx_dx,dbx_dy],[dby_dx,dby_dy]])

def mbfunc(x,t): return -bfunc(x,t)

def mjac(x,t): return -jac(x,t)

points = []
for line in range(0,len(xf)):
	print "starting footpoint= ",xf[line],yf[line]
	points.append(integrate.odeint( bfunc,np.array([xf[line],yf[line]]),parametric_t,Dfun= jac,mxstep=600000,rtol=0.,atol=0.01*np.min(dx)))
	points.append(integrate.odeint(mbfunc,np.array([xf[line],yf[line]]),parametric_t,Dfun=mjac,mxstep=600000,rtol=0.,atol=0.01*np.min(dx)))


# Defines Colormap  :  See Help(Plt.Colormaps)
cm = mpl.cm.hot


# Normalizes        : Norm = Mpl.Colors.Normalize(Vmin=1e-8, vmax=1e-7)
# ----------------------------------------------------------------------
norm = mpl.colors.Normalize()
#norm = mpl.colors.Normalize(vmin=-0.5, vmax=3.0) #Rho/Rho_0
#norm = mpl.colors.Normalize(vmin=1.0e-8-2.0e-8, vmax=1.0e-8+3.0e-8)

#norm = mpl.colors.Normalize(vmin=-1, vmax=2)    
#norm = mpl.colors.Normalize(vmin=0.05, vmax=0.15)

mpl.rc('xtick', labelsize=15)
mpl.rc('ytick', labelsize=15) 

#Andrew note: imshow is pretty fucked up.  Must use transpose, origin="lower" or else everything comes up backwards
plt.gca().set_aspect(1.0)
plt.imshow(np.log10(img),cmap=cm,norm=norm)
plt.colorbar()

for i in range(0,len(points)):
	xdata = (points[i])[:,0]
	ydata = (points[i])[:,1]
	args = np.where(xdata>xmin)
	xdata=xdata[args]; ydata=ydata[args]
	args = np.where(xdata<xmax)
	xdata=xdata[args]; ydata=ydata[args]
	args = np.where(ydata>xmin)
	xdata=xdata[args]; ydata=ydata[args]
	args = np.where(ydata<xmax)
	xdata=xdata[args]; ydata=ydata[args]
	plt.plot(xdata,ydata,color='#33CC33',lw=2)
#plt.plot(xdata,ydata,color='0.5',lw=2)
	#plt.plot(xf,yf,marker='o',color='g',linestyle='None')

# draw outline of sink region
nf=40
center=[0.,0.]
rsink=5.
xsink = rsink*np.cos(2*np.pi*(np.linspace(0,1,nf)+0.5/nf))+center[0]
ysink = rsink*np.sin(2*np.pi*(np.linspace(0,1,nf)+0.5/nf))+center[1]
plt.plot(xsink,ysink,color='k',lw=2)

ax=plt.gca()
ax.set_xlim(xmin,xmax)
ax.set_ylim(xmin,xmax)
#ax.set_xlim(-0.5,0.5)
#ax.set_ylim(-0.5,0.5)

ax.xaxis.set_major_locator(plt.MaxNLocator(5))
ax.yaxis.set_major_locator(plt.MaxNLocator(5))
plt.xlabel(r"$Y/r_B$")
plt.ylabel(r"$Z/r_B$")
#plt.title(title+"$t$ = "+'%.2f'%time+r' $t_B$',fontsize=20)

plt.savefig("blines.png")
