#https://yt.readthedocs.org/en/yt-2.4/cookbook/complex_plots.html
from yt.mods import * # set up our namespace
import matplotlib.colorbar as cb
#import matplotlib.colors as mc
import numpy as np

import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import my_fund_const as fc
import ic_dfields as myic      #intial conditions and all derived fields for simulation data

def test_plot(path,file, rhomin, rhomax):
	'''make multiplot for one output data file, to get what want'''
	pf = load(path+f) # load data

	# There's a lot in here:
	#   From this we get a containing figure, a list-of-lists of axes into which we
	#   can place plots, and some axes that we'll put colorbars.
	# We feed it:
	#   Number of plots on the x-axis, number of plots on the y-axis, and how we
	#   want our colorbars oriented.  (This governs where they will go, too.
	#   bw is the base-width in inches, but 4 is about right for most cases.
	orient="vertical"
	fig, axes, colorbars = get_multi_plot(1, 2, colorbar=orient, bw = 4)

	# We'll use a plot collection, just for convenience's sake
	pc = PlotCollection(pf, pf.domain_center)

	# Now we follow the method of "multi_plot.py" but we're going to iterate
	# over the columns, which will become axes of slicing.
	rhomin=ic.rho0 * 1.e-1
	rhomax = ic.rho0 * 1.e1

	#make plots
	p = pc.add_slice("density", "x", figure = fig, axes = axes[0][0],
				 use_colorbar=False)
	p.modify["velocity"]()
	p.modify["point"](pf.domain_center, "VELOCITY Arrows")
	p.set_cmap("winter") # this is our colormap
	p.set_zlim(rhomin,rhomax)
	# We do this again, but this time we take the 1-index column.
	p = pc.add_slice("density", "x", figure=fig, axes=axes[1][0],
				 use_colorbar=False)
	#p.modify["quiver"]("x-vel","y-vel",10,normalize=True)
	p.modify["magnetic_field"]()
	p.modify["point"](pf.domain_center, "B FIELD Arrows")
	p.set_cmap("winter") # a different colormap
	p.set_zlim(rhomin,rhomax) 
	#

	# Each 'p' is a plot -- this is the Density plot and the Temperature plot.
	# Each 'cax' is a colorbar-container, into which we'll put a colorbar.
	# zip means, give these two me together.  Note that it cuts off after the
	# shortest iterator is exhausted, in this case pc.plots.
	for ind in range(2):
		p = pc.plots[ind]
		cbar = cb.Colorbar(colorbars[ind], p.image, orientation=orient)
		p.colorbar = cbar
		p._autoset_label()
	#for p, cax in zip(pc.plots, colorbars):
	#    # Now we make a colorbar, using the 'image' attribute of the plot.
	#    # 'image' is usually not accessed; we're making a special exception here,
	#    # though.  'image' will tell the colorbar what the limits of the data are.
	#    cbar = cb.Colorbar(cax, p.image, orientation=orient)
	#    # Now, we have to do a tiny bit of magic -- we tell the plot what its
	#    # colorbar is, and then we tell the plot to set the label of that colorbar.
	#    p.colorbar = cbar
	#    p._autoset_label()

	# And now we're done!  Note that we're calling a method of the figure, not the
	# PlotCollection.
	fig.savefig(spath+"multi_%s.pdf" % (f[5:9],),format="pdf")


#Initial conditions for SIM
pars =myic.ParsNeed()
pars.temp=1.e6
pars.mach_s=1.41
pars.cells=32.
pars.levs =0
pars.Lbox=10.
pars.prout()
ic=myic.initCondData(pars)
ic.prout()
myic.add_ALL_fields(ic)
#######

path = "/clusterfs/henyey/kaylanb/Test_Prob/Sink/Lee14/quick/beta-10_M-1.41/lev2/"
spath = path+"plots/"

dpatt = "data.00*.3d.hdf5"
files = glob.glob1(path, dpatt)

rhomin=ic.rho0 * 1.e-1
rhomax = ic.rho0 * 1.e1
for f in files:
     test_plot(path,f, rhomin,rhomax)
