%:Run: /Applications/MATLAB_R2014b.app/bin/matlab -nodisplay -nosplash -nodesktop -r vel2curl 
load bins_to_matlab.mat %contains: med_data,mean_data,med_pred,mean_pred,bins used to make data pdf
%cdfs
mu=log( median(data) );
sig=sqrt(2*log( mean(data)/median(data) ));
data_model = makedist('Lognormal','mu',mu,'sigma',sig);
[h1,p1,ks1,cv1]=kstest(data,'CDF',data_model,'Alpha',0.2);
data_model_cdf=data_model.cdf(bins);
data_model_pdf=bins.*data_model.pdf(bins);
%best fit
mu= best_fit(1);
sig= best_fit(2);
bf_model = makedist('Lognormal','mu',mu,'sigma',sig);
[h2,p2,ks2,cv2]=kstest(data,'CDF',bf_model,'Alpha',0.2);
bf_model_cdf=bf_model.cdf(bins);
bf_model_pdf=bins.*bf_model.pdf(bins);
%
%fill for as many predictions as have
%h=zeros(1,3)-1;p=zeros(1,3)-1;ks=zeros(1,3)-1;cv=zeros(1,3)-1;
%pred_model_cdf=zeros(numel(bins),3)-1;
%pred_model_pdf=zeros(numel(bins),3)-1;
%for i = 1:numel(predict_mean)
%	mu=log(predict_med(i));
%	sig=sqrt(2*log(predict_mean(i)/predict_med(i)));
%	pred_model = makedist('Lognormal','mu',mu,'sigma',sig);
%	[h(i),p(i),ks(i),cv(i)]=kstest(data,'CDF',pred_model,'Alpha',0.2);
%	pred_model_cdf(:,i)=pred_model.cdf(bins);
%	pred_model_pdf(:,i)=bins.*pred_model.pdf(bins);
%end

%subplot(2,1,1)
%A = stairs(data_cdf_x,data_cdf,'-k','LineWidth',2);
%hold on;
%B = plot(data_cdf_x,data_model_cdf,'b-','LineWidth',2);
%hold on;
%C = plot(data_cdf_x,theory_model_cdf,'r-','LineWidth',2);
%set(gca, 'XScale', 'log')
%xlabel('$$\mathbf{\beta}$$','Interpreter','latex');
%ylabel('CDF');
%subplot(2,1,2)
%%a=bar(exp(centers),N,'hist','k')
%stairs(exp(centers),N,'-k','LineWidth',2);
%set(gca, 'XScale', 'log');
%hold on
%x=exp(edges);
%semilogx(x,x.*data_model.pdf(x),'b-','LineWidth',2);
%hold on;
%semilogx(x,x.*theory_model.pdf(x),'r-','LineWidth',2);
%xlabel('$$\mathbf{\beta}$$','Interpreter','latex');
%ylabel('PDF = dP / d ln$$\mathbf{\dot{M}/\dot{M}_B}$$','Interpreter','latex');
%legend([A B C],...
%       'data','data model','theory model',...
%       'Location','SE');
%saveas(gcf,'cdf.png')
%save('cdf_pdf_to_python.mat','data_model_cdf','data_model_pdf','pred_model_cdf','pred_model_pdf',...
		%'h1','p1','ks1','cv1','h','p','ks','cv')
save('cdf_pdf_to_python.mat','data_model_cdf','data_model_pdf','bf_model_cdf','bf_model_pdf',...
		'h1','p1','ks1','cv1','h2','p2','ks2','cv2')
exit
