'''Run IFF already ran:
1) "Sim_AvgQs.py", outputting "tseries.dump" 
2) "Sim_SinkData.py", outputting "sink_data.pickle" '''

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import glob
import pickle

import my_fund_const as fc

#K06 Mdot
def func_wstar(w,Msink,cs):
    rB= fc.G*Msink/cs**2
    return w*rB/cs
def func_fstar(w_star):
    if w_star > 1e4:
        raise ValueError
    else:
        return 1./(1+w_star**0.9)
def Mdot_wstar(Msink,rho_inf,cs,f_star):
    return 4*np.pi*rho_inf*(fc.G*Msink)**2/cs**3*0.34*f_star
def Mdot_BH(rho_inf,Msink,cs,MachS):
    lam=1.1
    head=4.*np.pi*rho_inf*(fc.G*Msink)**2/cs**3
    tail= ((lam**2+MachS**2)/(1+MachS**2)**4)**0.5
    return head*tail
def Mdot_0(Msink,rho_inf,MachS,cs):
    return 4*np.pi*rho_inf*(fc.G*Msink)**2/(MachS*cs)**3
def Mdot_turb(Mdot_BH,Mdot_wstar):
    return (Mdot_BH**-2+Mdot_wstar**-2)**-0.5

#Lee14 Mdot, eqns 27,30
def Mdot_B(Msink,rho_inf,cs):
    lam=1.1
    return 4.*np.pi*lam*(fc.G*Msink)**2*rho_inf/cs**3
def script_MBH(MachS):
    lam=1.1
    head= (1+MachS**4)**0.333
    tail=(1+(MachS/lam)**2)**(1./6)
    return head/tail
def Mdot_par(script_MBH,Mdot_B,beta):
    beta_ch= 18.3
    n=0.94
    head=Mdot_B/script_MBH**2
    tail=(script_MBH**n+(beta_ch/beta)**(n/2))**(-1/n)
    return head*tail
def Mdot_perp(script_MBH,Mdot_B,beta):
    beta_ch= 18.3
    n=0.94
    head=Mdot_B/script_MBH
    tail=(script_MBH**n+(beta_ch/beta)**(n/2))**(-1/n)
    a=head*tail
    b=Mdot_B/script_MBH**3
    print "a,b= ",a,b
    print "min= ",min([a,b])
    #can use min() when args are arrays
#     minima= a.copy()
#     ind=np.where(a<b)[0]
#     minima[ind]= a[ind]
#     ind=np.where(a>=b)[0]
#     minima[ind]= b[ind]
#     return minima
    return min([a,b])

#Calc all Mdot
class Mdots_K06():
    def __init__(self,Mdot_BH,Msink,rho_inf,cs,f_star,MachS):
        self.M0= Mdot_0(Msink,rho_inf,MachS,cs)
        self.wstar= Mdot_wstar(Msink,rho_inf,cs,f_star)
        self.turb= Mdot_turb(Mdot_BH,self.wstar)
class Mdots_Lee14():
    def __init__(self,Mdot_B,Msink,rho_inf,cs,lee_script_MBH,beta):
        self.par= Mdot_par(lee_script_MBH,Mdot_B,beta)
        self.perp= Mdot_perp(lee_script_MBH,Mdot_B,beta)
class Mdots():
    def __init__(self,Msink,rho_inf,cs,f_star,MachS,lee_script_MBH,beta):
        self.B= Mdot_B(Msink,rho_inf,cs)
        self.BH= Mdot_BH(rho_inf,Msink,cs,MachS)
        self.K06= Mdots_K06(self.BH,Msink,rho_inf,cs,f_star,MachS)
        self.Lee14= Mdots_Lee14(self.B,Msink,rho_inf,cs,lee_script_MBH,beta)
###
def plot_Mdots(fsave,mdot,sink,tB):
    '''ylim (optional)'''
    fig,ax= plt.subplots(figsize=(5,5))
    time= (sink.time - sink.time[0])/tB
    M0= mdot.K06.M0
    labs=[r'$\dot{M}_{MEAN}$',r'$\dot{M}_{MED}$',\
          r'$\dot{M}_{B}$',r'$\dot{M}_{BH}$',\
          r'$\dot{M}_{\omega}$',r'$\dot{M}_{turb}$',\
          r'$\dot{M}_{B \parallel}$',r'$\dot{M}_{B \perp}$']
    ax.plot(time,np.average(sink.Mdot,axis=1)/M0,"k-*",label=labs[0])
    ax.plot(time,np.median(sink.Mdot,axis=1)/M0,"k-^",label=labs[1])
    ax.hlines(mdot.B/M0,time.min(),time.max(),\
              colors="k",linestyle="solid",label=labs[2])
    ax.hlines(mdot.BH/M0,time.min(),time.max(),\
              colors="k",linestyle="dashed",label=labs[3])
    ax.hlines(mdot.K06.wstar/M0,time.min(),time.max(),\
              colors="b",linestyle="solid",label=labs[4])
    ax.hlines(mdot.K06.turb/M0,time.min(),time.max(),\
              colors="b",linestyle="dashed",label=labs[5])
    ax.hlines(mdot.Lee14.par/M0,time.min(),time.max(),\
              colors="r",linestyle="solid",label=labs[6])
    ax.hlines(mdot.Lee14.perp/M0,time.min(),time.max(),\
              colors="r",linestyle="dashed",label=labs[7])
    ax.set_xlabel(r"$\mathbf{t/t_B}$")
    ax.set_ylabel(r"$\mathbf{\dot{M}/\dot{M}_0}$")
    leg_box=ax.legend(loc=(1.01,0.01),ncol=1)
    ax.set_yscale('log')
#     if ylim: ax.set_ylim([ylim[0],ylim[1]])
    fig.savefig(fsave,dpi=150,bbox_extra_artists=(leg_box,), bbox_inches='tight')

###main
class TimeSeries():
    def __init__(self,files):
        '''files: hdf5 file list'''
        self.Time= np.zeros(len(files)) -1
        self.Rho= self.Time.copy()
        self.MachS= self.Time.copy()
        self.Beta= self.Time.copy()
        self.MachA= self.Time.copy()
        self.Curl= self.Time.copy()
        self.Brms= self.Time.copy()

class SinkData():
    def __init__(self,NSinks,NSinkFiles):
        shape= (NSinkFiles,NSinks)
        self.time= np.zeros(NSinkFiles)-1
        self.mass= np.zeros(shape)-1
        self.x= np.zeros(shape)-1
        self.y= np.zeros(shape)-1
        self.z= np.zeros(shape)-1
        self.Mdot= np.zeros(shape)-1 #Mdot of sink particles
    def get_time(self,f_hdf5):
        for cnt,f in enumerate(f_hdf5):
            print "sink time: loading %s" % f
            pf=load(f)
            self.time[cnt]= pf.current_time
    def get_MassPosMdot(self,f_sink):
        '''f_sink: list of .sink files'''
        for cnt in range(len(f_sink)):
            print "sink MassPos: loading %s" % f_sink[cnt]
            mass,x,y,z= np.loadtxt(f_sink[cnt],dtype='float',\
                                   skiprows=1,usecols=(0,1,2,3),unpack=True)
            self.mass[cnt,:]= mass
            self.x[cnt,:]= x
            self.y[cnt,:]= y
            self.z[cnt,:]= z
        #calc Mdot
        if self.time[0].astype('int') == -1:
            print "time not loaded"
            raise ValueError
        else:
            self.Mdot[0,:]=0.
            for cnt in np.arange(1,len(f_sink)):
                deltaM= self.mass[cnt,:] -self.mass[cnt-1,:] 
                deltaT= self.time[cnt]- self.time[cnt-1]
                self.Mdot[cnt,:]= deltaM/deltaT

spath="./plots/"
fin=open("../plots/tseries.dump",'r')
(ts,cs_iso,tB)=pickle.load(fin)
fin.close()
fin=open("./plots/sink_data.pickle",'r')
sink=pickle.load(fin)
fin.close()

#evaluated once, at insert of sinks
Curl=ts.Curl[-1]
MachS= ts.MachS[-1]
Rho=ts.Rho[-1]
Beta=ts.Beta[-1]
Msinks_const= sink.mass[0,0] #sink.mass.shape = (Nfiles,Nsinks)
rB= fc.G*Msinks_const/cs_iso**2
w_star = Curl*rB/cs_iso
print "w_star = %f" % w_star
f_star= func_fstar(w_star)
lee_script_MBH= script_MBH(MachS)

mdot= Mdots(Msinks_const,Rho,cs_iso,f_star,MachS,lee_script_MBH,Beta)
#save mdot analystic vals
fout=open(spath+"analytic_mdots.pickle","w")
pickle.dump(mdot,fout)
fout.close()

fsave=spath+"mdot_results.png"
plot_Mdots(fsave,mdot,sink,tB)#ylim=(10**-4,10**1))
