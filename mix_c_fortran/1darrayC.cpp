#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

double ***make_KJB3(int nrow,int ncol, int ndep) {
   int i,j,k;
   double ***t =(double ***) malloc((size_t)((nrow)*sizeof(double**)));
   if (!t) {
    printf ("Allocation Failure in matrix (1) \n");
    exit (1);
   }
    for (i=0; i<nrow; i++) {
        t[i]=(double **) malloc((size_t)((nrow*ncol)*sizeof(double*)));
        if (!t[i]) {
            printf ("Allocation Failure in matrix (2) \n");
            exit (1);
        }
        for (j=0; j<ncol; j++) {
            t[i][j]=(double *) malloc((size_t)((nrow*ncol*ndep)*sizeof(double)));
            if (!t[i][j]) {
                printf ("Allocation Failure in matrix (3) \n");
                exit (1);
            }
        }
    }
    for (i=0; i<nrow; i++) {
        for (j=0; j<ncol; j++) {
            for (k=0; k<ndep; k++) {
                t[i][j][k]=-1;
    }}}
    return t;
}


void print_array(double *t,int nrow) {
    int i,j,k;
    for (i=0; i<nrow; i++) {
    			printf("t[%d]= %f\n", i,t[i]); 
  }

}

void fill_1d_asif_3d(double *t1,int nrow,int ncol,int ndep) {
    int i,j,k;
    for (i = 0; i < nrow; i++){
       for (j = 0; j < ncol; j++){
          for (k = 0; k < ndep; k++){
             t1[(i * ncol + j) * ndep + k]= i*2+k-j;
    }}}
}

void fill_3d(double ***t3,int nrow,int ncol,int ndep) {
    int i,j,k;
    for (i = 0; i < nrow; i++){
       for (j = 0; j < ncol; j++){
          for (k = 0; k < ndep; k++){
             t3[i][j][k]= i*2+k-j;
    }}}
}

extern "C" void fortfunc_(double *array,int *nrow,int *ncol,int *ndep);

int main()
{
  int i,j,k,nrow=3,ncol=3,ndep=3;
  //double *t3=(double *) malloc((size_t)((len)*sizeof(double))); 
  //for (i=0; i<len; i++) {
  //          t3[i]=i*2;
  //  }
  double *t1=(double *) malloc((size_t)((nrow*ncol*ndep)*sizeof(double)));
  fill_1d_asif_3d(t1, nrow,ncol,ndep); 
  //for (i=0; i<nrow*ncol*ndep; i++) {
  //  printf("t[%d]= %f\n", i,t1[i]);
  //}

  //double ***t3= make_KJB3(nrow,ncol,ndep);
  //fill_3d(t3,nrow,ncol,ndep);

  for (i = 0; i < nrow; i++){
       for (j = 0; j < ncol; j++){
          for (k = 0; k < ndep; k++){
             printf("cpp: i,j,k,t1= %d %d %d %.1f\n",i,j,k,t1[(i * ncol + j) * ndep + k]);
    }}}   
printf("before fortfunc\n"); 
  //print_array(t3,len);
  //fortfunc_(&*t3,&len); //ngrow);
  fortfunc_(&*t1,&nrow,&ncol,&ndep); //ngrow);
printf("after fortfunc\n"); 
  
  for (i = 0; i < nrow; i++){
       for (j = 0; j < ncol; j++){
          for (k = 0; k < ndep; k++){
             printf("cpp: i,j,k,t1= %d %d %d %.1f\n",i,j,k,t1[(i * ncol + j) * ndep + k]);
    }}}
  
  free(t1);
return 0;
}
