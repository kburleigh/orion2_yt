!subroutine fortfunc(array,ngrow)
subroutine fortfunc(array,nrow,ncol,ndep)
    implicit none
    !integer ngrow
    !real*8 array(-ngrow:ngrow, -ngrow:ngrow,-ngrow:ngrow)
    integer nrow,ncol,ndep
    real*8 array(nrow*ncol*ndep)
    integer i,j,k 
    !do k=1,ndep
    !   do j=1,ncol
    !      do i=1,nrow
    do i=0,nrow-1
       do j=0,ncol-1
          do k=0,ndep-1
            print *,"fort: i,j,k,arr= ",i,j,k,array((i * ncol + j) * ndep + k  +1)
            array((i * ncol + j) * ndep + k  +1) = array((i * ncol + j) * ndep +k  +1)-1
          enddo
        enddo
    enddo
    return 
end          
