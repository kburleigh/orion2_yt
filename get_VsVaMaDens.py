import argparse
import numpy as np
parser = argparse.ArgumentParser(description="plot Avg Va,Vs,Ma vs. time")
parser.add_argument("cs",type=np.float,help='sound speed, isothermal or gamma=1.001 ONLY')
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import matplotlib.pyplot as plt
import glob

#yt derived fields
def _xvel(field, data):
    return data['X-momentum']/data['density']
add_field("x-vel", function=_xvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _yvel(field, data):
    return data['Y-momentum']/data['density']
add_field("y-vel", function=_yvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _zvel(field, data):
    return data['Z-momentum']/data['density']
add_field("z-vel", function=_zvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bx(field, data):
    return (4.*fc.pi)**0.5 *data['X-magnfield']
add_field("Bx", function=_Bx, take_log=False,
          units=r'\rm{gauss}')

def _By(field, data):
    return (4.*fc.pi)**0.5 *data['Y-magnfield']
add_field("By", function=_By, take_log=False,
          units=r'\rm{gauss}')

def _Bz(field, data):
    return (4.*fc.pi)**0.5 *data['Z-magnfield']
add_field("Bz", function=_Bz, take_log=False,
          units=r'\rm{gauss}')

##
def _Vmag(field, data):
    return np.sqrt(data['x-vel']**2 + data['y-vel']**2 + data['z-vel']**2)
add_field("Vmag", function=_Vmag, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bmag(field, data):
    return np.sqrt( data['Bx']**2 + data['By']**2 + data['Bz']**2)
add_field("Bmag", function=_Bmag, take_log=False,
          units=r'\rm{gauss}')

def _Va(field, data):
    return data['Bmag'] / np.sqrt(4*np.pi* data['density'])
add_field("Va", function=_Va, take_log=False,
          units=r'\rm{cm/s}')
###
def plot_Vs_Va_Ma(cs,time,Vmag_Avg,Vmag_WtAvg,Va_Avg,Va_WtAvg):
    f,ax = plt.subplots(1,3)
    ax[0].plot(time,Vmag_Avg/cs,'k-',label="Avg")
    ax[0].plot(time,Vmag_WtAvg/cs,'k--',label="WtAvg")
    ax[0].set_title("Vs/cs")
    ax[0].legend(loc=0)
    ax[1].plot(time,Va_Avg/cs,'k-',label="Avg")
    ax[1].plot(time,Va_WtAvg/cs,'k--',label="WtAvg")
    ax[1].set_title("Va/cs")
    ax[1].legend(loc=0)
    ax[2].plot(time,Vmag_Avg/Va_Avg,'k-',label="Avg")
    ax[2].plot(time,Vmag_WtAvg/Va_WtAvg,'k--',label="WtAvg")
    ax[2].set_title("Vs/Va")
    ax[2].legend(loc=0)
    plt.savefig("./plots/VsVaMa.png")

def print_to_file_VsVaMa(cs,time,Dens_Avg,Dens_WtAvg,Vmag_Avg,Vmag_WtAvg,\
                         Va_Avg,Va_WtAvg):
    fout=open("./plots/DensVsVaMa.dat","w")
    fout.write("cs,time,Dens_Avg,Dens_WtAvg,Vmag_Avg,Vmag_WtAvg,Va_mag,Va_WtAvg\n")
    for cnt,t in enumerate(time):
        line="%g %g %g %g %g %g %g %g\n" % \
            (cs,t,Dens_Avg[cnt],Dens_WtAvg[cnt],\
             Va_Avg[cnt],Va_WtAvg[cnt],Vmag_Avg[cnt],Vmag_WtAvg[cnt])
        fout.write(line)
    fout.close()

##Main
f_drive_hdf5= glob.glob("data.*.3d.hdf5")
f_drive_hdf5.sort()
print f_drive_hdf5

Va_Avg= np.zeros(len(f_drive_hdf5))-1
Va_WtAvg= Va_Avg.copy()
Vmag_Avg= Va_Avg.copy()
Vmag_WtAvg= Va_Avg.copy()
time = Va_Avg.copy()
Dens_Avg= Va_Avg.copy()
Dens_WtAvg= Va_Avg.copy()
for cnt,f_drive in enumerate(f_drive_hdf5):
    pf=load(f_drive)
    cube= pf.h.covering_grid(level=0,\
                              left_edge=pf.h.domain_left_edge,
                              dims=pf.domain_dimensions)
    d = cube["density"]
    Va= cube["Va"]
    Vmag= cube["Vmag"]
    MassWt= d/d.max()
    time[cnt]= pf.current_time
    Dens_Avg[cnt]= np.average(d)
    Dens_WtAvg[cnt]= np.average(d,weights=MassWt)
    Va_Avg[cnt]= np.average(Va)
    Va_WtAvg[cnt]= np.average(Va,weights=MassWt)
    Vmag_Avg[cnt]= np.average(Vmag)
    Vmag_WtAvg[cnt]= np.average(Vmag,weights=MassWt)

print_to_file_VsVaMa(args.cs,time,Dens_Avg,Dens_WtAvg,Vmag_Avg,Vmag_WtAvg,\
                         Va_Avg,Va_WtAvg)
plot_Vs_Va_Ma(args.cs,time,Vmag_Avg,Vmag_WtAvg,Va_Avg,Va_WtAvg)
