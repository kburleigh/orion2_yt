#!/bin/bash
#SBATCH -J ygrids           # job name
#SBATCH -o run.o%j       # outputfile name (%j expands to jobID)
#SBATCH -e run.e%j       # error file name (%j expands to jobID)
#SBATCH -n 16             # total tasks (divide by 16 for number nodes)
#SBATCH -p serial     # queue (partition) -- normal, development, etc.
#SBATCH -t 00:30:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=kburleigh.gsi@gmail.com
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
cd /scratch/03048/kaylanb/production/256cube/THD/Mach40/r1/sinks_M5/sinks_64/256Proc/
export PATH=/home1/03048/kaylanb/anaconda/bin:$PATH
export MYDIR=/home1/03048/kaylanb/myCode/ytScripts/orion2_yt
python $MYDIR/allfile_multi_grids.py data.0035.3d.hdf5 -sp "./"
