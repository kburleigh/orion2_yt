import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import yt
from yt.units import *
from yt.utilities.physical_constants import *
import numpy as np
import numpy.linalg as LA
from mpi4py import MPI
import glob

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#yt.enable_parallelism()


def cooling_function(field,data):
    LAMBDA = np.zeros_like(kb*data['temperature'] * cm**3/s)
    kbT = (kb*data['temperature']).in_units('keV').value
    LAMBDA[np.where(kbT > 5.0)[0]] = 6.10540967286092 * (kbT[np.where(kbT > 5.0)[0]]/100.)**0.4
    LAMBDA[np.where((kbT<= 5.0)&(kbT > 0.7))[0]] = 1.842055928644565 *  (kbT[np.where((kbT<= 5.0)&(kbT > 0.7))[0]]/5.)**0.22
    LAMBDA[np.where((kbT<= 0.7)&(kbT > 0.13))[0]] = 1.1952286093343938 *  (kbT[np.where((kbT<= 0.7)&(kbT > 0.13))[0]]/0.7)**-0.5
    LAMBDA[np.where((kbT<= 0.13)&(kbT > 0.02))[0]] = 2.773500981126145 *  (kbT[np.where((kbT<= 0.13)&(kbT > 0.02))[0]]/0.13)**-1.7
    LAMBDA[np.where((kbT<= 0.02)&(kbT > 0.0017235))[0]] = 67.2 *  (kbT[np.where((kbT<= 0.02)&(kbT > 0.0017235))[0]]/0.02)**0.6
    LAMBDA[np.where((kbT<= 0.0017235)&(kbT > kbTfloor))[0]] = 15.44 *  (kbT[np.where((kbT<= 0.0017235)&(kbT > kbTfloor))[0]]/0.0017235)**6.0
    return LAMBDA*1e-23
yt.add_field(("gas", "cooling_function"), function=cooling_function, units='erg*cm**3/s', display_name=r"$\Lambda$")


def tcool(field,data):
    return 1.5*kb*data['temperature']/((data['density']/mp/0.62) * data['cooling_function'])
yt.add_field(("gas","tcool"),function=tcool,units="Myr", display_name=r"$\t_{cool}$")


def _my_radial_velocity(field, data):
    if data.has_field_parameter("bulk_velocity"):
        bv = data.get_field_parameter("bulk_velocity").in_units("cm/s")
    else:
        bv = data.ds.arr(np.zeros(3), "cm/s")
    xv = data["gas","velocity_x"] - bv[0]
    yv = data["gas","velocity_y"] - bv[1]
    zv = data["gas","velocity_z"] - bv[2]
    center = data.get_field_parameter('center')
    x_hat = data["x"] - center[0]
    y_hat = data["y"] - center[1]
    z_hat = data["z"] - center[2]
    r = np.sqrt(x_hat*x_hat+y_hat*y_hat+z_hat*z_hat)
    x_hat /= r
    y_hat /= r
    z_hat /= r
    return xv*x_hat + yv*y_hat + zv*z_hat
yt.add_field(("gas","rv"),
             function=_my_radial_velocity,
             units="cm/s",
             take_log=False,
             display_name=r"$\v_{r}$")


H0 = 70 * km /s / Mpc
mu = 0.62
athinput = open('athinput', 'r')
for line in athinput:
    if "Tigm" in line: Tigm = float(line.strip()[12:])
    if "M15" in line: M15 = float(line.strip()[12:])
athinput.close()

UnitLength = YTQuantity((G * M15 * 1e15 * Msun / H0**2)**(1./3.)).convert_to_units('kpc')
UnitTime = YTQuantity(1/H0).convert_to_units('yr')
UnitMass = YTQuantity(M15 * 1e15 * Msun).convert_to_units('Msun')
Tvir = ((0.5*mp*0.62*(10*H0*G*M15*1e15*Msun)**(2./3.))/kb).convert_to_units('K')
kbTfloor = ((UnitLength**2 / UnitTime**2).in_units('cm**2/s**2') * 0.62 * mp).in_units('keV') * Tigm
Tfloor = (kbTfloor/kb).in_units('K')
Rvir = 10**(-2./3.)*UnitLength


ts = yt.load("id0/galaxyhalo.*.vtk", parameters=
               {"length_unit":(UnitLength.value, UnitLength.units),  
                "time_unit":(UnitTime.value, UnitTime.units), 
                "mass_unit":(UnitMass.value, UnitMass.units)})

i = rank
while i < len(ts):
    ds = ts[i]
    sphere = ds.sphere([0.,0.,0.], (2.2*Rvir.value, "kpc"))
    profile = yt.create_profile(
        data_source=sphere,
        bin_fields=["radius"],
        fields=["density", "pressure", "temperature", "rv", "entropy", "tcool"],
        n_bins=60.,
        units=dict(radius="kpc",
                   rv="km/s"),
        logs=dict(radius=False),
        weight_field='cell_volume',
        extrema=dict(radius=(0,2.2*Rvir.value)),
        )
    plt.loglog(profile.x, profile['density'])
    plt.xlabel('R[kpc]')
    plt.ylabel('Density [g/cm**3]')
    plt.savefig('Density_'+str(i).zfill(4)+'.png')
    plt.clf()
    
    plt.plot(profile.x, profile['rv'])
    plt.xlabel('R[kpc]')
    plt.ylabel('rv [km/s]')
    plt.savefig('rv_'+str(i).zfill(4)+'.png')
    plt.clf()

    plt.plot(profile.x, profile['temperature'])
    plt.xlabel('R[kpc]')
    plt.ylabel('temperature [K]')
    plt.savefig('temperature_'+str(i).zfill(4)+'.png')
    plt.clf()

    plt.plot(profile.x, profile['tcool'])
    plt.xlabel('R[kpc]')
    plt.ylabel('tcool [Myr]')
    plt.savefig('tcool_'+str(i).zfill(4)+'.png')
    plt.clf()

    i+=size



### I want to redo all this in Object Oriented Programming
def make_prof_cut(Object, cut, fields):
    cr = ds.cut_region(Object, [cut])
    prof = yt.create_profile(
        data_source=cr,
        bin_fields=["radius"],
        fields=fields,
        n_bins=60.,
        units=dict(radius="kpc",
                   rv="km/s"),
        logs=dict(radius=False),
        weight_field='cell_volume',
        extrema=dict(radius=(0,2.2*Rvir.value)),
        )
    return prof

def plot_hot_cold(i, prof_cold, prof_hot, field, cut,CutName, log=False):
    cold_label=r'$T<'+cut+'$'
    hot_label =r'$T > '+cut+'$' #\geq
    plt.plot(prof_cold.x, prof_cold[field], label=cold_label)
    plt.plot(prof_hot.x, prof_hot[field], label=hot_label)
    field_unit = str(prof_hot[field].units).replace('**', '^').replace('*', '\,')
    plt.xlabel(r'$R\,[kpc]$')
    plt.ylabel(r'$\mathrm{'+field+'}\,['+field_unit+']$')
    if log == True:
        plt.xscale('log')
        plt.yscale('log')
    plt.axvline(Rvir, ls =':', color = 'k')
    plt.legend(loc='best')
    plt.savefig(CutName+'_'+field+'_'+str(i).zfill(4)+'.png')
    plt.clf()

    
    
Tvir_Cuts     = ("obj['temperature'] < "+str(1.*Tvir.value),"obj['temperature'] >= "+str(1.*Tvir.value))
HalfTvir_Cuts = ("obj['temperature'] < "+str(1.*Tvir.value/2.),"obj['temperature'] >= "+str(1.*Tvir.value/2.))
ConstT_Cuts   = ("obj['temperature'] < "+str(250000.0),"obj['temperature'] >= "+str(250000.0))

Cuts=dict(Tvir = Tvir_Cuts, HalfTvir=HalfTvir_Cuts, ConstT=ConstT_Cuts)
CutsLabel=dict(Tvir = 'T_{vir}', HalfTvir='T_{vir}/2.', ConstT='2.5 \mathrm{x} 10^{5} \, \mathrm{K}')

fields=["density", "pressure", "temperature", "rv", "entropy", "tcool"]
Logged=[True     , True      , True         , True, True     , True   ]

i = rank
while i < len(ts):
    ds = ts[i]
    sphere = ds.sphere([0.,0.,0.], (2.2*Rvir.value, "kpc"))
    
    for key in Cuts.keys():
        prof_cold = make_prof_cut(sphere, Cuts[key][0],fields)
        prof_hot  = make_prof_cut(sphere, Cuts[key][1],fields)
        for field,log in zip(fields, Logged):
            plot_hot_cold(i,prof_cold, prof_hot, field,CutsLabel[key], key, log=log)
    

    i+=size


