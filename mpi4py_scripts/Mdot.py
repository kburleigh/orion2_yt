import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import yt
from yt.units import *
from yt.utilities.physical_constants import *
import numpy as np
import numpy.linalg as LA
from mpi4py import MPI
import glob

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#yt.enable_parallelism()


def cooling_function(field,data):
    LAMBDA = np.zeros_like(kb*data['temperature'] * cm**3/s)
    kbT = (kb*data['temperature']).in_units('keV').value
    LAMBDA[np.where(kbT > 5.0)[0]] = 6.10540967286092 * (kbT[np.where(kbT > 5.0)[0]]/100.)**0.4
    LAMBDA[np.where((kbT<= 5.0)&(kbT > 0.7))[0]] = 1.842055928644565 *  (kbT[np.where((kbT<= 5.0)&(kbT > 0.7))[0]]/5.)**0.22
    LAMBDA[np.where((kbT<= 0.7)&(kbT > 0.13))[0]] = 1.1952286093343938 *  (kbT[np.where((kbT<= 0.7)&(kbT > 0.13))[0]]/0.7)**-0.5
    LAMBDA[np.where((kbT<= 0.13)&(kbT > 0.02))[0]] = 2.773500981126145 *  (kbT[np.where((kbT<= 0.13)&(kbT > 0.02))[0]]/0.13)**-1.7
    LAMBDA[np.where((kbT<= 0.02)&(kbT > 0.0017235))[0]] = 67.2 *  (kbT[np.where((kbT<= 0.02)&(kbT > 0.0017235))[0]]/0.02)**0.6
    LAMBDA[np.where((kbT<= 0.0017235)&(kbT > kbTfloor))[0]] = 15.44 *  (kbT[np.where((kbT<= 0.0017235)&(kbT > kbTfloor))[0]]/0.0017235)**6.0
    return LAMBDA*1e-23
yt.add_field(("gas", "cooling_function"), function=cooling_function, units='erg*cm**3/s', display_name=r"$\Lambda$")


def tcool(field,data):
    return 1.5*kb*data['temperature']/((data['density']/mp/0.62) * data['cooling_function'])
yt.add_field(("gas","tcool"),function=tcool,units="Myr", display_name=r"$\t_{cool}$")


def _my_radial_velocity(field, data):
    if data.has_field_parameter("bulk_velocity"):
        bv = data.get_field_parameter("bulk_velocity").in_units("cm/s")
    else:
        bv = data.ds.arr(np.zeros(3), "cm/s")
    xv = data["gas","velocity_x"] - bv[0]
    yv = data["gas","velocity_y"] - bv[1]
    zv = data["gas","velocity_z"] - bv[2]
    center = data.get_field_parameter('center')
    x_hat = data["x"] - center[0]
    y_hat = data["y"] - center[1]
    z_hat = data["z"] - center[2]
    r = np.sqrt(x_hat*x_hat+y_hat*y_hat+z_hat*z_hat)
    x_hat /= r
    y_hat /= r
    z_hat /= r
    return xv*x_hat + yv*y_hat + zv*z_hat
yt.add_field(("gas","rv"),
             function=_my_radial_velocity,
             units="cm/s",
             take_log=False,
             display_name=r"$\v_{r}$")


H0 = 70 * km /s / Mpc
mu = 0.62
athinput = open('athinput', 'r')
for line in athinput:
    if "Tigm" in line: Tigm = float(line.strip()[12:])
    if "M15" in line: M15 = float(line.strip()[12:])
athinput.close()

UnitLength = YTQuantity((G * M15 * 1e15 * Msun / H0**2)**(1./3.)).convert_to_units('kpc')
UnitTime = YTQuantity(1/H0).convert_to_units('yr')
UnitMass = YTQuantity(M15 * 1e15 * Msun).convert_to_units('Msun')
Tvir = ((0.5*mp*0.62*(10*H0*G*M15*1e15*Msun)**(2./3.))/kb).convert_to_units('K')
kbTfloor = ((UnitLength**2 / UnitTime**2).in_units('cm**2/s**2') * 0.62 * mp).in_units('keV') * Tigm
Tfloor = (kbTfloor/kb).in_units('K')
Rvir = 10**(-2./3.)*UnitLength


ts = yt.load("id0/galaxyhalo.*.vtk", parameters=
               {"length_unit":(UnitLength.value, UnitLength.units),  
                "time_unit":(UnitTime.value, UnitTime.units), 
                "mass_unit":(UnitMass.value, UnitMass.units)})

masses = {}
times = {}
radii = {}
i = rank
while i < len(ts[:]):
    ds = ts[i]
    sphere = ds.sphere([0.,0.,0.], (2.2*Rvir.value, "kpc"))
    profile = yt.create_profile(
        data_source=sphere,
        bin_fields=["radius"],
        fields=["cell_mass"],
        n_bins=60.,
        units=dict(radius="kpc",
                   cell_mass="Msun"),
        logs=dict(radius=False),
        weight_field=None,
        extrema=dict(radius=(0,2.2*Rvir.value)),
        accumulation=True
        )
    masses[i] = 1.*profile['cell_mass'].value
    times[i] = 1.*ds.current_time.in_units('yr').value
    radii[i] = 1.*profile.x.value
    i += size

masses = comm.gather(masses, root=0)
if rank == 0:
    all_masses = {}
    for d in masses:
        for k, v in d.iteritems():
            all_masses.setdefault(k, []).append(v)

radii = comm.gather(radii, root=0)
if rank == 0:
    all_radii = {}
    for d in radii:
        for k, v in d.iteritems():
            all_radii.setdefault(k, []).append(v)


times = comm.gather(times, root=0)
if rank == 0:
    all_times = {}
    for d in times:
        for k, v in d.iteritems():
            all_times.setdefault(k, []).append(v)
    TIME = np.array([])
    for key in all_times.keys():
        TIME = np.append(TIME,all_times[key])
    TIME = np.sort(TIME)
    Ms = np.zeros( (len(all_times.keys()), len(all_masses[0][0])) )
    print Ms.shape
    for key in all_masses.keys():
        print all_masses[key]
        Ms[int(key)] = all_masses[key][0]
    Ms = np.transpose(Ms)
    Mdots = np.array([np.diff(M)/np.diff(TIME) for M in Ms])
    for i in xrange(6):
        plt.plot(TIME[1:], Mdots[10*i], label=str(all_radii[0][0][10*i]))
    plt.legend(loc="upper left")
    plt.savefig('Mdots.png')
    plt.clf()








    # # I can use this to make all the 1d profiles I want
    # profile = yt.create_profile(
    #     data_source=sphere,
    #     bin_fields=["radius"],
    #     fields=["density", "pressure", "temperature", "rv", "entropy"],#, "tcool"],
    #     n_bins=60.,
    #     units=dict(radius="kpc",
    #                rv="km/s"),
    #     logs=dict(radius=False),
    #     weight_field='cell_volume',
    #     extrema=dict(radius=(0,2.2*Rvir.value)),
    #     )











