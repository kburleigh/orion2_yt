import numpy as np
import numpy.linalg as LA
from mpi4py import MPI
import glob

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

data = {}
i = rank
while i < 5:
	data[i] = np.arange(i,7*i)
	i+=size

data = comm.gather(data, root=0)
if rank == 0:
	super_dict = {}
	for d in data:
		for k, v in d.iteritems():
			super_dict.setdefault(k, []).append(v)
	for key in super_dict.keys():
		print key, super_dict[key]
