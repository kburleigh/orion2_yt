import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-cs",action="store",help='sound speed in cm/s')
parser.add_argument("-M0i",action="store",help='Mach number of turbulence when insert sinks')
parser.add_argument("-tBH",action="store",help='bondi hoyle time when insert sinks')
args = parser.parse_args()
if args.tBH and args.cs and args.M0i: 
    cs= float(args.cs)
    M0i= float(args.M0i)
    tBH = float(args.tBH)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import glob
from pickle import dump as pdump
import my_fund_const as fc
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def CurlCellDiff(pf,vx,vy,vz):
    #FIRST PART
    class CellFaces():
        class Vel():
            def __init__(self,vx,vy,vz):
                dim= np.array(vx.shape)+1  #vx,vy,vz all same size
                self.Vx= np.zeros(dim)
                self.Vy= self.Vx.copy()
                self.Vz= self.Vx.copy()
        def __init__(self,vx,vy,vz):
            '''vx,vy,vz are from vx=covering_grid['X-momentum']/covering_grid['density'], etc
            LR - Left/Right faces (x dir)
            FB - Front/Back faces (y dir)
            BT - Bottom/Top faces (z dir)
            '''
            self.LR= self.Vel(vx,vy,vz)
            self.TB= self.Vel(vx,vy,vz)
            self.FB= self.Vel(vx,vy,vz)

    Face= CellFaces(vx,vy,vz)
    first=0
    last=vx.shape[0]-1
    #iterate Left/Right faces first (eg. x direction)
    for i in range(vx.shape[0]):
        ind=i-1
        if i==first: ind=last
        Face.LR.Vx[i,:-1,:-1]= (vx[ind,:,:]+vx[i,:,:])/2  #:-1 keep dimensions same
        Face.LR.Vy[i,:-1,:-1]= (vy[ind,:,:]+vy[i,:,:])/2
        Face.LR.Vz[i,:-1,:-1]= (vz[ind,:,:]+vz[i,:,:])/2
    #fill that last index of face in x dir, it is the same as the first Face value
    Face.LR.Vx[-1,:,:]= Face.LR.Vx[0,:,:] #dim match so no -1
    Face.LR.Vy[-1,:,:]= Face.LR.Vy[0,:,:] 
    Face.LR.Vz[-1,:,:]= Face.LR.Vz[0,:,:]
    #iterate Front/Back faces (eg. y direction)
    for j in range(vx.shape[0]):
        ind=j-1
        if j==first: ind=last
        Face.FB.Vx[:-1,j,:-1]= (vx[:,ind,:]+vx[:,j,:])/2
        Face.FB.Vy[:-1,j,:-1]= (vy[:,ind,:]+vy[:,j,:])/2
        Face.FB.Vz[:-1,j,:-1]= (vz[:,ind,:]+vz[:,j,:])/2
    Face.FB.Vx[:,-1,:]= Face.FB.Vx[:,0,:]  
    Face.FB.Vy[:,-1,:]= Face.FB.Vy[:,0,:]
    Face.FB.Vz[:,-1,:]= Face.FB.Vz[:,0,:]
    #iterate Top/Bottom faces (eg. z direction)
    for k in range(vx.shape[0]):
        ind=k-1
        if k==first: ind=last
        Face.TB.Vx[:-1,:-1,k]= (vx[:,:,ind]+vx[:,:,k])/2
        Face.TB.Vy[:-1,:-1,k]= (vy[:,:,ind]+vy[:,:,k])/2
        Face.TB.Vz[:-1,:-1,k]= (vz[:,:,ind]+vz[:,:,k])/2
    Face.TB.Vx[:,:,-1]= Face.TB.Vx[:,:,0] 
    Face.TB.Vy[:,:,-1]= Face.TB.Vy[:,:,0]
    Face.TB.Vz[:,:,-1]= Face.TB.Vz[:,:,0] 
    #3 faces (1/2 of cell in corner [-1,-1,-1]) have not been filled, do so now
    Face.LR.Vx[-1,-1,-1]= Face.LR.Vx[0,-1,-1]
    Face.LR.Vy[-1,-1,-1]= Face.LR.Vy[0,-1,-1]
    Face.LR.Vz[-1,-1,-1]= Face.LR.Vz[0,-1,-1]
    #
    Face.FB.Vx[-1,-1,-1]= Face.FB.Vx[-1,0,-1]
    Face.FB.Vy[-1,-1,-1]= Face.FB.Vy[-1,0,-1]
    Face.FB.Vz[-1,-1,-1]= Face.FB.Vz[-1,0,-1]
    #
    Face.TB.Vx[-1,-1,-1]= Face.TB.Vx[-1,-1,0]
    Face.TB.Vy[-1,-1,-1]= Face.TB.Vy[-1,-1,0]
    Face.TB.Vz[-1,-1,-1]= Face.TB.Vz[-1,-1,0]
    #SECOND PART
    class Gradients():
        def __init__(self,vx):
            dim= np.array(vx.shape)
            #grad in x dir
            self.dVy_dx= np.zeros(dim)
            self.dVz_dx= self.dVy_dx.copy()
            #grad in y dir
            self.dVx_dy= self.dVy_dx.copy()
            self.dVz_dy= self.dVy_dx.copy()
            #grad in z dir
            self.dVx_dz= self.dVy_dx.copy()
            self.dVy_dz= self.dVy_dx.copy()
    Grad= Gradients(vx)
    #grad in x dir
    for i in range(vx.shape[0]):
        Grad.dVy_dx[i,:,:]= Face.LR.Vy[i+1,:-1,:-1]-Face.LR.Vy[i,:-1,:-1] #Grad arrays have Sim data dims
        Grad.dVz_dx[i,:,:]= Face.LR.Vz[i+1,:-1,:-1]-Face.LR.Vz[i,:-1,:-1]
    #grad in y dir
    for j in range(vx.shape[0]):
        Grad.dVx_dy[:,j,:]= Face.FB.Vx[:-1,j+1,:-1]-Face.FB.Vx[:-1,j,:-1]
        Grad.dVz_dy[:,j,:]= Face.FB.Vz[:-1,j+1,:-1]-Face.FB.Vz[:-1,j,:-1]
    #grad in z dir
    for k in range(vx.shape[0]):
        Grad.dVx_dz[:,:,k]= Face.TB.Vx[:-1,:-1,k+1]-Face.TB.Vx[:-1,:-1,k]
        Grad.dVy_dz[:,:,k]= Face.TB.Vy[:-1,:-1,k+1]-Face.TB.Vy[:-1,:-1,k]
    #don't forget to DIVIDE BY CELL WIDTH!
    lev=0
    wid_cell= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**lev
    Grad.dVy_dx /= wid_cell
    Grad.dVz_dx /= wid_cell
    #
    Grad.dVx_dy /= wid_cell
    Grad.dVz_dy /= wid_cell
    #
    Grad.dVx_dz /= wid_cell
    Grad.dVy_dz /= wid_cell
    #THIRD PART
    class CurlVector():
        def __init__(self,Grad):
            self.xhat= Grad.dVz_dy- Grad.dVy_dz
            self.yhat= -(Grad.dVz_dx- Grad.dVx_dz)
            self.zhat= Grad.dVy_dx- Grad.dVx_dy
            self.mag= np.sqrt(self.xhat**2+self.yhat**2+self.zhat**2)
    Curl= CurlVector(Grad)
    return Curl.mag

class TimeEvol():
    def __init__(self,f_hdf5):
        self.t= np.zeros(len(f_hdf5))
        self.Mdot_turb_div_0_mean= self.t.copy()
        self.Mdot_turb_div_0_med= self.t.copy()

def MdotTurb_for_rB_M0(log_rB_L,M0i,cs,  pf,rho,vx,vy,vz,CurlMag):
    rB= 10**log_rB_L*pf.domain_width[0]
    Msink= rB*cs**2/fc.G
    Mdot_0= 4.*np.pi*rho.mean()*(fc.G*Msink)**2/(M0i*cs)**3
    Mach_rms= np.array(np.sqrt(vx**2+vy**2+vz**2)/cs)
    #print 'type,shape',type(Mach_rms),Mach_rms.shape
    Mdot_bh=4*np.pi*rho*(fc.G*Msink)**2*np.sqrt((1.1+Mach_rms**2)/(1+Mach_rms**2)**4)/cs**3
    #a1= 4*np.pi*rho*(fc.G*Msink)**2/cs**3
    #a2=Mach_rms**2
    #a2= (1+a2)**4
    #a3= (1.1+Mach_rms**2)
    #mdot_bh= a1*(a3/a2)**0.5
    w_star= np.array(CurlMag.mean()*rB/cs)
    if w_star > 5.e3: raise ValueError 
    Mdot_w= 4.*np.pi*rho*(fc.G*Msink)**2*0.34/cs**3/(1+w_star**0.9)
    Mdot_turb= (Mdot_bh**-2 + Mdot_w**-2)**-0.5
    return Mdot_turb/Mdot_0

def plot_tevol(Tevol,tBH,log_rB_L,M0i): #,fsave):
    fig,ax= plt.subplots(figsize=(5,5))
    time= (Tevol.t - Tevol.t[0])/tBH
    ax.plot(time,Tevol.Mdot_turb_div_0_mean,"k-",label='Mean')
    ax.plot(time,Tevol.Mdot_turb_div_0_med,"b-",label='Median')
#     ax.hlines(0.39,time.min(),time.max(),\
#               colors="r",linestyle="dashed",label='\phi_{med} K06')
#     ax.hlines(1.7,time.min(),time.max(),\
#               colors="r",linestyle="dashed",label='\phi_{mean} K06')
    ax.set_xlabel(r"$\mathbf{t/t_{BH}}$")
    ax.set_ylabel(r"$\mathbf{\dot{M}_{turb}/\dot{M}_0}$")
    ax.set_title('Log_rB_div_L= %.1f, M0i = %0.1f' % (log_rB_L,M0i))
    leg_box=ax.legend(loc=0)
    ax.set_yscale('log')
#     if ylim: ax.set_ylim([ylim[0],ylim[1]])
    plt.savefig(fsave,dpi=150) #bbox_extra_artists=(leg_box,), bbox_inches='tight')

log_rB_L= np.array([-5.,-3.,-1.,1.])
log_rB_L= -5.

spath="./plots/"
f_hdf5= glob.glob("./data.003*.3d.hdf5")
f_hdf5.sort()

times = {}
phi_mean= {}
phi_median= {}
i = rank
while i < len(f_hdf5):
    pf=load(f_hdf5[i])
    print 'processor %d, loading file: %s' % (i,f_hdf5[i])
    t[i]= pf.current_time
    lev=0
    cgrid= pf.h.covering_grid(level=lev,left_edge=pf.h.domain_left_edge,
                              dims=pf.domain_dimensions) 
    print 'extracting rho,vx,... from covering grid'
    rho=cgrid['density']
    vx=cgrid['X-momentum']/rho
    vy=cgrid['Y-momentum']/rho
    vz=cgrid['Z-momentum']/rho
    print 'calc curl'
    CurlMag= CurlCellDiff(pf,vx,vy,vz)
    print 'calc mdots'
    Mdot_turb_div_0= MdotTurb_for_rB_M0(log_rB_L,M0i,cs,  pf,rho,vx,vy,vz,CurlMag)
    phi_mean[i]= Mdot_turb_div_0.mean()
    phi_median[i]= np.median(Mdot_turb_div_0)
    i += size

times = comm.gather(times, root=0)
if rank == 0:
    all_times = {}
    for d in times:
        for k, v in d.iteritems():
            all_times.setdefault(k, []).append(v)

phi_mean = comm.gather(phi_mean, root=0)
if rank == 0:
    all_phi_mean = {}
    for d in phi_mean:
        for k, v in d.iteritems():
            all_phi_mean.setdefault(k, []).append(v)

phi_median = comm.gather(phi_median, root=0)
if rank == 0:
    all_phi_median = {}
    for d in phi_median:
        for k, v in d.iteritems():
            all_phi_median.setdefault(k, []).append(v)

#save data
if rank == 0:
    name=spath+ "K06_Table1_logrBL_%.1f_M0_%.1f.pickle" % (log_rB_L,M0i)
    fout=open(name,"w")
    pdump((times,phi_mean,phi_median),fout)
    fout.close()
    print "gathered and pickle dumped data"
