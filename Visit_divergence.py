#Visit_vel_cli.py -- run from cli to get width window, size vel arrows, etc 
#Visit_vel.py -- run as job to visualize all  hdf5 with window, vel arrows above 
#visit -cli -nowin -s Visit_vel.py
#edison, hopper  **np is # cores
OpenComputeEngine("localhost", ("-l", "qsub/aprun", "-np", "24", "-p", "debug", "-t", "00:30:00"))
#carver  **np is # NODES!
#OpenComputeEngine("localhost", ("-l", "qsub/mpirun", "-np", "4", "-p", "debug", "-t", "00:30:00"))

####
fdir="/global/scratch2/sd/kaylanb/stampede/Burleighetal/insertSinks/base256/Lbox2.0/drive/seed2/beta_1/rBl_0.203/ntagFix/lev5"
save_dir=fdir+"/visit/abs_divergence"
save_name='zslice_'
#
n_hdf5=73 
slice_dir= 'z'
(x,y,z)= (0.75,0.25,-0.75)
#
sinklevel=5
lbox=2.
base=256.
smrWide=48.
dx=lbox/base/2**(sinklevel-2)
left= (smrWide/2.)*dx
#
cmin=5
cmax=1e4
#
vel_n= 2000
vel_length= 0.015
vel_head= 0.5
vel_color_tup= (255,255,255,255) #change first 3, 255^3 = white

SetWindowLayout(1)
OpenDatabase("localhost:"+fdir+"/data.*.3d.hdf5 database", 0)
DeleteAllPlots()
#define scalars, vectors
DefineScalarExpression("vmag", "sqrt((<X-momentum>^2+<Y-momentum>^2+<Z-momentum>^2)/density^2)")
DefineVectorExpression("velocity", "momentum/density")
DefineScalarExpression("abs_diverg", "abs(divergence(velocity))")

#exact output from commands cli
SetActiveWindow(1)
AddPlot("Pseudocolor", "abs_diverg", 1, 1)
AddOperator("Slice", 1)
SetActivePlots(0)
SliceAtts = SliceAttributes()
SliceAtts.originType = SliceAtts.Point  # Point, Intercept, Percent, Zone, Node
SliceAtts.originPoint = (x, y, z)
SliceAtts.originIntercept = 0
SliceAtts.originPercent = 0
SliceAtts.originZone = 0
SliceAtts.originNode = 0
if slice_dir == 'z':
    SliceAtts.normal = (0, 0, 1)
    SliceAtts.axisType = SliceAtts.ZAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
    SliceAtts.upAxis = (0, 1, 0)
    SliceAtts.project2d = 1
    SliceAtts.interactive = 1
    SliceAtts.flip = 0
    SliceAtts.originZoneDomain = 0
    SliceAtts.originNodeDomain = 0
    SliceAtts.meshName = "Mesh"
    SliceAtts.theta = 0
    SliceAtts.phi = 90
elif slice_dir == 'y':
    SliceAtts.normal = (0, -1, 0)
    SliceAtts.axisType = SliceAtts.YAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
    SliceAtts.upAxis = (0, 0, 1)
    SliceAtts.project2d = 1
    SliceAtts.interactive = 1
    SliceAtts.flip = 0
    SliceAtts.originZoneDomain = 0
    SliceAtts.originNodeDomain = 0
    SliceAtts.meshName = "Mesh"
    SliceAtts.theta = 0
    SliceAtts.phi = 0
elif slice_dir == 'x':
    SliceAtts.normal = (-1, 0, 0)
    SliceAtts.axisType = SliceAtts.XAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
    SliceAtts.upAxis = (0, 1, 0)
    SliceAtts.project2d = 1
    SliceAtts.interactive = 1
    SliceAtts.flip = 0
    SliceAtts.originZoneDomain = 0
    SliceAtts.originNodeDomain = 0
    SliceAtts.meshName = "Mesh"
    SliceAtts.theta = 90
else: raise ValueError
SetOperatorOptions(SliceAtts, 1)
PseudocolorAtts = PseudocolorAttributes()
PseudocolorAtts.scaling = PseudocolorAtts.Log  # Linear, Log, Skew
PseudocolorAtts.skewFactor = 1
PseudocolorAtts.limitsMode = PseudocolorAtts.CurrentPlot  # OriginalData, CurrentPlot
PseudocolorAtts.minFlag = 1
PseudocolorAtts.min = cmin
PseudocolorAtts.maxFlag = 1
PseudocolorAtts.max = cmax
PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
PseudocolorAtts.colorTableName = "hot"
PseudocolorAtts.invertColorTable = 0
PseudocolorAtts.opacityType = PseudocolorAtts.FullyOpaque  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
PseudocolorAtts.opacityVariable = ""
PseudocolorAtts.opacity = 1
PseudocolorAtts.opacityVarMin = 0
PseudocolorAtts.opacityVarMax = 1
PseudocolorAtts.opacityVarMinFlag = 0
PseudocolorAtts.opacityVarMaxFlag = 0
PseudocolorAtts.pointSize = 0.05
PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
PseudocolorAtts.pointSizeVarEnabled = 0
PseudocolorAtts.pointSizeVar = "default"
PseudocolorAtts.pointSizePixels = 2
PseudocolorAtts.lineType = PseudocolorAtts.Line  # Line, Tube, Ribbon
PseudocolorAtts.lineStyle = PseudocolorAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
PseudocolorAtts.lineWidth = 0
PseudocolorAtts.tubeDisplayDensity = 10
PseudocolorAtts.tubeRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
PseudocolorAtts.tubeRadiusAbsolute = 0.125
PseudocolorAtts.tubeRadiusBBox = 0.005
PseudocolorAtts.varyTubeRadius = 0
PseudocolorAtts.varyTubeRadiusVariable = ""
PseudocolorAtts.varyTubeRadiusFactor = 10
PseudocolorAtts.endPointType = PseudocolorAtts.None  # None, Tails, Heads, Both
PseudocolorAtts.endPointStyle = PseudocolorAtts.Spheres  # Spheres, Cones
PseudocolorAtts.endPointRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
PseudocolorAtts.endPointRadiusAbsolute = 1
PseudocolorAtts.endPointRadiusBBox = 0.005
PseudocolorAtts.endPointRatio = 2
PseudocolorAtts.renderSurfaces = 1
PseudocolorAtts.renderWireframe = 0
PseudocolorAtts.renderPoints = 0
PseudocolorAtts.smoothingLevel = 0
PseudocolorAtts.legendFlag = 1
PseudocolorAtts.lightingFlag = 1
SetPlotOptions(PseudocolorAtts)
# Begin spontaneous state
#View2DAtts = View2DAttributes()
#View2DAtts.windowCoords = (-left, left, -left, left)
#View2DAtts.viewportCoords = (0.2, 0.95, 0.2, 0.95)
#View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
#View2DAtts.fullFrameAutoThreshold = 100
#View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
#View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
#View2DAtts.windowValid = 1
#SetView2D(View2DAtts)
# End spontaneous state
##########
AddPlot("Subset", "levels", 1, 1)
SetActivePlots(1)
SubsetAtts = SubsetAttributes()
SubsetAtts.colorType = SubsetAtts.ColorByMultipleColors  # ColorBySingleColor, ColorByMultipleColors, ColorByColorTable
SubsetAtts.colorTableName = "Default"
SubsetAtts.invertColorTable = 0
SubsetAtts.filledFlag = 1
SubsetAtts.legendFlag = 1
SubsetAtts.lineStyle = SubsetAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
SubsetAtts.lineWidth = 2
SubsetAtts.singleColor = (0, 0, 0, 255)
SubsetAtts.SetMultiColor(0, (0, 0, 0, 142))
SubsetAtts.SetMultiColor(1, (0, 0, 0, 0))
SubsetAtts.SetMultiColor(2, (0, 0, 255, 0))
SubsetAtts.SetMultiColor(3, (0, 255, 255, 0))
SubsetAtts.SetMultiColor(4, (255, 0, 255, 0))
SubsetAtts.SetMultiColor(5, (0, 0, 0, 255))
SubsetAtts.subsetNames = ("0", "1", "2", "3", "4", "5")
SubsetAtts.subsetType = SubsetAtts.Group  # Domain, Group, Material, EnumScalar, Mesh, Unknown
SubsetAtts.opacity = 1
SubsetAtts.wireframe = 1
SubsetAtts.drawInternal = 0
SubsetAtts.smoothingLevel = 0
SubsetAtts.pointSize = 0.05
SubsetAtts.pointType = SubsetAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
SubsetAtts.pointSizeVarEnabled = 0
SubsetAtts.pointSizeVar = "default"
SubsetAtts.pointSizePixels = 2
SetPlotOptions(SubsetAtts)
###############
AddPlot("Vector", "velocity", 1, 1)
SetActivePlots(2)
VectorAtts = VectorAttributes()
VectorAtts.glyphLocation = VectorAtts.UniformInSpace  # AdaptsToMeshResolution, UniformInSpace
VectorAtts.useStride = 0
VectorAtts.stride = 1
VectorAtts.nVectors = vel_n
VectorAtts.lineStyle = VectorAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
VectorAtts.lineWidth = 1
VectorAtts.scale = vel_length
VectorAtts.scaleByMagnitude = 1
VectorAtts.autoScale = 0
VectorAtts.headSize = vel_head
VectorAtts.headOn = 1
VectorAtts.colorByMag = 0
VectorAtts.useLegend = 1
VectorAtts.vectorColor = vel_color_tup
VectorAtts.colorTableName = "gray"
VectorAtts.invertColorTable = 1
VectorAtts.vectorOrigin = VectorAtts.Tail  # Head, Middle, Tail
VectorAtts.minFlag = 1
VectorAtts.maxFlag = 1
VectorAtts.limitsMode = VectorAtts.CurrentPlot  # OriginalData, CurrentPlot
VectorAtts.min = 1
VectorAtts.max = 10
VectorAtts.lineStem = VectorAtts.Line  # Cylinder, Line
VectorAtts.geometryQuality = VectorAtts.Fast  # Fast, High
VectorAtts.stemWidth = 0.08
VectorAtts.origOnly = 1
VectorAtts.glyphType = VectorAtts.Arrow  # Arrow, Ellipsoid
SetPlotOptions(VectorAtts)
DrawPlots()



SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.outputToCurrentDirectory = 0
SaveWindowAtts.outputDirectory = save_dir
SaveWindowAtts.fileName = save_name
SaveWindowAtts.family = 1
SaveWindowAtts.format = SaveWindowAtts.PNG  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1536
SaveWindowAtts.height = 1536
SaveWindowAtts.screenCapture = 0
SaveWindowAtts.saveTiled = 0
SaveWindowAtts.quality = 100
SaveWindowAtts.progressive = 0
SaveWindowAtts.binary = 0
SaveWindowAtts.stereo = 0
SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
SaveWindowAtts.forceMerge = 0
SaveWindowAtts.resConstraint = SaveWindowAtts.NoConstraint  # NoConstraint, EqualWidthHeight, ScreenProportions
SaveWindowAtts.advancedMultiWindowSave = 0
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()

for i in range(1,n_hdf5):
    SetTimeSliderState(i)
    SaveWindowAtts = SaveWindowAttributes()
    SaveWindowAtts.outputToCurrentDirectory = 0
    SaveWindowAtts.outputDirectory = save_dir
    SaveWindowAtts.fileName = save_name
    SaveWindowAtts.family = 1
    SaveWindowAtts.format = SaveWindowAtts.PNG  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
    SaveWindowAtts.width = 1536
    SaveWindowAtts.height = 1536
    SaveWindowAtts.screenCapture = 0
    SaveWindowAtts.saveTiled = 0
    SaveWindowAtts.quality = 100
    SaveWindowAtts.progressive = 0
    SaveWindowAtts.binary = 0
    SaveWindowAtts.stereo = 0
    SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
    SaveWindowAtts.forceMerge = 0
    SaveWindowAtts.resConstraint = SaveWindowAtts.NoConstraint  # NoConstraint, EqualWidthHeight, ScreenProportions
    SaveWindowAtts.advancedMultiWindowSave = 0
    SetSaveWindowAttributes(SaveWindowAtts)
    SaveWindow()
