'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and plot vs. time'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-MaxLevel",action="store",help='max level of refinement')
args = parser.parse_args()
if args.MaxLevel: MaxLevel = int(args.MaxLevel)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob
from pickle import dump as pdump

def plot_TstepVsIndex(tstep,MaxLevel,fsave):
    fig,axis=plt.subplots(3,2,figsize=(10,8),sharey=True)
    ax=axis.flatten()
    for i in range(MaxLevel+1):
        ax[i].plot(range(len(tstep[str(i)])),np.log10(tstep[str(i)]))
        ax[i].set_title('level '+str(i))
        ax[i].set_ylabel('tstep')
        if i==4 or i==5: ax[i].set_xlabel('ith tstep')   
    fig.subplots_adjust(hspace=0.4)
    plt.savefig(fsave,dpi=150)

#MAIN
spath="./plots/"
path='./plots/'

t= {}
tstep={}
for i in range(MaxLevel+1):
    fname= path+'tsteps_lev'+str(i)+'.txt'
    t[str(i)]= np.loadtxt(fname,dtype=np.float64,usecols=(5,),unpack=True)
    tstep[str(i)]= t[str(i)][1:]-t[str(i)][:-1]
fsave=spath+"tsteps_from_pout.png"
plot_TstepVsIndex(tstep,MaxLevel,fsave)
print "finished"
