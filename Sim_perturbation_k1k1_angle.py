import h5py
import numpy as np
import numpy.fft
from math import *
from optparse import OptionParser
import sys
import matplotlib.pyplot as plt

def init_perturbations(n, kmin, kmax, dtype):
    kx = np.zeros(n, dtype=dtype)
    ky = np.zeros(n, dtype=dtype)
    kz = np.zeros(n, dtype=dtype)
    # perform fft k-ordering convention shifts
    for j in range(0,n[1]):
        for k in range(0,n[2]):
            kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
    for i in range(0,n[0]):
        for k in range(0,n[2]):
            ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
    for i in range(0,n[0]):
        for j in range(0,n[1]):
            kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
            
    kx = np.array(kx, dtype=dtype)
    ky = np.array(ky, dtype=dtype)
    kz = np.array(kz, dtype=dtype)
    k = np.sqrt(np.array(kx**2+ky**2+kz**2, dtype=dtype))

    # only use the positive frequencies
    inds = np.where(np.logical_and(k**2 >= kmin**2, k**2 < (kmax+1)**2))
    nr = len(inds[0])

    phasex = np.zeros(n, dtype=dtype)
    phasex[inds] = 2.*pi*np.random.uniform(size=nr)
    fx = np.zeros(n, dtype=dtype)
    fx[inds] = np.random.normal(size=nr)
    
    phasey = np.zeros(n, dtype=dtype)
    phasey[inds] = 2.*pi*np.random.uniform(size=nr)
    fy = np.zeros(n, dtype=dtype)
    fy[inds] = np.random.normal(size=nr)
    
    phasez = np.zeros(n, dtype=dtype)
    phasez[inds] = 2.*pi*np.random.uniform(size=nr)
    fz = np.zeros(n, dtype=dtype)
    fz[inds] = np.random.normal(size=nr)

    # rescale perturbation amplitude so that low number statistics
    # at low k do not throw off the desired power law scaling.
    for i in range(kmin, kmax+1):
        slice_inds = np.where(np.logical_and(k >= i, k < i+1))
        rescale = sqrt(np.sum(np.abs(fx[slice_inds])**2 + np.abs(fy[slice_inds])**2 + np.abs(fz[slice_inds])**2))
        fx[slice_inds] = fx[slice_inds]/rescale
        fy[slice_inds] = fy[slice_inds]/rescale
        fz[slice_inds] = fz[slice_inds]/rescale

    # set the power law behavior
    # wave number bins
    fx[inds] = fx[inds]*k[inds]**-(0.5*alpha)
    fy[inds] = fy[inds]*k[inds]**-(0.5*alpha)
    fz[inds] = fz[inds]*k[inds]**-(0.5*alpha)

    # add in phases
    fx = np.cos(phasex)*fx + 1j*np.sin(phasex)*fx
    fy = np.cos(phasey)*fy + 1j*np.sin(phasey)*fy
    fz = np.cos(phasez)*fz + 1j*np.sin(phasez)*fz

    return fx, fy, fz, kx, ky, kz


def normalize(fx, fy, fz):
    norm = np.sqrt(np.sum(fx**2 + fy**2 + fz**2)/np.product(n))
    fx = fx/norm
    fy = fy/norm
    fz = fz/norm
    return fx, fy, fz


def make_perturbations(n, kmin, kmax, f_solenoidal):
    fx, fy, fz, kx, ky, kz = init_perturbations(n, kmin, kmax, dtype)
    if f_solenoidal != None:
        k2 = kx**2+ky**2+kz**2
        # solenoidal part
        fxs = 0.; fys =0.; fzs = 0.
        if f_solenoidal != 0.0:
            fxs = np.real(fx - kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16))
            fys = np.real(fy - ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16))
            fzs = np.real(fz - kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16))
            ind = np.where(k2 == 0)
            fxs[ind] = 0.; fys[ind] = 0.; fzs[ind] = 0.
            # need to normalize this before applying relative weighting of solenoidal / compressive components
            norm = np.sqrt(np.sum(fxs**2+fys**2+fzs**2))
            fxs = fxs/norm
            fys = fys/norm
            fzs = fzs/norm
        # compressive part
        # get a different random cube for the compressive part
        # so that we can target the RMS solenoidal fraction,
        # instead of setting a constant solenoidal fraction everywhere.
        fx, fy, fz, kx, ky, kz = init_perturbations(n, kmin, kmax, dtype)
        fxc = 0.; fyc =0.; fzc = 0.
        if f_solenoidal != 1.0:
            fxc = np.real(kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16))
            fyc = np.real(ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16))
            fzc = np.real(kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16))
            ind = np.where(k2 == 0)
            fxc[ind] = 0.; fyc[ind] = 0.; fzc[ind] = 0.
            # need to normalize this before applying relative weighting of solenoidal / compressive components
            norm = np.sqrt(np.sum(fxc**2+fyc**2+fzc**2))
            fxc = fxc/norm
            fyc = fyc/norm
            fzc = fzc/norm
        # back to real space
        pertx = np.real(np.fft.ifftn(f_solenoidal*fxs + (1.-f_solenoidal)*fxc))
        perty = np.real(np.fft.ifftn(f_solenoidal*fys + (1.-f_solenoidal)*fyc))
        pertz = np.real(np.fft.ifftn(f_solenoidal*fzs + (1.-f_solenoidal)*fzc))
    else:
        # just convert to real space
        pertx = np.real(np.fft.ifftn(fx))
        perty = np.real(np.fft.ifftn(fy))
        pertz = np.real(np.fft.ifftn(fz))
    
    # subtract off COM (assuming uniform density)
    pertx = pertx-np.average(pertx)
    perty = perty-np.average(perty)
    pertz = pertz-np.average(pertz)
    # scale RMS of perturbation cube to unity
    pertx, perty, pertz = normalize(pertx, perty, pertz)
    return pertx, perty, pertz, kx,ky,kz


def get_erot_ke_ratio(pertx, perty, pertz):
    x, y, z = np.mgrid[0:n[0], 0:n[1], 0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    r2 = x**2+y**2+z**2
    erot_ke_ratio = (np.sum(y*pertz-z*perty)**2 +
                     np.sum(z*pertx-x*pertz)**2 +
                     np.sum(x*perty-y*pertx)**2)/(np.sum(r2)*np.product(n)) 
    return erot_ke_ratio


def plot_spectrum1D(pertx, perty, pertz):
    # plot the 1D power to check the scaling.
    fx = np.abs(np.fft.fftn(pertx))
    fy = np.abs(np.fft.fftn(perty))
    fz = np.abs(np.fft.fftn(pertz))
    fx = np.abs(fx)
    fy = np.abs(fy)
    fz = np.abs(fz)
    kx = np.zeros(n, dtype=dtype)
    ky = np.zeros(n, dtype=dtype)
    kz = np.zeros(n, dtype=dtype)
    # perform fft k-ordering convention shifts
    for j in range(0,n[1]):
        for k in range(0,n[2]):
            kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
    for i in range(0,n[0]):
        for k in range(0,n[2]):
            ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
    for i in range(0,n[0]):
        for j in range(0,n[1]):
            kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
    k = np.sqrt(np.array(kx**2+ky**2+kz**2,dtype=dtype))
    k1d = []
    power = []
    for i in range(kmin,kmax+1):
        slice_inds = np.where(np.logical_and(k >= i, k < i+1))
        k1d.append(i+0.5)
        power.append(np.sum(fx[slice_inds]**2 + fy[slice_inds]**2 + fz[slice_inds]**2))
        print i,power[-1]
    import matplotlib.pyplot as plt
    plt.loglog(k1d, power)
    plt.show()


###################
# input parameters, read from command line
###################
parser = OptionParser()
parser.add_option('--kmin', dest='kmin', 
                  help='minimum wavenumber.', 
                  default=-1)
parser.add_option('--kmax', dest='kmax', 
                  help='maximum wavenumber.', 
                  default=-1)
parser.add_option('--size', dest='size', 
                  help='size of each direction of data cube.  default=256', 
                  default=256)
parser.add_option('--alpha', dest='alpha', 
                  help='negative of power law slope.  (Power ~ k^-alpha) '+
                  'supersonic turbulence is near alpha=2.  '+
                  'driving over a narrow band of two modes is often done with alpha=0', 
                  default = None)
parser.add_option('--seed', dest='seed', 
                  help='seed for random # generation.  default=0', 
                  default = 0)
parser.add_option('--f_solenoidal', dest='f_solenoidal', 
                  help='volume RMS fraction of solenoidal componet of the perturbations relative to the total.  ' + 
                       'If --f_solenoidal=None, the motions are purely random.  For low wave numbers ' +
                       'the relative imporance of solenoidal to compressive may be sensitve to the ' +
                       'choice of radom seed.  It has been suggested (Federrath 2008) that ' +
                       'f_solenoidal=2/3 is the most natural driving mode and this is currently' + 
                       'the suggested best-practice.', 
                  default = -1)

(options, args) = parser.parse_args()

# size of the data domain
n = [int(options.size), int(options.size), int(options.size)]
# range of perturbation length scale in units of the smallest side of the domain
kmin = int(options.kmin)
kmax = int(options.kmax)
if kmin > kmax or kmin < 0 or kmax < 0:
    print "kmin must be < kmax, with kmin > 0, kmax > 0.  See --help."
    sys.exit(0)
if kmax > floor(np.min(n))/2:
    print "kmax must be <= floor(size/2).  See --help."
    sys.exit(0)
f_solenoidal = options.f_solenoidal
if f_solenoidal == "None" or f_solenoidal == "none":
    f_solenoidal = None
else:
    f_solenoidal = float(options.f_solenoidal)
    if f_solenoidal > 1. or f_solenoidal < 0.:
        print "You must choose f_solenoidal.  See --help."
        sys.exit(0)
alpha = options.alpha
if alpha==None:
    print "You must choose a power law slope, alpha.  See --help."
    sys.exit(0)
alpha = float(options.alpha)
if alpha < 0.:
    print "alpha is less than zero. Thats probably not what ou want.  See --help."
    sys.exit(0)
seed = int(options.seed)
# data precision
dtype = np.float64
# ratio of solenoidal to compressive components
if options.f_solenoidal=="None" or options.f_solenoidal==None:
    f_solenoidal = None
else:
    f_solenoidal = min(max(float(options.f_solenoidal), 0.), 1.)

###################
# begin computation
###################
class MyObj():
    pass
s1= MyObj()
s2= MyObj()
s100= MyObj()
#for labeling plots
ylab=['seed 10','seed 20','seed 30']
#s1
np.random.seed(seed=10)
s1.pertx, s1.perty, s1.pertz,s1.kx,s1.ky,s1.kz = make_perturbations(n, kmin, kmax, f_solenoidal)
s1.erot_ke_ratio = get_erot_ke_ratio(s1.pertx, s1.perty, s1.pertz)
#s2
np.random.seed(seed=20)
s2.pertx, s2.perty, s2.pertz,s2.kx,s2.ky,s2.kz = make_perturbations(n, kmin, kmax, f_solenoidal)
s2.erot_ke_ratio = get_erot_ke_ratio(s2.pertx, s2.perty, s2.pertz)
#s100
np.random.seed(seed=30)
s100.pertx, s100.perty, s100.pertz,s100.kx,s100.ky,s100.kz = make_perturbations(n, kmin, kmax, f_solenoidal)
s100.erot_ke_ratio = get_erot_ke_ratio(s100.pertx, s100.perty, s100.pertz)

def vec_mag(x,y,z):
    return np.sqrt(x**2+y**2+z**2)

def k1_2_pert(sd):
    sd.k= np.sqrt(np.array(sd.kx**2+sd.ky**2+sd.kz**2)) 
    ik1= np.where(np.logical_and(sd.k**2 >= 1.**2, sd.k**2 < (2.)**2))
    ik2= np.where(np.logical_and(sd.k**2 >= 2.**2, sd.k**2 < (3.)**2))
    (sd.k1_pertx,sd.k1_perty,sd.k1_pertz)= (sd.pertx[ik1],sd.perty[ik1],sd.pertz[ik1])
    sd.k1_pert_mag= vec_mag(sd.k1_pertx,sd.k1_perty,sd.k1_pertz)
    (sd.k2_pertx,sd.k2_perty,sd.k2_pertz)= (sd.pertx[ik2],sd.perty[ik2],sd.pertz[ik2])
    sd.k2_pert_mag= vec_mag(sd.k2_pertx,sd.k2_perty,sd.k2_pertz)
k1_2_pert(s1)
k1_2_pert(s2)
k1_2_pert(s100)

ref_vec= np.array([0,0,1])
def getAngle(sd,vec):
    mag_vec= np.sqrt(vec[0]**2+vec[1]**2+vec[2]**2)
    sd.k1_cos= (sd.k1_pertx*vec[0]+sd.k1_perty*vec[1]+sd.k1_pertz*vec[2])/mag_vec/sd.k1_pert_mag
    sd.k2_cos= (sd.k2_pertx*vec[0]+sd.k2_perty*vec[1]+sd.k2_pertz*vec[2])/mag_vec/sd.k2_pert_mag
getAngle(s1,ref_vec)
getAngle(s2,ref_vec)
getAngle(s100,ref_vec)
#plot
f,axis=plt.subplots(3,1,sharex=True,figsize=(10,10))
plt.subplots_adjust( hspace=0,wspace=0.2 )
plt.suptitle('Histograms of angle between (0,0,1) and k1, k2 driving modes',fontweight='bold',fontsize='x-large')
ax=axis.flatten()
bins=10
ax[0].hist(s1.k1_cos,bins=bins,align='mid',histtype='step',color='b',label='k1')
ax[0].hist(s1.k2_cos,bins=bins,align='mid',histtype='step',color='r',label='k2')
ax[1].hist(s2.k1_cos,bins=bins,align='mid',histtype='step',color='b')
ax[1].hist(s2.k2_cos,bins=bins,align='mid',histtype='step',color='r')
ax[2].hist(s100.k1_cos,bins=bins,align='mid',histtype='step',color='b')
ax[2].hist(s100.k2_cos,bins=bins,align='mid',histtype='step',color='r')
ax[2].set_xlim(-1,1)
ax[2].set_xlabel(r'cos($\theta$)',fontweight='bold',fontsize='x-large')
for i,axi in enumerate(ax):
	axi.set_ylabel(ylab[i],fontweight='bold',fontsize='x-large')
ax[0].legend(loc=(0.,1.01),ncol=2,fontsize='large')
plt.savefig('k1_k2_angle.png',dpi=150)
#plot_spectrum1D(pertx,perty,pertz)

#divV_rms = 0
#for i in range(1,n[0]-1):
#    for j in range(1,n[1]-1):
#        for k in range(1,n[2]-1):
#            divV_rms += ((pertx[i+1,j,k]-pertx[i-1,j,k])+(perty[i,j+1,k]-perty[i,j-1,k])+(pertz[i,j,k+1]-pertz[i,j,k-1]))**2
#divV_rms = sqrt(divV_rms/np.product(n))
#print divV_rms


