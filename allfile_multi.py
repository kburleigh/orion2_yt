from yt.mods import *
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import AxesGrid

import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import my_fund_const as fc
import init_cond as mic
import deriv_fields 

pars= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
ic = mic.calcIC(pars)
ic.prout()

######
def onefile_multi_plot(file):
    pf= load(path+file)
    #units
    #myu = "r [rB]"
    #pf.units[myu] = 1./ic.rB #rB = ic.rB bondi radii, so 1 cm = 1/ic.rB centimeters!

    #plot
    fig = plt.figure()
    grid = AxesGrid(fig, (0.075,0.075,0.85,0.85),
                 nrows_ncols = (2,2),
                 axes_pad =1.5,
                 label_mode = "all",
                 share_all = False,
                 cbar_location="right",
                 cbar_mode="each",
                 cbar_size="3%",
                 cbar_pad="0%")
    #1
    units = "au"
    p = SlicePlot(pf, 'x', "density", center=pf.domain_center)
#commented out for Hydro case
# p.annotate_title("B Field")
# p.annotate_particles(1,p_size=4,col="r")
# Bargs ={}
# Bargs["color"]="black"
# p.annotate_streamlines("Y-magnfield","Z-magnfield",factor=200,plot_args=Bargs)
    ylab = (ic.Lbox/1.5e13, units)
    zlab = (ic.Lbox/1.5e13, units)
    p.set_width(ylab,zlab)
    p.set_axes_unit(units)
    #p.set_zlim("density",dens_ex[0],dens_ex[1])
    p.set_cmap("density","winter")

    plot = p.plots[ "density" ]
    plot.figure = fig
    plot.axes = grid[0].axes
    plot.cax = grid.cbar_axes[0]
    p._setup_plots()

    #2
    p = SlicePlot(pf, 'x', "density", center=pf.domain_center)
    p.annotate_title("Velocity Field")
    p.annotate_particles(1,p_size=4,col="r")
    Bargs ={}
    Bargs["color"]="black"
    p.annotate_streamlines("y-vel","z-vel",factor=200,plot_args=Bargs)
    ylab = (ic.Lbox/1.5e13, units)
    zlab = (ic.Lbox/1.5e13, units)
    p.set_width(ylab,zlab)
    p.set_axes_unit(units)
    #p.set_zlim("density",dens_ex[0],dens_ex[1])
    p.set_cmap("density","winter")

    plot = p.plots[ "density" ]
    plot.figure = fig
    plot.axes = grid[1].axes
    plot.cax = grid.cbar_axes[1]
    p._setup_plots()

    #3
    p = SlicePlot(pf, 'x', "density", center=pf.domain_center)
    p.annotate_title("Grids")
    p.annotate_grids()
    # ylab = (ic.Lbox/ic.rB, myu)
#        # 	zlab = (ic.Lbox/ic.rB, myu)
#     ylab = (ic.Lbox/1.5e13, units)
#     zlab = (ic.Lbox/1.5e13, units)
#     p.set_width(ylab,zlab)
#     #    p.set_axes_unit(myu)
#     p.set_axes_unit(units)
#     #p.set_zlim("density",dens_ex[0],dens_ex[1])
    p.set_cmap("density","winter")

    plot = p.plots[ "density" ]
    plot.figure = fig
    plot.axes = grid[2].axes
    plot.cax = grid.cbar_axes[2]
    p._setup_plots()

    #output
    name = "imgX4_%s.pdf" % file[5:9]
    spath = path+"plots/" + name
    plt.savefig(spath)
#####


path = "/clusterfs/henyey/kaylanb/Test_Prob/Turb/bondiTurb/quick/driving_t0_only/MHD/lev2/"

dpatt = "data.0009.3d.hdf5"
files = glob.glob1(path, dpatt)

#dens_ex = [ic.rho0/2.,ic.rho0*2.]
print "READING from DIR: %s" % path
for file in files:
    print "READING FILE: %s" % file
	onefile_multi_plot(file) #,dens_ex)


