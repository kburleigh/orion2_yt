'''
WARNING: to correctly combine pout.0_* or out_* files:
    grep out_1 -e step|tail
    grep out_2 -e step|head
    if first 'coarse time step' in `grep out_2 -e step|head` also occurs in out2 then:
        good_step= 1 less than overlapping coarse time step in above 'if statement'
        grep out_1 -e 'coarse time step  good_step' -n
        go to that line in out_1
        delete everything after it (eg "coarse time step" printed after all subcycling is done, which means my 'KJB: advance level' statments have all printed for that coarse time step already)
    then go to last 'coarse time step' in every out_* file and delete any time/sink info that comes after it (if hasn't already been done). if walltime got exceeded the time/sink info printing stopped midway without coarse time step telling you it is there so this script will get messed up
    cat out_1 out_2 > out_12

dependecies:
    Combined standard out files: pout.0_* or out_* 
    Sim_bash_MdotEveryTstep.sh -- does inital parsing of standard out from simulation 
input: 
    standard out from simulation (or concatenation of many standard outputs) giving sink particle info every finest-level step (AMRLevelOrion.cpp, SinkParticleList.cpp print statements).
output: 
    pickle file "mdots_EveryStep*.pickle" containing time,mass,mdot for every sink on the finest-level
'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-path",action="store",help='relative path to find data/write analysis',required=True)
parser.add_argument("-pout",action="store",help='standard out/err from simulation',required=True)
parser.add_argument("-cs",type=float,action="store",help='isothermal sounds speed',required=True)
parser.add_argument("-NSinks",type=int,action="store",help='# of sinks',required=True)
#optional
parser.add_argument("-rms_local_global",action="store",help='relative path to local_rmsV.txt and global_rmsV.txt files (optional)',required=False)
args = parser.parse_args()

import subprocess
import matplotlib
matplotlib.use('Agg')
import numpy as np
from pickle import dump as pdump
import sys
import numpy.linalg
from scipy import interpolate
import os

print "all libraries imported"

class SinkData():
    def __init__(self,NSinks,NSinkFiles):
        shape= (NSinks,NSinkFiles)
        self.sid= np.zeros(shape).astype(np.int)-1
        #print mass to 16 decimals (max decimals for double precision) so need float64
        self.time= np.zeros(NSinkFiles).astype(np.float64)-1
        self.dt= self.time.copy()
        self.mass= np.zeros(shape).astype(np.float64)-1
        self.x= np.zeros(shape).astype(np.float64)-1
        self.y= np.zeros(shape).astype(np.float64)-1
        self.z= np.zeros(shape).astype(np.float64)-1
        #local rmsV,rmsVa,meanRho which is read in from LocalData() object, interpolated to sink.time points
        if args.rms_local_global: 
            self.rmsV= np.zeros(shape).astype(np.float64)-1
            self.rmsVa= self.rmsV.copy()
            self.meanRho= self.rmsV.copy() 
            #global rmsV,rmsVa,meanRho over simulation domain, interpolated to sink.time points 
            self.glob_rmsV= np.zeros(NSinkFiles).astype(np.float64)-1
            self.glob_rmsVa= np.zeros(NSinkFiles).astype(np.float64)-1
            self.glob_meanRho= np.zeros(NSinkFiles).astype(np.float64)-1
    def MdotFine_tFine(self):
        self.MdotLikeLev0= (self.mass[:,1:-1]-self.mass[:,:-2])/self.dt[2:] #closes agreement with mdot form sinkLev0 data, fluctuations are above the median mdot 
        self.timeLikeLev0= self.time[:-2] 
        self.MdotRaw= (self.mass[:,1:]-self.mass[:,:-1])/self.dt[:-1] #makes most sense looking from stdout files
        self.timeRaw= self.time[:-1]
        #self.MdotFine_neg2= (self.mass[:,1:-1]-self.mass[:,:-2])/self.dt[2:]
        #self.MdotFine_neg1= (self.mass[:,1:]-self.mass[:,:-1])/self.dt[1:]
        #self.MdotFine_0= (self.mass[:,1:]-self.mass[:,:-1])/self.dt[:-1]
        #self.MdotFine_2= (self.mass[:,3:]-self.mass[:,2:-1])/self.dt[:-3]
        #self.MdotFine[0,:]=0. 
        #first time step dm is from draining all gas in 4dx zone into sink, no physical, set to 0
        #self.MdotFine[1,:]=0. 
class LocalData():
	def __init__(self,NSinks,n_hdf5_files,NSinkFiles):
		'''local rmsV,rmsVa,meanRho for each sinks'''
		#data from local_rmsV.txt file: NSinks each with n_hdf5_files time points
		self.time= np.zeros(n_hdf5_files).astype(np.float64)-1
		self.sid= np.zeros(NSinks).astype(np.int)-1
		shape= (NSinks,n_hdf5_files)
		self.rmsV= np.zeros(shape).astype(np.float64)-1
		self.rmsVa= self.rmsV.copy()
		self.meanRho= self.rmsV.copy()

class GlobalData():
	pass
	
#grep for only finest-level sink particle data printed out from simulation and save to file "pout_grep"
Pout= os.path.join(args.path,args.pout)
Grep= Pout+"_grep"
subprocess.call(["./Sim_bash_MdotEveryTstep.sh", Pout,Grep])
#
f=open(Grep,'r')
lines= f.readlines()
f.close()
npts= np.int64(lines[1].split()[-1]) #2nd line in "pout_grep" contains number of finest-level time steps, so number of mass points for every sink
sink= SinkData(args.NSinks,npts)  
#fill the rest with data on finest level
f=open(Pout,'r')
lines= f.readlines()
f.close()
m1= "coarse time step"
step=0 #with FixedhiRes, time = 0 is the first coarst time read in
i=0
print "now reading concatenated output file"
while i < len(lines):
# for i in np.arange(len(lines)).astype(np.int64):
    if lines[i].startswith(m1):  #info after this line is for all sinks
        #save the time
        sink.dt[step]= np.around(np.float64(lines[i].split()[-1]),decimals=16)
        old_time=  np.around(np.float64(lines[i].split()[-5]),decimals=16)
        sink.time[step]=old_time+sink.dt[step]
#         counter=1  #go through some number of lines getting all sink info
        sid=0  #sink id
        #reset i to 64 lines before "m1" occured so can read in sink data
        i=i-64
        while sid < args.NSinks:
            if lines[i].startswith("Id,cnt,dt,mass,x,y,z="):      
                #print "i=%d" % i
                #fill mass and other sink info
                info= lines[i].split()
#                 print "sink.sid shape= ",sink.sid.shape
#                 print "filling sink: info 2,4,5,6,7",info[2],info[4],info[5],info[6],info[7]
                sink.sid[sid,step]= np.int(info[2])
                sink.mass[sid,step]= np.around(np.float64(info[4]),decimals=16)
                sink.x[sid,step]= np.around(np.float64(info[5]),decimals=16)
                sink.y[sid,step]= np.around(np.float64(info[6]),decimals=16)
                sink.z[sid,step]= np.around(np.float64(info[7]),decimals=16)
                #update counters
                sid+=1  #one sink's info saved, do so for the next
                i+= 1   #make i the last line we read
            else: raise ValueError #error if found anything other than sink info
        #exited while loop
        i+= 1   #i is the last line read, so move i+=1 reads the next line
        step+=1 #"step" is the index where sink info at the NEXT TIME will go 
    else: i+=1   #read the next line
print "finished reading file"
#all sink data extracted, now compute mdot and plot
#test if time increases monotonically, if not, then 'args.pout' was not concatenated from out_1, out_2, ...,out_N correctly, crash
ibad=np.where(sink.dt <= 0.)[0]
print "ibad= ",ibad
print "dt= ",sink.dt
print "mass= ",sink.mass[0,:]
if ibad.size > 0:
    #see commit #75131169699209bda7
    print "standard output files (eg pout.0_1,...,pout.0_N or out_1,...,out_N) not combined correctly, time info does not increase monotonically everywhere!"
    sys.exit()
print "calculating Mdot"
#finally, compute mdotFine,tFine for simulation
sink.MdotFine_tFine()

#global and local data
def interp_coarse_to_fine(coarse_x,coarse_y,  fine_x):
	'''sink.time data occurs earlier and later than hdf5 data which can cause issue
	this func sets earlier occuring data equal to first time hdf5 data we have
	and later occuring data equal to last time hdf5 data we have'''
	ihalf= coarse_x.shape[0]/2
	ibeg= np.where(fine_x <= coarse_x[ihalf])[0]
	iend= np.where(fine_x > coarse_x[ihalf])[0]
	# +-3 for safety to bound larger time domain
	fbeg= interpolate.interp1d(coarse_x[:ihalf+3],coarse_y[:ihalf+3],\
							   bounds_error=False,fill_value=coarse_y[0]) 
	fend= interpolate.interp1d(coarse_x[ihalf-3:],coarse_y[ihalf-3:],\
							   bounds_error=False,fill_value=coarse_y[-1])
	fine_y= fine_x.copy()
	fine_y[ibeg]= fbeg(fine_x[ibeg])
	fine_y[iend]= fend(fine_x[iend])
	return fine_y

#get global rmsV,rmsVa,meanRho (if user said so)
if args.rms_local_global:
	glob= GlobalData()
	data=np.loadtxt(args.rms_local_global+'global_rmsV_rmsVa.txt', dtype=np.string_)
	sort_ind= np.argsort(data[:,1]) #sorted index for time to increase monotonically
	glob.time= data[:,1][sort_ind].astype(np.float64)
	glob.rmsV= data[:,2][sort_ind].astype(np.float64)
	glob.rmsVa= data[:,3][sort_ind].astype(np.float64)
	glob.meanRho= data[:,4][sort_ind].astype(np.float64)
	#interpolate to sink.time points
	sink.glob_rmsV= interp_coarse_to_fine(glob.time,glob.rmsV, sink.time)
	sink.glob_rmsVa= interp_coarse_to_fine(glob.time,glob.rmsVa, sink.time)
	sink.glob_meanRho= interp_coarse_to_fine(glob.time,glob.meanRho, sink.time)
	#get local rmsV,rmsVa,meanRho
	data=np.loadtxt(args.rms_local_global+'local_rmsV_rmsVa.txt', dtype=np.string_)
	sort_ind= np.argsort(data[:,1]) #sorted index for time to increase monotonically
	sid_ind= np.arange(2,data.shape[1]-1,4) #index of each sink id, starts at 2, then every 4th index after that
	#get object to hold local rmsV,etc, data
	n_hdf5_files= data.shape[0]
	local= LocalData(args.NSinks,n_hdf5_files,npts+1)
	#get LocalData from data
	local.time= data[:,1][sort_ind].astype(np.float128)
	#local data from each sink
	for cnt,i in enumerate(sid_ind):
		print "cnt,i= ",cnt,i
		local.sid[cnt]= data[:,i][0] #redundant, but keeps writting in the correct sid
		print "sid[cnt],data[:,i][0]=",local.sid[cnt],data[:,i][0]
		local.rmsV[:,cnt]= data[:,i+1][sort_ind].astype(np.float128) #i+1 is ith sink's rmsV at all times
		local.rmsVa[:,cnt]= data[:,i+2][sort_ind].astype(np.float128) 
		local.meanRho[:,cnt]= data[:,i+3][sort_ind].astype(np.float128) 
	#interpolate
	for i in range(args.NSinks):
		sink.rmsV[:,i]= interp_coarse_to_fine(local.time,local.rmsV[:,i], sink.time)
		sink.rmsVa[:,i]= interp_coarse_to_fine(local.time,local.rmsVa[:,i], sink.time)
		sink.meanRho[:,i]= interp_coarse_to_fine(local.time,local.meanRho[:,i], sink.time)
#save data 
print "saving data"
if args.rms_local_global: name="sinks_yesRms.pickle"
else: name= "sinks_noRms.pickle"
fout=open(os.path.join(args.path,name),"w")
pdump(sink,fout)
fout.close()
