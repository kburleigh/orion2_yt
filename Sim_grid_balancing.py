import argparse
import subprocess
import numpy as np
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5",action="store",help='',required=True)
parser.add_argument("-maxlev",type=int,action="store",help='', required=True)
parser.add_argument("-ncomp",type=int,action="store",help='number of components in convserved fluid equations, ie 5 if ideal gas HD, 7 if MHD but isothermal', required=True)
parser.add_argument("-ref_ratio",type=int,action="store",help='refinement ratio between levels', default=2,required=False)
args = parser.parse_args()

subprocess.call(["./Sim_grid_balancing.sh", args.hdf5, str(args.maxlev)])
fil=open("amrBalancing_"+args.hdf5+"_.txt","r")
lines=np.array(fil.readlines())
st=np.where(lines == 'CELLS\n')[0][0]+2
nlev= args.maxlev+1
cells=np.zeros(nlev)-1
for cnt,line in enumerate(lines[st:st+nlev]): 
    cells[cnt]= float(line.split()[4])
for i in range(nlev):
    if i == args.maxlev: cells[i] /= args.ncomp
    else: cells[i]= (cells[i]-cells[i+1]/args.ref_ratio**3)/args.ncomp
print "level, ncells, eff_cube, ratio_ncells_to_lev0"
for i,c in enumerate(cells): print "%d, %d, %.1f, %.2f" % (i,cells[i],cells[i]**0.333333,cells[i]/cells[0])

