import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-path",action="store",help='path to hdf5 file(s)')
parser.add_argument("-indiv_mdots",action="store",help='hdf5 file(s)')
parser.add_argument("-title",action="store",help='hdf5 file(s)')
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import matplotlib.pyplot as plt
import glob 
import os
import pickle

import Sim_constants as sc
print "finished importing modules"

font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 10,
        }

bigG = 1.
def GetLogHist(mdot_norm,bins=100):
    logw=np.log(mdot_norm)
    (logn,logbins,jnk)= plt.hist(logw,bins=bins,align='mid',normed=True,histtype='bar')
    plt.close()
    return logbins, logn

def mdot_bondi():
	rho0=1e-2
	mass0=13./32
	cs=1.
	return 4*np.pi*rho0*(bigG*mass0)**2/cs**3

#main
fin= open(os.path.join(args.path,args.indiv_mdots),'r')
indiv=pickle.load(fin)
fin.close()
class Mdots():
    def __init__(self):
        self.B= mdot_bondi() 
mdot=Mdots()
class Obj():
	pass
(logbins,loghists)= GetLogHist(indiv/mdot.B,bins=15)
bins=(logbins[:-1]+logbins[1:])/2
bins=np.exp(bins)
left=np.exp(logbins[:-1])
right=np.exp(logbins[1:])
#plt.plot(bins,loghists,'k-')
plt.bar(bins,loghists,width=right-left,align='center',fill=False)
for b,h in zip(bins,loghists): plt.vlines(b,0,0.5,color='r',linestyle='dashed')
plt.xlim([1e-2,1e2])
plt.ylim([0,0.5])
#     plt.yscale('log')
plt.xscale('log')
plt.xlabel(r'$\dot{M}/\dot{M}_{B}$')
plt.ylabel(r'dp/d ln (M/M_{B})')
plt.title(args.title)
plt.savefig("./indiv_distribution.png")
plt.close()
