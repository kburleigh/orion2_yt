import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm

import multiprocessing
import resource
from functools import partial

import yt	
from yt.fields.api import ValidateParameter
from yt.visualization.base_plot_types import get_multi_plot
from yt.units.yt_array import YTArray
from argparse import ArgumentParser
import numpy as np
import sys
import pickle
import os

from modules.sink.data_types import InitSink

# Define new fields
def _my_vx(field, data):
    return data['X-momentum']/data['density']
def _my_vy(field, data):
    return data['Y-momentum']/data['density']
def _my_vz(field, data):
    return data['Z-momentum']/data['density']

def _my_radius(field,data):
    if data.has_field_parameter("center"):
        center= data.get_field_parameter("center").in_units("cm")
    else:
        raise ValueError
    x_hat = data["x"] - center[0]
    y_hat = data["y"] - center[1]
    z_hat = data["z"] - center[2]
    return np.sqrt(x_hat*x_hat+y_hat*y_hat+z_hat*z_hat)


def _my_radial_velocity(field, data):
    if data.has_field_parameter("bulk_velocity") and \
       data.has_field_parameter("center"):
        bv = data.get_field_parameter("bulk_velocity").in_units("cm/s")
        center= data.get_field_parameter("center").in_units("cm")
    else:
        raise ValueError #bv = data.ds.arr(np.zeros(3), "cm/s")
    xv = data["my_vx"] - bv[0]
    yv = data["my_vy"] - bv[1]
    zv = data["my_vz"]- bv[2]
#     center = data.get_field_parameter('center')
    x_hat = data["x"] - center[0]
    y_hat = data["y"] - center[1]
    z_hat = data["z"] - center[2]
    #r = np.sqrt(x_hat*x_hat+y_hat*y_hat+z_hat*z_hat)
    r= data['my_radius']
    x_hat /= r
    y_hat /= r
    z_hat /= r
    return xv*x_hat + yv*y_hat + zv*z_hat

def _my_bx(field, data):
    return data['X-magnfield']*(4.*np.pi)**0.5
def _my_by(field, data):
    return data['Y-magnfield']*(4.*np.pi)**0.5
def _my_bz(field, data):
    return data['Z-magnfield']*(4.*np.pi)**0.5

def _my_btot(field, data):
    return np.sqrt(data['my_bx']**2+ data['my_by']**2+data['my_bz']**2)

def get_vafast(btot=None,dens=None):
    assert(btot is not None and dens is not None)
    cs=1.
    va= btot/np.sqrt(4*np.pi*dens)
    return np.sqrt(cs**2 + np.power(va,2) ) 



# simulation parameters
def sim_params():
    msink=13./32
    cs=1.
    mach=5.
    rho0=1.e-2
    rBH= msink/cs**2/(mach**2+1)
    beta1_after_drive=0.075
    vABH= cs*np.sqrt(1.+mach**2+2./beta1_after_drive)
    rABH= msink/vABH**2
    print "rABH/rBH=",rABH/rBH
    return dict(rBH=rBH,cs=cs,rABH=rABH)

def four_panels(kwargs,\
                sink_id, bin_vals, ds=None,width=None,dxmin=None, init_sink_file=None, d_params=None,\
                name='radial_velocity.png'):
    '''kwargs -- dict with all params needed to choose between hydro vs magn plots'''
    if ds is None or dxmin is None or bin_vals is None or width is None or d_params is None: raise ValueError

    # Choose model
    if kwargs['which'] == 'hydro':
        fields=[('gas','my_radial_velocity')]
        ylab= r'$\mathbf{ V_r/c_s }$'
        cbar_lab= r'$\mathbf{ V_r/c_s }$'
    elif kwargs['which'] == 'magn':
        fields=[('gas','my_radial_velocity'),('gas','my_btot'),'density']
        ylab= r'$\mathbf{ V_r/V_{\rm{a,\, F}} }$'
        cbar_lab= r'$\mathbf{ V_r/V_{\rm{a,\, F}} }$'
    else: raise ValueError
 
    # Initialize plot
    fig, axes, colorbars = get_multi_plot(4, 1, colorbar='horizontal', bw = 4,cbar_padding=0.5)
    axes= axes[0]
   
    # Radial profile axis[0]
    axes[0].plot(bin_vals['binc']/d_params['rABH'], bin_vals['q50']/d_params['cs'],c='b',ls='-',lw=2, label='q50')
    axes[0].fill_between(bin_vals['binc']/d_params['rABH'],bin_vals['q25']/d_params['cs'],bin_vals['q75']/d_params['cs'],\
                         color='b',alpha=0.25,label='q25, q75')
    # Line for 4-12dx
    #for ndx in [4,6,8,10,12]:
    #    axes[0].plot([ndx*dxmin/d_params['rBH']]*2,\
    #                 [bin_vals['q25'].min()/d_params['cs'],bin_vals['q75'].max()/d_params['cs']],\
    #                 c='k',ls='--',lw=1,label=r'%d $\mathbf{ \Delta \, x_{\rm{min}} }$' % ndx)
    axes[0].plot(list(axes[0].get_xlim()), [-1]*2,\
                 c='k',ls=':',lw=2)
    laba=dict(fontweight='bold',fontsize='x-large')
    xlab= axes[0].set_xlabel(r'$\mathbf{ r [r_{\rm{ABH}}] (\rm{Bin Centers})}$',**laba)
    ylab= axes[0].set_ylabel(ylab,**laba)
    axes[0].legend(loc='lower right')
    # Remove far right tick
    xticks= np.linspace(axes[0].get_xlim()[0],axes[0].get_xlim()[1],num=7)[:-1]
    axes[0].set_xticks(xticks)
    # Set ylim max to 0
    axes[0].set_ylim(axes[0].get_ylim()[0],0.)

    # Imshow panels
    sink= InitSink(init_sink_file)
    sink.set_coord(sink_id)
    center= sink.xyz_for_sid(sink_id)

    slices={}
    frbs={}
    np_arr={}
    xmin,xmax= 0,512 #fixed res buffer
    for xyz in ['x','y','z']:
        slices[xyz] = yt.SlicePlot(ds, xyz, center=center,fields=fields)
        frbs[xyz] = slices[xyz].data_source.to_frb(width, xmax)
        # Converting our Fixed Resolution Buffers to numpy arrays
        if kwargs['which'] == 'hydro':
            np_arr[xyz]= np.array(frbs[xyz][('gas','my_radial_velocity')])
        else: 
            vr= np.array(frbs[xyz][('gas','my_radial_velocity')])
            btot= np.array(frbs[xyz][('gas','my_btot')])
            dens= np.array(frbs[xyz]['density'])
            np_arr[xyz]= vr/get_vafast(btot=btot,dens=dens)
    plots={}
    labs=dict(x=['y','z'],y=['z','x'],z=['x','y']) #x,y labels from yt when slice
    for i,xyz in zip(range(1,4),['x','y','z']): 
        plots[xyz]= axes[i].imshow(np_arr[xyz], origin='lower') #norm=LogNorm()
        #axes[i].contour(np_arr[xyz], [0], hold='on', origin='lower',\
        #                linewidths=2.,linestyles='solid',colors='w')
        #axes[i].contour(np_arr[xyz], [1], hold='on', origin='lower',\
        #                linewidths=1.,linestyles='dashed',colors='w')
        # Set ticklabels about 0 and in units rBH
        xticks= np.linspace(xmin,xmax,num=5)
        axes[i].set_xticks(xticks) # same 0 to 512Left and rightmost ticks in imshow
        axes[i].set_yticks(xticks) 
        labels= (xticks-xticks[2])/xmax # +/-0.5 from center in plot frame units
        labels= (labels*width[0]/d_params['rABH']).astype(str)
        if i == 1 or i == 2: labels[-1]= '' # Prevent overlap
        axes[i].set_xticklabels(labels)
        axes[i].set_yticklabels(labels)
        # continue
        axes[i].tick_params(direction='out')
        ti= axes[i].set_title('%s Slice' % xyz.upper(),fontsize='x-large')
        # Confirmed by makign individual slices and seeing xy labs
        xlab= axes[i].set_xlabel('%s [rABH]' % labs[xyz][0].upper())
        ylab= axes[i].set_ylabel('%s [rABH]' % labs[xyz][1].upper())
        # only show some
        axes[i].yaxis.set_visible(False)
        #axes[i].yaxis.set_visible(False)

    for xyz in ['x','y','z']: 
        plots[xyz].set_clim((np_arr[xyz].min(),np_arr[xyz].max()))
        plots[xyz].set_cmap("viridis")

    titles=[cbar_lab]*3 

    for p, cax, t in zip([plots['x'],plots['y'],plots['z']], colorbars[1:], titles):
        cbar = fig.colorbar(p, cax=cax, orientation='horizontal')
        cbar.set_label(t)

    fig.savefig(name, bbox_extra_artists=[ti,xlab,ylab], bbox_inches='tight')
    plt.close()

def bin_up(data_bin_by,data_for_percentile, bin_minmax=(0.,10.),nbins=100):
    '''bins "data_for_percentile" into "nbins" using "data_bin_by" to decide how indices are assigned to bins
    returns bin center,N,q25,50,75 for each bin
    '''
    bin_edges= np.linspace(bin_minmax[0],bin_minmax[1],num= nbins+1)
    vals={}
    for key in ['q50','q25','q75','n']: vals[key]=np.zeros(nbins)+np.nan
    vals['binc']= (bin_edges[1:]+bin_edges[:-1])/2.
    for i,low,hi in zip(range(nbins), bin_edges[:-1],bin_edges[1:]):
        keep= np.all((low < data_bin_by,data_bin_by <= hi),axis=0)
        if np.where(keep)[0].size > 0:
            vals['n'][i]= np.where(keep)[0].size
            vals['q25'][i]= np.percentile(data_for_percentile[keep],q=25)
            vals['q50'][i]= np.percentile(data_for_percentile[keep],q=50)
            vals['q75'][i]= np.percentile(data_for_percentile[keep],q=75)
        else:
            vals['n'][i]=0
    return vals

def process_a_sink_wrapper(sink_id, ds=None,radius=None,init_sink_file=None,model=None,outdir=None):
    '''wrapper around actual function so get traceback for processes'''
    try:
        process_a_sink(sink_id, ds=ds,radius=radius,init_sink_file=init_sink_file,model=model,outdir=outdir) 
    except: 
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, file=sys.stdout)

def process_a_sink(sink_id, ds=None,radius=None,dxmin=None,init_sink_file=None,model=None,outdir=None):
    '''radius -- tuple of (float,"cm") where fload is number cm corresponding some # rBH
    dxmin -- dx level 5 in simulation units so "cm"
    Returns: dict of q25-75 and bin centers for radius and radial velocity relative location of sink in simulation units "cm"'''
    assert(ds is not None and radius is not None and model is not None)
    # Data within a sphere of radius ~ rBH, centered on sink
    sink= InitSink(init_sink_file)
    sink.set_coord(sink_id)
    center= sink.xyz_for_sid(sink_id)
    print('sink_id=',sink_id,'center= ',center)
    sp = ds.sphere(center, radius) 
    # Compute my_radius relative to sink  
    sp.set_field_parameter("center", yt.YTArray(center, "cm"))
    # Hydro calculations (for all models)
    # Compute my_radial_velocity subtracting correct Bulk vel 
    # Bulk vel is avg Vxyz between radius= (6dxmin, outer radius]
    r= np.array( sp[('gas','my_radius')] )
    b_keep= r > 6.*dxmin
    bulk_v=np.zeros(3)
    for i,comp in enumerate(['x','y','z']):
        bulk_v[i]= np.average( np.array(sp['my_v'+comp])[b_keep] )
    sp.set_field_parameter("bulk_velocity", yt.YTArray(bulk_v, "cm/s"))
    # Bin up Vr/cs and Vr/Vafast if later exists
    bin_vals={}
    # Bin radius, get 25-75 quartiles of my_radial_vel in each bin
    rad= np.array( sp[('gas','my_radius')] )
    vr= np.array( sp[('gas','my_radial_velocity')] )
    bin_vals['hydro']= bin_up(rad,vr, bin_minmax=(6*dxmin,rad.max()),nbins=100) 
    if model != 'bInf':
        rad= np.array( sp[('gas','my_radius')] )
        vr= np.array( sp[('gas','my_radial_velocity')] )
        btot= np.array( sp[('gas','my_btot')] )
        dens= np.array( sp['density'] )
        bin_vals['magn']= bin_up(rad,vr/get_vafast(btot=btot,dens=dens), \
                                 bin_minmax=(6*dxmin,rad.max()),nbins=100)
    return bin_vals 
     
    ## 1D profile object
    #if model == 'bInf':
    #    prof = yt.create_profile(sp, ('gas','my_radius'), ('gas','my_radial_velocity'))
    #else: 
    #    prof = yt.create_profile(sp, ('gas','my_radius'), [('gas','my_radial_velocity'),\
    #                                                       ('gas','my_va')])
    #r = prof.x.value
    #rvel= prof['gas','my_radial_velocity'].value
    #if model != 'bInf': va= prof['gas','my_va'].value
    ## save data
    #if model == 'bInf':
    #    fout=open(os.path.join(outdir,'r_rvel_%d.pickle' % sink_id), 'w') 
    #    pickle.dump((r,rvel),fout) #rva
    #    fout.close()
    #else:
    #    fout=open(os.path.join(outdir,'r_rvel_va_%d.pickle' % sink_id), 'w') 
    #    pickle.dump((r,rvel,va),fout) #rva
    #    fout.close()
    #print "wrote: %s" % fout
    ## Plot for sanity check
    #plt.plot(r, rvel)
    ## Plot the variance of the velocity magnitude.
    #plt.xlabel('r [cm]')
    #plt.ylabel('Radial Velocity [cm/s]')
    #plt.savefig(os.path.join(outdir,'radial_vel_%d.png' % sink_id))
    #plt.close()


def current_mem_usage():
    '''return mem usage in MB'''
    return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.**2

if __name__ == '__main__':
    parser = ArgumentParser(description="test")
    parser.add_argument("--hdf5",action="store",default="data/data.0006.3d.hdf5",help='hdf5 file to load',required=True)
    parser.add_argument("--model",choices=['bInf','b1','b1e-2'],action="store",help='hdf5 file to load',required=True)
    parser.add_argument("--init_sink_file",action="store",default="modules/hdf5_params/init.sink",help='init.sink file',required=True)
    parser.add_argument("--sink_id",type=int,action="store",help='[0-63] are possible',required=True)
    parser.add_argument("--cores",type=int,default=1,action="store",help='if > 1 do multiprocessing',required=False)
    args = parser.parse_args()

    # Outdir
    outdir= os.path.join(args.model, os.path.basename(args.hdf5).replace('.','_'))
    if not os.path.exists(outdir): os.makedirs(outdir)	 

    # Autotmatically add fields to all subsequent data loads
    yt.add_field("my_vx",function=_my_vx,units='cm/s')
    yt.add_field("my_vy",function=_my_vy,units='cm/s')
    yt.add_field("my_vz",function=_my_vz,units='cm/s')
    yt.add_field("my_radius",
                 function=_my_radius,
                 units="cm",
                 take_log=False,
                 validators=[ValidateParameter('center')])
    yt.add_field("my_radial_velocity",
                 function=_my_radial_velocity,
                 units="cm/s",
                 take_log=False,
                 validators=[ValidateParameter('center'),
                             ValidateParameter('bulk_velocity')])
    if args.model != 'bInf':
    #    # FIX ME, units?
        yt.add_field("my_bx",function=_my_bx, units='code_magnetic')
        yt.add_field("my_by",function=_my_by,units='code_magnetic')
        yt.add_field("my_bz",function=_my_bz,units='code_magnetic')
        yt.add_field("my_btot",function=_my_btot,units='code_magnetic')
        #yt.add_field("my_vafast",function=_my_vafast) #,units='auto')
    # load hdf5
    ds = yt.load(args.hdf5)
    # Cores iterate over center positions, each receives "ds" object
    #step= np.array(ds.domain_width/ds.domain_dimensions)[0]
    #radius=10*step
    d_params= sim_params()
    #radius= dict(profile=3.*d_params['rBH'], imshow=0.5*d_params['rBH']) 
    radius= dict(profile=3.*d_params['rABH'], imshow=0.5*d_params['rABH']) 
    dxmin= 2./256./2**5
    if args.cores > 1:
        # Multiprocess
        print 'Global maximum memory usage b4 multiprocessing: %.2f (mb)' % current_mem_usage()
        sink_ids= range(63)
        pool = multiprocessing.Pool(args.cores)
        results=pool.map(partial(process_a_sink_wrapper, ds=ds,radius=radius['profile'],dxmin=dxmin, init_sink_file=args.init_sink_file,model=args.model,outdir=outdir), \
                         sink_ids)
        pool.close()
        pool.join()
        del pool
        print 'Global maximum memory usage after multiprocessing: %.2f (mb)' % current_mem_usage()
        print 'multiprocessing done, exiting'
        sys.exit()
    # Bin up Vr/cs, Vr/Vafast
    vals_dict= process_a_sink(args.sink_id, ds=ds,radius=radius['profile'],dxmin=dxmin,init_sink_file=args.init_sink_file,model=args.model,outdir=outdir)
    # 4 panel plots
    # Hydro
    kwargs= dict(which='hydro',name='rv_imshow_%d.png' % args.sink_id)
    four_panels(kwargs,\
                args.sink_id, vals_dict[ kwargs['which']], ds=ds,width=(2*radius['imshow'], 'cm'),dxmin=dxmin, \
                init_sink_file=args.init_sink_file,d_params=d_params,\
                name=os.path.join(outdir,kwargs['name']))
    if args.model != 'bInf':
        # Magnetic
        kwargs= dict(which='magn',name='vafast_imshow_rBHA_%d.png' % args.sink_id)
        four_panels(kwargs,\
                    args.sink_id, vals_dict[kwargs['which']], ds=ds,width=(2*radius['imshow'], 'cm'),dxmin=dxmin, \
                    init_sink_file=args.init_sink_file,d_params=d_params,\
                    name=os.path.join(outdir,kwargs['name']))

    # Save enough to remake 1d plots
    fout=open(os.path.join(outdir,'binvals_%d.pickle' % args.sink_id),'w')
    pickle.dump((vals_dict,d_params,dxmin),fout)
    fout.close()
	###################
	# slice plots
	#slc= yt.SlicePlot(ds, 'z', [('gas','my_radial_velocity')], 
	#				  width = (2*rad, 'cm'))
	#slc.set_cmap(field=('gas','my_radial_velocity'), cmap='viridis') #'inferno','viridis'
	#slc.save('z_radial_velocity_viridis.png')
	#slc.set_cmap(field=('gas','my_radial_velocity'), cmap='inferno') #'inferno','viridis'
	#slc.save('z_radial_velocity_inferno.png')






