import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from argparse import ArgumentParser
import numpy as np
import sys
import pickle
import os
import glob
import seaborn as sns


def plot_a_profile(ax, profile,lab):
    ax.plot(np.mean(profile['r'],axis=0), np.mean(profile['vr_vff'],axis=0),label=lab)

if __name__ == '__main__':
    parser = ArgumentParser(description="test")
    parser.add_argument("--search",action="store",default="r_rvel_*.pickle",help='',required=True)
    parser.add_argument("--model",choices=['bInf','b1','b1e-2'],action="store",help='tells if magn keys exist in pickle file',required=True)
    parser.add_argument("--vafast",action="store_true",help='set to plot vr/vafast instead of vr/cs',required=False)
    parser.add_argument("--va_cs",action="store_true",help='plot vafast/cs = vr/cs / vr/vafast',required=False)
    parser.add_argument("--vff",action="store_true",help='plot vr/vfreefall',required=False)
    parser.add_argument("--save_profile",action="store_true",help='set save profile to pickle file',required=False)
    parser.add_argument("--plot_profiles",action="store_true",help='set plot hydro, beta=1 profiles and quit',required=False)
    args = parser.parse_args()
    
    if args.plot_profiles:
        # Set up plot
        sns.set_style('ticks')
        sns.set_palette('colorblind')
        laba=dict(fontweight='bold',fontsize='x-large')
        kwargs_axtext=dict(fontweight='bold',fontsize='x-large',va='bottom',ha='center')
        fig,ax=plt.subplots()
        # Add curves
        hydro= 'bInf/data_0194_3d_hdf5/profile_vrvff_bInf.pickle'
        beta1= 'b1/data_0484_3d_hdf5/profile_vrvff_b1.pickle'
        for name,fil in zip(['hydro',r'$\mathbf{ \beta = 1 }$'],[hydro,beta1]):
            fin=open(fil,'r')
            prof=pickle.load(fin)
            fin.close()
            plot_a_profile(ax, prof,name) 
        # Finish labeling
        ax.legend(loc='lower right',fontsize='medium',frameon=True)   
        ylab= ax.set_ylabel(r'$\mathbf{ V_r/V_{\rm{ff}} \, \rm{(Sink Averaged)} }$',**laba)
        xlab= ax.set_xlabel(r'$\mathbf{ r [r_{\rm{BH}}] \, \rm{(Bin Centers)} }$',**laba)
        ax.tick_params(direction='in')
        sname='avg_profiles.png' 
        plt.savefig(sname, bbox_extra_artists=[xlab,ylab], bbox_inches='tight')
        plt.close()
        print "wrote %s" % sname
        print "exiting early"
        sys.exit()


    if args.save_profile and not args.vff: 
        print('args.vff must be turned on if save_profile is')
        raise ValueError

    if args.vafast:
        kwargs=dict(ylabel=r'$\mathbf{ V_r/V_{\rm{a,\, F}} }$',\
                    curves='magn',\
                    ylim=None)
    elif args.va_cs:
        kwargs=dict(ylabel=r'$\mathbf{ V_{\rm{a,\, F}} / c_s }$',\
                    curves='both',\
                    ylim=None)
    elif args.vff:
        kwargs=dict(ylabel=r'$\mathbf{ V_r/V_{\rm{ff}} }$',\
                    curves='hydro',\
                    ylim=None)
    else:
        kwargs=dict(ylabel=r'$\mathbf{ V_r/c_s }$',\
                    curves='hydro',\
                    ylim=[-16,2])
    if args.model == 'bInf': kwargs['title']= r'$\mathbf{ \beta_0 = \infty }$'
    else: kwargs['title']= r'$\mathbf{ \beta_0 = 1 }$'
                    

    sns.set_style('ticks')
    sns.set_palette('colorblind')
    laba=dict(fontweight='bold',fontsize='x-large')
    kwargs_axtext=dict(fontweight='bold',fontsize='x-large',va='bottom',ha='center')
    fig,ax=plt.subplots()

    fils= np.sort(np.array(glob.glob(args.search)))
    if len(fils) == 0: raise ValueError
    # Output dir basename of any file
    outdir= os.path.dirname(fils[0])
    # 
    ylab= ax.set_ylabel(kwargs['ylabel'],**laba)
    ti= ax.set_title(kwargs['title'],**laba)
    xlab= ax.set_xlabel(r'$\mathbf{ r [r_{\rm{BH}}] (\rm{Bin Centers})}$',**laba)
    ax.tick_params(direction='in')

    # Plot 64 lines
    fil_fast=[]
    # store r,vr/vff profiles in dict
    profile=dict(r=np.zeros((len(fils),100))+np.nan,\
                 vr_vff=np.zeros((len(fils),100))+np.nan)
    for ifil,fil in enumerate(fils):
        print "loading %s" % fil
        fin=open(os.path.join(fil), 'r') 
        (bin_vals,d_params,dxmin)= pickle.load(fin)
        fin.close()
        # Add 64 curves to each plot
        if args.va_cs:
            ax.plot(bin_vals[ 'hydro' ]['binc']/d_params['rBH'], bin_vals['hydro']['q50']/bin_vals['magn']['q50']/d_params['cs'])
        elif args.vff:
            vff= np.sqrt(2*(13./32)/bin_vals[ 'hydro' ]['binc'])
            profile['r'][ifil,:]= bin_vals[ 'hydro' ]['binc']/d_params['rBH']
            profile['vr_vff'][ifil,:]= bin_vals['hydro']['q50']/vff
            ax.plot(profile['r'][ifil,:], profile['vr_vff'][ifil,:])
        else:
            ax.plot(bin_vals[ kwargs['curves'] ]['binc']/d_params['rBH'], bin_vals[ kwargs['curves'] ]['q50']/d_params['cs'])
            if np.where(bin_vals[ kwargs['curves'] ]['q50']/d_params['cs'] <= -1)[0].size > 0: 
                fil_fast+= [fil]
    print 'files with vr/va < -1 someplace: '
    for f in fil_fast: print f
    # Sonic point line
    ax.plot(list(ax.get_xlim()), [-1]*2,\
             c='k',ls=':',lw=2)
    # Smaller number of yticks
    if args.va_cs == False and args.vff == False:
        if kwargs['curves'] == 'magn': ax.set_yticks(np.linspace(ax.get_ylim()[0],ax.get_ylim()[1],num=7).astype(int))
        else: 
            ax.set_yticks(np.arange(2,-16-3,-3))
            ax.set_ylim([-16,2])
    ax.legend(loc='lower right',fontsize='medium',frameon=True)    
    if args.va_cs:
        name=os.path.join(outdir,'%dprofiles_%s_va_cs.png' % (len(fils),args.model))
    elif args.vff:
        name=os.path.join(outdir,'%dprofiles_%s_vr_vff.png' % (len(fils),args.model))
    else:
        name=os.path.join(outdir,'%dprofiles_%s_%s.png' % (len(fils),args.model,kwargs['curves']))
    plt.savefig(name, bbox_extra_artists=[xlab,ylab,ti], bbox_inches='tight')
    plt.close()
    print 'wrote %s' % name
    # save profile?
    if args.save_profile:
        name= os.path.join(outdir,'profile_vrvff_%s.pickle' % args.model)
        if os.path.exists(name): os.remove(name)
        fout=open(name,'w')
        pickle.dump(profile,fout)
        fout.close
        print "wrote %s" % name
    print 'Done'
	
