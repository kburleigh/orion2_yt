import argparse
import yt
import numpy as np
import glob 
import os

from modules.sink.rates_and_tscales import get_tBH  

def make_dir(name):
    if os.path.exists(name): raise ValueError
    else: os.makedirs(name)

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5_dir",action="store",help='path to hdf5 file(s)')
parser.add_argument("-freq",type=float,action="store",help='ex) 0.1, fraction of tBH to grab hdf5 files for')
args = parser.parse_args()

outdir= os.path.join(args.hdf5_dir,'hdf5_every_10th_tBH/')
make_dir(outdir)
freq=args.freq*get_tBH() #fraction of tBH

fnames= glob.glob(os.path.join(args.hdf5_dir,'data*.hdf5'))
fnames= np.sort(fnames)
print 'first 5 sorted fnames'
for fn in fnames: print fn
cp_list=[]
for cnt,fname in enumerate(fnames):
    print 'reading %d of %d files' % (cnt+1,len(fnames))
    ds=yt.load(fname)
    new_time= float(ds.current_time)
    if cnt == 0: 
        cp_list.append(fname)
        prev_time= new_time
    else: 
        if new_time-prev_time >= freq:
            cp_list.append(fname)
            prev_time= new_time
        else: continue
assert(len(cp_list) > 0)
print 'copying %d hdf5 files to freq dir' % len(cp_list)
for fname in cp_list: 
    cmd= ' '.join(['cp',fname,outdir])
    print 'issuing: ',cmd
    if os.system(cmd): raise ValueError
print 'done'
