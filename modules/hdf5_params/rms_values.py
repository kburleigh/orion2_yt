import yt
import numpy as np
import os
 
def three_square(a,b,c):
    return np.power(a,2)+np.power(b,2)+np.power(c,2)

def yt_prim_vars(box,more_fields=[]):
	data={}
	for key in ['density']:
		data[key]= np.array(box[key])
	for gd_key,bd_key in zip(['vx','vy','vz'],['X-momentum','Y-momentum','Z-momentum']):
		data[gd_key]= np.array(box[bd_key])/data['density']
	test= np.array(box['X-magnfield'])
	test= test.max()
	if test > 1e-10:
		for gd_key,bd_key in zip(['bx','by','bz'],['X-magnfield','Y-magnfield','Z-magnfield']):
			data[gd_key]= np.array(box[bd_key])*(4.*np.pi)**0.5
	for key in more_fields:
		data[key]= np.array(box[key])
	if len(data['density'].shape) != 1: 
		for key in data.keys(): data[key]= data[key].flatten()
	return data

def rms_calculator(data):
	'''data is dict of flattened numpy arrays for density,vx,vy,vz,bx, etc'''
	assert(len(data['dx'].shape) == 1)
	dV= three_square(data['dx'],data['dy'],data['dz'])
	mass= data['density']*dV
	ans={}
	ans['avg_dens']= np.sum(mass)/np.sum(dV)
	ans['rms_mach']= np.average(three_square(data['vx'],data['vy'],data['vz']), weights=mass)**0.5
	ans['med_dens']= np.median(data['density'])
	ans['med_dens_wtvol']= np.median(data['density']*dV)/np.sum(dV)
	if 'bx' in data.keys(): 
		ans['rms_beta']= 8*np.pi*ans['avg_dens']/np.average(three_square(data['bx'],data['by'],data['bz']), weights=dV)
	else: ans['rms_beta']= 'inf'
	return ans

def beta(rho,B):
	cs=1.
	return rho*cs**2*np.power(B, -2)/8/np.pi

def alfven_v(rho,B):
	return B*np.power(rho, -0.5)/(4*np.pi)**0.5

def alfven_mach(v,rho,B):
	return v*np.power(alfven_v(rho,B), -1)


if __name__ == "__main__":
    #for i in bInf b100 b10 b1 b0.1 b0.01; do sbatch submit_rms_vals.sh sd2/${i}; done
	import argparse
	parser = argparse.ArgumentParser(description="test")
	parser.add_argument("-data",action="store",help='hdf5 data file',required=True)
	parser.add_argument("-quick_rms",action="store",help='set to anything to get rms values for base level cells only',required=False)
	args = parser.parse_args()
	
	ds=yt.load(args.data)
	if args.quick_rms: 
		cube= ds.covering_grid(level=0,left_edge=ds.domain_left_edge,\
						dims=ds.domain_dimensions)
		data= yt_prim_vars(cube, more_fields=['dx','dy','dz'])
		ans= rms_calculator(data)
		for key in ans.keys(): print '%s= '% key,ans[key]	
	else:
		box= ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
		data= yt_prim_vars(box, more_fields=['dx','dy','dz'])
		ans= rms_calculator(data)

		name='rms_values.txt'
		name= os.path.join(os.path.dirname(args.data),name)
		if not os.path.exists(name): 
			fout=open(name,'w')
			fout.write('#filename time avg_dens rms_mach rms_beta\n')
		else: fout=open(name,'a') #created if doesn't exist, steam points to EOF
		fout.write('%s %.8f %.5f %.5f %s\n' % (os.path.basename(args.data),float(ds.current_time),\
												ans['avg_dens'],ans['rms_mach'],ans['rms_beta']))
		fout.close()

