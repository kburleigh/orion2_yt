import numpy as np
import argparse
import os
import sys
from pickle import dump,load
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_palette('colorblind')

from modules.sink.data_types import CombinedSinkData 
from modules.sink.rates_and_tscales import mdotB,get_tBH,t_rms_mach_near_end,tBH_to_tBHA,steady_state_begins,low_hi_mdot_all_models
from modules.sink.stats import steady_rates,index_before_tBH,pdf_for_model_64_x_N,clip_mdots,steady_state_test_rw_to_end,steady_state_test_rw_to_rw,final_measurement,bootstrap
	


def all_rates(sink, args, corr=True):
	fig,ax=plt.subplots()
	for i in range(sink.mdot.shape[0]):
		if corr: ax.plot(sink.tnorm(),sink.mdot_vcorr[i,:]/mdotB()) #,label='ID: %d' % i)		
		else: ax.plot(sink.tnorm(),sink.mdot[i,:]/mdotB()) #,label='ID: %d' % i)		
	ax.set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	#second x axis
	ax2 = ax.twiny()
	ax2.set_xlabel(r'$\mathbf{ t/t_{\rm{ABH}} }$',fontweight='bold',fontsize='xx-large')
	ax2.plot(tBH_to_tBHA(sink.tnorm(),args.str_beta),sink.mdot_vcorr[i,:]/mdotB(),visible=False)		
	#y label, left vertical panels
	ax.set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_B }$',fontweight='bold',fontsize='xx-large')
	ax.set_yscale('log')
	if corr: lab= 'Model: %s, corrected' % args.str_beta
	else: lab= 'Model: %s' % args.str_beta
	ax.text(ax.get_xlim()[1]*0.01,ax.get_ylim()[1]*0.9,lab,fontweight='bold',fontsize='large',ha='left',va='top')
	#ax.legend(loc=(1.01,0),ncol=4) #,fontsize='small'
	if corr: name='all-rates-corrected-%s.png' % args.str_beta
	else: name='all-rates-%s.png' % args.str_beta
	#ax.set_xlim(0,20) 
	plt.savefig(os.path.join(os.path.dirname(args.master_sink),name))
	plt.close()

def median_rate(ax,sink,args,raw=True,corr=True,clip=True,times=[],c_ls=[]): #tsteady):
	laba=dict(fontweight='bold',fontsize='xx-large')
	if raw: ax.plot(sink.tnorm(),np.median(sink.mdot,axis=0)/mdotB(),c='k',label='raw')
	if corr: ax.plot(sink.tnorm(),np.median(sink.mdot_vcorr,axis=0)/mdotB(),c='b',label='corr')
	if clip: ax.plot(sink.tnorm(),np.median(sink.mdot_vcorr_clip,axis=0)/mdotB(),c='r',label='corr, clip')
	for time,cls in zip(times,c_ls): 
		ax.plot([time]*2,ax.get_ylim(),cls,lw=2.)
	ax.set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',**laba)
	#2nd axis
	ax2 = ax.twiny()
	ax2.set_xlabel(r'$\mathbf{ t/t_{\rm{ABH}} }$',fontweight='bold',fontsize='xx-large')
	ax2.plot(tBH_to_tBHA(sink.tnorm(),args.str_beta),np.median(sink.mdot_vcorr,axis=0)/mdotB(),visible=False)
	#ax.plot([t_steady]*2,ax[0].get_ylim(),'r--',label='Steady State')
	ax.text(ax.get_xlim()[1]*0.01,ax.get_ylim()[1]*0.9,'Median',fontweight='bold',fontsize='large',va='center',ha='left')
	ax.set_yscale('log')
	if args.ylim: ax.set_ylim(args.ylim[0],args.ylim[1]) 
	ax.legend(loc=4,fontsize='small')
	ax.set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_{\rm{B}} }$',**laba)
	#if corr: ax.plot([steady_state_begins(args.str_beta,True)]*2,[ax.get_ylim()[0],ax.get_ylim()[1]],'r--')

def when_equil_begins(ax,sink,str_beta,corr=True):
	#samples are sequential time ranges
	time_LHS,welch,ks,U= steady_state_test_rw_to_end(sink,corr=corr)
	#ax.plot(time_LHS,welch['p'],c='r',lw=2,label="Welch's t test")
	#ax.plot(time_LHS,ks['p'],c='b',lw=2,label='KS 2 sample')
	ax.plot(time_LHS,U['p'],c='y',lw=2,label='U test')
	ax.plot(ax.get_xlim(),[0.05]*2,'k--',label='P=0.05')
	ax.legend(loc=0,fontsize='small')
	ax.set_xlabel(r'$\mathbf{ t_{LHS}/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	ax.set_ylabel("P(t)",fontweight='bold',fontsize='xx-large')
	ax.set_ylim(-0.1,1.1)
	#return time when reach equilibrium
	return time_LHS[ U['p'] >= 0.05 ][0]

def all_equil_tests(sink,str_beta,corr=True):
	rows,cols=2,2
	fig,ax=plt.subplots(rows,cols,figsize=(10,10),sharey=True,sharex=True)
	plt.subplots_adjust(hspace=0.1,wspace=0)
	for row in range(rows):
		for col in range(cols):	
			if row == 0 and col == 0: time_LHS,welch,ks,U= steady_state_test_rw_to_end(sink,corr=corr)
			elif row == 0 and col == 1: time_LHS,welch,ks,U= steady_state_test_rw_to_rw(sink,dt=0.1,wind_dt=0.2,corr=corr)
			elif row == 1 and col == 0: time_LHS,welch,ks,U= steady_state_test_rw_to_rw(sink,dt=0.5,wind_dt=0.2,corr=corr)
			elif row == 1 and col == 1: time_LHS,welch,ks,U= steady_state_test_rw_to_rw(sink,dt=1.,wind_dt=0.2,corr=corr)
			else: raise ValueError
			ax[row,col].plot(time_LHS,welch['p'],c='r',lw=2,label="Welch's t")
			ax[row,col].plot(time_LHS,ks['p'],c='b',lw=2,label='KS')
			ax[row,col].plot(time_LHS,U['p'],c='y',lw=2,label='U')
			if row == 0 and col == 0: ax[row,col].set_title('row to end')
			elif row == 0 and col == 1: ax[row,col].set_title('dt=0.1,wind_dt= 0.2')
			elif row == 1 and col == 0: ax[row,col].set_title('dt=0.5,wind_dt=0.2')
			elif row == 1 and col == 1: ax[row,col].set_title('dt=1.0,wind_dt=0.2')
			ax[row,col].set_ylim(-0.1,1.1)
	#samples are sequential time ranges
	ax[1,1].legend(loc=0,fontsize='small')
	for col in range(2): ax[1,col].set_xlabel(r'$\mathbf{ t_{LHS}/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	for row in range(2): ax[row,0].set_ylabel("P(t)",fontweight='bold',fontsize='xx-large')
	plt.savefig(os.path.join(os.path.dirname(args.master_sink),'all-equil-test-%s.png' % args.str_beta))
	plt.close()
	
def just_median(sink,args):
	#fig,axes=plt.subplots(1,2,sharey=True)
	fig,ax=plt.subplots()
	#ax=axes.flatten()
	median_rate(ax,sink,args,raw=True,corr=True,clip=False)
	#median_rate(ax[0],sink,args,raw=True,corr=True,clip=False)
	#median_rate(ax[1],sink,args,raw=False,corr=False,clip=True)
	#for i in range(2): ax[i].set_xlim(0,20)
	#plt.savefig(os.path.join(os.path.dirname(args.master_sink),'median-raw-corr-%s.png' % args.str_beta))
	plt.savefig(os.path.join(os.path.dirname(args.master_sink),'median-%s.png' % args.str_beta))
	plt.close()


def median_and_when_equil_begins(sink,args):
	fig,axes=plt.subplots(2,1,sharex=True)
	ax=axes.flatten()
	plt.subplots_adjust(hspace=0)
	t_u= when_equil_begins(ax[1],sink,args.str_beta,corr=True)
	print 'model: %s, equil begins t/tBH=%.2f' % (args.str_beta,t_u)
	median_rate(ax[0],sink,args,raw=False,corr=True,clip=False,times=[t_u],c_ls=['y--'])
	#ax[1].set_xlim(0,20)
	plt.savefig(os.path.join(os.path.dirname(args.master_sink),'median-and-when-equil-begins-%s.png' % args.str_beta))
	plt.close()


def mvp_for_peter(master_sink,args):	
	#write MVP data to file for Peter
	rate=master_sink.mdot/mdotB()
	time=(master_sink.mdotTime-master_sink.mdotTime[0])/get_tBH()
	fout=open(os.path.join(os.path.dirname(args.master_sink),'%s.pickle' % args.str_beta),'w')
	master_sink=dump((rate,time),fout)
	fout.close()

def steady_pdf_cdf(sink,args):
	low,hi= low_hi_mdot_all_models()
	arr,bins,pdf,cdf= pdf_for_model_64_x_N(sink,args.str_beta, low,hi,nsinks=64,corr=True,last_2tBH=True,dt=2.)
	#plot hist
	fig,axes=plt.subplots(2,1,sharex=True)
	ax=axes.flatten()
	plt.subplots_adjust(hspace=0.1)
	ax[0].plot((bins[1:]+bins[:-1])/2,cdf,c='b')
	ax[1].plot((bins[1:]+bins[:-1])/2,pdf,c='b')
	ax[0].set_ylabel('CDF')
	ax[1].set_ylabel('PDF')
	ax[1].set_xlabel(r'$\dot{M}$')
	for i in range(2): 
		ax[i].set_xlim(-7,-2)
		ax[i].set_ylim(-0.1,1.1)
	plt.suptitle('Model: %s' % (args.str_beta,))
	plt.savefig(os.path.join(os.path.dirname(args.master_sink),'steady-pdf-cdf-%s.png' % args.str_beta))
	plt.close()

def bootstrap_sigma_vs_nsink(final_pdf,str_beta):
	'''Bootstrap plot showing shrinking of error on median measurement'''
	nsink_arr= [8,16,32,64]
	meds,q50,q158,q842={},{},{},{}
	for Nsinks in nsink_arr:
		meds['%d' % Nsinks],q50['%d' % Nsinks],q158['%d' % Nsinks],q842['%d' % Nsinks],std_low,std_hi= bootstrap(final_pdf['arr'][str_beta],str_beta,sub_sample_size=Nsinks)
	#plot distribution of medians
	fig,ax=plt.subplots(1,4,figsize=(15,5))
	# plt.subplots_adjust(wspace=1)
	kwargs=dict(fontsize=20)
	for i,Nsink in zip(range(4),nsink_arr):
		ax[i].hist(meds[str(Nsink)],normed=True)
		ax[i].plot([q50[str(Nsink)]-q158[str(Nsink)]]*2,ax[i].get_ylim(),'r--',lw=2.)
		ax[i].plot([q50[str(Nsink)]+q842[str(Nsink)]]*2,ax[i].get_ylim(),'r--',lw=2.)
		ax[i].set_title('N=%d, med=%.3f,\n+/- %.3f/%.3f' % \
						(Nsink,q50[str(Nsink)],q158[str(Nsink)],q842[str(Nsink)]),fontsize=15)
		ax[i].set_xlabel(r"Bootstrap Medians",**kwargs)
		#ax[i].set_xlim(-4.8,-3.4)
	ax[0].set_ylabel('PDF',**kwargs)
	plt.savefig(os.path.join(os.path.dirname(args.master_sink),'bootstrap-medians-%s.png'% args.str_beta),dpi=150)
	plt.close()
	#plot root n proof
	x=nsink_arr
	sig_left= np.zeros(len(x))-1
	sig_right= sig_left.copy()
	for i,Nsinks in enumerate(x):
		sig_left[i]= q158[str(Nsinks)]/q158['64']
		sig_right[i]= q842[str(Nsinks)]/q842['64']
	##
	fig,ax=plt.subplots()
	# plt.subplots_adjust(wspace=0.5)
	kwargs=dict(fontsize=20)
	# for i, in zip(range(2),['left','right'],['q15.8','q84.2']):
	ax.scatter(x,sig_left,marker='o',s=80,c='b',label=r'data: $\sigma^{-}$')
	ax.scatter(x,sig_right,marker='o',s=80,c='y',label=r'data: $\sigma^{+}$')
	#expected shape
	x_pred=np.linspace(8,64,num=100)
	ax.plot(x_pred,np.sqrt(64/x_pred),'k--',label=r'root N: $\sqrt{64/N}$')
	#
	ax.set_xticks(x)
	ax.legend(loc=1,fontsize=15,frameon=True)
	ax.set_xlabel('N sinks',**kwargs)
	ax.set_ylabel(r'$\sigma_N / \sigma_{64}$',**kwargs)
	plt.savefig(os.path.join(os.path.dirname(args.master_sink),'root-n-proof-%s.png'% args.str_beta),dpi=150)
	plt.close()


parser = argparse.ArgumentParser(description="test")
parser.add_argument("-master_sink",action="store",help='master sink.pickle file',required=True)
parser.add_argument("-str_beta",choices=['bInf','bInfsd3','b100','b10','b1','b1e-1','b1e-2',],action="store",help='directory containing parsed mdot files',required=True)
parser.add_argument("-ylim",nargs=2,type=float,action="store",help='',required=False)
parser.add_argument("-xlim",nargs=2,type=float,action="store",help='',required=False)
args = parser.parse_args()


fin=open(args.master_sink,'r')
master_sink=load(fin)
fin.close()

# Print t raw, t/tBH 
#raw_t= master_sink.old_t[-master_sink.tnorm().size:].copy()
print "%s" % args.str_beta
print "mdotTime, tnorm()"
for raw_t,norm in zip(master_sink.mdotTime,master_sink.tnorm()):
	print raw_t,norm
print 'exiting early'
sys.exit()

#tend
print 'model: %s, last t/tBH= %.5f' % (args.str_beta,master_sink.tnorm()[-1])
if False: # print machrms at end
	t_end= min(20.,master_sink.tnorm()[-1])
	i_end= index_before_tBH(master_sink,t_end,units_tBH=True)
	mach_end= master_sink.f_rms_mach(master_sink.mdotTime[i_end])
	print 'model= %s, tsteady/tBH= %.2f, tend/tBH= %.2f, rms Mach end= %.2f' % \
			(args.str_beta, steady_state_begins(args.str_beta,True),t_end,mach_end) 
#corr mdots	
end_t,end_mach= t_rms_mach_near_end(args.str_beta)
master_sink.rm_mdot_systematics(end_t,end_mach)
#final PDF
final_pdf= dict(arr={},bins={},pdf={},cdf={})  #PDF USING, 64 pdf, over last 2tBH
low,hi= low_hi_mdot_all_models()
final_pdf['arr'][args.str_beta],final_pdf['bins'][args.str_beta],final_pdf['pdf'][args.str_beta],final_pdf['cdf'][args.str_beta]= pdf_for_model_64_x_N(master_sink,args.str_beta, low,hi,nsinks=64,corr=True,last_2tBH=True,dt=2.)
#make plots
just_median(master_sink,args)
median_and_when_equil_begins(master_sink,args)
steady_pdf_cdf(master_sink,args)
all_rates(master_sink,args,corr=True)  
bootstrap_sigma_vs_nsink(final_pdf,args.str_beta)
print 'finished'
