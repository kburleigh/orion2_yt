import numpy as np
import matplotlib.pyplot as plt

def multi_plot(nrow,ncol,x,y,indices=None,fname='test.png',titles=None,xlim=False,ylim=False,logx=False,logy=False):
    '''x,y have shape (nrow*ncol,values,extra_axis)
    extra_axis is not necessary if plotting single line per plot otherwise extra axis 
    contains those lines'''
    assert(x.shape[0] == nrow*ncol)
    if nrow == 1 or ncol == 1: raise ValueError
    w,h=20,10
    fig,ax=plt.subplots(nrow,ncol,figsize=(w,h))
    plt.subplots_adjust(hspace=0,wspace=0)
    cnt=0
    for r in range(nrow):
        for c in range(ncol):
            if len(x.shape) == 3: #exta_axis, multiple lines per plot
                for extra in range(x.shape[2]):
					#some values may be nan or inf if some lines longer than others
					iuse= np.isfinite(x[cnt,:,extra])
					ax[r,c].plot(x[cnt,iuse,extra],y[cnt,iuse,extra])
            else: ax[r,c].plot(x[cnt,:],y[cnt,:])
            if titles is not None: ax[r,c].set_title('%s' % titles[cnt])
            cnt+=1
    for r in range(nrow):
        for c in range(ncol):
            if xlim: ax[r,c].set_xlim(xlim[0],xlim[1])
            if ylim: ax[r,c].set_ylim(ylim[0],ylim[1])
            if logx: ax[r,c].set_xscale('log')
            if logy: ax[r,c].set_yscale('log')
    for r in range(nrow):
        for c in range(ncol):
            if r != range(nrow)[-1]: ax[r,c].xaxis.set_major_formatter(plt.NullFormatter())
            if c != 0: ax[r,c].yaxis.set_major_formatter(plt.NullFormatter())
    plt.savefig(fname)
    plt.close()
