#!/bin/bash

python modules/plot/fundamental.py -master_sink data/processed/bInf_sd3/lev5/master_sink_bInfsd3.pickle -str_beta bInfsd3
#open data/processed/bInf_sd3/lev5/*bInfsd3.png
python modules/plot/fundamental.py -master_sink data/processed/bInf_sd2/lev5/master_sink_bInf.pickle -str_beta bInf 
#open data/processed/bInf_sd2/lev5/*bInf.png
python modules/plot/fundamental.py -master_sink data/processed/b100_sd2/lev4/master_sink_b100.pickle -str_beta b100 
#open data/processed/b100_sd2/lev4/*b100.png
python modules/plot/fundamental.py -master_sink data/processed/b10_sd2/lev4/master_sink_b10.pickle -str_beta b10 
#open data/processed/b10_sd2/lev4/*b10.png
python modules/plot/fundamental.py -master_sink data/processed/b1_sd2/lev5/master_sink_b1.pickle -str_beta b1 
#open data/processed/b1_sd2/lev5/*b1.png
python modules/plot/fundamental.py -master_sink data/processed/b0.1_sd2/lev5/master_sink_b1e-1.pickle -str_beta b1e-1 
#open data/processed/b0.1_sd2/lev5/*b1e-1.png
python modules/plot/fundamental.py -master_sink data/processed/b0.01_sd2/lev5/master_sink_b1e-2.pickle -str_beta b1e-2 
#open data/processed/b0.01_sd2/lev5/*b1e-2.png

#dir=data/processed
#for seed in sd2 sd3; do
#	if [ ${seed} -eq sd2 ]; then
#		for level in lev4 lev5; do
#			if [ ${level} -eq lev4 ]; then
#				for model in b10 b100; do
#					python modules/plot/fundamental.py -master_sink ${dir}/$i/master_sink_${model}.pickle -str_beta ${model} -ttest_beg 2 -ttest_wid 2
#for i in b0.01_sd2/lev5 b0.1_sd2/lev5  b100_sd2/lev4 b10_sd2/lev4   b1_sd2/lev5    bInf_sd2/lev5  bInf_sd3/lev5; do
#	echo ${dir}/$i
#	python modules/plot/fundamental.py -master_sink ${dir}/$i/master_sink_${model}.pickle -str_beta ${model} -ttest_beg 2 -ttest_wid 2
#done
