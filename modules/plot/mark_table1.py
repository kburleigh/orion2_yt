import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import os

phi_v=0.93
phi_w=1.25
phi_u=0.95
M0=5.

def mean_eqn(x):
    return 3./phi_u**3 * (np.log(2*phi_u*M0) - 1) * \
		   (1 + 100 * x / M0)**(-0.68)

def med_eqn(x):
    return phi_v**(-3) / np.sqrt(1 + M0**2/4) *\
		   (1 + 10/phi_v**6 * (10*phi_w*x/M0**2.3)**1.8 )**(-0.5)

def power_law(x, a,b,c):
	return a+ b*c**x

def linear(x, a,b):
	return a + b*x


parser = argparse.ArgumentParser(description="test")
parser.add_argument("--outdir",action="store",help='directory to save plots to',required=True)
args = parser.parse_args()

# Table 1 Krumholz 2006
# M0=5
phi_mean=np.array([4.2,4.0,1.7,0.1])
err_mean=np.array([0.34,0.31,0.072,0.004])
phi_med=np.array([0.42,0.42,0.39,0.047])
err_med=np.array([0.12,0.12,0.11,0.008])
log_rBl=np.array([-5.,-3.,-1.,1.])
rBl= 10**log_rBl

popt={}
popt['err_mean'], pcov = curve_fit(linear, np.log10(rBl),err_mean, p0 = [0.4,-1e-5])
popt['err_med'], pcov = curve_fit(power_law, np.log10(rBl),err_med, p0 = [0,-0.1,2.])
xvals=np.logspace(log_rBl.min(),log_rBl.max(),num=20)

fig,axes=plt.subplots(2,2)
ax=axes.flatten()
plt.subplots_adjust(wspace=0.2,hspace=0)
test_pt=0.2
# Mean
ax[0].errorbar(rBl,phi_mean,yerr=err_mean,c='k',marker='o')
ax[0].plot([0.2]*2,[0.01,20],'r--')
ax[0].plot(xvals, mean_eqn(xvals),'b-')
ax[0].scatter(test_pt,mean_eqn(test_pt),c='r',marker='o')
ax[0].text(test_pt,mean_eqn(test_pt),'%s' % str(mean_eqn(test_pt)))
ax[1].scatter(np.log10(rBl),err_mean,c='k',marker='o')
ax[1].plot(np.log10(xvals),linear(np.log10(xvals),*popt['err_mean']),'b-')
ax[1].scatter(np.log10(test_pt),linear(np.log10(test_pt),*popt['err_mean']),c='r',marker='o')
ax[1].text(np.log10(test_pt),linear(np.log10(test_pt),*popt['err_mean']),'%s' % \
			str(linear(np.log10(test_pt),*popt['err_mean'])))
#ax[1].plot([0.2]*2,[-0.05,0.4],'r--')
# Median	
ax[2].errorbar(rBl,phi_med,yerr=err_med,c='k',marker='o')
ax[2].plot([0.2]*2,[0.01,2],'r--')
ax[2].plot(xvals, med_eqn(xvals),'b-')
ax[2].scatter(test_pt,med_eqn(test_pt),c='r',marker='o')
ax[2].text(test_pt,med_eqn(test_pt),'%s' % str(med_eqn(test_pt)))
ax[3].scatter(np.log10(rBl),err_med,c='k',marker='o')
ax[3].plot(np.log10(xvals),power_law(np.log10(xvals),*popt['err_med']),'b-')
ax[3].scatter(np.log10(test_pt),power_law(np.log10(test_pt),*popt['err_med']),c='r',marker='o')
ax[3].text(np.log10(test_pt),power_law(np.log10(test_pt),*popt['err_med']),'%s' % \
			str(power_law(np.log10(test_pt),*popt['err_med'])))
#ax[3].plot([0.2]*2,[-0.02,0.14],'r--')
for i in [0,2]:
	ax[i].set_xscale('log')
for i in [0,2]:
	ax[i].set_yscale('log')
for i in [2,3]:
	#xlab= ax[i].set_xlabel(r'$\mathbf{ r_B/\el }$',fontweight='bold',fontsize='large')
	xlab= ax[i].set_xlabel('rBl',fontweight='bold',fontsize='large')
# xylims
for i in [0,2]:
	ax[i].set_xlim(1e-6,1e2)
for i in [1,3]:
	ax[i].set_xlim(-6,2)
ax[0].set_ylim(0.01,20.)
ax[2].set_ylim(0.01,2.)
# Save
name=os.path.join(args.outdir,'table1.png' )
plt.savefig(name,bbox_extra_artists=[xlab], bbox_inches='tight',dpi=150)
plt.close()
print('Wrote %s' % name)

