'''
RUN:
BETA = Inf,1,0.1,0.01
beta="b1e-2" 
beta2="b0.01"
python modules/plot/convergenc_plots.py -str_beta ${beta} -sinks ../processed_April242016_2/sd2/${beta}/lev4/master_sink_${beta}.pickle ../processed_April242016_2/sd2/${beta}/lev3/master_sink_${beta}.pickle ../processed_April242016_2/sd2/${beta}/lev2/master_sink_${beta}.pickle data/processed/${beta2}_sd2/lev5/master_sink_${beta}.pickle -level 4 3 2 5 -outdir ../processed_April242016_2/sd2/${beta}

BETA = 10,100
python modules/plot/convergenc_plots.py -str_beta ${beta} -sinks ../processed_April242016_2/sd2/${beta}/lev3/master_sink_${beta}.pickle ../processed_April242016_2/sd2/${beta}/lev2/master_sink_${beta}.pickle data/processed/${beta2}_sd2/lev4/master_sink_${beta}.pickle -level 3 2 4 -outdir ../processed_April242016_2/sd2/${beta}
'''



import numpy as np
import argparse
import os
from glob import glob
from pickle import dump,load
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns

from modules.sink.data_types import CombinedSinkData 
from modules.sink.rates_and_tscales import mdotB,mdotBH,get_tBH,steady_state_begins,tBH_to_tBHA,mark_to_kaylan_mdot,ic_rms_vals,lee_parallel,lee_perp,lee_avg_par_perp,ic_median_beta
import modules.sink.stats as stats
from modules.plot.multi_panels import multi_plot


xlim={}
xlim['bInf']=(0,10.)
xlim['b100']=(0,20)
xlim['b10']=(0,20)
xlim['b1']=(0,20)
xlim['b1e-1']=(0,14)
xlim['b1e-2']=(0,5.5)

ylim={}
ylim['bInf']=(5.e-4,4.e-3)
ylim['b100']=(6.e-4,6.e-3)
ylim['b10']=(6.e-4,6.e-3)
ylim['b1']=(5.e-4,5.e-3)
ylim['b1e-1']=(4.e-4,4.e-3)
ylim['b1e-2']=(2.e-4,3.e-3)

tmax={}
tmax['bInf']=9.2
tmax['b100']=20
tmax['b10']=20
tmax['b1']=17.8
tmax['b1e-1']=13.
tmax['b1e-2']=5.3


def get_colors():
	sns.set_palette('colorblind')
	colors= sns.color_palette()
	return colors


#def get_colors():
#	out='colors.pkl'
#	if not os.path.exists(out):
#		import seaborn as sns
#		sns.set_palette('colorblind')
#		colors= sns.color_palette()
#		with open(out,'w') as foo:
#			dump(colors,foo)
#	with open(out,'r') as foo:
#		colors= load(foo)	
#	return colors

def get_median(mdot_vcorr,tnorm,str_beta='b1',lev='5'):
	med= np.median(mdot_vcorr,axis=0)
	ind=None
	if str_beta == 'bInf':
		if lev == '4':
			ind= med > 3.5e-3
	if str_beta == 'b1':
		if lev == '2':
			ind= med > 4.5e-3
		if lev == '4':
			ind= (med < 1.7e-3)*(tnorm > 7.)
	if str_beta == 'b1e-1':
		if lev == '2':
			ind= (med < 2.3e-3)*(tnorm > 3)
			ind= np.any((ind, med > 4e-3),axis=0)
		if lev == '3':
			ind= med > 3.5e-3
	if str_beta == 'b1e-2':
		if lev == '2':
			ind= med > 3.e-3
		if lev == '3':
			ind= np.any( (med > 2e-3,med < 5e-4), axis=0)
		if lev == '4':
			ind= (med < 3.5e-4)*(tnorm > 0.5)
			ind= np.any((ind, (med < 5.5e-4)*(tnorm > 1.5)),axis=0)
	if not ind is None:
		ind= np.where(ind)[0]
		print('str_beta=%s, level=%s, removing this many mdots=%d' % \
				(str_beta,lev,len(ind)))
		for i in ind:
			med[i]= np.median(med[i-5:i+5]) 
	return med
	
FS=18 #'x-large'
dFS=FS+2 #'x-large'

labs={}
labs['bInf']= r'$\mathbf{ \beta = \infty }$'
labs['b100']= r'$\mathbf{ \beta = 100 }$'
labs['b10']= r'$\mathbf{ \beta = 10 }$'
labs['b1']= r'$\mathbf{ \beta = 1 }$'
labs['b1e-1']= r'$\mathbf{ \beta = 0.1 }$'
labs['b1e-2']= r'$\mathbf{ \beta = 0.01 }$'

def plot_rates(ax,sinks,\
		       str_beta='b1',colors=['b'],corr=True):
	keys= np.sort(sinks.keys())
	print('keys=',keys)
	for lev,c in zip(keys,colors):
		# Remove t=0 artificial spikes
		if str_beta == 'bInf':
			if lev == '2':
				sinks[lev].mdot_vcorr[:,2]= 0 # Zero out initial mdot spike
		if str_beta == 'b1':
			if lev == '2':
				sinks[lev].mdot_vcorr[:,0]= 0 # Zero out initial mdot spike
		if str_beta == 'b1e-1':
			if lev == '2':
				sinks[lev].mdot_vcorr[:,:5]= 0 # Zero out initial mdot spike
		if str_beta == 'b1e-2':
			if lev == '2':
				#print(np.median(sinks[lev].mdot_vcorr,axis=0)[:10]/mdotB())
				#raise ValueError
				sinks[lev].mdot_vcorr[:,:2]= 0 # Zero out initial mdot spike
			if lev == '3':
				sinks[lev].mdot_vcorr[:,:10]= 0 # Zero out initial mdot spike
		print sinks[lev].mdot_vcorr[0,:10]/mdotB()
		#raise ValueError
		sz=len(sinks[lev].mdot_vcorr[0,:])
		#if corr: ax.plot(sinks[lev].tnorm(),np.median(sinks[lev].mdot_vcorr,axis=0)/mdotB(),c=c,label='L=%s' % lev)
		if corr: med= get_median(sinks[lev].mdot_vcorr/mdotB(),sinks[lev].tnorm(),\
								 str_beta=str_beta,lev=lev)
		else: med= np.median(sinks[lev].mdot,axis=0)/mdotB()	
		#ax.plot(sinks[lev].tnorm(),np.median(sinks[lev].mdot_vcorr,axis=0)/mdotB(),c=c,label='L=%s' % lev)	
		#ax.plot(sinks[lev].old_t[:sz],med,c=c,label='L=%s' % lev)
		ind= sinks[lev].tnorm() <= tmax[str_beta]	
		ax.plot(sinks[lev].tnorm()[ind],med[ind],c=c,label='%s' % lev)	
	ax.set_yscale('log')
	ax.set_xlim(xlim[str_beta])
	ax.set_ylim(ylim[str_beta])
	ax.text(0.05,0.95,labs[str_beta],\
			transform=ax.transAxes,fontweight='bold',fontsize=FS,ha='left',va='top')
	
def col_plot(outdir='./'):
	#sns.set_palette('colorblind')
	sns.set_style('ticks')
	sns.set_style({"xtick.direction": "in","ytick.direction": "in"})
	colors= get_colors()
	fig,ax=plt.subplots(3,1,figsize=(7,12))
	plt.subplots_adjust(hspace=0.2)
	for cnt,str_beta in enumerate(['bInf','b1','b1e-2']):
	#for cnt,str_beta in enumerate(['bInf']):
		tsteady= steady_state_begins(str_beta,True)
		print 'STEADY STATE: model=%s, t/tBH=%.2f, t/tABH=%.2f' % (str_beta,tsteady,tBH_to_tBHA(tsteady, str_beta))
		# Get data
		fns= glob('../processed_April242016_2/sd2/%s/lev*/master_sink_%s.pickle' % (str_beta,str_beta) )
		sinks={}
		for fn in fns:
			lev= fn[ fn.find('lev')+3 ]
			fin=open(fn,'r')
			sinks[lev]=load(fin)
			fin.close()
			#corr mdots	
			end_t,end_mach= stats.t_rms_mach_near_end(str_beta)
			sinks[lev].rm_mdot_systematics(end_t,end_mach)
		plot_rates(ax[cnt],sinks,\
				  str_beta=str_beta,colors=colors,corr=True)
	#leg=ax[0].legend(loc=(0.05,1.02),ncol=4,title='Level',frameon=True)
	leg=ax[0].legend(loc=(0.8,0.01),ncol=1,fontsize=FS,frameon=False,numpoints=1,scatterpoints=1)
	ax[0].text(0.9,0.6,r'$\mathbf{ L_{\rm{max}} }$',\
			   transform=ax[0].transAxes,fontweight='bold',fontsize=FS,ha='left',va='top')
	xlab= ax[2].set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize=FS+dFS)
	for cnt in [0,1,2]:
		ylab=ax[cnt].set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_B }$',fontweight='bold',fontsize=FS+dFS)
	for i in range(3):
		ax[i].tick_params(axis='both',labelsize=FS)
	name=os.path.join(outdir,'convergence_col.png' )
	plt.savefig(name,bbox_extra_artists=[leg,xlab,ylab], bbox_inches='tight',dpi=150)
	plt.close()
	print('Wrote %s' % name)



def multi_plot(outdir='./'):
	#sns.set_palette('colorblind')
	sns.set_style('ticks')
	sns.set_style({"xtick.direction": "in","ytick.direction": "in"})
	colors= get_colors()
	fig,axes=plt.subplots(3,2)
	ax=axes.flatten()
	plt.subplots_adjust(wspace=0.2,hspace=0.4)
	for cnt,str_beta in enumerate(['bInf','b100','b10','b1','b1e-1','b1e-2']):
		tsteady= steady_state_begins(str_beta,True)
		print 'STEADY STATE: model=%s, t/tBH=%.2f, t/tABH=%.2f' % (str_beta,tsteady,tBH_to_tBHA(tsteady, str_beta))
		# Get data
		fns= glob('../processed_April242016_2/sd2/%s/lev*/master_sink_%s.pickle' % (str_beta,str_beta) )
		sinks={}
		for fn in fns:
			lev= fn[ fn.find('lev')+3 ]
			if lev == '2':
				fin=open(fn,'r')
				sinks[lev]=load(fin)
				fin.close()
				#corr mdots	
				end_t,end_mach= stats.t_rms_mach_near_end(str_beta)
				sinks[lev].rm_mdot_systematics(end_t,end_mach)
		plot_rates(ax[cnt],sinks,\
				  str_beta=str_beta,colors=colors,corr=True)
	ax[5].legend(loc='lower right')
	for cnt in [4,5]:
		xlab= ax[cnt].set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize=FS)
	for cnt in [0,2,4]:
		ylab=ax[cnt].set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_B }$',fontweight='bold',fontsize=FS)
	name=os.path.join(outdir,'convergence_multi.png' )
	plt.savefig(name,bbox_extra_artists=[xlab,ylab], bbox_inches='tight',dpi=150)
	plt.close()
	print('Wrote %s' % name)

def single_plot(str_beta='b1e-2',level='2',outdir='./'):
	#sns.set_palette('colorblind')
	#sns.set_style('ticks')
	#colors= sns.color_palette()
	colors= get_colors()
	fig,ax=plt.subplots()
	tsteady= steady_state_begins(str_beta,True)
	print 'STEADY STATE: model=%s, t/tBH=%.2f, t/tABH=%.2f' % (str_beta,tsteady,tBH_to_tBHA(tsteady, str_beta))
	# Get data
	fns= glob('../processed_April242016_2/sd2/%s/lev*/master_sink_%s.pickle' % (str_beta,str_beta) )
	sinks={}
	for fn in fns:
		lev= fn[ fn.find('lev')+3 ]
		if lev == level:
			fin=open(fn,'r')
			sinks[lev]=load(fin)
			fin.close()
			#corr mdots	
			end_t,end_mach= stats.t_rms_mach_near_end(str_beta)
			sinks[lev].rm_mdot_systematics(end_t,end_mach)
	plot_rates(ax,sinks,\
			  str_beta=str_beta,colors=colors,corr=True)
	ax.legend(loc='lower right')
	xlab= ax.set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='large')
	ylab=ax.set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_B }$',fontweight='bold',fontsize='large')
	name=os.path.join(outdir,'convergence_%s_lev%s.png' % (str_beta,level) )
	plt.savefig(name,bbox_extra_artists=[xlab,ylab], bbox_inches='tight',dpi=150)
	plt.close()
	print('Wrote %s' % name)




parser = argparse.ArgumentParser(description="test")
#parser.add_argument("-str_beta",choices=['bInf','bInfsd3','b100','b10','b1','b1e-1','b1e-2',],action="store",help='which model',required=True)
#parser.add_argument("-sinks",nargs=4,action="store",help='master sink.pickle file',required=True)
#parser.add_argument("-level",nargs=4,choices=['2','3','4','5',],action="store",help='directory containing parsed mdot files',required=True)
parser.add_argument("--outdir",action="store",help='directory to save plots to',required=True)
#parser.add_argument("-ylim",nargs=2,type=float,action="store",help='',required=False)
#parser.add_argument("-xlim",nargs=2,type=float,action="store",help='',required=False)
args = parser.parse_args()

#for str_beta in ['bInf','b100','b10','b1','b1e-1','b1e-2']:
#for str_beta in ['b1e-2']:
#	for lev in [2,3,4]:
#	for lev in [2,3]:
#		single_plot(str_beta=str_beta,level=str(lev),outdir=args.outdir)
#multi_plot(outdir=args.outdir)
col_plot(outdir=args.outdir)
#a=get_colors()
