import numpy as np
import argparse
import os
from pickle import dump,load
import matplotlib.pyplot as plt
from matplotlib import gridspec
from sys import exit
from scipy.stats import ttest_ind,ks_2samp
import seaborn as sns
sns.set_palette('colorblind')

from modules.sink.data_types import CombinedSinkData 
from modules.sink.rates_and_tscales import mdotB,mdotBH,get_tBH,steady_state_begins,tBH_to_tBHA,mark_to_kaylan_mdot,ic_rms_vals,lee_parallel,lee_perp,lee_avg_par_perp,ic_median_beta
import modules.sink.stats as stats
from modules.plot.multi_panels import multi_plot

def all_models_cdfs_pdfs(cdf,pdf,bins,args,nsinks=64):
	sns.set_style('whitegrid') #,{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	#kwargs
	laba=dict(fontweight='bold',fontsize='xx-large')
	#str_beta map
	addtext={}
	addtext['hydro_both']=r'$\mathbf{ \beta_0 = \infty \, (1+2) }$'
	addtext['b100']=r'$\mathbf{ \beta_0 = 100 }$'
	addtext['b10']=r'$\mathbf{ \beta_0 = 10 }$'
	addtext['b1']= r'$\mathbf{ \beta_0 = 1 }$'
	addtext['b1e-1']= r'$\mathbf{ \beta_0 = 0.1 }$'
	addtext['b1e-2']=r'$\mathbf{ \beta_0 = 0.01 }$'
	#plot
	fig,axes=plt.subplots(2,1,sharex=True)
	ax=axes.flatten()
	plt.subplots_adjust(hspace=0.1)
	colors= sns.color_palette() #color blind colors b/c set_palette() called above
	cnt=0
	for mod in cdf.keys():
		if mod == 'bInf' or mod == 'bInfsd3': continue
		ax[1].plot((bins[mod][1:]+bins[mod][:-1])/2,cdf[mod],c=colors[cnt],label=addtext[mod])
		ax[0].plot((bins[mod][1:]+bins[mod][:-1])/2,pdf[mod],c=colors[cnt],label=addtext[mod])
		cnt+=1
	ax[1].legend(loc=4,fontsize=10.,frameon=True,fancybox=True)
	#reorganize legend
	handles, labels = ax[1].get_legend_handles_labels()
	handles= handles[:2]+[handles[-1],handles[-2],handles[-3],handles[2]]
	labels= labels[:2]+[labels[-1],labels[-2],labels[-3],labels[2]]
	ax[1].legend(handles, labels,loc=4,fontsize=10.,frameon=True,fancybox=True)
	#
	ylab= ax[1].set_ylabel('CDF',**laba)
	ylab= ax[0].set_ylabel('PDF',**laba)
	xlab= ax[1].set_xlabel(r'$\mathbf{Log_{\rm{10}} \,\, \dot{M} }$',**laba)
	#ax[1].set_xlim(-7,-2.5)
	for i in range(2): 
		ax[i].set_xlim(-6.5,-2.5)
	ax[1].set_ylim(-0.1,1.1)
	ax[0].set_ylim([-0.1,ax[0].get_ylim()[-1]])
	plt.savefig(os.path.join(args.outdir,'all-models-cdfs-pdfs-nsinks-%d.png' % nsinks),\
				bbox_extra_artists=[xlab,ylab], bbox_inches='tight')
	plt.close()

FS=16
dFS=FS+4

def all_models_cdfs_pdfs2(cdf,pdf,bin_c,args,nsinks=64):
	'''bins -- linear spaced 10^val not val'''
	sns.set_style('whitegrid') #,{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	#kwargs
	laba=dict(fontweight='bold',fontsize=dFS)
	#str_beta map
	addtext={}
	addtext['hydro_both']=r'$\mathbf{ \beta_0 = \infty \, (1+2) }$'
	addtext['b100']=r'$\mathbf{ \beta_0 = 100 }$'
	addtext['b10']=r'$\mathbf{ \beta_0 = 10 }$'
	addtext['b1']= r'$\mathbf{ \beta_0 = 1 }$'
	addtext['b1e-1']= r'$\mathbf{ \beta_0 = 0.1 }$'
	addtext['b1e-2']=r'$\mathbf{ \beta_0 = 0.01 }$'
	#plot
	fig,axes=plt.subplots(2,1,sharex=True)
	ax=axes.flatten()
	plt.subplots_adjust(hspace=0.1)
	colors= sns.color_palette() #color blind colors b/c set_palette() called above
	cnt=0
	for mod in cdf.keys():
		if mod == 'bInf' or mod == 'bInfsd3': continue
		ax[1].plot(bin_c[mod]/mdotB(),cdf[mod],c=colors[cnt],label=addtext[mod])
		ax[0].plot(bin_c[mod]/mdotB(),pdf[mod],c=colors[cnt],label=addtext[mod])
		cnt+=1
	#ax[1].legend(loc=4,fontsize=FS,frameon=True,fancybox=True)
	#reorganize legend
	handles, labels = ax[1].get_legend_handles_labels()
	handles= handles[:2]+[handles[-1],handles[-2],handles[-3],handles[2]]
	labels= labels[:2]+[labels[-1],labels[-2],labels[-3],labels[2]]
	ax[1].legend(handles, labels,loc=(0.76,0.01),ncol=1,fontsize=FS-5,frameon=True,fancybox=True)
	#
	ylab= ax[1].set_ylabel('CDF',**laba)
	ylab= ax[0].set_ylabel('PDF',**laba)
	xlab= ax[1].set_xlabel(r'$\mathbf{\dot{M}/\dot{M}_{\rm{B}} }$',**laba)
	#ax[1].set_xlim(-7,-2.5)
	for i in range(2): ax[i].set_xlim(1e-5,1e-1)
	ax[1].set_ylim(-0.1,1.1)
	ax[0].set_ylim([-0.1,ax[0].get_ylim()[-1]])
	for i in range(2): 
		ax[i].set_xscale('log')
		ax[i].tick_params(axis='both',labelsize=FS)
	plt.savefig(os.path.join(args.outdir,'all-models-cdfs-pdfs-nsinks-%d-2.png' % nsinks),\
				bbox_extra_artists=[xlab,ylab], bbox_inches='tight',dpi=150)
	plt.close()


def model_v_model_cdfs_pdfs(arr,bins,cdf,pdf, args,nsinks=64):
	done=[]
	for ref_key in arr.keys():
		for key in arr.keys():
			if ref_key == key: continue
			#if (ref_key != 'bInf' and ref_key != 'bInfsd3') and (key != 'bInf' and key != 'bInfsd3'): continue
			#if ref_key != 'hydro_both' and key != 'hydro_both': continue
			if '%s_v_%s' % (ref_key,key) in done or '%s_v_%s' % (key,ref_key) in done: continue
			done.append( '%s_v_%s' % (ref_key,key) )
			t= ttest_ind(arr[ref_key],arr[key], equal_var=False)
			ks= ks_2samp(arr[ref_key],arr[key])	
			#plot histograms and show Welch t, KS p-values
			fig,axes=plt.subplots(2,1,sharex=True)
			ax=axes.flatten()
			plt.subplots_adjust(hspace=0.1)
			ax[0].plot((bins[ref_key][1:]+bins[ref_key][:-1])/2,cdf[ref_key],c='b',label='%s' % ref_key)
			ax[0].plot((bins[key][1:]+bins[key][:-1])/2,cdf[key],c='k',label='%s' % key)
			ax[1].plot((bins[ref_key][1:]+bins[ref_key][:-1])/2,pdf[ref_key],c='b',label='%s' % ref_key)
			ax[1].plot((bins[key][1:]+bins[key][:-1])/2,pdf[key],c='k',label='%s' % key)
			ax[1].legend(loc=0,fontsize='medium')
			ax[0].set_ylabel('CDF')
			ax[1].set_ylabel('PDF')
			ax[1].set_xlabel(r'$\dot{M}$')
			for i in range(2): 
				ax[i].set_xlim(-7,-2)
				ax[i].set_ylim(-0.1,1.1)
			plt.suptitle('%s_v_%s: Welch P(t)=%.2g, KS P(t)=%.2g' % (ref_key,key,t[1],ks[1]))
			plt.savefig(os.path.join(args.outdir,'%s-v-%s-nsinks-%d.png' % (ref_key,key,nsinks)))
			plt.close()
	#repeat for hydro1, hydro2, hydro 1+2
	t= ttest_ind(arr[ref_key],arr[key], equal_var=False)
	ks= ks_2samp(arr[ref_key],arr[key])
	#plot histograms and show Welch t, KS p-values
	fig,axes=plt.subplots(2,1,sharex=True)
	ax=axes.flatten()
	plt.subplots_adjust(hspace=0)
	for key,c in zip(['hydro_both','bInf','bInfsd3'],['b','y','k']):
		ax[0].plot((bins[key][1:]+bins[key][:-1])/2,cdf[key],c=c,label='%s' % key)
		ax[1].plot((bins[key][1:]+bins[key][:-1])/2,pdf[key],c=c,label='%s' % key)
	ax[1].legend(loc=0,fontsize='medium')
	ax[0].set_ylabel('CDF')
	ax[1].set_ylabel('PDF')
	ax[1].set_xlabel(r'$\dot{M}$')
	for i in range(2): ax[i].set_xlim(-6,-2.5)
	plt.savefig(os.path.join(args.outdir,'hydro1+2-v-hydro1-v-hydro2-nsinks-%d.png' % nsinks))
	plt.close()

def put_errbars_in_arrays(final):
	'''final is dict returned by final_measurement()
	vals are stored in dicts, want order them into arrays'''
	name,rms_beta,med_beta,yax_val,med,med_low,med_hi,std_low,std_hi= [],[],[],[],[],[],[],[],[]
	mods= ['bInf', 'bInfsd3', 'b100','b10','b1','b1e-1', 'b1e-2']
	for cnt,key in enumerate(mods[::-1]):
		name.append( key)
		rms_va,rms_b= ic_rms_vals(key)
		rms_beta.append( rms_b )
		med_beta.append( ic_median_beta(key)  )
		yax_val.append( cnt+1)
		#median and +/- sigma using quartiles
		med.append( final[key]['final']['med'] )
		med_low.append( final[key]['final']['med_low'] )
		med_hi.append( final[key]['final']['med_hi'] )
		#width mdot distributing from +/- quartiles
		std_low.append( final[key]['std_pdf']['low'] )
		std_hi.append( final[key]['std_pdf']['hi'] ) 
	return np.array(med),np.array(med_low),np.array(med_hi),np.array(std_low),np.array(std_hi), np.array(yax_val),np.array(rms_beta),np.array(med_beta),name

def put_errbars_in_arrays_linear(final):
	'''DON'T TAKE LOG10 of mdot IN THIS VERSION
	final is dict returned by final_measurement()
	vals are stored in dicts, want order them into arrays'''
	name,rms_beta,med_beta,yax_val,med,med_low,med_hi,std_low,std_hi= [],[],[],[],[],[],[],[],[]
	mods= ['bInf', 'bInfsd3', 'b100','b10','b1','b1e-1', 'b1e-2']
	print 'put_errbars_in_arrays_linear: mods= ',mods
	print 'put_errbars_in_arrays_linear: final.keys()= ',final.keys()
	for cnt,key in enumerate(mods[::-1]):
		name.append( key)
		rms_va,rms_b= ic_rms_vals(key)
		rms_beta.append( rms_b )
		med_beta.append( ic_median_beta(key)  )
		yax_val.append( cnt+1)
		#median and +/- sigma using quartiles
		med.append( final[key]['final']['med'] )
		med_low.append( final[key]['final']['med_low'] )
		med_hi.append( final[key]['final']['med_hi'] )
		#width mdot distributing from +/- quartiles
		std_low.append( final[key]['std_pdf']['low'] )
		std_hi.append( final[key]['std_pdf']['hi'] ) 
	return np.array(med),np.array(med_low),np.array(med_hi),np.array(std_low),np.array(std_hi), np.array(yax_val),np.array(rms_beta),np.array(med_beta),name


def box_and_whisker(ax, med,sem,std,yloc,boxw=0.2,legend_lab_ea_color=False):
	sem_low,sem_hi= sem
	std_low,std_hi= std
	#get colors
	sns.set_palette('colorblind')
	c_med='k' #median
	c_sem=sns.color_palette()[2] #error on median box 'r', bootstraped
	c_std= sns.color_palette()[0] #std dev of pdf 'b', percentiled
	#legend labels (if called)
	ll= ['Median','Std. Error of Median','Std. Dev. of PDF']
	#std error med box
	if legend_lab_ea_color: 
		print '-------------------------- adding legend -------------'
		ax.plot([med]*2,[yloc-boxw,yloc+boxw],c=c_med,label=ll[0])
	else: ax.plot([med]*2,[yloc-boxw,yloc+boxw],c=c_med)
	ax.plot([med-sem_low]*2,[yloc-boxw,yloc+boxw],c=c_sem)
	ax.plot([med+sem_hi]*2,[yloc-boxw,yloc+boxw],c=c_sem)
	ax.plot([med-sem_low,med+sem_hi],[yloc-boxw]*2,c=c_sem)
	if legend_lab_ea_color: ax.plot([med-sem_low,med+sem_hi],[yloc+boxw]*2,c=c_sem,label=ll[1])
	else: ax.plot([med-sem_low,med+sem_hi],[yloc+boxw]*2,c=c_sem)
	#pdf std dev
	ax.plot([med-std_low,med-sem_low],[yloc]*2,c=c_std)
	ax.plot([med+sem_hi,med+std_hi],[yloc]*2,c=c_std)
	ax.plot([med-std_low]*2,[yloc-boxw,yloc+boxw],c=c_std)
	if legend_lab_ea_color: ax.plot([med+std_hi]*2,[yloc-boxw,yloc+boxw],c=c_std,label=ll[2])
	else: ax.plot([med+std_hi]*2,[yloc-boxw,yloc+boxw],c=c_std)

def model_vs_errorbars(final64):
	'''put model names on y axis, error bars on x axis'''
	sns.set_style('whitegrid') #ticks',{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	c_med='k' #median
	c_sem=sns.color_palette()[2] #error on median box 'r', bootstraped
	c_std= sns.color_palette()[0] #std dev of pdf 'b', percentiled
	#kwargs
	laba=dict(fontweight='bold',fontsize='xx-large')
	kwargs_axtext=dict(fontweight='bold',fontsize='x-large',va='top',ha='left')
	#mapping
	addtext={}
	addtext['bInf']=r'$\mathbf{ \beta_0 = \infty }$ (1)'
	addtext['bInfsd3']=r'$\mathbf{ \beta_0 = \infty }$ (2)'
	addtext['b100']=r'$\mathbf{ \beta_0 = 100 }$'
	addtext['b10']=r'$\mathbf{ \beta_0 = 10 }$'
	addtext['b1']= r'$\mathbf{ \beta_0 = 1 }$'
	addtext['b1e-1']= r'$\mathbf{ \beta_0 = 0.1 }$'
	addtext['b1e-2']=r'$\mathbf{ \beta_0 = 0.01 }$'
	#plot
	fig,ax=plt.subplots()
	ax.set_ylim(0,8)
	ylabels = [item.get_text() for item in ax.get_yticklabels()] #y tick labels
	meds,med_lows,med_his,std_lows,std_his,yax_vals,rms_betas,med_betas,names= put_errbars_in_arrays(final64)
	for med,med_low,med_hi,std_low,std_hi,yax_val,rms_beta,med_beta,name in zip(meds,med_lows,med_his,std_lows,std_his,yax_vals,rms_betas,med_betas,names): 
		if name == names[-1]: box_and_whisker(ax, med,(med_low,med_hi),(std_low,std_hi),yax_val,legend_lab_ea_color=True)
		else: box_and_whisker(ax, med,(med_low,med_hi),(std_low,std_hi),yax_val)
		ylabels[yax_val]= addtext[name]	
	ax.set_yticklabels(ylabels,fontsize='large')
	xlab=ax.set_xlabel(r'$\mathbf{Log_{\rm{10}} \,\, \dot{M} }$',**laba)
	ylab=ax.set_ylabel('Simulation',**laba)
	ax.legend(loc=4,fontsize=10,frameon=True,fancybox=True)
	plt.tick_params(axis='both', which='major', labelsize='large')
	#turn off horizontal grid lines
	ax.yaxis.grid(False)
	#remove top and right axis lines
	#sns.despine()
	#save
	plt.savefig(os.path.join(args.outdir,'models-vs-errorbars.png'),\
				bbox_extra_artists=[xlab,ylab], bbox_inches='tight')

def model_vs_errorbars2(final64):
	'''final64 is linear mdots now
	put model names on y axis, error bars on x axis'''
	sns.set_style('ticks') #,{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	c_med='k' #median
	c_sem=sns.color_palette()[2] #error on median box 'r', bootstraped
	c_std= sns.color_palette()[0] #std dev of pdf 'b', percentiled
	#kwargs
	laba=dict(fontweight='bold',fontsize='xx-large')
	kwargs_axtext=dict(fontweight='bold',fontsize='x-large',va='top',ha='left')
	#mapping
	addtext={}
	addtext['bInf']=r'$\mathbf{ \beta_0 = \infty }$ (1)'
	addtext['bInfsd3']=r'$\mathbf{ \beta_0 = \infty }$ (2)'
	addtext['b100']=r'$\mathbf{ \beta_0 = 100 }$'
	addtext['b10']=r'$\mathbf{ \beta_0 = 10 }$'
	addtext['b1']= r'$\mathbf{ \beta_0 = 1 }$'
	addtext['b1e-1']= r'$\mathbf{ \beta_0 = 0.1 }$'
	addtext['b1e-2']=r'$\mathbf{ \beta_0 = 0.01 }$'
	#plot
	fig,ax=plt.subplots()
	ax.set_ylim(0,8)
	ax.set_xlim(1e-4,2e-2)
	ylabels = [item.get_text() for item in ax.get_yticklabels()] #y tick labels
	meds,med_lows,med_his,std_lows,std_his,yax_vals,rms_betas,med_betas,names= put_errbars_in_arrays_linear(final64)
	for med,med_low,med_hi,std_low,std_hi,yax_val,rms_beta,med_beta,name in zip(meds,med_lows,med_his,std_lows,std_his,yax_vals,rms_betas,med_betas,names): 
		if name == names[-1]: box_and_whisker(ax, med/mdotB(),(med_low/mdotB(),med_hi/mdotB()),(std_low/mdotB(),std_hi/mdotB()),yax_val,legend_lab_ea_color=True)
		else: box_and_whisker(ax, med/mdotB(),(med_low/mdotB(),med_hi/mdotB()),(std_low/mdotB(),std_hi/mdotB()),yax_val)
		ylabels[yax_val]= addtext[name]	
	ax.set_yticklabels(ylabels,fontsize='large')
	xlab=ax.set_xlabel(r'$\mathbf{\dot{M}/\dot{M}_{\rm{B}} }$',**laba)
	ylab=ax.set_ylabel('Simulation',**laba)
	ax.legend(loc=4,fontsize=10,frameon=True,fancybox=True)
	plt.tick_params(axis='x', which='major', labelsize='large')
	#turn off horizontal grid lines
	ax.yaxis.grid(False)
	ax.xaxis.grid(True)
	ax.set_xscale('log')
	ax.yaxis.tick_left()
	#ax.set_yticks([])
	#remove top and right axis lines
	#sns.despine()
	#save
	plt.savefig(os.path.join(args.outdir,'models-vs-errorbars2.png'),\
				bbox_extra_artists=[xlab,ylab], bbox_inches='tight')


def change_last_ticklabel(ax):
	labels=ax.get_xticks().tolist()
	print '-------ax2 labels=',labels,'------------'
	labels=np.array(labels).astype(str)
	labels[:]=''
	labels[-1]= r'$\mathbf{ \infty }$'
	ax.set_xticklabels(labels)
	#return labels

def errorbars_vs_beta(final64,use_rms_beta=True):
	sns.set_style('ticks') #,{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	c_med='k' #median
	c_bhline= sns.color_palette()[0] #'b'
	c_aaron=sns.color_palette()[2] #error on median box 'r'
	c_mark= sns.color_palette()[1] #'g'
	#kwargs
	laba=dict(fontweight='bold',fontsize='xx-large')
	kwargs_axtext=dict(fontweight='bold',fontsize='x-large',va='top')
	#final measure
	med,med_low,med_hi,std_low,std_hi,yax_val,rms_beta,med_beta,name= put_errbars_in_arrays(final64)
	#marks results
	mdot0= 0.0166
	mark_median_sim= mark_to_kaylan_mdot(0.35*mdot0)
	mark_pred_median_256= mark_to_kaylan_mdot(0.36*mdot0)
	mark_pred_median_err= 0.1 #sigma Mdot/Mdot0 ~ 0.1, Mdot0 is a constant so 0.1 error on Mdot
	#special figure for different sized subplots
	fig = plt.figure() 
	gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1])
	fig.subplots_adjust(wspace=0.05)
	ax=[0,0] 
	ax[0] = plt.subplot(gs[0])
	ax[1] = plt.subplot(gs[1])
	#B != 0 points go on left side
	ax[0].errorbar(rms_beta[:-2],med[:-2],yerr=[med_low[:-2],med_hi[:-2]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	ax[0].errorbar(rms_beta[:-2],med[:-2],yerr=[med_low[:-2],med_hi[:-2]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med,label=r'This Study (Median $\mathbf{Log_{\rm{10}} \,\, \dot{M} }$)')
	#just for legend, plot some junk with right colors for mine and marks
	#ax[0].plot([5,6],med[-2:],c=c_med,visible=False,label=r'This Study (Median $\mathbf{Log_{\rm{10}} \,\, \dot{M} }$)')
	#ax[0].plot([5,6],med[-2:],c=c_mark,visible=False,label='Krumholz et al. (2006)')
	#aarons
	cont_beta=np.logspace(-3,1,num=50)
	ax[0].plot(cont_beta,np.log10(lee_parallel(cont_beta)),c=c_aaron,ls='--',lw=2,label='Lee et al. (2014)')
	#print useful number for paper
	print 'ratio of mdot to (mdot_perp+mdot_par)/2 is: betarms,mdot,mdotpar,mdot/mdotpar'
	for myb,mymdot,mdotpar in zip(rms_beta[:-2],med[:-2],np.log10(lee_parallel(rms_beta[:-2]))): print myb,mymdot,mdotpar,10**(mdotpar)/10**(mymdot) 
	#mdot BH
	ax[0].plot(ax[0].get_xlim(),[np.log10(mdotBH())]*2,c=c_bhline,ls='--',lw=2,label=r'$\mathbf{ \dot{M}_{BH} }$')
	#ax[0].text(1e-2,np.log10(mdotBH()),r'$\mathbf{ \dot{M}_{BH} }$',color=c_bhline,**kwargs_ax[0].ext)
	#B = 0 points go on right side
	ax[1].plot(range(10),visible=False)
	#Marks prediction
	ax[1].errorbar(4,np.log10(mark_pred_median_256),yerr=mark_pred_median_err,fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	#my hydro pts
	ax[1].errorbar([5,6],med[-2:],yerr=[med_low[-2:],med_hi[-2:]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med)
	#mdotBH and same for hydro limit of lee14
	ax[1].plot(ax[1].get_xlim(),[np.log10(mdotBH())]*2,c=c_bhline,ls='--',lw=2)
	ax[1].plot([0.5,8.5],[np.log10(mdotBH())]*2,c=c_aaron,ls='--',lw=2) 
	#finish labeling
	for i in range(2): 
		ax[i].set_ylim(-4.8,-3.7)
	ax[0].set_xscale('log')
	ax[0].set_xlim(8e-3,1e1)
	#ax[1].set_xlim(2,1e1)
	ylab= ax[0].set_ylabel(r'$\mathbf{Log_{\rm{10}} \,\, \dot{M} }$',**laba)
	xlab= fig.text(0.5, 0., r'$\mathbf{ \beta_{rms} }$', ha='center', **laba) #ax[0].set_xlabel(r'$\mathbf{ \beta_{rms} }$',**laba)
	ax[0].legend(loc=4,fontsize='medium',frameon=True,fancybox=True)
	#reorganize legend
	handles, labels = ax[0].get_legend_handles_labels()
	handles= [handles[-1],handles[-2],handles[0],handles[1]]
	labels= [labels[-1],labels[-2],labels[0],labels[1]]
	ax[0].legend(handles, labels,loc=4,fontsize='medium',frameon=True,fancybox=True)
	#remove top and right axis lines
	#sns.despine()
	#remove spines and ticks between ax and ax2
	#ax[0].spines['right'].set_visible(False)
	#ax[1].spines['left'].set_visible(False)
	ax[1].set_yticks([])
	#add slashes indicating break in axis
	d = .015 # how big to make the diagonal lines in axes coordinates
	# arguments to pass plot, just so we don't keep repeating them
	kwargs = dict(transform=ax[0].transAxes, color='k', clip_on=False)
	ax[0].plot((1-d,1+d), (-d,+d), **kwargs)
	ax[0].plot((1-d,1+d),(1-d,1+d), **kwargs)
	kwargs.update(transform=ax[1].transAxes)  # switch to the bottom axes
	ax[1].plot((-d*3,+d*3), (1-d,1+d), **kwargs)
	ax[1].plot((-d*3,+d*3), (-d,+d), **kwargs)
	#put infinity on 2nd axis
	ax[1].set_xticks([])
	ax[1].set_xlabel(r'$\mathbf{ \infty }$',fontsize='large')
	plt.tick_params(axis='both', which='major', labelsize='large')
	#save
	plt.savefig(os.path.join(args.outdir,'errorbars_v_brms.png'),\
				bbox_extra_artists=[xlab,ylab], bbox_inches='tight')

def errorbars_vs_beta2(final64,use_rms_beta=True):
	'''all mdot quantities are in units of mdot/mdotB'''
	sns.set_style('ticks') #,{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	c_med='k' #median
	c_bhline= sns.color_palette()[0] #'b'
	c_aaron=sns.color_palette()[2] #error on median box 'r'
	c_mark= sns.color_palette()[1] #'g'
	#kwargs
	laba=dict(fontweight='bold',fontsize='xx-large')
	kwargs_axtext=dict(fontweight='bold',fontsize='x-large',va='top')
	#final measure
	med,med_low,med_hi,std_low,std_hi,yax_val,rms_beta,med_beta,name= put_errbars_in_arrays_linear(final64)
	#marks results
	rmdot0= 0.0166
	mark_norm_mdot0=dict(median_sim=0.35,median_pred_256=0.36,err_median_pred_256=0.1)
	mark_norm_mdotB={}
	for key in mark_norm_mdot0.keys():
		mark_norm_mdotB[key]= mark_norm_mdot0[key]* 4/np.exp(1.5)/5.**3
	#special figure for different sized subplots
	fig = plt.figure() 
	gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1])
	fig.subplots_adjust(wspace=0.05)
	ax=[0,0] 
	ax[0] = plt.subplot(gs[0])
	ax[1] = plt.subplot(gs[1])
	#B != 0 points go on left side
	ax[0].errorbar(rms_beta[:-2],med[:-2]/mdotB(),yerr=[med_low[:-2]/mdotB(),med_hi[:-2]/mdotB()], fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	ax[0].errorbar(rms_beta[:-2],med[:-2]/mdotB(),yerr=[med_low[:-2]/mdotB(),med_hi[:-2]/mdotB()], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med,label=r'This Study (Median)')
	#just for legend, plot some junk with right colors for mine and marks
	#ax[0].plot([5,6],med[-2:],c=c_med,visible=False,label=r'This Study (Median $\mathbf{Log_{\rm{10}} \,\, \dot{M} }$)')
	#ax[0].plot([5,6],med[-2:],c=c_mark,visible=False,label='Krumholz et al. (2006)')
	#aarons
	cont_beta=np.logspace(-3,1,num=50)
	ax[0].plot(cont_beta,lee_parallel(cont_beta,b_norm=True),c=c_aaron,ls='--',lw=2,label='Lee et al. (2014)')
	#print useful number for paper
	print 'ratio of mdot to (mdot_perp+mdot_par)/2 is: betarms,mdot,mdotpar,mdot/mdotpar'
	for myb,mymdot,mdotpar in zip(rms_beta[:-2],med[:-2],np.log10(lee_parallel(rms_beta[:-2]))): print myb,mymdot,mdotpar,10**(mdotpar)/10**(mymdot) 
	#mdot BH
	ax[0].plot(ax[0].get_xlim(),[mdotBH()/mdotB()]*2,c=c_bhline,ls='--',lw=2,label=r'$\mathbf{ \dot{M}_{BH} }$')
	#ax[0].text(1e-2,np.log10(mdotBH()),r'$\mathbf{ \dot{M}_{BH} }$',color=c_bhline,**kwargs_ax[0].ext)
	#B = 0 points go on right side
	ax[1].plot(range(10),visible=False)
	#Marks prediction
	ax[1].errorbar(4,mark_norm_mdotB['median_pred_256'],yerr=mark_norm_mdotB['err_median_pred_256'],fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	#my hydro pts
	ax[1].errorbar([5,6],med[-2:]/mdotB(),yerr=[med_low[-2:]/mdotB(),med_hi[-2:]/mdotB()], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med)
	#mdotBH and same for hydro limit of lee14
	ax[1].plot(ax[1].get_xlim(),[mdotBH()/mdotB()]*2,c=c_bhline,ls='--',lw=2)
	ax[1].plot([0.5,8.5],[mdotBH()/mdotB()]*2,c=c_aaron,ls='--',lw=2) 
	#finish labeling
	for i in range(2): 
		ax[i].set_ylim(6e-4,1e-2)
	ax[0].set_xscale('log')
	ax[0].set_xlim(8e-3,1e1)
	#ax[1].set_xlim(2,1e1)
	ylab= ax[0].set_ylabel(r'$\mathbf{\dot{M}/\dot{M}_{\rm{B}} }$',**laba)
	xlab= fig.text(0.5, 0., r'$\mathbf{ \beta_{rms} }$', ha='center', **laba) #ax[0].set_xlabel(r'$\mathbf{ \beta_{rms} }$',**laba)
	ax[0].legend(loc=4,fontsize='medium',frameon=True,fancybox=True)
	#reorganize legend
	handles, labels = ax[0].get_legend_handles_labels()
	handles= [handles[-1],handles[-2],handles[0],handles[1]]
	labels= [labels[-1],labels[-2],labels[0],labels[1]]
	ax[0].legend(handles, labels,loc=4,fontsize='medium',frameon=True,fancybox=True)
	#remove top and right axis lines
	#sns.despine()
	#remove spines and ticks between ax and ax2
	#ax[0].spines['right'].set_visible(False)
	#ax[1].spines['left'].set_visible(False)
	for i in range(2): ax[i].tick_params(axis='both', which='major', labelsize='large')
	ax[1].set_yticks([])
	#add slashes indicating break in axis
	d = .015 # how big to make the diagonal lines in axes coordinates
	# arguments to pass plot, just so we don't keep repeating them
	kwargs = dict(transform=ax[0].transAxes, color='k', clip_on=False)
	ax[0].plot((1-d,1+d), (-d,+d), **kwargs)
	ax[0].plot((1-d,1+d),(1-d,1+d), **kwargs)
	kwargs.update(transform=ax[1].transAxes)  # switch to the bottom axes
	ax[1].plot((-d*3,+d*3), (1-d,1+d), **kwargs)
	ax[1].plot((-d*3,+d*3), (-d,+d), **kwargs)
	#put infinity on 2nd axis
	ax[1].set_xticks([])
	ax[1].set_xlabel(r'$\mathbf{ \infty }$',fontsize='large')
	#plt.tick_params(axis='both', which='major', labelsize='large')
	# show only some of yaxis ticks
	#ax[0].yaxis.tick_left()
	ax[1].yaxis.tick_right()
	# y log scale
	for i in range(2): ax[i].set_yscale('log')
	#save
	plt.savefig(os.path.join(args.outdir,'errorbars_v_brms2.png'),\
				bbox_extra_artists=[xlab,ylab], bbox_inches='tight')



parser = argparse.ArgumentParser(description="test")
parser.add_argument("-sinks",nargs=7,action="store",help='master sink.pickle file',required=True)
parser.add_argument("-str_beta",nargs=7,choices=['bInf','bInfsd3','b100','b10','b1','b1e-1','b1e-2',],action="store",help='directory containing parsed mdot files',required=True)
parser.add_argument("-outdir",action="store",help='directory to save plots to',required=True)
parser.add_argument("-ylim",nargs=2,type=float,action="store",help='',required=False)
parser.add_argument("-xlim",nargs=2,type=float,action="store",help='',required=False)
args = parser.parse_args()


print 'BEFORE CALC, fixed t_steady: '
for str_beta in args.str_beta: 
	tsteady= steady_state_begins(str_beta,True)
	print 'STEADY STATE: model=%s, t/tBH=%.2f, t/tABH=%.2f' % (str_beta,tsteady,tBH_to_tBHA(tsteady, str_beta))

sinks={}
for fn,str_beta in zip(args.sinks,args.str_beta):
	fin=open(fn,'r')
	sinks[str_beta]=load(fin)
	fin.close()
	#corr mdots	
	end_t,end_mach= stats.t_rms_mach_near_end(str_beta)
	sinks[str_beta].rm_mdot_systematics(end_t,end_mach)
#deterine min and max mdot of all models
low,hi= 100,1.e-10
for key in sinks.keys():
	istart= stats.index_before_tBH(sinks[key], steady_state_begins(key,True))
	iend= stats.index_before_tBH(sinks[key], 20.)
	#print 'istart= ',istart,'iend= ',iend
	#print 'sinks[key].mdot.shae[istart:iend].shape= ',sinks[key].mdot[istart:iend].shape
	#print 'sinks[key].mdot[istart:iend].shape= ',sinks[key].mdot[istart:iend].shape
	gt_zero= sinks[key].mdot[:,istart:iend] > 0 #mdot < 0 when dt plummeted, huge vertical spike in mdot vs. t...
	new_low,new_hi= sinks[key].mdot[:,istart:iend][gt_zero].min(), sinks[key].mdot[:,istart:iend][gt_zero].max()
	print "model= %s: low,new_low,hi,new_hi= " % (key,),low,new_low,hi,new_hi
	low,hi= min(low,new_low),max(hi,new_hi)
print '----lowest and highest mdots for all models are: lo=',low,', hi=',hi,' ----' 
nsinks=64 #for nsinks in [8,16,32,64]: 
#first, store pdfs,cdfs b/c kaylans_pdf_one_model calls plot functions and messing up plot if make inside loop
final_pdf= dict(arr={},bins={},pdf={},cdf={})  #PDF USING, 64 pdf, over last 2tBH
for mod in sinks.keys():
	#PDF for each model
	final_pdf['arr'][mod],final_pdf['bins'][mod],final_pdf['pdf'][mod],final_pdf['cdf'][mod]= stats.pdf_for_model_64_x_N(sinks[mod],mod, low,hi,nsinks=nsinks,corr=True,last_2tBH=True,dt=2.,median=True)
	#if 128 PDF desired = stats.pdf_for_model_64_x_N(sinks[mod],mod, low,hi,nsinks=nsinks,corr=True,last_2tBH=True,dt=1.)

#combine the 2 hydro models
def combine_hydro(arr,bins,pdf,cdf): 
	arr['hydro_both'], bins['hydro_both']= np.concatenate([arr['bInf'],arr['bInfsd3']],axis=0), bins['bInf'] #bins same all models
	pdf['hydro_both'], cdf['hydro_both']= stats.get_pdf_cdf(arr['hydro_both'],bins['hydro_both']) 
combine_hydro(final_pdf['arr'],final_pdf['bins'],final_pdf['pdf'],final_pdf['cdf'])
#PLOTS
#all_models_cdfs_pdfs(final_pdf['cdf'],final_pdf['pdf'],final_pdf['bins'],args,nsinks=nsinks)

final_pdf_bin_c= {}
for key in final_pdf['bins'].keys():
	# log average to get centers then 10^log avg to get linear spacing
	final_pdf_bin_c[key]= np.power(10, (final_pdf['bins'][key][1:]+final_pdf['bins'][key][:-1])/2 )  
all_models_cdfs_pdfs2(final_pdf['cdf'],final_pdf['pdf'],final_pdf_bin_c,args,nsinks=nsinks)
raise ValueError
model_v_model_cdfs_pdfs(final_pdf['arr'],final_pdf['bins'],final_pdf['cdf'],final_pdf['pdf'], args,nsinks=nsinks)
#final measurement
#print 'Quantities using mdot= Log10(Mdot)'
#final_meas= stats.final_measurement(final_pdf['arr'])

# This gives correct Mdot/MdotB final measurements BUT the values CAN be computed from above log10(Mdot) like this
# q25,q50,q75 = above q25,q50,q75 / mdotB
# define: y= log10(mdot), x= mdot/mdotB
# then x = exp(y*ln10)/mdotB
# so sigma_x = sigma_y* exp(y*ln10)*ln10/mdotB)
# doing that agrees with numbers output below
final_pdf_linear,final_pdf_log= {},{}
for key in final_pdf['arr'].keys():
	#final_pdf_linear[key]= np.log10( np.power(10,final_pdf['arr'][key])/ mdotB() )
	final_pdf_linear[key]= np.power(10,final_pdf['arr'][key])/ mdotB()
	final_pdf_log[key]= np.log10( np.power(10,final_pdf['arr'][key])/ mdotB() )
print 'Quantities using mdot/mdotB, where mdotB=',mdotB()
final_meas_linear= stats.final_measurement(final_pdf_linear)
print 'Quantities using log10(mdot/mdotB), where mdotB=',mdotB()
final_meas_log= stats.final_measurement(final_pdf_log)

exit('exiting after print final mean measurements')

# The plots below used to work when mdot = mdot without mdotB normalization
# They don't know but can recreate by going to appropriate commmit #
for key in final_pdf_linear.keys():
	final_pdf_linear[key]= final_pdf_linear[key]*mdotB()

#plot model name vs. errobars, do for both final64 and final128 
model_vs_errorbars(final_meas) 
model_vs_errorbars2(final_meas_linear) 
#plot measurements vs. beta_rms
errorbars_vs_beta(final_meas,use_rms_beta=True)
errorbars_vs_beta2(final_meas_linear,use_rms_beta=True)
#errorbars_vs_beta(final_meas,use_rms_beta=False)
#save models pdfs
fout=open(os.path.join(args.outdir,'final_pdfs_and_measurements.pickle'), 'w')
dump((final_pdf,final_meas),fout)
fout.close()
print "finished"
exit(0)


