'''
RUN: python Draft_figs_allBeta.py -dirs b100/lev4 b10/lev4 b1/lev5 b0.1/lev5 b0.01/lev5 -labels 100 10 1 0.1 0.01 -cs 1 -mach 5 -rho0 1e-2 -mass0 0.40625 -nsinks 64 -Geq1 1 -ylim1 1e-4 2e-2 -ylim2 1e-4 2e-2 -steady_wid 3 8 1 3 0.5 -steady_end 21 40 12.5 13 3.5 -xlim 0 20

*above was used to calculate initial set (generated on Oct. 16,2015) of steady state median,mean mdot results for beta 100-0.01 

dependecies: 
    Sim_SinkData_EveryTStep.py -- creates the "mdots*.pickle" file 
input:
    "mdots*.pickle" file containing sink particle time,mass,mdot on finest-level
    general simulation info
output:
    plot of mdot vs. time on finest-level or sampled with user-defined frequency (eg 0.2*bondi time)
'''

import argparse
 

import matplotlib
#matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pickle
import glob 
import os
import sys
import mpld3
from matplotlib.ticker import MaxNLocator
import modules.constants as sc

from modules.sink.data_types import SinkData,CombinedSinkData
from modules.sink.rates_and_tscales import mdotB,get_tBH
from modules.plot.multi_panels import multi_plot

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-dirs",nargs=2,action="store",help='directories to plot all sink.pickle data from, each directory its own color')
parser.add_argument("-labels",nargs=2,choices=['bInf','b100','b10','b1','b01','b001'],action="store",help='labels for each dir color')
parser.add_argument("-init_sink",action="store",help='init.sink file used in simulations, mass wont be used only x,y,z,sid so any init.sink from sims is fine')
parser.add_argument("-save_name",action="store",help='prefix for filename save to')
#parser.add_argument("-cs",type=float,action="store",help='bondi hoyle time at instant insert sinks in simulation units')
#parser.add_argument("-mach",type=float,action="store",help='rms Mach number when sinks were inserted')
#parser.add_argument("-rho0",type=float,action="store",help='mean rho')
#parser.add_argument("-mass0",type=float,action="store",help='initial sink mass')
parser.add_argument("-nsinks",type=int,action="store",help='number of sinks')
parser.add_argument("-Geq1",action="store",help='set this to anything, say 1 if units of G = 1', required=False)
#optional
parser.add_argument("-krumholz_norm",action="store",help='set this to anything, say 1, to normalize to krumholz 2006 result for the accretion rate', required=False)
parser.add_argument("-boxcar",nargs=1,type=float,action="store",help='boxcar width in units tBH, ex) 0.02tB = 2.5tBH (if mach = 5) is in Cunningham 2012',required=False)
parser.add_argument("-steady_wid",nargs=1,type=float,action="store",help='steady WIDTH -- calc avg and std of mdot data over indices spanning  [end-widthy,end] in time',required=False)
parser.add_argument("-steady_end",nargs=1,type=float,action="store",help='steady END -- calc avg and std of mdot data over indices spanning  [end-width,end] in time',required=False)
parser.add_argument("-xlim",nargs=2,metavar=('low','hi'),type=float,action="store",help='mdot vs. time, time limits',required=False)
parser.add_argument("-ylim",nargs=2,metavar=('low','hi'),type=float,action="store",help='for median plot',required=False)
parser.add_argument("-ylim1",nargs=2,metavar=('low','hi'),type=float,action="store",help='for median plot',required=False)
parser.add_argument("-ylim2",nargs=2,metavar=('low','hi'),type=float,action="store",help='for mean plot',required=False)
parser.add_argument("-sids",nargs=9,type=int,action="store",help='sink IDs to plot individual sinks for',required=False)
parser.add_argument("-write_master",action="store",help='set to anythign to force new master_sink.pickle file to be written for each dir/',required=False)
#get args
args = parser.parse_args()
if args.steady_wid == None or args.steady_end == None: 
	nargs=5
	(args.steady_wid,args.steady_end)= ([-1]*nargs,[-1]*nargs) 
if args.boxcar == None: 
	nargs=5
	args.boxcar= [-1]*nargs

if args.Geq1: bigG= 1.
else: bigG= sc.G


args.cs=1.
args.mach=5.
args.rho0= 1.e-2
args.mass0= 13./32


def mdot_bondi(args):
    return 4*np.pi*args.rho0*(bigG*args.mass0)**2/args.cs**3

def mdot_krumholz(args):
	#Krumholz 2006 eqn 41, since table 1 par ~ perp and par > perp for large |B|
	return 4*np.pi*args.rho0*(bigG*args.mass0)**2/(args.mach*args.cs)**3

def mdot_bh(rho,msink,rmsV,args):
    return 4*np.pi*rho*np.power(bigG*msink,2)/np.power( np.power(rmsV,2)+args.cs**2, 1.5)

def lee_mach_bh(rmsV,args):
	lam=np.exp(1.5)/4
	return np.power( 1.+np.power(rmsV/args.cs,4), 1./3)/np.power( 1.+np.power(rmsV/args.cs/lam,2), 1./6)

def mdot_lee_const(sink,args):
	#Lee 2014 eqn 41, since table 1 par ~ perp and par > perp for large |B|
	bh= mdot_bh(sink.glob_meanRho[0],sink.mass[0,0],sink.glob_rmsV[0],args)
	mach_bh= lee_mach_bh(sink.glob_rmsV[0],args)
	inv_beta= sink.glob_rmsVa[0]**2/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_lee_glob_ft_V(sink,args):
	bh= mdot_bh(sink.glob_meanRho[0],sink.mass[0,0],sink.glob_rmsV,args)
	mach_bh= lee_mach_bh(sink.glob_rmsV,args)
	inv_beta= sink.glob_rmsVa[0]**2/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_lee_glob_ft_Va(sink,args):
	bh= mdot_bh(sink.glob_meanRho[0],sink.mass[0,0],sink.glob_rmsV[0],args)
	mach_bh= lee_mach_bh(sink.glob_rmsV[0],args)
	inv_beta= sink.glob_rmsVa**2/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_lee_glob_ft_Rho(sink,args):
	bh= mdot_bh(sink.glob_meanRho,sink.mass[0,0],sink.glob_rmsV[0],args)
	mach_bh= lee_mach_bh(sink.glob_rmsV[0],args)
	inv_beta= sink.glob_rmsVa[0]**2/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)
	
def mdot_lee_local_ft_V(sink,args):
	#multi/division of numpy arrays works by broadcast if: shape*shape = (10000,64)*(64)
	bh= mdot_bh(sink.meanRho[0,:],sink.mass,sink.rmsV,args)
	mach_bh= lee_mach_bh(sink.rmsV,args)
	inv_beta= np.power(sink.rmsVa[0,:],2)/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_lee_local_ft_Va(sink,args):
	#multi/division of numpy arrays works by broadcast if: shape*shape = (10000,64)*(64)
	bh= mdot_bh(sink.meanRho[0,:],sink.mass,sink.rmsV[0,:],args)
	mach_bh= lee_mach_bh(sink.rmsV[0,:],args)
	inv_beta= np.power(sink.rmsVa,2)/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_lee_local_ft_Rho(sink,args):
	#multi/division of numpy arrays works by broadcast if: shape*shape = (10000,64)*(64)
	bh= mdot_bh(sink.meanRho,sink.mass,sink.rmsV[0,:],args)
	mach_bh= lee_mach_bh(sink.rmsV[0,:],args)
	inv_beta= np.power(sink.rmsVa[0,:],2)/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_lee_local_ft_VVaRho(sink,args):
	#multi/division of numpy arrays works by broadcast if: shape*shape = (10000,64)*(64)
	bh= mdot_bh(sink.meanRho,sink.mass,sink.rmsV,args)
	mach_bh= lee_mach_bh(sink.rmsV,args)
	inv_beta= np.power(sink.rmsVa,2)/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

class Mdots():
    def __init__(self,args):
		self.const= mdot_bondi(args) #mdot_krumholz_const(sink,args)
			#self.const= mdot_lee_const(sink,args)
			#self.glob_ft_V= mdot_lee_glob_ft_V(sink,args)
			#self.glob_ft_Va= mdot_lee_glob_ft_Va(sink,args)
			#self.glob_ft_Rho= mdot_lee_glob_ft_Rho(sink,args)
			#self.local_ft_V= mdot_lee_local_ft_V(sink,args)
			#self.local_ft_Va= mdot_lee_local_ft_Va(sink,args)
			#self.local_ft_Rho= mdot_lee_local_ft_Rho(sink,args)
			#self.local_ft_VVaRho= mdot_lee_local_ft_VVaRho(sink,args)


def resampleMdot(sink,t_sample):
    '''sample finest level Mdot by t_sample'''
    sample= sink.time/t_sample
    imax=int(sample.max())
    imin=int(sample.min())
    ind=[]
    #if only have mdot info for some initial time > 0, must start from i = imin not i= 0 
    for i in (np.arange(imin,imax)+1):
        ind.append(np.where(sample < float(i))[0][-1])
    ind=np.array(ind)
    tbin= sink.time[ind]
    mbin= sink.mass[ind,:]
    mdotFine= np.zeros(mbin.shape)-1
    tFine= np.zeros(tbin.shape)-1
    newMdot[0,:]=0.
    tFine[0]= tbin[0]
    for cnt in np.arange(1,mbin.shape[0]):
        deltaM= mbin[cnt,:] -mbin[cnt-1,:] 
        deltaT= tbin[cnt]- tbin[cnt-1]
        mdotFine[cnt,:]= deltaM/deltaT
        tFine[cnt]= tbin[cnt-1]+deltaT/2.
    return (tFine,mdotFine)

def isMonotonic(t):
    '''t is array-like, raise error if t is not a monotoonly increasing'''
    ibad=np.where(t[1:]-t[:-1] <= 0.)[0]
    #print "ibad= ",ibad
    #print "time ibad= ",t[ibad]
    if ibad.size > 0: raise ValueError
    else: pass

def Boxcar(t,y,wid,units_tBH=True):
	'''t,y are array-like. t is dependent variable, y is independent var we are taking boxcar average of.
	wid is width of boxcar average
	returns: tuplee of new boxcar averaged t,y '''
	print "computing a BoxCar average for Mdot info"
	isMonotonic(t)
	if not units_tBH: #false (so not evaluated) unless units_tBHA set to False
		raise ValueError
	new_mdot=[]
	new_t= []
	low=t.min()
	hi=t.min()+wid
	cnt= 1
	while (low < t.max()):
		imin=np.where(t >= low)[0]
		imax=np.where(t < hi)[0]
		if len(imin) == 0 or len(imax) == 0: #did not find value between low and hi
			cnt+=1
			hi= t.min()+cnt*wid  #increase hi so can find value between original low and new hi
			if hi > t.max():  #no data in [end-small,end), okay if "small" is indeed small 
				break
		else: #found values continue as normal
			imin=imin.min()
			imax=imax.max()
			ind=np.arange(imin,imax+1,1)
			if y[ind].size == 0:
				#hack, even with check on len(imin,imax) above, y[ind] can be empty with right choice of boxcar wid
				cnt+=1
				low=hi
				hi= t.min()+cnt*wid
				if (low + wid >= t.max()): hi= t.max()
				continue #print "###y[ind]= ",y[ind]
			new_mdot.append( np.median(y[ind]) )
			new_t.append( (t[imin]+t[imax])/2 )
			cnt+=1
			low=hi
			hi= t.min()+cnt*wid
			if (low + wid >= t.max()): hi= t.max()
	return (np.array(new_t),np.array(new_mdot))


def plot_Mdots_lee14(simul,tBHA,mdot,args):
    fig,ax= plt.subplots() #1,2) #,figsize=(10,3))
    #ax= axis.flatten()
    time= simul.time/tBHA  
    lab=""
    if args.avgOver: lab +="mean= %.2f (%.1f-%.1f tB)" % (simul.avg/mdot.norm/args.answer,args.avgOver[0],args.avgOver[1])
    if args.boxcar: lab += " (boxcar %.1g tB)" % args.boxcar
    ax.plot(time,simul.mdot/mdot.norm/args.answer,"k-",label=lab)
    #can plot average mdot if > 1 sink particle 
    #ax.plot(time,np.average(simul.mdot,axis=1)/mdot.norm,"k-",label='K Mean')
    #ax.plot(time,np.median(simul.mdot,axis=1)/mdot.norm,"k--",label='K Median')
    ax.set_xlabel("t / %s" % args.tBHA)
    ylab= r"$\mathbf{\dot{M}}$"+" /%s/$\dot{M}_{Ramses}$" % args.mdotBHA
    #below doesn't work, not sure why
    if args.freq_sample: ylab += " (Sampled every %.1f tB)" % args.freq_sample
    ax.set_ylabel(ylab)
    ax.set_yscale('log')
    ax.legend(loc=4)
    if args.ylim: ax.set_ylim(args.ylim[0],args.ylim[1])
    if args.xlim: ax.set_xlim(args.xlim[0],args.xlim[1])
    if args.LinearY: ax.set_yscale('linear')
    #leg_box=ax.legend(loc=(1.01,0.01),ncol=1)
    if args.fname: fsave= "lee14_mdot_"+args.fname+".png"
    else: fsave="lee14_mdot.png"
    plt.savefig(fsave) #bbox_extra_artists=(leg_box,), bbox_inches='tight')
    plt.close()

def plot_krum06_avgMdot(sink,mdot,tBHA,args):
	#prep plot
	f,axis=plt.subplots(2,2,sharex=True,figsize=(15,10))
	plt.subplots_adjust( hspace=0,wspace=0 )
	ax=axis.flatten()
	#calc mean and median Mdot
	avg= np.average(sink.MdotFine/mdot.lee_const,axis=1)
	med= np.median(sink.MdotFine/mdot.lee_const,axis=1)
	avg_ft= np.average(sink.MdotFine,axis=1)/mdot.lee_ft #mdot.lee_ft has len = n time points
	avg_ft_environ= np.average(sink.MdotFine/mdot.lee_ft_environ,axis=1)
	med_ft= np.median(sink.MdotFine,axis=1)/mdot.lee_ft #mdot.lee_ft has len = n time points
	med_ft_environ= np.median(sink.MdotFine/mdot.lee_ft_environ,axis=1)
	time= (sink.time-sink.time[0])/tBHA
	if args.boxcar:
		#'time' returned for each below are all the same b/c input sink.time are the same 
		(time,avg)= Boxcar((sink.time-sink.time[0])/tBHA,avg,args.boxcar*tBHA,units_tBHA=True)
		(time,med)= Boxcar((sink.time-sink.time[0])/tBHA,med,args.boxcar*tBHA,units_tBHA=True)
		(time,avg_ft)= Boxcar((sink.time-sink.time[0])/tBHA,avg_ft,args.boxcar*tBHA,units_tBHA=True)
		(time,avg_ft_environ)= Boxcar((sink.time-sink.time[0])/tBHA,avg_ft_environ,args.boxcar*tBHA,units_tBHA=True)
		(time,med_ft)= Boxcar((sink.time-sink.time[0])/tBHA,med_ft,args.boxcar*tBHA,units_tBHA=True)
		(time,med_ft_environ)= Boxcar((sink.time-sink.time[0])/tBHA,med_ft_environ,args.boxcar*tBHA,units_tBHA=True)   
	#MEDIAN - right panels
	ax[0].plot(time,med,'k-',lw=2)    
	ax[0].plot(time,med_ft,'b-',lw=2)    
	ax[0].plot(time,med_ft_environ,'g-',lw=2)  
	ax[2].plot(time,med,'k-',lw=2)  
	ax[2].plot(time,med_ft,'b-',lw=2)
	ax[2].plot(time,med_ft_environ,'g-',lw=2)    
	#MEAN - left panels
	ax[1].plot(time,avg,'k-',lw=2)  
	ax[1].plot(time,avg_ft,'b-',lw=2)    
	ax[1].plot(time,avg_ft_environ,'g-',lw=2)    
	ax[3].plot(time,avg,'k-',lw=2,label=r"$\dot{M}=$ const")    
	ax[3].plot(time,avg_ft,'b-',lw=2,label=r"$\dot{M}=$ f(t)")    
	ax[3].plot(time,avg_ft_environ,'g-',lw=2,label=r"$\dot{M}=$ f(t,sink)") 
	#annotate plot
	ax[0].set_title("Median")
	ax[1].set_title("Mean")
	ax[0].set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_0 }$')
	ax[2].set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_0 }$')
	ax[2].set_xlabel(r"$\mathbf{ t/t_{BHA} }$")
	ax[3].set_xlabel(r"$\mathbf{ t/t_{BHA} }$")
	if args.boxcar: plt.subtitle("boxcar %.2g tBHA" % args.boxcar)
	ax[3].legend(loc=4,fontsize='medium')
	#scaling
	for i in [0,1]: ax[i].set_yscale('log')
	for i in [2,3]: ax[i].set_yscale('linear')
	for a in ax:
		if args.ylim: a.set_ylim(args.ylim[0],args.ylim[1])
		if args.xlim: a.set_xlim(args.xlim[0],args.xlim[1])
	if args.fname: fsave= "median_mean_mdot_"+args.fname+".png"
	else: fsave="median_mean_mdot.png"
	plt.savefig(fsave,dpi=150)
	plt.close() 

def plot_3x1_8sinks_local_stats(sid_indices,fname, sink,mdot,tBH,args):
	fig,axis=plt.subplots(1,3,sharex=True,figsize=(18,7))
	plt.subplots_adjust( hspace=0,wspace=0 )
	ax=axis.flatten()
	colors=['k','b','g','r','c','m','y'] +['#33CCFF']
	lw= 7*[2.]+[2.]
	#plot rmsV,rmsVa,meanRho on Left Vertical panels (ax1-ax3)
	#indiv sink mdots on Right Vertical Panels (ax4)
	time= (sink.time-sink.time[0])/tBH
	for i,ind in enumerate(sid_indices):
		ax[0].plot(time,sink.meanRho[:,ind],ls='-',c=colors[i],lw=lw[i])
		ax[1].plot(time,sink.rmsV[:,ind],ls='-',c=colors[i],lw=lw[i])
		ax[2].plot(time,2.*args.cs**2*np.power(sink.rmsVa[:,ind],-2),ls='-',c=colors[i],lw=lw[i],label='local,sid='+str(sink.sid[:,ind][0]))
	#plot global averaged values
	ax[0].plot(time,sink.glob_meanRho,c='0.8',ls='--',lw=4.)
	ax[1].plot(time,sink.glob_rmsV,c='0.8',ls='--',lw=4.)
	ax[2].plot(time,2.*args.cs**2*np.power(sink.glob_rmsVa,-2),c='0.8',ls='--',lw=4.,label="global")
	#finish plot
	ax[0].set_ylabel(r"$\mathbf{ \bar{\rho} }$",fontweight='bold',fontsize='x-large')
	ax[1].set_ylabel(r"$\mathbf{ V_{RMS} }$",fontweight='bold',fontsize='x-large')
	ax[2].set_ylabel(r"$\mathbf{ \beta_{RMS} }$",fontweight='bold',fontsize='x-large')
	ax[2].legend(loc=(1.01,0),fontsize='medium',ncol=1)
	plt.savefig('local_8sinks_'+fname+'.png')
	plt.close()

def plot_3x1_glob_vs_time(sink,tBH,args):
	fig,axis= plt.subplots(3,1,sharex=True)#,sharey=True)
	fig.set_size_inches(15., 8.)
	ax=axis.flatten()
	fig.subplots_adjust(hspace=0,wspace=0) 
	time= (sink.time-sink.time[0])/tBH
	ax[0].plot(time,sink.glob_meanRho,'k-',lw=2)
	ax[1].plot(time,sink.glob_rmsV,'k-',lw=2)
	ax[2].plot(time,sink.glob_rmsVa,'k-',lw=2)
	ax[2].set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	ax[0].set_ylabel(r'global $\mathbf{ \bar{\rho} }$',fontweight='bold',fontsize='xx-large')
	ax[1].set_ylabel(r'global $\mathbf{ V_{\rm{RMS}} }$',fontweight='bold',fontsize='xx-large')
	ax[2].set_ylabel(r'global $\mathbf{ V_{\rm{A, RMS}} }$',fontweight='bold',fontsize='xx-large')
	plt.savefig('global_vs_time.png')
	plt.close()	

def plot_2x2_Mdot_globalNorm(sink,mdot,tBH,args):
	#set up plot
	fig,axis= plt.subplots(2,2,sharex=True)#,sharey=True)
	fig.set_size_inches(15., 8.)
	ax=axis.flatten()
	fig.subplots_adjust(hspace=0,wspace=0) 
	#
	colors=['k','b','g','r','c','m','y']
	time= (sink.time-sink.time[0])/tBH
	#MEDIAN - left panels
	for i in [0,2]: #top, bottom r log and linear scaled but otherwise identical	
		ax[i].plot(time,np.median(sink.MdotFine/mdot.const,axis=1),c=colors[0],ls='-',lw=2.)
		ax[i].plot(time,np.median(sink.MdotFine,axis=1)/mdot.glob_ft_V,c=colors[1],ls='-',lw=2.)
		ax[i].plot(time,np.median(sink.MdotFine,axis=1)/mdot.glob_ft_Va,c=colors[2],ls='-',lw=2.)
		ax[i].plot(time,np.median(sink.MdotFine,axis=1)/mdot.glob_ft_Rho,c=colors[3],ls='-',lw=2.)
		#ax[i].plot(time,np.median(sink.MdotFine/mdot.local_ft,axis=1),c=colors[3],ls='-',lw=2.)
		#ax[i].plot(time,np.median(sink.MdotFine/mdot.local_ft_andRho,axis=1),c=colors[4],ls='-',lw=2.)
	#MEAN - right panels	
	for i in [1,3]: #top, bottom r log and linear scaled but otherwise identical	
		ax[i].plot(time,np.mean(sink.MdotFine/mdot.const,axis=1),c=colors[0],ls='-',lw=2.,label='const')
		ax[i].plot(time,np.mean(sink.MdotFine,axis=1)/mdot.glob_ft_V,c=colors[1],ls='-',lw=2.,label='glob_ft_V')
		ax[i].plot(time,np.mean(sink.MdotFine,axis=1)/mdot.glob_ft_Va,c=colors[2],ls='-',lw=2.,label='glob_ft_Va')
		ax[i].plot(time,np.mean(sink.MdotFine,axis=1)/mdot.glob_ft_Rho,c=colors[3],ls='-',lw=2.,label='glob_ft_Rho')
		#ax[i].plot(time,np.mean(sink.MdotFine/mdot.local_ft,axis=1),c=colors[3],ls='-',lw=2.,label='local_ft')
		#ax[i].plot(time,np.mean(sink.MdotFine/mdot.local_ft_andRho,axis=1),c=colors[4],ls='-',lw=2.,label='local_ft_andRho')
	#y label, left vertical panels
	ax[0].set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_{\rm{NORM}} }$',fontweight='bold',fontsize='xx-large')
	ax[2].set_ylabel(r'Linear $\mathbf{ \dot{M}/\dot{M}_{\rm{NORM}} }$',fontweight='bold',fontsize='xx-large')
	for i in [2,3]: ax[i].set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	for i in [0,1]: ax[i].set_yscale('log')
	for i in [2,3]: ax[i].set_yscale('linear')
	#legends
	ax[3].legend(loc=4,fontsize='small',ncol=1)
	#titles
	ax[0].set_title('Median',fontweight='bold',fontsize='xx-large')
	ax[1].set_title('Mean',fontweight='bold',fontsize='xx-large')
	plt.savefig('mdot_2x2_globalNorms.png')
	plt.close()

def plot_2x2_Mdot_localNorm(sink,mdot,tBH,args):
	#set up plot
	fig,axis= plt.subplots(2,2,sharex=True)#,sharey=True)
	fig.set_size_inches(15., 8.)
	ax=axis.flatten()
	fig.subplots_adjust(hspace=0,wspace=0) 
	#
	colors=['k','b','g','r','c','m','y']
	time= (sink.time-sink.time[0])/tBH
	#MEDIAN - left panels
	for i in [0,2]: #top, bottom r log and linear scaled but otherwise identical	
		ax[i].plot(time,np.median(sink.MdotFine/mdot.const,axis=1),c=colors[0],ls='-',lw=2.)
		ax[i].plot(time,np.median(sink.MdotFine/mdot.local_ft_V,axis=1),c=colors[1],ls='-',lw=2.)
		ax[i].plot(time,np.median(sink.MdotFine/mdot.local_ft_Va,axis=1),c=colors[2],ls='-',lw=2.)
		ax[i].plot(time,np.median(sink.MdotFine/mdot.local_ft_Rho,axis=1),c=colors[3],ls='-',lw=2.)
		#ax[i].plot(time,np.median(sink.MdotFine/mdot.local_ft,axis=1),c=colors[3],ls='-',lw=2.)
		#ax[i].plot(time,np.median(sink.MdotFine/mdot.local_ft_andRho,axis=1),c=colors[4],ls='-',lw=2.)
	#MEAN - right panels	
	for i in [1,3]: #top, bottom r log and linear scaled but otherwise identical	
		ax[i].plot(time,np.mean(sink.MdotFine/mdot.const,axis=1),c=colors[0],ls='-',lw=2.,label='const')
		ax[i].plot(time,np.mean(sink.MdotFine/mdot.local_ft_V,axis=1),c=colors[1],ls='-',lw=2.)
		ax[i].plot(time,np.mean(sink.MdotFine/mdot.local_ft_Va,axis=1),c=colors[2],ls='-',lw=2.)
		ax[i].plot(time,np.mean(sink.MdotFine/mdot.local_ft_Rho,axis=1),c=colors[3],ls='-',lw=2.)
		#ax[i].plot(time,np.mean(sink.MdotFine/mdot.local_ft,axis=1),c=colors[3],ls='-',lw=2.,label='local_ft')
		#ax[i].plot(time,np.mean(sink.MdotFine/mdot.local_ft_andRho,axis=1),c=colors[4],ls='-',lw=2.,label='local_ft_andRho')
	#y label, left vertical panels
	ax[0].set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_{\rm{NORM}} }$',fontweight='bold',fontsize='xx-large')
	ax[2].set_ylabel(r'Linear $\mathbf{ \dot{M}/\dot{M}_{\rm{NORM}} }$',fontweight='bold',fontsize='xx-large')
	for i in [2,3]: ax[i].set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	for i in [0,1]: ax[i].set_yscale('log')
	for i in [2,3]: ax[i].set_yscale('linear')
	#legends
	ax[3].legend(loc=4,fontsize='small',ncol=1)
	#titles
	ax[0].set_title('Median',fontweight='bold',fontsize='xx-large')
	ax[1].set_title('Mean',fontweight='bold',fontsize='xx-large')
	plt.savefig('mdot_2x2_localNorms.png')
	plt.close()

def add_to_plot(ax,label,color, sink,mdot,tBH,args, steady_wid,steady_end,boxcar_w,  cnt):
	time= (sink.mdotTime-sink.mdotTime[0])/tBH
	if boxcar_w > 0:
		#MEDIAN - left panels
		(boxT,boxMed)= Boxcar(time,np.median(sink.mdot,axis=0)/mdot.const,boxcar_w,units_tBH=True)
		ax[0].plot(boxT,boxMed,c=color,ls='-',lw=2.)
		#MEAN - right panels   
		(boxT,boxMean)= Boxcar(time,np.mean(sink.mdot,axis=0)/mdot.const,boxcar_w,units_tBH=True)
		ax[1].plot(boxT,boxMean,c=color,ls='-',lw=2.,label=label)
	else: 
		ax[0].plot(time,np.median(sink.mdot,axis=0)/mdot.const,c=color,ls='-',lw=2.)		
		ax[1].plot(time,np.mean(sink.mdot,axis=0)/mdot.const,c=color,ls='-',lw=2.)		
	#print mean,std of steady mdot
	if steady_wid > 0 and steady_end > 0:
		#median,mean 
		##################  measureSteady is now index_steady() in diff module#########
		(med_avg,med_std)=measureSteady(np.median(sink.mdot,axis=0)/mdot.const,time,steady_wid,steady_end,units_tBH=True)
		(mean_avg,mean_std)=measureSteady(np.mean(sink.mdot,axis=0)/mdot.const,time,steady_wid,steady_end,units_tBH=True)
		print "label= %s: steady median= %f +/- %f, mean = %f +/- %f" % (label,med_avg,med_std,mean_avg,mean_std)
		#label plots
		ax[0].hlines(med_avg,time.min(),time.max(),color=color,linestyle='dashed')	
		ax[1].hlines(mean_avg,time.min(),time.max(),color=color,linestyle='dashed')
		#indiv sink steady mdots for mdot histogram
		indiv_mdot=np.zeros(sink.mdot.shape[0])-1
		for i in range(sink.mdot.shape[0]):	(indiv_mdot[i],std_junk)= measureSteady(sink.mdot[i,:]/mdot.const,time,steady_wid,steady_end,units_tBH=True)
		fout=open(os.path.join(sink_dir,'beta_'+label+'_indivSteadyMdots.pickle'),"w")
		pickle.dump(indiv_mdot,fout)
		fout.close()

def add_indivsinks_to_plot(ax,axid, sid_indices, sink,mdot,tBH,args, boxcar_w,label, **kwargs):
	colors=['k','b','g','r','c','m','y'] +['#33CCFF']
	#plot mdots
	time= (sink.mdotTime-sink.mdotTime[0])/tBH
	for cnt,i in enumerate(sid_indices):
		if boxcar_w > 0:
			(boxT,boxMed)= Boxcar(time,sink.mdot[i,:]/mdot.const,boxcar_w,units_tBH=True)
			ax.plot(boxT,boxMed,c=colors[cnt],ls='-',lw=2.,label='ID: %d' % i)
		else: 
			ax.plot(time,sink.mdot[i,:]/mdot.const,c=colors[cnt],ls='-',lw=2.,label='ID: %d' % i)		
	#y label, left vertical panels
	ax.set_yscale('log')
	ax.set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',**kwargs['labargs'])
	#legends
	ax.legend(loc=4,ncol=1,scatterpoints=1,numpoints=1,**kwargs['legargs'])
	#titles
	ax.set_title(r'$\beta_0$ = ' + label,**kwargs['labargs'])
	if axid < 2: 
		ax.xaxis.set_major_locator(MaxNLocator(prune='upper'))
	if axid == 0:	
		ax.set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_{\rm{B}} }$',**kwargs['labargs']) #fontweight='bold',fontsize='xx-large')

def plot_3x2_8sinks_mdotNorm_sink_VVaRho(sid_indices,fname, sink,mdot,tBH,args):
	#set up plot
	fig,axis= plt.subplots(2,3,sharex=True)#,sharey=True)
	fig.set_size_inches(18.5, 10.5)
	ax=axis.flatten()
	fig.subplots_adjust(hspace=0,wspace=0) 
	#
	colors=['k','b','g','r','c','m','y'] +['#33CCFF']
	lw= 7*[2.]+[2.]
	time= (sink.time-sink.time[0])/tBH
	#indiv sink mdots in left panels
	for cnt,i in enumerate(sid_indices):
		ax[0].plot(time,sink.MdotFine[:,i]/mdot.local_ft_VVaRho[:,i],ls='-',c=colors[cnt],lw=lw[cnt],label='sid='+str(sink.sid[:,i][0]))
		ax[3].plot(time,sink.MdotFine[:,i]/mdot.local_ft_VVaRho[:,i],ls='-',c=colors[cnt],lw=lw[cnt])
	#MEDIAN - middle panels
	ax[1].plot(time,np.median(sink.MdotFine[:,sid_indices]/mdot.local_ft_VVaRho[:,sid_indices],axis=1),'k-',lw=2.)
	ax[1].plot(time,np.median(sink.MdotFine/mdot.local_ft_VVaRho,axis=1),c='0.8',ls='-',lw=2.)
	ax[4].plot(time,np.median(sink.MdotFine[:,sid_indices]/mdot.local_ft_VVaRho[:,sid_indices],axis=1),'k-',lw=2.)
	ax[4].plot(time,np.median(sink.MdotFine/mdot.local_ft_VVaRho,axis=1),c='0.8',ls='-',lw=2.)
	#MEAN - right panels	
	ax[2].plot(time,np.mean(sink.MdotFine[:,sid_indices]/mdot.local_ft_VVaRho[:,sid_indices],axis=1),'k-',lw=2.)
	ax[2].plot(time,np.mean(sink.MdotFine/mdot.local_ft_VVaRho,axis=1),c='0.8',ls='-',lw=2.)
	ax[5].plot(time,np.mean(sink.MdotFine[:,sid_indices]/mdot.local_ft_VVaRho[:,sid_indices],axis=1),'k-',lw=2.,label='8 sinks')
	ax[5].plot(time,np.mean(sink.MdotFine/mdot.local_ft_VVaRho,axis=1),c='0.8',ls='-',lw=2.,label='64 sinks')
	#y label, left vertical panels
	ax[0].set_ylabel(r'Log $\mathbf{ \dot{M}/f(t,sink) }$',fontweight='bold',fontsize='xx-large')
	ax[3].set_ylabel(r'Linear $\mathbf{ \dot{M}/f(t,sink) }$',fontweight='bold',fontsize='xx-large')
	for i in [0,1,2]: 
		ax[i].set_yscale('log')
	for i in [3,4,5]: 
		ax[i].set_yscale('linear')
	#right vertical panels
	for i in [3,4,5]: ax[i].set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	#legends
	ax[0].legend(loc=4,fontsize='small',ncol=2)
	#titles
	ax[0].set_title('8 Sinks',fontweight='bold',fontsize='xx-large')
	ax[1].set_title('Median',fontweight='bold',fontsize='xx-large')
	ax[2].set_title('Mean',fontweight='bold',fontsize='xx-large')
	plt.savefig('plot_3x2_8sinks_'+fname+'_mdotNorm_sinks.png')
	plt.close()


def plot_rmsM_v_time(sink,tBHA,args):
    norm= (sink.time.copy() -sink.time[0])/tBHA
    plt.plot(norm,sink.rmsM,"k-",label="rms Mach")
    plt.xlabel("$\mathbf{ t/t_{bh} }$")
    dylab= r"rms Mach number"
    plt.ylabel(ylab)
    plt.yscale('log')
    if args.ylim: plt.ylim(args.ylim[0],args.ylim[1])
    if args.xlim: plt.xlim(args.xlim[0],args.xlim[1])
    if args.LinearY: plt.yscale('linear')
    plt.legend(loc=2,fontsize='small')
    #save it
    if args.fname: fsave= "krum06_rmsMach.png_"+args.fname+".png"
    else: fsave="krum06_rmsMach.png"
    plt.savefig(fsave)
    plt.close() 


def plot_krum06_ExtremaMdot(simul,tBHA,mdot,args):
    #get average, median Mdot and do box car average if required
    fig,axis=plt.subplots(1,2,figsize=(20,5))
    ax=axis.flatten()
    maxMdot= np.max(simul.mdot,axis=1)/mdot.norm/args.answer
    minMdot= np.min(simul.mdot,axis=1)/mdot.norm/args.answer
    if args.boxcar: 
        (tMax,maxMdot)= Boxcar(simul.time,maxMdot,args.boxcar*tBHA)
        (tMin,minMdot)= Boxcar(simul.time,minMdot,args.boxcar*tBHA)
    else: (tMax,tMin)= (simul.time,simul.time)
    (tMax,tMin)= (tMax/tBHA,tMin/tBHA)
    #plot
    ax[0].plot(tMax,maxMdot,"k-")
    ax[0].set_title("MAX Mdot")
    ax[1].plot(tMin,minMdot,"k-")
    ax[1].set_title("MIN Mdot")
    plt.xlabel("$\mathbf{ t/t_{bh} }$")
    ylab= r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$"
    for myax in ax:
        if args.freq_sample: ylab += " (Sampled every %.1f tBH)" % args.freq_sample
        if args.boxcar: ylab += " (boxcar %.2g tBH)" % args.boxcar
        if args.vrms_vs_time: ylab += " vrms=f(t) corrected"
        myax.set_ylabel(ylab)
        myax.set_yscale('log')
        if args.ylim: myax.set_ylim(args.ylim[0],args.ylim[1])
        if args.xlim: myax.set_xlim(args.xlim[0],args.xlim[1])
        if args.LinearY: myax.set_yscale('linear')
        #leg_box=ax.legend(loc=(1.01,0.01),ncol=1)
        #     bbox=dict(facecolor='red', alpha=0.5)
        #plt.legend(loc=4,fontsize='small')
    #save it
    if args.fname: fsave= "krum06_ExtremaMdot_"+args.fname+".png"
    else: fsave="krum06_ExtremaMdot.png"
    plt.savefig(fsave)
    plt.close() 

  
def sort_SIDs_byTavgMdot(low,hi,simul,tBHA):
	'''returns numpy array of sink ids, sorted highest to lowest time averaged Mdot over low to hi range'''
	normed= simul.time.copy()/tBHA
	if (low > normed.max()) or (hi > normed.max()): raise ValueError
	imin=np.where(normed > low)[0].min()
	imax=np.where(normed < hi)[0].max()
	ind=np.arange(imin,imax+1,1)
	#average each sink's mdot over the interval
	if args.vrms_vs_time:  #mdot.norm[ind] has shape (size,1) but simul.mdot[ind,:] has shape (size,nsinks)
		tavg_mdot=simul.mdot[ind,:].copy()
		for sid in range(args.nsinks):
			tavg_mdot[:,sid]=tavg_mdot[:,sid]/ mdot.norm[ind]/args.answer
		tavg_mdot= np.mean(tavg_mdot,axis=0) #so shape is nsinks
	else:
		tavg_mdot= np.mean(simul.mdot[ind,:]/mdot.norm/args.answer,axis=0) #shape is nsinks
	isort=np.argsort(tavg_mdot) #indices that will sort tavg_mdot lowest to highest
	return isort[::-1] #highest to lowest

def plot_sids(sids, simul,mdot,tBHA,args,colors,lines,fsave):
    for i,sid in enumerate(sids):
        if args.boxcar:
            (boxTime,boxMdot)= Boxcar(simul.time,simul.mdot[:,sid]/mdot.norm/args.answer,args.boxcar*tBHA)
            plt.plot(boxTime/tBHA,boxMdot,c=colors[i],ls=lines[i],label=str(sid))
        else:
            plt.plot(simul.time/tBHA,simul.mdot[:,sid]/mdot.norm/args.answer,c=colors[i],ls=lines[i],label="%d" % sid)
    plt.xlabel("$\mathbf{ t/t_{bh} }$")
    ylab= r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$"
    if args.freq_sample: ylab += " (Sampled every %.1f tBH)" % args.freq_sample
    if args.boxcar: ylab += " (boxcar %.2g tBH)" % args.boxcar
    if args.vrms_vs_time: ylab += " vrms=f(t) corrected"
    plt.ylabel(ylab)
    plt.yscale('log')
    #if args.ylim: plt.ylim(args.ylim[0],args.ylim[1])
    #if args.xlim: plt.xlim(args.xlim[0],args.xlim[1])
    if args.LinearY: plt.yscale('linear')
    plt.legend(loc=(1.01,0.01),ncol=2,fontsize='xx-small')
    plt.tight_layout(rect=(0,0,0.85,1))
    plt.savefig(fsave) 
    plt.close()
 
def plot_indivSinks_byMdotTavg(Tlow,Thi, simul,mdot,tBHA,args):
	#prep plot
	sinks_per_plot=8
	colors=['k','b','g','r','c','m','y'] +['k']
	lines=7*['-']+['--']
	if args.nsinks % sinks_per_plot != 0: raise ValueError
	#get sink ID numbers in highest to lower time averaged mdot order
	isort= sort_SIDs_byTavgMdot(Tlow,Thi,simul,tBHA)
	#pass every sinks_per_plot sink IDs to plot function
	for i in range( int(args.nsinks/sinks_per_plot) ): 
		sids= isort[i*sinks_per_plot:(i+1)*sinks_per_plot]  #[0:8] is 8 sids to plot, next would be [8:16]
		fsave= "indivSinks_mdotRank_%s" % (i) #lower the "rank" higher the mdots in that group
		if args.fname: fsave=fsave+ "_"+args.fname
		plot_sids(sids, simul,mdot,tBHA,args,colors,lines,fsave+".png")


def plot_mdotdata_mdotsteadyline(simul,tnorm,mdot,args):
    '''1 plot per sink, so nsink plots
    this is a check that steady mdot calculation looks reasonable for each sink's mdot data'''
    time= simul.time/tBHA
    mdotBHA= simul.mdot.copy()/mdot.norm/args.answer
    for sid in range(8): #range(args.nsinks):
        lab=""
        if args.vrms_vs_time: lab= " vrms=f(t) corrected"
        plt.plot(time,mdotBHA,'k-',label='data'+lab)
        steady=simul.tavg_mdot[sid]/mdot.norm/args.answer
        plt.hlines(simul.tavg_mdot[sid]/mdot.norm/args.answer,time.min(),time.max(),\
            linestyles='dashed',colors='b',label="steady= %.2g" % steady)
        plt.xlabel("$\mathbf{ t/t_{bh} }$")
        ylab= r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$"
        ylab += r" (<$\mathbf{ \dot{M} }$> after t= %.2f tBH)" % args.tReachSteady
        plt.ylabel(ylab)
        plt.yscale('log')
    #     ax.legend(loc=4)
        if args.ylim: plt.ylim(args.ylim[0],args.ylim[1])
        if args.xlim: plt.xlim(args.xlim[0],args.xlim[1])
        if args.LinearY: plt.yscale('linear')
        plt.legend(loc=(1.01,0.01),ncol=2,fontsize='xx-small')
        fsave="mdot_%d.png" % sid
        plt.savefig(fsave) 
        plt.close()


def plot_testRange(simul,mdot,tBHA,stat_mean,stat_med,ind):
	normed= simul.time.copy()/tBHA
	avgMdot= np.average(simul.mdot,axis=1)/mdot.norm/args.answer
	medianMdot= np.median(simul.mdot,axis=1)/mdot.norm/args.answer
	plt.plot(normed,avgMdot,"k-",label="mean")
	plt.plot(normed,medianMdot,"b-",label="median")
	plt.vlines(normed[ind[0]],avgMdot.min(),avgMdot.max(),colors="r",linestyles='dashed')
	plt.vlines(normed[ind[-1]],avgMdot.min(),avgMdot.max(),colors="r",linestyles='dashed')
	plt.hlines(stat_mean,normed.min(),normed.max(),colors="m",linestyles='dashed')
	plt.hlines(stat_med,normed.min(),normed.max(),colors="m",linestyles='dashed')
	plt.xlabel('t/tBH')
	plt.ylabel('Mdot/Mdot0')
	fname='steadyDist_testRange_%.1f_%.1f.png' % (normed[ind[0]],normed[ind[-1]])
	plt.savefig(fname,dpi=150)
	plt.close()

def plot_steadyDist(lower,upper,simul,mdot,tBHA,args):
    normed= simul.time.copy()/tBHA
    if (lower > normed.max()) or (upper > normed.max()):
        print "args.steadyDist too large, max time have data for is t/tBHA = %.5f" % normed.max()
        raise ValueError
    imin=np.where(normed > lower)[0].min()
    imax=np.where(normed < upper)[0].max()
    ind=np.arange(imin,imax+1,1)
    print "in steadyDist: time/tBHA min=%f, max=%f" % (normed[ind[0]],normed[ind[-1]])
    #get average mdot for each sink over time interval
    if args.vrms_vs_time:  #mdot.norm[ind] has shape (size,1) but simul.mdot[ind,:] has shape (size,nsinks)
        mdot_norm=simul.mdot[ind,:].copy()
        for sid in range(args.nsinks):
            mdot_norm[:,sid]=mdot_norm[:,sid]/ mdot.norm[ind]/args.answer
        med_mdot= np.median(mdot_norm,axis=0)
        mean_mdot= np.mean(mdot_norm,axis=0)
    else:
        med_mdot= np.median(simul.mdot[ind,:]/mdot.norm/args.answer,axis=0)
        mean_mdot= np.mean(simul.mdot[ind,:]/mdot.norm/args.answer,axis=0)
    mean_of_med= np.mean(med_mdot)
    med_of_med= np.median(med_mdot)
    mean_of_mean= np.mean(mean_mdot)
    med_of_mean= np.median(mean_mdot)
    #test calc props correctly
    #plot_testRange(simul,mdot,tBHA,mean_of_med,med_of_med,ind)	
    #plot distribution
    f,axis=plt.subplots(2,2,figsize=(20,5)) #sharey=True
    plt.subplots_adjust( hspace=0.5,wspace=0.2 )
    ax=axis.flatten()
    #median mdot
    lab= "Median mdot over %.2f - %2.f tBH" % (normed[ind[0]],normed[ind[-1]])
    ax[0].set_title(lab)
    lab= "mean over sinks: %.2f \nmedian over sinks: %.2f" % (mean_of_med,med_of_med)
    ax[0].hist(med_mdot,bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[0].set_ylabel("dP/d M  (normalized to 1)")
    ax[0].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")   
    ax[0].legend(loc=1,ncol=1,fontsize='small') 
    ax[2].hist(np.log10(med_mdot),bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[2].set_ylabel("dP/d logM  (normalized to 1)")
    ax[2].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")  
    ax[2].set_xscale('log')
    #mean mdot
    lab= "Mean mdot over %.2f - %2.f tBH" % (normed[ind[0]],normed[ind[-1]])
    ax[1].set_title(lab)
    lab= "mean over sinks: %.2f \nmedian over sinks: %.2f" % (mean_of_mean,med_of_mean)
    ax[1].hist(mean_mdot,bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[1].set_ylabel("dP/d M  (normalized to 1)")
    ax[1].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")   
    ax[1].legend(loc=(0.,1.01),ncol=1,fontsize='small') 
    ax[3].hist(np.log10(mean_mdot),bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[3].set_ylabel("dP/d logM  (normalized to 1)")
    ax[3].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")  
    ax[3].set_xscale('log')  
    ax[1].legend(loc=1,ncol=1,fontsize='small') 
    fname='steadyDist_%d_%d_%d_time_%.1f_%.1f.png' % \
        (int(args.steadyDist[0]),int(args.steadyDist[1]),int(args.steadyDist[2]),normed[ind[0]],normed[ind[-1]])
    plt.savefig(fname,dpi=150)
    plt.close()
    return (np.mean([lower,upper]),mean_of_med,med_of_med,mean_of_mean,med_of_mean)

def plot_nmad(master,label):
	kwargs= dict(labargs=dict(fontsize=16,fontweight='bold'),\
					xtickargs=dict(fontsize=13),\
					ytickargs=dict(fontsize=16),\
					legargs=dict(fontsize=12))
	matplotlib.rcParams['xtick.labelsize'] = kwargs['xtickargs']['fontsize']
	matplotlib.rcParams['ytick.labelsize'] = kwargs['ytickargs']['fontsize']
	#fig,axes=plt.subplots(1,3,sharey=True)
	fig,ax= plt.subplots()
	x,y= master.med/mdotB(), master.nmad/mdotB()
	ax.scatter(x,y,s=50,c='b') #master.med/master.med.max())
	ax.set_yscale('log')
	ax.set_xscale('log')
	ax.set_xlim(x.min()/2,x.max()*2)
	ax.set_ylim(y.min()/2,y.max()*2)
	if args.xlim and args.ylim:
		ax.set_xlim(args.xlim[0],args.xlim[1])
		ax.set_ylim(args.ylim[0],args.ylim[1])
	ax.set_xlabel(r'Median($\dot{M}/\dot{M}_B$)',**kwargs['labargs'])
	ax.set_ylabel(r'Med[|$\dot{M}$ - Med($\dot{M}$)|]/$\dot{M}_B$',**kwargs['labargs'])
	ax.set_title(r'NMAD, $\beta_0$ = ' + label,**kwargs['labargs'])
	plt.savefig('nmad_beta%s.png' % label)
	plt.close()


#correlated sinks
master={}
for sink_dir,str_beta in zip(args.dirs,args.labels):
	fin=open(os.path.join(sink_dir,'master_sink.pickle'),'r')
	master[str_beta]=pickle.load(fin)
	master[str_beta].sink_xyz(args.init_sink)
	fin.close()
sizes=range(len(args.labels))
for i,str_beta in enumerate(args.labels):
	sizes[i]= master[str_beta].mdotTime.shape[0]
junk=args.labels[0]
x= np.empty( (master[junk].mdot.shape[0],)+(max(sizes),)+(len(args.labels),))*np.nan
y=x.copy()
for i,str_beta in enumerate(args.labels):
	time=(master[str_beta].mdotTime-master[str_beta].mdotTime[0])/get_tBH()
	x[:,:sizes[i],i]= np.transpose(np.repeat(time[:,np.newaxis], x.shape[0], axis=1)) #(npts,) array to (64,npts,)
	y[:,:sizes[i],i]= master[str_beta].mdot/mdotB()
titles= np.core.defchararray.add(np.array(['sid ']*len(master[str_beta].init.sid)),master[str_beta].init.sid.astype(str))
nrow,ncol=8,8
multi_plot(nrow,ncol,x,y,titles=titles,logy=True,ylim=args.ylim)

print 'exiting after correlated sinks'
sys.exit(0)

#nmad plot
print 'plotting nmad'
for sink_dir,label in zip(args.dirs,args.labels):
	fin=open(os.path.join(sink_dir,'master_sink.pickle'),'r')
	master=pickle.load(fin)
	fin.close()
	plot_nmad(master,label)

#print "exiting after function nmad()"
#sys.exit(0)


#plot sids for b100,1,0.01
if len(args.dirs) == 3 and args.sids:
	kwargs= dict(labargs=dict(fontsize=22,fontweight='bold'),\
				xtickargs=dict(fontsize=13),\
				ytickargs=dict(fontsize=16),\
				legargs=dict(fontsize=12))
	matplotlib.rcParams['xtick.labelsize'] = kwargs['xtickargs']['fontsize']
	matplotlib.rcParams['ytick.labelsize'] = kwargs['ytickargs']['fontsize']
	fig,axes=plt.subplots(1,3,sharey=True)
	fig.set_size_inches(12., 6.5)
	fig.subplots_adjust(wspace=0)	
	ax=axes.flatten()
	for cnt,sink_dir,boxcar_w,lab in zip(range(3),args.dirs,args.boxcar,args.labels):
		fin=open(os.path.join(sink_dir,'master_sink.pickle'),'r')
		master_sink=pickle.load(fin)
		fin.close()
		#
		tBH = bigG*args.mass0/(args.mach**2+ args.cs**2)**(3./2)
		if len(args.sids) > 3: add_indivsinks_to_plot(ax[cnt],cnt, args.sids[cnt*3:(cnt+1)*3],master_sink,mdot,tBH,args, boxcar_w,lab, **kwargs)
		else: add_indivsinks_to_plot(ax[cnt],cnt, args.sids,master_sink,mdot,tBH,args, boxcar_w,lab, **kwargs)
	plt.savefig('indivsinks_sids.png')
	plt.close()

#load each master_sink file adding mean,median mdot to plot as go 
fig,axis= plt.subplots(1,2,sharex=True)
fig.set_size_inches(15., 8.)
ax=axis.flatten()
fig.subplots_adjust(hspace=0,wspace=0.1) 
colors=['k','b','g','r','c','m','y'][:len(args.dirs)]
#plot set of levMax,lev0 for each Beta 
legend_patches=[]
cnt=0	
for sink_dir,label,color,steady_wid,steady_end,boxcar_w in zip(args.dirs,args.labels,colors,args.steady_wid,args.steady_end,args.boxcar):
	cnt+=1
    #load	
	fin=open(os.path.join(sink_dir,'master_sink.pickle'),'r')
	master_sink=pickle.load(fin)
	fin.close()
	tBH = bigG*args.mass0/(args.mach**2+ args.cs**2)**(3./2)
	#plot mean and median mdots
	add_to_plot(ax,label,color, master_sink,mdot,tBH,args, steady_wid,steady_end,boxcar_w,  cnt)
	#add legend
	#label= r"$\beta$= "+label
	#legend_patches.append( mpatches.Patch(color=color, label=label) )
#add legend
ax[1].legend(loc=4,handles=legend_patches)
#user defined x,y limits?
if args.ylim1: ax[0].set_ylim(args.ylim1[0],args.ylim1[1]) 
if args.ylim2: ax[1].set_ylim(args.ylim2[0],args.ylim2[1]) 
if args.xlim: 
	for i in [0,1]: ax[i].set_xlim(args.xlim[0],args.xlim[1]) 
#label plot
laba=dict(fontweight='bold',fontsize='xx-large')
if args.krumholz_norm: ax[0].set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_{\rm{0}} }$',**laba)
else: ax[0].set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_{\rm{B}} }$',**laba)
for i in [0,1]: 
	ax[i].set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',**laba)
	ax[i].set_yscale('log')
#legends
ax[1].legend(loc=4,fontsize='small',ncol=1,handlelength=3.)
#titles
ax[0].set_title('Median',fontweight='bold',fontsize='xx-large')
ax[1].set_title('Mean',fontweight='bold',fontsize='xx-large')
#plt.show()
plt.savefig(os.path.join('./',args.save_name+'_mdot_v_tBH.png'))
#mpld3.save_html(fig,args.save_name+'_mdot_v_tBH.html')
plt.close()


