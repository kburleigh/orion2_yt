import yt
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt

import modules.hdf5_params.rms_values as val
from modules.sink.rates_and_tscales import get_tBH

def fill_data(hdf5_file):
	ds=yt.load(hdf5_file)
	data= val.yt_prim_vars(ds)
	data['time']= float(ds.current_time)
	data['vmag']= val.three_square(data['vx'],data['vy'],data['vz'])
	data['bmag']= val.three_square(data['bx'],data['by'],data['bz'])
	data['mach_a']= val.alfven_mach(data['vmag'],data['density'],data['bmag'])
	return data

parser = ArgumentParser(description="test")
parser.add_argument("-ic_data",action="store",help='IC hdf5 data file',required=True)
parser.add_argument("-end_data",action="store",help='hdf5 data file towards end of simulation',required=True)
parser.add_argument("-rms_mach",nargs=2,type=float,action="store",help='ic end rms_mach numbers for data',required=False)
args = parser.parse_args()

ic= fill_data(args.ic_data)
ic_vmag,ic_mach_a,ic_time= ic['vmag'],ic['mach_a'],ic['time']
del ic #save mem
fc= fill_data(args.end_data)
fc_vmag,fc_mach_a,fc_time= fc['vmag'],fc['mach_a'],fc['time']
del fc 
#rms mach
cs=1.
lab= 't=0'
if args.rms_mach: lab+= ', rms_mach=%.1f' % args.rms_mach[0]
plt.hist(np.log10(ic_vmag/cs),bins=50,normed=True,color='y',alpha=0.5,label=lab)
t=(fc_time-ic_time)/get_tBH()
lab= 't= %.2f' % t
if args.rms_mach: lab+= ', rms_mach=%.1f' % args.rms_mach[1]
plt.hist(np.log10(fc_vmag/cs),bins=50,normed=True,color='b',alpha=0.5,label=lab)
plt.xlabel('rms Mach')
plt.legend(loc=0)
plt.savefig('rms_mach_hist.png')
plt.close()
#alfven mach
plt.hist(np.log10(ic_mach_a),bins=50,normed=True,color='y',alpha=0.5,label='t= 0')
plt.hist(np.log10(fc_mach_a),bins=50,normed=True,color='b',alpha=0.5,label='t= %.2f' % t)
plt.xlabel('Alfven Mach')
plt.legend(loc=0)
plt.savefig('mach_a_hist.png')
plt.close()
