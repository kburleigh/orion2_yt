'''
8/26/2016
SIGMA_PDF: Chris found that I reversed the upper/lower vals of these in table 1 (e.g. lower sigma > upper sigma for beta=1).. This was copy paste error. the following describes how get the right values and in the correct upper/lower order. it is unclear how to get sigma_PDF from the mdot/mdotB distribution so I get it from log10(mdot/mdotB) which is printed out to "bootstrap_PDF.txt" when "modules/plot/fundamental_multi_sink.py" calls "stats.final_measurment(final_pdf_log)". This script uses the LINEAR mdot/mdotB values in "bootstrap_PDF.txt" and "bootstrap_2.txt" to compute median,mean, assocated error bars but sigma_pdf needs to come from the log10 final measurement values in bootstrap_PDF.txt
SIGMA_SINK: printed to terminal under INDIV SINK MEDIAN 1-SIGMA

8/21/2016
Makes 1) errbars vs. betarms and 2) model vs. errbars
MEDIAN: Sample median and bootstrapped errbars computed using 
	"modules/plot/bootstrap_PDF.txt" file. 
	This is the original accretion rate PDF method and produces the numbers in Table 1 
	I've been using all along.
MEAN: Sample median and bootstrapped errbars computed using 
	"modules/plot/bootstrap_2.txt" file.
	Because the median and mean of the accretion rate PDF are the same to a few percent,
	the mean cannot be computed using the accretion PDF method. Instead the bootstraps are
	taken over mdotArray of shape (64,Nsteadytimepts), sampling the 64 sids with replacement
	for each 64 sid sample, compute np.mean(mdotArray[sid_sample,:],axis=0) to get a single
	mean curve, then compute np.median(mean cuve) to get the steady state mdot for that 
	sample. The median with errbars can be computed in the same with
	np.median(mdotArray[sid_sample,:],axis=0) and np.median(median curve)
Summary: the  bootstrap errbars on the median in "bootstrap_PDF.txt" and 
	"bootstrap_2.txt" are within a few percent of each other. Therefore I report median
	values using "bootstrap_PDF.txt" (since accretion rate PDF can be used throughout the 
	paper) and mean values using "bootstrap_2.txt" (since they are only defined for this
	method) 
'''


import numpy as np
import argparse
import os
import pickle
import matplotlib.pyplot as plt
from matplotlib import gridspec
import sys
import seaborn as sns
sns.set_palette('colorblind')

from modules.sink.rates_and_tscales import mdotB,mdotBH,ic_rms_vals,lee_parallel,ic_median_beta

def std2log(x,sig_x):
	'''given data x with std deviation sig_x, return std deviation sig_y
	where y=log10(x)'''
	return (1./np.log(10.))*sig_x/x

def print_measurement(d,name):
	'''d as returned by get_measurment()'''
	if name == 'boot_PDF':
		print "beta $median^{+}_{-}$"
		print "%s FINAL MEASUREMENTS (units mdot/mdotB):" % name
		for i in range(len(d['beta'])): 
			print "%s $%.6g^{+%.6g}_{-%.6g}$ & $^{+%.6g}_{-%.6g}$" % \
				  (d['beta'][i],d['smed'][i],d['med_hi'][i],d['med_low'][i],d['pdf_hi'][i],d['pdf_low'][i])
		print "%s FINAL MEASUREMENTS (units Log10 mdot/mdotB):" % name
		for i in range(len(d['beta'])): 
			print "%s $%.6g^{+%.6g}_{-%.6g}$" % \
				  (d['beta'][i],np.log10(d['smed'][i]),\
				   std2log(d['smed'][i],d['med_hi'][i]),std2log(d['smed'][i],d['med_low'][i]))
	elif name == 'boot_2':
		print "beta $median^{+}_{-} \,\, mean^{+}_{-}$"
		print "%s FINAL MEASUREMENTS (units mdot/mdotB):" % name
		for i in range(len(d['beta'])): 
			print "%s $%.6g^{+%.6g}_{-%.6g} \,\, %.6g^{+%.6g}_{-%.6g}$" % \
				  (d['beta'][i],d['smed'][i],d['med_hi'][i],d['med_low'][i],\
								d['smean'][i],d['mean_hi'][i],d['mean_low'][i])
		print "%s FINAL MEASUREMENTS (units Log10 mdot/mdotB):" % name
		for i in range(len(d['beta'])): 
			print "%s $%.6g^{+%.6g}_{-%.6g} \,\, %.6g^{+%.6g}_{-%.6g}$" % \
				  (d['beta'][i],np.log10(d['smed'][i]),\
				   std2log(d['smed'][i],d['med_hi'][i]),std2log(d['smed'][i],d['med_low'][i]),\
				   np.log10(d['smean'][i]),\
				   std2log(d['smean'][i],d['mean_hi'][i]),std2log(d['smean'][i],d['mean_low'][i]))

	
def get_measurment(name):
	d={}
	if name == 'boot_PDF':
		d['beta'],d['smed'],d['med_low'],d['med_hi'],d['pdf_low'],d['pdf_hi']= \
			np.loadtxt('modules/plot/bootstrap_PDF.txt',dtype=str,usecols=tuple(range(6)),unpack=True)
	elif name == 'boot_2':
		d['beta'],d['smed'],d['med_low'],d['med_hi'],d['smean'],d['mean_low'],d['mean_hi']=\
			np.loadtxt('modules/plot/bootstrap_2.txt',dtype=str,usecols=tuple(range(7)),unpack=True)
	else: raise ValueError
	for key in d.keys():
		if key != 'beta': 
			d[key]= d[key].astype(float)		
	return d

#def name2beta(name):
#	if 'Inf' in nm: return np.inf
#	else: return float(name[1:]) 

def add_to_measurement(d):
	'''d is dict returned from "get_measurement()"'''
	new={}
	for key in ['name','rms_beta','yax_val','med','med_low','med_hi']:
		new[key]= np.zeros(len(d['beta']))	
		# Set non float types
		if key == 'name': new[key]= new[key].astype(str)
		elif key == 'yax_val': new[key]= new[key].astype(int)
	if d.has_key('smean'):
		for key in ['mean','mean_low','mean_hi']:
			new[key]= np.zeros(len(d['beta']))
	if d.has_key('pdf_low'):
		for key in ['pdf_low','pdf_hi']:
			new[key]= np.zeros(len(d['beta']))
	mods= ['bInf', 'bInfsd3', 'b100','b10','b1','b1e-1', 'b1e-2']
	assert(len(mods) == len(d['beta']))
	for i,key in enumerate(mods[::-1]):
		new['name'][i]= key
		rms_va,rms_b= ic_rms_vals(key)
		new['rms_beta'][i]= rms_b 
		new['yax_val'][i]= i+1
		#median and +/- sigma using quartiles
		idx= list(d['beta']).index(key)
		new['med'][i]= d['smed'][idx] 
		new['med_low'][i]= d['med_low'][idx]
		new['med_hi'][i]= d['med_hi'][idx]
		if d.has_key('smean'):
			#mean and +/- sigma using quartiles
			new['mean'][i]= d['smean'][idx] 
			new['mean_low'][i]= d['mean_low'][idx] 
			new['mean_hi'][i]= d['mean_hi'][idx] 
		if d.has_key('pdf_low'):
			new['pdf_low'][i]= d['pdf_low'][idx] 
			new['pdf_hi'][i]= d['pdf_hi'][idx] 
	return new


def box_and_whisker(ax, med,sem,std,yloc,boxw=0.2,legend_lab_ea_color=False):
	sem_low,sem_hi= sem
	std_low,std_hi= std
	#get colors
	sns.set_palette('colorblind')
	c_med='k' #median
	c_sem=sns.color_palette()[2] #error on median box 'r', bootstraped
	c_std= sns.color_palette()[0] #std dev of pdf 'b', percentiled
	#legend labels (if called)
	ll= ['Median','Std. Error of Median','Std. Dev. of PDF']
	#std error med box
	if legend_lab_ea_color: 
		print '-------------------------- adding legend -------------'
		ax.plot([med]*2,[yloc-boxw,yloc+boxw],c=c_med,label=ll[0])
	else: ax.plot([med]*2,[yloc-boxw,yloc+boxw],c=c_med)
	ax.plot([med-sem_low]*2,[yloc-boxw,yloc+boxw],c=c_sem)
	ax.plot([med+sem_hi]*2,[yloc-boxw,yloc+boxw],c=c_sem)
	ax.plot([med-sem_low,med+sem_hi],[yloc-boxw]*2,c=c_sem)
	if legend_lab_ea_color: ax.plot([med-sem_low,med+sem_hi],[yloc+boxw]*2,c=c_sem,label=ll[1])
	else: ax.plot([med-sem_low,med+sem_hi],[yloc+boxw]*2,c=c_sem)
	#pdf std dev
	ax.plot([med-std_low,med-sem_low],[yloc]*2,c=c_std)
	ax.plot([med+sem_hi,med+std_hi],[yloc]*2,c=c_std)
	ax.plot([med-std_low]*2,[yloc-boxw,yloc+boxw],c=c_std)
	if legend_lab_ea_color: ax.plot([med+std_hi]*2,[yloc-boxw,yloc+boxw],c=c_std,label=ll[2])
	else: ax.plot([med+std_hi]*2,[yloc-boxw,yloc+boxw],c=c_std)



def put_errbars_in_arrays_linear(final):
	'''DON'T TAKE LOG10 of mdot IN THIS VERSION
	final is dict returned by final_measurement()
	vals are stored in dicts, want order them into arrays'''
	name,rms_beta,med_beta,yax_val,med,med_low,med_hi,std_low,std_hi= [],[],[],[],[],[],[],[],[]
	mods= ['bInf', 'bInfsd3', 'b100','b10','b1','b1e-1', 'b1e-2']
	print 'put_errbars_in_arrays_linear: mods= ',mods
	print 'put_errbars_in_arrays_linear: final.keys()= ',final.keys()
	for cnt,key in enumerate(mods[::-1]):
		name.append( key)
		rms_va,rms_b= ic_rms_vals(key)
		rms_beta.append( rms_b )
		med_beta.append( ic_median_beta(key)  )
		yax_val.append( cnt+1)
		#median and +/- sigma using quartiles
		med.append( final[key]['med'] )
		med_low.append( final[key]['med_low'] )
		med_hi.append( final[key]['med_hi'] )
		#width mdot distributing from +/- quartiles
		std_low.append( final[key]['std_low'] )
		std_hi.append( final[key]['std_hi'] ) 
	return np.array(med),np.array(med_low),np.array(med_hi),np.array(std_low),np.array(std_hi), np.array(yax_val),np.array(rms_beta),np.array(med_beta),name


FS=18
dFS=FS+5

laba=dict(fontweight='bold',fontsize=dFS)
text=dict(fontweight='bold',fontsize=FS)
def errorbars_vs_beta2(final64,use_rms_beta=True,name='test.png'):
	'''all mdot quantities are in units of mdot/mdotB'''
	sns.set_style('ticks') #,{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	c_med='k' #median
	c_mean= sns.color_palette()[0] #'b'
	c_bhline= sns.color_palette()[1] #'g'
	c_aaron=sns.color_palette()[2] #error on median box 'r'
	c_mark= sns.color_palette()[3] #'pink'
	#kwargs
	laba=dict(fontweight='bold',fontsize=dFS)
	kwargs_axtext=dict(fontweight='bold',fontsize=FS,va='top')
	# orig final measure, REPLACED BY bootstrap_PDF.txt
	#med,med_low,med_hi,std_low,std_hi,yax_val,rms_beta,med_beta,name= put_errbars_in_arrays_linear(final64)
	# MEDIAN, bootstrap PDF
	bPDF= get_measurment('boot_PDF')
	bPDF= add_to_measurement(bPDF)
	# MEAN, bootstrap 2
	b2= get_measurment('boot_2')
	b2= add_to_measurement(b2)
	# get rms_beta
	rms_beta= bPDF['rms_beta']
	#marks results
	rmdot0= 0.0166
	# Values when submitted to MNRAS
	#mark_norm_mdot0=dict(median_sim=0.35,median_pred_256=0.36,err_median_pred_256=0.1,\
	#					 mean_pred_256=1.26,err_mean_pred_256=0.1,\
	#					 mean_sim=0.9,err_mean_sim=0.1) # 0.1 is guess
	# Values for revision, after fitting marks table 1
	mark_norm_mdot0=dict(median_sim=0.35,median_pred_256=0.36,err_median_pred_256=0.106,\
						 mean_pred_256=1.26,err_mean_pred_256=0.100,\
						 mean_sim=0.9,err_mean_sim=0.1) # 0.1 is guess
	mark_norm_mdotB={}
	for key in mark_norm_mdot0.keys():
		mark_norm_mdotB[key]= mark_norm_mdot0[key]* 4/np.exp(1.5)/5.**3
	#special figure for different sized subplots
	fig = plt.figure(figsize=(10,6)) 
	gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1])
	fig.subplots_adjust(wspace=0.05)
	ax=[0,0] 
	ax[0] = plt.subplot(gs[0])
	ax[1] = plt.subplot(gs[1])
	#B != 0 points go on left side
	#ax[0].errorbar(rms_beta[:-2],med[:-2]/mdotB(),yerr=[med_low[:-2]/mdotB(),med_hi[:-2]/mdotB()], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med,label=r'This Study (Median)')
	# My Magnetic Points
	# median
	#ax[0].errorbar(rms_beta[:-2],med[:-2]/mdotB(),yerr=[med_low[:-2]/mdotB(),med_hi[:-2]/mdotB()], fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	ax[0].errorbar(bPDF['rms_beta'][:-2],bPDF['med'][:-2],yerr=[bPDF['med_low'][:-2],bPDF['med_hi'][:-2]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	ax[0].errorbar(bPDF['rms_beta'][:-2],bPDF['med'][:-2],yerr=[bPDF['med_low'][:-2],bPDF['med_hi'][:-2]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med,label='Median')
	#ax[0].errorbar(b2['rms_beta'][:-2],b2['med'][:-2],yerr=[b2['med_low'][:-2],b2['med_hi'][:-2]], fmt='o',ms=6,mew=2.,mfc='none',mec='b',c='b',label='boot 2 median')
	# mean
	ax[0].errorbar(b2['rms_beta'][:-2],b2['mean'][:-2],yerr=[b2['mean_low'][:-2],b2['mean_hi'][:-2]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_mean,c=c_mean,label='Mean')
	#just for legend, plot some junk with right colors for mine and marks
	#ax[0].plot([5,6],med[-2:],c=c_med,visible=False,label=r'This Study (Median $\mathbf{Log_{\rm{10}} \,\, \dot{M} }$)')
	#ax[0].plot([5,6],med[-2:],c=c_mark,visible=False,label='Krumholz et al. (2006)')
	#aarons
	cont_beta=np.logspace(-3,1,num=50)
	ax[0].plot(cont_beta,lee_parallel(cont_beta,b_norm=True),c=c_aaron,ls='--',lw=2,label='Lee et al. (2014)')
	#print useful number for paper
	#print 'ratio of mdot to (mdot_perp+mdot_par)/2 is: betarms,mdot,mdotpar,mdot/mdotpar'
	#for myb,mymdot,mdotpar in zip(rms_beta[:-2],med[:-2],np.log10(lee_parallel(rms_beta[:-2]))): print myb,mymdot,mdotpar,10**(mdotpar)/10**(mymdot) 
	#mdot BH
	ax[0].plot(ax[0].get_xlim(),[mdotBH()/mdotB()]*2,c=c_bhline,ls='--',lw=2)
	ax[0].text(1e-2,mdotBH()/mdotB(),r'$\mathbf{ \dot{M}_{BH} }$',verticalalignment='bottom',**text) #,transform=ax[0].transAxes)
	#ax[0].text(1e-2,np.log10(mdotBH()),r'$\mathbf{ \dot{M}_{BH} }$',color=c_bhline,**kwargs_ax[0].ext)
	#B = 0 points go on right side
	ax[1].plot(range(10),visible=False)
	#Marks prediction
	ax[1].errorbar(4,mark_norm_mdotB['median_pred_256'],yerr=mark_norm_mdotB['err_median_pred_256'],fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	#ax[1].errorbar(4,mark_norm_mdotB['mean_pred_256'],yerr=mark_norm_mdotB['err_mean_pred_256'],fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	ax[1].errorbar(4,mark_norm_mdotB['mean_sim'],yerr=mark_norm_mdotB['err_mean_sim'],fmt='o',ms=6,mew=2.,mfc='none',mec=c_mark,c=c_mark,label='Krumholz et al. (2006)')
	#my hydro pts
	#ax[1].errorbar([5,6],med[-2:]/mdotB(),yerr=[med_low[-2:]/mdotB(),med_hi[-2:]/mdotB()], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med)
	ax[1].errorbar([5,6],bPDF['med'][-2:],yerr=[bPDF['med_low'][-2:],bPDF['med_hi'][-2:]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_med,c=c_med)
	#ax[1].errorbar([9,10],b2['med'][-2:],yerr=[b2['med_low'][-2:],b2['med_hi'][-2:]], fmt='o',ms=6,mew=2.,mfc='none',mec='r',c='r')
	ax[1].errorbar([5,6],b2['mean'][-2:],yerr=[b2['mean_low'][-2:],b2['mean_hi'][-2:]], fmt='o',ms=6,mew=2.,mfc='none',mec=c_mean,c=c_mean)
	#mdotBH and same for hydro limit of lee14
	ax[1].plot(ax[1].get_xlim(),[mdotBH()/mdotB()]*2,c=c_bhline,ls='--',lw=2)
	ax[1].plot([0.5,8.5],[mdotBH()/mdotB()]*2,c=c_aaron,ls='--',lw=2) 
	#finish labeling
	for i in range(2): 
		ax[i].set_ylim(6e-4,1e-2)
	ax[0].set_xscale('log')
	ax[0].set_xlim(8e-3,1e1)
	#ax[1].set_xlim(2,1e1)
	ylab= ax[0].set_ylabel(r'$\mathbf{\dot{M}/\dot{M}_{\rm{B}} }$',**laba)
	xlab= fig.text(0.5, 0., r'$\mathbf{ \beta_{turb} }$', ha='center', **laba) #ax[0].set_xlabel(r'$\mathbf{ \beta_{rms} }$',**laba)
	ax[0].legend(loc=4,fontsize=FS,frameon=True,fancybox=True)
	#reorganize legend
	handles, labels = ax[0].get_legend_handles_labels()
	handles= [handles[-1],handles[-2],handles[1],handles[0]]
	labels= [labels[-1],labels[-2],labels[1],labels[0]]
	ax[0].legend(handles, labels,loc=4,frameon=True,fancybox=True,fontsize=FS)
	#remove top and right axis lines
	#sns.despine()
	#remove spines and ticks between ax and ax2
	#ax[0].spines['right'].set_visible(False)
	#ax[1].spines['left'].set_visible(False)
	for i in range(2): ax[i].tick_params(axis='both', which='major', labelsize=FS)
	ax[1].set_yticks([])
	#add slashes indicating break in axis
	d = .015 # how big to make the diagonal lines in axes coordinates
	# arguments to pass plot, just so we don't keep repeating them
	kwargs = dict(transform=ax[0].transAxes, color='k', clip_on=False)
	ax[0].plot((1-d,1+d), (-d,+d), **kwargs)
	ax[0].plot((1-d,1+d),(1-d,1+d), **kwargs)
	kwargs.update(transform=ax[1].transAxes)  # switch to the bottom axes
	ax[1].plot((-d*3,+d*3), (1-d,1+d), **kwargs)
	ax[1].plot((-d*3,+d*3), (-d,+d), **kwargs)
	#put infinity on 2nd axis
	ax[1].set_xticks([])
	ax[1].set_xlabel(r'$\mathbf{ \infty }$',**laba)
	#plt.tick_params(axis='both', which='major', labelsize='large')
	# show only some of yaxis ticks
	#ax[0].yaxis.tick_left()
	ax[1].yaxis.tick_right()
	# y log scale
	for i in range(2): ax[i].set_yscale('log')
	#SAVE IS BROKEN, save manually
	plt.tight_layout()
	#plt.show()
	#save
	name='testing.png'
	plt.savefig(name, bbox_extra_artists=[xlab,ylab], bbox_inches='tight',dpi=150)
	print 'wrote %s' % name


laba=dict(fontweight='bold',fontsize=dFS)
def model_vs_errorbars2(final64, name='test.png'):
	'''final64 is linear mdots now
	put model names on y axis, error bars on x axis'''
	sns.set_style('ticks') #,{"axes.facecolor": ".97"})
	sns.set_palette('colorblind')
	c_med='k' #median
	c_sem=sns.color_palette()[2] #error on median box 'r', bootstraped
	c_std= sns.color_palette()[0] #std dev of pdf 'b', percentiled
	#kwargs
	kwargs_axtext=dict(fontweight='bold',fontsize=FS,va='top',ha='left')
	#mapping
	addtext={}
	addtext['bInf']=r'$\mathbf{ \beta_0 = \infty }$ (1)'
	addtext['bInfsd3']=r'$\mathbf{ \beta_0 = \infty }$ (2)'
	addtext['b100']=r'$\mathbf{ \beta_0 = 100 }$'
	addtext['b10']=r'$\mathbf{ \beta_0 = 10 }$'
	addtext['b1']= r'$\mathbf{ \beta_0 = 1 }$'
	addtext['b1e-1']= r'$\mathbf{ \beta_0 = 0.1 }$'
	addtext['b1e-2']=r'$\mathbf{ \beta_0 = 0.01 }$'
	#plot
	fig,ax=plt.subplots()
	ax.set_ylim(-1,8)
	ax.set_xlim(1e-4,2e-2)
	ylabels = [item.get_text() for item in ax.get_yticklabels()] #y tick labels
	#meds,med_lows,med_his,std_lows,std_his,yax_vals,rms_betas,med_betas,names= put_errbars_in_arrays_linear(final64)
	#for med,med_low,med_hi,std_low,std_hi,yax_val,rms_beta,med_beta,name in zip(meds,med_lows,med_his,std_lows,std_his,yax_vals,rms_betas,med_betas,names):
		#if name == names[-1]: box_and_whisker(ax, med/mdotB(),(med_low/mdotB(),med_hi/mdotB()),(std_low/mdotB(),std_hi/mdotB()),yax_val,legend_lab_ea_color=True)
		#else: box_and_whisker(ax, med/mdotB(),(med_low/mdotB(),med_hi/mdotB()),(std_low/mdotB(),std_hi/mdotB()),yax_val)
		#ylabels[yax_val]= addtext[name]	
	bPDF= get_measurment('boot_PDF')
	bPDF= add_to_measurement(bPDF)
	print "bPDF.keys()= ",bPDF.keys()
	for i in range(len(bPDF['med'])):
		if bPDF['name'][i] == bPDF['name'][-1]: box_and_whisker(ax, bPDF['med'][i],(bPDF['med_low'][i],bPDF['med_hi'][i]),(bPDF['pdf_low'][i],bPDF['pdf_hi'][i]),bPDF['yax_val'][i],legend_lab_ea_color=True)
		else: box_and_whisker(ax, bPDF['med'][i],(bPDF['med_low'][i],bPDF['med_hi'][i]),(bPDF['pdf_low'][i],bPDF['pdf_hi'][i]),bPDF['yax_val'][i])
		ylabels[ bPDF['yax_val'][i]+1 ]= addtext[ bPDF['name'][i] ]	
	ax.set_yticklabels(ylabels,fontsize=dFS)
	xlab=ax.set_xlabel(r'$\mathbf{\dot{M}/\dot{M}_{\rm{B}} }$',**laba)
	ylab=ax.set_ylabel('Simulation',**laba)
	ax.legend(loc=(0.575,0.01),fontsize=FS-3,frameon=True,fancybox=True)
	plt.tick_params(axis='x', which='major', labelsize=FS)
	#turn off horizontal grid lines
	ax.yaxis.grid(False)
	ax.xaxis.grid(True)
	ax.set_xscale('log')
	ax.yaxis.tick_left()
	#ax.set_yticks([])
	#remove top and right axis lines
	#sns.despine()
	#save
	plt.savefig(name,bbox_extra_artists=[xlab,ylab], bbox_inches='tight',dpi=150)
	plt.close()
	print 'wrote %s' % name

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-pickle",action="store",default='modules/plot/orig_final_measure.pickle',help='pickle file',required=False)
parser.add_argument("-outdir",action="store",default='./',required=False)
args = parser.parse_args()

# print final measurements
print_measurement(get_measurment('boot_PDF'), 'boot_PDF')
print_measurement(get_measurment('boot_2'), 'boot_2')

# make plots
fout=open(args.pickle, 'r')
final_pdf_linear,final_meas_linear= pickle.load(fout)
fout.close()



name=os.path.join(args.outdir,'model_v_errbars.png') # % args.pickle)
model_vs_errorbars2(final_meas_linear, name=name)
#name=os.path.join(args.outdir,'mod_errbars_%s.png' % args.pickle) 
print 'WARNING: interactive plots needs to be closed'
errorbars_vs_beta2(final_meas_linear,use_rms_beta=True) #,name=name)

print 'done'
