import numpy as np

def low_hi_mdot_all_models():
	'''used to compute final steady PDF,CDF for each model
	return lowest and highest mdot for all models evaluated over range t_steady_begin for each model until min(20tBH,sink.tnorm()[-1])''' 
	return 6.83709111075e-08, 0.088603818424 #mdot low,hi as previously computed by fundamental_mult_sink.py


def steady_state_begins(str_beta,units_tBH):
	'''determined from ks_stat_steady_state(), when U-test p-value >= 0.05'''
	assert(units_tBH == True)
	tbeg={'bInfsd3': 1.50,\
			'bInf': 2.40,\
			'b100': 2.70,\
			'b10': 3.40,\
			'b1': 4.60,\
			'b1e-1': 2.90,\
			'b1e-2': 1.60}
	return tbeg[str_beta]


def ic_rms_vals(str_beta):
	#times={'bInfsd3': 0.820615,\
	#		'bInf': 0.828638,\
	#		'b100': 0.878586,\
	#		'b10': 1.03891,\
	#		'b1': 0.854807,\
	#		'b1e-1': 0.839822,\
	#		'b1e-2': 0.816491}
	rms_va={'bInfsd3': 0.,\
			'bInf': 0.,\
			'b100': 1.10,\
			'b10': 2.12,\
			'b1': 3.52,\
			'b1e-1': 5.15,\
			'b1e-2': 14.18}
	rms_beta={}
	for key in rms_va.keys(): 
		if key == 'bInf' or key == 'bInfsd3': rms_beta[key]= np.nan
		else: rms_beta[key]= 2./rms_va[key]**2	
	return rms_va[str_beta],rms_beta[str_beta]

def ic_median_beta(str_beta):
	med_beta={'bInfsd3': np.nan,\
			'bInf': np.nan,\
			'b100': 3.146489,\
			'b10': 0.448662,\
			'b1': 0.1121503,\
			'b1e-1': 0.059687,\
			'b1e-2': 0.009402 }
	return med_beta[str_beta]	

def t_rms_mach_near_end(str_beta):
	times={'bInfsd3': 0.820615,\
			'bInf': 0.828638,\
			'b100': 0.878586,\
			'b10': 1.03891,\
			'b1': 0.854807,\
			'b1e-1': 0.839822,\
			'b1e-2': 0.816491}
	rms_mach={'bInfsd3': 4.810,\
			'bInf': 4.708,\
			'b100': 4.173,\
			'b10': 3.119,\
			'b1': 4.386,\
			'b1e-1': 4.800,\
			'b1e-2': 4.978}	
	return times[str_beta],rms_mach[str_beta]


def mdotB():
	cs,rho,G,mass,lam= 1.,1.e-2,1.,0.40625,1.12
	return 4*np.pi*lam*(G*mass)**2*rho/cs**3

def mdotBH():
	lam,mach= 1.12,5.
	return mdotB()/lam/(1.+mach**2)**1.5


def mdotB_x(rho_x):
	msinks=13./32
	G=1.
	cs=1.
	return 4.*np.pi*(G*msinks)**2*rho_x/cs**3

def mdotBH_interp_x(vrms_x,rho_x):
	cs=1.
	lam=1.12
	vBH= np.power(cs**2+np.power(vrms_x,2), 0.5)
	phiBH= np.power(1.+np.power(vrms_x,2), 1.5)*np.power(1.+np.power(vrms_x/lam,2), 0.5)/(1.+np.power(vrms_x,4))
	return mdotB_x(rho_x)*phiBH*np.power(vBH/cs,-3)


def get_vB():
	cs= 1.
	return cs

def get_vBH():
	cs,mach= 1.,5.
	return np.sqrt(cs**2+(mach*cs)**2)

def get_vABH(str_beta):
	cs,mach= 1.,5.
	va_rms,beta_rms= ic_rms_vals(str_beta) 
	return np.sqrt(cs**2+ (mach*cs)**2+ va_rms**2)

def get_rB():
	G,M= 1.,13/32.
	return G*M/get_vB()**2

def get_rBH():
	G,M= 1.,13/32.
	return G*M/get_vBH()**2

def get_rABH(str_beta):
	G,M= 1.,13/32.
	return G*M/get_vABH(str_beta)**2

def get_tB():
	return get_rB()/get_vB()

def get_tBH():
	return get_rBH()/get_vBH()

def get_tABH(str_beta):
	return get_rABH(str_beta)/get_vABH(str_beta)

def tBH_to_tBHA(t_div_tBH, str_beta):
	return t_div_tBH*get_tBH()/get_tABH(str_beta)

def mark_to_kaylan_mdot(mdot):
    return mdot/100.

#Lee et al. 2014 mdots
def lee_mach_bh():
	mach=5.
	return (1.+mach**4)**(1./3)/(1.+(mach/1.12)**2)**(1./6)

def lee_parallel(beta,beta_ch=19.8,n=1.,b_norm=False):
	if b_norm:
		# for post-July 15, 2016 code
		return (1./lee_mach_bh()**2)*np.power(lee_mach_bh()**n + np.power(beta_ch/beta, n/2), -1/n)
	else:
		# what my pre-July 15, 2016 code assumes
		return mdotBH()* np.power(1.+ np.power(beta_ch/lee_mach_bh()**2/beta, n/2), -1/n)

def lee_perp(beta,beta_ch=19.8,n=1.):
	a= np.zeros(len(beta))+mdotBH()
	b= lee_mach_bh()*lee_parallel(beta)
	return np.min(np.array([a,b]),axis=0)

def lee_avg_par_perp(beta,beta_ch=19.8,n=1.):
	return 0.5*(lee_parallel(beta) + lee_perp(beta))
