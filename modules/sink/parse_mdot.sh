#!/bin/bash 
##arg 1: pout.0_*_processed to just sink data
##arg 2: output name 
exist=`find ./  -name "${2}"|wc -l`
if [ $exist -gt 0 ]
then
	rm $2	
fi
#fill in file
nlines=`grep $1 -e "coarse time step" |wc -l`
echo "${nlines} #number of coarse time steps" > $2
nvrms=`grep $1 -e "vrms" |wc -l`
echo "${nvrms} #number of vrms occurrences" >> $2
loRes_test=`grep $1 -e "KJB2: level 0 dt="|wc -l`
echo "${loRes_test} #> 0 if loRes stdout" >> $2
