'''
combine processed sink.pickle files then clean up the combined sink object 
'''
import numpy as np
from scipy.interpolate import interp1d 

from modules.sink.rates_and_tscales import get_tBH

def get_nmad(time,mdot,str_beta):
	'''str_beta is a string, options: bInf,b100,b10,b1,b01,b001'''
	min_tBH= dict(bInf=5.,b100=5.,b10=3.,b1=2.,b01=2.,b001=2.)
	t= (time-time[0])/get_tBH()
	imin=np.where(t >= min_tBH[str_beta])[0][0]
	nmad= np.zeros(64)-1
	med=np.median(mdot[:,imin:],axis=1)
	for i in range(mdot.shape[0]):
		nmad[i]= np.median(np.absolute(mdot[i,imin:]-med[i]))
	return nmad,med

class InitSink(object):
    def __init__(self,fsink):
        self.x,self.y,self.z,self.sid= self.read_init_sink(fsink)
    def read_init_sink(self,fsink):
        fin=open(fsink,'r')
        lines=fin.readlines()
        fin.close()
        x,y,z,sid = np.zeros(64),np.zeros(64),np.zeros(64),np.zeros(64).astype('int')
        for i,line in enumerate(lines[1:]):
            x[i],y[i],z[i],sid[i]= float(line.split()[1]),float(line.split()[2]),float(line.split()[3]),int(line.split()[-1])
        return x,y,z,sid
    def xyz_for_sid(self,sid):
        return [self.x[sid],self.y[sid],self.z[sid]]
    def set_coord(self,sid):
        '''useful for slice plot
        sets sink.coord to {} containing x,y,z keys of that sid's location'''
        self.coord=dict(x=self.x[sid],y=self.y[sid],z=self.z[sid])


class SinkData():
	def __init__(self,NSinks,NSinkFiles, rms_local_global=False):
		shape= (NSinks,NSinkFiles)
		self.sid= np.zeros(shape).astype(np.int)-1
		#print mass to 16 decimals (max decimals for double precision) so need float64
		self.CTS= np.zeros(NSinkFiles).astype(np.int)-1 #CTS = coarse time step
		self.old_t= np.zeros(NSinkFiles).astype(np.float64)-1
		self.old_dt= self.old_t.copy()
		self.mass= np.zeros(shape).astype(np.float64)-1
		#local rmsV,rmsVa,meanRho which is read in from LocalData() object, interpolated to sink.time points
		if rms_local_global: 
			self.rmsV= np.zeros(shape).astype(np.float64)-1
			self.rmsVa= self.rmsV.copy()
			self.meanRho= self.rmsV.copy() 
			#global rmsV,rmsVa,meanRho over simulation domain, interpolated to sink.time points 
			self.glob_rmsV= np.zeros(NSinkFiles).astype(np.float64)-1
			self.glob_rmsVa= np.zeros(NSinkFiles).astype(np.float64)-1
			self.glob_meanRho= np.zeros(NSinkFiles).astype(np.float64)-1
	def rmEnds(self,no_info_steps):
		self.CTS= self.CTS[:-no_info_steps]
		self.old_t= self.old_t[:-no_info_steps]
		self.old_dt= self.old_dt[:-no_info_steps]
		self.sid= self.sid[:,:-no_info_steps]
		self.mass= self.mass[:,:-no_info_steps]
	def MdotFine_tFine(self):
		curr_t= self.old_t+self.old_dt
		self.mdot= (self.mass[:,1:]-self.mass[:,:-1])/(curr_t[1:]-curr_t[:-1])
		self.mdotTime= curr_t[:-1]
		#shape mdot= shape mass-1, make a mass array with shape matching mdot for later comparisons
		self.mass_match_mdot= self.mass[:,1:]
		self.old_dt_match_mdot= self.old_dt[1:]
	def rmIndices(self,idel):
		self.mass=np.delete(self.mass,idel,axis=1)
		self.sid= np.delete(self.sid,idel,axis=1)
		self.CTS= np.delete(self.CTS,idel)
		self.old_t= np.delete(self.old_t,idel)
		self.old_dt= np.delete(self.old_dt,idel)
		self.MdotFine_tFine() #update mdots

#self.timeRaw= self.time[:-1]
class LocalData():
	def __init__(self,NSinks,n_hdf5_files,NSinkFiles):
		'''local rmsV,rmsVa,meanRho for each sinks'''
		#data from local_rmsV.txt file: NSinks each with n_hdf5_files time points
		self.time= np.zeros(n_hdf5_files).astype(np.float64)-1
		self.sid= np.zeros(NSinks).astype(np.int)-1
		shape= (NSinks,n_hdf5_files)
		self.rmsV= np.zeros(shape).astype(np.float64)-1
		self.rmsVa= self.rmsV.copy()
		self.meanRho= self.rmsV.copy()

class GlobalData():
	pass


class CombinedSinkData():
	def __init__(self, sink):
		'''initializes itself as a copy of a SinkData() object'''
		self.sid=sink.sid.copy()
		self.mass=sink.mass.copy()
		self.mass_match_mdot=sink.mass_match_mdot.copy()
		self.mdot=sink.mdot.copy()
		self.mdotTime=sink.mdotTime.copy()
		self.CTS=sink.CTS.copy()
		self.old_t=sink.old_t.copy()
		self.old_dt=sink.old_dt.copy()
		self.old_dt_match_mdot=sink.old_dt.copy()
	def concat(self,sink):
		self.sid=np.concatenate((self.sid,sink.sid),axis=1)
		self.mass=np.concatenate((self.mass,sink.mass),axis=1)
		self.mass_match_mdot=np.concatenate((self.mass_match_mdot,sink.mass_match_mdot),axis=1)
		self.mdot=np.concatenate((self.mdot,sink.mdot),axis=1)
		self.mdotTime=np.concatenate((self.mdotTime,sink.mdotTime))
		self.CTS=np.concatenate((self.CTS,sink.CTS))
		self.old_t=np.concatenate((self.old_t,sink.old_t))
		self.old_dt=np.concatenate((self.old_dt,sink.old_dt))
		self.old_dt_match_mdot=np.concatenate((self.old_dt_match_mdot,sink.old_dt_match_mdot))
	def rem_repeated_CTS(self):
		cts_diff=(self.CTS[1:]-self.CTS[:-1]).copy()
		ind=np.where(cts_diff < 1)[0]
		num_repeat=np.abs(cts_diff[ind])
		rm_inds=np.array([])
		for num,i in zip(num_repeat,ind): rm_inds=np.concatenate(( rm_inds,np.arange(i-num,i+1) ))
		self.rmIndices(rm_inds)
	def rem_drops_in_time(self):
		#remove all drops in time
		iproblem= np.where(self.mdotTime[1:]-self.mdotTime[:-1] < 0)[0]
		i_master= [] #append all bad indices and remove all at one time
		for iprob in iproblem:
			idrop= iprob+1
			irem= np.where(self.mdotTime[idrop:idrop+10] < self.mdotTime[idrop-1])[0]
			for j in irem: i_master.append(idrop+j)
		self.rmIndices(i_master)
		#assert(np.where(self.mdotTime[1:]-self.mdotTime[:-1] < 0)[0].size == 0)
	def rem_repeated_time_pts(self):
		#remove repeated time points 
		isame= np.where(self.mdotTime[1:]-self.mdotTime[:-1] == 0)[0]+ 1
		self.rmIndices(isame)
		assert(np.where(self.mdotTime[1:]-self.mdotTime[:-1] == 0)[0].size == 0)
	def rmIndices(self,idel):
		self.mass=np.delete(self.mass,idel,axis=1)
		self.mass_match_mdot=np.delete(self.mass_match_mdot,idel,axis=1)
		self.sid= np.delete(self.sid,idel,axis=1)
		self.CTS= np.delete(self.CTS,idel)
		self.old_t= np.delete(self.old_t,idel)
		self.old_dt= np.delete(self.old_dt,idel)
		self.old_dt_match_mdot= np.delete(self.old_dt_match_mdot,idel)
		self.mdotTime= np.delete(self.mdotTime,idel) #idel is correct b/c mdotTime=old_t[:-1]+old_dt[:-1]
		self.mdot= np.delete(self.mdot,idel,axis=1) #if correct to remove 'idel' from 'mdotTime', then must remove idel from 'mdot' as well
	def sink_xyz(self,f_init_sink):
		self.init= InitSink(f_init_sink)
	def nmad(self,str_beta):
		'''Peter's nmad suggestion for each sink particle, med(abs(data-med)) vs. med'''
		self.nmad,self.med= get_nmad(self.old_t,self.mdot,str_beta)
	def interp_rms_mach(self,final_time,final_rms_mach):
		'''final_time,final_rms_mach are simulation time and rms mach for some time near end of sim
		that data point used to linear interp to rms mach at last time point in sim, then interp func made for that'''
		slope=(final_rms_mach-5.)/(final_time-self.mdotTime[0])
		yint= 5.-slope*self.mdotTime[0]
		last_mach= slope*self.mdotTime[-1]+ yint
		self.f_rms_mach= interp1d([self.mdotTime[0],self.mdotTime[-1]], [5.,last_mach], kind='linear')
	def rm_mdot_systematics(self, t_near_end,rms_mach_near_end):
		'''define new mdots correctly for systematic decrease in rms Mach number in time and incrase in Msink
		decrease in rms Mach number is dominate effect'''
		#we divide out the systematic contribution to mdot
		#mdot \propto M^2/V^3 and we are dividing by this
		self.interp_rms_mach(t_near_end,rms_mach_near_end)
		rms= self.f_rms_mach(self.mdotTime)
		self.mdot_vcorr= np.zeros(self.mdot.shape)
		for i in range(self.mdot.shape[0]):
			self.mdot_vcorr[i,:]= self.mdot[i,:]* np.power(rms/5., 3)
		self.mdot_mcorr= self.mdot* np.power(self.mass_match_mdot/self.mass[0,0], -2)
	def tnorm(self):
		'''return normalized time'''
		return (self.mdotTime-self.mdotTime[0])/get_tBH()
	def old_dt_minima(self,str_beta):
		'''return old_dt (orion2 timestep) for which need to remove all indices with dt <= dt_crit'''
		dt_crit= {'bInfsd3': 3.e-6,\
				'bInf': 1.e-6,\
				'b100': 5.e-6,\
				'b10': 5.e-6,\
				'b1': 3.e-7,\
				'b1e-1': 6.e-7,\
				'b1e-2': 5.e-7}
		return dt_crit[str_beta]

	


if __name__ == "__main__":
	print "need to write unit tests"
	import argparse
	parser = argparse.ArgumentParser(description="test")
	parser.add_argument("-dirs",nargs=3,action="store",help='directories to plot all sink.pickle data from, each directory its own color',required=False)
	args = parser.parse_args()


