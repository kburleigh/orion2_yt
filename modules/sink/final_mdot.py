import numpy as np
import argparse
import glob
import os
from pickle import load,dump

from modules.sink.data_types import SinkData,CombinedSinkData 

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-parsed_dir",action="store",help='directory containing parsed mdot files',required=True)
parser.add_argument("-str_beta",choices=['bInf','bInfsd3','b100','b10','b1','b1e-1','b1e-2',],action="store",help='directory containing parsed mdot files',required=True)
args = parser.parse_args()

files=np.array(glob.glob( os.path.join(args.parsed_dir,'sink.pickle.*')))
#time[0] for each sink.pickle to put in time sequential order
time0=[]
for fil in files:
	print 'loading file: %s' %fil
	fin=open(fil,'r')
	sink=load(fin)
	fin.close()
	time0.append( sink.mdotTime[0] )
isort=np.argsort(np.array(time0))
for fil in files[isort]:
	fin=open(fil,'r')
	sink=load(fin)
	fin.close()
	print 'loaded file: %s, ti=%.10f  tf=%.10f' %(os.path.basename(fil),sink.mdotTime[0],sink.mdotTime[-1])
#concatenate all sink files for this beta,level
#combine sink data
fin=open(files[isort][0],'r')
sink=load(fin)
fin.close()
master_sink= CombinedSinkData(sink) #initialize with SinkData() object
#print "filled master sink, shape master.sid and sink.sid= ",master_sink.sid.shape,sink.sid.shape
for fil in files[isort][1:]:
	fin=open(fil,'r')
	sink=load(fin)
	fin.close()
	master_sink.concat(sink)
	master_sink.concat(sink)
#save and plot for testing
#plt.plot(master_sink.CTS,c='b',label='CTS before')
#plt.plot(master_sink.old_t,c='b',label='old_t before')
master_sink.rem_repeated_CTS()
#plt.plot(master_sink.CTS,c='r',label='after rem CTS')
#plt.plot(master_sink.old_t,c='r',label='old_t after')
#plt.savefig(os.path.join(sink_dir,'master_sink_oldt.png'))
#plt.close()
master_sink.rem_drops_in_time() #duplicated times are not sequential, allows boxcar
try: assert(np.where(master_sink.mdotTime[1:]-master_sink.mdotTime[:-1] < 0)[0].size == 0)
except AssertionError: 
	master_sink.rem_drops_in_time() #sometimes get everythin after 2nd pass
	try: assert(np.where(master_sink.mdotTime[1:]-master_sink.mdotTime[:-1] < 0)[0].size == 0)
	except AssertionError: 
		master_sink.rem_drops_in_time() #sometimes get everythin after 3rd pass
		try: assert(np.where(master_sink.mdotTime[1:]-master_sink.mdotTime[:-1] < 0)[0].size == 0)
		except AssertionError: 
			print np.where(master_sink.mdotTime[1:]-master_sink.mdotTime[:-1] < 0)
			raise AssertionError
#remove repeated indices
master_sink.rem_repeated_time_pts()
#last check
assert(np.array(time0).min() == master_sink.mdotTime[0])
#remove the HUGE mdot spikes since we know old_dt_match_mdot
dt_crit= master_sink.old_dt_minima(args.str_beta)
master_sink.rmIndices( np.where(master_sink.old_dt_match_mdot <= dt_crit)[0] )
#save master_sink if does not exist already
fname= os.path.join(args.parsed_dir,'master_sink_%s.pickle' %args.str_beta)
fout=open(fname,'w')
dump(master_sink,fout)
fout.close()


