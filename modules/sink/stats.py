import numpy as np
import numpy.ma as ma
from pickle import load,dump
from scipy.stats import ttest_ind,pearsonr,ks_2samp,mannwhitneyu
import matplotlib.pyplot as plt
from astropy.stats import sigma_clip
import sys

from modules.sink.data_types import SinkData,CombinedSinkData
from modules.sink.rates_and_tscales import mdotB,get_tBH,t_rms_mach_near_end,steady_state_begins

def index_before_tBH(sink,t,units_tBH=True):
    '''returns nearest index at or before t= tBH'''
    inds= np.where(sink.tnorm() <= t)[0]
    if len(inds) > 0: return inds[-1]
    else:
        print 'no time points exist before sink.tnorm()= ',t
        raise ValueError #no time points

def index_after_tBH(sink,t,units_tBH=True):
    '''returns nearest index at or before t= tBH'''
    inds= np.where(sink.tnorm() >= t)[0]
    if len(inds) > 0: return inds[0]
    else: return -1

def clip_mdots(sink, sigma=3.):
    sink.mdot_vcorr_clip= np.zeros(sink.mdot_vcorr.shape)-100
    i_start=0
    i_end= index_after_tBH(sink,sink.tnorm()[i_start]+0.5,units_tBH=True)
    while True:
        print 'clip_mdots: i_start=%d,i_end-1=%d, t_start=%.2f,t_end=%.2f' % (i_start,i_end-1,sink.tnorm()[i_start],sink.tnorm()[i_end-1])
        if i_start >= sink.mdot.shape[1] or i_start == -1: break
        if i_start == i_end: break
        if i_end >= sink.mdot.shape[1]: i_end= -1
        for i in range(sink.mdot.shape[0]):
            sink.mdot_vcorr_clip[i,i_start:i_end]= sigma_clip(sink.mdot_vcorr[i,i_start:i_end],sig=sigma)
            #print 'sink.mdot_vcorr_clip[i,i_start:i_end]= ', sink.mdot_vcorr_clip[i,i_start:i_end][[0,-1]]
        i_start= i_end
        i_end= index_after_tBH(sink,sink.tnorm()[i_start]+0.5,units_tBH=True)
    if len(np.where(sink.mdot_vcorr_clip[0,:] < 0)[0]) == 1: sink.mdot_vcorr_clip[:,-1]= sink.mdot_vcorr[:,-1]
    else:
        print 'WARNING sink.mdot_vcorr_clip[0,:] < 0 more than once, something bad is happening!, np.where(< 0) = ', np.where(sink.mdot_vcorr_clip[0,:] < 0)[0]
        raise ValueError

def steady_rates(mdotData,timeData,steady_wid,steady_end,units_tBH=True,fill_nan=True,str_beta=None,median=True):
	'''returns 64 length array of time averaged mdots for each sink
	if data does not exist in steady_wid fills with np.nan'''
	assert(units_tBH)
	assert(mdotData.shape[0] == 64)
	imin=np.where(timeData >= steady_end-steady_wid)[0].min()
	imax=np.where(timeData <= steady_end)[0].max()
	ind=range(imin,imax+1)
	try:
		if str_beta is not None:
			# plot median and mean mdot on same panel
			plt.plot(timeData,np.median(mdotData,axis=0)/mdotB(),'b-',label='median')
			plt.plot(timeData,np.mean(mdotData,axis=0)/mdotB(),'g-',label='mean')
			# Get bootstraps using mdot over sinks at each time pt
			med= bootstrap2(mdotData[:,ind]/mdotB(),str_beta, on_median=True)
			mean= bootstrap2(mdotData[:,ind]/mdotB(),str_beta, on_median=False)
			xvals=[timeData[ind].min(),timeData[ind].max()]
			plt.fill_between(xvals,med['low'],med['hi'],color='b',alpha=0.4)
			plt.fill_between(xvals,mean['low'],mean['hi'],color='g',alpha=0.4)
			print 'mdotData.shape=',mdotData.shape,'np.mean(mdotData,axis=0).shape=',np.mean(mdotData,axis=0).shape
			plt.legend(loc=0)
			plt.yscale('log')
			plt.savefig('medmean_%s.png' % str_beta)
			plt.close()
			# Also store these to text file
			fin=open('bootstrap_2.txt','a')
			fin.write('#beta sample_med -booterr +booterr sample_mean -booterr +booterr\n')
			fin.write('%s %.5g %.5g %.5g %.5g %.5g %.5g\n' % \
						(str_beta,med['sample_med'],np.abs(med['med']-med['low']),np.abs(med['med']-med['hi']),\
							 mean['sample_mean'],np.abs(mean['med']-mean['low']),np.abs(mean['med']-mean['hi'])))
			fin.close()
			# indiv sink, median mean in steady state
			#for sid in [0,10,20,30,55]:
			#	plt.plot(timeData[ind],mdotData[sid,ind]/mdotB(),'k-')
			#	xvals=[timeData[ind].min(),timeData[ind].max()]
			#	plt.plot(xvals,[np.median(mdotData[sid,ind])/mdotB()]*2,'b-',label='median')
			#	plt.plot(xvals,[np.mean(mdotData[sid,ind])/mdotB()]*2,'g-',label='mean')
			#	plt.legend(loc=0)
			#	plt.yscale('log')
			#	plt.savefig('medmean_sid%d_%s.png' % (sid,str_beta))
			#	plt.close()
			# Make plots showing rms fluctuation for individual sink
			#for sid in [0,10,20,30,55]:
			#	plt.plot(timeData[ind],mdotData[sid,ind]/mdotB(),'b-')
			#	xvals=[timeData[ind].min(),timeData[ind].max()]
			#	med= np.median(mdotData[sid,ind])/mdotB()	
			#	std= np.std(mdotData[sid,ind]/mdotB() - med)
			#	plt.plot(xvals,[med]*2,'k--',label='median')
			#	plt.plot(xvals,[med+std]*2,'r--',label='+std')
			#	plt.plot(xvals,[med-std]*2,'r--',label='-std')
			#	plt.plot(xvals,[med+2*std]*2,'g--',label='+2std')
			#	plt.plot(xvals,[med-2*std]*2,'g--',label='-2std')
			#	plt.legend(loc=0)
			#	plt.yscale('log')
			#	plt.savefig('std_indivsink_sid%d_%s' % (sid,str_beta))
			#	plt.close()
			# Compute median 1-sigma fluctuation for all sinks, bootstrap the errors
			mdot= np.log10(mdotData[:,ind]/mdotB())
			med= np.percentile(mdot,q=50.,axis=1)
			sig_sink={}
			for key,q in zip(['low','hi'],[15.8,84.2]):
				sig_sink[key]= np.abs(med - np.percentile(mdot,q=q,axis=1))
				assert(len(sig_sink[key]) == 64)
				#sig_sink[key]= np.median(sig_sink[key])
			#one_sigma= 1.*np.std(mdot.T - med,axis=0)
			# take median of one_sigmas and bootstrap it for error
			#med= np.median(one_sigma)
			#d_boots,final,std_pdf= bootstrap(one_sigma,str_beta,print_results=False)
			print "INDIV SINK MEDIAN 1-SIGMA  = percentile(log10(mdot/mdotB)) then median over sinks: $median^{+q75}_{-q25}$"
			print "%s: $^{+%.8f}_{-%.8f}$" % (str_beta,np.median(sig_sink['hi']),np.median(sig_sink['low']))
			for sid in [0,4,20,42,55,63]:
				plt.plot(timeData[ind],np.log10(mdotData[sid,ind]/mdotB()),'b-')
				xvals=[timeData[ind].min(),timeData[ind].max()]
				plt.fill_between(xvals,med[sid]-sig_sink['low'][sid],med[sid]+sig_sink['hi'][sid],color='b',alpha=0.4)
				plt.savefig('onesig_sid-%d_%s.png' % (sid,str_beta))
				plt.close()
			#a,b,c=plt.hist(two_sigmas)
			#plt.plot([med]*2,[0,np.max(a)],'b--',label='median')
			#plt.plot([final['med']-final['med_low']]*2,[0,np.max(a)],'r--',label='- err median')
			#plt.plot([final['med']+final['med_hi']]*2,[0,np.max(a)],'r--',label='+ err median')
			#plt.legend(loc=0)
			#plt.savefig('twosigma_indivsink_%s' % str_beta)
			#plt.close()
		if median:
			return np.median(mdotData[:,ind],axis=1)
		else:
			print '----WARNING----: MEAN over steady state, bool_median=',median
			return np.mean(mdotData[:,ind],axis=1)
	except IndexError:
		print 'WARNING: no data between %.1f < t/tBH < %.1f' % (steady_end-steady_wid,steady_end)
		return np.empty(mdotData.shape[0])*np.nan 

def get_num_intervals(sink,tbeg,twid, tend=1000.):
    '''returns how many bins of width twid there are between tbeg and tend. 
    tend default = 1000 so very last index returned'''
    ibeg= index_before_tBH(sink,tbeg)
    iend= index_before_tBH(sink,tend)
    return int((sink.tnorm()[iend]-sink.tnorm()[ibeg])/twid)+ 1 #add one to get the number correct

def sequential_pdfs(sink,tbeg,twid,corr=True):
	n_intervals= get_num_intervals(sink,tbeg,twid)
	pdfs= np.empty((n_intervals,64))*np.nan
	times= []
	for i in range(n_intervals):
		if corr: pdfs[i,:]= steady_rates(sink.mdot_vcorr,sink.tnorm(),twid,tbeg+(i+1)*twid,units_tBH=True)
		else: pdfs[i,:]= steady_rates(sink.mdot,sink.tnorm(),twid,tbeg+(i+1)*twid,units_tBH=True)
		times.append( tbeg+(i+0.5)*twid )
	return pdfs,np.array(times)

def moving_window_pdfs(sink,dt=0.1,wind_dt=1.,corr=True):
	tbeg,tmax= 0.,sink.tnorm()[-1] 
	pdfs= [] #ends up as (N,64) shape numpy array 
	time_LHS= [] #'' (N,) shape ''
	tend= tbeg+wind_dt
	while tend <= tmax:
		if corr: pdfs.append( steady_rates(sink.mdot_vcorr,sink.tnorm(),wind_dt,tend,units_tBH=True) )
		else: pdfs.append( steady_rates(sink.mdot,sink.tnorm(),wind_dt,tend,units_tBH=True) )
		time_LHS.append( tbeg )
		tbeg+= dt
		tend= tbeg+wind_dt
	return np.array(pdfs),np.array(time_LHS)

def approx_steady_pdf(sink,corr=True):
	tmax= min(20., sink.tnorm()[-1])
	dt=1.
	sample=[]
	for cnt in range(2): #,100):
		tend= tmax- cnt*dt
		if corr: sample+= list( steady_rates(sink.mdot_vcorr,sink.tnorm(),dt,tend,units_tBH=True,fill_nan=True) )  
		else: sample+= list( steady_rates(sink.mdot,sink.tnorm(),dt,tend,units_tBH=True,fill_nan=True) )  
	sample= np.array(sample)
	#check data is good
	assert( np.all(np.isfinite(sample)) )
	assert(len(sample.shape) == 1) 
	return sample 

def steady_state_test_rw_to_rw(sink,dt=0.1,wind_dt=1.,corr=True):
	pdfs,time_LHS= moving_window_pdfs(sink,dt=dt,wind_dt=wind_dt,corr=corr)
	assert(pdfs.shape[0] == len(time_LHS))
	#compare pdfs
	welch= dict(t=[],p=[])
	ks=dict(t=[],p=[])
	U=dict(t=[],p=[])
	for i in range(len(time_LHS)-1):
		s1= pdfs[i,:] 
		s2= pdfs[i+1,:] 
		if np.all( np.isfinite(s1) ) and  np.all( np.isfinite(s2) ): 
			t_p= ttest_ind(s1,s2, equal_var=False) 
			welch['t'].append( t_p[0] )
			welch['p'].append( t_p[1] )
			t_p= ks_2samp(s1,s2) 
			ks['t'].append( t_p[0] )
			ks['p'].append( t_p[1] )
			t_p= mannwhitneyu(s1,s2) 
			U['t'].append( t_p[0] )
			U['p'].append( t_p[1] )
		else: #no data found between tbeg and tbeg+twid 
			welch['t'].append( -1 )
			welch['p'].append( -1 )
			ks['t'].append( -1 )
			ks['p'].append( -1 )
			U['t'].append( -1 )
			U['p'].append( -1 )
	return time_LHS[:-1],welch,ks,U

def steady_state_test_rw_to_end(sink,corr=True):
	steady_pdf= approx_steady_pdf(sink,corr=corr)
	pdfs,time_LHS= moving_window_pdfs(sink,dt=0.1,wind_dt=1.,corr=corr)
	assert(pdfs.shape[0] == len(time_LHS))
	#compare pdfs
	welch= dict(t=[],p=[])
	ks=dict(t=[],p=[])
	U=dict(t=[],p=[])
	for i in range(len(time_LHS)):
		s1= pdfs[i,:] 
		if np.all( np.isfinite(s1) ) and  np.all( np.isfinite(steady_pdf) ): 
			t_p= ttest_ind(s1,steady_pdf, equal_var=False) 
			welch['t'].append( t_p[0] )
			welch['p'].append( t_p[1] )
			t_p= ks_2samp(s1,steady_pdf) 
			ks['t'].append( t_p[0] )
			ks['p'].append( t_p[1] )
			t_p= mannwhitneyu(s1,steady_pdf) 
			U['t'].append( t_p[0] )
			U['p'].append( t_p[1] )
		else: #no data found between tbeg and tbeg+twid 
			welch['t'].append( -1 )
			welch['p'].append( -1 )
			ks['t'].append( -1 )
			ks['p'].append( -1 )
			U['t'].append( -1 )
			U['p'].append( -1 )
	for key in welch.keys():
		welch[key]= np.array(welch[key])
		ks[key]= np.array(ks[key])
		U[key]= np.array(U[key])
	return time_LHS,welch,ks,U

def get_pdf_cdf(sample,bins):
	'''given sample of pts and bins to use, return pdf and cdf'''
	pdf,j1,j2= plt.hist(sample,bins=bins,normed=True)
	cdf,j1,j2=plt.hist(sample,bins=bins,cumulative=True,normed=True)
	plt.close()
	try: assert( ((bins[1:]-bins[:-1])*pdf).sum() == 1. )
	except AssertionError: 
		print '---- WARNING  sum not 1, what is it? ------------ '
		print '((bins[1:]-bins[:-1])*pdf).sum()= ',((bins[1:]-bins[:-1])*pdf).sum()
	return pdf,cdf


def pdf_for_model(sink, str_beta,mdot_low,mdot_hi,nsinks=64,corr=True):
	'''returns mdot pdf of 64*(tend-tbeg)/dt points following Peter's idea
	Returns: array of mdots for pdf, list of non nan indices of that arraydict of models, each model is a dict of the models it was ttest compared to
	'''
	#time steady state begins from KS 2 samp stat
	tbeg= steady_state_begins(str_beta,units_tBH=True)
	tend= tbeg+3.
	if corr: sample= steady_rates(sink.mdot_vcorr,sink.tnorm(),tend-tbeg,tend,units_tBH=True,fill_nan=True)
	else: sample= steady_rates(sink.mdot,sink.tnorm(),tend-tbeg,tend,units_tBH=True,fill_nan=True)
	#check data is good
	assert( np.all(np.isfinite(sample)) ) #robust, either data exist in above range or not, will not have good data and nans in this sample
	#is desired, take subset of sinks
	sink_ids=np.arange(64)
	np.random.shuffle(sink_ids)
	sample= sample[ sink_ids[:nsinks] ]
	#can safely take log10 of rates now
	sample= np.log10(sample)
	#equal log spaced bins covering entire range of mdots
	bins= np.linspace(np.log10(mdot_low/2),np.log10(mdot_hi*2),num=41) # N bins = num-1
	pdf,cdf= get_pdf_cdf(sample,bins)
	print 'sample= ',sample
	print 'bins= ',bins
	print 'pdf= ',pdf
	print 'cdf= ',cdf
	return sample,bins,pdf,cdf

def pdf_for_model_64_x_N(sink, str_beta,mdot_low,mdot_hi,nsinks=64,corr=True,last_2tBH=True,dt=2.,median=True):
	'''default: compute 64 point PDF taking median each sink over last 2 tBH
	can change dt = 1 to take 64x2 point PDF over last 2tBH
	or even dt=1 and last_2tBH=False to take 64xN point pdf between tsteady and tend of every model
	'''
	if last_2tBH: 
		tbeg= -2.+ min(19.8,sink.tnorm()[-1])
		assert(tbeg >= steady_state_begins(str_beta,units_tBH=True))
	else: tbeg= steady_state_begins(str_beta,units_tBH=True) #time steady state begins from KS 2 samp stat
	sample=[]
	for cnt in range(1,100):
		tend= tbeg+cnt*dt
		if tend > min(20.,sink.tnorm()[-1]): break
		if median:
			# Take median mdot over steady state time range for each sink
			if corr: sample+= list( steady_rates(sink.mdot_vcorr,sink.tnorm(),dt,tend,units_tBH=True,fill_nan=True,str_beta=str_beta,median=median) )  
			else: sample+= list( steady_rates(sink.mdot,sink.tnorm(),dt,tend,units_tBH=True,fill_nan=True,str_beta=str_beta,median=median) )  
		else:
			# Take mean
			if corr: sample+= list( steady_rates(sink.mdot_vcorr,sink.tnorm(),dt,tend,units_tBH=True,fill_nan=True,str_beta=str_beta,median=False) )  
			else: sample+= list( steady_rates(sink.mdot,sink.tnorm(),dt,tend,units_tBH=True,fill_nan=True,str_beta=str_beta,median=False) )  
		print 'inside loop pdf_for_model_64_x_N, dt= ',dt,'tend= ',tend
	sample= np.array(sample)
	#sanity check
	print 'finished pdf_for_model_64_x_N, model=%s, len(sample)=%d' % (str_beta,len(sample))
	if last_2tBH and dt > 1.01: assert(len(sample) == 64)
	elif last_2tBH and dt < 1.01: assert(len(sample) == 128)
	else: assert(len(sample) >= 128)
	#check data is good
	assert( np.all(np.isfinite(sample)) ) #robust, either data exist in above range or not
	#can safely take log10 of rates now
	sample= np.log10(sample)
	#equal log spaced bins covering entire range of mdots
	bins= np.linspace(np.log10(mdot_low/2),np.log10(mdot_hi*2),num=41) # N bins = num-1
	pdf,cdf= get_pdf_cdf(sample,bins)
	print 'bins[[0,-1]]=  ',bins[[0,-1]]
	print 'pdf= ',pdf
	print 'cdf= ',cdf
	return sample,bins,pdf,cdf

def bootstrap(full_sample,str_beta,sub_sample_size=64,print_results=True,median=True):
	'''Returns 3 dictionaries:
	d_boots -- bootstrap samples full_sample, takes the median each time, this is array of those medians
	final -- median of those medians and the std error on the median 
	std_pdf -- std dev of accretion rate pdf
	---
	boostrap sample with replacement Ntimes
	default is assuming all 64 sinks, so subsample size is 64 pts
	if full_sample is 128 pts, so 64xN N=2, then let sub_sample_size=128'''
	assert(len(full_sample) <= 64 or len(full_sample) == 128) #64 pt PDF over last 2tBH or 128 pt PDF over last 0-1 and 1-2 tBH 
	assert(len(full_sample) % sub_sample_size == 0)
	if len(full_sample) == 128: sub_sample_size=128 #hard code safety value, if 64xN N=2 then resample all 128 pts, just like do for 64 PDF
	Ntimes=min(sub_sample_size*(sub_sample_size-1),1000)
	n_intervals= len(full_sample)/sub_sample_size
	if median: d_boots= dict(med=[])
	else: d_boots= dict(mean=[])
	#median and errors on medians 
	i_beg,i_end= 0,sub_sample_size
	for cnt in range(n_intervals):
		sample= full_sample[i_beg:i_end]
		#bootstrap sample with replacement
		for ith_boot in range(Ntimes):
			i_sub= np.random.randint(len(sample), size=len(sample))
			sub= sample[i_sub]
			assert(i_sub.max() <= sub_sample_size-1)
			assert(len(sub) == len(sample))
			if median:
				d_boots['med'].append( np.median(sub) )
			else:
				d_boots['mean'].append( np.mean(sub) )
		i_beg+= sub_sample_size
		i_end+= sub_sample_size
	for key in d_boots.keys(): d_boots[key]= np.array(d_boots[key])
	#final measurement is median of (median or mean distribution)
	# +/- 34.2 in rank
	final={}
	for key in d_boots.keys():
		# Median of distribution is most likely value...
		final[key]= np.percentile(d_boots[key],50.)
		final[key+'_low']= np.abs(final[key]- np.percentile(d_boots[key],15.8))
		final[key+'_hi']= np.abs(final[key]- np.percentile(d_boots[key],84.2))
	#68 percentile width for full PDF of mdots
	std_pdf={}
	junk_med= np.percentile(full_sample,50.)
	std_pdf['low']= np.abs(junk_med- np.percentile(full_sample,15.8))
	std_pdf['hi']= np.abs(junk_med- np.percentile(full_sample,84.2))
	std_pdf['stddev']= np.std(full_sample)
	#print measurements
	if print_results:
		print "FINAL MEASURMENT: model=%s, $median^{+q75}_{-q25}$ & $^{+upper_sigma}_{-lower_sigma}$ stddev_mdotpdf" % str_beta
		for key in d_boots.keys():
			print "%s: $%.8f^{+%.8f}_{-%.8f}$ & $^{+%.8f}_{-%.8f}$ %.8f" % \
				(key,final[key],final[key+'_low'],final[key+'_hi'],std_pdf['low'],std_pdf['hi'],std_pdf['stddev'])
		# Also store these to text file
		fin=open('bootstrap_PDF.txt','a')
		fin.write('#beta sample_med -booterr +booterr -stdpdf +stdpdf\n')
		fin.write('%s %.5g %.5g %.5g %.5g %.5g\n' % \
					(str_beta,np.median(full_sample),final[key+'_low'],final[key+'_hi'],std_pdf['low'],std_pdf['hi']))
		fin.close()
	return d_boots,final,std_pdf


def bootstrap2(mdotSteady,str_beta,on_median=True):
	'''mdotSteady.shape = (64 or 128,ntimepts during steady state)
	compute error on median or mean using mdotSteady whcih has shape (64,time pts) insetady of shape(64) as bootstrap does above
	d_boots -- bootstrap samples full_sample, takes the median each time, this is array of those medians
	final -- median of those medians and the std error on the median 
	std_pdf -- std dev of accretion rate pdf
	---
	boostrap sample with replacement Ntimes
	default is assuming all 64 sinks, so subsample size is 64 pts
	if full_sample is 128 pts, so 64xN N=2, then let sub_sample_size=128'''
	nsinks= mdotSteady.shape[0]
	assert(nsinks == 64 or nsinks == 128) #64 pt PDF over last 2tBH or 128 pt PDF over last 0-1 and 1-2 tBH 
	Ntimes=min(nsinks*(nsinks-1),1000)
	vals=dict(boots=np.zeros(Ntimes)+np.nan,med=0,low=0,hi=0) 
	#bootstrap sample with replacement
	for ith_boot in range(Ntimes):
		r_sids= np.random.randint(nsinks, size=nsinks)
		sub= mdotSteady[r_sids,:]
		assert(sub.shape == mdotSteady.shape)
		if on_median:
			# median over sinks
			mdot= np.median(sub,axis=0)
		else:
			# mean over sinks
			mdot= np.mean(sub,axis=0)
		# Once we have the median or mean curve, take median to get single value
		vals['boots'][ith_boot]= np.median(mdot) 
	assert(np.all(np.isfinite(vals['boots'])))
	#final measurement is median of (median or mean distribution) +/- 34.2 in rank
	# Sample stats
	vals['sample_med']= np.median( np.median(mdotSteady,axis=0) )
	vals['sample_mean']= np.median( np.mean(mdotSteady,axis=0) )
	# Uncertainty on median or mean
	vals['med']= np.percentile(vals['boots'],50.)
	vals['low']= np.percentile(vals['boots'],15.8)
	vals['hi']= np.percentile(vals['boots'],84.2)
	return vals



#Bootstrap to get error bars on median measurement
def final_measurement(arr,median=True):
	'''arr is pdf['arr'], the sample of median accretion rates, as dictionary for all models'''
	final={}
	for str_beta in arr.keys():
		if str_beta == 'hydro_both': continue
		else:
			final[str_beta]= {}
			final[str_beta]['d_boots'],final[str_beta]['final'],final[str_beta]['std_pdf']= \
				bootstrap(arr[str_beta],str_beta,sub_sample_size=64,median=median)
	return final



def ttest_simult_two_sinks(sinkA,sinkB,tbeg,twid, equal_var=False):
	'''compute student t-statistic (default: welch's unequal var) for two sample populations
	sample populations are two different beta modelseach of 64 points, 
	s1=sink A median mdot [tbeg, tbeg+twid]
	s2=sink B median mdot [tbeg, tbeg+twid]
	repeat that pattern until run out of time points for sink with least number time points
	Returns: dict of lists of time,t-statistic,p-value'''
	n_intervals= min(get_num_intervals(sinkA,tbeg,twid),get_num_intervals(sinkB,tbeg,twid))
	stud_t=dict(time=[],t=[],p=[])
	for i in range(1,n_intervals+1):
		s1= steady_rates(sinkA.mdot,sinkA.tnorm(),twid,tbeg+i*twid,units_tBH=True)
		s2= steady_rates(sinkB.mdot,sinkB.tnorm(),twid,tbeg+i*twid,units_tBH=True)
		if np.all( np.isfinite(s1) ) and  np.all( np.isfinite(s2) ): 
			t_p= ttest_ind(s1,s2, equal_var=equal_var) 
			stud_t['t'].append( t_p[0] )
			stud_t['p'].append( t_p[1] )
			stud_t['time'].append( (tbeg+(i-1)*twid + tbeg+i*twid)/2 ) #middle of t range
		else: #no data found between tbeg and tbeg+twid 
			stud_t['t'].append( -1 )
			stud_t['p'].append( -1 )
			stud_t['time'].append( (tbeg+(i-1)*twid + tbeg+i*twid)/2 ) #middle of t range
	return stud_t


def pdf_for_model_mean_of_many_pdfs(sink, str_beta,mdot_low,mdot_hi, tend=20.,dt=0.1):
	'''returns mdot pdf of 64*(tend-tbeg)/dt points following Peter's idea
	Returns: array of mdots for pdf, list of non nan indices of that arraydict of models, each model is a dict of the models it was ttest compared to
	'''
	#corr mdots	
	end_t,end_mach= t_rms_mach_near_end(str_beta)
	sink.rm_mdot_systematics(end_t,end_mach)
	#time steady state begins from KS 2 samp stat
	tbeg= steady_state_begins(str_beta,units_tBH=True)
	#median accretion rate in 0.1 bins between t=2tBH and t=end
	n_intervals= get_num_intervals(sink,tbeg,dt, tend=tend)
	pdfs= np.empty((n_intervals,64))*np.nan
	for ibin in range(n_intervals):
		#get 64 steady mdots for this time range
		pdfs[ibin,:]= steady_rates(sink.mdot_vcorr,sink.tnorm(),dt,tbeg+(ibin+1)*dt,units_tBH=True,fill_nan=True)
	#mask the array where have nans
	pdfs= ma.array(pdfs)
	pdfs.mask= np.isfinite(pdfs) == False
	#can safely take log10 of rates now
	pdfs= np.log10(pdfs)
	#equal log spaced bins covering entire range of mdots
	bins= np.linspace(np.log10(mdot_low/2),np.log10(mdot_hi*2),num=41) # N bins = num-1
	#bins= bins[::-1] #start with smallest number first (most negative)
	print 'bins= ',bins
	#bin up data
	hist_vals= np.empty((n_intervals,len(bins)-1))*np.nan
	for i in range(n_intervals):	
		if np.any(pdfs.mask[i,:]): continue 
		hist_vals[i,:],b,c=plt.hist(pdfs[i,:],bins=bins)
	plt.close()
	hist_vals= ma.array(hist_vals)
	hist_vals.mask= np.isfinite(hist_vals) == False
	#compute averaged histogram
	final_hist= np.round(np.mean(hist_vals,axis=0)).astype(int) #average each bin count over n_intervals, round to nearest count, save as integer
	#convert histogram into array of values
	bin_c= (bins[1:]+bins[:-1])/2
	final_arr=[]
	for cnt,c in enumerate(bin_c): final_arr+= [c]*final_hist[cnt]
	assert(len(final_arr) == final_hist.sum())
	pdf,junk1,junk2= plt.hist(final_arr,bins=bins,normed=True)
	cdf,j1,j2=plt.hist(final_arr,bins=bins,cumulative=True,normed=True)
	plt.close()
	try: assert( ((bins[1:]-bins[:-1])*pdf).sum() == 1. )
	except AssertionError: 
		print '---- WARNING  sum not 1, what is it? ------------ '
		print '((bins[1:]-bins[:-1])*pdf).sum()= ',((bins[1:]-bins[:-1])*pdf).sum()
		#raise AssertionError
	#return array of values, bins, pdf and cdf
	return np.array(final_arr),bins,pdf,cdf


def correlation_bw_sinks(ref_sink,other_sinks, cut=True):
	'''ref sink -- CombinedSinkData() object
	other_sinks -- dictionary of CombinedSinkData() objects
	cut -- removes first 1 tBH from mdot data b/c this is transiet phase and always increases, we want to know if equilibrium rates are correlated
	correlation between ref sink and sinks in N different models, either at same location as ref sink or diff location
	returns pearson r correlation coeff for same location and different location
	'''
	r_same_loc, r_diff_loc= [],[]
	for key in other_sinks.keys():
		size= min(other_sinks[key].mdot.shape[1],ref_sink.mdot.shape[1])
		#if cut=False, then size is all we need, if cut=True then if ref starts at 10 and other at 2 then cannot just grab 10:size and 2:size, would be unequal array lengths
		cut_ref= index_before_tBH(ref_sink,1.)
		cut_other= index_before_tBH(other_sinks[key],1.)
		end_ref= size-other_sinks[key].mdot[:,:cut_other].shape[1] #if other starts at index 10, and ref at 5, then ref should end sooner than other (size - 10)
		end_other= size-ref_sink.mdot[:,:cut_ref].shape[1]
		#number of indices to grab has been figured out, finallly lets do pearson r calculation 
		for i in range(64):
			if i == 0: #same location 
				if cut: r_same_loc.append( pearsonr(other_sinks[key].mdot[i,cut_other:end_other],ref_sink.mdot[0,cut_ref:end_ref])[1] )
				else: r_same_loc.append( pearsonr(other_sinks[key].mdot[i,:size],ref_sink.mdot[0,:size])[1] )
			else: #different location 
				if cut: r_diff_loc.append( pearsonr(other_sinks[key].mdot[i,cut_other:end_other],ref_sink.mdot[0,cut_ref:end_ref])[1] )
				else: r_diff_loc.append( pearsonr(other_sinks[key].mdot[i,:size],ref_sink.mdot[0,:size])[1] )
	assert(len(r_same_loc) == len(other_sinks.keys()) )
	assert(len(r_diff_loc) == (len(other_sinks.keys())*(64-1) ) )
	return r_same_loc,r_diff_loc

def correlation_same_model(sink,i_ref, cut=True):
	'''sink -- CombinedSinkData() object
	i_ref -- id/index of sink to use as reference
	cut -- removes first 1 tBH from mdot data
	'''
	r_same_mod=[]
	if cut:  
		icut= index_before_tBH(sink,1.)
	for i in range(64):
		if i == i_ref: continue
		elif cut: r_same_mod.append( pearsonr(sink.mdot[i,icut:],sink.mdot[i_ref,icut:])[1] ) 
		else: r_same_mod.append( pearsonr(sink.mdot[i,:],sink.mdot[i_ref,:])[1] ) 
	assert(len(r_same_mod) == 64-1 )
	return r_same_mod



