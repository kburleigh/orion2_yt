
'''
WARNING: to correctly combine pout.0_* or out_* files:
    grep out_1 -e step|tail
    grep out_2 -e step|head
    if first 'coarse time step' in `grep out_2 -e step|head` also occurs in out2 then:
        good_step= 1 less than overlapping coarse time step in above 'if statement'
        grep out_1 -e 'coarse time step  good_step' -n
        go to that line in out_1
        delete everything after it (eg "coarse time step" printed after all subcycling is done, which means my 'KJB: advance level' statments have all printed for that coarse time step already)
    then go to last 'coarse time step' in every out_* file and delete any time/sink info that comes after it (if hasn't already been done). if walltime got exceeded the time/sink info printing stopped midway without coarse time step telling you it is there so this script will get messed up
    cat out_1 out_2 > out_12

dependecies:
    Combined standard out files: pout.0_* or out_* 
    Sim_bash_MdotEveryTstep.sh -- does inital parsing of standard out from simulation 
input: 
    standard out from simulation (or concatenation of many standard outputs) giving sink particle info every finest-level step (AMRLevelOrion.cpp, SinkParticleList.cpp print statements).
output: 
    pickle file "mdots_EveryStep*.pickle" containing time,mass,mdot for every sink on the finest-level
'''


def extract_loRes(lines,sink):
	print "assert: loRes file"
	step_str= "KJB2: level 0 dt="
	step=0 #with FixedhiRes, time = 0 is the first coarst time read in
	i=0
	while i < len(lines):
		if lines[i].startswith(step_str): 
			if lines[i-1].startswith('coarse time step'): #for loRes, if have 'step_str' very small chance that sink info does not follow it, but if 'coarse time step' preceeds it, then sink info guarenteed to be there
				i=i-1 #coarse time step line
				sink.CTS[step]= np.around(np.float64(lines[i].split()[3]),decimals=16)
				sink.old_t[step]= np.around(np.float64(lines[i].split()[-5]),decimals=16)
				sink.old_dt[step]= np.around(np.float64(lines[i].split()[-1]),decimals=16)
				#get sink info
				i=i+2 
				sid=0 
				#loop over sink lines
				while sid < args.NSinks:
					#print "CTS, time, step=  ",sink.CTS[step],sink.time[step],step
					info= lines[i].split()
					sink.sid[sid,step]= np.int(info[0])
					sink.mass[sid,step]= np.around(np.float64(info[1]),decimals=16)
					#update counters
					sid+=1  #one sink's info saved, do so for the next
					i+= 1   #make i the last line we read
				#exited while loop
				i+= 1  #back to "step_str", +1 more to get to next line
				step+=1  #for sink info at next coarse time step
			else: i+=1
		else: i+=1   #read the next line
	extra_lines= len(sink.CTS)-len(np.where(sink.CTS > -1)[0])
	print 'extra_lines= ',extra_lines
	if extra_lines > 0: sink.rmEnds(extra_lines) #if use 0, sink.data[:0]  will erase data

def extract_hiRes(lines,sink,vrms_test):
	if vrms_test > 0: print "assert: hiRes file, vrms"
	else: print "assert: hiRes file, NO vrms"
	step_str= "coarse time step"
	sink_str="Id,cnt,dt,mass,x,y,z="
	step=0 #with FixedhiRes, time = 0 is the first coarst time read in
	i=0
	while i < len(lines):
		if lines[i].startswith('coarse time step KJB:AMRLevelOrion.cpp'): #rare but possible
			i+=1
		elif lines[i].startswith(step_str):  #info after this line is for all sinks
			#print "lines[i]= ",lines[i]
			#print "lines[i-3]= ",lines[i-3]
			if lines[i-3].startswith(sink_str): #avoids first few CTS if they don't have sink info
				#print "filling CTS"
				sink.CTS[step]= np.around(np.float64(lines[i].split()[3]),decimals=16)
				sink.old_t[step]= np.around(np.float64(lines[i].split()[-5]),decimals=16)
				sink.old_dt[step]= np.around(np.float64(lines[i].split()[-1]),decimals=16)
				#get sink info
				sid=0  #sink id
				if vrms_test > 0: 
					i-=66
				else: i-=64
				while sid < args.NSinks:
					if sid == 0: print "first type of hiRes file"
					#print "CTS, time, step=  ",sink.CTS[step],sink.time[step],step
					info= lines[i].split()
					if info[0] != sink_str: 
						print info[0],"!=",sink_str
						raise ValueError
					sink.sid[sid,step]= np.int(info[1])
					sink.mass[sid,step]= np.around(np.float64(info[4]),decimals=16)
					#update counters
					sid+=1  #one sink's info saved, do so for the next
					i+= 1   #make i the last line we read
				#exited while loop
				if vrms_test > 0: i+=2+1 #vrms, then +2 to get back to "step_str", +1 more to get to next line
				else: i+= 1   #no vrms, then +1 to get to next line
				step+=1 #"step" is the index where sink info at the NEXT TIME will go
			elif lines[i-2].startswith('AMRLevelOrion::postTimeStep 0 finished'): #verbose rms stdout file
				if vrms_test > 0:
					#print "filling CTS"
					sink.CTS[step]= np.around(np.float64(lines[i].split()[3]),decimals=16)
					sink.old_t[step]= np.around(np.float64(lines[i].split()[-5]),decimals=16)
					sink.old_dt[step]= np.around(np.float64(lines[i].split()[-1]),decimals=16)
					#get sink info
					sid=0  #sink id
					if args.max_level == 3: goBack,goAhead = 86,22
					elif args.max_level == 4: goBack,goAhead = 88,24
					elif args.max_level == 5: goBack,goAhead = 90,26
					else: 
						print "WARNING: need set goBack,goAhead for this case"
						raise ValueError
					i-=goBack
					while sid < args.NSinks:
						if sid == 0: 
							print "second type of hiRes file"
						#print "CTS, time, step=  ",sink.CTS[step],sink.time[step],step
						info= lines[i].split()
						if info[0] != sink_str:
							print info[0],"!=",sink_str
							raise ValueError
						sink.sid[sid,step]= np.int(info[1])
						sink.mass[sid,step]= np.around(np.float64(info[4]),decimals=16)
						#update counters
						sid+=1  #one sink's info saved, do so for the next
						i+= 1   #make i the last line we read
					i+=goAhead+1 #+26 to get back to "step_str", +1 more to get to next line
					step+=1
				else: i+=1
			else: i+=1 #read the next line
		else: i+=1 
	extra_lines= len(sink.CTS)-len(np.where(sink.CTS > -1)[0])
	print 'extra_lines= ',extra_lines
	if extra_lines > 0: sink.rmEnds(extra_lines)	

def make_sanity_plots(sink,jobid,args):
	fig,axes=plt.subplots(2,1)
	plt.subplots_adjust(hspace=0.25)
	ax=axes.flatten()
	ax[0].plot(sink.old_t,sink.old_dt)
	ax[0].set_xlabel('old_t')
	ax[0].set_ylabel('old_dt')
	ax[0].set_yscale('log')
	ax[1].plot(sink.mdotTime,np.mean(sink.mdot,axis=0))
	ax[1].set_xlabel('curr_t')
	ax[1].set_ylabel('mdot (sink-averaged)')
	plt.savefig(os.path.join(args.path,'dt_mdot_'+jobid+'.png'))
	plt.close()

#for global and local data
def interp_coarse_to_fine(coarse_x,coarse_y,  fine_x):
	'''sink.time data occurs earlier and later than hdf5 data which can cause issue
	this func sets earlier occuring data equal to first time hdf5 data we have
	and later occuring data equal to last time hdf5 data we have'''
	ihalf= coarse_x.shape[0]/2
	ibeg= np.where(fine_x <= coarse_x[ihalf])[0]
	iend= np.where(fine_x > coarse_x[ihalf])[0]
	# +-3 for safety to bound larger time domain
	fbeg= interpolate.interp1d(coarse_x[:ihalf+3],coarse_y[:ihalf+3],\
							   bounds_error=False,fill_value=coarse_y[0]) 
	fend= interpolate.interp1d(coarse_x[ihalf-3:],coarse_y[ihalf-3:],\
							   bounds_error=False,fill_value=coarse_y[-1])
	fine_y= fine_x.copy()
	fine_y[ibeg]= fbeg(fine_x[ibeg])
	fine_y[iend]= fend(fine_x[iend])
	return fine_y

def index_range_low_dt(istart,old_dt,min_dt):
	igood= np.where(old_dt > min_dt)[0]
	crit_dt= np.mean(old_dt[igood])-old_dt[igood].std()
	ileft,iright=istart,istart
	while old_dt[ileft] < crit_dt:
		ileft-=1
		if ileft <= 0:
			ileft=0
			break
	while old_dt[iright] < crit_dt:
		iright+=1
		if iright >= len(sink.old_dt)-1:
			iright=len(sink.old_dt)-1
			break
	print "removing indices in range: ",ileft,iright
	return np.arange(ileft,iright+1).astype(int)

def get_sink_data(sink,stdout,jobid,args):
	#get raw data
	f=open(stdout,'r')
	lines= f.readlines()
	f.close()
	#extra sink info
	print "extracting sink info"
	if loRes_test > 0: extract_loRes(lines,sink)  #correct masses are after CTS
	else: extract_hiRes(lines,sink,vrms_test)  #correct masses are before CTS
	print "finished extracting"
	#all sink data extracted, now compute mdot and plot
	#test if time increases monotonically, if not, then 'args.pout' was not concatenated from out_1, out_2, ...,out_N correctly, crash
	ibad=np.where(sink.CTS[1:] - sink.CTS[:-1] < 0.)[0]
	if ibad.size > 0:
		print "ibad= ",ibad
		print "CTS= ",sink.CTS
		print "mass= ",sink.mass[0,:]
		#see commit #75131169699209bda7
		print "standard output files (eg pout.0_1,...,pout.0_N or out_1,...,out_N) not combined correctly, time info does not increase monotonically everywhere!"
		sys.exit()
	print "calculating Mdot"
	#finally, compute mdotFine,tFine for simulation
	sink.MdotFine_tFine()
	#get global rmsV,rmsVa,meanRho (if user said so)
	if args.rms_local_global:
		glob= GlobalData()
		data=np.loadtxt(args.rms_local_global+'global_rmsV_rmsVa.txt', dtype=np.string_)
		sort_ind= np.argsort(data[:,1]) #sorted index for time to increase monotonically
		glob.time= data[:,1][sort_ind].astype(np.float64)
		glob.rmsV= data[:,2][sort_ind].astype(np.float64)
		glob.rmsVa= data[:,3][sort_ind].astype(np.float64)
		glob.meanRho= data[:,4][sort_ind].astype(np.float64)
		#interpolate to sink.time points
		sink.glob_rmsV= interp_coarse_to_fine(glob.time,glob.rmsV, sink.time)
		sink.glob_rmsVa= interp_coarse_to_fine(glob.time,glob.rmsVa, sink.time)
		sink.glob_meanRho= interp_coarse_to_fine(glob.time,glob.meanRho, sink.time)
		#get local rmsV,rmsVa,meanRho
		data=np.loadtxt(args.rms_local_global+'local_rmsV_rmsVa.txt', dtype=np.string_)
		sort_ind= np.argsort(data[:,1]) #sorted index for time to increase monotonically
		sid_ind= np.arange(2,data.shape[1]-1,4) #index of each sink id, starts at 2, then every 4th index after that
		#get object to hold local rmsV,etc, data
		n_hdf5_files= data.shape[0]
		local= LocalData(args.NSinks,n_hdf5_files,npts+1)
		#get LocalData from data
		local.time= data[:,1][sort_ind].astype(np.float128)
		#local data from each sink
		for cnt,i in enumerate(sid_ind):
			print "cnt,i= ",cnt,i
			local.sid[cnt]= data[:,i][0] #redundant, but keeps writting in the correct sid
			print "sid[cnt],data[:,i][0]=",local.sid[cnt],data[:,i][0]
			local.rmsV[:,cnt]= data[:,i+1][sort_ind].astype(np.float128) #i+1 is ith sink's rmsV at all times
			local.rmsVa[:,cnt]= data[:,i+2][sort_ind].astype(np.float128) 
			local.meanRho[:,cnt]= data[:,i+3][sort_ind].astype(np.float128) 
		#interpolate
		for i in range(args.NSinks):
			sink.rmsV[:,i]= interp_coarse_to_fine(local.time,local.rmsV[:,i], sink.time)
			sink.rmsVa[:,i]= interp_coarse_to_fine(local.time,local.rmsVa[:,i], sink.time)
			sink.meanRho[:,i]= interp_coarse_to_fine(local.time,local.meanRho[:,i], sink.time)
	if args.min_dt: 
		#for each region in time with dt < min_dt, 1 index extracted for each region
		#for each region, its 1 index is converted to range of indices describing extend of that region
		#multiple ranges of indices concaneated and removed from sink info, then recalc mdot
		ibad=np.where(sink.old_dt <= args.min_dt)[0]
		extra_comp=np.where(ibad[1:]-ibad[:-1] > 1)[0]+1 #if multiple regions with low dt
		#get 1 index from each group with low dt
		ind_groups=[]
		ind_groups.append(ibad[0]) #for sure 1 group
		for comp in extra_comp: ind_groups.append(ibad[comp]) #multiple groups
		#using each group's one index, turn it into a range of indices for that group
		irem=np.array([]).astype(int)
		for i in ind_groups: irem=np.concatenate((irem,index_range_low_dt(i,sink.old_dt,args.min_dt)))
		sink.rmIndices(irem) #removes indices and recalculates mdot

if __name__ == "__main__":	
	import argparse
	import matplotlib
	matplotlib.use('Agg')
	import matplotlib.pyplot as plt
	import numpy as np
	from pickle import dump as pdump
	import sys
	import numpy.linalg
	from scipy import interpolate
	import os
	import glob

	from data_types import SinkData

	parser = argparse.ArgumentParser(description="test")
	parser.add_argument("-path",action="store",help='relative path',required=True)
	parser.add_argument("-search",action="store",help='wild card to find all relevant files',required=True)
	parser.add_argument("-prefix",action="store",help='prefix for all files, regardless of serach string so job id can be obtained',required=True)
	parser.add_argument("-NSinks",type=int,action="store",help='# of sinks',required=True)
	parser.add_argument("-max_level",type=int,action="store",help='grep "maximum level" in stdout file, only needed if hiRes, vrms, second type',required=True)
	#optional
	parser.add_argument("-min_dt",type=float,action="store",help='minimum dt to remove indices for',required=False)
	parser.add_argument("-rms_local_global",action="store",help='relative path to local_rmsV.txt and global_rmsV.txt files (optional)',required=False)
	args = parser.parse_args()

	#get files and filter out non stdout files
	files=glob.glob(os.path.join(args.path,args.search))
	for i,fil in enumerate(files): 
		if '.grep' in fil: files.pop(i)
	print "reading files= ",files
	for stdout in files:
		jobid=stdout.strip(os.path.join(args.path,args.prefix))
		print "stdout, jobid= ",stdout,jobid
		#get sink object
		Grep= stdout+".grep"
		path_and_bash= os.path.join(os.path.dirname(os.path.realpath(__file__)),'parse_mdot.sh') 
		if os.system(' '.join(["bash",path_and_bash, stdout,Grep])): raise ValueError
		f=open(Grep,'r')
		lines= f.readlines()
		f.close()
		npts= np.int64(lines[0].split()[0]) 
		vrms_test= np.int64(lines[1].split()[0])
		loRes_test= np.int64(lines[2].split()[0]) 
		sink= SinkData(args.NSinks,npts) 
		#fill with data	
		get_sink_data(sink,stdout,jobid,args)
		#make sanity check plots
		make_sanity_plots(sink,jobid,args)	
		#save pickle file for each stdout 
		print "saving sink.pickle from ",stdout
		if args.rms_local_global: name="sink.pickle.rms."+jobid
		else: name= "sink.pickle."+jobid
		fout=open(os.path.join(args.path,name),"w")
		pdump(sink,fout)
		fout.close()

