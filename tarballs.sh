#!/bin/bash
#SBATCH -p normal
#SBATCH -t 04:00:00  
#SBATCH -J tarballs
#SBATCH -o std.o%j    
#SBATCH -e std.e%j    
#SBATCH -N 1                     # Total number of nodes requested (16 cores/node)
#SBATCH -n 1                     # Total number of tasks
#SBATCH --mail-user=kaylanb@berkeley.edu
#SBATCH --mail-type=end    # email me when the job finishes

name=data1
tar -zcvf ${name}.tar.gz -T ${name}.txt
