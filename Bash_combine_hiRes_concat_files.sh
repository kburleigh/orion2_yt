#!/bin/bash
##args are hiRest edit_concat files from each machine, given time sequential order
tstep=`grep $2 -e step|head -n 1 | cut -d ' ' -f 4`
let tstep=$tstep-1
line=`grep $1 -e "coarse time step "$tstep -n | cut -d ':' -f 1`
head $1 -n $line > $1_matched
cat $1_matched $2 > edit_concat
