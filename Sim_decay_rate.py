import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-pout",nargs=2,action="store",help='standard out/err from simulation')
parser.add_argument("-names",nargs=2,action="store",help='standard out/err from simulation')
parser.add_argument("-mach",type=float,action="store",help='rms mach number')
parser.add_argument("-cs",type=float,action="store",help='sound speed in simulation')
parser.add_argument("-Lbox",type=float,action="store",help='size of simultion box')
parser.add_argument("-basegrid",type=int,action="store",help='ex) 256 if 256^3 base grid')
parser.add_argument("-maxlev",type=int,action="store",help='max refinement level, 0 if unigrid')
parser.add_argument("-cfl",type=float,action="store",help='CFL number used')
parser.add_argument("-time0",type=float,action="store",help='simulation time when inserted sinks')
#optional
parser.add_argument("-fname",action="store",help='prefix for mdot_sinkFiles.png', required=False)
args = parser.parse_args()

import subprocess
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt

time=[-1]*len(args.pout)
dt=[-1]*len(args.pout)
rmsM=[-1]*len(args.pout)
maxM=[-1]*len(args.pout)
#get data
for cnt,fil in enumerate(args.pout):
	print "opening: %s, %s_vrms.txt, %s_vmax.txt, %s_coarse.txt" % (fil,fil,fil,fil)
	subprocess.call(["./Sim_decay_rate.sh", fil])
	rmsM[cnt]=np.loadtxt(fil+"_vrms.txt",unpack=True)/args.cs
	maxM[cnt]=np.loadtxt(fil+"_vmax.txt",unpack=True)/args.cs
	f=open(fil+"_coarse.txt","r")
	lines= f.readlines()
	myt=np.zeros(len(lines))-1
	mydt=myt.copy()
	for i,line in enumerate(lines): 
		(myt[i],mydt[i])= (line.split()[7],line.split()[11])
	myt=myt- args.time0
	time[cnt]= myt.copy()
	dt[cnt]= mydt.copy()
#normalizations
tcr=args.Lbox/args.mach/args.cs
f,axis=plt.subplots(3,1,sharex=True,figsize=(8,10))
plt.subplots_adjust( hspace=0,wspace=0.5 )
ax=axis.flatten()
skip=100
for cnt in range(len(rmsM)):
	ax[0].plot(time[cnt]/tcr,rmsM[cnt],label=args.names[cnt])
	ax[0].set_ylim(0,args.mach*1.1)
	ax[1].plot(time[cnt]/tcr,maxM[cnt])
	tcour= args.cfl*args.Lbox/(maxM[cnt]*args.cs)/args.basegrid/2**args.maxlev
	ax[2].plot(time[cnt][::skip]/tcr,dt[cnt][::skip]/tcour[::skip])
	ax[2].set_yscale('log')
ax[0].set_ylabel(r'$\mathcal{M}_{\rm{RMS}}$',fontweight='bold',fontsize='x-large')
ax[1].set_ylabel(r'$\mathcal{M}_{\rm{MAX}}$',fontweight='bold',fontsize='x-large')
ax[2].set_ylabel(r'$\Delta t / \Delta t_{\rm{COURANT}}$',fontweight='bold',fontsize='x-large')
ax[2].set_xlabel(r'$t/t_{\rm{CROSS, mach 40}}$',fontweight='bold',fontsize='x-large')
ax[0].legend(loc=(0.,1.01),ncol=2)
fname='decay_rate'
if args.fname: fname=fname+"_"+args.fname
plt.savefig(fname+".png",dpi=150)
plt.show()
