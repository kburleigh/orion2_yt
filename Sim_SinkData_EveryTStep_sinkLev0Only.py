'''
WARNING: to correctly combine pout.0_* or out_* files:
    grep out_1 -e step|tail
    grep out_2 -e step|head
    if first 'coarse time step' in `grep out_2 -e step|head` also occurs in out2 then:
        good_step= 1 less than overlapping coarse time step in above 'if statement'
        grep out_1 -e 'coarse time step  good_step' -n
        go to that line in out_1
        delete everything after it (eg "coarse time step" printed after all subcycling is done, which means my 'KJB: advance level' statments have all printed for that coarse time step already)
    then go to last 'coarse time step' in every out_* file and delete any time/sink info that comes after it (if hasn't already been done). if walltime got exceeded the time/sink info printing stopped midway without coarse time step telling you it is there so this script will get messed up
    cat out_1 out_2 > out_12

dependecies:
    Combined standard out files: pout.0_* or out_* 
    Sim_bash_MdotEveryTstep.sh -- does inital parsing of standard out from simulation 
input: 
    standard out from simulation (or concatenation of many standard outputs) giving sink particle info every finest-level step (AMRLevelOrion.cpp, SinkParticleList.cpp print statements).
output: 
    pickle file "mdots_EveryStep*.pickle" containing time,mass,mdot for every sink on the finest-level
'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-path",action="store",help='relative path to find data/write analysis',required=True)
parser.add_argument("-pout",action="store",help='standard out/err from simulation',required=True)
parser.add_argument("-restartTime",type=float,action="store",help='give output of h5dump -A restart.hdf5|grep time -A 5',required=True)
parser.add_argument("-NSinks",type=int,action="store",help='# of sinks',required=True)
args = parser.parse_args()

import subprocess
import matplotlib
matplotlib.use('Agg')
import numpy as np
from pickle import dump as pdump
import sys
import numpy.linalg
from scipy import interpolate
import os

print "all libraries imported"

class SinkData():
	def __init__(self,NSinks,NSinkFiles):
		shape= (NSinks,NSinkFiles)
		#print mass to 16 decimals (max decimals for double precision) so need float64
		self.mass= np.zeros(shape).astype(np.float64)-1
		self.dt= np.zeros(NSinkFiles).astype(np.float64)-1
		self.time= np.zeros(NSinkFiles).astype(np.float64)-1
		self.sid= np.arange(NSinks)
	def MdotFine_tFine(self):
		self.MdotFine= np.zeros(self.mass.shape)-1
		self.MdotFine[:,0]=0.  #dt=0 at time=0 or restart in output file, so set Mdot=0 
		self.MdotFine[:,1:]= (self.mass[:,1:]-self.mass[:,:-1])/self.dt[1:]
	def shift_to_startTime(self,istart):
		'''istart: starting index to have all sink data begin at'''
		self.dt= self.dt[istart:]
		self.time= self.time[istart:]
		self.mass= self.mass[:,istart:]
		self.MdotFine= self.MdotFine[:,istart:]
		

#grep for only finest-level sink particle data printed out from simulation and save to file "pout_grep"
Pout= os.path.join(args.path,args.pout)
Grep= Pout+"_grep"
subprocess.call(["./Sim_bash_MdotEveryTstep_sinkLev0Only.sh", Pout,Grep])
#
f=open(Grep,'r')
lines= f.readlines()
npts= np.int64(lines[1].split()[-1]) #2nd line in "pout_grep" contains number of finest-level time steps, so number of mass points for every sink
f.close()
sink= SinkData(args.NSinks,npts)  #+1 b/c time = 0 not included in args.pout file
#fill with level 0 sink info
f=open(Pout,'r')
lines= f.readlines()
f.close()
m1= "KJB2: level 0"  #new mass for each sink every time find this
step=0 
i=0
print "now reading concatenated output file"
while i < len(lines):
	if lines[i].startswith(m1):  #info after this line is for all sinks
		#save the time
		sink.dt[step]= np.around(np.float64(lines[i].split()[4]),decimals=16)
		sink.time[step]= np.around(np.float64(lines[i].split()[6]),decimals=16)
		#         counter=1  #go through some number of lines getting all sink info
		sid=0  #sink id
		while sid < args.NSinks:
			i+= 1   #read next line
			if lines[i].startswith(m1): #error check
				print "found new sink data before finished current data"
				raise ValueError  
			info= lines[i].split()
			sink.mass[sid,step]= np.around(np.float64(info[1]),decimals=16)
			#get next sink id
			sid+=1
		#exited while loop so prepare for sink info at next time step
		step+=1 
	#exited if statment so look for next "m1" occurennce
	#i is at line with 63rd sink info
	i+=1 #increment to read next line
print "finished reading file"
#time[0] can get messed up so set to time in hdf5 header of checkpoint restarted from
sink.time[0]= args.restartTime
print "calculating Mdot"
#compute mdot
sink.MdotFine_tFine()
#drop initial mdot data to start mdot at correct time
#if args.restartTime > 0: istart=1 #restarted from a checkpoint, accretion begins immediately, no mdot=0
istart= np.where(sink.MdotFine[0,:] > 0)[0][0] -1 #istart = 0 unless mdot > 0 beyond index 0 when restart from unigrid turbulence
sink.shift_to_startTime(istart)
#test if time increases monotonically, if not, then 'args.pout' was not concatenated from out_1, out_2, ...,out_N correctly, crash
ibad=np.where(sink.time[1:]-sink.time[:-1] <= 0.)[0]
if ibad.size > 0:
    #see commit #75131169699209bda7
    print "standard output files (eg pout.0_1,...,pout.0_N or out_1,...,out_N) not combined correctly, time info does not increase monotonically everywhere!"
    sys.exit()
#save data 
print "saving data"
name= os.path.join(args.path,"sinks_level0only.pickle")
fout=open(name,"w")
pdump(sink,fout)
fout.close()
#output restart time for future reference
fin= open(os.path.join(args.path,"restart_time.dat"),"w")
fin.write("restart time= %.6f\n" % args.restartTime)
fin.close()
