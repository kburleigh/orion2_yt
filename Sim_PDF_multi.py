'''Just like Sim_PDF.py exect ONLY make default PDFs and does so for multiple data'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-multiFiles",nargs=4,action="store",help='if set then draw as multiple lines in default PDFs')
parser.add_argument("-multiNames",nargs=4,action="store",help='required for multiFiles')
parser.add_argument("-multiColors",nargs=4,action="store",help='required for multiFiles')
parser.add_argument("-cs",type=float,action="store",help='hdf5 file to get velocities for')
#optional
parser.add_argument("-ytVersion2",action="store",help='set to 1 if only have yt version less than 3.0')
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
if args.ytVersion2: from yt.mods import *
else: import yt
import numpy as np
import matplotlib.pyplot as plt

import Sim_curl
print "finished importing modules"

font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 10,
        }

def densityPDF(fsave,rho_l,args):
    '''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
    f,axis=plt.subplots(2,1,sharex=True,figsize=(5,10))
    plt.subplots_adjust( hspace=0 )
    ax=axis.flatten()
    s=[]
    for i in range(len(rho_l)): 
        s= np.log( rho_l[i].flatten()/rho_l[i].mean() ) 
        (s_pdf,s_bins,jnk)= ax[0].hist(s,bins=100,normed=True,align='mid',histtype='step',color=args.multiColors[i],label=args.multiNames[i])
        (s_pdf,s_bins,jnk)= ax[1].hist(s,bins=100,normed=True,align='mid',histtype='step',color=args.multiColors[i],label=args.multiNames[i])
    for axi in ax:
        axi.legend(loc=1,fontsize='small')
    ax[0].set_yscale('linear')
    ax[1].set_yscale('log')
    ax[1].set_xscale('linear')
    ax[0].set_ylabel('PDF s')
    ax[1].set_ylabel('Log10 PDF s')
    ax[1].set_xlabel(r"s $\equiv$ Ln( $\rho / \rho_0$ )")
    ax[1].set_xlim(-5,5)
    plt.savefig(fsave,dpi=150) 
    plt.close()

def velPDF(fsave,vmag_l,args):
    '''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
    f,axis=plt.subplots(2,1,sharex=True,figsize=(5,10))
    plt.subplots_adjust( hspace=0 )
    ax=axis.flatten()
    for i in range(len(vmag_l)):
        (M_pdf,M_bins,jnk)= ax[0].hist(vmag_l[i].flatten()/args.cs,bins=50,normed=True,align='mid',histtype='step',\
                                        color=args.multiColors[i],label=args.multiNames[i])
        (M_pdf,M_bins,jnk)= ax[1].hist(vmag_l[i].flatten()/args.cs,bins=50,normed=True,align='mid',histtype='step',\
                                        color=args.multiColors[i],label=args.multiNames[i])
    for axi in ax:
        axi.legend(loc=1,fontsize='small')
    ax[0].set_yscale('linear')
    ax[1].set_yscale('log')
    ax[1].set_xscale('linear')
    ax[0].set_ylabel('PDF(Mach)')
    ax[1].set_ylabel('Log10 PDF(Mach)')
    ax[1].set_xlabel(r"Mach number")
    ax[1].set_xlim(0,15)
    plt.savefig(fsave,dpi=150)
    plt.close() 

def vortPDF(fsave,vort_l,lbox_l,mach3D_l,args):
    '''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
    f,axis=plt.subplots(2,1,sharex=True,figsize=(5,10))
    plt.subplots_adjust( hspace=0 )
    ax=axis.flatten()
    for i in range(len(vort_l)):
        vort_eff= mach3D_l[i]*args.cs/lbox_l[i]
        (w_pdf,w_bins,jnk)= ax[0].hist(vort_l[i].flatten()/vort_eff,bins=500,normed=True,align='mid',histtype='step',\
                                        color=args.multiColors[i],label=args.multiNames[i])
        (w_pdf,w_bins,jnk)= ax[1].hist(vort_l[i].flatten()/vort_eff,bins=500,normed=True,align='mid',histtype='step',\
                                        color=args.multiColors[i],label=args.multiNames[i])
    ax[0].set_ylabel('PDF($\omega / \omega_0$)')
    for axi in ax:
        axi.legend(loc=1,fontsize='small')	
    ax[0].set_yscale('linear')
    ax[1].set_yscale('log')
    ax[1].set_xscale('linear')
    ax[1].set_ylabel(r'Log10 PDF($\omega / \omega_0$)')
    ax[1].set_xlabel(r"$\omega / \omega_0$")
    ax[1].set_xlim(0,50)
    plt.savefig(fsave,dpi=150) 
    plt.close()

def getVrms_weighted3DVrms(vx,vy,vz,rho,args):
    vmag2= (vx**2+vy**2+vz**2)
    vrms_mw= np.sqrt(np.average(vmag2,weights=rho))
    return (np.sqrt(vmag2),vrms_mw/args.cs)

def extract_DensVelVort(fil,args,vars):
    lev=0
    if args.ytVersion2: 
       ds=load(fil)
       lbox= float(ds.domain_width[0])
       cube= ds.h.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions) 
    else:
        ds=yt.load(fil)
        lbox= float(ds.domain_width[0])
        cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions)
    print "gathering 3d data arrays"
    #3D arrays
    rho_x = np.array(cube["density"])
    vx = np.array(cube["X-momentum"])/rho_x
    vy = np.array(cube["Y-momentum"])/rho_x
    vz = np.array(cube["Z-momentum"])/rho_x
    print "3d arrays extracted, now calculating vorticity and single numbers like 3D Mach"
    rho_inf= rho_x.flatten().mean()
    (vmag_x,Mach_3D)= getVrms_weighted3DVrms(vx,vy,vz,rho_x,args)
    wmag_x= Sim_curl.getCurl(vx,vy,vz,  ds) #3D array
    del vx,vy,vz,cube
    #store variables
    vars.rho.append(rho_x)
    vars.vmag.append(vmag_x)
    vars.wmag.append(wmag_x)
    vars.basename.append(ds.basename)
    vars.lbox.append(lbox)
    vars.mach3d.append(Mach_3D)
    del rho_x,vmag_x,wmag_x


#main
class MyObj():
    def __init__(self):
        self.rho=[]
        self.vmag=[]
        self.wmag=[]
        self.basename=[]
        self.lbox=[]
        self.mach3d=[]
vars= MyObj()
for fil in args.multiFiles:
    print "extracting 3D array data for file: %s" % fil
    extract_DensVelVort(fil,args,vars)
#plot
print "plotting"
id=""
for i in range(len(vars.basename)):
    id=id+"_"+vars.basename[i][5:9]
name='velocityPDF_%s.png' % id
velPDF(name,vars.vmag,args)
name='vorticityPDF_%s.png' % id
vortPDF(name,vars.wmag,vars.lbox,vars.mach3d,args)
name='densityPDF_%s.png' % id
densityPDF(name,vars.rho,args)
print 'done'
