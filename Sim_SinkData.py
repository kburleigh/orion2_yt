import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-tB",action="store",help='bondi time in sec')
args = parser.parse_args()
if args.tB: tB = float(args.tB)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob
from pickle import dump as pdump

class SinkData():
    def __init__(self,NSinks,NSinkFiles):
        shape= (NSinkFiles,NSinks)
        self.time= np.zeros(NSinkFiles)-1
        self.mass= np.zeros(shape)-1
        self.x= np.zeros(shape)-1
        self.y= np.zeros(shape)-1
        self.z= np.zeros(shape)-1
        self.Mdot= np.zeros(shape)-1 #Mdot of sink particles
    def get_times(self,f_hdf5):
        for cnt,f in enumerate(f_hdf5):
            print "getting times: loading %s" % f
            pf=load(f)
            self.time[cnt]= pf.current_time
    def get_MassPosMdot(self,f_sink):
        '''f_sink: list of .sink files'''
        for cnt in range(len(f_sink)):
            print "getting sink props: loading %s" % f_sink[cnt]
            mass,x,y,z= np.loadtxt(f_sink[cnt],dtype='float',\
                                   skiprows=1,usecols=(0,1,2,3),unpack=True)
            self.mass[cnt,:]= mass
            self.x[cnt,:]= x
            self.y[cnt,:]= y
            self.z[cnt,:]= z
        #calc Mdot
        if self.time[0].astype('int') == -1:
            print "time not loaded"
            raise ValueError
        else:
            self.Mdot[0,:]=0.
            for cnt in np.arange(1,len(f_sink)):
                deltaM= self.mass[cnt,:] -self.mass[cnt-1,:] 
                deltaT= self.time[cnt]- self.time[cnt-1]
                self.Mdot[cnt,:]= deltaM/deltaT

def read_times_from_txtfile(tfile):
    

def PlotMdot4EachSink(fsave,sink,tB):
    '''sink: returned by CalcSinkQuantities()
    av: returned by CalcAvgQuantities
    tB: bondi time in units of simulation'''
    fig,ax= plt.subplots(figsize=(5,5))
    time= (sink.time - sink.time[0])/tB
    for cnt in range(sink.Mdot.shape[1]):
        ax.plot(time,np.log10(sink.Mdot[:,cnt]))
    mdot_avg= np.average(sink.Mdot,axis=1)
    ax.plot(time,np.log10(mdot_avg),'k*',label="Average")
    mdot_med= np.median(sink.Mdot,axis=1)
    ax.plot(time,np.log10(mdot_med),'ko',label="Median")
    ax.set_xlabel("t/tB")
    ax.set_ylabel("Mdot")
    ax.legend(loc=0)
#     ax.set_ylim([-0.1*sink.MdotBHA_Avg.max(),1.1*sink.MdotBHA_Avg.max()])
    fig.savefig(fsave,dpi=150)
#     plt.show()

## Main
spath="./plots/"
NSinks=8
f_hdf5= glob.glob("./data.00*.3d.hdf5")
f_sink= glob.glob("./data.00*.3d.sink")
f_hdf5.sort()
f_sink.sort()
print "len each file list: %d %d" % (len(f_hdf5),len(f_sink))

sink=SinkData(NSinks=NSinks,NSinkFiles=len(f_sink))
sink.get_times(f_hdf5)
sink.get_MassPosMdot(f_sink)
#save sink data arr
fout=open(spath+"sink_data.pickle","w")
pdump(sink,fout)
fout.close()
print "pickle dumped sink class"
#plot to check that it worked
fsave=spath+"mdot_all_sinks.png"
PlotMdot4EachSink(fsave,sink,tB)
print "plotted Mdots"
