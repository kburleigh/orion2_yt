import os
import numpy as np
import argparse
import glob

def convert_to_jpeg(imgfns):
	for cnt,img in enumerate(imgfns):
		newname= os.path.join(os.path.dirname(img),"%04d"%cnt + '.jpg')
		cmd = ' '.join(['convert',img,newname])
		if os.system(cmd) == 0: print("Success: ",img,"->",newname) 
		else: print("Failed: ",img,"->",newname)

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-img_search",action="store",help='full path + search string for images to convert to a movie')
parser.add_argument("-fps",type=int,action="store",help='# frames per sec')
parser.add_argument("-movie_name",action="store",help='{name}.mp4')
parser.add_argument("-no_convert",action="store",help='set to anything to skip converting image format to jpeg')
args = parser.parse_args()

#convert images to jpg
imgfns= glob.glob(args.img_search)
imgfns.sort()
if args.no_convert: pass
else: convert_to_jpeg(imgfns)
#make movie with jpg images
cwd= os.getcwd()
os.chdir(os.path.dirname(imgfns[0]))
if args.movie_name: name= args.movie_name+'.mp4'
else: name= 'test.mp4'
cmd = ' '.join(['ffmpeg','-framerate','%d'%args.fps,'-i','%04d.jpg','-f','mp4','-vcodec','libx264','-pix_fmt','yuv420p',name]) #-framerate sets both input and output (-r) rates, best
if os.system(cmd) == 0: print("Successly made movie: ",name) 
else: print("Failed to make movie")
os.chdir(cwd)
print('done')
