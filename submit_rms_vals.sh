#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N 1
#SBATCH -t 00:10:00
#SBATCH -J rms
#SBATCH -o rms.o%j
#SBATCH --mail-user=kburleigh@lbl.gov
#SBATCH --mail-type=END,FAIL
#SBATCH -L SCRATCH

# give value of sd3/bInf, sd2/bInf, sd2/b1 etc
export mymod="$1"
# 
export mydir=/project/projectdirs/astro250/kburleigh/hdf5
export fn=data.0010.3d.hdf5
#if [ "$mymod" == "sd3/bInf" ]; then
#    export fil=sd3/bInf
#elif [ "$mymod" == "bInf" ]; then
#    export fil=
#elif [ "$mymod" == "b1" ]; then
#    export fil=/scratch1/scratchdirs/kaylanb/sf/supersonic_accretion/b1_sd2/data.0484.3d.hdf5
#fi

echo name is ${mydir}/${mymod}/${fn}

cd $SLURM_SUBMIT_DIR
date
srun -n 1 python modules/hdf5_params/rms_values.py -data ${mydir}/${mymod}/${fn} -quick_rms 1
date
echo DONE
