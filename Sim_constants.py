#CGS
h= 6.62606876e-27  #plancks constant
c= 2.99792458e10  #speed light
kb = 1.3806505e-16   #ergs K^-1
mp= 1.67262171e-24  #mass proton
me= 9.1093826e-28 #mass electron
mn = 1.67492728e-24   #mass nuetron
mH = 1.6733e-24 # mass hydrogen atom
mD = 3.34324e-24  #g, mass deuterium (1p + 1n nucleus)
e= 4.8e-10
a= 7.57e-15  #radiation const
sigma_sb= 5.67051e-5  #stephan boltzman constant
sigma_T= 6.6524e-25  #thomson cross section
a0= 0.5e-10  #bohr radius
G= 6.6726e-8  #grav constant

##astrophysical quantities
msun = 1.98892e33 # mass sun
rsun = 6.96e10 # radius sun
lsun = 3.8e33 # lum sun
mear = 5.9736e27 #mass earth
rear = 6.378136e8 #radius earth

##Other
pi = 3.14159265358979
secyr = 3.156e7  #sec in 1 yr   
amu = 1.66053886e-24     #  atomic mass unit
Na = 6.0221367e23       #Avogadro Contant
pc = 3.0856775807e18    #  parsec                    */
ly = 0.9461e18          #  light year                */
au = 1.49597892e13      #  astronomical unit         */
eV = 1.602176463158e-12  #1 eV in ergs
