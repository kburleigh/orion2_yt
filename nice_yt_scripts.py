#from Mathew Turk slides
from yt.mods import *
pf = load("DD0087/DD0087")
v, c = pf.h.find_max("Density")
pc = PlotCollection(pf, c)
pc.add_projection("Density", 0, "Density")
pc.set_width(1000.0, ’au’)
pc.save()
