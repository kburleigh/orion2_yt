import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

colors=['k','b','g','r','c','m','y'] +['#33CCFF']
data=pd.read_csv('vel_vort_evolution.txt',header=0)
#simulation results
fig,axes=plt.subplots(1,2)
ax=axes.flatten()
ax[0].plot(data['beta'], data['w-tilde-mean'],'b') 
ax[0].plot(data['beta'], data['w-tilde-med'],'b--') 
ax[1].plot(data['beta'], data['u-mean'],'g-',label='mean') 
ax[1].plot(data['beta'], data['u-med'],'g--',label='median') 
#ax[1].legend(loc=4,ncol=2,fontsize='x-small')
for i in [0,1]: 
	ax[i].set_xlim(data['beta'].min()/2,2e4) #data['beta'].max()*2)
	ax[i].set_xlabel(r'$\beta$')
	ax[i].set_xscale('log')
ax[0].set_title(r'vorticity = $\tilde{w}$')
ax[1].set_title(r'veloocity = $u$')
plt.savefig('vel_vort.png')
plt.close()
