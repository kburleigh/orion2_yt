#!/bin/bash 
grep -e 'AMRLevelOrion::advance level 0 to time' pout.0 > ./plots/tsteps_lev0.txt
grep -e 'AMRLevelOrion::advance level 1 to time' pout.0 > ./plots/tsteps_lev1.txt
grep -e 'AMRLevelOrion::advance level 2 to time' pout.0 > ./plots/tsteps_lev2.txt
grep -e 'AMRLevelOrion::advance level 3 to time' pout.0 > ./plots/tsteps_lev3.txt
grep -e 'AMRLevelOrion::advance level 4 to time' pout.0 > ./plots/tsteps_lev4.txt
grep -e 'AMRLevelOrion::advance level 5 to time' pout.0 > ./plots/tsteps_lev5.txt
