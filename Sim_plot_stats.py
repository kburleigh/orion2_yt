import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-mach",type=float,action="store",help='stats file output with Sim_PDF')
parser.add_argument("-cs",type=float,action="store",help='stats file output with Sim_PDF')
parser.add_argument("-lbox",type=float,action="store",help='stats file output with Sim_PDF')
parser.add_argument("-stats",nargs=2,action="store",help='stats file output with Sim_PDF')
parser.add_argument("-labels",nargs=2,action="store",help='stats file output with Sim_PDF')
parser.add_argument("-colors",nargs=2,action="store",help='stats file output with Sim_PDF')
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt

def addData(ax,data,label,color,args, zdrv_fsol):
	keys=['sMin','sMax','sMean','fsol','mMean','mMed','wMean','wMed']
	cross=args.lbox/args.mach/args.cs
	for cnt,axi in enumerate(ax):
		if keys[cnt] == 'fsol':
			t=[0.]+list(data['t'])
			fsol=[zdrv_fsol]+list(data['fsol'])
			axi.plot(np.array(t)/cross,np.array(fsol),c=color,label=label,ls='-',marker='o',ms=5)
		else:	
			axi.plot(data['t']/cross,data[keys[cnt]],c=color,label=label,ls='-',marker='o',ms=5)
#get stats.txt data
formats=('S10','f10','f10','f10','f10','f10','f10','f10','f10','f10','f10','f10','f10','f10')
names=('fil','t','sMin','sMax','sMean','sMed','mMin','mMax','mMean','mMed','Mrms','wMean','wMed','fsol')
dtype={'names': names,'formats': formats}
data=len(args.stats)*[-1]
zdrv_fsol= len(args.stats)*[-1]
for i in range(len(args.stats)):
	data[i]=np.loadtxt(args.stats[i], dtype=dtype) #a['fil'],a['t'] etc
	#check for zdrv data
	f=open(args.stats[i],"r")
	lines= f.readlines()
	for line in lines:
		if line.split()[0].endswith('zdrv.hdf5'): zdrv_fsol[i]= line.split()[1]
#plot
f,axis=plt.subplots(4,2,sharex=True,figsize=(15,10))
plt.subplots_adjust( hspace=0.2,wspace=0.2 )
ax=axis.flatten()
for i in range(len(args.stats)):
	addData(ax,data[i],args.labels[i],args.colors[i],args, zdrv_fsol[i])
ax[6].set_xlabel(r'$t/t_{cross}$',fontweight='bold',fontsize='x-large')
ax[7].set_xlabel(r'$t/t_{cross}$',fontweight='bold',fontsize='x-large')
ax[0].legend(loc=(0,1.05),fontsize='x-large',ncol=3)
ylabs=['Min(s)','Max(s)','Mean(s)','f_solenoidal',r'Mean($\mathcal{M}/ \mathcal{M}_0$)',r'Median($\mathcal{M}/ \mathcal{M}_0$)',r'Mean($\omega$)',r'Median($\omega$)']
for cnt,axi in enumerate(ax): 
	axi.set_ylabel(ylabs[cnt],fontsize='x-large')
plt.savefig('stats.png')
