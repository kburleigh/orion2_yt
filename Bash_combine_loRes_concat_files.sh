#!/bin/bash
##args are loRest edit_concat files from each machine, given time sequential order
step=`grep $2 -e "coarse time step" |head -n 1| cut -d ' ' -f 4`
#1st file, remove everythin after this step
afterLine=`grep $1 -e "coarse time step ${step}" -n |cut -d ':' -f 1`
let afterLine=$afterLine+1
maxLine=`wc -l $1 |cut -d ' ' -f 1`
sed "${afterLine},${maxLine}d" $1 > edit1
#2nd file, rm step 
upToLine=`grep $2 -e "coarse time step ${step}" -n |cut -d ':' -f 1`
sed "1,${upToLine}d" $2 > edit2
#concat
cat edit1 edit2 > edit_concat
