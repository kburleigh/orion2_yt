import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-f_hdf5",action="store",help='hdf5 file to get velocities for')
args = parser.parse_args()
if args.f_hdf5:
    hdf5 = str(args.f_hdf5)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt

ds=load(hdf5)

def AllData(pf):
    ad= pf.h.all_data()
    rho = np.array(ad["density"])
    vx= np.array(ad["X-momentum"])/rho
    vy= np.array(ad["Y-momentum"])/rho
    vz= np.array(ad["Z-momentum"])/rho
    return (rho,vx,vy,vz)

def CovGrid(ds):
    lev= 0
    cube= ds.h.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions)
    rho = np.array(cube["density"])
    vx = np.array(cube["X-momentum"]/rho)
    vy = np.array(cube["Y-momentum"]/rho)
    vz = np.array(cube["Z-momentum"]/rho)
    return (rho,vx,vy,vz)


#def GetLogHist(vort_flat,lbox,mach,cs):
#    w_squig= vort_flat*lbox/mach/cs
#    logw=np.log(w_squig)
#    (logn,logbins,jnk)= plt.hist(logw,bins=100,align='mid',normed=True,histtype='bar')
#    logbins=(logbins[:-1]+logbins[1:])/2
#    plt.close()
#    return logbins, logn
#
#def plotwmag(fname,w_kay,w_matin,w_mat,lbox,mach,cs):
#    class Obj():
#        pass
#    logbins=CurlVec()
#    loghists=CurlVec()
#    (logbins.kay,loghists.kay)= GetLogHist(w_kay.flatten(),lbox,mach,cs)
#    (logbins.matin,loghists.matin)= GetLogHist(w_matin.flatten(),lbox,mach,cs)
#    (logbins.mat,loghists.mat)= GetLogHist(w_mat.flatten(),lbox,mach,cs)
#    plt.plot(np.exp(logbins.kay),loghists.kay,'ro',ms=10.,label='Kay')
#    plt.plot(np.exp(logbins.matin),loghists.matin,'bo',ms=12.,mew=2.,mfc='none', mec='b',label='Mat Inner')
#    plt.plot(np.exp(logbins.matin),loghists.matin,'go',ms=15.,mew=2.,mfc='none', mec='g',label='Mat')
#    # plt.xlim([1e-2,1e3])
#    # plt.ylim([1e-6,1])
#    plt.yscale('log')
#    plt.xscale('log')
#    plt.xlabel(r'$\tilde{\omega}= \omega L/(M_0c_s)$')
#    plt.ylabel(r'dp/d ln w')
#    plt.legend(loc=2)
#    plt.savefig(fname)
#    plt.close()
