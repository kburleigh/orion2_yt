'''
'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-master_sinks",nargs=2,action="store",help='master_sink.pickle files to compare individual mdots')
parser.add_argument("-labels",nargs=2,action="store",help='64 sink or just 1 sink evolution etc')
parser.add_argument("-indices",nargs=2,type=int,action="store",help='indices to compare')
parser.add_argument("-cs",type=float,action="store",help='bondi hoyle time at instant insert sinks in simulation units')
parser.add_argument("-mach",type=float,action="store",help='rms Mach number when sinks were inserted')
parser.add_argument("-rho0",type=float,action="store",help='mean rho')
parser.add_argument("-mass0",type=float,action="store",help='initial sink mass')
parser.add_argument("-Geq1",action="store",help='set this to anything, say 1 if units of G = 1', required=False)
#optional
parser.add_argument("-boxcar",nargs=5,type=float,action="store",help='boxcar width for each directory data, ex) 0.02tB is in Cunningham 2012',required=False)
parser.add_argument("-xlim",nargs=2,metavar=('low','hi'),type=float,action="store",help='mdot vs. time, time limits',required=False)
parser.add_argument("-ylim",nargs=2,metavar=('low','hi'),type=float,action="store",help='for median plot',required=False)
#get args
args = parser.parse_args()

import matplotlib
#matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pickle
import glob 
import os
import mpld3

import Sim_constants as sc
print "imported all modules"
if args.Geq1: bigG= 1.
else: bigG= sc.G

def mdot_bondi(args):
    return 4*np.pi*args.rho0*(bigG*args.mass0)**2/args.cs**3

class Mdots():
    def __init__(self,args):
		self.const= mdot_bondi(args) #mdot_krumholz_const(sink,args)

class SinkData():
	pass

class CombinedSinkData():
	def __init__(self):
		pass
	def fill(self,sink):
		self.sid=sink.sid.copy()
		self.mass=sink.mass.copy()
		self.mdot=sink.mdot.copy()
		self.mdotTime=sink.mdotTime.copy()
		self.CTS=sink.CTS.copy()
		self.old_t=sink.old_t.copy()
		self.old_dt=sink.old_dt.copy()
	def concat(self,sink):
		self.sid=np.concatenate((self.sid,sink.sid),axis=1)
		self.mass=np.concatenate((self.mass,sink.mass),axis=1)
		self.mdot=np.concatenate((self.mdot,sink.mdot),axis=1)
		self.mdotTime=np.concatenate((self.mdotTime,sink.mdotTime))
		self.CTS=np.concatenate((self.CTS,sink.CTS))
		self.old_t=np.concatenate((self.old_t,sink.old_t))
		self.old_dt=np.concatenate((self.old_dt,sink.old_dt))
	def rem_repeated_CTS(self):
		cts_diff=(self.CTS[1:]-self.CTS[:-1]).copy()
		ind=np.where(cts_diff < 1)[0]
		num_repeat=np.abs(cts_diff[ind])
		rm_inds=np.array([])
		for num,i in zip(num_repeat,ind): rm_inds=np.concatenate(( rm_inds,np.arange(i-num,i+1) ))
		self.rmIndices(rm_inds)
	def rmIndices(self,idel):
		self.mass=np.delete(self.mass,idel,axis=1)
		self.sid= np.delete(self.sid,idel,axis=1)
		self.CTS= np.delete(self.CTS,idel)
		self.old_t= np.delete(self.old_t,idel)
		self.old_dt= np.delete(self.old_dt,idel)
		self.mdotTime= np.delete(self.mdotTime,idel) #idel is correct b/c mdotTime=old_t[:-1]+old_dt[:-1]
		self.mdot= np.delete(self.mdot,idel,axis=1) #if correct to remove 'idel' from 'mdotTime', then must remove idel from 'mdot' as well

def Boxcar(t,y,wid,units_tBHA=True):
	'''t,y are array-like. t is dependent variable, y is independent var we are taking boxcar average of.
	wid is width of boxcar average
	returns: tuplee of new boxcar averaged t,y '''
	print "computing a BoxCar average for Mdot info"
	isMonotonic(t)
	if not units_tBHA: #false (so not evaluated) unless units_tBHA set to False
		raise ValueError
	new_mdot=[]
	new_t= []
	low=t.min()
	hi=t.min()+wid
	cnt= 1
	while (low < t.max()):
		imin=np.where(t >= low)[0]
		imax=np.where(t < hi)[0]
		if len(imin) == 0 or len(imax) == 0: #did not find value between low and hi
			cnt+=1
			hi= t.min()+cnt*wid  #increase hi so can find value between original low and new hi
			if hi > t.max():  #no data in [end-small,end), okay if "small" is indeed small 
				break
		else: #found values continue as normal
			imin=imin.min()
			imax=imax.max()
			ind=np.arange(imin,imax+1,1)
			new_mdot.append( y[ind].mean() )
			new_t.append( (t[imin]+t[imax])/2 )
			cnt+=1
			low=hi
			hi= t.min()+cnt*wid
			if (low + wid >= t.max()): hi= t.max()
	return (np.array(new_t),np.array(new_mdot))

def add_to_plot(ax,c,ls,label, sink,index,mdot,tBH,args):
	time= (sink.mdotTime-sink.mdotTime[0])/tBH
	if args.boxcar:
		(boxT,boxMed)= Boxcar(time,sink.mdot[index,:]/mdot.const,args.boxcar,units_tBHA=True)
		ax.plot(boxT,boxMed,c=c,ls=ls,lw=2.,label=label)
	else: 
		ax.plot(time,sink.mdot[index,:]/mdot.const,c=c,ls=ls,lw=2.,label=label)		
	

#setup plot
fig,ax= plt.subplots(1,1,sharex=True)
fig.set_size_inches(10., 8.)
#fig.subplots_adjust(hspace=0,wspace=0.1) 
colors=['k','b','g','r','c','m','y'][:len(args.master_sinks)]
line_style=['-','--','-','--'][:len(args.master_sinks)]
#plot set of levMax,lev0 for each Beta 
legend_patches=[]	
for fil,c,ls,label,index in zip(args.master_sinks,colors,line_style,args.labels,args.indices):
	fin=open(fil,'r')
	master=pickle.load(fin)
	fin.close()
	#normalization mdot
	mdot= Mdots(args)
	#plot it
	tBH = bigG*args.mass0/(args.mach**2+ args.cs**2)**(3./2)
	#master sink ready for plotting + saving to file
	add_to_plot(ax,c,ls,label, master,index,mdot,tBH,args)
#labeling
ax.legend(loc=4,fontsize='large')
#user defined x,y limits?
if args.ylim: ax.set_ylim(args.ylim[0],args.ylim[1]) 
if args.xlim: ax.set_xlim(args.xlim[0],args.xlim[1]) 
#label plot
ax.set_ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_{\rm{B}} }$',fontweight='bold',fontsize='xx-large')
ax.set_xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
ax.set_yscale('log')
#plt.show()
prefix='compare_indiv_mdots_indices_%d_%d' % (args.indices[0],args.indices[1])
plt.savefig(prefix+'.png')
mpld3.save_html(fig,prefix+'.html')
plt.close()


