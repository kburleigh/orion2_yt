#include "REAL.H"
#include "SPACE.H"
#include "CONSTANTS.H"

      subroutine CHECK(
     &           state
     &           ,istatelo0,istatelo1,istatelo2
     &           ,istatehi0,istatehi1,istatehi2
     &           ,nstatecomp
     &           ,flag
     &           ,isreglo0,isreglo1,isreglo2
     &           ,isreghi0,isreghi1,isreghi2
     &           ,dx
     &           )

      implicit none
      integer nstatecomp
      integer istatelo0,istatelo1,istatelo2
      integer istatehi0,istatehi1,istatehi2
      REAL_T state(
     &           istatelo0:istatehi0,
     &           istatelo1:istatehi1,
     &           istatelo2:istatehi2,
     &           0:nstatecomp-1)
      integer flag
      integer isreglo0,isreglo1,isreglo2
      integer isreghi0,isreghi1,isreghi2
      REAL_T dx
      integer i,j,k
      integer ii,jj,kk
      integer itr
      real*8 den, q, hvel2, hb2, temp
      real*8 dbl_one, dbl_zero
      real*8 x, y, z
      integer loclo0, loclo1, loclo2, lochi0, lochi1, lochi2
      real*8 dentot, edentot, frac, rat, ncell
      include "commons_h.F"
      dbl_one = 1.d0
      dbl_zero = 0.d0
      
      do k = isreglo2,isreghi2
      do j = isreglo1,isreghi1
      do i = isreglo0,isreghi0

      x = (i + 0.5d0) * dx
      y = (j + 0.5d0) * dx
      z = (k + 0.5d0) * dx
      if (ANY(ISNAN(state(i,j,k,:))) .or.
     &    ANY(ABS(state(i,j,k,:))==dbl_one/dbl_zero)) then
         flag = 2
         print *, " Warning! Encountered NAN or +/-Infinity at ", x,y,z
         return
      end if
      den = state(i,j,k, DN)
      if (den < SMALL_DN) then
         if(EN<=0) then
            do itr=0,NTRACER-1
               state(i,j,k, TR+itr) = 
     +              MIN(MAX(state(i,j,k, TR+itr)/
     +              state(i,j,k, DN), 0.d0), 1.d0)
            end do
            state(i,j,k, DN) = SMALL_DN
            do itr=0,NTRACER-1
               state(i,j,k, TR+itr) = 
     +              state(i,j,k, TR+itr)*state(i,j,k, DN)
            end do
         else
            flag = 1
            do itr=0,NTRACER-1
               state(i,j,k, TR+itr) = 
     +              MIN(MAX(state(i,j,k, TR+itr)/
     +              state(i,j,k, DN), 0.d0), 1.d0)
            end do
            loclo0 = max( isreglo0, i - 1)
            loclo1 = max( isreglo1, j - 1)
            loclo2 = max( isreglo2, k - 1)
            lochi0 = min( isreghi0, i + 1)
            lochi1 = min( isreghi1, j + 1)
            lochi2 = min( isreghi2, k + 1)
            ncell = 0.d0
            do ii = loclo0, lochi0
               do jj = loclo1, lochi1
                  do kk = loclo2, lochi2
                     ncell = ncell + 1.d0
                     den = state(ii,jj,kk, DN)
                     hvel2 = 5.d-1 * (state(ii,jj,kk, MX) **2 +
     &                    state(ii,jj,kk, MY) **2 +
     &                    state(ii,jj,kk, MZ) **2) /
     &                    den**2
                     if (BX > 0) then
                     hb2    = 5.d-1 * (state(ii,jj,kk, BX) **2 +
     &                    state(ii,jj,kk, BY) **2 +
     &                    state(ii,jj,kk, BZ) **2)
                     else
	             hb2    = 0.0
                     endif
                     state(ii,jj,kk,EN) = 
     &                    state(ii,jj,kk,EN) - den * hvel2 - hb2
                  enddo
               enddo
            enddo
            frac = 1.d0 / (1.d0 + (1.d0/(ncell - 1.d0) ) )
            dentot = 0.d0
            edentot = 0.d0
            do ii = loclo0, lochi0
               do jj = loclo1, lochi1
                  do kk = loclo2, lochi2
                     dentot = dentot + state(ii,jj,kk, DN)
                     edentot = edentot + state(ii,jj,kk,EN)
                  enddo
               enddo
            enddo
            do ii = loclo0, lochi0
               do jj = loclo1, lochi1
                  do kk = loclo2, lochi2
                     if ((ii .ne. i) .or. (jj. ne. j) .or. (kk. ne. k)) then
                        rat = dentot * state(ii,jj,kk, DN) 
                        rat = rat / (dentot - state(i,j,k, DN))
                        state(ii,jj,kk, DN) = rat * frac
                        rat = edentot * state(ii,jj,kk, EN)
                        rat = rat / (edentot - state(i,j,k, EN))
                        state(ii,jj,kk, EN) = rat * frac
                     else
                        state(ii,jj,kk, DN) = dentot / ncell
                        state(ii,jj,kk, EN) = edentot / ncell
                     endif
                  enddo
               enddo
            enddo
            do ii = loclo0, lochi0
               do jj = loclo1, lochi1
                  do kk = loclo2, lochi2
                     den = state(ii,jj,kk, DN)
                     hvel2 = 5.d-1 * (state(ii,jj,kk, MX) **2 +
     &                    state(ii,jj,kk, MY) **2 +
     &                    state(ii,jj,kk, MZ) **2) /
     &                    den**2
                     if (BX > 0) then
                     hb2    = 5.d-1 * (state(ii,jj,kk, BX) **2 +
     &                    state(ii,jj,kk, BY) **2 +
     &                    state(ii,jj,kk, BZ) **2)
                     else
	             hb2    = 0.0
                     endif
                     state(ii,jj,kk,EN) = 
     &                    state(ii,jj,kk,EN) + den * hvel2 + hb2
                  enddo
               enddo
            enddo
            do itr=0,NTRACER-1
               state(i,j,k, TR+itr) = 
     +              state(i,j,k, TR+itr)*state(i,j,k, DN)
            end do
         endif
      endif
      if (EN > 0) then
         den = state(i,j,k, DN)
         hvel2 = 5.d-1 * (state(i,j,k, MX) **2 +
     &        state(i,j,k, MY) **2 +
     &        state(i,j,k, MZ) **2) /
     &        den**2
         if (BX > 0) then
         hb2    = 5.d-1 * (state(i,j,k, BX) **2 +
     &        state(i,j,k, BY) **2 +
     &        state(i,j,k, BZ) **2)
         else
	 hb2    = 0.0
         endif
         q = state(i,j,k,EN) - den * hvel2 - hb2
         if (q < SMALL_PR/(GMM-1.d0)) then
            flag = 1
            state(i,j,k,EN) = SMALL_PR/(GMM-1.d0) + den * hvel2 + hb2
         endif
         if(FLOOR_TGAS > 0.d0) then
            temp = (GMM-1.d0)*(state(i,j,k,EN) - den * hvel2 - hb2) / 
     +           (state(i,j,k,DN) * (CONST_kB/(2.33*CONST_mp)))
            if(temp < FLOOR_TGAS) then
               flag = 1  
               state(i,j,k,EN) = FLOOR_TGAS*(CONST_kB/(2.33*CONST_mp))/(GMM-1.d0)*
     +              state(i,j,k,DN) + den * hvel2 + hb2
            end if
         endif
      end if
      
      enddo
      enddo
      enddo
      end
      subroutine CHECKRAD(
     &           rad
     &           ,iradlo0,iradlo1,iradlo2
     &           ,iradhi0,iradhi1,iradhi2
     &           ,flag
     &           ,irreglo0,irreglo1,irreglo2
     &           ,irreghi0,irreghi1,irreghi2
     &           ,dx
     &           )

      implicit none
      integer iradlo0,iradlo1,iradlo2
      integer iradhi0,iradhi1,iradhi2
      REAL_T rad(
     &           iradlo0:iradhi0,
     &           iradlo1:iradhi1,
     &           iradlo2:iradhi2)
      integer flag
      integer irreglo0,irreglo1,irreglo2
      integer irreghi0,irreghi1,irreghi2
      REAL_T dx
      integer i,j,k
      real*8 floor_er
      real*8 x,y,z
      real*8 dbl_zero, dbl_one;
      include "commons_h.F"
      dbl_one = 1.d0
      dbl_zero = 0.d0
      floor_er = 7.5657e-15*FLOOR_TRAD**4/(UNIT_DENSITY*UNIT_VELOCITY**2)
      
      do k = irreglo2,irreghi2
      do j = irreglo1,irreghi1
      do i = irreglo0,irreghi0

      x = (i + 0.5d0) * dx
      y = (j + 0.5d0) * dx
      z = (k + 0.5d0) * dx
      if (rad(i,j,k) < floor_er) then
         flag = 1
         rad(i,j,k) = floor_er
      endif
      if (isnan( rad(i,j,k) ) .or.
     &     ABS( rad(i,j,k) ) == dbl_one/dbl_zero ) then
         flag = 2
         return
         print *, "Warning! NAN or +/- inf found in er at ", x,y,z
      endif
      
      enddo
      enddo
      enddo
      end
      subroutine ENFORCE_CEILVA(
     &           state
     &           ,istatelo0,istatelo1,istatelo2
     &           ,istatehi0,istatehi1,istatehi2
     &           ,nstatecomp
     &           ,density_added
     &           ,mom_change
     &           ,eng_change
     &           ,isreglo0,isreglo1,isreglo2
     &           ,isreghi0,isreghi1,isreghi2
     &           )

      implicit none
      integer nstatecomp
      integer istatelo0,istatelo1,istatelo2
      integer istatehi0,istatehi1,istatehi2
      REAL_T state(
     &           istatelo0:istatehi0,
     &           istatelo1:istatehi1,
     &           istatelo2:istatehi2,
     &           0:nstatecomp-1)
      REAL_T density_added
      REAL_T mom_change
      REAL_T eng_change
      integer isreglo0,isreglo1,isreglo2
      integer isreghi0,isreghi1,isreghi2
      integer i,j,k
      integer ni, itr
      real*8 den, b2, va2
      real*8 mom2, hvel2, q, pres, c2
      real*8 eta, deltaE, deltaM2, oq, eng
      include "commons_h.F"
      density_added = 0.d0
      mom_change =0.d0
      eng_change =0.d0
      
      do k = isreglo2,isreghi2
      do j = isreglo1,isreghi1
      do i = isreglo0,isreghi0

      den = state(i,j,k, DN)
      b2    = (state(i,j,k, BX) **2 +
     &         state(i,j,k, BY) **2 +
     &         state(i,j,k, BZ) **2)
      mom2 = 5.d-1 * (state(i,j,k, MX) **2 +
     &        state(i,j,k, MY) **2 +
     &        state(i,j,k, MZ) **2)
      hvel2 = mom2/den**2
      va2 = b2/den
      if (EN <= 0) then
         c2 = C_ISO*C_ISO
      else
         q = state(i,j,k,EN) - den * hvel2 - 0.5d0*b2
         pres =  q * (GMM-1.d0)
         c2 = (GMM * pres) / den
      end if
      if (EN>0) then
      	 eng = state(i,j,k,EN)
      end if
      oq = q
      if (va2 > CEIL_VA**2) then
        do itr=0,NTRACER-1
           state(i,j,k, TR+itr) =
     +           MIN(MAX(state(i,j,k, TR+itr)/
     +           state(i,j,k, DN), 0.d0), 1.d0)
        end do
        state(i,j,k, DN) = b2/CEIL_VA**2
        density_added = density_added + state(i,j,k, DN) - den
        do itr=0,NTRACER-1
           state(i,j,k, TR+itr) =
     +           state(i,j,k, TR+itr)*state(i,j,k, DN)
        end do
	den = state(i,j,k, DN)
        hvel2 = mom2 / den**2
        if (hvel2 > 0.5d0*CEIL_VA**2) then
          eta = sqrt(0.5d0*CEIL_VA**2/hvel2)
          state(i,j,k, MX) = state(i,j,k, MX)*eta
          state(i,j,k, MY) = state(i,j,k, MY)*eta
          state(i,j,k, MZ) = state(i,j,k, MZ)*eta
	  deltaM2 = 2.0d0*mom2 - state(i,j,k, MX) **2
     &            + state(i,j,k, MY) **2
     &            + state(i,j,k, MZ) **2
	  eng_change = eng_change + deltaM2 / (2.0d0 * den)
          mom_change = mom_change + sqrt(deltaM2)
        endif
        if (EN <= 0) then
          c2 = C_ISO*C_ISO
        else
          q = state(i,j,k,EN) - den * hvel2 - 0.5d0*b2
          pres =  q * (GMM-1.d0)
          c2 = (GMM * pres) / den
        end if
        if (c2 > CEIL_VA**2) then
          q = den * CEIL_VA**2/(GMM*(GMM-1.d0))
          if (hvel2 < 0.5d0*CEIL_VA**2) then
            state(i,j,k,EN) = q + den * hvel2 + 0.5d0*b2
          else
            state(i,j,k,EN) = q + b2
          endif
          eng_change = eng_change + oq - q
        endif
      else
        if (hvel2 > 0.5d0*CEIL_VA**2) then
          eta = sqrt(0.5d0*CEIL_VA**2/hvel2)
          state(i,j,k, MX) = state(i,j,k, MX)*eta
          state(i,j,k, MY) = state(i,j,k, MY)*eta
          state(i,j,k, MZ) = state(i,j,k, MZ)*eta
	  deltaM2 = 2.0d0*mom2 - state(i,j,k, MX) **2
     &            + state(i,j,k, MY) **2
     &            + state(i,j,k, MZ) **2
	  eng_change = eng_change + deltaM2 / (2.0d0 * den)
          mom_change = mom_change + sqrt(deltaM2)
        endif
        if (c2 > CEIL_VA**2) then
          q = den * CEIL_VA**2/GMM/(GMM-1.d0)
          if (hvel2 < 0.5d0*CEIL_VA**2) then
            state(i,j,k,EN) = q + den * hvel2 + 0.5d0*b2
          else
            state(i,j,k,EN) = q + den * 0.5d0*CEIL_VA**2 + 0.5d0*b2
          endif
          eng_change = eng_change + oq - q
        endif
      endif
      
      enddo
      enddo
      enddo
      end
