#include "REAL.H"
#include "SPACE.H"
#include "CONSTANTS.H"

      SUBROUTINE PRINT_COMMONS(
     &           )

      implicit none
      include "commons_h.F"
      PRINT*, '================================================='
      PRINT*, 'Parameters seen by fortran in commons_h.F'
      PRINT*, '================================================='
      PRINT*, 'DN', DN
      PRINT*, 'MX', MX
      PRINT*, 'MY', MY
      PRINT*, 'MZ', MZ
      PRINT*, 'VX', VX
      PRINT*, 'VY', VY
      PRINT*, 'VZ', VZ
      PRINT*, 'BX', BX
      PRINT*, 'BY', BY
      PRINT*, 'BZ', BZ
      PRINT*, 'PR', PR
      PRINT*, 'EN', EN
      PRINT*, 'TR', TR
      PRINT*, 'NTRACER', NTRACER
      PRINT*, ''
      PRINT*, 'CONST_mH', CONST_mH
      PRINT*, 'CONST_kB', CONST_kB
      PRINT*, 'CONST_sigma', CONST_sigma
      PRINT*, 'CONST_sigmaT', CONST_sigmaT
      PRINT*, 'CONST_NA', CONST_NA
      PRINT*, ''
      PRINT*, 'CONST_c', CONST_c
      PRINT*, 'CONST_Msun', CONST_Msun
      PRINT*, 'CONST_Rsun', CONST_Rsun
      PRINT*, 'CONST_Mearth', CONST_Mearth
      PRINT*, 'CONST_Rearth', CONST_Rearth
      PRINT*, ''
      PRINT*, 'CONST_G', CONST_G
      PRINT*, 'CONST_h', CONST_h
      PRINT*, 'CONST_pc', CONST_pc
      PRINT*, 'CONST_ly', CONST_ly
      PRINT*, 'CONST_au', CONST_au
      PRINT*, 'CONST_eV', CONST_eV
      PRINT*, ''
      PRINT*, 'gmm', gmm
      PRINT*, 'c_iso', c_iso
      PRINT*, ''
      PRINT*, 'SMALL_DN', SMALL_DN
      PRINT*, 'SMALL_PR', SMALL_PR
      PRINT*, 'T_CUT_COOL', T_CUT_COOL
      PRINT*, 'FLOOR_TRAD', FLOOR_TRAD
      PRINT*, 'FLOOR_TGAS', FLOOR_TGAS
      PRINT*, 'CEIL_VA', CEIL_VA
      PRINT*, '================================================='
      PRINT*, 'End fortran commons.'
      PRINT*, '================================================='
      END
