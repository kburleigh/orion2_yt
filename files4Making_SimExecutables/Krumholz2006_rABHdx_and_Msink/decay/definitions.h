#define    PHYSICS   MHD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   15
#define    TURBULENCE_DRIVING   INITIAL_ONLY
#define    PRINT_TO_STDOUT   YES
#define    RAREFACTION_FLATTEN   YES
#define    SINK_PARTICLES   NO
#define    STAR_PARTICLES   NO
#define    CLUSTER_PARTICLES   NO
#define    RAD_PARTICLES   NO
#define    CIC   NO
#define    RADIATION   NO
#define    RAYTRACE_DIRECT_RAD   NO
#define    RAYTRACE_IONIZATION   NO
#define    FUV   NO
#define    IONZATION_CONS_RECOMB   NO
#define    DO_CHEMISTRY   NO
#define    CHEM_NSPECIES   0
#define    SELF_GRAVITY   NO
#define    NEWTON_COOL   NO

/* -- physics dependent declarations -- */

#define    EOS   ISOTHERMAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    RESISTIVE_MHD   NO
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  GAMMA   0
#define  vinit   1
#define  ldrv   2
#define  den0   3
#define  my_small_dn   4
#define  vx0   5
#define  vy0   6
#define  vz0   7
#define  bx0   8
#define  by0   9
#define  bz0   10
#define  pr0   11
#define  cs0   12
#define  Jeans   13
#define  SCRH   14

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      NO
#define  PRINT_TO_FILE         NO
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               mc_lim
#define  CT_EMF_AVERAGE        UCT_HLL
#define  CT_EN_CORRECTION      YES
#define  CT_VEC_POT_INIT       NO
#define  COMPUTE_DIVB          NO
