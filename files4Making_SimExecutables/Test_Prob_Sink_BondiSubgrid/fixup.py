import odict
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
fontsize = 18
mpl.rcParams['font.size'] = fontsize
mpl.rcParams['xtick.labelsize'] = fontsize
mpl.rcParams['ytick.labelsize'] = fontsize
mpl.rcParams['axes.titlesize'] = fontsize
mpl.rcParams['axes.labelsize'] = fontsize
mpl.rcParams['legend.fontsize'] = 12
mpl.rcParams['axes.linewidth'] = 2.0
mpl.rcParams['lines.linewidth'] = 2.0
mpl.rcParams['lines.markeredgewidth'] = 2.0
#mpl.rcParams['mathtext.fontset'] = 'stix'
mpl.rcParams['figure.subplot.bottom']=0.17
mpl.rcParams['figure.subplot.left']=0.15

CONST_G = 6.6726e-8 
UNIT_DENSITY = 1.0/(CONST_G)
UNIT_LENGTH = 1.0
UNIT_VELOCITY = 1.0
G_CODE = CONST_G*(UNIT_VELOCITY/UNIT_LENGTH)**2*UNIT_DENSITY
LAMBDA = 1.1204222675845161
BETAB = 19.4
NB = 1.

mdot_par = odict.odict({})
mdot_par[(1000,0.)] = 1#4.900000e-01
mdot_par[(100.,0.)] = 1#3.510000e-01
mdot_par[(10.,0.)] = 2.270000e-01
mdot_par[(1.,0.)] = 1.290000e-01
mdot_par[(0.1,0.)] = 6.300000e-02
mdot_par[(0.01,0.)] = 2.300000e-02

mdot_par[(100.,0.014)] = 1#0.32269367
mdot_par[(100.,1.41)] = 0.36289792
mdot_par[(100.,4.47)] = 0.0104879345521
#mdot_par[(10.,0.141)] = 0.247
mdot_par[(10.,1.41)] = 0.27295584
mdot_par[(10.,4.47)] = 0.01206267
mdot_par[(1.,1.41)] = 0.10623015
mdot_par[(1.,4.47)] = 0.01284386
mdot_par[(0.1,0.447)] = 0.06374545
mdot_par[(0.1,44.7)] = 0.00000932
mdot_par[(0.1,4.47)] = 0.00183674
mdot_par[(0.01,4.47)] = 0.00205473758772

mdot_perp = odict.odict({})
mdot_perp[(1000,0.)] = 1#4.900000e-01
mdot_perp[(100.,0.)] = 1#3.510000e-01
mdot_perp[(10.,0.)] = 2.270000e-01
mdot_perp[(1.,0.)] = 1.290000e-01
mdot_perp[(0.1,0.)] = 6.300000e-02
mdot_perp[(0.01,0.)] = 2.300000e-02

mdot_perp[(100.,0.014)] = 1#0.37942408
mdot_perp[(100.,1.41)] = 0.33178516
mdot_perp[(100.,4.47)] = 0.0101961607753
mdot_perp[(10.,1.41)] = 0.29444388
mdot_perp[(10.,4.47)] = 0.01067521
mdot_perp[(1.,1.41)] = 0.18162350
mdot_perp[(1.,4.47)] = 0.01175693
mdot_perp[(0.1,0.447)] = 0.06130758
mdot_perp[(0.1,44.7)] = 0.00000874
mdot_perp[(0.1,4.47)] = 0.01119731
mdot_perp[(0.01,4.47)] = 0.00259851807017

f=open('result.txt')

labelhihi=r'$\Delta x = r_B/64$'
labelhi=r'$\Delta x = r_B/32$'
labelmed=r'$\Delta x = r_B/8$'
labello=r'$\Delta x = r_B/2$'
er0 = odict.odict()
er = odict.odict()
for line in f:
    try:
        line = line.strip(' \n').split()[0:4]
        line[0] = float(line[0])
        line[1] = float(line[1])
        line[3] = float(line[3])
        direction = line[2]
    except:
        if line[0][0:5]=='orion': 
            print line[0]
            ext=line[0][11:]
            if ext=='hihires':
                label = labelhihi
            elif ext=='hires':
                label = labelhi
            elif ext=='medres':
                label = labelmed
            if ext=='lowres':
                label = labello
        continue

    if direction=='par': mdot=mdot_par
    if direction=='per': mdot=mdot_perp

    beta = line[0]
    Mach = line[1]
   
    MachABH = (((1.+Mach**4)**(1./3.)/(1.+(Mach/LAMBDA)**2)**(1./6.))**NB + (BETAB/beta)**(NB/2.))**(1./NB)
    MachBH = (1.+Mach**4)**(1./3.)/(1.+(Mach/LAMBDA)**2)**(1./6.)

    # parallel
    if direction=='par': mdotf = 1./MachBH**2*(MachBH**NB+(BETAB/beta)**(NB/2.))**(-1./NB)
    # perpendicular
    if direction=='per': mdotf = 1./MachBH*min(1./MachBH**2,1./MachABH)

    #print beta,Mach
    try:
        error = (line[3]+1.)*mdotf/mdot[(beta,Mach)]
        fit_error = line[3]+1
        print "%(a)7.2f  %(b)6.3f  %(c)s  %(d)6.3f  %(e)6.3f"%{'a':beta, 'b':Mach, 'c':direction, 'd':fit_error, 'e':error}
        er0[(beta,Mach,direction,label)] = fit_error
        er[(beta,Mach,direction,label)] = error
    except:
        pass

rms_error = {}
count = {}
for key in er:
    if not key[3] in rms_error: 
        rms_error[key[3]] = 0.
        count[key[3]] = 0
    rms_error[key[3]] += er[key]**2
    count[key[3]] += 1
for key in rms_error:
    rms_error[key] = sqrt(rms_error[key]/count[key])

px=[]
py0=[]
py=[]
for label in [labelhihi,labelhi,labelmed,labello]:
    for key in er0:
        beta,Mach,direction,labelnow = key
        y = er[key]
        x = beta
        if direction=='par': color='k'
        if direction=='per': color='r'
        if labelnow==label:
            if Mach==0:
                color = 'k'
                m0,=plt.plot(x,y,ls='None',marker='o',markersize=12,color='None',markeredgecolor=color)
            elif Mach==0.014:
                m1,=plt.plot(x,y,ls='None',marker='*',markersize=12,color='None',markeredgecolor=color)
#            elif Mach==0.141: 
#                m2,=plt.plot(x,y,ls='None',marker='x',markersize=12,color=color)
            elif Mach==0.447: 
                m3,=plt.plot(x,y,ls='None',marker='^',markersize=12,color='None',markeredgecolor=color) 
            elif Mach==1.41: 
                m4,=plt.plot(x,y,ls='None',marker='+',markersize=12,color=color) #
            elif Mach==4.47: 
                m5,=plt.plot(x,y,ls='None',marker='D',markersize=12,color='None',markeredgecolor=color) 
            elif Mach==44.7: 
                m6,=plt.plot(x,y,ls='None',marker='.',markersize=10,color=color) #
            else:
                print Mach
                print 'crap! should never get here.' 
                import sys;sys.exit()
    plt.gca().set_xlabel(r'$\beta$') 
    plt.gca().set_ylabel(r'$\dot{M_{\rm SG}}/\dot{M}$')
    plt.gca().set_yscale('log')
    plt.gca().set_xscale('log')
    plt.title(label)
    if label==labelhihi: plt.savefig('errhihi.png', dpi=200)
    if label==labelhi: plt.savefig('errhi.png', dpi=200)
    if label==labelmed: plt.savefig('errmed.png', dpi=200)
    if label==labello: plt.savefig('errlo.png', dpi=200)
    plt.clf()

#m2.set_color('k')

legend=plt.figure(figsize = (1.625,2.0))
#plt.figlegend((m0,m1,m2,m3,m4,m5,m6),(r'${\cal M}=0$',r'${\cal M}=0.0447$',r'${\cal M}=0.141$',r'${\cal M}=0.447$',r'${\cal M}=1.41$',r'${\cal M}=4.47$',r'${\cal M}=44.7$'),loc='left',numpoints=1)
plt.figlegend((m0,m1,m3,m4,m5,m6),(r'${\cal M}=0$',r'${\cal M}=0.14$',r'${\cal M}=0.447$',r'${\cal M}=1.41$',r'${\cal M}=4.47$',r'${\cal M}=44.7$'),loc='left',numpoints=1)
plt.savefig('errlegend.png', dpi=200)

import os
#os.system('convert -trim errlegend.png errlegend.png')
os.system('convert +append errlo.png errlegend.png errloleg.png')
os.system('convert +append errmed.png errlegend.png errmedleg.png')
os.system('convert +append errhi.png errlegend.png errhileg.png')
os.system('convert +append errhihi.png errlegend.png errhihileg.png')
os.system('convert -append errlo.png errhi.png 1.png;convert -append errmed.png errhihi.png 2.png;convert +append 1.png 2.png errlegend.png outleg.png')

for key in rms_error:
    print 'rms error', key, rms_error[key]
