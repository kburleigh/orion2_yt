#define    PHYSICS   MHD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   13
#define    TURBULENCE_DRIVING   NO
#define    PRINT_TO_STDOUT   YES
#define    RAREFACTION_FLATTEN   YES
#define    SINK_PARTICLES   YES
#define    STAR_PARTICLES   YES
#define    CLUSTER_PARTICLES   NO
#define    RAD_PARTICLES   NO
#define    CIC   NO
#define    RADIATION   YES
#define    RAYTRACE_DIRECT_RAD   NO
#define    RAYTRACE_IONIZATION   NO
#define    FUV   NO
#define    IONZATION_CONS_RECOMB   NO
#define    DO_CHEMISTRY   NO
#define    CHEM_NSPECIES   0
#define    SELF_GRAVITY   NO
#define    NEWTON_COOL   NO

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    RESISTIVE_MHD   NO
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  GAMMA   0
#define  ldrv   1
#define  den0   2
#define  vx0   3
#define  vy0   4
#define  vz0   5
#define  bx0   6
#define  by0   7
#define  bz0   8
#define  pr0   9
#define  cs0   10
#define  Jeans   11
#define  SCRH   12

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      NO
#define  PRINT_TO_FILE         NO
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               mc_lim
#define  CT_EMF_AVERAGE        UCT_HLL
#define  CT_EN_CORRECTION      YES
#define  CT_VEC_POT_INIT       NO
#define  COMPUTE_DIVB          NO
