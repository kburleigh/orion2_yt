import os
import sys

#MPIRUN = "mpirun -np 4"
#oneRun = False

MPIRUN = "ibrun"
oneRun = False

MPIRUN = "ibrun"
oneRun = False

#MPIRUN = "aprun -n 12"
#oneRun = True

header = """
#!/bin/bash         
#$ -V                     # Inherit the submission environment
#$ -cwd                   # Start job in  submission directory
#$ -N allrun               # Job Name
#$ -j y                   # combine stderr & stdout into stdout    
#$ -o $JOB_NAME.o$JOB_ID  # Name of the output file (eg. myMPI.oJobID)
#$ -pe 16way 16           # Requests 16 cores/node, 32 cores total
#$ -q normal              # Queue name
#$ -l h_rt=12:00:00
##$ -q development              # Queue name
##$ -l h_rt=1:00:00
## -M myEmailAddress      # Email notification address (UNCOMMENT)
## -m be                  # Email at Begin/End of job  (UNCOMMENT)

source ~/.bashrc
"""

def run(parameters, input_files):
    i = 0
    for input_file in input_files:
        for parameter in parameters:
            i+=1
            beta = parameter[0]
            Mach = parameter[1]
            dir = parameter[2]
            # if this is the mach 10 case, set tend to a smaller value to avoid boundary effects
            sed_str1 = 'sleep 10 && cat ' + input_file + ' | sed \'s/tstop            .*/tstop            1.0/g\' > orion2.ini'
            exec_str = \
                "mkdir run%d\n"%i + \
                "cp orion2 init.sink Bondi_Plot.py run%d/ \n"%i
            if Mach >= 5.:  exec_str += sed_str1 + "\n"
            exec_str += \
                "cp "+input_file+" run%d/orion2.ini \n"%i + \
                "cd run%d\n"%i + \
                "export MACH=%e"%Mach + "\n" + \
                "export BETA=%e"%beta + "\n" + \
                "export DIR=\'" + dir + "\'\n" + \
                MPIRUN+" ./orion2 -restart -1 &> run.${JOB_ID}.out &\n" + \
                "sleep 10 && cd ..\n\n"
            if oneRun:
                print exec_str
            else:
                f=open('qsub.run%d'%i,'w')
                f.write(header)
                f.write(exec_str)
                f.write("wait \n")
                f.close()
    i = 0
    if oneRun: print "wait \n"
    for input_file in input_files:
        for parameter in parameters:
            i+=1
            beta = parameter[0]
            Mach = parameter[1]
            dir = parameter[2]
            exec_str = \
                "cp Bondi_Plot.py run%d\n"%i + \
                "cd run%d\n"%i + \
                "export MACH=%e"%Mach + "\n" + \
                "export BETA=%e"%beta + "\n" + \
                "export DIR=\'" + dir + "\'\n" + \
                "python Bondi_Plot.py\n" + \
                "cd ..\n"
            if oneRun:
                print exec_str
            else:
                f=open('qsub.run%d'%i,'a')
                f.write(exec_str)
                f.close()
                print 'qsub '+'qsub.run%d'%i

# run Aaron's parameter values
parameters = []
f = open('BondiHoyleFit.txt')
for line in f:
    line = line.strip(' \n')
    if line[0] == '#': continue
    line = line.split(' ')
    while '' in line:
        line.remove('')
    parameters.append((float(line[0]),float(line[1]),'per'))
    if float(line[1])==0.:continue
    parameters.append((float(line[0]),float(line[1]),'par'))

input_files = ['orion2.ini.hires']
run(parameters, input_files)
