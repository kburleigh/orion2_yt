import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from math import *
import os
from yt.mods import *
fontsize = 18
mpl.rcParams['font.size'] = fontsize
mpl.rcParams['xtick.labelsize'] = fontsize
mpl.rcParams['ytick.labelsize'] = fontsize
mpl.rcParams['axes.titlesize'] = fontsize
mpl.rcParams['axes.labelsize'] = fontsize
mpl.rcParams['legend.fontsize'] = fontsize
mpl.rcParams['axes.linewidth'] = 2.0
mpl.rcParams['lines.linewidth'] = 2.0
mpl.rcParams['lines.markeredgewidth'] = 1.0
#mpl.rcParams['text.usetex'] = True

CONST_G = 6.6726e-8 
UNIT_DENSITY = 1.0/(CONST_G)
UNIT_LENGTH = 1.0
UNIT_VELOCITY = 1.0
G_CODE = CONST_G*(UNIT_VELOCITY/UNIT_LENGTH)**2*UNIT_DENSITY
LAMBDA = 1.1204222675845161
NB = 1.
BETAB = 19.4

f = open('orion2.ini','r')
for line in f:
    line = line.strip(' \n').split(' ')
    while '' in line: line.remove('')
    if len(line)<1: continue
    if 'den0' == line[0]: den0 = float(line[1])
    if 'cs0'  == line[0]:  cs0 = float(line[1])
    if 'vx0'  == line[0]:  vx0 = float(line[1])
    if 'vy0'  == line[0]:  vy0 = float(line[1])
    if 'vz0'  == line[0]:  vz0 = float(line[1])
    if 'bx0'  == line[0]:  bx0 = float(line[1])
    if 'by0'  == line[0]:  by0 = float(line[1])
    if 'bz0'  == line[0]:  bz0 = float(line[1])
f.close()

P0 = den0*cs0**2
beta = 2.*P0/max(bx0**2+by0**2+bz0**2,1.e-300)
Mach = sqrt(vx0**2+vy0**2+vz0**2)/cs0
cos_angle = (bx0*vx0 + by0*vy0 + bz0*vz0) / sqrt((vx0**2+vy0**2+vz0**2)* max(bx0**2+by0**2+bz0**2,1.e-300))
if cos_angle == 1.0: dir='par'
if cos_angle == 0.0: dir='per'

# check if we are doing an automated parameter survey
Mach_env = os.getenv("MACH")
beta_env = os.getenv("BETA")
dir_env = os.getenv("DIR")
if Mach_env!=None and beta_env!=None:
    Mach = float(Mach_env)
    beta = float(beta_env)
    dir = dir_env
    if beta == 0.: beta=1.e300

mdotB = 4. * pi * den0 * LAMBDA * (G_CODE * 1.0)**2 / cs0**3

MachABH = (((1.+Mach**4)**(1./3.)/(1.+(Mach/LAMBDA)**2)**(1./6.))**NB + (BETAB/beta)**(NB/2.))**(1./NB)
MachBH = (1.+Mach**4)**(1./3.)/(1.+(Mach/LAMBDA)**2)**(1./6.)
# parallel
if dir=='par': mdot = mdotB/MachBH**2*(MachBH**NB+(BETAB/beta)**(NB/2.))**(-1./NB)
# perpendicular
if dir=='per': mdot = mdotB/MachBH*min(1./MachBH**2,1./MachABH)

i=0
m=[]
mana = []
t=[]
while os.path.exists('data.%04d.3d.sink'%i):
    f = open('data.%04d.3d.sink'%i)
    line = f.readline()
    line = f.readline()
    f.close()
    m.append(float(line.strip(' \n').split(' ')[0]))
    pf = load('data.%04d.3d.hdf5'%i)
    t.append(pf.current_time)
    mana.append(t[-1]*mdot)
    i += 1

if t[-1]-t[-2] < 0.01:
    t.pop()
    m.pop()
    mana.pop()

m = np.array(m) - m[0]

plt.figure().subplots_adjust(left=0.25)
plt.plot(t,m)
plt.plot(t,mana)
plt.gca().set_xlabel(r'$t/t_B$')
plt.gca().set_ylabel(r'$(\dot{M} t -M_0)\,/\, M_0$')
plt.gca().set_title(r'$\beta=$ %.3f'%beta+r',   Mach = '+str(Mach))
plt.savefig('bondi.png')


mdot_code = (m[-1]-m[-2]) / (t[-1]-t[-2])
error = abs(mdot_code-mdot)/mdot

# error criteria setup for beta=1.0, Mach=1.41, direction=parallel
# run to t = 1 t_B on a 32 r_B^3 domain with 32^3 cells and 2 AMR levels.
print error
if error < 0.15:
    print 'Pass!'
    writer=open('test_pass', 'w')
    writer.close()
else:
    print 'FAIL!'

