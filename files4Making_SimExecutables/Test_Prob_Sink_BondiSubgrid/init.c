#include "pluto.h"

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 * 
 * NAME 
 * 
 *   INIT
 *
 *
 * PURPOSE
 *
 *   Set inital condition. 
 *
 *
 * ARGUMENTS 
 *
 *   us (OUT)           a pointer to a array_1D of primitive 
 *                      (when *prim_var = 1) or conservative
 *                      (when *prim_var = 0) variables;
 *
 *   x1, x2, x3 (IN)    the three coordinates; the meaning is different
 *                      depending on the geometry:
 *
 *                       x1      x2      x3
 *                       -------------------                
 *                        x       y       z     in cartesian geometry
 *                        r       z       -     in cylindrical geometry 
 *                        r      phi      z     in polar geometry 
 *                        r     theta    phi    in spherical geometry
 *                  
 *   i , j , k  (IN)    the integer indexes of the cell containing the 
 *                      point x1, x2, x3.
 *                
 *   *prim_var (OUT)    an integer flag. When *prim_var = 1 initial 
 *                      conditions are assigned in terms of 
 *                      primitive values. When *prim_var = 0 initial
 *                      conditions are given in terms of conservative 
 *                      value.
 *
 *
 * Variable names are accessed as us[nv], where
 *
 *   nv = DN is density
 *   nv = PR is pressure
 *  
 *   Vector components are labelled always as VX,VY,VZ, but alternative
 *   labels may be used:
 *
 *   Cartesian       VX     VY      VZ
 *   Cylindrical    iVR    iVZ     iVPHI
 *   Polar          iVR    iVPHI   iVZ
 *   Spherical      iVR    iVTH    iVPHI
 *
 * 
 *
 **************************************************************** */
{

  real r, rho, u, press, beta;
  char *env_var, *dir;
  #if EOS == IDEAL
  real C_ISO;
  #endif

  rho = aux[den0];
  #if EOS == IDEAL
   gmm = aux[GAMMA];
   press = rho*aux[cs0]*aux[cs0]/gmm;
   us[PR] = press;
  #else
   C_ISO = aux[cs0];
   press = rho*C_ISO*C_ISO;
  #endif

  *prim_var = 1;

  // work in G==1 units
  SMALL_DN = aux[den0]/1e4;
  SMALL_PR = SMALL_DN*C_ISO*C_ISO/1e2;
  UNIT_DENSITY = 1.0/(CONST_G);

  r = sqrt(x1*x1+x2*x2+x3*x3);

  us[DN] = aux[den0];
  //if (us[DN] > 1e-8) us[DN] = 16.*aux[den0]/r;
  us[VX] = aux[vx0];
  us[VY] = aux[vy0];
  us[VZ] = aux[vz0];

#if PHYSICS == MHD
   us[BX] = aux[bx0];
   us[BY] = aux[by0];
   us[BZ] = aux[bz0]; 
#endif

  // grab mach # and beta from environment variable, if set
  //env_var = getenv ( "MACH" );
  //if (env_var != NULL) {
  //  us[VY] = us[VZ] = 0; 
  //  us[VX] = atof(env_var);
  //}
  //env_var = getenv ( "BETA" );
  //dir = getenv ( "DIR" );
  //if (env_var != NULL) {
  //  if (atof(env_var) == 0.) {
  //    us[BX] = us[BY] = us[BZ] = 0.;
  //  } else {
  //    if (0==strcmp(dir,"par")){
  //  us[BY] = us[BZ] = 0.;
  //  us[BX] = sqrt(2.*press/atof(env_var));
  //    } else {
  //  us[BX] = us[BY] = 0.;
  //  us[BZ] = sqrt(2.*press/atof(env_var));
  //    }
  //  }
  //}
}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   ANALYSIS
 *
 *
 * PURPOSE
 *  
 *   Perform some pre-processing data
 *
 * 
 * ARGUMENTS
 *
 *   uu(IN)     solution data.
 *
 *   GG(IN)     pointer to array of GRID structures  
 *
 **************************************************************** */
{

}
#if PHYSICS == MHD
/* ************************************************************** */
void BACKGROUND_FIELD (real x1, real x2, real x3, real *B0)
/* 
 *
 *
 * NAME
 * 
 *   BACKGROUND_FIELD
 *
 *
 * PURPOSE
 *
 *   Define the component of a static, curl-free background 
 *   magnetic field.
 *
 *
 * ARGUMENTS
 *
 *   x1, x2, x3  (IN)    coordinates
 *
 *   B0         (OUT)    array_1D component of the background field.
 *
 *
 **************************************************************** */
{
   B0[0] = 0.0;
   B0[1] = 0.0;
   B0[2] = 0.0;
}
#endif


/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid)
/* 
 *
 * 
 **************************************************************** */
{
}


#if ADD_INTERNAL_BOUNDARY == YES
/* ************************************************************** */

void INTERNAL_BOUNDARY(real ***uu[], real ***uu_old[],
                       Grid *grid, 
                       int i0, int i1, int j0, int j1, int k0, int k1)

/* 
 *
 *
 * NAME
 *
 *   INTERNAL_BOUNDARY
 *
 *
 * PURPOSE
 *
 *   Allow the user to control 
 *
 *
 * ARGUMENTS
 *
 *   uu      (IN/OUT)    three-dimensional array_1D containing the solution data;
 *
 *   uu_old  (IN/OUT)    old, kept for backward compatibility;
 *
 *   grid    (IN)        pointer to grid structures;
 * 
 *   i0, j0, k0 (IN)     indexes of the lower-coordinate point inside
 *                       the domain; 
 *
 *   i1, j1, k1, (IN)    indexes of the upper-coordinate point inside
 *                       the domain.
 *   
 *  
 *
 *
 **************************************************************** */
{
  int  i, j, k;
  real x1, x2, x3;
  
  for (k = k0; k <= k1; k++) { x3 = grid[KDIR].x[k];  
  for (j = j0; j <= j1; j++) { x2 = grid[JDIR].x[j];  
  for (i = i0; i <= i1; i++) { x1 = grid[IDIR].x[i];  
         
  }}}
    
}

#endif
