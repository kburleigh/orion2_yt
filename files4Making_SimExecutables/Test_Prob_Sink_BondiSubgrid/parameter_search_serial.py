import os
import sys

def run(parameters, input_files):
    for input_file in input_files:
        header_str = 'echo '+input_file+' >> result.txt'
        print header_str
        os.system(header_str)
        for parameter in parameters:
            beta, Mach, dir = parameter
            cp_str = 'cp '+input_file+' orion2.ini'
            # if this is the mach 10 case, set tend to a smaller value to avoid boundary effects
            sed_str1 = 'sleep 10 && cat ' + input_file + ' | sed \'s/tstop            .*/tstop            1.0/g\' > orion2.ini'
            exec_str = \
                "export MACH=%e"%Mach + \
                " && export BETA=%e"%beta + \
                " && export DIR=\'" + dir + "\'" + \
                " && mpirun -np 4 nice ./orion2 < /dev/null && python Bondi_Plot.py && rm data.* chk.*"

            if Mach >= 5.:  
                print sed_str1
                os.system(sed_str1)
            print cp_str
            os.system(cp_str)
            print exec_str
            os.system(exec_str)

# run Aaron's parameter values
parameters = []
f = open('BondiHoyleFit.txt')
for line in f:
    line = line.strip(' \n')
    if line[0] == '#': continue
    line = line.split(' ')
    while '' in line:
        line.remove('')
    parameters.append((float(line[0]),float(line[1]),'per'))
    if float(line[1])==0.:continue
    parameters.append((float(line[0]),float(line[1]),'par'))

input_files = ['orion2.ini.lowres','orion2.ini.medres']
run(parameters, input_files)
