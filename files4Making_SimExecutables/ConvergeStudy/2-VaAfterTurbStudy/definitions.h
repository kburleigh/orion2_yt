#define    PHYSICS   MHD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   9
#define    TURBULENCE_DRIVING   HDF5
#define    PRINT_TO_STDOUT   YES
#define    RAREFACTION_FLATTEN   NO
#define    SINK_PARTICLES   NO
#define    STAR_PARTICLES   NO
#define    CLUSTER_PARTICLES   NO
#define    RAD_PARTICLES   NO
#define    CIC   NO
#define    RADIATION   NO
#define    RAYTRACE_DIRECT_RAD   NO
#define    RAYTRACE_IONIZATION   NO
#define    FUV   NO
#define    IONZATION_CONS_RECOMB   NO
#define    DO_CHEMISTRY   NO
#define    CHEM_NSPECIES   0
#define    SELF_GRAVITY   NO
#define    NEWTON_COOL   NO

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    RESISTIVE_MHD   NO
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  ldrv   0
#define  vinit   1
#define  GAMMA   2
#define  CS   3
#define  B_CODE   4
#define  RHO0   5
#define  MY_CEIL_VA   6
#define  MY_SMALL_DN   7
#define  MY_SMALL_PR   8

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      NO
#define  PRINT_TO_FILE         NO
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               mc_lim
#define  CT_EMF_AVERAGE        UCT_CONTACT
#define  CT_EN_CORRECTION      YES
#define  CT_VEC_POT_INIT    NO
#define  COMPUTE_DIVB       NO
