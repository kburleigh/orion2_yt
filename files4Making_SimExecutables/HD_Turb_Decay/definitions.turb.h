#define    PHYSICS   HD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   9
#define    TURBULENCE_DRIVING   INITIAL_ONLY
#define    PRINT_TO_STDOUT   YES
#define    RAREFACTION_FLATTEN   NO
#define    SINK_PARTICLES   NO
#define    STAR_PARTICLES   NO
#define    CLUSTER_PARTICLES   NO
#define    RAD_PARTICLES   NO
#define    RADIATION   NO
#define    RAYTRACE_DIRECT_RAD   NO
#define    RAYTRACE_IONIZATION   NO
#define    FUV   NO
#define    IONZATION_CONS_RECOMB   NO
#define    DO_CHEMISTRY   NO
#define    CHEM_NSPECIES   0
#define    SELF_GRAVITY   NO
#define    NEWTON_COOL   NO

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    ENTROPY_SWITCH   NO
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  GAMMA   0
#define  CS   1
#define  ldrv   2
#define  vinit   3
#define  RHO0   4
#define  MU   5
#define  MY_FLOOR_TGAS   6
#define  MY_SMALL_DN   7
#define  MY_SMALL_PR   8

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      YES
#define  PRINT_TO_FILE         YES
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               mc_lim
