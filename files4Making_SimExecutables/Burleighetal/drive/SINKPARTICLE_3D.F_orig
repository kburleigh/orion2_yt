#undef CH_LANG_CC
#define CH_LANG_FORT

#include "REAL.H"
#include "SINKPARTICLE_F.H"

#define SDIM 3
#define LAMBDA 1.1204222675845161

c new fit
#define NB 1.d0
#define BETAB 19.4d0

c...  Parameters used throughout:
#define G_CODE (CONST_G*(UNIT_VELOCITY/UNIT_LENGTH)**2*UNIT_DENSITY)

c...  Routine to create sink particles in cells that are violating
c...  the Jeans condition. On exit, pos, mom, ang_mom, and mass should
c...  contain the position and mass of every new sink cell.

      subroutine FORT_SINK_CREATE(sloi1, sloi2, sloi3, shii1, shii2,
     .     shii3, nstate, state, xlo, dx, ncells,
     .     pos, mom, ang_mom, mass, nsink)

      implicit none

      integer sloi1, sloi2, sloi3, shii1, shii2, shii3, nstate
      integer nsink, ncells
      REAL_T state(sloi1:shii1, sloi2:shii2, sloi3:shii3, 0:nstate-1)
      REAL_T phi(sloi1:shii1, sloi2:shii2, sloi3:shii3, 1)
      REAL_T xlo(SDIM), dx(SDIM)
      REAL_T pos(ncells*SDIM), mom(ncells*SDIM), ang_mom(ncells*SDIM)
      REAL_T mass(ncells)
      include "commons_h.F"

#define PI 3.1415926535897931d0
#define JEANSNO 0.25d0

      REAL_T rho(sloi1:shii1,sloi2:shii2,sloi3:shii3)
      REAL_T eint(sloi1:shii1,sloi2:shii2,sloi3:shii3)
      REAL_T gamc(sloi1:shii1,sloi2:shii2,sloi3:shii3)
      REAL_T p(sloi1:shii1,sloi2:shii2,sloi3:shii3)
      REAL_T c(sloi1:shii1,sloi2:shii2,sloi3:shii3)
      REAL_T dxmax, rho_J, ethermal, pres
      REAL_T especific, velx, vely, velz, ke
      REAL_T cfluid(sloi1:shii1,sloi2:shii2,sloi3:shii3)
      integer itr
      
      integer i,j,k

      REAL_T beta, b2, den, vol

c...  Find maximum grid spacing
      dxmax = dx(1)
      vol=dx(1)*dx(2)*dx(3)
      do i=2,3
         if (dxmax .lt. dx(i)) then
            dxmax = dx(i)
         endif
      enddo

c...  Initialize effective sound speed array
      do k=sloi3, shii3
         do j=sloi2, shii2
            do i=sloi1, shii1
               IF(EN<=0) THEN
c...  Isothermal EOS
                  c(i,j,k) = C_ISO
               ELSE
c...  Ideal Gas EOS
                  c(i,j,k) = SQRT(GMM*((GMM-1.d0)*
     .                 MAX(state(i,j,k,EN) - 
     .                 0.5d0*(state(i,j,k,MX)**2+state(i,j,k,MY)**2+state(i,j,k,MZ)**2)/state(i,j,k,DN) -
     .                 0.5d0*(state(i,j,k,BX)**2+state(i,j,k,BY)**2+state(i,j,k,BZ)**2),SMALL_PR))/state(i,j,k,DN))
               END IF
            enddo
         enddo
      enddo

c...  Check for cells that are violating the Jeans condition
      do k=sloi3, shii3
         do j=sloi2, shii2
            do i=sloi1, shii1
c...  Maximum density allowed by the Jeans condition
               rho_J = PI * c(i,j,k)**2 / (G_CODE * (dxmax/JEANSNO)**2)

               b2 = state(i,j,k,BX)**2+state(i,j,k,BY)**2+state(i,j,k,BZ)**2
               IF(EN<=0) THEN
c...  Isothermal EOS
                  beta = 2.d0 * state(i,j,k,DN) * C_ISO*C_ISO / MAX(b2, 1.d-100)
               ELSE
c...  Ideal Gas EOS
                  pres =
     .                 (GMM-1.d0)*(state(i,j,k,EN) -
     .                 0.5d0*(state(i,j,k,MX)**2 + state(i,j,k,MY)**2+state(i,j,k,MZ)**2)/state(i,j,k,DN) -
     .                 0.5d0*(state(i,j,k,BX)**2+state(i,j,k,BY)**2+state(i,j,k,BZ)**2))

                  beta = 2.d0 * pres / MAX(b2, 1.d-100)
               END IF
               
               rho_J = rho_J * (1.d0 + (7.4d-1/beta))

c...  Create a sink if density > jeans density
               if (state(i,j,k,DN) .gt. rho_J) then

c...  Put sink in the center of the Jeans-violating cell
                  pos(nsink*3+1) = xlo(1) + dx(1) * (0.5d0 + i)
                  pos(nsink*3+2) = xlo(2) + dx(2) * (0.5d0 + j)
                  pos(nsink*3+3) = xlo(3) + dx(3) * (0.5d0 + k)

c...  Store the specific energy of the cell
                  IF(.NOT. EN<=0) THEN
                     especific = (state(i,j,k,EN) - 0.5d0* 
     .                   (state(i,j,k,BX)**2+state(i,j,k,BY)**2+state(i,j,k,BZ)**2))/state(i,j,k,DN) 
                  END IF
c...  Set sink cell mass equal to the amount of mass by which the cell is
c...  violating Jeans. Remove an equal amount of mass from the cell,
c...  divided among the fluids proportionally to their density fraction
c...  in the cell
                  mass(nsink+1) = (state(i,j,k,DN) - rho_J) * vol
                  den=state(i,j,k,DN)
c...  Keep advective tracer fractions constant
                  DO itr=0,NTRACER-1
                     state(i,j,k,TR+itr) = state(i,j,k,TR+itr)/state(i,j,k,DN)
                  END DO
                  state(i,j,k,DN) = rho_J
                  DO itr=0,NTRACER-1
                     state(i,j,k,TR+itr) = state(i,j,k,TR+itr)*state(i,j,k,DN)
                  END DO

c...  Set sink cell velocity equal to the velocity of the gas in the cell,
c...  and remove an equal amount of momentum from the cell.
                  mom(nsink*3+1) = state(i,j,k,MX)*(1.0-rho_J/den)*vol
                  mom(nsink*3+2) = state(i,j,k,MY)*(1.0-rho_J/den)*vol
                  mom(nsink*3+3) = state(i,j,k,MZ)*(1.0-rho_J/den)*vol
                  state(i,j,k,MX) = state(i,j,k,MX)*rho_J/den
                  state(i,j,k,MY) = state(i,j,k,MY)*rho_J/den
                  state(i,j,k,MZ) = state(i,j,k,MZ)*rho_J/den
                                    
c...  Remove the appropriate amount of thermal energy from the cell.
                  IF(.NOT. EN<=0) THEN
                     state(i,j,k,EN) = especific*state(i,j,k,DN) + 
     .                    0.5d0*(state(i,j,k,BX)**2+state(i,j,k,BY)**2+state(i,j,k,BZ)**2)
                  END IF

                  ang_mom(nsink*3+1) = 0.d0
                  ang_mom(nsink*3+2) = 0.d0
                  ang_mom(nsink*3+3) = 0.d0

c...  Increment the number of sink particles
                  nsink = nsink + 1

               endif
            enddo
         enddo
      enddo

#undef PI
#undef JEANSNO
      end

      
      subroutine FORT_SINK_ACCRETE_EXTENDED(sloi1, sloi2, sloi3,
     .     shii1, shii2, shii3, nstate, state, gsloi1, gsloi2, gsloi3,
     .     gshii1, gshii2, gshii3, growstate, ngrow, domlo,
     .     dx, pos, mom, angmom, mass, dt, r_angmom)
      implicit none

      integer sloi1, sloi2, sloi3, shii1, shii2, shii3, nstate
      integer gsloi1, gsloi2, gsloi3, gshii1, gshii2, gshii3, ngrow
      REAL_T state(sloi1:shii1, sloi2:shii2, sloi3:shii3, 0:nstate-1)
      REAL_T growstate(gsloi1:gshii1, gsloi2:gshii2, gsloi3:gshii3, 0:nstate-1)
      REAL_T domlo(SDIM), dx(SDIM)
      REAL_T pos(SDIM), mom(SDIM), angmom(SDIM), mass, dt, r_angmom
      include "commons_h.F"

#define PI 3.1415926535897931d0
c...  ACCRETERADMAX = effective size of a sink particle in units of dx
#define ACCRETERADMAX 2.d0
c...  ACCRETERADMIN = effective size of a sink particle in units of dx
#define ACCRETERADMIN 1.d0
#define MAXACCFAC     1.0d0

      REAL_T vgas(3), bgas(3), vsink(3), mdotB, mdot, mdot_par, mdot_perp, dm
      REAL_T bondi_alpha, dxmean, vol
      integer i, j, k, m, isub, jsub, ksub, idx(SDIM)
      REAL_T xdist, ydist, zdist, dist
      REAL_T maccrete(-ngrow:ngrow, -ngrow:ngrow, -ngrow:ngrow)
      REAL_T eaccrete(-ngrow:ngrow, -ngrow:ngrow, -ngrow:ngrow)
      REAL_T paccrete(-ngrow:ngrow, -ngrow:ngrow, -ngrow:ngrow, 3)
      REAL_T ptot(-ngrow:ngrow, -ngrow:ngrow, -ngrow:ngrow, 3)
      REAL_T cm(3), jsp(3), jspsqr, esp, rmin, dxmin
      REAL_T rhocell, reff(3), prad(3), rdotp
      REAL_T pradnew(3), pnew(3), kenew, rsqr
      REAL_T ptrans(3), ptransnew(3), ptransmag
      REAL_T r_AB, r_BH, r_ABH, rho_mean, rho0, rho_min, rho_inf
      REAL_T p_mean, c_mean, alpha_scale, rkernel
      REAL_T wgt(-ngrow:ngrow, -ngrow:ngrow, -ngrow:ngrow), wgttot
      REAL_T v_mean(3), b_mean(3), b2_mean, bmag, vmag
      REAL_T vff, vamax, cmax, temp
      REAL_T deltam, deltap(3), deltal(3)
      REAL_T Mach, MachBH, MachLast, MachBar, MachAB, MachABH
      REAL_T beta, cos_bvangle, beta0, beta_last, rho_acc, eb, evb
      REAL_T alpha_pow, f_bsgeom, sinTheta, stmin, stmax, bperp, bpar
      integer itr, count

c...  Find the cell in which this particle resides
      idx(1) = floor((pos(1) - domlo(1)) / dx(1))
      idx(2) = floor((pos(2) - domlo(2)) / dx(2))
      idx(3) = floor((pos(3) - domlo(3)) / dx(3))

c...  Compute the sink particle velocity
      do i=1,3
         vsink(i) = mom(i)/mass
      enddo

c...  Set the quadrature mean of the cell spacings
      dxmean = sqrt((dx(1)**2 + dx(2)**2 + dx(3)**2)/3.0d0)
      vol = dx(1)*dx(2)*dx(3)
      
c...  Compute state properties in a locally, outside the accretion region
      count = 0
      v_mean(1) = 0.d0
      v_mean(2) = 0.d0
      v_mean(3) = 0.d0
      b_mean(1) = 0.d0
      b_mean(2) = 0.d0
      b_mean(3) = 0.d0
      b2_mean = 0.d0
      p_mean = 0.
      vamax = 0.d0
      cmax = 0.d0
      rho_mean = 0.d0
      rho_min = 1.d300
      do k=-ngrow,ngrow
         do j=-ngrow,ngrow
            do i=-ngrow,ngrow
               xdist = NINT(2.d0*((pos(1) - domlo(1)) / dx(1) -
     .              (idx(1) + i + 0.5d0)))*0.5d0
               ydist = NINT(2.d0*((pos(2) - domlo(2)) / dx(2) -
     .              (idx(2) + j + 0.5d0)))*0.5d0
               zdist = NINT(2.d0*((pos(3) - domlo(3)) / dx(3) -
     .              (idx(3) + k + 0.5d0)))*0.5d0

               if(xdist**2+ydist**2+zdist**2 .le. (ngrow)**2 .AND. 
     .             xdist**2+ydist**2+zdist**2-EPSILON(1.d0) .gt. (4)**2) then
c...  Maximum alfven speed
                  vamax = MAX((growstate(idx(1)+i,idx(2)+j,idx(3)+k,BX)**2 +
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,BY)**2 +
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,BZ)**2) /
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN), vamax)
               end if

               if(xdist**2+ydist**2+zdist**2 .le. (ngrow)**2) then
                  rho_min = MIN(rho_min, growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN))
               end if

               if (.not. (xdist**2+ydist**2+zdist**2 .le. (ngrow)**2 .AND. 
     .             xdist**2+ydist**2+zdist**2-EPSILON(1.d0) .gt. (ngrow-2)**2 )) cycle
c... Gas speed
               do m=1,3
                  v_mean(m) = v_mean(m) + growstate(idx(1)+i,idx(2)+j,idx(3)+k,MX+m-1)
               enddo

c... Magnetic field
               do m=1,3
                  b_mean(m) = b_mean(m) + 
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,BX+m-1)
               enddo
               b2_mean = b2_mean + 
     .              growstate(idx(1)+i,idx(2)+j,idx(3)+k,BX)**2 + 
     .              growstate(idx(1)+i,idx(2)+j,idx(3)+k,BY)**2 + 
     .              growstate(idx(1)+i,idx(2)+j,idx(3)+k,BZ)**2

c... Pressure
               IF(EN<=0) THEN
                  temp = growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN) * C_ISO**2
                  cmax = C_ISO
               ELSE
                  temp = 
     .                 ((GMM-1.d0)*growstate(idx(1)+i,idx(2)+j,idx(3)+k,EN) - 
     .                 0.5d0*(growstate(idx(1)+i,idx(2)+j,idx(3)+k,MX)**2+
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,MY)**2+
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,MZ)**2)
     .                 /growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN) -
     .                 0.5d0*(growstate(idx(1)+i,idx(2)+j,idx(3)+k,BX)**2+
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,BY)**2+
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,BZ)**2))
                  temp = MAX(temp, SMALL_PR)
c... Local maximum sound speed                  
                  cmax = MAX(cmax,SQRT(GMM*temp/growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN)))
               END IF
               p_mean = p_mean + temp

c... Density
               rho_mean = rho_mean + growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN)
               count = count + 1
            enddo
         enddo
      enddo

      vamax = SQRT(vamax)
      do i=1,3
         v_mean(i) = v_mean(i) / rho_mean
         b_mean(i) = b_mean(i) / REAL(count,KIND=8)
      enddo
      rho_mean = rho_mean / REAL(count,KIND=8)
      p_mean = p_mean / REAL(count,KIND=8)
      b2_mean = b2_mean / REAL(count,KIND=8)
      IF(EN<=0) THEN
         c_mean = C_ISO
      ELSE
         c_mean = SQRT(GMM*p_mean/rho_mean)
      END IF
      bmag = SQRT(b_mean(1)**2+b_mean(2)**2+b_mean(3)**2)
      vmag = SQRT((v_mean(1)-vsink(1))**2+(v_mean(2)-vsink(2))**2+(v_mean(3)-vsink(3))**2)

c...  Compute the relative velocities of the sink cell and the ambient gas
      Mach = SQRT(v_mean(1)**2+v_mean(2)**2+v_mean(3)**2)/c_mean
      MachBar = Mach
      cos_bvangle = (b_mean(1)*(v_mean(1)-vsink(1))+b_mean(2)*(v_mean(2)
     .     -vsink(2))+b_mean(3)*(v_mean(3)-vsink(3)))/MAX(bmag*vmag,1.d-100)

      MachBH = (1.d0+Mach**4)**(1.d0/3.d0)/(1.d0+(Mach/LAMBDA)**2)**(1.d0/6.d0)
      r_BH = G_CODE * mass / (MachBH * c_mean)**2

c...  Local magnetic field strength
c      beta = 2.d0*p_mean/MAX(b2_mean,1.d-100)
      beta = 2.d0*rho_mean*c_mean**2/MAX(b2_mean,1.d-100)

c...  Iterate until we find rho_inf and beta_inf
      beta0 = beta
      count = 0
c...  The effective radius of the sink region relative to the analytic bondi solution in dx
      alpha_scale = REAL(ngrow-1, KIND=8)
      DO WHILE(.TRUE.)
c...  Mach numbers
         MachAB = (1.d0+(BETAB/beta)**(NB/2.d0))**(1.d0/NB)
         MachABH = (((1.d0+Mach**4)**(1.d0/3.d0)/(1.d0+(Mach/LAMBDA)**2)**(1.d0/6.d0))**NB + 
     .        (BETAB/beta)**(NB/2.d0))**(1.d0/NB)

c...  compute the accretion scale radius with the local-averaged state properties
         r_ABH = G_CODE * mass / (MachABH * c_mean)**2
         r_AB = G_CODE * mass / (MachAB * c_mean)**2

c...  Assuming the flow locally is represented by a Bondi flow,
c...  compute the effective value of rho_infinity
         alpha_pow = MAX(MIN(0.5d0/beta**0.13d0-0.27d0,1.d0),0.d0)
c         rho_inf = rho_mean / bondi_alpha(alpha_scale*MAX(dxmean/r_BH,0.125d0*r_ABH/r_BH))**(1.d0-alpha_pow)
         rho_inf = rho_mean / bondi_alpha(alpha_scale*MAX(dxmean,MIN(0.125d0*r_BH,0.5d0*r_ABH))/r_BH)**(1.d0-alpha_pow)
         beta_last = beta 
c...  Assume |B| ~ rho during the bondi infall for r > r_alfven/4
c...  Assume |B| ~ B_o due to reconnection if the Alven radius is well resolved.
c...  The following formulae connects the limits
c         beta = beta0 * bondi_alpha(alpha_scale*dxmean/MIN(0.25*r_AB,r_BH))**(1.d0-alpha_pow)
         beta = beta0 * bondi_alpha(alpha_scale*dxmean/r_BH)**(1.d0-alpha_pow)
         count = count + 1
         IF (beta > 1.d9 .OR. ABS(beta-beta_last)/ABS(beta_last+beta) < 1.d-4) EXIT
         IF (count > 100) THEN
            PRINT*, 'Sink properties only converge to',
     .	             ABS(beta-beta_last)/ABS(beta_last+beta),
     .              'in 100 iterations.  Giving up.'
            EXIT
         END IF
      END DO

c      PRINT*, rho_inf, beta, MachBH, cos_bvangle
c      CALL FLUSH(6)

c...  Compute mass accretion rate.
      mdotB = 4. * PI * rho_inf * LAMBDA * (G_CODE * mass)**2 / c_mean**3
c...  Parallel
      mdot_par = mdotB/MachBH**2*(MachBH**NB+(BETAB/beta)**(NB/2.d0))**(-1.d0/NB)
c...  Perpendicular
      MachABH = (((1.d0+Mach**4)**(1.d0/3.d0)/(1.d0+(Mach/LAMBDA)**2)**(1.d0/6.d0))**NB + 
     .     (BETAB/beta)**(NB/2.d0))**(1.d0/NB)
      mdot_perp = mdotB/MachBH*MIN(1.d0/MachBH**2, 1.d0/MachABH)
c...  Angle-interpolated
      mdot = mdot_par*cos_bvangle**2 + mdot_perp*(1.d0-cos_bvangle**2)
      dm = mdot*dt

c KJB      dm = 1.d300

c...  Compute the size of the accretion kernel
      rkernel = r_ABH / dxmean
      if (rkernel .lt. ACCRETERADMIN) then
         rkernel = ACCRETERADMIN
      else if (rkernel .gt. ACCRETERADMAX) then
         rkernel = ACCRETERADMAX
      endif

c...  Compute the distances to all the other cells in the accretion box,
c...  and the weightings that go with them

      wgttot = 0.d0
      do k=-ngrow,ngrow
         do j=-ngrow,ngrow
            do i=-ngrow,ngrow     
c...  Assign weight to cells inside the spherical accretion region
               xdist = NINT(2.d0*((pos(1) - domlo(1)) / dx(1) -
     .              (idx(1) + i + 0.5d0)))*0.5d0
               ydist = NINT(2.d0*((pos(2) - domlo(2)) / dx(2) -
     .              (idx(2) + j + 0.5d0)))*0.5d0
               zdist = NINT(2.d0*((pos(3) - domlo(3)) / dx(3) -
     .              (idx(3) + k + 0.5d0)))*0.5d0
               if (xdist**2+ydist**2+zdist**2 .le. (4)**2) then
                  if (xdist**2+ydist**2+zdist**2 .le. (1)**2) then
                     wgt(i,j,k) = 1.d0
                  else
                     wgt(i,j,k) = exp(-(xdist**2 + ydist**2 + zdist**2 - 1.d0) /
     .                    (rkernel)**2)
                  end if
                  wgttot = wgttot + wgt(i,j,k)
               else
                  wgt(i,j,k) = 0.d0
               endif
            enddo
         enddo
      enddo


c...  Apportion the amount to be accreted among the cells in the
c...  accretion box
      do k=-ngrow,ngrow
         do j=-ngrow,ngrow
            do i=-ngrow,ngrow
               maccrete(i,j,k) = dm * wgt(i,j,k)/wgttot
            enddo
         enddo
      enddo

c...  Correct the accretion rate in any cell that doesn't have enough
c...  material to provide the amount we want to accrete.
      do k=-ngrow,ngrow
         do j=-ngrow,ngrow
            do i=-ngrow,ngrow
               rhocell = growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN)

c...  compute limited mass accretion 
               maccrete(i,j,k) = MIN(maccrete(i,j,k), 
     .                           MAXACCFAC * vol * rhocell,
     .                           (rhocell-MAX(MIN(rho_min,0.1d0*rho_inf),SMALL_DN))*vol)

c...  prevent the alven speed in the accretion region from 
c...  exceeding vamax.
               rho_acc = rhocell
     .                 -(growstate(idx(1)+i,idx(2)+j,idx(3)+k,BX)**2 +
     .                   growstate(idx(1)+i,idx(2)+j,idx(3)+k,BY)**2 +
     .                   growstate(idx(1)+i,idx(2)+j,idx(3)+k,BZ)**2) / 
     .                   MAX(vamax**2, MAX(dxmean/r_ABH, 1.d-3)**2*(vamax**2+cmax**2+MachBH**2), SMALL_PR/rhocell)

               maccrete(i,j,k) = MAX(MIN(maccrete(i,j,k), rho_acc*vol), 0.d0)
            enddo
         enddo
      enddo

c...  Compute the amount of momentum to accrete. 
      do k=-ngrow,ngrow
         do j=-ngrow,ngrow
            do i=-ngrow,ngrow
c...  Convenient shorthand
               rhocell = growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN)

               do m=1,3
                  paccrete(i,j,k,m) =
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,MX+m-1) *
     .                 maccrete(i,j,k) / rhocell
               enddo
               IF(.NOT. EN<=0) THEN
                  eaccrete(i,j,k) =
     .                 (growstate(idx(1)+i,idx(2)+j,idx(3)+k,EN) -
     .                 0.5d0*(growstate(idx(1)+i,idx(2)+j,idx(3)+k,BX)**2+
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,BY)**2+
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,BZ)**2))* 
     .                 maccrete(i,j,k) / rhocell
               END IF

c...  Store the total momentum of the cells in the accretion
c...  region, without regard to the angular momentum barrier.
c...  We do this to track the direction of spin.

               do m=1,3
                  ptot(i,j,k,m) =
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,MX+m-1)
               enddo

            enddo
         enddo
      enddo


      deltam = 0.d0
      do m=1,3
         deltap(m) = 0.d0
         deltal(m) = 0.d0
      enddo

c...  Alter the sink cell mass and momentum only if it is contained
c...  within this grid.
      if ((sloi1 .le. idx(1)) .and. (idx(1) .le. shii1) .and.
     .     (sloi2 .le. idx(2)) .and. (idx(2) .le. shii2) .and.
     .     (sloi3 .le. idx(3)) .and. (idx(3) .le. shii3)) then
         do k=-ngrow,ngrow
            do j=-ngrow,ngrow
               do i=-ngrow,ngrow
c...  Add mass
                  deltam = deltam + maccrete(i,j,k)

c...  Add momentum
                  do m=1,3
                     deltap(m) = deltap(m) + paccrete(i,j,k,m)
                  enddo
                  if (r_angmom <= 0.) then
c...  Update spin direction tracking/angular momentum
                     deltal(1) = deltal(1) + dt/vol*
     .                    (j*dx(2)*ptot(i,j,k,3) - k*dx(3)*ptot(i,j,k,2))
                     deltal(2) = deltal(2) + dt/vol*
     .                    (k*dx(3)*ptot(i,j,k,1) - i*dx(1)*ptot(i,j,k,3))
                     deltal(3) = deltal(3) + dt/vol*
     .                    (i*dx(1)*ptot(i,j,k,2) - j*dx(2)*ptot(i,j,k,1))
c...  If we are passed a positive kepler radius, use the new angular momentum treatment
                  else
                     rhocell = growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN)
c...  Note - half integer rounding of distances is for symmetry preservation x,y,zdist is in units of dx!
                     xdist = NINT(2.d0*((idx(1) + i + 0.5d0) -
     .                    ((pos(1) - domlo(1)) / dx(1))))*0.5d0
                     ydist = NINT(2.d0*((idx(2) + j + 0.5d0) -
     .                    ((pos(2) - domlo(2)) / dx(2))))*0.5d0
                     zdist = NINT(2.d0*((idx(3) + k + 0.5d0) -
     .                    ((pos(3) - domlo(3)) / dx(3))))*0.5d0
                     deltal(1) = deltal(1) + maccrete(i,j,k) *
     .                    (ydist*dx(2)*(ptot(i,j,k,3)/rhocell-vsink(3)) - zdist*dx(3)*(ptot(i,j,k,2)/rhocell-vsink(2)))
                     deltal(2) = deltal(2) + maccrete(i,j,k) *
     .                    (zdist*dx(3)*(ptot(i,j,k,1)/rhocell-vsink(1)) - xdist*dx(1)*(ptot(i,j,k,3)/rhocell-vsink(3)))
                     deltal(3) = deltal(3) + maccrete(i,j,k) *
     .                    (xdist*dx(1)*(ptot(i,j,k,2)/rhocell-vsink(2)) - ydist*dx(2)*(ptot(i,j,k,1)/rhocell-vsink(1)))
                  endif
               enddo
            enddo
         enddo
      endif
      mass = mass + deltam
      do m=1,3
         mom(m) = mom(m) + deltap(m)
      enddo
      if (r_angmom <= 0.) then
         do m=1,3
            angmom(m) = angmom(m) + deltal(m)
         enddo
      else
         temp = 0.d0
         do m=1,3
            temp = temp + deltal(m)**2
         end do
         temp = sqrt(temp)
c...     take the smaller of the orbital angular momentum of accreted gas and
c...     what the orbital angular momentum of the accreted gas would be for a circular
c...     orbit at the r_angmom radius.
c		 print*, 'mass of star', mass
         if (deltam*r_angmom * sqrt(G_CODE*mass/r_angmom) < temp) then
            do m=1,3
               angmom(m) = angmom(m) + deltam*r_angmom * sqrt(G_CODE*mass/r_angmom)  * (deltal(m)/temp)
            enddo
         else
            do m=1,3
               angmom(m) = angmom(m) + deltal(m)
            enddo
         end if
      endif

c...  Alter the state data (state, not growstate since growstate is a copy
c...  that will be discarded). Note that state may not include all the cells
c...  that have to be altered, so we have to check for this.
      do k=-ngrow,ngrow
         if ((idx(3)+k .lt. sloi3) .or. (idx(3)+k .gt. shii3)) cycle
         do j=-ngrow,ngrow
            if ((idx(2)+j .lt. sloi2) .or. (idx(2)+j .gt. shii2)) cycle
            do i=-ngrow,ngrow
               if ((idx(1)+i .lt. sloi1) .or. (idx(1)+i .gt. shii1)) cycle

               do m=1,3
                  vgas(m) = growstate(idx(1)+i,idx(2)+j,idx(3)+k,MX+m-1) /
     .                 growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN)
               enddo

c...  Accrete mass
c...  Keep advective tracer fractions constant
               DO itr=0,NTRACER-1
                  state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr) = 
     .                 state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr)/
     .                 state(idx(1)+i,idx(2)+j,idx(3)+k,DN)
               END DO
               state(idx(1)+i,idx(2)+j,idx(3)+k,DN) =
     .              state(idx(1)+i,idx(2)+j,idx(3)+k,DN) -
     .              maccrete(i,j,k)/vol
               DO itr=0,NTRACER-1
                  state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr) = 
     .                 state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr)*
     .                 state(idx(1)+i,idx(2)+j,idx(3)+k,DN)
               END DO

c...  Accrete energy
               IF(.NOT. EN<=0) THEN
                  state(idx(1)+i,idx(2)+j,idx(3)+k,EN) =
     .                 state(idx(1)+i,idx(2)+j,idx(3)+k,EN) -
     .                 eaccrete(i,j,k)/vol
               END IF

c...  Accrete momentum
               do m=1,3
                  state(idx(1)+i,idx(2)+j,idx(3)+k,MX+m-1) =
     .                 state(idx(1)+i,idx(2)+j,idx(3)+k,MX+m-1) -
     .                 paccrete(i,j,k,m)/vol
               enddo

c...  Check for negative density and energy. This can happen due to numerical
c...  precision effects even though we have limited the mass transferred.
               if (state(idx(1)+i,idx(2)+j,idx(3)+k,DN)
     .              .lt. SMALL_DN) then
c...  Keep advective tracer fractions constant
                  DO itr=0,NTRACER-1
                     state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr) = 
     .                    state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr)/
     .                    state(idx(1)+i,idx(2)+j,idx(3)+k,DN)
                  END DO
                  state(idx(1)+i,idx(2)+j,idx(3)+k,DN) = SMALL_DN
                  DO itr=0,NTRACER-1
                     state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr) = 
     .                    state(idx(1)+i,idx(2)+j,idx(3)+k,TR+itr)*
     .                    state(idx(1)+i,idx(2)+j,idx(3)+k,DN)
                  END DO
                  do m=1,3
                     state(idx(1)+i,idx(2)+j,idx(3)+k,MX+m-1) = 
     .                    SMALL_DN * vgas(m)
                  enddo
               endif
               IF(.NOT. EN<=0) THEN
                  evb = 0.5d0*(state(idx(1)+i,idx(2)+j,idx(3)+k,MX)**2 + 
     .                         state(idx(1)+i,idx(2)+j,idx(3)+k,MY)**2 + 
     .                         state(idx(1)+i,idx(2)+j,idx(3)+k,MZ)**2) /
     .                 state(idx(1)+i,idx(2)+j,idx(3)+k,DN) +
     .                 0.5d0*(state(idx(1)+i,idx(2)+j,idx(3)+k,BX)**2 + 
     .                        state(idx(1)+i,idx(2)+j,idx(3)+k,BY)**2 + 
     .                        state(idx(1)+i,idx(2)+j,idx(3)+k,BZ)**2)
                  if (state(idx(1)+i,idx(2)+j,idx(3)+k,EN) - evb
     .                 .lt. SMALL_PR/(GMM-1.d0)) then
                     state(idx(1)+i,idx(2)+j,idx(3)+k,EN) = SMALL_PR/(GMM-1.d0) + evb
                  endif
               END IF
            enddo
         enddo
      enddo

      return
      end

#undef PI
#undef MAXACCFAC

      subroutine FORT_GRAV_GAS_PARTICLE(rholo1, rholo2, rholo3,
     .     rhohi1, rhohi2, rhohi3, rho, dx, domlo, domhi, period,
     .     npart, pos, mom, mass, dt, soften)

      implicit none

      integer rholo1, rholo2, rholo3, rhohi1, rhohi2, rhohi3
      REAL_T rho(rholo1:rhohi1, rholo2:rhohi2, rholo3:rhohi3)
      integer slo1, slo2, slo3, shi1, shi2, shi3, nstate
      REAL_T dx(3), domlo(3), domhi(3)
      integer npart, period(3)
      REAL_T pos(CH_SPACEDIM*npart), mom(CH_SPACEDIM*npart), mass(npart)
      REAL_T dt, soften

      integer n, i, j, k, isub, jsub, ksub
      integer idx(3)
      REAL_T x, y, z, rx, ry, rz, r, rnosoft, dxmax, cellvol, domsize(3)
      REAL_T xsub, rxsub, ysub, rysub, zsub, rzsub

      include "commons_h.F"
#define NDIV 8

c...  Get maximum gridspacing, which we use for the softening length, and
c...  the volume of a cell
      dxmax=dx(1)
      cellvol=dx(1)
      do i=2,3
         if (dxmax .lt. dx(i)) then
            dxmax = dx(i)
         endif
         cellvol = cellvol*dx(i)
      enddo

c...  Find the domain size
      do n=1,CH_SPACEDIM
         domsize(n) = (domhi(n)-domlo(n))
      enddo

c...  Loop over particles
      do n=1, npart
c...  Find the particle location in the grid
         do i=1, 3
            idx(i) = floor((pos(3*(n-1)+i) - domlo(i)) / dx(i))
         enddo

         do k=rholo3, rhohi3
            z = NINT(2.d0*(domlo(3)/dx(3) + (k+0.5d0)))*0.5d0*dx(3)
            rz = pos(3*n) - z
c...  Wrap if periodic
            if (period(3) .eq. 1) then
               if (rz .gt. domsize(3)/2.d0) rz = rz - domsize(3)
               if (rz .lt. -domsize(3)/2.d0) rz = rz + domsize(3)
            endif
            do j=rholo2, rhohi2
               y = NINT(2.d0*(domlo(2)/dx(2) + (j+0.5d0)))*0.5d0*dx(2)
               ry = pos(3*n-1) - y
               if (period(2) .eq. 1) then
                  if (ry .gt. domsize(2)/2.d0) ry = ry - domsize(2)
                  if (ry .lt. -domsize(2)/2.d0) ry = ry + domsize(2)
               endif
               do i=rholo1, rhohi1
                  x = NINT(2.d0*(domlo(1)/dx(1) + (i+0.5d0)))*0.5d0*dx(1)
                  rx = pos(3*n-2) - x
                  if (period(1) .eq. 1) then
                     if (rx .gt. domsize(1)/2.d0) rx = rx - domsize(1)
                     if (rx .lt. -domsize(1)/2.d0) rx = rx + domsize(1)
                  endif
                  
                  if (
     .                 (k .gt. idx(3)+1) .or. 
     .                 (k .lt. idx(3)-1) .or. 
     .                 (j .gt. idx(2)+1) .or. 
     .                 (j .lt. idx(2)-1) .or. 
     .                 (i .gt. idx(1)+1) .or. 
     .                 (i .lt. idx(1)-1)
     .                 ) then
                     
c...  Compute using Plummer force law in cells other than the
c...  host cell and its neigbhors
                     
                     r = sqrt(rx**2+ry**2+rz**2+(soften*dxmax)**2)
                     rnosoft = sqrt(rx**2+ry**2+rz**2)
                     if (rnosoft <= 1.d-5*dxmax) then
                        rx=0.d0
                        ry=0.d0
                        rz=0.d0
                        rnosoft = 1.0d0
                     endif
                     
c...  Update the particle momentum
                     mom(3*n-2) = mom(3*n-2) - dt *
     .                    G_CODE * rho(i,j,k) * cellvol * mass(n) / 
     .                    (r**2) * rx/rnosoft
                     mom(3*n-1) = mom(3*n-1) - dt *
     .                    G_CODE * rho(i,j,k) * cellvol * mass(n) /
     .                    (r**2) * ry/rnosoft
                     mom(3*n) = mom(3*n) - dt *
     .                    G_CODE * rho(i,j,k) * cellvol * mass(n) /
     .                    (r**2) * rz/rnosoft

                  else
                     
c...  In the host cell and its neighbors, compute the potential
c...  gradient more accurately by averaging the potential gradient
c...  at NDIV^3 points distributed evenly throughout the cell.

                     do ksub = 0,NDIV-1
                        zsub = z - 0.5d0*dx(3) + (ksub+0.5d0)*dx(3)/NDIV
                        rzsub = pos(3*n) - zsub
c...  Wrap if periodic
                        if (period(3) .eq. 1) then
                           if (rzsub .gt. domsize(3)/2.d0)
     .                          rzsub = rzsub - domsize(3)
                           if (rzsub .lt. -domsize(3)/2.d0)
     .                          rzsub = rzsub + domsize(3)
                        endif
                        do jsub = 0,NDIV-1
                           ysub = y - 0.5d0*dx(2) + (jsub+0.5d0)*dx(2)/NDIV
                           rysub = pos(3*n-1) - ysub
                           if (period(2) .eq. 1) then
                              if (rysub .gt. domsize(2)/2.d0)
     .                             rysub = rysub - domsize(2)
                              if (rysub .lt. -domsize(2)/2.d0)
     .                             rysub = rysub + domsize(2)
                           endif
                           do isub = 0,NDIV-1
                              xsub = x - 0.5d0*dx(1) + (isub+0.5d0)*dx(1)/NDIV
                              rxsub = pos(3*n-2) - xsub
                              if (period(1) .eq. 1) then
                                 if (rxsub .gt. domsize(1)/2.d0)
     .                                rxsub = rxsub - domsize(1)
                                 if (rxsub .lt. -domsize(1)/2.d0)
     .                                rxsub = rxsub + domsize(1)
                              endif

                              r = sqrt(rxsub**2+rysub**2+rzsub**2+
     .                             (soften*dxmax)**2)
                              rnosoft = sqrt(rxsub**2+rysub**2+rzsub**2)
                              if (rnosoft <= 1.d-5*dxmax) then
                                 rx=0.d0
                                 ry=0.d0
                                 rz=0.d0
                                 rnosoft = 1.0d0
                              endif

c...  Update the particle momentum
                              mom(3*n-2) = mom(3*n-2) - dt *
     .                             G_CODE * rho(i,j,k) *
     .                             (cellvol/NDIV**3) * mass(n)
     .                             / (r**2) * rxsub/rnosoft
                              mom(3*n-1) = mom(3*n-1) - dt *
     .                             G_CODE * rho(i,j,k) *
     .                             (cellvol/NDIV**3) * mass(n)
     .                             / (r**2) * rysub/rnosoft
                              mom(3*n) = mom(3*n) - dt *
     .                             G_CODE * rho(i,j,k) *
     .                             (cellvol/NDIV**3) * mass(n)
     .                             / (r**2) * rzsub/rnosoft

                           enddo
                        enddo
                     enddo

                  endif
                  
               enddo
            enddo
         enddo
      enddo

      return
      end


c...  compute acceleration of gas due to sink particle gravity
      subroutine FORT_SINK_ACCEL(rholo1, rholo2, rholo3,
     .     rhohi1, rhohi2, rhohi3, accel,
     .     dx, domlo, domhi, period,
     .     npart, pos, mass, soften)

      implicit none

      integer rholo1, rholo2, rholo3, rhohi1, rhohi2, rhohi3
      REAL_T ACCEL(rholo1:rhohi1, rholo2:rhohi2, rholo3:rhohi3,CH_SPACEDIM)
      REAL_T dx(3), domlo(3), domhi(3)
      integer npart, period(3)
      REAL_T pos(CH_SPACEDIM*npart), mom(CH_SPACEDIM*npart), mass(npart)
      REAL_T soften

      integer n, i, j, k, m, isub, jsub, ksub
      integer idx(3)
      REAL_T x, y, z, rx, ry, rz, r, rnosoft, dxmax, cellvol, domsize(3)
      REAL_T xsub, rxsub, ysub, rysub, zsub, rzsub

      include "commons_h.F"

c...  Get maximum gridspacing, which we use for the softening length, and
c...  the volume of a cell
      dxmax=dx(1)
      cellvol=dx(1)
      do i=2,3
         if (dxmax .lt. dx(i)) then
            dxmax = dx(i)
         endif
         cellvol = cellvol*dx(i)
      enddo

c...  Find the domain size
      do n=1,CH_SPACEDIM
         domsize(n) = (domhi(n)-domlo(n))
      enddo

c...  Loop over particles
      do n=1, npart

c...  Find the particle location in the grid
         do i=1, 3
            idx(i) = floor((pos(3*(n-1)+i) - domlo(i)) / dx(i))
         enddo

         do k=rholo3, rhohi3
            z = domlo(3) + dx(3) * (k+0.5d0)
            rz = pos(3*n) - z
c...  Wrap if periodic
            if (period(3) .eq. 1) then
               if (rz .gt. domsize(3)/2.d0) rz = rz - domsize(3)
               if (rz .lt. -domsize(3)/2.d0) rz = rz + domsize(3)
            endif
            do j=rholo2, rhohi2
               y = domlo(2) + dx(2) * (j+0.5d0)
               ry = pos(3*n-1) - y
               if (period(2) .eq. 1) then
                  if (ry .gt. domsize(2)/2.d0) ry = ry - domsize(2)
                  if (ry .lt. -domsize(2)/2.d0) ry = ry + domsize(2)
               endif
               do i=rholo1, rhohi1
                  x = domlo(1) + dx(1) * (i+0.5d0)
                  rx = pos(3*n-2) - x
                  if (period(1) .eq. 1) then
                     if (rx .gt. domsize(1)/2.d0) rx = rx - domsize(1)
                     if (rx .lt. -domsize(1)/2.d0) rx = rx + domsize(1)
                  endif

c...  Compute using Plummer force law in cells other than the
c...  host cell and its neigbhors

                  r = sqrt(rx**2+ry**2+rz**2+(soften*dxmax)**2)
                  rnosoft = sqrt(rx**2+ry**2+rz**2)
                  if (rnosoft <= 1.d-5*dxmax) then
                     rx=0.d0
                     ry=0.d0
                     rz=0.d0
                     rnosoft = 1.0d0
                  endif
                  
c...  compute acceleration
c..   NOTE {rx,ry,rz} points from cell to particle, so sign is positive
                  ACCEL(i,j,k,1) = ACCEL(i,j,k,1) +
     .                 G_CODE * mass(n) / 
     .                 (r**2) * rx/rnosoft
                  ACCEL(i,j,k,2) = ACCEL(i,j,k,2) +
     .                 G_CODE * mass(n) / 
     .                 (r**2) * ry/rnosoft
                  ACCEL(i,j,k,3) = ACCEL(i,j,k,3) +
     .                 G_CODE * mass(n) / 
     .                 (r**2) * rz/rnosoft
               enddo
            enddo
         enddo
      enddo

      return
      end


c     Routine to return alpha, defined as rho/rho_inf, for a critical
c     Bondi accretion solution. The argument is x = r / r_Bondi.

      REAL_T function bondi_alpha(x)

      implicit none
      REAL_T x

#define XMIN 0.01d0
#define XMAX 2.d0
#define NTABLE 51

      REAL_T alphatable(NTABLE)
      REAL_T xtable, xtablep1, alpha_exp
      integer idx

c     Table of alpha values. These correspond to x values that run from
c     0.01 to 2.0 with uniform logarithmic spacing. The reason for
c     this choice of range is that the asymptotic expressions are
c     accurate to better than 2% outside this range.
      alphatable = (/
     .     820.254, 701.882, 600.752, 514.341, 440.497, 377.381, 323.427, 
     .     277.295, 237.845, 204.1, 175.23, 150.524, 129.377, 111.27, 95.7613,
     .     82.4745, 71.0869, 61.3237, 52.9498, 45.7644, 39.5963, 34.2989, 
     .     29.7471, 25.8338, 22.4676, 19.5705, 17.0755, 14.9254, 13.0714, 
     .     11.4717, 10.0903, 8.89675, 7.86467, 6.97159, 6.19825, 5.52812, 
     .     4.94699, 4.44279, 4.00497, 3.6246, 3.29395, 3.00637, 2.75612, 
     .     2.53827, 2.34854, 2.18322, 2.03912, 1.91344, 1.80378, 1.70804,  
     .     1.62439 /)

c     Deal with the off-the-table cases
      if (x .le. XMIN) then
         bondi_alpha = LAMBDA / sqrt(2.d0 * x**3)
      else if (x .ge. XMAX) then
         bondi_alpha = exp(1.d0/x)
      else
c     We are on the table
         idx = floor ((NTABLE-1) * log(x/XMIN) / log(XMAX/XMIN))
         xtable = exp(log(XMIN) + idx*log(XMAX/XMIN)/(NTABLE-1))
         xtablep1 = exp(log(XMIN) + (idx+1)*log(XMAX/XMIN)/(NTABLE-1))
         alpha_exp = log(x/xtable) / log(xtablep1/xtable)
c     Note the extra +1s below because of stupid fortran 1 offset arrays
         bondi_alpha = alphatable(idx+1) * 
     .        (alphatable(idx+2)/alphatable(idx+1))**alpha_exp
      end if

      return
      end

#undef NTABLE
#undef XMIN
#undef XMAX
