import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import fund_const as fc
    
#main
Nsinks= 64
f=open("init.sink","w")
f.write("%d %d\n" % (Nsinks,Nsinks) )
msink=1.
rBH=msink/5**2
ss=125/4.*rBH
Lbox=5.
#pos=np.array([-0.5,0.5]).astype('float')*Lbox/2
#pos=np.array([-0.67,0.,0.67]).astype('float')*Lbox/2
pos=np.array([-1.5,-0.5,0.5,1.5]).astype('float')*ss
cnt=0
for i in pos:
	for j in pos:
		for k in pos:
			f.write("%1.6f %1.2f %1.2f %1.2f  0. 0. 0.  0. 0. 0. %d\n" % (msink,i,j,k,cnt))
			cnt+=1
f.close()

#store positions in arrays for 3D plot and zoom-in print out
xarr=np.zeros(Nsinks)-1
yarr= xarr.copy()
zarr= xarr.copy()
cnt=0
for i in pos:
    for j in pos:
        for k in pos:
            xarr[cnt]=i
            yarr[cnt]=j
            zarr[cnt]=k
            cnt+=1

### for when finding sonic length, sample 1000 boxes
#f= 0.12
#lbox=2.
#lr=f*lbox/2
#nboxes_per_dir= int(1/f)
#step= f*lbox
#xarr=np.zeros(nboxes_per_dir**3)-1
#yarr= xarr.copy()
#zarr= xarr.copy()
#cnt=0
#for i in range(nboxes_per_dir):
#    for j in range(nboxes_per_dir):
#        for k in range(nboxes_per_dir):
#			xarr[cnt]=lr+i*step  -lbox/2 +lr #periodic box so subtract Lbox/2
#			yarr[cnt]=lr+j*step  -lbox/2 +lr
#			zarr[cnt]=lr+k*step  -lbox/2 +lr
#			center=np.array([lr+i*step,lr+j*step,lr+k*step]) #varies
#			left_edge= center-lr #same every time
#			right_edge= center+lr
#			cnt+=1


#PS zoom-in refinement regions for every sink
#for marks 2006 problem in these units preventing refinement
#outside level 2 worked for at least one sink. this gives
#sink location +- 0.02
f=open("zoom-in-refinement.sink","w")
pm= 0.15
for i,x,y,z in zip(range(Nsinks),xarr,yarr,zarr):
    f.write("zoxbeg[%d]= %f;\nzoxend[%d]= %f;\n" \
            % (i,x-pm,i,x+pm))
    f.write("zoybeg[%d]= %f;\nzoyend[%d]= %f;\n" \
            % (i,y-pm,i,y+pm))
    f.write("zozbeg[%d]= %f;\nzozend[%d]= %f;\n" \
            % (i,z-pm,i,z+pm))
f.close()

#3d plot to double check
def scat3D(x,y,z,Lbox):
    '''3D plot to sanity check pos of sinks'''
    fig,ax = plt.subplots()
    ax = Axes3D(fig)
    ax.scatter(x,y,z)
    ax.set_xlim(-Lbox/2,Lbox/2)
    ax.set_ylim(-Lbox/2,Lbox/2)
    ax.set_zlim(-Lbox/2,Lbox/2)
    plt.show()
Lbox=5.
scat3D(xarr,yarr,zarr,Lbox)
