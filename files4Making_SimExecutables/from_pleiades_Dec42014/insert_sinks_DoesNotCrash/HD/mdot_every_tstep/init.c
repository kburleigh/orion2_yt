#include "pluto.h"

/*my funcs*/
real relDiff(real a, real b)
{
	return abs( (a-b) /b);
}

/****/

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 * 
 * NAME 
 * 
 *   INIT
 *
 *
 * PURPOSE
 *
 *   Set inital condition. 
 *
 *
 * ARGUMENTS 
 *
 *   us (OUT)           a pointer to a array_1D of primitive 
 *                      (when *prim_var = 1) or conservative
 *                      (when *prim_var = 0) variables;
 *
 *   x1, x2, x3 (IN)    the three coordinates; the meaning is different
 *                      depending on the geometry:
 *
 *                       x1      x2      x3
 *                       -------------------                
 *                        x       y       z     in cartesian geometry
 *                        r       z       -     in cylindrical geometry 
 *                        r      phi      z     in polar geometry 
 *                        r     theta    phi    in spherical geometry
 *                  
 *   i , j , k  (IN)    the integer indexes of the cell containing the 
 *                      point x1, x2, x3.
 *                
 *   *prim_var (OUT)    an integer flag. When *prim_var = 1 initial 
 *                      conditions are assigned in terms of 
 *                      primitive values. When *prim_var = 0 initial
 *                      conditions are given in terms of conservative 
 *                      value.
 *
 *
 * Variable names are accessed as us[nv], where
 *
 *   nv = DN is density
 *   nv = PR is pressure
 *  
 *   Vector components are labelled always as VX,VY,VZ, but alternative
 *   labels may be used:
 *
 *   Cartesian       VX     VY      VZ
 *   Cylindrical    iVR    iVZ     iVPHI
 *   Polar          iVR    iVPHI   iVZ
 *   Spherical      iVR    iVTH    iVPHI
 *
 * 
 *
 **************************************************************** */
{

  real gamma,cs,rho0;
  real my_ceil_va, my_small_dn, my_small_pr;
	
	*prim_var = 1;

	gamma = aux[GAMMA];
     cs = aux[CS];
	rho0 = aux[RHO0];

	my_small_dn = aux[MY_SMALL_DN];
     my_small_pr = aux[MY_SMALL_PR];


  us[DN] = rho0;
 
  #if EOS == IDEAL
  gmm = gamma;
  us[PR] = rho0 * cs*cs / gmm;
  #else
  C_ISO = cs;
  #endif

  us[VX] = 0.;
  us[VY] = 0.;
  us[VZ] = 0.;

#if PHYSICS == MHD
   us[BX] = 0.;
   us[BY] = 0.;
   us[BZ] = 0.; 
#endif

	SMALL_DN = my_small_dn; 
	SMALL_PR = my_small_pr;
}

/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   ANALYSIS
 *
 *
 * PURPOSE
 *  
 *   Perform some pre-processing data
 *
 * 
 * ARGUMENTS
 *
 *   uu(IN)     solution data.
 *
 *   GG(IN)     pointer to array of GRID structures  
 *
 **************************************************************** */
{
 
}



#if PHYSICS == MHD
/* ************************************************************** */
void BACKGROUND_FIELD (real x1, real x2, real x3, real *B0)
/* 
 *
 *
 * NAME
 * 
 *   BACKGROUND_FIELD
 *
 *
 * PURPOSE
 *
 *   Define the component of a static, curl-free background 
 *   magnetic field.
 *
 *
 * ARGUMENTS
 *
 *   x1, x2, x3  (IN)    coordinates
 *
 *   B0         (OUT)    array_1D component of the background field.
 *
 *
 **************************************************************** */
{
   B0[0] = 0.0;
   B0[1] = 0.0;
   B0[2] = 0.0;
}
#endif


/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid)
/* 
 *
 * 
 **************************************************************** */
{
}


#if ADD_INTERNAL_BOUNDARY == YES
/* ************************************************************** */

void INTERNAL_BOUNDARY(real ***uu[], real ***uu_old[],
                       Grid *grid,
                       int i0, int i1, int j0, int j1, int k0, int k1)

/* 
 *
 *
 * NAME
 *
 *   INTERNAL_BOUNDARY
 *
 *
 * PURPOSE
 *
 *   Allow the user to control 
 *
 *
 * ARGUMENTS
 *
 *   uu      (IN/OUT)    three-dimensional array_1D containing the solution data;
 *
 *   uu_old  (IN/OUT)    old, kept for backward compatibility;
 *
 *   grid    (IN)        pointer to grid structures;
 * 
 *   i0, j0, k0 (IN)     indexes of the lower-coordinate point inside
 *                       the domain; 
 *
 *   i1, j1, k1, (IN)    indexes of the upper-coordinate point inside
 *                       the domain.
 *   
 *  
 *
 *
 **************************************************************** */
{
  int  i, j, k;
  real x1, x2, x3;

  for (k = k0; k <= k1; k++) { x3 = grid[KDIR].x[k];
  for (j = j0; j <= j1; j++) { x2 = grid[JDIR].x[j];
  for (i = i0; i <= i1; i++) { x1 = grid[IDIR].x[i];

  }}}

}

#endif
