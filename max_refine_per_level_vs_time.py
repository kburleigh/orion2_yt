from yt.mods import *
import numpy as np
import numpy.random
import numpy.ma as ma
import pylab as py
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import glob

import deriv_fields as df

#classes to store data want
class CInfo(object):
    def printMe(self):
        import pprint
        pprint.pprint(self.__dict__)

class MyData(CInfo):
    def __init__(self):
        self.t = [] #time
        self.L0 = Levels()
        self.L1 = Levels()
        self.L2 = Levels()
    def convert(self):
        self.t = np.array(self.t)        

class Levels(CInfo):
    def __init__(self):
        self.dpMax = [] #[0] is dpMax of any x,y,z direction of data cube
        self.dvMax = []
        self.dpMaxTime = [] #[0] will be list of 3 numbers, 1st number is dpMax in x direction, 3rd is that in z dir
        self.dvMaxTime = []
    def convert(self):
        self.dpMax = np.array(self.dpMax)
        self.dvMax = np.array(self.dvMax)

#funcs do calculations want
def do_xdiff(data):
    l=data[0:-1,:,:]
    r=data[1:,:,:]
    delta = l-r
    avg = (np.abs(l) + np.abs(r) )/2.
    norm = delta / avg
    return norm.max()

def do_ydiff(data):
    l=data[:,0:-1,:]
    r=data[:,1:,:]
    delta = l-r
    avg = (np.abs(l) + np.abs(r) )/2.
    norm = delta / avg
    return norm.max()

def do_zdiff(data):
    l=data[:,:,0:-1]
    r=data[:,:,1:]
    delta = l-r
    avg = (np.abs(l) + np.abs(r) )/2.
    norm = delta / avg
    return norm.max()

#most IMPORTANT calculator
def calc(cube,Levels):
    '''cube: variable returned by pf.h.covering_grid
    Levels: object of class Leveles'''
    d = cube["density"]
    vx = cube["x-vel"]
    vy = cube["y-vel"]
    vz = cube["z-vel"]
    
    dp = np.zeros(3)
    dv = np.zeros(3)
    dp[0] = do_xdiff(d)
    dp[1] = do_ydiff(d)
    dp[2] = do_zdiff(d)
    dv[0] = do_xdiff(vx)
    dv[1] = do_ydiff(vy)
    dv[2] = do_zdiff(vz)
    
    Levels.dpMaxTime.append( dp )
    Levels.dpMax.append( dp.max() )
    Levels.dvMaxTime.append( dv )
    Levels.dvMax.append( dv.max() )


dat = MyData()
path = "/clusterfs/henyey/kaylanb/Test_Prob/Sink/Lee14/quick/HD/2_MoreLv0/BALANCED/wTurb/wRefine_1a/"
patt = "data.000*.3d.hdf5"
files = glob.glob1(path, patt)
for f in files:
	print "READING from file,path: %s, %s" % (f,path)
	pf = load(path+f)
	dat.t.append( pf.current_time )
     #1 calc call per AMR level
	print "Level 0 Data"
	cube= pf.h.covering_grid(level=0,left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions* 2**0) 
	calc(cube,dat.L0)
	print "Level 1 Data"
	cube= pf.h.covering_grid(level=1,left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions* 2**1)  
	calc(cube,dat.L1)
	print "Level 2 Data"	
	cube= pf.h.covering_grid(level=2,left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions* 2**2) 
	calc(cube,dat.L2)
    
dat.L0.convert()
dat.L1.convert()
dat.L2.convert()
dat.convert()

dat.t, dat.L0.printMe(), dat.L1.printMe(), dat.L2.printMe()


#funcs
def getData(lev, pf, ic, level):
    '''
    lev --  TimeProfQs() object that append data to
    level -- integer, AMR grid
    pf -- returned by load("data.hdf5")
    ic -- returned by mic.calcIC(params)
    '''
    #easy data
    lev.t.append( pf.current_time )
    #med data
    fields=["density","rms-vel"]
    dims= list(pf.domain_dimensions)
    cube= pf.h.covering_grid(level=level,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    rho = cube["density"]
    rmsV = cube["rms-vel"]
    lev.maxRho.append( rho.max() )
    lev.minRho.append( rho.min() )
    lev.avgRho.append( rho.mean() )
    lev.rmsV.append( rmsV.mean() )
    #harder data
    m_rho = ma.masked_where(rho <= ic.rho0, rho) 
    if pf.basename == "data.0000.3d.hdf5":
        lev.avgRho_HiPa.append( rho.mean() )
    else:
        lev.avgRho_HiPa.append( m_rho.mean() )   
    

def plot_max_avg_Rho_lev0(lev0, ic, spath):
    '''lev -- TimeProfQs() object, whose lev.convert() attribute has been called'''
    #avgRho
    Tratio = lev0.t / ic.tCr
    fig, ax = plt.subplots()
    plt.plot(Tratio,lev0.maxRho,ls="-",c="black",lw=2.,label="max")
    plt.plot(Tratio,lev0.minRho,ls="-",c="green",lw=2.,label="min")
    plt.plot(Tratio,lev0.avgRho,ls="-",c="blue",lw=2.,label="avg")
    plt.plot(Tratio,lev0.avgRho_HiPa,ls="-",c="red",lw=2.,label=r"avg ($\rho > \rho_0$)")
    plt.hlines(ic.rho0, Tratio.min(),Tratio.max(), colors="magenta", linestyles='dashed',label="initial")
    #finish
    plt.yscale("log")
    plt.xlabel("t / t_cross")
    plt.ylabel("Densities [g/cm^3]")
    plt.title(r"Max & Avg Densities, Lev0")
    py.legend(loc=4, fontsize="small")
    name = spath+"max_avg_Rho_Lev0.pdf"
    plt.savefig(name,format="pdf")
    plt.close()

def plot_max_avg_Rho_lev012(lev0,lev1,lev2, ic, spath):
    '''lev -- TimeProfQs() object, whose lev.convert() attribute has been called'''
    #avgRho
    Tratio = lev0.t / ic.tCr
    fig, ax = plt.subplots()
    #initial
    plt.hlines(ic.rho0, Tratio.min(),Tratio.max(), colors="magenta", linestyles='dashed',label="initial")
    #lev0
    plt.plot(Tratio,lev0.maxRho,ls="-",c="black",lw=2.,label="max Lev0")
    plt.plot(Tratio,lev0.minRho,ls="-",c="green",lw=2.,label="min Lev0")
    plt.plot(Tratio,lev0.avgRho,ls="-",c="blue",lw=2.,label="avg Lev0")
    plt.plot(Tratio,lev0.avgRho_HiPa,ls="-",c="red",lw=2.,label=r"avg ($\rho > \rho_0$)")
    #lev1
    plt.plot(Tratio,lev1.maxRho,ls="--",c="black",lw=2.,label="max Lev1")
    plt.plot(Tratio,lev1.minRho,ls="--",c="green",lw=2.,label="min Lev1")
    plt.plot(Tratio,lev1.avgRho,ls="--",c="blue",lw=2.,label="avg Lev1")
    plt.plot(Tratio,lev1.avgRho_HiPa,ls="--",c="red",lw=2.,label=r"avg Lev1 ($\rho > \rho_0$)")
    #lev2
    plt.plot(Tratio,lev2.maxRho,ls=":",c="black",lw=2.,label="max Lev2")
    plt.plot(Tratio,lev2.minRho,ls=":",c="green",lw=2.,label="min Lev2")
    plt.plot(Tratio,lev2.avgRho,ls=":",c="blue",lw=2.,label="avg Lev2")
    plt.plot(Tratio,lev0.avgRho_HiPa,ls=":",c="red",lw=2.,label=r"avg Lev1 ($\rho > \rho_0$)")
    #finish
    plt.yscale("log")
    plt.xlabel("t / t_cross")
    plt.ylabel("Densities [g/cm^3]")
    plt.title(r"Max & Avg Densities, Lev0")
    py.legend(loc=4, fontsize="small")
    name = spath+"max_avg_Rho_Lev012.pdf"
    plt.savefig(name,format="pdf")
    plt.close()
##

def ind_1st_3_maxima(nparr):
    '''nparr is numpy array of times'''
    ind = [0]*3  
    ind[0] =np.where(nparr == nparr.max())[0][0]
    nparr[ ind[0] ] = 0.
    ind[1] =np.where(nparr == nparr.max())[0][0]
    nparr[ ind[1] ] = 0.
    ind[2] =np.where(nparr == nparr.max())[0][0]
    return ind


#levels = 0 #even if AMR simulation, level 0 is identical to unigrid only simulation data...
#path = "/clusterfs/henyey/kaylanb/Test_Prob/Turb/bondiTurb/quick/driving_t0_only/HD/lev0/"
#spath = path+"plots/"
#patt = "data.00*.3d.hdf5"
#
#lev0 = TimeProfQs()
#lev1 = TimeProfQs()
#lev2 = TimeProfQs()
#
#p= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
#ic = mic.calcIC(p)
#ic.prout()
#
#files = glob.glob1(path, patt)
#print "## READING from DIR: %s" % path
## files = files[0:3]
#for f in files:
#    print "#####\n\nloading file: %s \n\n#######\n" % f
#    pf = load(path+ f)
#    getData(lev0, pf, ic, level=0) #data appended onto lev0 for each f
#    if levels > 0:
#        getData(lev1, pf, ic, level=1) 
#        getData(lev2, pf, ic, level=2) 
#
#lev0.convert()
#lev1.convert()
#lev2.convert()
#
#plot_max_avg_Rho_lev0(lev0, ic, spath) #plot lev0 every time, as less cluttered
#if levels > 0:
#    plot_max_avg_Rho_lev012(lev0, lev1, lev2, ic, spath)
## plot_lev0_Tprof_Qs(lev0, ic, spath)
## if levels > 0:
##     plot_lev012_Tprof_Qs(lev0,lev1,lev2, ic, spath)
#
##diagnostic printing
#print "from path: %s" % path
#tCr = lev0.t / ic.tCr
#mach = lev0.rmsV / ic.cs
#cp = np.copy( lev0.maxRho )
#ind_maxRho = ind_1st_3_maxima( cp )
#print "t/t_cross,v/cs at maxRho 1st,2nd,3rd maxima:"
#for ind in ind_maxRho: print "%f, %f" % (tCr[ind], mach[ind])
#
#cp = np.copy( lev0.avgRho_HiPa )
#ind_avgRho_HiPa = ind_1st_3_maxima( cp )
#print "t/t_cross,v/cs at avgRho_HiPa 1st,2nd,3rd maxima"
#for ind in ind_avgRho_HiPa: print "%f, %f" % (tCr[ind], mach[ind])
#
