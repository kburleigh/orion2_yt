import argparse
parser = argparse.ArgumentParser(description="test")
#required
parser.add_argument("-fhdf5",action="store",help='filename or file wildcard or path to filename and wild')
parser.add_argument("-frac",nargs=3,type=float,action="store",help='fraction of box size for sub-box size, 3 values: start,stop,step')
parser.add_argument("-maxBoxes",type=int,action="store",help='max number of sub-boxes to compute rmsMach for')
parser.add_argument("-cs",type=float,action="store",help='adiabatic sound speed in cm/sec')
#optional
parser.add_argument("-outName",action="store",help='append this to stats.txt for unique name')
args = parser.parse_args()

import numpy as np
import yt

#def _kayVx(field, data):
#    return data["X-momentum"]/data["density"]
#def _kayVy(field, data):
#    return data["Y-momentum"]/data["density"]
#def _kayVz(field, data):
#    return data["Z-momentum"]/data["density"]

def restFrame_rmsMach(ytregion,cs):
    '''returns rest fram rms Mach # given ytregion object and the isothermal sound speed'''
    rho= np.array(ytregion["density"])
    vx= np.array(ytregion["X-momentum"])/rho
    vy= np.array(ytregion["Y-momentum"])/rho
    vz= np.array(ytregion["Z-momentum"])/rho
    #shift to rest frame
    vx= vx-vx.mean()
    vy= vy-vy.mean()
    vz= vz-vz.mean()
    #tolerance on whether worked
    if (vx+vy+vz).sum() > 1.e-8: raise ValueError
    #return rest frame rms v for this region
    rmsV2= (vx**2+vy**2+vz**2)
    rmsV2= np.average(rmsV2,weights=rho)
    return np.sqrt(rmsV2)/cs

def boxCenters(frac,lbox):
    '''return x,y,z 1D arrays for center location of boxes, and 
    distance from center to either edge of every box'''
    lr=frac*lbox/2
    nboxes_per_dir= int(1/frac)
    step= frac*lbox
    xarr=np.zeros(nboxes_per_dir**3)-1
    yarr= xarr.copy()
    zarr= xarr.copy()
    cnt=0
    for i in range(nboxes_per_dir):
        for j in range(nboxes_per_dir):
            for k in range(nboxes_per_dir):
                xarr[cnt]= lr+i*step  -lbox/2 #periodic box so subtract Lbox/2
                yarr[cnt]= lr+j*step  -lbox/2
                zarr[cnt]= lr+k*step  -lbox/2  
                cnt+=1
    return (xarr,yarr,zarr,lr)

print "loading data: %s" % args.fhdf5
ds=yt.load(args.fhdf5)
# ds.add_field("kayVx", function=_kayVx, units="cm/s")
# ds.add_field("kayVy", function=_kayVy, units="cm/s")
# ds.add_field("kayVz", function=_kayVz, units="cm/s")
lbox=float(ds.domain_width[0])
frac_arr= np.arange(args.frac[0],args.frac[1],args.frac[2])
print "frac_arr= ",frac_arr
for frac in frac_arr:
    print "frac= %f, getting box centers" % frac
    (xarr,yarr,zarr,edge)= boxCenters(frac,lbox)
    nSample= min(args.maxBoxes,len(xarr))
    rmsMach= np.zeros(nSample)-1
    print "starting loop for rmsMach in each sub-box"
    for i in range(nSample):
        c_arr=np.array([xarr[i],yarr[i],zarr[i]]) 
        l_arr= c_arr-edge #same every time
        r_arr= c_arr+edge
        box= ds.region(c_arr,l_arr,r_arr)
        rmsMach[i]= restFrame_rmsMach(box,args.cs)
    print "writing results from loop to file"
    #write results to file
    if args.outName: outName= "sonicL_"+args.outName+".txt"
    else: outName= "sonicL.txt"
    fin= open(outName,'a')
    fin.write("#filename, frac box, mean rmsMach, std dev rmsMach,# subBoxes, #cells^1/3 per subBox, first/last 5 rmsMach vals\n")
    dx=lbox/ds.domain_dimensions[0]/2**0 #level 0 turbulence
    ncells_per_boxlen= frac*lbox/dx
    fin.write("%s %.3f %.5f %.2f %d %.1f " % (ds.basename,frac,rmsMach.mean(),rmsMach.std(),nSample,ncells_per_boxlen))
    for val in rmsMach[:5]: fin.write("%.2f " % val)
    for val in rmsMach[-5:]: fin.write("%.2f " % val)
    fin.write("\n")
    fin.close()
print "done"


#may be useful: http://yt-project.org/doc/analyzing/filtering.html 
#-->Filtering Fields by Spatial Location: Geometric Objects
