'''zoom in slice and overlay velocity vectors for each sink in x,z dir. 8 per sink, 2 per dir so 16 plots per time step'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-Nsinks",action="store",help='Number of sink particles')
parser.add_argument("-rBH",action="store",help='bondi hoyle radius in cm')
parser.add_argument("-MaxLevel",action="store",help='finest level of refinement, grids will be drawn for this level')
args = parser.parse_args()
if args.Nsinks: Nsinks = int(args.Nsinks)
else: raise ValueError
if args.rBH: rBH = float(args.rBH)
else: raise ValueError
if args.MaxLevel: MaxLevel = int(args.MaxLevel)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

spath="./plots/"
path="./"

f_hdf5= glob.glob(path+"data.004*.3d.hdf5")
f_sink= glob.glob(path+"data.004*.3d.sink")
f_hdf5.sort()
f_sink.sort()
print f_sink,f_hdf5
xyz=np.zeros((Nsinks,3))
for fcnt in range(len(f_hdf5)):
    pf = load(f_hdf5[fcnt])
    for i in range(Nsinks):
        mass,x,y,z,sid= np.loadtxt(f_sink[fcnt],dtype='float',\
                             skiprows=1,usecols=(0,1,2,3,10),unpack=True)
        xyz[:,0]=x
        xyz[:,1]=y
        xyz[:,2]=z
        #x projection
        # slc = SlicePlot(pf,"x","density", center=xyz[i,:],axes_unit="cm",width=(2*rBH,2*rBH))
#         d={"s":100.,"c":"black","alpha":1}
#         slc.annotate_marker(xy[i,:],marker="o",plot_args=d)
#         slc.annotate_velocity()
#         title="rBH = %.2g cm" % rBH
#         slc.annotate_title(title)
#         name= "vis_sink_id%d_dirX_f%s.png" % (int(sid),pf.basename[5:9])
#         slc.save(name=spath+name)
        #z projection	
        slc = SlicePlot(pf,"z","density", center=xyz[i,:],axes_unit="cm",width=(2*rBH,2*rBH))
#         d={"s":100.,"c":"black","alpha":1}
#         slc.annotate_marker(xy[i,:],marker="o",plot_args=d)
        slc.annotate_velocity()
        slc.annotate_grids(min_level=MaxLevel, max_level=MaxLevel)
        title="rBH = %.2g cm" % rBH
        slc.annotate_title(title)
        name= "SinkID_%d_ZSlice_f%s.png" % (int(sid[i]),pf.basename[5:9])
        slc.save(name=spath+name)
        slc.zoom(4)
        name= "SinkID_%d_ZSlice_Zm4x_f%s.png" % (int(sid[i]),pf.basename[5:9])
        slc.save(name=spath+name)

            
