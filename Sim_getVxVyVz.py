import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import pickle

spath="./plots/"
f_hdf5= "data.0000.3d.hdf5"
lev=0

pf= load(f)
cube= pf.h.covering_grid(level=lev,\
                      left_edge=pf.h.domain_left_edge,
                      dims=pf.domain_dimensions)
rho = cube["density"]
vx= cube["X-momentum"]/rho
vy= cube["Y-momentum"]/rho
vz= cube["Z-momentum"]/rho
fsave=spath+"VxVyVz_lev0_%s.pickle" % (pf.basename[5:9])
fout= open(fsave,"w")
pickle.dump((vx,vy,vz),fout)
fout.close()