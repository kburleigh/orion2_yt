%{run: 
1) use "Sim_PrimVars2Matlab.py" to output hdf5 dump rho, vx,vy,vz and ndim=size of base grid (eg. 512 if box is 512^3) to file.mat  
2) run this script reading in those primitive vars 
3) f3 is 3D power spectrum  
%}

load myfile.mat

s=size(v1);

c=complex(v1,0.0);
clear v1
temp=fftn(c);
clear c
fc1=fftshift(temp);
clear temp
c=complex(v2,0.0);
clear v2
temp=fftn(c);
clear c
fc2=fftshift(temp);
clear temp
c=complex(v3,0.0);
clear v3
temp=fftn(c);
clear c
fc3=fftshift(temp);
clear temp
totp=s(1)*s(2)*s(3);
mpoint=ndim/2+1;

f1=zeros(1,s(1)/2);
f2=f1;
f3=f1;
% p=(abs(fc1)).^2+(abs(fc2)).^2+(abs(fc3)).^2;
% clear fc1 fc2 fc3
p=(abs(fc1)).^2+(abs(fc2)).^2;
clear fc1 fc2
q=(abs(fc3)).^2;
clear fc3

for k=1:mpoint
  for j=1:mpoint
    for i=1:mpoint
      l1=fix(sqrt((i-mpoint)^2+(j-mpoint)^2+(k-mpoint)^2));
      if l1 > 0 && l1<=ndim/2
        f1(l1)=f1(l1)+p(i,j,k);
        f2(l1)=f2(l1)+q(i,j,k);
        f3(l1)=f3(l1)+p(i,j,k)+q(i,j,k);
      end
    end
  end
end
f1=f1/totp;
f2=f2/totp;
f3=f3/totp;
k= 1:mpoint;
save('power_spec.mat','f3','k')
