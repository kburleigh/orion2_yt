#!/bin/bash
##arg1= path to stdout files
##arg2= prefix for all stdout files
#remove *.grep files in path
rm $1/*.grep
#rm list.txt if exists
exist=`find ./$1  -name "${2}"|wc -l`
if [ $exist -gt 0 ]
then
	rm $1/$2	
fi
#make file list
ls $1/$2*|sort > $1/list.txt
