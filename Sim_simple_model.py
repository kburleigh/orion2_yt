import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-results",action="store",help='results.txt file')
parser.add_argument("-cs",type=float,action="store",help='sound speed')
args = parser.parse_args()

#import matplotlib
#matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from scipy.optimize import leastsq,curve_fit

def mdot_bondi(args):
    return 4*np.pi*args.rho0*(bigG*args.mass0)**2/args.cs**3

def mdot_0_krumholz06(sink,args):
	return 4*np.pi*args.rho0*(bigG*sink.mass[0,0])**2/(args.mach*args.cs)**3

def lee_mach_bh(rmsV,cs):
	lam=np.exp(1.5)/4
	return np.power( 1.+np.power(rmsV/cs,4), 1./3)/np.power( 1.+np.power(rmsV/cs/lam,2), 1./6)

def mdot_lee14_eqn27(rmsV,rmsVa,cs):
	beta_ch=19.8
	mach_bh= lee_mach_bh(rmsV,cs)
	inv_beta= rmsVa**2/(2.*cs**2)  
	return mach_bh**-2*np.power(mach_bh+np.power(beta_ch*inv_beta,0.5), -1)

def mdot_lee14_eqn30(rmsV,rmsVa,cs):
	beta_ch=19.8
	mach_bh= lee_mach_bh(rmsV,cs)
	inv_beta= rmsVa**2/(2.*cs**2)  
	return mach_bh**-2*np.minimum(mach_bh**-1,mach_bh*np.power(mach_bh+np.power(beta_ch*inv_beta,0.5), -1))

def mdot_lee14_combo(rmsV,rmsVa,cs):
	log_mean= 0.5*(np.log10(mdot_lee14_eqn27(rmsV,rmsVa,cs))+np.log10(mdot_lee14_eqn30(rmsV,rmsVa,cs)))
	return 10**log_mean

def mdot_bh_div_B(rmsV,args):
    lam= np.exp(1.5)/4
    paren= (lam**2+ (rmsV/args.cs)**2)/(1+(rmsV/args.cs)**2)**4
    return np.sqrt(paren)/lam

def mdot_w_div_B(rmsV,rB_div_L,w_tilde,args):
    lam= np.exp(1.5)/4
    w_x= w_tilde*(rmsV/args.cs)*rB_div_L
    fstar_x= 1./(1.+w_x**0.9)
    return 0.34/lam*fstar_x


def func_va(beta,cs):
	return np.sqrt(2/beta)*cs

def func_beta(va,cs):
	return 2*np.sqrt(cs/va)**2

colors=['k','b','g','r','c','m','y'] +['#33CCFF']
data=pd.read_csv('steady_results.txt',header=0)
#simulation results
log_beta= np.log10(data['beta'])
log_mean= np.log10(data['mean'])
log_med= np.log10(data['median'])
plt.errorbar(log_beta, log_med,ls='none',marker='o',mec='k',mfc='none',ms=10,mew=2,label='median') 
plt.errorbar(log_beta, log_mean,ls='none',marker='s',mec='k',mfc='none',ms=10, mew=2,label='mean')
#fit to parallel Lee14 Eqn 27
def lee_parallel(x,A,beta_ch,n):
	'''x = log10(beta)'''
	beta=np.power(10,x)
	rmsV=5.
	cs=1.
	mach_bh= lee_mach_bh(rmsV,cs)
	return np.log10( A*mach_bh**-2*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n) )
def residuals_par(p, y, x):
	A, beta_ch, n = p
	err = (y - lee_parallel(x,A,beta_ch,n))**2
	#print "residuals_par= ",err
	return err

#opt_mean, cov_mean = curve_fit(lee_parallel, log_beta, log_mean)
#opt_med, cov_med = curve_fit(lee_parallel, log_beta,log_med)
p0 = [1,19.8,1] #opt_mean[0], opt_mean[1],opt_mean[2]]
for i in range(3):
	lsq_mean = leastsq(residuals_par, p0, args=(log_mean, log_beta))
	p0=lsq_mean[0]
plt.plot(log_beta, lee_parallel(log_beta,lsq_mean[0][0],lsq_mean[0][1],lsq_mean[0][2]),c='b',ls='-',label=r'$\parallel$ mean')
#median
p0 = [1,19.8,1] #opt_med[0], opt_med[1],opt_med[2]]
lsq_med = leastsq(residuals_par, p0, args=(log_med, log_beta))
plt.plot(log_beta, lee_parallel(log_beta,lsq_med[0][0],lsq_med[0][1],lsq_med[0][2]),c='b',ls='--',label=r'$\parallel$ median')
print "PAR lsqr A, beta_ch, n: for mean = %.2f,%.2f,%.2f, for median = %.2f,%.2f,%.2f" % \
	(lsq_mean[0][0],lsq_mean[0][1],lsq_mean[0][2],lsq_med[0][0],lsq_med[0][1],lsq_med[0][2])
#fit to perp Lee14 Eqn 30
def lee_perp(x,A,beta_ch,n):
	'''x = log10(beta)'''
	beta=np.power(10,x)
	rmsV=5.
	cs=1.
	mach_bh= lee_mach_bh(rmsV,cs)
	return np.log10( A*mach_bh**-2*np.minimum(mach_bh**-1,mach_bh*np.power(mach_bh**n+np.power(beta_ch/beta,n/2.), -1./n)) )

def residuals_perp(p, y, x):
	A, beta_ch, n = p
	err = (y - lee_perp(x,A,beta_ch,n))**2
	#print "residuals_perp= ",err
	return err
#opt_mean, cov_mean = curve_fit(lee_perp, va, data['mean'])
#opt_med, cov_med = curve_fit(lee_perp, va, data['median'])

#mean
p0 = [1,19.8,1] #[opt_mean[0], opt_mean[1],opt_mean[2]]
for i in range(5): 
	lsq_mean = leastsq(residuals_perp, p0, args=(log_mean, log_beta))
	p0=lsq_mean[0]
plt.plot(log_beta, lee_perp(log_beta,lsq_mean[0][0],lsq_mean[0][1],lsq_mean[0][2]),c='g',ls='-',label=r'$\perp$ mean')
#median
#p0 = [1,19.8,1] #[opt_med[0], opt_med[1],opt_med[2]]
for i in range(5):
	lsq_med = leastsq(residuals_perp, p0, args=(log_med, log_beta))
	p0=lsq_med[0]
plt.plot(log_beta, lee_perp(log_beta,lsq_med[0][0],lsq_med[0][1],lsq_med[0][2]),c='g',ls='--',label=r'$\perp$ median')
print "PERP lsqr A, beta_ch, n: for mean = %.2f,%.2f,%.2f, for median = %.2f,%.2f,%.2f" % \
	(lsq_mean[0][0],lsq_mean[0][1],lsq_mean[0][2],lsq_med[0][0],lsq_med[0][1],lsq_med[0][2])
#
plt.legend(loc=4,ncol=2,fontsize='x-small')
#plt.xscale('log')
#plt.yscale('log')
plt.xlim(-2.5,2.5) #data['beta'].max()*2)
plt.xlabel(r'$Log_{10}(\beta)$')
plt.ylabel(r'Steady $Log_{10}(\dot{M}/\dot{M}_{B})$')
plt.savefig('steady_result.png')
plt.close()

#lee14 func of simulation data
#plt.errorbar(data['beta'], mdot_lee14_eqn27(data['mach'].values,data['rmsVa'].values,args.cs),ls='none',marker='v',mec='b',mfc='none', ms=10, mew=1,label=r'$\parallel$ data')
#plt.errorbar(data['beta'], mdot_lee14_eqn30(data['mach'].values,data['rmsVa'].values,args.cs),ls='none',marker='^',mec='b',mfc='none',ms=10, mew=1,label=r'$\perp$ data')
#lee14 model
beta=np.linspace(0.01,1e2,num=100)
va=func_va(beta,args.cs)
mach=5.
#plt.plot(beta, mdot_lee14_eqn27(mach,va,args.cs),c='b',ls='-',label=r'$\parallel$ continuous')
#plt.plot(beta, mdot_lee14_eqn30(mach,va,args.cs),c='b',ls='--',label=r'$\perp$ continuous')
#plt.plot(data['beta'], mdot_lee14_eqn27(mach,va,args.cs),c='b',ls='-',label=r'$\parallel$ continuous')
#plt.plot(data['beta'], mdot_lee14_eqn30(mach,va,args.cs),c='b',ls='--',label=r'$\perp$ continuous')
#continuous krumholz06
#data=pd.read_csv('vel_vort_evolution.txt',header=0)
#plt.plot(data['beta'], mdot_w_div_B(mach,0.2,data['w-tilde-mean'],args),c='g',ls='-',label=r'$\dot{M}_w$, w-mean')
#plt.plot(data['beta'], mdot_w_div_B(mach,0.2,data['w-tilde-med'],args),c='g',ls='--',label=r'$\dot{M}_w$, w-med')
#plt.hlines(mdot_bh_div_B(mach,args),data['beta'].min(),data['beta'].min(),linestyle='dashed',color='r',label=r'$\dot{M}_BH$')
#mdot at every cell, mean and median computed using k06 turb mdot and Lee14 par and perp mdot
data=pd.read_csv('mdot_every_cell_results.txt',header=0)
m0_div_mB=1./5.**3
##k06 turb,w,bh
#plt.errorbar(data['beta'], data['k06-turb-mean'].values*m0_div_mB,ls='none',marker='+',mec='g',mfc='none',ms=10,mew=1,label='k06-turb-mean')
#plt.errorbar(data['beta'], data['k06-turb-med'].values*m0_div_mB,ls='none',marker='x',mec='g',mfc='none',ms=10,mew=1,label='k06-turb-med')
#plt.errorbar(data['beta'], data['k06-w-mean'].values*m0_div_mB,ls='none',marker='+',mec='0.8',mfc='none',ms=10,mew=1,label='k06-w-mean')
#plt.errorbar(data['beta'], data['k06-w-med'].values*m0_div_mB,ls='none',marker='x',mec='0.8',mfc='none',ms=10,mew=1,label='k06-w-med')
#plt.errorbar(data['beta'], data['k06-bh-mean'].values*m0_div_mB,ls='none',marker='+',mec='y',mfc='none',ms=10,mew=1,label='k06-bh-mean')
#plt.errorbar(data['beta'], data['k06-bh-med'].values*m0_div_mB,ls='none',marker='x',mec='y',mfc='none',ms=10,mew=1,label='k06-bh-med')
##lee14 par,perp
#plt.errorbar(data['beta'], data['lee14-par-mean'].values*m0_div_mB,ls='none',marker='3',mec='m',mfc='none',ms=10,mew=1,label='lee14-par-mean')
#plt.errorbar(data['beta'], data['lee14-par-med'].values*m0_div_mB,ls='none',marker='4',mec='m',mfc='none',ms=10,mew=1,label='lee14-par-med')
#plt.errorbar(data['beta'], data['lee14-perp-mean'].values*m0_div_mB,ls='none',marker='>',mec='r',mfc='none',ms=10,mew=1,label='lee14-perp-mean')
#plt.errorbar(data['beta'], data['lee14-perp-med'].values*m0_div_mB,ls='none',marker='<',mec='r',mfc='none',ms=10,mew=1,label='lee14-perp-med')
data=pd.read_csv('mdot_every_cell_results2.txt',header=0)
m0_div_mB=1./5.**3
#plt.errorbar(data['beta'], data['perp-w-mean'].values*m0_div_mB,ls='none',marker='+',mec='g',mfc='none',ms=10,mew=1,label='perp-w-mean')
##plt.errorbar(data['beta'], data['perp-w-med'].values*m0_div_mB,ls='none',marker='x',mec='g',mfc='none',ms=10,mew=1,label='perp-w-med')
#plt.errorbar(data['beta'], data['par-w-mean'].values*m0_div_mB,ls='none',marker='+',mec='0.8',mfc='none',ms=10,mew=1,label='par-w-mean')
#plt.errorbar(data['beta'], data['par-w-med'].values*m0_div_mB,ls='none',marker='x',mec='0.8',mfc='none',ms=10,mew=1,label='par-w-med')
#plt.errorbar(data['beta'], data['perp-par-w-mean'].values*m0_div_mB,ls='none',marker='+',mec='y',mfc='none',ms=10,mew=1,label='perp-par-w-mean')
#plt.errorbar(data['beta'], data['perp-par-w-med'].values*m0_div_mB,ls='none',marker='x',mec='y',mfc='none',ms=10,mew=1,label='perp-par-w-med')
#lee14 results
lee=pd.read_csv('steady_results_lee14.txt',header=0)
#plt.plot(lee['beta'], lee['par'],c='c',label=r'$\parallel$ results')
#plt.plot(lee['beta'], lee['perp'],c='m',label=r'$\perp$ results')
#krumholz06 results
beta=[1e4,1e4]
mean=np.array([0.9,1.26])*m0_div_mB #mdot0 to mdotB
med=np.array([0.34,0.36])*m0_div_mB #mdot0 to mdotB
#plt.errorbar(beta,mean, yerr=0,c='y',fmt='o',label='k06 mean results')
#plt.errorbar(beta,med, yerr=0,c='y',fmt='*',label='k06 median results')
#plt.legend(loc=4,ncol=2,fontsize='x-small')
#plt.xscale('log')
#plt.yscale('log')
#plt.xlim(data['beta'].min()/2,data['beta'].max()*2) #data['beta'].max()*2)
#plt.xlabel(r'$\beta$')
#plt.ylabel(r'Steady $\dot{M}/\dot{M}_{B}$')
#plt.savefig('steady_result.png')
#plt.close()
