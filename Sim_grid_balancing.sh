#!/bin/bash

#RUN
#bash hw1.sh /project/projectdirs/astro250/data  
#OR chmod +x hw1.sh; ./hw1.sh /project/projectdirs/astro250/data
hdf5=${1}
maxlev=${2}
f="amrBalancing_${hdf5}_.txt"
#grids per level
echo "GRIDS" > ${f}
for (( c=0; c<=${maxlev}; c++ ))
do
    one=level_${c}
    two=\"${a}\"
    three="GROUP ${b}"
    line=`h5dump -A ${hdf5} |grep "${three}" -A 78|grep 'DATASET "boxes"' -A 9|grep 'DATASPACE  SIMPLE'`
    echo "level ${c}" >> ${f}
    echo "${line}" >> ${f}
done
#cells per level
echo "CELLS" >> ${f}
for (( c=0; c<=${maxlev}; c++ ))
do
    one=level_${c}
    two=\"${a}\"
    three="GROUP ${b}"
    line=`h5dump -A ${hdf5} |grep "${three}" -A 82|grep 'DATASET "data:datatype=0"' -A 2|grep 'DATASPACE  SIMPLE'`
    echo "level ${c}" >> ${f}
    echo "${line}" >> ${f}
done 
