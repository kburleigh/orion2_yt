'''To use: copy this script to dir with data.*.hdf5 files and execute with submit script that uses cd to get supercompute to dir with data.*.hdf5 files and python to execute this script'''

from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

def makePlot(f_hdf5,f_sink):
	pf = load(f_hdf5)
	mass,x,y,z= np.loadtxt(f_sink,dtype='float',skiprows=1,usecols=(0,1,2,3),unpack=True)
	
	xy=np.zeros((64,2))
	xy[:,0]=x
	xy[:,1]=y
	inds=np.arange(0,len(xy)-1,4)
	slice_xy= xy[inds]
	
	center= [0.,0.,0.25]
	proj = SlicePlot(pf,"z","density", center=center)
	d={"s":100.,"c":"black","alpha":1}
	for loc in slice_xy:
	    proj.annotate_marker(loc,marker="o",plot_args=d)
	proj.annotate_velocity()
	
	name= "xySlice_Center_0-0-0.25_.%s.png" % f_hdf5[5:9] 
	proj.save(name=name)

f_hdf5= glob.glob1("./","data.*.3d.hdf5")
f_sink= glob.glob1("./","data.*.3d.sink")
for ind in range(1):#len(f_hdf5)):
	makePlot(f_hdf5[ind],f_sink[ind])



	
