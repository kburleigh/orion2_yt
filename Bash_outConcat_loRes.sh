#!/bin/bash
#arg[0] = 0 -- renames all files to std.o*
#arg[0] = 7 -- rename all std.* files as edit.* and PRINT which of these user needs to manually edit when consecutive tsteps
#arg[0] = 777 -- lines of all edit.* files so tsteps are consecutive, allowing concatenation of all edit.*

#rename to std.*
if [ $1 -eq 0 ]
then 
    for i in `ls HDdecay512.o*`
    do 
    #if hit walltime partial sink info could be printed without dt,time info this is bad, so remove those parts if they exist
    upTo=`grep $i -e "coarse time step" -n |tail -n 1|cut -d ':' -f 1`    
    head $i -n $upTo > std.o${i:12}
    done 
fi
#print where manual editting is needed
if [ $1 -eq 7 ]
then
    echo ----- USEFUL INFO ----
    for i in `ls std.o*|sort`; do echo head:$i; grep -e step $i|head -n 1; echo tail; grep -e step $i|tail -n 1; done
    echo ----------------------
    nfiles=`ls std.o*|wc -l`
    count=1
    for i in `ls std.o*|sort`
    do
        if [ $count -eq 1 ]
        then
            file1=$i
            echo first file=$file1
        else
            file2=$i
            echo second are later files, file1=$file1 file2=$file2
            curr=`grep $file1 -e "coarse time step"|tail -n 1|cut -d ' ' -f 4`
            next=`grep $file2 -e "coarse time step"|head -n 1|cut -d ' ' -f 4`
            let curr=$curr+1
            if [ $curr -eq $next ]
            then
                let cm=$count-1
                echo WARNING: manually edit first tstep of $file2 aka edit.$count, concatenate it with edit.$cm, rerun skip with 1st arg=777
            fi
            let cm1=$count-1
            cp $file1 edit.$cm1
            file1=$file2
            if [ $count -eq $nfiles ]
            then
                cp $file2 edit.$count
            fi        
        fi
        let count=$count+1
    done
fi
#auto editing to line up tsteps for concatenation
if [ $1 -eq 777 ]
then
    nitems=`ls edit.* |wc -l`
    cnt=1
    for i in `ls edit.* |sort`
    do
        if [ $cnt -eq 1 ]
        then #just save last step since first file
            echo in IF, cnt=$cnt, i=$i
            cp $i ${i}.beg        
            file1=${i}.beg        
        else #remove lines upto and including step from 2nd file
            echo in ELSE, cnt=$cnt, i=$i
            file2=$i
    #        echo file1=$file1, file2=$file2
            step=`grep $file2 -e "coarse time step" |head -n 1| cut -d ' ' -f 4`
            #1st file, remove everythin after this step
            afterLine=`grep $file1 -e "coarse time step ${step}" -n |cut -d ':' -f 1`
            let afterLine=$afterLine+1
            maxLine=`wc -l $file1|cut -d ' ' -f 1`
            sed "${afterLine},${maxLine}d" $file1 > ${file1}.end
            #2nd file, rm step 
            upToLine=`grep $file2 -e "coarse time step ${step}" -n |cut -d ':' -f 1`
            sed "1,${upToLine}d" $file2 > ${file2}.beg
            #reset file1 to file2
            file1=${file2}.beg
        fi
        if [ $cnt -eq $nitems ]
        then
            echo at end of loop, last file=$file1
            cp $file1 ${file1}.end
        fi
        let cnt=$cnt+1
    done
    #check if got right answer
    echo ----- CORRECT ANSWER? ----
    for i in `ls edit.*.beg.end|sort`; do echo head:$i; grep -e step $i|head -n 1; echo tail; grep -e step $i|tail -n 1; done
    echo ----- IF SO, CONCATENATE -----------------
    #print cat command
    cat_string='cat'
    for out in `ls edit.*.beg.end |sort`
    do
      cat_string="${cat_string} ${out}"
    done
    cat_string="${cat_string} > edit_concat"
    echo ${cat_string}
fi

