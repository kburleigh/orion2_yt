import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-path",action="store",help='path to hdf5 file(s)')
parser.add_argument("-file",action="store",help='hdf5 file(s)')
parser.add_argument("-hasBfield",action="store",help='set to 1 if data has B field info')
parser.add_argument("-cs",type=float,action="store",help='hdf5 file to get velocities for')
#optional
parser.add_argument("-k06_figs",action="store",help='set to 1 to plot krumholz 2006 figs 4,6,7')
parser.add_argument("-msink",type=float,action="store",help='required if k06_figs')
parser.add_argument("-tavg_rho_pdf",action="store",help='set to filename containing data*.hdf5 names to use to make a time averaged density pdf')
parser.add_argument("-append_statsFile",action="store",help='set to 1 to append min,max s, mean vorticity, etc to set to text file for plotting time evolution of turbulent statistics')
parser.add_argument("-fsol_zdrv",action="store",help='if append_statsFile is set, then setting this to path/to/zdrv.hdf5 will append solenoidal ratio for zdrv.hdf5 to stats.txt')
parser.add_argument("-plot_statsFile",type=float,action="store",help='if already have a text file time evolution of min,max rho etc, then the evolution will be plotted')
parser.add_argument("-Geq1",action="store",help='set to 1 if G = 1 units')
parser.add_argument("-ytVersion2",action="store",help='set to 1 if only have yt version less than 3.0')
parser.add_argument("-fname",action="store",help='append fname to end of usual file name so can load,analyze, and save analysis of many different sets from the same directory')
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
if args.ytVersion2: from yt.mods import *
else: import yt
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize 
import scipy.special 
import glob 
import numpy.fft
import h5py
import os
import pickle

import Sim_constants as sc
import Sim_curl
print "finished importing modules"

global global_Svar
print "defined global variables: global_Svar"

font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 10,
        }

if args.Geq1: BIGG = 1.
else: BIGG = sc.G
class PhiVals():
    def __init__(self,phimean,phimed):
        self.phimean= phimean
        self.phimed= phimed

def MdotBH_x(rho_x,MachS_x,msinks,cs,Mach3d):
    lam= np.exp(1.5)/4
    paren= (lam**2+ MachS_x**2)/(1+MachS_x**2)**4
    #paren= (lam**2+ Mach3d**2)/(1+Mach3d**2)**4
    return 4*np.pi*rho_x*(BIGG*msinks)**2/cs**3*np.sqrt(paren)

def MdotW_x(rho_x,vort_x,cs,msinks):
    rB= BIGG*msinks/cs**2
    w_x= vort_x*rB/cs
    fstar_x= 1./(1.+w_x**0.9)
    return 4*np.pi*rho_x*(BIGG*msinks)**2/cs**3 *0.34*fstar_x

def MdotB_x(rho_x,args):
    lam= np.exp(1.5)/4
    return 4.*np.pi*(BIGG*args.msink)**2*rho_x/args.cs**3

def MdotLee_x(mdot, mach_x,beta_x,rho_x,args,Bch=19.8,n=1.):
	lam= np.exp(1.5)/4
	mBH= np.power( 1+np.power(mach_x,4), 1./3 ) / np.power( 1.+np.power(mach_x/lam,2), 1./6  )
	mdot.par= MdotB_x(rho_x,args)/mBH**2*np.power( np.power(mBH,n)+np.power(Bch/beta_x,n/2.), -1./n )
	perp= mBH*np.power(np.power(mBH,n)+np.power(Bch/beta_x,n/2.), -1./n ) #min b/w this and 1./mBH
	imin= np.where(1./mBH < perp)
	if len(imin[0] > 0): perp[imin]= (1./mBH)[imin] #insure non empty index
	mdot.perp= perp* MdotB_x(rho_x,args)/mBH**2  
	#mdot.lee= 0.5*(mdot.par+mdot.perp)

def MdotTurb_x(mdot,args):
    #if args.hasBfield: return 1./np.sqrt(np.power(mdot.bh,-2) + np.power(mdot.w,-2))  + np.power(mdot.lee,-2) )
    #else: return 1./np.sqrt(np.power(mdot.bh,-2) + np.power(mdot.w,-2) )
    return 1./np.sqrt(np.power(mdot.bh,-2) + np.power(mdot.w,-2) )

def Mdot_perp_w_x(mdot,args):
    return 1./np.sqrt(np.power(mdot.perp,-2) + np.power(mdot.w,-2) )

def Mdot_par_w_x(mdot,args):
    return 1./np.sqrt(np.power(mdot.par,-2) + np.power(mdot.w,-2) )

def Mdot_perp_par_w_x(mdot,args):
    return 1./np.sqrt(np.power(mdot.perp,-2)+np.power(mdot.par,-2) + np.power(mdot.w,-2) )

def Mdot0(rho_inf,msink,mach_3d,cs):
    return 4*np.pi*rho_inf*(BIGG*msink)**2/(mach_3d*cs)**3

def GetLogHist(mdot_norm,bins=100):
    logw=np.log(mdot_norm)
    (logn,logbins,jnk)= plt.hist(logw,bins=bins,align='mid',normed=True,histtype='bar')
    plt.close()
    return logbins, logn

def fit_to_u(u, p1):
  return p1*u**3*np.exp(-1.5*u**1.7)


def PlotFig6(fsave,vrms_x,mach3d,args):
    #plot dp/d ln u for data
    (logbins,loghist)= GetLogHist((vrms_x/mach3d/args.cs).flatten())
    bins=(logbins[:-1]+logbins[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghist,'k*',label='u, Mach 5')
    #plt.vlines np.median( wmag_x.flatten()*lbox/Mach_3D/args.cs )
    #plot fit to data using krumholz 2006's functional form "fit_to_u" function
    #guess= fit_to_u(loghist,1.).max()/loghist.max()
    #p1 = curve_fit(fit_to_u, bins, loghist,p0=(guess))
    #fit= fit_to_u(loghist,p1[0][0])
    #plt.plot(bins,fit,'b-',label='fit')
    #finish annotating
    plt.xlim([1e-3,1e1])
    plt.ylim([5e-7,3])
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'u')
    plt.ylabel('dp(u) / d ln u')
    v_mean= (vrms_x/mach3d/args.cs).flatten().mean()
    v_med= np.median((vrms_x/mach3d/args.cs).flatten())
    title="mean= %.2f, med= %.2f" % (v_mean,v_med) 
    plt.title(title)
    plt.savefig(fsave)
    plt.close()

def PlotFig7(fsave,wmag_x,Lbox,mach3d,args):
    #plot dp/d ln u for data
    (logbins,loghist)= GetLogHist(wmag_x.flatten()*Lbox/mach3d/args.cs)
    bins=(logbins[:-1]+logbins[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghist,'k*',label=r'$\tilde{w}$, Mach 5')
    #plt.vlines np.median( vmag_x.flatten()/Mach_3D/args.cs )
    #finish annotating
    plt.xlim([1e-2,1e3])
    plt.ylim([1e-6,1])
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\tilde{w}$')
    plt.ylabel(r'dp($\tilde{w}$) / d ln $\tilde{w}$')
    w_mean= (wmag_x.flatten()*Lbox/mach3d/args.cs).mean()
    w_med= np.median(wmag_x.flatten()*Lbox/mach3d/args.cs)
    title="mean= %.2f, med= %.2f" % (w_mean,w_med) 
    plt.title(title)
    plt.savefig(fsave)
    plt.close()


def PlotFig4(fname,mdot,rBL,args):
    print "shape of mdot.turb,bh= ",mdot.turb.shape,mdot.bh.shape,mdot.w.shape
    class Obj():
        pass
    logbins=Obj()
    loghists=Obj()
    (logbins.turb,loghists.turb)= GetLogHist(mdot.turb.flatten()/mdot.o)
    (logbins.bh,loghists.bh)= GetLogHist(mdot.bh.flatten()/mdot.o,bins=logbins.turb)
    (logbins.w,loghists.w)= GetLogHist(mdot.w.flatten()/mdot.o,bins=logbins.turb)
    if args.hasBfield: 
        (logbins.par,loghists.par)= GetLogHist(mdot.par.flatten()/mdot.o,bins=logbins.turb)
        (logbins.perp,loghists.perp)= GetLogHist(mdot.perp.flatten()/mdot.o,bins=logbins.turb)
    bins=(logbins.turb[:-1]+logbins.turb[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghists.turb,'k-',label='turb')
    plt.plot(bins,loghists.bh,'k--',label='bh')
    plt.plot(bins,loghists.w,'k:',label='w')
    if args.hasBfield: 
        plt.plot(bins,loghists.par,'b--',label='Lee par')
        plt.plot(bins,loghists.perp,'b:',label='Lee perp')
    plt.xlim([1e-2,1e2])
    plt.ylim([0,0.5])
    #     plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\dot{M}/\dot{M}_0$')
    plt.ylabel(r'dp/d ln (M/M0)')
    title="Turb rB/L= %.2f" % rBL
    plt.title(title)
    plt.legend(loc=0)
    plt.savefig(fname)
    plt.close()
    #fout=open(os.path.join(args.path,'mdot_turb.pickle'),"w")
    #pickle.dump((mdot.turb,bins,loghists.turb),fout)
    #fout.close()

def get_fsol(pertx,perty,pertz):
	'''resturn solenoidal ratio'''
	n = pertx.shape
	dtype = np.float
	fx = np.real(np.fft.fftn(pertx))
	fy = np.real(np.fft.fftn(perty))
	fz = np.real(np.fft.fftn(pertz))
	kx = np.zeros(n, dtype=dtype)
	ky = np.zeros(n, dtype=dtype)
	kz = np.zeros(n, dtype=dtype)
	# perform fft k-ordering convention shifts
	for j in range(0,n[1]):
		for k in range(0,n[2]):
			kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
	for i in range(0,n[0]):
		for k in range(0,n[2]):
			ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
	for i in range(0,n[0]):
		for j in range(0,n[1]):
			kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
	k2 = kx**2+ky**2+kz**2
	# comprssive part
	fxc = np.zeros(n, dtype=dtype)
	fyc = np.zeros(n, dtype=dtype)
	fzc = np.zeros(n, dtype=dtype)
	fxc = kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
	fyc = ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
	fzc = kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
	# solenoidal part
	fxs = np.zeros(n, dtype=dtype)
	fys = np.zeros(n, dtype=dtype)
	fzs = np.zeros(n, dtype=dtype)
	fxs = fx - kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
	fys = fy - ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
	fzs = fz - kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
	normC = np.sqrt(np.sum(fxc**2 + fyc**2 + fzc**2)/np.product(n))
	normS = np.sqrt(np.sum(fxs**2 + fys**2 + fzs**2)/np.product(n))
	return normS/(normS+normC)

def GetPercentile(x,ptile):
    xsort= np.sort(x,kind="mergesort")
    p= int(ptile*len(xsort))
    return xsort[p]

def densityPDF(fsave,s_flat,args):
	'''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
	f,axis=plt.subplots(2,1,sharex=True,figsize=(5,10))
	plt.subplots_adjust( hspace=0 )
	ax=axis.flatten()
	(s_pdf,s_bins,jnk)= ax[0].hist(s_flat,bins=100,normed=True,align='mid',histtype='step',color="k")
	ax[0].set_ylabel('PDF s')
	#     bins=(s_bins[:-1]+s_bins[1:])/2
	#     plt.plot(s_bins,s_pdf,'k-')    
	#     ax[0].vlines(s.mean(),0,1.1*n.max(),colors='b',linestyles='dashed',label="mean")
	ax[0].legend(loc=1) #,fontdict=font)
	ax[0].text(0.8, 0.8,'min= %.2g' %s_flat.min(), \
				horizontalalignment='center',verticalalignment='center',transform=ax[0].transAxes,\
				bbox=dict(facecolor='blue', alpha=0.1),fontdict=font)
	ax[0].text(0.8, 0.7,'max= %.2g' %s_flat.max(), \
				horizontalalignment='center',verticalalignment='center',transform=ax[0].transAxes,\
				bbox=dict(facecolor='blue', alpha=0.1),fontdict=font)
	ax[0].text(0.8, 0.6,'mean= %.2g' %s_flat.mean(), \
				horizontalalignment='center',verticalalignment='center',transform=ax[0].transAxes,\
				bbox=dict(facecolor='blue', alpha=0.1),fontdict=font)
	ax[0].set_yscale('linear')
	(s_pdf,s_bins,jnk)= ax[1].hist(s_flat,bins=100,normed=True,align='mid',histtype='step',color="k")
	ax[1].set_xlim(-5,5)
	ax[1].set_xscale('linear')
	ax[1].set_yscale('log')
	ax[1].set_ylabel('Log10 PDF s')
	ax[1].set_xlabel(r"s $\equiv$ Ln( $\rho / \rho_0$ )")
	plt.savefig(fsave,dpi=150) 
	plt.close()

def velPDF(fsave,vmag_flat,mach3D,args):
	'''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
	f,axis=plt.subplots(2,1,sharex=True,figsize=(5,10))
	plt.subplots_adjust( hspace=0 )
	ax=axis.flatten()
	(M_pdf,M_bins,jnk)= ax[0].hist(vmag_flat/args.cs,bins=50,normed=True,align='mid',histtype='step',color="k")
	ax[0].vlines(vmag_flat.mean()/args.cs,M_pdf.min(),M_pdf.max(),linestyle='dotted',colors='blue',label='mean')
	ax[0].set_ylabel('PDF(Mach)')
	#     bins=(s_bins[:-1]+s_bins[1:])/2
	#     plt.plot(s_bins,s_pdf,'k-')    
	#     ax[0].vlines(s.mean(),0,1.1*n.max(),colors='b',linestyles='dashed',label="mean")
	val= vmag_flat.min()/args.cs
	ax[0].text(0.8, 0.8,'min= %.2g' % val, \
				horizontalalignment='center',verticalalignment='center',transform=ax[0].transAxes,\
				bbox=dict(facecolor='blue', alpha=0.1),fontdict=font)
	val= vmag_flat.max()/args.cs
	ax[0].text(0.8, 0.7,'max= %.2g' % val, \
				horizontalalignment='center',verticalalignment='center',transform=ax[0].transAxes,\
				bbox=dict(facecolor='blue', alpha=0.1),fontdict=font)
	ax[0].text(0.8, 0.6,'3D rms Mach= %.2g'  %mach3D, \
				horizontalalignment='center',verticalalignment='center',transform=ax[0].transAxes,\
				bbox=dict(facecolor='blue', alpha=0.1),fontdict=font)
	ax[0].set_yscale('linear')
	(M_pdf,M_bins,jnk)= ax[1].hist(vmag_flat/args.cs,bins=100,normed=True,align='mid',histtype='step',color="k")
	ax[1].vlines(vmag_flat.mean()/args.cs,M_pdf.min(),M_pdf.max(),linestyle='dotted',colors='blue',label='mean')
	ax[1].set_xlim(0,15)
	ax[1].set_xscale('linear')
	ax[1].set_yscale('log')
	ax[1].set_ylabel('Log10 PDF(Mach)')
	ax[1].set_xlabel(r"Mach number")
	ax[0].legend(loc=1) #,fontdict=font)
	ax[1].legend(loc=1) #,fontdict=font)
	plt.savefig(fsave,dpi=150)
	plt.close() 

def vortPDF(fsave,vort_flat,lbox,mach3D,args):
	'''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
	f,axis=plt.subplots(2,1,sharex=True,figsize=(5,10))
	plt.subplots_adjust( hspace=0 )
	ax=axis.flatten()
	vort_eff= mach3D*args.cs/lbox
	(w_pdf,w_bins,jnk)= ax[0].hist(vort_flat/vort_eff,bins=500,normed=True,align='mid',histtype='step',color="k")
	ax[0].set_ylabel('PDF($\omega / \omega_0$)')
	#     bins=(s_bins[:-1]+s_bins[1:])/2
	#     plt.plot(s_bins,s_pdf,'k-')    
	#     ax[0].vlines(s.mean(),0,1.1*n.max(),colors='b',linestyles='dashed',label="mean")
	ax[0].legend(loc=1) #,fontdict=font)
	ax[0].set_yscale('linear')
	(w_pdf,w_bins,jnk)= ax[1].hist(vort_flat/vort_eff,bins=500,normed=True,align='mid',histtype='step',color="k")
	ax[1].set_xlim(0,50)
	ax[1].set_xscale('linear')
	ax[1].set_yscale('log')
	ax[1].set_ylabel(r'Log10 PDF($\omega / \omega_0$)')
	ax[1].set_xlabel(r"$\omega / \omega_0$")
	plt.savefig(fsave,dpi=150) 
	plt.close()

def getVrms_weighted3DVrms(vx,vy,vz,rho,args):
    vmag2= (vx**2+vy**2+vz**2)
    vrms_mw= np.sqrt(np.average(vmag2,weights=rho))
    return (np.sqrt(vmag2),vrms_mw/args.cs)

def normalize(x,y):
    coeff= 1./np.trapz(y=y,x=x)
    return coeff*y

def gauss_func(x,mu,std):
	pdf= np.exp(-(x-mu)**2/2/std**2)/std/np.sqrt(2*np.pi)
	return normalize(x,pdf)

def log_normal(x,Svar):
    return np.exp(-(x+Svar/2)**2/2/Svar)/np.sqrt(2*np.pi*Svar)

def hopkins_func(x,T):
	print "hopkins_func: using global Svar= %.2f" % global_Svar
	Svar= global_Svar
	if T == 0.: 
		pdf= log_normal(x,Svar)
	else:
		pdf=np.zeros(len(x))-1 
		lam=Svar/2/T**2
		for i in range(len(x)):
			u=lam/(1.+T)- x[i]/T
			if u < 0: pdf[i]=0.
			elif np.sqrt(lam*u) > 50: #avoid Nan from exp(HUGE)
				a=lam**0.25/u**0.75/(4*np.pi)**0.5
				b=np.exp(2*np.sqrt(lam*u)-lam-u)
				pdf[i]= a*b
			else: #compute directly
				pdf[i]= scipy.special.iv(1,2*np.sqrt(lam*u))*np.exp(-(lam+u))*np.sqrt(lam/u)
	return normalize(x,pdf)
	#return pdf

def get_chi2(o,e):
    chi2= (o-e)**2/e
    return chi2.sum()

#main
if args.tavg_rho_pdf:
    fnames= np.loadtxt(args.tavg_rho_pdf,dtype=np.string_,unpack=True) 
    bins= 100. 
    hists= np.zeros([len(fnames),bins])-1
    check_means=np.zeros(len(fnames)) #check that mean rho does not change
    #plot as go
    f,axis=plt.subplots(3,1,sharex=True,figsize=(8,10))
    plt.subplots_adjust( hspace=0,wspace=0.5 )
    ax=axis.flatten()
    #load data 
    for cnt,f in enumerate(fnames):
        lev=0
        if args.ytVersion2: 
           ds=load(f)
           lbox= float(ds.domain_width[0])
           cube= ds.h.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                              dims=ds.domain_dimensions) 
        else:
            ds=yt.load(f)
            lbox= float(ds.domain_width[0])
            cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                              dims=ds.domain_dimensions)
        ln_p = np.array(cube["density"])
        check_means[cnt]=ln_p.flatten().mean()
        ln_p= np.log( ln_p/ln_p.flatten().mean() )
        if cnt == 0: 
            file_insertSinks= f
            (hists[cnt],const_bins,jnk)= ax[2].hist(ln_p.flatten(),bins=bins,normed=True,align='mid',histtype='step',label=str(cnt))
        else: (hists[cnt],jnk2,jnk)= ax[2].hist(ln_p.flatten(),bins=const_bins,normed=True,align='mid',histtype='step',label=str(cnt))
    #fits to time averaged pdf
    xdata=(const_bins[1:]+const_bins[:-1])/2
    ydata=np.mean(hists,axis=0)
    #wts= np.sum(hists,axis=0)/np.sum(hists,axis=0).sum() #total counts per bin normalized by total counts
    ax[1].scatter(xdata,ydata,s=10,facecolors='none',edgecolors='k',label='time averaged')
    #log-normal fit to time averaged pdf
    opt, cov = scipy.optimize.curve_fit(gauss_func, xdata, ydata) #,sigma=wts)
    err = np.sqrt(np.diag(cov))
    chi2= get_chi2(ydata,gauss_func(xdata,opt[0],opt[1]))
    lab='log-norm: mu=%.2f, std=%.2f, chi-sq=%.2f, DF=%d' % (opt[0],opt[1],chi2,len(opt))
    ax[1].plot(xdata,gauss_func(xdata,opt[0],opt[1]),'b-',label=lab)
    #hopkins 2013 fit needs the variance of the best fit log-normal curve, 
    #but curve_fit does not allow passing constants so define GLOBAL var for variance
    global_Svar= opt[1]
    opt, cov = scipy.optimize.curve_fit(hopkins_func, xdata, ydata) #,sigma=wts)
    err = np.sqrt(np.diag(cov))
    chi2= get_chi2(ydata,hopkins_func(xdata,opt[0]))
    lab='hopkins-13: T=%.2f, chi-sq=%.2f, DF=%d' % (opt[0],chi2,len(opt))
    #ax[1].plot(xdata,hopkins_func(xdata,opt[0]),'r-',label=lab)
    #ax[1].plot(xdata,hopkins_func(xdata,0.6),'k-',label="T=0.6")
    #ax[1].plot(xdata,hopkins_func(xdata,0.4),'g-',label="T=0.4")
    #ax[1].plot(xdata,hopkins_func(xdata,0.12),'c-',label="T=0.12")
    #fits to pdf inserted sinks into
    ydata=hists[0]
    #wts= hists[0]/np.sum(hists[0]) #total counts per bin normalized by total counts
    ax[0].scatter(xdata,ydata,s=10,facecolors='none',edgecolors='k',label=file_insertSinks)
    #log-normal fit to time averaged pdf
    opt, cov = scipy.optimize.curve_fit(gauss_func, xdata, ydata) #,sigma=wts)
    err = np.sqrt(np.diag(cov))
    chi2= get_chi2(ydata,gauss_func(xdata,opt[0],opt[1]))
    lab='log-norm: mu=%.2f, std=%.2f, chi-sq=%.2f, DF=%d' % (opt[0],opt[1],chi2,len(opt))
    ax[0].plot(xdata,gauss_func(xdata,opt[0],opt[1]),'b-',label=lab)
    #using log-normal variance, hopkins 2013 fit to TA pdf
    global_Svar= opt[1]
    opt, cov = scipy.optimize.curve_fit(hopkins_func, xdata, ydata) #,sigma=wts)
    err = np.sqrt(np.diag(cov))
    chi2= get_chi2(ydata,hopkins_func(xdata,opt[0]))
    lab='hopkins-13: T=%.2f, chi-sq=%.2f, DF=%d' % (opt[0],chi2,len(opt))
    #ax[0].plot(xdata,hopkins_func(xdata,opt[0]),'r-',label=lab)
    #finish labeling
    ax[0].set_ylabel("PDF insert Sinks into")
    ax[1].set_ylabel("Time Averaged PDF")
    ax[2].set_ylabel("Volume Weighted PDF")
    ax[2].set_xlabel(r'$ln(\rho/\bar{\rho})$')
    for axi in ax: 
        axi.set_yscale('log')
        axi.set_ylim(1e-6,1e0)
    ax[0].legend(loc=3,fontsize='small')
    ax[1].legend(loc=3,fontsize='small')
    ax[0].set_title('density PDF')
    plt.savefig("tavg_rho_pdf.png",dpi=150)
    tol= (check_means.min() - check_means.max())/(check_means.min() + check_means.max())
    if tol > 1e-5: 
        print "mean rho is changing in time, Something is Bad"
        raise ValueError


lev=0
if args.ytVersion2: 
   ds=load(os.path.join(args.path,args.file))
   lbox= float(ds.domain_width[0])
   cube= ds.h.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions) 
else:
    ds=yt.load(os.path.join(args.path,args.file))
    lbox= float(ds.domain_width[0])
    cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions)

print "gathering 3d data arrays"
#3D arrays
rho_x = np.array(cube["density"])
vx = np.array(cube["X-momentum"])/rho_x
vy = np.array(cube["Y-momentum"])/rho_x
vz = np.array(cube["Z-momentum"])/rho_x
if args.hasBfield:
    bx = np.array(cube["X-magnfield"])*(4.*np.pi)**0.5
    by = np.array(cube["Y-magnfield"])*(4.*np.pi)**0.5
    bz = np.array(cube["Z-magnfield"])*(4.*np.pi)**0.5
    Bmag= np.sqrt(bx**2+by**2+bz**2)
    beta_x= 8*np.pi*rho_x*args.cs**2/Bmag**2
    del bx,by,bz,Bmag
print "3d arrays extracted, now calculating vorticity and single numbers like 3D Mach"
rho_inf= rho_x.flatten().mean()
(vmag_x,Mach_3D)= getVrms_weighted3DVrms(vx,vy,vz,rho_x,args)
(wmag_x,div_x)= Sim_curl.getCurl(vx,vy,vz,  ds) #3D array
fsol= get_fsol(vx,vy,vz) 
del vx,vy,vz,cube
#plot projection plots following federrath 2010
#proj=[]
#proj.append( np.log10(np.sum(rho_x,axis=2)) )
#proj.append( np.log10(np.sum(div_x,axis=2)) )
#proj.append( np.sum(wmag_x,axis=2)/100. )
#fig = plt.figure()
#plt.subplots_adjust(hspace=0.1)
#cticks= [[-2,-1,0,1],[1,2,3],[-1,0,1]]
#cmaps=['Blues','hot','rainbow']
#labs=[r'column density $log_{10}\Sigma$',r'proj. vorticity $log_{10}|\nabla x v|$',r'proj. divergence $\nabla v [10^2]$']
#for i in [0,1,2]:
#    a=fig.add_subplot(3,1,i+1)
#    imgplot = plt.imshow(proj[i])
#    a.get_yaxis().set_ticks([])
#    a.set_ylabel(labs[i])
#    a.get_xaxis().set_ticks([])
#    #imgplot.set_clim(proj[i].min(),proj[i].max())
#    imgplot.set_clim(cticks[i][0],cticks[i][-1])
#    imgplot.set_cmap(cmaps[i])
#    #plt.colorbar(ticks=[proj[i].min(),(proj[i].min()+proj[i].max())/2,proj[i].max()], orientation ='vertical',pad=0.01)
#    plt.colorbar(ticks=cticks[i], orientation ='vertical',pad=0.01)
#plt.savefig(os.path.join(args.path,'proj.png'),dpi=150)
#plt.close()
#from here have 2 options
#1)append to stats.txt with statistics
#2)plot PDFs for a given turbulent snapshot
if args.append_statsFile:  #1
	print "appending turbulent statistics to stats.txt"
	s=np.log(rho_x.flatten()/rho_inf)
	m=vmag_x.flatten()/args.cs
	w=wmag_x.flatten()/(Mach_3D*args.cs/lbox)
	fin= open(os.path.join(args.path,"stats.txt"),'a')
	fin.write("#filename, time, s min,s max,s mean,s med,m min,m max,m mean,m median,m rms,w mean,w med,fsol\n")
	fin.write("%s %f %.3g %.3g %.3f %.3f %.3f %.3f %.3g %.3g %.3g %.3g %.3g %.2f\n" % 
			  (ds.basename,float(ds.current_time),s.min(),s.max(),s.mean(),np.median(s),\
				m.min(),m.max(),m.mean(),np.median(m),Mach_3D,  w.mean(),np.median(w), fsol))
	del s,m,w
	if args.fsol_zdrv: #then output fsol for zdrv.hdf5 pointed to by args.fol_zdrv
		f = h5py.File(args.fsol_zdrv, 'r')
		vx = f['pertx'][:]
		vy = f['perty'][:]
		vz = f['pertz'][:]
		f.close()	
		fsol= get_fsol(vx,vy,vz)
		fin.write("#filename fsol\n")
		fin.write("%s %.2f\n" % (args.fsol_zdrv,fsol) )
	fin.close()
else:  #2
	print "plotting default PDFs"
	if args.fname:
		velName='velocityPDF_%s_%s.png' % (ds.basename[5:9],args.fname)
		vorName='vorticityPDF_%s_%s.png' % (ds.basename[5:9],args.fname)
		denName='densityPDF_%s_%s.png' % (ds.basename[5:9],args.fname)
	else:
		velName='velocityPDF_%s.png' % ds.basename[5:9]
		vorName='vorticityPDF_%s.png' % ds.basename[5:9]
		denName='densityPDF_%s.png' % ds.basename[5:9]
	velPDF(os.path.join(args.path,velName),vmag_x.flatten(),Mach_3D,args)
	vortPDF(os.path.join(args.path,vorName),wmag_x.flatten(),lbox,Mach_3D,args)
	s=np.log(rho_x/rho_inf)
	densityPDF(os.path.join(args.path,denName),s.flatten(),args)
	del s

if args.k06_figs:
    print "making Krumholz 2006 figures 4,6,7"
    if args.fname:
        f6Name="k06_Fig6_"+ds.basename[5:9]+"_"+args.fname+".png"
        f7Name="k06_Fig7_"+ds.basename[5:9]+"_"+args.fname+".png"
        f4Name="k06_Fig4_"+ds.basename[5:9]+"_"+args.fname+".png"
    else:
        f6Name="k06_Fig6_"+ds.basename[5:9]+".png"
        f7Name="k06_Fig7_"+ds.basename[5:9]+".png"
        f4Name="k06_Fig4_"+ds.basename[5:9]+".png"
    PlotFig6(os.path.join(args.path,f6Name),vmag_x,Mach_3D,args)
    PlotFig7(os.path.join(args.path,f7Name),wmag_x,lbox,Mach_3D,args)
    wsquig= wmag_x.flatten()*lbox/Mach_3D/args.cs
    phi_w= np.median(wsquig)/10
    phi_v= np.median(vmag_x.flatten()/Mach_3D/args.cs)
    print "phi_v= %.2f, phi_w= %.2f" % (phi_v,phi_w)
    class Objs():
        pass
    mdot= Objs()
    print "calculating 3D Mdot arrays Mbh,Mw,Mturb"
    ##3D mdot arrays
    mdot.bh= MdotBH_x(rho_x,vmag_x/args.cs,args.msink,args.cs,Mach_3D)
    mdot.w= MdotW_x(rho_x,wmag_x,args.cs,args.msink)
    if args.hasBfield: 
        MdotLee_x(mdot, vmag_x/args.cs,beta_x,rho_x,args)
        mdot.perp_w= Mdot_perp_w_x(mdot,args)
        mdot.par_w= Mdot_par_w_x(mdot,args)
        mdot.perp_par_w= Mdot_perp_par_w_x(mdot,args)
    mdot.turb= MdotTurb_x(mdot,args)
    ##free non-mdot_x arrays
    del rho_x,vmag_x,wmag_x
    ##0th order single number  mdot
    mdot.o= Mdot0(rho_inf,args.msink,Mach_3D,args.cs)
    #print 
    print "PRINTING MDOTS: mdotTurb mean=%.2f,med=%.2f" % \
        (np.mean(mdot.turb)/mdot.o,np.median(mdot.turb)/mdot.o)
    if args.hasBfield: 
        print "CONT'D: mdotPar mean=%.2f,med=%.2f, mdotPerp mean=%.2f,med=%.2f" % \
        (np.mean(mdot.par)/mdot.o,np.median(mdot.par)/mdot.o,  np.mean(mdot.perp)/mdot.o,np.median(mdot.perp)/mdot.o)
    print "CONT'D: mdotW mean=%.2f,med=%.2f, mdotBH mean=%.2f,med=%.2f" % \
        (np.mean(mdot.w)/mdot.o,np.median(mdot.w)/mdot.o,np.mean(mdot.bh)/mdot.o,np.median(mdot.bh)/mdot.o)
    print "CONT'D: mdot_perp_w mean=%.2f,med=%.2f, mdot_par_w mean=%.2f,med=%.2f, mdot_perp_par_w mean=%.2f,med=%.2f" % \
        (np.mean(mdot.perp_w)/mdot.o,np.median(mdot.perp_w)/mdot.o,np.mean(mdot.par_w)/mdot.o,np.median(mdot.par_w)/mdot.o, np.mean(mdot.perp_par_w)/mdot.o,np.median(mdot.perp_par_w)/mdot.o)
    rBL= BIGG*args.msink/args.cs**2/lbox
    PlotFig4(os.path.join(args.path,f4Name),mdot,rBL,args)
    #
    mod_med= np.median(mdot.turb.flatten())/mdot.o
    mod_mean= np.mean(mdot.turb.flatten())/mdot.o
    print "Model Prediction: phi_mean= %.4f, phi_med = %.4f" % (mod_mean,mod_med)
    print "rB/L= %.4f, Log10 rB/L = %.4f" % (rBL,np.log10(rBL))

print "done"

