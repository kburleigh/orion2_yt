'''visualize grids on a given level compared to level 0. Used for comparing SMR grinding to actual Sink gridding'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-MaxLev",action="store",help='integer for max level')
parser.add_argument("-smr",action="store",help='y if gridding for smr')
parser.add_argument("-sink",action="store",help='y if gridding for sink')
args = parser.parse_args()
if args.MaxLev: MaxLev= int(args.MaxLev)
else: raise ValueError
if args.smr: name_ext= "smr"
elif args.sink: name_ext= "sink"
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

def x1CellBnds_GridsOneLevel(fsave,pf,level,dir_proj):  
    cell_width= float(abs(pf.domain_left_edge[0])+pf.domain_right_edge[0])/pf.domain_dimensions[0] 
    #pf.units["cells"]= 1/cell_width
    x1cells= pf.domain_dimensions[0]
    p = ProjectionPlot(pf, dir_proj, "density", center=pf.domain_center)#,axes_unit="cells")
    p.annotate_grids(min_level=level, max_level=level)
    args={'color':'white','linewidth':2,'ls':'--'}
    for x in np.arange(1,x1cells): #draw base grid cell boundaries
        pt=x/float(x1cells)
        p.annotate_image_line((pt, 0),(pt, 1), plot_args=args)
    title= "Level %s" % str(level)
    p.annotate_title(title)
    p.save(name=fsave)


#MAIN
#two files only, either 2 smr file OR 2 sink files
f_hdf5= ["data.0075.3d.hdf5","data.0087.3d.hdf5"]
spath="./plots/"

for f in f_hdf5:
    print "loading hdf5 file: %s" % f
    pf=load(f)
    for lev in np.arange(1,MaxLev+1):
        #x proj
        fsave=spath+"vis_grids_dirX_lev%d_typ%s_fil%s.png" % (lev,name_ext,pf.basename[5:9])
        x1CellBnds_GridsOneLevel(fsave,pf,level=lev,dir_proj="x")
        #z proj
        fsave=spath+"vis_grids_dirZ_lev%d_typ%s_fil%s.png" % (lev,name_ext,pf.basename[5:9])
        x1CellBnds_GridsOneLevel(fsave,pf,level=lev,dir_proj="z")
