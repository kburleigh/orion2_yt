'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and plot vs. time'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5",action="store",help='hdf5 data dump to load')
parser.add_argument("-cs",action="store",help='adiabatic sound speed in cm/sec')
args = parser.parse_args()
if args.hdf5 and args.cs: 
    hdf5= str(args.hdf5)
    cs= float(args.cs)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np

def _velocity_x(field, data):
    return data['X-momentum']/data['density']
yt.add_field(("gas","velocity_x"), function=_velocity_x) #, units="cm/s")

def _velocity_y(field, data):
    return data['Y-momentum']/data['density']
yt.add_field(("gas","velocity_y"), function=_velocity_y) #, units="cm/s")

def _velocity_z(field, data):
    return data['Z-momentum']/data['density']
yt.add_field(("gas","velocity_z"), function=_velocity_z) #, units="cm/s")

ds= load(hdf5)
#yt method
box=ds.region(ds.domain_center,ds.domain_left_edge,ds.domain_right_edge)
rho= np.array(box[('chombo', 'density')])
vx= np.array(box[("gas","velocity_x")])
vy= np.array(box[("gas","velocity_y")])
vz= np.array(box[("gas","velocity_z")])
vmag2= vx**2+vy**2+vz**2
v2rms= np.average(vmag2,weights=rho)
Machrms= v2rms**0.5/cs
print 'yt Machrms: ', Machrms
#OM method
cube= ds.covering_grid(level=0,\
                  left_edge=ds.h.domain_left_edge,
                  dims=ds.domain_dimensions)
rho = cube["density"]
vx= cube["X-momentum"]/rho
vy= cube["Y-momentum"]/rho
vz= cube["Z-momentum"]/rho
vmag2= (vx**2+vy**2+vz**2)
v2rms= np.average(vmag2,weights=rho)
Machrms= v2rms**0.5/cs
print 'OM Machrms: ', Machrms



