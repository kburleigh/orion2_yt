file = "test.sink"
f = open(file)
line = f.readline()
line = f.readline()
f.close()

sink=[]
d={}
keys = ["m","x","y","z","px","py","pz","Lx","Ly","Lz","id"]
larr = line.strip(' \n').split(' ')
larr.remove("")
for i,key in enumerate(keys):
    d[key] = float(larr[i])
sink.append(d)