#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N 1
#SBATCH -t 00:08:00
#SBATCH -J rv
#SBATCH -o rv.o%j
#SBATCH --mail-user=kburleigh@lbl.gov
#SBATCH --mail-type=END,FAIL
#SBATCH -L SCRATCH

sink_id=${1}

export mymod=b1
if [ "$mymod" == "bInf" ]; then
    export fil=/scratch1/scratchdirs/kaylanb/sf/supersonic_accretion/bInf_sd2/data.0194.3d.hdf5
elif [ "$mymod" == "b1" ]; then
    export fil=/scratch1/scratchdirs/kaylanb/sf/supersonic_accretion/b1_sd2/data.0484.3d.hdf5
fi

cd $SLURM_SUBMIT_DIR
date
srun -n 1 python modules/hdf5_params/radial_velocity.py --hdf5 ${fil} --model ${mymod} --init_sink_file modules/hdf5_params/init.sink --sink_id ${sink_id}
date
echo DONE
