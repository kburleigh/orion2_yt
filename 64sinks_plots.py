from yt.mods import *
fdata= "data.00*.3d.hdf5"
fsinks= "data.00*.3d.sink"
pf = load(fdata)
pf.h.print_stats()
mass,x,y,z= np.loadtxt(fsinks,dtype='float',skiprows=1,usecols=(0,1,2,3),unpack=True)

proj = ProjectionPlot(pf,"x","density", center=pf.domain_center,\
                      max_level=1)

d={"s":100.,"c":"black","alpha":1}
xyz=np.zeros((64,3))
xyz[:,0]=x
xyz[:,1]=y
xyz[:,2]=z
for i in range(64):
    proj.annotate_marker(xyz[i],marker="o",plot_args=d)
proj.annotate_velocity(factor=32)
proj.save()
