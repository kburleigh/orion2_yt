'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and plot vs. time'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-cs",action="store",help='adiabatic sound speed in cm/sec')
parser.add_argument("-tb",action="store",help='bondi time in sec')
args = parser.parse_args()
if args.cs: cs_ad = float(args.cs)
else: raise ValueError
if args.tb: tB = float(args.tb)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob
from pickle import dump as pdump

class TimeSeries():
    def __init__(self,files):
        '''files: hdf5 file list'''
        self.Time= np.zeros(len(files)) -1
        self.Rho= self.Time.copy()
        self.MachS= self.Time.copy()
#         self.Beta= self.Time.copy()
#         self.MachA= self.Time.copy()
        self.Curl= self.Time.copy()
#         self.Brms= self.Time.copy()

def save_vals(ts,cnt,dat):
    ts.Rho[cnt]= dat.alld.AvgQs.rho
    ts.MachS[cnt]=dat.alld.AvgQs.machs
#     ts.Brms[cnt]= avgQ.Brms
#     ts.Beta[cnt]= avgQ.Beta
#     ts.MachA[cnt]= avgQ.MachA
    ts.Curl[cnt]= dat.cgrid.AvgQs.curl

    
class HistBothProps():
    def __init__(self):
        class Props():
            def __init__(self):
                self.bins=0
                self.n_alld=0
                self.n_cgrid=0
        class Both():
            def __init__(self):
                self.cgrid= Props()
                self.alld= Props()
        self.rho= Both()
        self.machs= Both()
    
def plot_hgrams(fsave,dat):
    '''plot covering grid and yt data on each plot'''
    myhist= HistBothProps() #class to save rho,machs hist info in
    f,axis=plt.subplots(3,2,figsize=(15,15))
    ax=axis.flatten()
    xlab=['Log rho','Log MachS','Residual','Residual','Log |Curl|','blank']
    ylab=['PDF','PDf','(cgrid - alld)/alld','(cgrid - alld)/alld','Log |Curl|','blank']
    #log rho
    y=np.log10(dat.cgrid.rho.flatten())
    (myhist.rho.n_cgrid, myhist.rho.bins, patches)=ax[0].hist(y,bins=100,color="b",histtype='step',\
                                label="CovGrid",normed=True,stacked=True)
    ax[0].vlines(np.log10(dat.cgrid.AvgQs.rho),0,1.1*myhist.rho.n_cgrid.max(),colors='b',linestyles='dashed')
    y=np.log10(dat.alld.rho.flatten())
    (myhist.rho.n_alld, bins, patches)=ax[0].hist(y,bins=myhist.rho.bins,color="k",histtype='step',\
                                label="AllData",normed=True,stacked=True)
    ax[0].vlines(np.log10(dat.alld.AvgQs.rho),0,1.1*myhist.rho.n_alld.max(),colors='k',linestyles='dotted')
    #log machs
    y=np.log10(dat.cgrid.machs.flatten())
    (myhist.machs.n_cgrid, myhist.machs.bins, patches)=ax[1].hist(y,bins=100,color="b",histtype='step',\
                                label="CovGrid",normed=True,stacked=True)
    ax[1].vlines(np.log10(dat.cgrid.AvgQs.machs),0,1.1*myhist.machs.n_cgrid.max(),colors='b',linestyles='dashed')
    y=np.log10(dat.alld.machs.flatten())
    (myhist.machs.n_alld, bins, patches)=ax[1].hist(y,bins=myhist.machs.bins,color="k",histtype='step',\
                                label="AllData",normed=True,stacked=True)
    ax[1].vlines(np.log10(dat.alld.AvgQs.machs),0,1.1*myhist.machs.n_alld.max(),colors='k',linestyles='dotted')
    #Residual log rho
    y= (myhist.rho.n_cgrid - myhist.rho.n_alld)/myhist.rho.n_alld
    ax[2].scatter(myhist.rho.bins[:-1],y,s=30,c='b',marker='o')
    ax[2].hlines(0,myhist.rho.bins[:-1].min(),myhist.rho.bins[:-1].max(),colors='k',linestyle='dashed')
    #Residual log machs
    y= (myhist.machs.n_cgrid - myhist.machs.n_alld)/myhist.machs.n_alld
    ax[3].scatter(myhist.machs.bins[:-1][10:90],y[10:90],s=30,c='b',marker='o')
    ax[3].hlines(0,myhist.machs.bins[:-1].min(),myhist.machs.bins[:-1].max(),colors='k',linestyle='dashed')
    #log curl
    y=np.log10(dat.cgrid.curl.flatten())
    (n, bins, patches)=ax[4].hist(y,bins=100,color="k",histtype='step',label="CovGrid",\
                                  normed=True,stacked=True)
    ax[4].vlines(np.log10(dat.cgrid.AvgQs.curl),0,1.1*n.max(),colors='b',linestyles='dashed')
#     y=np.log10(dat.alld.curl.flatten())
#     (n, bins, patches)=ax[2].hist(y,bins=bins,color="k",histtype='step',label="AllData")
#     ax[2].vlines(np.log10(dat.alld.AvgQ.curl),0,1.1*n.max(),colors='k',linestyles='dashed')
    for cnt,axes in enumerate(ax):
        axes.set_xlabel(xlab[cnt])
        axes.set_ylabel(ylab[cnt])
        axes.legend(loc=2)
    plt.savefig(fsave,dpi=150)

def plot_TSeries(fsave,ts,tB):
    '''ts: returned by TimeSeries
    tB: bondi time in units of simulation'''
    f,axis= plt.subplots(1,3,figsize=(10,2.5))
    ax=axis.flatten()
    time= (ts.Time - ts.Time[0])/tB
    ax[0].plot(time,ts.Rho,"k*")
    ax[1].plot(time,ts.MachS,"k*")
#     ax[2].plot(time,ts.MachA,"k*")
#     ax[3].plot(time,ts.Beta,"k*",)
#     ax[4].plot(time,ts.Brms,"k*")
    ax[2].plot(time,ts.Curl,"k*")
    ylabs= ['Rho',"MachS",'|Curl|']
    for i in range(3):
        ax[i].set_xlabel("t/tB")
        ax[i].set_ylabel(ylabs[i])
    ax[0].legend(loc=(1.1,1.1),ncol=2)#bbox_to_anchor = (0.,1.4))
    f.subplots_adjust(wspace=0.5,hspace=0.5)
    f.savefig(fsave,dpi=150)   

def CalcCurl(Vx,Vy,Vz,lev,domain_width_0,domain_dimensions_0):
    wid_cell= float(domain_width_0)/domain_dimensions_0/2**lev
    #finite diff
    dVy_dx= (Vy[:-1,:,:]-Vy[1:,:,:])/wid_cell
    dVz_dx= (Vz[:-1,:,:]-Vz[1:,:,:])/wid_cell
    dVx_dy= (Vx[:,:-1,:]-Vx[:,1:,:])/wid_cell
    dVz_dy= (Vz[:,:-1,:]-Vz[:,1:,:])/wid_cell
    dVx_dz= (Vx[:,:,:-1]-Vx[:,:,1:])/wid_cell
    dVy_dz= (Vy[:,:,:-1]-Vy[:,:,1:])/wid_cell
    #keep just regions that overlap
    dVy_dx= dVy_dx[:,:-1,:-1]
    dVz_dx= dVz_dx[:,:-1,:-1]
    dVx_dy= dVx_dy[:-1,:,:-1]
    dVz_dy= dVz_dy[:-1,:,:-1]
    dVx_dz= dVx_dz[:-1,:-1,:]
    dVy_dz= dVy_dz[:-1,:-1,:]
    #compute curl
    xhat= dVz_dy- dVy_dz
    yhat= -(dVz_dx - dVx_dz)
    zhat= dVy_dx- dVx_dy
    return (xhat**2+yhat**2+zhat**2)**0.5

class CovGrid_vs_AllDat():
    def __init__(self,cgrid,alld,cs_iso,lev,domain_width_0,domain_dimensions_0):
        class RawQs():
            def __init__(self,ytcube,cs_iso,\
                         lev=0,domain_width_0=0,domain_dimensions_0=0,DoCurl=False):
                class AvgQs():
                    def __init__(self,rho,Vx,Vy,Vz,cs_iso):
                        vrms2= (Vx**2+Vy**2+Vz**2)
                        vrms2_MassWt= np.average(vrms2,weights=rho)
                        self.machs= vrms2_MassWt**0.5/cs_iso
                        self.rho= np.average(rho)
                        #         self.Brms= np.average(bx**2+by**2+bz**2)**0.5
                        #         self.Beta= 8*np.pi*self.Rho*cs_iso**2/self.Brms**2
                        #         self.MachA= (self.Beta/2)**0.5*self.MachS
                self.rho= ytcube['density']
                self.vx= ytcube['X-momentum']/self.rho
                self.vy= ytcube['Y-momentum']/self.rho
                self.vz= ytcube['Z-momentum']/self.rho
                self.machs= (self.vx**2+self.vy**2+self.vz**2)**0.5/cs_iso
                self.AvgQs= AvgQs(self.rho,self.vx,self.vy,self.vz,cs_iso)
                if DoCurl: 
                    self.curl=  CalcCurl(self.vx,self.vy,self.vz,\
                                         lev,domain_width_0,domain_dimensions_0)
                    self.AvgQs.curl= np.average(self.curl)
        self.alld= RawQs(alld,cs_iso)
        self.cgrid= RawQs(cgrid,cs_iso,\
                          lev,domain_width_0,domain_dimensions_0,DoCurl=True)
        

#MAIN
spath="./plots/"
f_hdf5= glob.glob("./data.00*.3d.hdf5")
#f_hdf5= ["data.0001.3d.hdf5","data.0010.3d.hdf5"]
#f_hdf5= glob.glob("../../images/stampede/MHDMyMac/drive_MachS-5_MachA-1/maxRes-rBdiv128/data.00*.3d.hdf5")
#f_hdf5.append(f_hdf5[0])
f_hdf5.sort()
lev=0

ts=TimeSeries(f_hdf5)
print "hdf5 files are: ", f_hdf5
#cs= float(raw_input('enter cs in cm/sec: '))
#tB= float(raw_input('enter bondi time in sec: '))
for cnt,f in enumerate(f_hdf5):
    print "loading file: %s" % f
    pf= load(f)
    ts.Time[cnt]= pf.current_time
    cgrid= pf.h.covering_grid(level=lev,\
                          left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions)  #approx from lev0
    alld= pf.h.all_data()  #yt exact
    gamma=1.001
    cs_iso= cs_ad/gamma**0.5
    dat= CovGrid_vs_AllDat(cgrid,alld,\
                           cs_iso,lev,pf.domain_width[0],pf.domain_dimensions[0])
    #make histograms    
    fsave=spath+"hist_"+pf.basename[5:9]+".png"
    #     Bmag=(bx**2+by**2+bz**2)**0.5
    #     beta= 8*np.pi*rho*cs_iso**2/Bmag**2
    #     MachA=(beta/2)**0.5*MachS
    plot_hgrams(fsave,dat)
    #save Qs of Interest
    save_vals(ts,cnt,dat)
fsave=spath+"tseries.png"
plot_TSeries(fsave,ts,tB)
fout=open(spath+"tseries.dump","w")
pdump((ts,cs_iso,tB),fout)
fout.close()
