import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-f_hdf5",action="store",help='hdf5 file to get velocities for')
args = parser.parse_args()
if args.f_hdf5: hdf5 = str(args.f_hdf5)
else: raise ValueError

import yt
import numpy as np
import scipy.io as sio

def PeriodicBox(vx):
    vxn=np.zeros(np.array(vx.shape)+2)
    vxn[1:-1,1:-1,1:-1]= vx
    #copy opposite edge values to ghost zones
    vxn[0,1:-1,1:-1]=vx[-1,:,:]
    vxn[-1,1:-1,1:-1]=vx[0,:,:]
    vxn[1:-1,0,1:-1]=vx[:,-1,:]
    vxn[1:-1,-1,1:-1]=vx[:,0,:]
    vxn[1:-1,1:-1,0]=vx[:,:,-1]
    vxn[1:-1,1:-1,-1]=vx[:,:,0]
    return vxn

ds=yt.load(hdf5)
lev= 0
cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions)
rho = np.array(cube["density"])
vx = np.array(cube["X-momentum"]/rho)
vy = np.array(cube["Y-momentum"]/rho)
vz = np.array(cube["Z-momentum"]/rho)
vxPB=PeriodicBox(vx)
vyPB=PeriodicBox(vy)
vzPB=PeriodicBox(vz)
sio.savemat('./vels_noPB.mat', {'vx':vx,'vy':vy,'vz':vz})
sio.savemat('./vels_PB.mat', {'vx':vxPB,'vy':vyPB,'vz':vzPB})
