import matplotlib
matplotlib.use('Agg')
from yt.mods import *
from string import rstrip
import sys
import pylab as pl

c = 17.320508075688771
sigma = 1.15470054

# attempt to parse our constants
try:
    lines = open('orion2.ini').readlines()
except IOError:
    print 'Error: orion2.ini not found'
    sys.exit()

for lineI, line in enumerate(lines):
    param, sep, vals = map(rstrip,line.partition('='))
    if param == 'radiation.sigma':
        sigma = float(vals)
    if param == 'radiation.c':
        c = float(vals)
    if param == 'radiation.const_planck':
        kappa = float(vals)
    if param == 'radiation.bndry':
        F_inc = float(vals.split()[0])
        F_inc = c * F_inc
    if param == 'radiation.cv_en':
        ex = float(vals)
try:
    sigma, c, kappa, F_inc, ex
except NameError:
    print 'Error: critical parameters not found'

# derive the rest
a = 4.0 * sigma / c
alpha = 40.0 * a

#define a couple of useful fields
def _ThermalEnergy(field, data):
    """ Generate Thermal (gas energy)
    """
    return data["energy-density"] - 0.5*(
        data["X-momentum"]**2.0
        + data["Y-momentum"]**2.0
        + data["Z-momentum"]**2.0 )/data["density"] 
add_field("ThermalEnergy", function=_ThermalEnergy)

def _Temperature(field, data):
    """ For this test problem, the specific heat capacity
    has the form c_v = alpha * Temperature ** 3
    """
    return (4 * data['ThermalEnergy'] / (data['density'] * alpha))**(1.0/(ex + 1.0))
add_field("Temperature", function=_Temperature)

def plot_numerical(fn):

    rad = 'radiation-energy-density'
    temp = 'Temperature'

    pf = load(fn) #load the file

    cen = (pf.domain_left_edge + pf.domain_right_edge) / 2
    pc = PlotCollection(pf,center = cen)

    p = pc.add_ray([pf.domain_left_edge[0],cen[1],cen[2]], \
                   [pf.domain_right_edge[0],cen[1] ,cen[2]], rad)

    u = (c / 4.0) * ( p.data[rad] / F_inc)
    v = (c / 4.0) * ( a * p.data[temp]**4 / F_inc)
    x = na.sqrt(3) * kappa * p.data['x']

    pl.loglog(x, u, 'b')
    pl.loglog(x, v, 'r')
    return (u,v,x)

#plot_numerical('data_time1.hdf5')
(u,v,x)=plot_numerical('data.0001.3d.hdf5')

#analytic solution, see Su & Olsen 1996
x_a = [0.0, 0.1, 0.25, 0.5, 0.75, 1.0]
u_a = [0.23997, 0.17979, 0.11006, 0.04104, 0.01214, 0.00268]
v_a = [0.00170, 0.00110, 0.00055, 0.00012, 0.00003]

pl.plot(x_a, u_a, 'bo', x_a[:-1], v_a, 'ro')

#plot_numerical('data_time2.hdf5')
(u,v,x)=plot_numerical('data.0031.3d.hdf5')

#analytic solution, see Su & Olsen 1996
x_a = [0.0, 0.1, 0.25, 0.5, 0.75, 1.0, 2.5, 5.0, 7.5]
u_a = [0.48556, 0.44289, 0.38544, 0.30500, 0.24062, 0.18922, 0.04167, 0.00238, 0.00008]
v_a = [0.11322, 0.10124, 0.08551, 0.06437, 0.04830, 0.03612, 0.00584, 0.00020, 0.00001]

pl.plot(x_a, u_a, 'bo', x_a, v_a, 'ro')

pl.axis((3e-2, 1e1, 3e-6, 1e0))
pl.legend(('Radiation Energy', 'Gas Energy'),loc='lower left')
ax = pl.gca()
ax.set_title('Nonequilibrium Marshak Wave Test')

pl.savefig('Marshak_Result')
#pass or fail?
def rms(x):
    return na.sqrt(na.mean(x**2))

u_err = abs(na.interp(x_a, x, u) - u_a) / u_a
v_err = abs(na.interp(x_a, x, v) - v_a) / v_a

if rms(u_err) < 0.05 and rms(v_err) < 0.25:
    print 'Pass!'
else:
    print "FAIL!"

