from yt.mods import *
import numpy as na 
from string import rstrip

def _MagneticEnergy(field,data):
    return (data["X-magnfield"]**2 +
            data["Y-magnfield"]**2 +
            data["Z-magnfield"]**2)/2.
add_field("MagneticEnergy", function=_MagneticEnergy, take_log=True,
          units=r"",display_name=r"B^2/8\pi")
#ChomboFieldInfo["MagneticEnergy"]._projected_units=r""

def _xVelocity(field, data):
    """generate x-velocity from x-momentum and density
    
    """
    return data["X-momentum"]/data["density"]
add_field("x-velocity",function=_xVelocity, take_log=False,
          units=r'\rm{cm}/\rm{s}')

def _yVelocity(field,data):
    """generate y-velocity from y-momentum and density
    
    """
    return data["Y-momentum"]/data["density"]
add_field("y-velocity",function=_yVelocity, take_log=False,
          units=r'\rm{cm}/\rm{s}')

def _zVelocity(field,data):
    """generate z-velocity from z-momentum and density
    
    """
    return data["Z-momentum"]/data["density"]
add_field("z-velocity",function=_zVelocity, take_log=False,
          units=r'\rm{cm}/\rm{s}')

def _rVelocity(field,data):
    """ generate the radial velocity from the other components

    """
    return na.sqrt(data["x-velocity"]**2 + data["y-velocity"]**2 + data['z-velocity']**2)
add_field("radial-velocity", function=_rVelocity, take_log=False,
          units=r'\rm{cm}/\rm{s}')

def _ThermalEnergy(field, data):
    """ Generate Thermal (gas energy)
    """
    return data["energy-density"] - 0.5*(
        data["X-momentum"]**2.0
        + data["Y-momentum"]**2.0
        + data["Z-momentum"]**2.0 )/data["density"]
add_field("ThermalEnergy", function=_ThermalEnergy)

def _Pressure(field,data):
    """M{(Gamma-1.0)*e, where e is thermal energy density
    NB: this will need to be modified for radiation
    """
    return (data.pf["Gamma"] - 1.0)*data["ThermalEnergy"]
add_field("Pressure", function=_Pressure, units=r"\rm{dyne}/\rm{cm}^{2}")

def _cs(fields,data):
    return na.sqrt(data.pf["Gamma"]*data["Pressure"]/data["density"])
add_field("cs", function=_cs, units=r"\rm{cm}/\rm{s}")

def _Temperature(field,data):
    return (data["ThermalEnergy"]/(1.650882e12*data["density"]))
add_field("Temperature",function=_Temperature,units=r"\rm{Kelvin}",take_log=False)

def _TotalEnergyDensity(field,data):
    """Total non-magnetic energy, including radiation.
    """
    return data["energy-density"] + data['radiation-energy-density']
add_field("total-energy-density",function=_TotalEnergyDensity,units=r"\rm{ergs}/\rm{cm^3}")

