from yt.mods import *
import matplotlib.colorbar as cb
import sys
import fields

fn = 'data.0006.3d.hdf5'
field = 'radiation-energy-density' 
dir = 2

pf = load(fn)
c = (pf.domain_left_edge + pf.domain_right_edge) / 2
pc = PlotCollection(pf,center = c)
p = pc.add_slice(field,dir)
p.modify['grids']()
p._redraw_image()
p.save_image('Blast_Result')

# this generates a list of plt files from whatever directory you call the script from
list_of_names = glob.glob('data.*.hdf5')
list_of_names.sort()
N = len(list_of_names)

vals = na.zeros(N)
times = na.zeros(N)

# loop through those files and make your plots
i = 0
for fn in list_of_names:
    print fn
    pf = load(fn)
    times[i] = pf.current_time
    data = pf.h.all_data()
    vals[i] = data.quantities["WeightedAverageQuantity"]("total-energy-density", "CellVolume", lazy_reader=True)
    i = i + 1

N = len(vals)
diff = na.zeros(N-1)

for n in na.arange(N-1):
    diff[n] = abs(vals[n+1]-vals[n])/vals[n]

if any(diff > 2.0e5):
    print "FAIL!"
else: print "Pass!" 
