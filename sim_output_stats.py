import argparse
import numpy as np

parser = argparse.ArgumentParser(description="Stats and Image ORION2 data for Node Balance testing")
parser.add_argument("file",type=np.str_,help='data file, looks in current directory')
parser.add_argument("-p",action="store",help='path to data file. Default: ./',default="./")
parser.add_argument("-n",action="store",help='name of output image. Default: ####')
parser.add_argument("-sp",action="store",help='dir save output image to. Default: /global/home/users/kaylanb/downloads/',default="/global/home/users/kaylanb/downloads/")
parser.add_argument("-noplot",action="store_true",help='use this option to NOT create a plot')
args = parser.parse_args()

##
from yt.mods import *
import matplotlib.pyplot as plt
print "args.n = %s, args.sp = %s" % (args.n, args.sp)
pf = load(args.p + args.file)
pf.h.print_stats()
if (args.noplot != True):
	#p = SlicePlot(pf, 'x', "density", center=pf.domain_center)
	p = ProjectionPlot(pf,"x","density", center=pf.domain_center)
	p.annotate_grids() #min_level=0, max_level=2) #alpha=0.25,min_pix=10000) 
	p.annotate_particles()  
	if args.n:
		spath = args.sp + args.n + ".pdf"
	else:
		spath = args.sp +  args.file[5:9] + ".pdf"	
	p.save(spath)
else:
	pass


