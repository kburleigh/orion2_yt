'''cgs UNITS
Same as "orion2_nums_SepSinks.py" but for 8 NOT 16 sinks. See that file for docs
'''

import numpy as np
import fund_const as fc

def RelativeDiff(final,initial):
    return np.abs( (final-initial)/initial )

class PrintMe():
    def printme(self,msg):
#         import pprint
        print msg
        print "="*len(msg)
#         pprint.pprint(self.__dict__)
#         print "dictionary printing"
        for key,val in self.__dict__.iteritems():
            if type(val) == 'float':
                SciNot= "%.4g" % val
                print key, SciNot
            else: print key+":", val

class LboxX1cellsRgravEstimate_4TurbDrive(PrintMe):
    '''cgs units
    Two conditions that specify dimensions of turbulent box:
    1) using 16 sinks (4 in x,y and 2 in z) --> Lbox = 5*SinkSep
    2) SinkSep = const [cm] = (12->50)rB
    Letting cs(T=10K,mu=1)~ 2e3, Msink(for SinkSep=50rB) = Msun, we get
        SinkSep=50rB case: rB= GMsun/(cs*MachS)**2 [cm]
        then SinkSep = 50*GMsun/(cs*MachS)**2 = 12*G* [Msink_unknown]/(cs*MachS)**2
            solve for new Msink_unknown for SinkSep=12rB case...
    One condition that must be met:
    1) tCross >> tB --> Lbox_minimum ~ 5*12*rB ~ 60rB > 50rB  (yep!)
    
    '''
    def __init__(self):
        self.MaxLev= 1.
        self.X1cells= 32
        self.rBH_div_dxmin= 10.
        self.Msink= fc.msun #can pick any Msink want, choose Msun for simplicity
        mu=1.4
        self.gamma= 1.001
        self.cs = (fc.kb*10./mu/fc.mp)**0.5
        self.MachS= 5.
        self.Beta= 10         
        #numbers below entirely depend on selections above
        self.rBH= fc.G*self.Msink/(self.cs*self.MachS)**2 #OM
        self.dxmin= self.rBH/self.rBH_div_dxmin
        self.Lbox= self.dxmin*self.X1cells*2**self.MaxLev
        print "Lbox_div_rBH=",self.Lbox/self.rBH
        #above is what need for inserting sinks, now solve for what need for turb driving
        self.rho0= 1.e-3*self.Msink/self.Lbox**3
        self.MachA= (self.Beta/2)**0.5*self.MachS
        Va= self.MachS*self.cs/self.MachA
        self.rBHA= fc.G*self.Msink/(self.cs*self.MachS)**2/(1.+ self.MachS**2/self.MachA**2)
        self.BCode= (2*self.rho0*self.cs**2/self.Beta)**0.5
        self.X1Grid= [-self.Lbox/2,self.Lbox/2]
    def OtherQuantities(self):
        class Tmp(PrintMe):
            pass
        self.oth=Tmp()
        self.oth.tBH=  self.rBH/(self.MachS*self.cs) #OM
        self.oth.tCross= self.Lbox/(self.MachS*self.cs) #OM
        self.oth.tFirstStep= 0.5*(self.Lbox/self.X1cells)/(self.MachS*self.cs) #OM
        self.oth.small_dn = 1.e-5* self.rho0
        self.oth.small_pr = self.oth.small_dn* self.cs**2/self.gamma
        self.oth.ceil_va = min(1.e2* self.BCode/(self.oth.small_dn)**0.5,0.1*fc.c)

turb=LboxX1cellsRgravEstimate_4TurbDrive()
turb.printme("SinkSep: Turb Drive -- Key params")
turb.OtherQuantities()
turb.oth.printme("SinkSep: Turb Drive -- params follow from Key params")
