'''cgs UNITS
Same as "orion2_nums_SepSinks.py" but for 8 NOT 16 sinks. See that file for docs
'''

import numpy as np
import fund_const as fc

def RelativeDiff(final,initial):
    return np.abs( (final-initial)/initial )

class PrintMe():
    def printme(self,msg):
#         import pprint
        print msg
        print "="*len(msg)
#         pprint.pprint(self.__dict__)
#         print "dictionary printing"
        for key,val in self.__dict__.iteritems():
            if type(val) == 'float':
                SciNot= "%.4g" % val
                print key, SciNot
            else: print key+":", val

def AbsRelDiff(a,b):
    return np.absolute( (a-b)/b)

class LboxX1cellsRgravEstimate_4TurbDrive(PrintMe):
    '''cgs units    
    '''
    def __init__(self):
        '''want reproduce K06 exactly so choose Msink that satifies conditions:'''
        self.PickMsink= fc.msun
        self.SinkSep_div_rB_desired= 32. 
        self.rBH_div_dxMin= 64 #delta x_min <= 1/<this value> *rBHA
        self.X1cells= 512
        self.MaxLev= 4
        self.MachS= 5. #when sinks inserted
        mu=1.4
        self.cs = (fc.kb*10./mu/fc.mp)**0.5
        self.rB= fc.G*self.PickMsink/self.cs**2
        self.rBH= self.rB/(1+ self.MachS**2) #OM
        #NOW SOLVE EVERYTHING ELSE
        self.SinkSep= self.SinkSep_div_rB_desired*self.rBH
        self.Lbox= 4*self.SinkSep  #what get for box of 4x4x4 sinks
        self.dxmin= self.rBH/self.rBH_div_dxMin
        self.dxminCheck= self.Lbox/self.X1cells/2.**self.MaxLev
        if AbsRelDiff(self.dxmin,self.dxminCheck) > 0.001:
            raise ValueError
        #above is what need for inserting sinks, now solve for what need for turb driving
        # self.rho0= 1.e-3*self.PickMsink/(0.5*self.SinkSep)**3
#         self.Beta= 10 #initial beta when start driving
#         self.BCode= (2*self.rho0*self.cs**2/self.Beta)**0.5
        self.gamma= 1.001
        self.X1Grid= [-self.Lbox/2,self.Lbox/2]
    def IndSinkMethodQuantities(self):
        class Tmp(PrintMe):
            pass
        self.onesink= Tmp()
        self.onesink.X1cells= self.X1cells/4
        self.onesink.Lbox= self.Lbox/4.
        self.onesink.X1Grid= [-self.onesink.Lbox/2,self.onesink.Lbox/2]
        self.onesink.tCross= self.onesink.Lbox/(self.MachS*self.cs) #OM
        self.onesink.tFirstStep= 0.5*(self.onesink.Lbox/self.X1cells)/(self.MachS*self.cs) #OM
        self.onesink.tBH=  self.rBH/(self.MachS*self.cs) #OM
        self.onesink.Vs= self.MachS*self.cs
        self.onesink.rho0= 1.e-3*self.PickMsink/self.onesink.Lbox**3
        self.onesink.small_dn = 1.e-5* self.onesink.rho0
        self.onesink.small_pr = self.onesink.small_dn* self.cs**2/ self.gamma

# def calcTimes(Msink,Lbox,cs,MachS):
#     rBH= fc.G*Msink/cs**2/(1+ MachS**2) #OM
#     tBH= rBH/(MachS*cs)
#     tCr= Lbox/(MachS*cs)
#     print "tBH= %g, tCr= %g" % (tBH,tCr)
# 
# try:
#     Msink= float(raw_input('enter Msink in grams: '))
#     Lbox=float(raw_input('enter Lbox in cm: '))
#     cs=float(raw_input('enter cs in cm/s: '))
#     MachS=float(raw_input('enter MachS: '))
# except ValueError:
#     print "Not a number"     
# calcTimes(Msink,Lbox,cs,MachS)


turb=LboxX1cellsRgravEstimate_4TurbDrive()
turb.IndSinkMethodQuantities()
turb.onesink.printme("One Sink per Box")
turb.printme("Global Properties")



