#Star mass, x postion, y position, z position, x-momentum, y-momentum, z-momentum, x-angmom, y-angmom, z-angmom, particle id
#
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def scat3D(x,y,z):
    '''3D plot to sanity check pos of sinks'''
    try:
        Lbox=float(raw_input('enter Lbox in cm: '))
    except ValueError:
        print "Not a number"
    fig,ax = plt.subplots()
    ax = Axes3D(fig)
    ax.scatter(x,y,z)
    ax.set_xlim(-Lbox/2.,Lbox/2.)
    ax.set_ylim(-Lbox/2.,Lbox/2.)
    ax.set_zlim(-Lbox/2.,Lbox/2.)
    plt.show()

def get_xyzSinks(SinkSep_cgs):
    if Nsinks==8:
        xs=np.array([-0.5,0.5])*SinkSep_cgs
        ys=xs.copy()
        zs=xs.copy()
    else: raise ValueError
    return xs,ys,zs

def print_xyz_forCppCode(x,y,z):
    f=open("init.sink_forCppCode.txt","w")
    for i in range(len(x)):
        line="Real Cent%d[3]= {%.3g,%.3g,%.3g};\n" % (i+1,x[i],y[i],z[i])
        f.write(line)
    f.close()

#cgs units!
Nsinks=8
try:
    rBHA=float(raw_input('enter rBHA in cm: '))
    SinkSep_div_rBHA=int(raw_input('enter integer for SinkSep_div_rBHA: '))
    Msink= float(raw_input('enter Msink in grams: '))
except ValueError:
    print "Not a number"
(xs,ys,zs)= get_xyzSinks(SinkSep_div_rBHA*rBHA)

f=open("init.sink","w")
f.write("%d %d\n" % (Nsinks,Nsinks) )
cnt=0
xarr=np.zeros(Nsinks)-1
yarr= xarr.copy()
zarr= xarr.copy()
for x in xs:
    for y in ys:
        for z in zs:
            f.write("%1.2e %1.2e %1.2e %1.2e 0. 0. 0. 0. 0. 0. %d\n" % (Msink,x,y,z,cnt))
            xarr[cnt]=x
            yarr[cnt]=y
            zarr[cnt]=z
            cnt+=1
f.close()
print_xyz_forCppCode(xarr,yarr,zarr)
scat3D(xarr,yarr,zarr)
