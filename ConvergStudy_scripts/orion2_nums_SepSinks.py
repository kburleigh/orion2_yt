'''cgs UNITS
How use: 
1. Calc Lbox,X1cells, and rBHA_estimate for entire simulation from max sink seperation going to use, initial Vs,Va, Msink_estimate
    -Msink_estimate ~ 2.e33 g seems fine
2. Drive turbulence for those sim params for 2 crossing times
3. At end of driving, output final Vs,Va. Want rBHA_estimate = actual rBHA after driving, so adjust Msink so that is so
4. output init.sink file that has Msink such that actual rBHA after driving = rBHA_estimate and sink separation is exactly what wanted
5. output second init.sink file with Msink = 1e-6*Msink found in #4
6. use second init.sink file to insert "mass less sinks" and turn turbulence off, run for 3 tBHA
7. After 3 tBHA, output final Vs,Va. Want rBHA_estimate = actual rBHA after 3 tBHA, so adjust Msink so that is so
8. output init.sink file that has Msink such that actual rBHA after driving = rBHA_estimate and sink separation is exactly what wanted
9. run for 3 more tBHA times this time with "massive" sinks


Notes form meeting 7/9
look at time spend on level 0 vs. finest level, if << then huge box fine
don't worry about changing rGrav after driving b/c should change by << factor 2 and our Mdot vary by factor ~ 2
'''

import numpy as np
import fund_const as fc

def RelativeDiff(final,initial):
    return np.abs( (final-initial)/initial )

class PrintMe():
    def printme(self,msg):
#         import pprint
        print msg
        print "="*len(msg)
#         pprint.pprint(self.__dict__)
#         print "dictionary printing"
        for key,val in self.__dict__.iteritems():
            if type(val) == 'float':
                SciNot= "%.4g" % val
                print key, SciNot
            else: print key+":", val

class LboxX1cellsRgravEstimate_4TurbDrive(PrintMe):
    '''cgs units
    Two conditions that specify dimensions of turbulent box:
    1) using 16 sinks (4 in x,y and 2 in z) --> Lbox = 5*SinkSep
    2) SinkSep = const [cm] = (12->50)rB
    Letting cs(T=10K,mu=1)~ 2e3, Msink(for SinkSep=50rB) = Msun, we get
        SinkSep=50rB case: rB= GMsun/(cs*MachS)**2 [cm]
        then SinkSep = 50*GMsun/(cs*MachS)**2 = 12*G* [Msink_unknown]/(cs*MachS)**2
            solve for new Msink_unknown for SinkSep=12rB case...
    One condition that must be met:
    1) tCross >> tB --> Lbox_minimum ~ 5*12*rB ~ 60rB > 50rB  (yep!)
    
    '''
    def __init__(self):
        SinkSep_div_rB_desired= 50.
        #case: Msink(for SinkSep=50rB) = Msun
        SinkSep_div_rB_ref=50.
        Msink_ref=fc.msun        
        self.Msink= (SinkSep_div_rB_ref/SinkSep_div_rB_desired)*Msink_ref
        #now solve for rB,SinkSep,Lbox given SinkSep_div_rB_desired
        self.cs = (fc.kb*10./1./fc.mp)**0.5
        self.MachS= 5.
        self.MachA= 1. #OM after driving
        self.rB= fc.G*self.Msink/(self.cs*self.MachS)**2 #OM
        self.SinkSep= SinkSep_div_rB_desired*self.rB
        self.Lbox=5*self.SinkSep
        #above is what need for inserting sinks, now solve for what need for turb driving
        self.rho0= 1.e-3*self.Msink/(0.5*self.SinkSep)**3
        self.Beta= 10. #initial beta when start driving
        self.BCode= (2*self.rho0*self.cs**2/self.Beta)**0.5
        self.gamma= 1.001
        self.MaxLevel= 5
        self.MinRes_rBHA_div_dxMin= 32 #delta x_min <= 1/<this value> *rBHA
        self.ntags= 14
        self.X1Grid= [-self.Lbox/2,self.Lbox/2]
        self.X1cells_min= self.Lbox*self.MinRes_rBHA_div_dxMin/self.rB/2.**self.MaxLevel
        try:
            print "X1cells_min= %f" % self.X1cells_min
            self.X1cells_ToUse=int(raw_input('enter X1cells to use: '))
        except ValueError:
            print "Not a number"
        dxMin= self.Lbox/self.X1cells_ToUse/2**self.MaxLevel
        self.MaxRes_rBHA_div_dxMin= self.rB/dxMin
        if self.MaxRes_rBHA_div_dxMin < float(self.MinRes_rBHA_div_dxMin):
            print "not resolving minimum resolution desired!"
            print "MinRes= %f, MaxRes_have= %f" % \
                        (self.MinRes_rBHA_div_dxMin, self.MaxRes_rBHA_div_dxMin)
            raise ValueError
    def OtherQuantities(self):
        class Tmp(PrintMe):
            pass
        self.oth=Tmp()
        self.oth.tB=  self.rB/(self.MachS*self.cs) #OM
        self.oth.tCross= self.Lbox/(self.MachS*self.cs) #OM
        self.oth.tFirstStep= 0.5*(self.Lbox/self.X1cells_ToUse)/(self.MachS*self.cs) #OM
        self.oth.Vs= self.MachS*self.cs
        
        self.oth.small_dn = 1.e-4* self.rho0
        self.oth.small_pr = self.oth.small_dn* self.cs**2/ self.gamma
        self.oth.ceil_va = min(1.e2* self.BCode/(self.oth.small_dn)**0.5,0.1*fc.c)

class MsinkAfterDrive_so_rBHAAFterDrive_eq_rBHAEstimate(PrintMe):
    def __init__(self,drive):    
        try:
            self.VsDivCsAfterDrive=float(raw_input('Enter VsDivCsAfterDrive: '))
            self.VaDivCsAfterDrive=float(raw_input('Enter VaDivCsAfterDrive: '))
        except ValueError:
            print "Not a number"    
        self.VmaxRatio= np.max([1.,self.VsDivCsAfterDrive,self.VaDivCsAfterDrive])/np.max([1.,drive.Vs_div_cs,drive.Va_div_cs])
        self.MsinkAfterDrive= drive.MsinkEstimate*self.VmaxRatio**2
        self.MsinkRatio= self.MsinkAfterDrive/drive.MsinkEstimate
        if (self.MsinkRatio >= 10.) or (self.MsinkRatio <= 1/10.):
            print "WARNING: MsinkAfterDrive changed by factor of 10 or 1/10 to keep conserve rBHA, so density gas for bondi regime could be too high!" 
            raise ValueError
        self.rho_new= 1.e-3*self.MsinkAfterDrive/drive.Lbox**3
        self.rBHA_new= fc.G*self.MsinkAfterDrive/drive.cs**2/np.max([1.,self.VsDivCsAfterDrive,self.VaDivCsAfterDrive])**2
        if RelativeDiff(self.rBHA_new,drive.rBHAEstimate) > 0.001:
            print "rBHA not conserved, rBHA_AfterDriving differs from rBHAEstimate by > 0.1%"
            raise ValueError

        
turb=LboxX1cellsRgravEstimate_4TurbDrive()
turb.printme("SinkSep: Turb Drive -- Key params")
turb.OtherQuantities()
turb.oth.printme("SinkSep: Turb Drive -- params follow from Key params")

# massless= MsinkAfterDrive_so_rBHAAFterDrive_eq_rBHAEstimate(turb)
# massless.printme("SinkSep: Massless Sinks")

# massive= MsinkAfterDrive_so_rBHAAFterDrive_eq_rBHAEstimate(turb)
# massive.printme("SinkSep: Massive Sinks")

