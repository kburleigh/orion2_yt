'''cgs UNITS
Same as "orion2_nums_SepSinks.py" but for 8 NOT 16 sinks. See that file for docs
'''

import numpy as np
import fund_const as fc

def RelativeDiff(final,initial):
    return np.abs( (final-initial)/initial )

class PrintMe():
    def printme(self,msg):
#         import pprint
        print msg
        print "="*len(msg)
#         pprint.pprint(self.__dict__)
#         print "dictionary printing"
        for key,val in self.__dict__.iteritems():
            if type(val) == 'float':
                SciNot= "%.4g" % val
                print key, SciNot
            else: print key+":", val

def AbsRelDiff(a,b):
    return np.absolute( (a-b)/b)

class LboxX1cellsRgravEstimate_4TurbDrive(PrintMe):
    '''cgs units
    Two conditions that specify dimensions of turbulent box:
    1) using 16 sinks (4 in x,y and 2 in z) --> Lbox = 5*SinkSep
    2) SinkSep = const [cm] = (12->50)rB
    Letting cs(T=10K,mu=1)~ 2e3, Msink(for SinkSep=50rB) = Msun, we get
        SinkSep=50rB case: rB= GMsun/(cs*MachS)**2 [cm]
        then SinkSep = 50*GMsun/(cs*MachS)**2 = 12*G* [Msink_unknown]/(cs*MachS)**2
            solve for new Msink_unknown for SinkSep=12rB case...
    One condition that must be met:
    1) tCross >> tB --> Lbox_minimum ~ 5*12*rB ~ 60rB > 50rB  (yep!)
    
    '''
    def __init__(self):
        '''want reproduce K06 exactly so choose Msink that satifies conditions:'''
        self.PickMsink= fc.msun
        self.SinkSep_div_rB_desired= 32. 
        self.rBH_div_dxMin= 64 #delta x_min <= 1/<this value> *rBHA
        self.X1cells= 512
        self.MachS= 5.
        mu=1.4
        self.cs = (fc.kb*10./mu/fc.mp)**0.5
        self.rB= fc.G*self.PickMsink/self.cs**2
        self.rBH= self.rB/(1+ self.MachS**2) #OM
        #NOW SOLVE EVERYTHING ELSE
        self.dxmin= self.rBH/self.rBH_div_dxMin
        self.SinkSep= self.SinkSep_div_rB_desired*self.rBH
        self.Lbox= 4*self.SinkSep  #what get for box of 4x4x4 sinks
        self.LboxCheck= self.dxmin*128*64
        if AbsRelDiff(self.Lbox,self.LboxCheck) > 0.01:
            raise ValueError
        self.MaxLevel=np.log2(self.Lbox/self.dxmin/self.X1cells)
        #above is what need for inserting sinks, now solve for what need for turb driving
        self.rho0= 1.e-3*self.PickMsink/(0.5*self.SinkSep)**3
#         self.Beta= 10 #initial beta when start driving
#         self.BCode= (2*self.rho0*self.cs**2/self.Beta)**0.5
        self.gamma= 1.001
        self.X1Grid= [-self.Lbox/2,self.Lbox/2]
    def OtherQuantities(self):
        class Tmp(PrintMe):
            pass
        self.oth=Tmp()
        self.oth.tBH=  self.rBH/(self.MachS*self.cs) #OM
        self.oth.tCross= self.Lbox/(self.MachS*self.cs) #OM
        self.oth.tFirstStep= 0.5*(self.Lbox/self.X1cells)/(self.MachS*self.cs) #OM
        self.oth.Vs= self.MachS*self.cs
        self.oth.small_dn = 1.e-5* self.rho0
        self.oth.small_pr = self.oth.small_dn* self.cs**2/ self.gamma
#         self.oth.ceil_va = min(1.e2* self.BCode/(self.oth.small_dn)**0.5,0.1*fc.c)

# def calcTimes(Msink,Lbox,cs,MachS):
#     rBH= fc.G*Msink/cs**2/(1+ MachS**2) #OM
#     tBH= rBH/(MachS*cs)
#     tCr= Lbox/(MachS*cs)
#     print "tBH= %g, tCr= %g" % (tBH,tCr)
# 
# try:
#     Msink= float(raw_input('enter Msink in grams: '))
#     Lbox=float(raw_input('enter Lbox in cm: '))
#     cs=float(raw_input('enter cs in cm/s: '))
#     MachS=float(raw_input('enter MachS: '))
# except ValueError:
#     print "Not a number"     
# calcTimes(Msink,Lbox,cs,MachS)


turb=LboxX1cellsRgravEstimate_4TurbDrive()
turb.printme("SinkSep: Turb Drive -- Key params")
turb.OtherQuantities()
turb.oth.printme("SinkSep: Turb Drive -- params follow from Key params")



