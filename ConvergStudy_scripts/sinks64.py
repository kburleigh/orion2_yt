import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-sinksep",action="store",help='Sink Separation:')
parser.add_argument("-msink",action="store",help='sink mass')
parser.add_argument("-Lbox",action="store",help='lenght of box')
args = parser.parse_args()
if args.sinksep and args.msink:
    sinksep= float(args.sinksep)
    msink= float(args.msink)
    Lbox= float(args.Lbox)
else: raise ValueError

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import fund_const as fc
    

def scat3D(x,y,z,Lbox):
    '''3D plot to sanity check pos of sinks'''
    fig,ax = plt.subplots()
    ax = Axes3D(fig)
    ax.scatter(x,y,z)
    ax.set_xlim(-Lbox/2.,Lbox/2.)
    ax.set_ylim(-Lbox/2.,Lbox/2.)
    ax.set_zlim(-Lbox/2.,Lbox/2.)
    plt.show()

def get_xyzSinks(SinkSep):
    if Nsinks==64:
        xs=np.array([-1.5,-0.5,0.5,1.5])*SinkSep
        ys=xs.copy()
        zs=ys.copy()
    else: raise ValueError
    return xs,ys,zs

# def print_xyz_forCppCode(x,y,z):
#     f=open("init.sink_forCppCode.txt","w")
#     for i in range(len(x)):
#         line="Real Cent%d[3]= {%.3g,%.3g,%.3g};\n" % (i+1,x[i],y[i],z[i])
#         f.write(line)
#     f.close()

#cgs units!
Nsinks=64
(xs,ys,zs)= get_xyzSinks(sinksep)

f=open("init.sink","w")
f.write("%d %d\n" % (Nsinks,Nsinks) )
cnt=0
xarr=np.zeros(Nsinks)-1
yarr= xarr.copy()
zarr= xarr.copy()
for x in xs:
    for y in ys:
        for z in zs:
            f.write("%1.2e %1.2e %1.2e %1.2e 0. 0. 0. 0. 0. 0. %d\n" % (msink,x,y,z,cnt))
            xarr[cnt]=x
            yarr[cnt]=y
            zarr[cnt]=z
            cnt+=1
f.close()
# print_xyz_forCppCode(xarr,yarr,zarr)
scat3D(xarr,yarr,zarr,Lbox)
