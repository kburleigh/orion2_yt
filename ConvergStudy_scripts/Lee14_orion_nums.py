'''cgs UNITS'''

import numpy as np
import fund_const as fc

def RelativeDiff(final,initial):
    return np.abs( (final-initial)/initial )

class PrintMe():
    def printme(self,msg):
#         import pprint
        print msg
        print "="*len(msg)
#         pprint.pprint(self.__dict__)
#         print "dictionary printing"
        for key,val in self.__dict__.iteritems():
            if type(val) == 'float':
                SciNot= "%.4g" % val
                print key, SciNot
            else: print key+":", val

class LboxX1cellsRgravEstimate_4TurbDrive(PrintMe):
    '''cgs units'''
    def __init__(self):
        self.Beta=1.
        self.MachS=1.41
        Min_of_rABH_div_dx=33.
        Lbox_div_rB= 50. 
        self.MaxLev=7
        self.X1cells= 64
        self.Msink= fc.msun
        #numbers set by above
        mu=1.4
        self.cs = (fc.kb*10./mu/fc.mp)**0.5
        self.rB= fc.G*self.Msink/self.cs**2
        self.MachA= self.MachS*(self.Beta/2.)**0.5
        self.rABH= fc.G*self.Msink/self.cs**2/(1.+self.MachS**2+(self.MachS/self.MachA)**2)
        self.rho0= 1.e-8*self.Msink/self.rB**3
        self.Lbox= Lbox_div_rB*self.rB
        self.Lbox_res_required=  (1./Min_of_rABH_div_dx)*self.rABH*self.X1cells*2.**self.MaxLev 
        if self.Lbox > self.Lbox_res_required:
            print "Box too large, need make smaller to incr. resolution"
            #raise ValueError
        self.BCode= (2*self.rho0*self.cs**2/self.Beta)**0.5
        self.gamma= 1.001
        self.ntags= 14
        self.X1Grid= [-self.Lbox/2,self.Lbox/2]
    def OtherQuantities(self):
        class Tmp(PrintMe):
            pass
        self.oth=Tmp()
        self.oth.tB=  self.rB/(self.MachS*self.cs) #OM
        self.oth.tCross= self.Lbox/(self.MachS*self.cs) #OM
        self.oth.tFirstStep= 0.5*(self.Lbox/self.X1cells)/(self.MachS*self.cs) #OM
        self.oth.Vs= self.MachS*self.cs
        
        self.oth.small_dn = 1.e-4* self.rho0
        self.oth.small_pr = self.oth.small_dn* self.cs**2/ self.gamma
        self.oth.ceil_va = min(1.e2* self.BCode/(self.oth.small_dn)**0.5,0.1*fc.c)

class MsinkAfterDrive_so_rBHAAFterDrive_eq_rBHAEstimate(PrintMe):
    def __init__(self,drive):    
        try:
            self.VsDivCsAfterDrive=float(raw_input('Enter VsDivCsAfterDrive: '))
            self.VaDivCsAfterDrive=float(raw_input('Enter VaDivCsAfterDrive: '))
        except ValueError:
            print "Not a number"    
        self.VmaxRatio= np.max([1.,self.VsDivCsAfterDrive,self.VaDivCsAfterDrive])/np.max([1.,drive.Vs_div_cs,drive.Va_div_cs])
        self.MsinkAfterDrive= drive.MsinkEstimate*self.VmaxRatio**2
        self.MsinkRatio= self.MsinkAfterDrive/drive.MsinkEstimate
        if (self.MsinkRatio >= 10.) or (self.MsinkRatio <= 1/10.):
            print "WARNING: MsinkAfterDrive changed by factor of 10 or 1/10 to keep conserve rBHA, so density gas for bondi regime could be too high!" 
            raise ValueError
        self.rho_new= 1.e-3*self.MsinkAfterDrive/drive.Lbox**3
        self.rBHA_new= fc.G*self.MsinkAfterDrive/drive.cs**2/np.max([1.,self.VsDivCsAfterDrive,self.VaDivCsAfterDrive])**2
        if RelativeDiff(self.rBHA_new,drive.rBHAEstimate) > 0.001:
            print "rBHA not conserved, rBHA_AfterDriving differs from rBHAEstimate by > 0.1%"
            raise ValueError

        
turb=LboxX1cellsRgravEstimate_4TurbDrive()
turb.printme("SinkSep: Turb Drive -- Key params")
turb.OtherQuantities()
turb.oth.printme("SinkSep: Turb Drive -- params follow from Key params")

# massless= MsinkAfterDrive_so_rBHAAFterDrive_eq_rBHAEstimate(turb)
# massless.printme("SinkSep: Massless Sinks")

# massive= MsinkAfterDrive_so_rBHAAFterDrive_eq_rBHAEstimate(turb)
# massive.printme("SinkSep: Massive Sinks")

