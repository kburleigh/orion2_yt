'''NON-DIMENSIONAL units'''
import numpy as np
import fund_const as fc

def cgs_Lbox(Nsinks,SinkSep_div_rBHA,rBHA):
    if Nsinks == 8:
        return 2.*SinkSep_div_rBHA*rBHA
    if Nsinks == 64:
        return 4.*SinkSep_div_rBHA*rBHA
    else: 
        print "that number of sinks not yet supported"
        raise ValueError


class t0Turb_cgs_SinkSep_ConvStudy():
    '''units: cgs'''
    def __init__(self):
        #driving params
        self.Temp= 10.
        self.Va_div_cs= 5.
        self.Vs_div_cs= 5.
        self.Msink= fc.msun
        self.gamma= 1.001
        #functions of above
        self.cs= (fc.kb*self.Temp/0.6/fc.mp)**0.5       
        self.Vs= self.Vs_div_cs*self.cs
        self.Va= self.Va_div_cs*self.cs 
        self.MachS= self.Vs_div_cs
        self.MachA= self.Vs/self.Va
        self.rBHA= fc.G*fc.msun/max(self.cs,self.Vs,self.Va)**2
        self.Lbox= self.rBHA
        self.X1grid=[-self.Lbox/2.,self.Lbox/2.]
        self.X1cells= 32
        self.rho0= 1.e-3*self.Msink/self.Lbox**3
        self.B_CODE= self.Va*(self.rho0)**0.5
        self.tCr= self.Lbox/max(self.cs,self.Vs,self.Va)
        self.tstep_1st= 0.5*(self.Lbox/self.X1cells)/max(self.cs,self.Vs,self.Va)
        # self.Tb_when_insert=
        self.small_dn = 1.e-12* self.rho0
        self.small_pr = self.small_dn* self.cs**2/ self.gamma
        self.ceil_va = min(1.e2* self.B_CODE/(4.*fc.pi*self.small_dn)**0.5,0.5*fc.c)
        self.floor_Tgas = max(1.e-1 *self.Temp,1.)
    def printme(self):
            import pprint
            print "Warning simulation units are in cgs!, if this is not your case don't use these numbers!"
            pprint.pprint(self.__dict__)

a=t0Turb_cgs_SinkSep_ConvStudy()
a.printme()
