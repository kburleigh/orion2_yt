#given Lbox, sink separation desired, and MachS, get Msink to satisfy Krum06, since email exchange with Mark in Nov. so actually understand what he did in 2006 paper
#

import argparse
import numpy as np   

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-Lbox",action="store",help='length of box in cm')
parser.add_argument("-MachS",action="store",help='mass weight mean MachS when insert sinks')
parser.add_argument("-cs",action="store",help='sound speed cm/s')
parser.add_argument("-MaxLev",action="store",help='Max Refinement Level after insert sinks')
parser.add_argument("-dnfloor",action="store",help='desnity floor for after insert sinks, g/cm^3')
args = parser.parse_args()
if args.Lbox and args.MachS and args.cs and args.MaxLev and args.dnfloor:
    Lbox= float(args.Lbox)
    MachS= float(args.MachS)
    cs= float(args.cs)
    MaxLev= float(args.MaxLev)
    dnfloor= float(args.dnfloor)
else: raise ValueError

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import fund_const as fc

def scat3D(x,y,z,Lbox):
    '''3D plot to sanity check pos of sinks'''
    fig,ax = plt.subplots()
    ax = Axes3D(fig)
    ax.scatter(x,y,z)
    ax.set_xlim(-Lbox/2.,Lbox/2.)
    ax.set_ylim(-Lbox/2.,Lbox/2.)
    ax.set_zlim(-Lbox/2.,Lbox/2.)
    plt.show()

def get_xyzSinks(SinkSep_cgs,Nsinks):
    if Nsinks==64:
        xs=np.array([-1.5,-0.5,0.5,1.5])*SinkSep_cgs
        ys=xs.copy()
        zs=ys.copy()
    else: raise ValueError
    return xs,ys,zs

#MAIN
Nsinks=64   #K06 number
SinkSep_div_rBH= 32. #K06 value
X1cells= 256 #K06 number

#calc from what given above
rBH= Lbox/4/SinkSep_div_rBH
Msink= rBH*(MachS*cs)**2/fc.G
(xs,ys,zs)= get_xyzSinks(SinkSep_div_rBH*rBH,Nsinks)

f=open("init.sink","w")
f.write("%d %d\n" % (Nsinks,Nsinks) )
cnt=0
xarr=np.zeros(Nsinks)-1
yarr= xarr.copy()
zarr= xarr.copy()
for x in xs:
    for y in ys:
        for z in zs:
            f.write("%1.2e %1.2e %1.2e %1.2e 0. 0. 0. 0. 0. 0. %d\n" % (Msink,x,y,z,cnt))
            xarr[cnt]=x
            yarr[cnt]=y
            zarr[cnt]=z
            cnt+=1
f.close()

dxmin = Lbox/X1cells/2**MaxLev #K06 resolves rBH with 64 dxmin cells
if rBH/dxmin < 64: 
    print "need more levels, not resolving rBH with 64 dxmin cells!"
    raise ValueError 
scat3D(xarr,yarr,zarr,Lbox)

print 'PROPERTIES OF SINKS'
print 'Msink, rBH, tBH= %g %g %g' % (Msink,rBH,rBH/MachS/cs)
gamma=1.001
prfloor= dnfloor*cs**2/gamma
print 'dnfloor, prfloor, vaceil= %g %g %g' % (dnfloor,prfloor,1e8)
