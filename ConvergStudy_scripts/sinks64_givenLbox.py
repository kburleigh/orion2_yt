import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-Lbox",action="store",help='lenght of box')
parser.add_argument("-cs",action="store",help='sound speed')
parser.add_argument("-ntag",action="store",help='ntag value used (should be same on each level)')
parser.add_argument("-BaseCells",action="store",help='integer, 512 if base grid is 512^3')
args = parser.parse_args()
if args.Lbox and args.cs:
    Lbox= float(args.Lbox)
    cs= float(args.cs)
    ntag= int(args.ntag)
    BaseCells= int(args.BaseCells)
else: raise ValueError

import numpy as np
import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
import fund_const as fc
    

def scat3D(x,y,z,Lbox):
    '''3D plot to sanity check pos of sinks'''
    fig,ax = plt.subplots()
    ax = Axes3D(fig)
    ax.scatter(x,y,z)
    ax.set_xlim(-Lbox/2.,Lbox/2.)
    ax.set_ylim(-Lbox/2.,Lbox/2.)
    ax.set_zlim(-Lbox/2.,Lbox/2.)
    plt.show()

#MAIN
rB= Lbox/4./32 #Krumholz 2006 requirement
msink= Lbox/4./32.*cs**2/fc.G  #math
SinkSep= 32.*rB  #Krumholz 2006 requirement
Nsinks=64
if Nsinks==64:
    xs=np.array([-1.5,-0.5,0.5,1.5])*SinkSep  #Krumholz 2006 requirement
    ys=xs.copy()
    zs=ys.copy()
else: raise ValueError

#write init.sink file 
f=open("init.sink","w")
f.write("%d %d\n" % (Nsinks,Nsinks) )
cnt=0
xarr=np.zeros(Nsinks)-1
yarr= xarr.copy()
zarr= xarr.copy()
for x in xs:
    for y in ys:
        for z in zs:
            f.write("%1.2e %1.2e %1.2e %1.2e 0. 0. 0. 0. 0. 0. %d\n" % (msink,x,y,z,cnt))
            xarr[cnt]=x
            yarr[cnt]=y
            zarr[cnt]=z
            cnt+=1
f.close()

#write x,y,z beginning and ending cgs locations for PS's zoom-in refinement
#only cells inside these regions are allowed to be refined, make N boxes, one per sink
#solves SMR problem when shock front on boundary of coarse/fine grid
#ie use AMR but don't sacrifice cpu time because AMR refinement can only occur as needed just outside level 1 cells around sink or closer to sink 
f=open("ZoomIn.txt","w")
if ntag == 7: 
    width_Lev1Box= 40. #diamater of 40 cells refined on 1st level around sink location
dxLev0= Lbox/BaseCells #cgs size of base grid cell
dxLev1= dxLev0/2.  
radius_Lev1= width_Lev1Box/2. +4 #buffer of 4 cells around sink box
cnt=0
for x,y,z in zip(xarr,yarr,zarr):
    f.write("zoxbeg[%d]=%1.10e;\n" % (cnt, x-radius_Lev1*dxLev1))
    f.write("zoxend[%d]=%1.10e;\n" % (cnt, x+radius_Lev1*dxLev1))
    f.write("zoybeg[%d]=%1.10e;\n" % (cnt, y-radius_Lev1*dxLev1))
    f.write("zoyend[%d]=%1.10e;\n" % (cnt, y+radius_Lev1*dxLev1))
    f.write("zozbeg[%d]=%1.10e;\n" % (cnt, z-radius_Lev1*dxLev1))
    f.write("zozend[%d]=%1.10e;\n" % (cnt, z+radius_Lev1*dxLev1))
    cnt+=1
f.close()
# scat3D(xarr,yarr,zarr,Lbox)
