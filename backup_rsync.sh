#!/bin/bash
#SBATCH -p normal
#SBATCH -t 12:00:00  
#SBATCH -J rsyncDecay
#SBATCH -o std.o%j    
#SBATCH -e std.e%j    
#SBATCH -N 1                     # Total number of nodes requested (16 cores/node)
#SBATCH -n 1                     # Total number of tasks
#SBATCH --mail-user=kaylanb@berkeley.edu
#SBATCH --mail-type=end    # email me when the job finishes

backup=Burleighetal/convergence/decay/
inc=decay_list.txt
find $backup -type f -name '*.tar.gz' > $inc && rsync -av --files-from=$inc ./ ${ARCHIVER}:${ARCHIVE}/
