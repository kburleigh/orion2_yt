'''script to make init.sink file
1. seperation b/w sink from edges of box is 1/2 seperation b/w sinks (ie periodic BCs)
2. seperation b/w sinks = Lbox / Nsinks == dX
3. position of given sink is -Lbox/2 + dX*(1/2 + index) ;index = np.arange(Nsinks)
so max(index)= Nsinks-1, so max(dX*(1/2 + index)) = 3.5*dX, Lbox = 4.*dX so works!
'''
#Star mass, x postion, y position, z position, x-momentum, y-momentum, z-momentum, x-angmom, y-angmom, z-angmom, particle id
#
import numpy as np

from matplotlib import pyplot
import pylab as py
from mpl_toolkits.mplot3d import Axes3D

Lbox=2.
geom=[4,4,4]
dX=Lbox/geom[0]
mass=5.e2
NSinks= lambda x: x[0]*x[1]*x[2]

def SinkPos(LeftEdge,dX,index):
	return LeftEdge + dX*(0.5+index)

xpt=np.zeros( NSinks(geom) )
ypt=np.copy(xpt)
zpt=np.copy(xpt)

f=open("init.sink","w")
f.write("%d %d\n" % (NSinks(geom),NSinks(geom) ))

cnt=0
for xi in np.arange(geom[0]):
	x=SinkPos(-Lbox/2.,dX,xi)
	for yi in np.arange(geom[1]):
		y=SinkPos(-Lbox/2.,dX,yi)
		for zi in np.arange(geom[2]):
			z=SinkPos(-Lbox/2.,dX,zi)
			cnt+=1
			f.write("%1.2e %1.2e %1.2e %1.2e 0. 0. 0. 0. 0. 0. %d\n" % (mass,x,y,z,cnt-1))
			xpt[cnt-1]=x
			ypt[cnt-1]=y
			zpt[cnt-1]=z
f.close()

def scat3D(Lbox,x,y,z):
	fig = py.figure()
	ax = Axes3D(fig)
	ax.scatter(x,y,z)
	ax.set_xlim(-Lbox/2.,Lbox/2.)
	ax.set_ylim(-Lbox/2.,Lbox/2.)
	ax.set_zlim(-Lbox/2.,Lbox/2.)
	pyplot.show()
