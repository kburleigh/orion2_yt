'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and plot vs. time'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-cs",action="store",help='adiabatic sound speed in cm/sec')
parser.add_argument("-tb",action="store",help='bondi time in sec')
args = parser.parse_args()
if args.cs: cs_ad = float(args.cs)
else: raise ValueError
if args.tb: tB = float(args.tb)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob
from pickle import dump as pdump

class AverageQs():
    def __init__(self,Q,wt):
        self.Avg= np.average(Q)
        self.WtAvg= np.average(Q,weights=wt)
  
class QsOfInterest():
    def __init__(self,rho,vx,vy,vz,bx,by,bz,ed,cs_iso,curl_mag):
#         vrms= (vx**2+vy**2+vz**2)**0.5
        vrms2= (vx**2+vy**2+vz**2)
        vrms2_MassWt= np.average(vrms2,weights=rho)
        self.MachS= vrms2_MassWt**0.5/cs_iso
        self.Rho= np.average(rho)
        self.Brms= np.average(bx**2+by**2+bz**2)**0.5
        self.Beta= 8*np.pi*self.Rho*cs_iso**2/self.Brms**2
        self.MachA= (self.Beta/2)**0.5*self.MachS
        self.Curl= np.average(curl_mag)

def CalcCurl(Vx,Vy,Vz, lev,domain_width_0,domain_dimensions_0):
    wid_cell= float(domain_width_0)/domain_dimensions_0/2**lev
    #finite diff
    dVy_dx= (Vy[:-1,:,:]-Vy[1:,:,:])/wid_cell
    dVz_dx= (Vz[:-1,:,:]-Vz[1:,:,:])/wid_cell
    dVx_dy= (Vx[:,:-1,:]-Vx[:,1:,:])/wid_cell
    dVz_dy= (Vz[:,:-1,:]-Vz[:,1:,:])/wid_cell
    dVx_dz= (Vx[:,:,:-1]-Vx[:,:,1:])/wid_cell
    dVy_dz= (Vy[:,:,:-1]-Vy[:,:,1:])/wid_cell
    #keep just regions that overlap
    dVy_dx= dVy_dx[:,:-1,:-1]
    dVz_dx= dVz_dx[:,:-1,:-1]
    dVx_dy= dVx_dy[:-1,:,:-1]
    dVz_dy= dVz_dy[:-1,:,:-1]
    dVx_dz= dVx_dz[:-1,:-1,:]
    dVy_dz= dVy_dz[:-1,:-1,:]
    #compute curl
    xhat= dVz_dy- dVy_dz
    yhat= -(dVz_dx - dVx_dz)
    zhat= dVy_dx- dVx_dy
    curl_mag= (xhat**2+yhat**2+zhat**2)**0.5
    return curl_mag

class TypesOfAverages():
    def __init__(self,files):
        '''files: hdf5 file list'''
        self.Avg= np.zeros(len(files)) -1
        self.WtAvg=np.zeros(len(files)) -1

class TimeSeries():
    def __init__(self,files):
        '''files: hdf5 file list'''
        self.Time= np.zeros(len(files)) -1
        self.Rho= self.Time.copy()
        self.MachS= self.Time.copy()
        self.Beta= self.Time.copy()
        self.MachA= self.Time.copy()
        self.Curl= self.Time.copy()
        self.Brms= self.Time.copy()

def save_vals(ts,cnt,avgQ):
    ts.Rho[cnt]= avgQ.Rho
    ts.MachS[cnt]=avgQ.MachS
    ts.Brms[cnt]= avgQ.Brms
    ts.Beta[cnt]= avgQ.Beta
    ts.MachA[cnt]= avgQ.MachA
    ts.Curl[cnt]= avgQ.Curl

def plot_hgrams(fsave,rho,MachS,MachA,beta,Bmag,curl_mag,avgQ):
    f,axis=plt.subplots(3,2,figsize=(15,10))
    ax=axis.flatten()
    xlab=['rho','MachS','MachA','Beta','Bmag','|Curl|']
    y=np.log10(rho.flatten())
    (n, bins, patches)=ax[0].hist(y,bins=100,color="b",histtype='stepfilled')
    ax[0].vlines(np.log10(avgQ.Rho),0,1.1*n.max(),colors='k',linestyles='dashed',label="Average Chosen")
    y=MachS.flatten()
    (n, bins, patches)=ax[1].hist(y,bins=100,color="b",histtype='stepfilled')
    ax[1].vlines(avgQ.MachS,0,1.1*n.max(),colors='k',linestyles='dashed')
    y=MachA.flatten()
    (n, bins, patches)=ax[2].hist(y,bins=100,color="b",histtype='stepfilled')
    ax[2].vlines(avgQ.MachA,0,1.1*n.max(),colors='k',linestyles='dashed')
    y=beta.flatten()
    (n, bins, patches)=ax[3].hist(y,bins=100,color="b",histtype='stepfilled')
    ax[3].vlines(avgQ.Beta,0,1.1*n.max(),colors='k',linestyles='dashed')
    y=Bmag.flatten()
    (n, bins, patches)=ax[4].hist(y,bins=100,color="b",histtype='stepfilled')
    y=curl_mag.flatten()
    ax[4].vlines(avgQ.Brms,0,1.1*n.max(),colors='k',linestyles='dashed')
    y=curl_mag.flatten()
    (n, bins, patches)=ax[5].hist(y,bins=100,color="b",histtype='stepfilled')   
    ax[5].vlines(avgQ.Curl,0,1.1*n.max(),colors='k',linestyles='dashed')
    for cnt,axes in enumerate(ax):
        axes.set_xlabel(xlab[cnt])
    ax[0].legend(loc=(1.1,1.1))
    plt.savefig(fsave,dpi=150)

def plot_TSeries(fsave,ts,tB):
    '''ts: returned by TimeSeries
    tB: bondi time in units of simulation'''
    f,axis= plt.subplots(2,3,figsize=(10,5))
    ax=axis.flatten()
    time= (ts.Time - ts.Time[0])/tB
    ax[0].plot(time,ts.Rho,"k*")
    ax[1].plot(time,ts.MachS,"k*")
    ax[2].plot(time,ts.MachA,"k*")
    ax[3].plot(time,ts.Beta,"k*",)
    ax[4].plot(time,ts.Brms,"k*")
    ax[5].plot(time,ts.Curl,"k*")
    ylabs= ['Rho',"MachS","MachA","Beta","Brms",'|Curl|']
    for i in range(6):
        ax[i].set_xlabel("t/tB")
        ax[i].set_ylabel(ylabs[i])
    ax[0].legend(loc=(1.1,1.1),ncol=2)#bbox_to_anchor = (0.,1.4))
    f.subplots_adjust(wspace=0.5,hspace=0.5)
    f.savefig(fsave,dpi=150)   

#MAIN
spath="./plots/"
f_hdf5= glob.glob("./data.00*.3d.hdf5")
#f_hdf5= ["data.0001.3d.hdf5","data.0010.3d.hdf5"]
#f_hdf5= glob.glob("../../images/stampede/MHDMyMac/drive_MachS-5_MachA-1/maxRes-rBdiv128/data.00*.3d.hdf5")
#f_hdf5.append(f_hdf5[0])
f_hdf5.sort()
lev=0

ts=TimeSeries(f_hdf5)
print "hdf5 files are: ", f_hdf5
#cs= float(raw_input('enter cs in cm/sec: '))
#tB= float(raw_input('enter bondi time in sec: '))
for cnt,f in enumerate(f_hdf5):
    print "loading file: %s" % f
    pf= load(f)
    ts.Time[cnt]= pf.current_time
    cube= pf.h.covering_grid(level=lev,\
                          left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions)
    rho = cube["density"]
    vx= cube["X-momentum"]/rho
    vy= cube["Y-momentum"]/rho
    vz= cube["Z-momentum"]/rho
    bx= (4*np.pi)**0.5*cube["X-magnfield"]
    by= (4*np.pi)**0.5*cube["Y-magnfield"]
    bz= (4*np.pi)**0.5*cube["Z-magnfield"]
    ed = cube["energy-density"]
    gamma=1.001
    cs_iso= cs_ad/gamma**0.5
    curl_mag= CalcCurl(vx,vy,vz, lev,pf.domain_width[0],pf.domain_dimensions[0])
    avgQ= QsOfInterest(rho,vx,vy,vz,bx,by,bz,ed,cs_iso,curl_mag)
    #save Qs of Interest
    save_vals(ts,cnt,avgQ)
    #plot distributions of primitive vars with avgQ values for comparison    
    fsave=spath+"hist_"+pf.basename[5:9]+".png"
    MachS=(vx**2+vy**2+vz**2)**0.5/cs_iso
    Bmag=(bx**2+by**2+bz**2)**0.5
    beta= 8*np.pi*rho*cs_iso**2/Bmag**2
    MachA=(beta/2)**0.5*MachS
    plot_hgrams(fsave,rho,MachS,MachA,beta,Bmag,curl_mag,avgQ)
fsave=spath+"tseries.png"
plot_TSeries(fsave,ts,tB)
fout=open(spath+"tseries.dump","w")
pdump((ts,cs_iso,tB),fout)
fout.close()


