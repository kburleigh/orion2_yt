#to submit this as job on nersc machines, ssh to machine, uncomment appropriate 
#line for given machine below, then run with
#visit -cli -nowin -s this_script.py
#
#or can run from visit GUI CLI with:
#Source("/path/to/this/script.py")
print "hello, you have entered the visit_script"
#edison, hopper  **np is # cores
OpenComputeEngine("localhost", ("-l", "qsub/aprun", "-np", "24", "-p", "debug", "-t", "00:30:00"))
save_dir="/global/scratch2/sd/kaylanb/stampede/Burleighetal/insertSinks/base256/Lbox2.0/drive/seed2/beta_1/lev2/vis/box_footpoints"
#carver  **np is # NODES!
#OpenComputeEngine("localhost", ("-l", "qsub/mpirun", "-np", "4", "-p", "debug", "-t", "00:30:00"))
#save_dir="/global/scratch2/sd/kaylanb/testing_visit/carver"

color_min= 1e-6
color_max= 1
ndata=21
sinklevel=2
save_name= "3-panel"
(x,y,z)= (0.25, 0.25, 0.25)
dx= 2./256/2**sinklevel
left= 24*dx

print "opening hdf5 data files"
SetWindowLayout(4)
SetActiveWindow(1)
OpenDatabase("localhost:/global/scratch2/sd/kaylanb/stampede/Burleighetal/insertSinks/base256/Lbox2.0/drive/seed2/beta_1/lev2/data.*.3d.hdf5 database", 0)
DeleteAllPlots()
SetActiveWindow(2)
DeleteAllPlots()
SetActiveWindow(3)
DeleteAllPlots()
#define scalars, vectors
DefineScalarExpression("vmag", "sqrt((<X-momentum>^2+<Y-momentum>^2+<Z-momentum>^2)/density^2)")
DefineVectorExpression("velocity", "momentum/density")

#draw X-proj in W1-3 then redraw y-proj and z-proj in W2-3
print "drawing x-proj on windows 1-3"
for i in range(1,4):
    print "on window %d" % i
    SetActiveWindow(i)
    #x-proj
    AddPlot("Pseudocolor", "density", 1, 1)
    AddOperator("Slice", 1)
    SetActivePlots(0)  ##KEY "density - slice - pseudocolor"
    #x-axis proj
    SliceAtts = SliceAttributes()
    SliceAtts.originType = SliceAtts.Point  # Point, Intercept, Percent, Zone, Node
    SliceAtts.originPoint = (x,y,z)
    SliceAtts.originIntercept = 0
    SliceAtts.originPercent = 0
    SliceAtts.originZone = 0
    SliceAtts.originNode = 0
    SliceAtts.normal = (-1, 0, 0)
    SliceAtts.axisType = SliceAtts.XAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
    SliceAtts.upAxis = (0, 1, 0)
    SliceAtts.project2d = 1
    SliceAtts.interactive = 1
    SliceAtts.flip = 0
    SliceAtts.originZoneDomain = 0
    SliceAtts.originNodeDomain = 0
    SliceAtts.meshName = "Mesh"
    SliceAtts.theta = 90
    SliceAtts.phi = 0
    SetOperatorOptions(SliceAtts, 1)
    #
    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.scaling = PseudocolorAtts.Log  # Linear, Log, Skew
    PseudocolorAtts.skewFactor = 1
    PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 0
    PseudocolorAtts.min = color_min
    PseudocolorAtts.maxFlag = 0
    PseudocolorAtts.max = color_max
    PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
    PseudocolorAtts.colorTableName = "hot"
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.FullyOpaque  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 1
    PseudocolorAtts.opacityVarMin = 0
    PseudocolorAtts.opacityVarMax = 1
    PseudocolorAtts.opacityVarMinFlag = 0
    PseudocolorAtts.opacityVarMaxFlag = 0
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizeVarEnabled = 0
    PseudocolorAtts.pointSizeVar = "default"
    PseudocolorAtts.pointSizePixels = 2
    PseudocolorAtts.lineType = PseudocolorAtts.Line  # Line, Tube, Ribbon
    PseudocolorAtts.lineStyle = PseudocolorAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
    PseudocolorAtts.lineWidth = 0
    PseudocolorAtts.tubeDisplayDensity = 10
    PseudocolorAtts.tubeRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
    PseudocolorAtts.tubeRadiusAbsolute = 0.125
    PseudocolorAtts.tubeRadiusBBox = 0.005
    PseudocolorAtts.varyTubeRadius = 0
    PseudocolorAtts.varyTubeRadiusVariable = ""
    PseudocolorAtts.varyTubeRadiusFactor = 10
    PseudocolorAtts.endPointType = PseudocolorAtts.None  # None, Tails, Heads, Both
    PseudocolorAtts.endPointStyle = PseudocolorAtts.Spheres  # Spheres, Cones
    PseudocolorAtts.endPointRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
    PseudocolorAtts.endPointRadiusAbsolute = 1
    PseudocolorAtts.endPointRadiusBBox = 0.005
    PseudocolorAtts.endPointRatio = 2
    PseudocolorAtts.renderSurfaces = 1
    PseudocolorAtts.renderWireframe = 0
    PseudocolorAtts.renderPoints = 0
    PseudocolorAtts.smoothingLevel = 0
    PseudocolorAtts.legendFlag = 1
    PseudocolorAtts.lightingFlag = 1
    SetPlotOptions(PseudocolorAtts)
    DrawPlots()
    #zoom in
    #SetWindowMode("zoom")
    # Begin spontaneous state
    View2DAtts = View2DAttributes()
    View2DAtts.windowCoords = (-left, left, -left, left)
    View2DAtts.viewportCoords = (0.2, 0.95, 0.2, 0.95)
    View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
    View2DAtts.fullFrameAutoThreshold = 100
    View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
    View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
    View2DAtts.windowValid = 1
    SetView2D(View2DAtts)
    # End spontaneous state
    DrawPlots()
    #
    AddPlot("Vector", "velocity", 1, 1)
    SetActivePlots(1) #"velocity - slice -vector"
    VectorAtts = VectorAttributes()
    VectorAtts.glyphLocation = VectorAtts.UniformInSpace  # AdaptsToMeshResolution, UniformInSpace
    VectorAtts.useStride = 0
    VectorAtts.stride = 1
    VectorAtts.nVectors = 50000
    VectorAtts.lineStyle = VectorAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
    VectorAtts.lineWidth = 0
    VectorAtts.scale = 0.05
    VectorAtts.scaleByMagnitude = 1
    VectorAtts.autoScale = 1
    VectorAtts.headSize = 0.5
    VectorAtts.headOn = 1
    VectorAtts.colorByMag = 0
    VectorAtts.useLegend = 1
    VectorAtts.vectorColor = (0, 0, 0, 255)
    VectorAtts.colorTableName = "Default"
    VectorAtts.invertColorTable = 0
    VectorAtts.vectorOrigin = VectorAtts.Tail  # Head, Middle, Tail
    VectorAtts.minFlag = 0
    VectorAtts.maxFlag = 0
    VectorAtts.limitsMode = VectorAtts.OriginalData  # OriginalData, CurrentPlot
    VectorAtts.min = 0
    VectorAtts.max = 1
    VectorAtts.lineStem = VectorAtts.Line  # Cylinder, Line
    VectorAtts.geometryQuality = VectorAtts.Fast  # Fast, High
    VectorAtts.stemWidth = 0.08
    VectorAtts.origOnly = 1
    VectorAtts.glyphType = VectorAtts.Arrow  # Arrow, Ellipsoid
    SetPlotOptions(VectorAtts)
    #add magnetic field lines
    AddPlot("Streamline", "magnfield", 1, 1)
    SetActivePlots(2)  #magnfield - slice -streamline
    StreamlineAtts = StreamlineAttributes()
    StreamlineAtts.sourceType = StreamlineAtts.SpecifiedBox  # SpecifiedPoint, SpecifiedPointList, SpecifiedLine, SpecifiedCircle, SpecifiedPlane, SpecifiedSphere, SpecifiedBox, Selection
    StreamlineAtts.pointSource = (0, 0, 0)
    StreamlineAtts.lineStart = (0, 0, 0)
    StreamlineAtts.lineEnd = (1, 0, 0)
    StreamlineAtts.planeOrigin = (0, 0, 0)
    StreamlineAtts.planeNormal = (0, 0, 1)
    StreamlineAtts.planeUpAxis = (0, 1, 0)
    StreamlineAtts.radius = 1
    StreamlineAtts.sphereOrigin = (0, 0, 0)
    StreamlineAtts.boxExtents = (-left, left, -left, left, 0, 0)
    StreamlineAtts.useWholeBox = 0
    StreamlineAtts.pointList = (0, 0, 0, 1, 0, 0, 0, 1, 0)
    StreamlineAtts.sampleDensity0 = 5
    StreamlineAtts.sampleDensity1 = 5
    StreamlineAtts.sampleDensity2 = 2
    StreamlineAtts.coloringMethod = StreamlineAtts.Solid  # Solid, ColorBySpeed, ColorByVorticity, ColorByLength, ColorByTime, ColorBySeedPointID, ColorByVariable, ColorByCorrelationDistance
    StreamlineAtts.colorTableName = "Default"
    StreamlineAtts.singleColor = (255, 255, 255, 255)
    StreamlineAtts.legendFlag = 1
    StreamlineAtts.lightingFlag = 1
    StreamlineAtts.integrationDirection = StreamlineAtts.Both  # Forward, Backward, Both
    StreamlineAtts.maxSteps = 1000
    StreamlineAtts.terminateByDistance = 0
    StreamlineAtts.termDistance = 10
    StreamlineAtts.terminateByTime = 0
    StreamlineAtts.termTime = 10
    StreamlineAtts.maxStepLength = 0.1
    StreamlineAtts.limitMaximumTimestep = 0
    StreamlineAtts.maxTimeStep = 0.1
    StreamlineAtts.relTol = 1e-06 #tolerance
    StreamlineAtts.absTolSizeType = StreamlineAtts.FractionOfBBox  # Absolute, FractionOfBBox
    StreamlineAtts.absTolAbsolute = 1e-06 #tolerance
    StreamlineAtts.absTolBBox = 1e-06 #tolerance
    StreamlineAtts.fieldType = StreamlineAtts.Default  # Default, FlashField, M3DC12DField, M3DC13DField, Nek5000Field, NIMRODField
    StreamlineAtts.fieldConstant = 1
    StreamlineAtts.velocitySource = (0, 0, 0)
    StreamlineAtts.integrationType = StreamlineAtts.DormandPrince  # Euler, Leapfrog, DormandPrince, AdamsBashforth, RK4, M3DC12DIntegrator
    StreamlineAtts.parallelizationAlgorithmType = StreamlineAtts.VisItSelects  # LoadOnDemand, ParallelStaticDomains, MasterSlave, VisItSelects
    StreamlineAtts.maxProcessCount = 10
    StreamlineAtts.maxDomainCacheSize = 3
    StreamlineAtts.workGroupSize = 32
    StreamlineAtts.pathlines = 0
    StreamlineAtts.pathlinesOverrideStartingTimeFlag = 0
    StreamlineAtts.pathlinesOverrideStartingTime = 0
    StreamlineAtts.pathlinesPeriod = 0
    StreamlineAtts.pathlinesCMFE = StreamlineAtts.POS_CMFE  # CONN_CMFE, POS_CMFE
    StreamlineAtts.coordinateSystem = StreamlineAtts.AsIs  # AsIs, CylindricalToCartesian, CartesianToCylindrical
    StreamlineAtts.phiScalingFlag = 0
    StreamlineAtts.phiScaling = 1
    StreamlineAtts.coloringVariable = ""
    StreamlineAtts.legendMinFlag = 0
    StreamlineAtts.legendMaxFlag = 0
    StreamlineAtts.legendMin = 0
    StreamlineAtts.legendMax = 1
    StreamlineAtts.displayBegin = 0
    StreamlineAtts.displayEnd = 1
    StreamlineAtts.displayBeginFlag = 0
    StreamlineAtts.displayEndFlag = 0
    StreamlineAtts.referenceTypeForDisplay = StreamlineAtts.Distance  # Distance, Time, Step
    StreamlineAtts.displayMethod = StreamlineAtts.Tubes  # Lines, Tubes, Ribbons
    StreamlineAtts.tubeSizeType = StreamlineAtts.FractionOfBBox  # Absolute, FractionOfBBox
    StreamlineAtts.tubeRadiusAbsolute = 0.125
    StreamlineAtts.tubeRadiusBBox = 0.0003 #tube width
    StreamlineAtts.ribbonWidthSizeType = StreamlineAtts.FractionOfBBox  # Absolute, FractionOfBBox
    StreamlineAtts.ribbonWidthAbsolute = 0.125
    StreamlineAtts.ribbonWidthBBox = 0.01
    StreamlineAtts.lineWidth = 2
    StreamlineAtts.showSeeds = 1
    StreamlineAtts.seedRadiusSizeType = StreamlineAtts.FractionOfBBox  # Absolute, FractionOfBBox
    StreamlineAtts.seedRadiusAbsolute = 1
    StreamlineAtts.seedRadiusBBox = 0.001 #seed width
    StreamlineAtts.showHeads = 1
    StreamlineAtts.headDisplayType = StreamlineAtts.Cone  # Sphere, Cone
    StreamlineAtts.headRadiusSizeType = StreamlineAtts.FractionOfBBox  # Absolute, FractionOfBBox
    StreamlineAtts.headRadiusAbsolute = 0.25
    StreamlineAtts.headRadiusBBox = 0.001 #seed head width
    StreamlineAtts.headHeightRatio = 2
    StreamlineAtts.opacityType = StreamlineAtts.FullyOpaque  # FullyOpaque, Constant, Ramp, VariableRange
    StreamlineAtts.opacityVariable = ""
    StreamlineAtts.opacity = 1
    StreamlineAtts.opacityVarMin = 0
    StreamlineAtts.opacityVarMax = 1
    StreamlineAtts.opacityVarMinFlag = 0
    StreamlineAtts.opacityVarMaxFlag = 0
    StreamlineAtts.tubeDisplayDensity = 10
    StreamlineAtts.geomDisplayQuality = StreamlineAtts.High  # Low, Medium, High, Super
    StreamlineAtts.sampleDistance0 = 10
    StreamlineAtts.sampleDistance1 = 10
    StreamlineAtts.sampleDistance2 = 10
    StreamlineAtts.fillInterior = 1
    StreamlineAtts.randomSamples = 0
    StreamlineAtts.randomSeed = 0
    StreamlineAtts.numberOfRandomSamples = 1
    StreamlineAtts.forceNodeCenteredData = 0
    StreamlineAtts.issueTerminationWarnings = 0
    StreamlineAtts.issueStiffnessWarnings = 0
    StreamlineAtts.issueCriticalPointsWarnings = 0
    StreamlineAtts.criticalPointThreshold = 0.001
    StreamlineAtts.varyTubeRadius = StreamlineAtts.None  # None, Scalar
    StreamlineAtts.varyTubeRadiusFactor = 10
    StreamlineAtts.varyTubeRadiusVariable = ""
    StreamlineAtts.correlationDistanceAngTol = 5
    StreamlineAtts.correlationDistanceMinDistAbsolute = 1
    StreamlineAtts.correlationDistanceMinDistBBox = 0.005
    StreamlineAtts.correlationDistanceMinDistType = StreamlineAtts.FractionOfBBox  # Absolute, FractionOfBBox
    StreamlineAtts.selection = ""
    SetPlotOptions(StreamlineAtts)
    DrawPlots()


#go back and redraw y-proj and z-proj in W2-3
#y-axis
print "redrawing window 2 with y-proj"
SetActiveWindow(2)
SliceAtts = SliceAttributes()
SliceAtts.originType = SliceAtts.Point  # Point, Intercept, Percent, Zone, Node
SliceAtts.originPoint = (x,y,z)
SliceAtts.originIntercept = 0
SliceAtts.originPercent = 0
SliceAtts.originZone = 0
SliceAtts.originNode = 0
SliceAtts.normal = (0, -1, 0)
SliceAtts.axisType = SliceAtts.YAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
SliceAtts.upAxis = (0, 0, 1)
SliceAtts.project2d = 1
SliceAtts.interactive = 1
SliceAtts.flip = 0
SliceAtts.originZoneDomain = 0
SliceAtts.originNodeDomain = 0
SliceAtts.meshName = "Mesh"
SliceAtts.theta = 180
SliceAtts.phi = 0
SetOperatorOptions(SliceAtts, 1)
DrawPlots()
#z-axis
print "redrawing window 2 with y-proj"
SetActiveWindow(3)
SliceAtts = SliceAttributes()
SliceAtts.originType = SliceAtts.Point  # Point, Intercept, Percent, Zone, Node
SliceAtts.originPoint = (x,y,z)
SliceAtts.originIntercept = 0
SliceAtts.originPercent = 0
SliceAtts.originZone = 0
SliceAtts.originNode = 0
SliceAtts.normal = (0, 0, 1)
SliceAtts.axisType = SliceAtts.ZAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
SliceAtts.upAxis = (0, 1, 0)
SliceAtts.project2d = 1
SliceAtts.interactive = 1
SliceAtts.flip = 0
SliceAtts.originZoneDomain = 0
SliceAtts.originNodeDomain = 0
SliceAtts.meshName = "Mesh"
SliceAtts.theta = 0
SliceAtts.phi = 90
SetOperatorOptions(SliceAtts, 1)
DrawPlots()

#save W1-3 as 3-panel window
SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.outputToCurrentDirectory = 0
SaveWindowAtts.outputDirectory = save_dir
SaveWindowAtts.fileName = save_name
SaveWindowAtts.family = 1
SaveWindowAtts.format = SaveWindowAtts.PNG  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1536
SaveWindowAtts.height = 512
SaveWindowAtts.screenCapture = 0
SaveWindowAtts.quality = 100
SaveWindowAtts.progressive = 0
SaveWindowAtts.binary = 0
SaveWindowAtts.stereo = 0
SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
SaveWindowAtts.forceMerge = 0
SaveWindowAtts.resConstraint = SaveWindowAtts.NoConstraint  # NoConstraint, EqualWidthHeight, ScreenProportions
SaveWindowAtts.advancedMultiWindowSave = 1
SaveWindowAtts.subWindowAtts.win1.position = (0, 0)
SaveWindowAtts.subWindowAtts.win1.size = (512, 512)
SaveWindowAtts.subWindowAtts.win1.layer = 0
SaveWindowAtts.subWindowAtts.win1.transparency = 0
SaveWindowAtts.subWindowAtts.win1.omitWindow = 0
SaveWindowAtts.subWindowAtts.win2.position = (513, 0)
SaveWindowAtts.subWindowAtts.win2.size = (512, 512)
SaveWindowAtts.subWindowAtts.win2.layer = 0
SaveWindowAtts.subWindowAtts.win2.transparency = 0
SaveWindowAtts.subWindowAtts.win2.omitWindow = 0
SaveWindowAtts.subWindowAtts.win3.position = (1025, 0)
SaveWindowAtts.subWindowAtts.win3.size = (512, 512)
SaveWindowAtts.subWindowAtts.win3.layer = 0
SaveWindowAtts.subWindowAtts.win3.transparency = 0
SaveWindowAtts.subWindowAtts.win3.omitWindow = 0
SaveWindowAtts.subWindowAtts.win4.position = (0, 0)
SaveWindowAtts.subWindowAtts.win4.size = (128, 128)
SaveWindowAtts.subWindowAtts.win4.layer = 0
SaveWindowAtts.subWindowAtts.win4.transparency = 0
SaveWindowAtts.subWindowAtts.win4.omitWindow = 0
SaveWindowAtts.subWindowAtts.win5.position = (0, 0)
SaveWindowAtts.subWindowAtts.win5.size = (128, 128)
SaveWindowAtts.subWindowAtts.win5.layer = 0
SaveWindowAtts.subWindowAtts.win5.transparency = 0
SaveWindowAtts.subWindowAtts.win5.omitWindow = 0
SaveWindowAtts.subWindowAtts.win6.position = (0, 0)
SaveWindowAtts.subWindowAtts.win6.size = (128, 128)
SaveWindowAtts.subWindowAtts.win6.layer = 0
SaveWindowAtts.subWindowAtts.win6.transparency = 0
SaveWindowAtts.subWindowAtts.win6.omitWindow = 0
SaveWindowAtts.subWindowAtts.win7.position = (0, 0)
SaveWindowAtts.subWindowAtts.win7.size = (128, 128)
SaveWindowAtts.subWindowAtts.win7.layer = 0
SaveWindowAtts.subWindowAtts.win7.transparency = 0
SaveWindowAtts.subWindowAtts.win7.omitWindow = 0
SaveWindowAtts.subWindowAtts.win8.position = (0, 0)
SaveWindowAtts.subWindowAtts.win8.size = (128, 128)
SaveWindowAtts.subWindowAtts.win8.layer = 0
SaveWindowAtts.subWindowAtts.win8.transparency = 0
SaveWindowAtts.subWindowAtts.win8.omitWindow = 0
SaveWindowAtts.subWindowAtts.win9.position = (0, 0)
SaveWindowAtts.subWindowAtts.win9.size = (128, 128)
SaveWindowAtts.subWindowAtts.win9.layer = 0
SaveWindowAtts.subWindowAtts.win9.transparency = 0
SaveWindowAtts.subWindowAtts.win9.omitWindow = 0
SaveWindowAtts.subWindowAtts.win10.position = (0, 0)
SaveWindowAtts.subWindowAtts.win10.size = (128, 128)
SaveWindowAtts.subWindowAtts.win10.layer = 0
SaveWindowAtts.subWindowAtts.win10.transparency = 0
SaveWindowAtts.subWindowAtts.win10.omitWindow = 0
SaveWindowAtts.subWindowAtts.win11.position = (0, 0)
SaveWindowAtts.subWindowAtts.win11.size = (128, 128)
SaveWindowAtts.subWindowAtts.win11.layer = 0
SaveWindowAtts.subWindowAtts.win11.transparency = 0
SaveWindowAtts.subWindowAtts.win11.omitWindow = 0
SaveWindowAtts.subWindowAtts.win12.position = (0, 0)
SaveWindowAtts.subWindowAtts.win12.size = (128, 128)
SaveWindowAtts.subWindowAtts.win12.layer = 0
SaveWindowAtts.subWindowAtts.win12.transparency = 0
SaveWindowAtts.subWindowAtts.win12.omitWindow = 0
SaveWindowAtts.subWindowAtts.win13.position = (0, 0)
SaveWindowAtts.subWindowAtts.win13.size = (128, 128)
SaveWindowAtts.subWindowAtts.win13.layer = 0
SaveWindowAtts.subWindowAtts.win13.transparency = 0
SaveWindowAtts.subWindowAtts.win13.omitWindow = 0
SaveWindowAtts.subWindowAtts.win14.position = (0, 0)
SaveWindowAtts.subWindowAtts.win14.size = (128, 128)
SaveWindowAtts.subWindowAtts.win14.layer = 0
SaveWindowAtts.subWindowAtts.win14.transparency = 0
SaveWindowAtts.subWindowAtts.win14.omitWindow = 0
SaveWindowAtts.subWindowAtts.win15.position = (0, 0)
SaveWindowAtts.subWindowAtts.win15.size = (128, 128)
SaveWindowAtts.subWindowAtts.win15.layer = 0
SaveWindowAtts.subWindowAtts.win15.transparency = 0
SaveWindowAtts.subWindowAtts.win15.omitWindow = 0
SaveWindowAtts.subWindowAtts.win16.position = (0, 0)
SaveWindowAtts.subWindowAtts.win16.size = (128, 128)
SaveWindowAtts.subWindowAtts.win16.layer = 0
SaveWindowAtts.subWindowAtts.win16.transparency = 0
SaveWindowAtts.subWindowAtts.win16.omitWindow = 0
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()
print "saved 3-panel.png for 0th data file"

#Iterate slider over remaining states, redrawing W1-3, as saving each as 3 panel window
print "looping through rest of hdf5 files, making same plot"
for i in range(1,ndata-1):
    print "on hdf5 file #= %d" % i
    SetActiveWindow(1)
    SetTimeSliderState(i)
    DrawPlots()
    SetActiveWindow(2)
    SetTimeSliderState(i)
    DrawPlots()
    SetActiveWindow(3)
    SetTimeSliderState(i)
    DrawPlots()
    SaveWindow()
print "finished"
