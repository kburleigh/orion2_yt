import matplotlib
matplotlib.use('Agg')
import argparse
import yt
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize 
import glob 
import os
import pickle
import scipy.io as io
import sys
from numpy.random import randint

import Sim_constants as sc
import Sim_curl
print "finished importing modules"

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-path",action="store",help='path to hdf5 file(s)')
parser.add_argument("-turb_box",action="store",help='hdf5 file(s)')
parser.add_argument("-pickles",nargs=5,action="store",help='hdf5 file(s)',required=False)
parser.add_argument("-labels",nargs=5,action="store",help='hdf5 file(s)',required=False)
args = parser.parse_args()

BIGG = 1.
bigG=1.

def mdot_const_bondi():
    rho0=1e-2
    mass0=13./32
    cs=1.
    return 4*np.pi*rho0*(bigG*mass0)**2/cs**3

def mdotB_x(rho_x):
    msinks=13./32
    cs=1.
    return 4.*np.pi*(BIGG*msinks)**2*rho_x/cs**3

def mdotBH_x(rho_x,vrms_x):
    msinks=13./32
    cs=1.
    lam= np.exp(1.5)/4
    paren= (lam**2+ np.power(vrms_x/cs,2))/np.power( (1+np.power(vrms_x/cs,2)) ,4)
    return mdotB_x(rho_x)*np.sqrt(paren)

def mdotW_x(rho_x,vort_x):
    msinks=13./32
    cs=1.
    rB= BIGG*msinks/cs**2
    w_x= vort_x*rB/cs
    fstar_x= 1./(1.+np.power(w_x,0.9))
    return mdotB_x(rho_x)*0.34*fstar_x

def mdotCunning_x(beta_x,rho_x,Bch=5.,n=0.42):
    return mdotB_x(rho_x)*np.power( np.power(Bch/beta_x,n/2.)+1. ,-1./n)

def lee_Mbh(vrms_x):
    cs=1.
    lam= np.exp(1.5)/4
    return np.power( 1+np.power(vrms_x/cs,4), 1./3 ) / np.power( 1.+np.power(vrms_x/cs/lam,2), 1./6  )

def mdotPar_x(vrms_x,beta_x,rho_x,A=1.,Bch=19.8,n=1.):
    cs=1.
    mBH= lee_Mbh(vrms_x)
    return A*mdotB_x(rho_x)*np.power(mBH,-2)*np.power( np.power(mBH,n)+np.power(Bch/beta_x,n/2.), -1./n )

def mdotBH_interp_x(vrms_x,rho_x):
    cs=1.
    lam=1.12
    vBH= np.power(cs**2+np.power(vrms_x,2), 0.5)
    phiBH= np.power(1.+np.power(vrms_x,2), 1.5)*np.power(1.+np.power(vrms_x/lam,2), 0.5)/(1.+np.power(vrms_x,4))
    return mdotB_x(rho_x)*phiBH*np.power(vBH/cs,-3)

def mdotPerp_x(vrms_x,beta_x,rho_x,A=1.,Bch=19.8,n=1.):
    cs=1.
    mBH= lee_Mbh(vrms_x)
    perp= mBH*np.power(np.power(mBH,n)+np.power(Bch/beta_x,n/2.), -1./n ) #min b/w this and 1./mBH
    imin= np.where(1./mBH < perp)
    if len(imin[0] > 0): perp[imin]= np.power(mBH,-1)[imin] #insure non empty index
    return A*mdotB_x(rho_x)*perp* np.power(mBH,-2)  

def Linavg(a,b):
    return (a+b)/2. 

def Logavg(a,b):
    return 10**((np.log10(a)+np.log10(b))/2) 

def mdotCombo2_x(a,b):
    return 1./np.sqrt(np.power(a,-2) + np.power(b,-2))

def mdotCombo4_x(a,b,c,d):
    return 1./np.sqrt(np.power(a,-2) + np.power(b,-2)+np.power(c,-2)+np.power(d,-2) ) 


def MdotTurb_div_B_x(mdot):
    return 1./np.sqrt(np.power(mdot.bh,-2) + np.power(mdot.w,-2) ) #already divided by mdot_const_bondi() in Mdot w,bh


def Mdot_perp_w_x(mdot,args):
    return 1./np.sqrt(np.power(mdot.perp,-2) + np.power(mdot.w,-2) )

def Mdot_par_w_x(mdot,args):
    return 1./np.sqrt(np.power(mdot.par,-2) + np.power(mdot.w,-2) )

def Mdot_perp_par_w_x(mdot,args):
    return 1./np.sqrt(np.power(mdot.perp,-2)+np.power(mdot.par,-2) + np.power(mdot.w,-2) )

def Mdot0(rho_inf,msink,mach_3d,cs):
    return 4*np.pi*rho_inf*(BIGG*msink)**2/(mach_3d*cs)**3

def GetLogHist(mdot_norm,bins=100):
    logw=np.log(mdot_norm)
    (logn,logbins,jnk)= plt.hist(logw,bins=bins,align='mid',normed=True,histtype='bar')
    plt.close()
    return logbins, logn

def GetLogCdf(mdot_norm,bins=100):
    logw=np.log(mdot_norm)
    (logn,logbins,jnk)= plt.hist(logw,bins=bins,align='mid',normed=True,cumulative=True,histtype='bar')
    plt.close()
    return logbins, logn


def fit_to_u(u, p1):
  return p1*u**3*np.exp(-1.5*u**1.7)

def add_to_plot(ax,logbins,loghist,c,ls,label):
    bins=(logbins[:-1]+logbins[1:])/2
    bins=np.exp(bins)
    ax.plot(bins,loghist,c=c,ls=ls,label=label)


def hist_data_model(mdot,data,save_path):
    #get distributions first b/c to do so plt.hist() is called, need to close all these before begin plotting
    #theoretical distributions
    class Obj():
        pass
    logbins=Obj()
    loghists=Obj()
    (logbins.turb,loghists.turb)= GetLogHist(mdot.turb.flatten())
    (logbins.bh,loghists.bh)= GetLogHist(mdot.bh.flatten())
    (logbins.w,loghists.w)= GetLogHist(mdot.w.flatten())
    #(logbins.par_mean_pars,loghists.par_mean_pars)= GetLogHist(mdot.par_mean_pars.flatten()) #,bins=logbins.w)
    #(logbins.par_med_pars,loghists.par_med_pars)= GetLogHist(mdot.par_med_pars.flatten()) #,bins=logbins.turb)
    #(logbins.perp_mean_pars,loghists.perp_mean_pars)= GetLogHist(mdot.perp_mean_pars.flatten()) #,bins=logbins.turb)
    #(logbins.perp_med_pars,loghists.perp_med_pars)= GetLogHist(mdot.perp_med_pars.flatten()) #,bins=logbins.turb)
     #data
    (databins,datahist)= GetLogHist(data,bins=15)
    #now can plot, add theory to plot
    fig,ax=plt.subplots(1,1)
    add_to_plot(ax, logbins.bh,loghists.bh,'g','-','bh')
    add_to_plot(ax, logbins.w,loghists.w,'m','-','w')
    #add_to_plot(ax, logbins.par_mean_pars,loghists.par_mean_pars,'b','-',r'$\parallel$ mean pars')
    #add_to_plot(ax, logbins.par_med_pars,loghists.par_med_pars,'b','--',r'$\parallel$ median pars')
    #add_to_plot(ax, logbins.perp_mean_pars,loghists.perp_mean_pars,'y','-',r'$\perp$ mean pars')
    #add_to_plot(ax, logbins.perp_med_pars,loghists.perp_med_pars,'y','--',r'$\perp$ median pars')
    #add data to plot   
    bins=(databins[:-1]+databins[1:])/2
    bins=np.exp(bins)
    left=np.exp(databins[:-1])
    right=np.exp(databins[1:])
    ax.bar(bins,datahist,width=right-left,align='center',fill=False,color='k',label='data')
    #plt.xlim([1e-2,1e2])
    #plt.ylim([0,0.5])
    #     plt.yscale('log')
    ax.set_xscale('log')
    ax.set_xlabel(r'$\dot{M}/\dot{M}_{B}$')
    ax.set_ylabel(r'dp/d ln ($\dot{M}/\dot{M}_{B}$)')
    ax.legend(loc=0)
    plt.savefig(os.path.join(save_path,'dist_test.png'))
    plt.close()
    #save PDFs to pickle
    fout=open(os.path.join(save_path,'data_models_PDFs.pickle'),"w")
    pickle.dump((logbins,loghists,databins,datahist),fout)
    fout.close()
    #ompute CDFs and save to pickle for KS stastic 
    model_bins=Obj()
    model_cdfs=Obj()
    (model_bins.turb,model_cdfs.turb)= GetLogCdf(mdot.turb.flatten())
    (model_bins.bh,model_cdfs.bh)= GetLogCdf(mdot.bh.flatten())
    (model_bins.w,model_cdfs.w)= GetLogCdf(mdot.w.flatten())
    #(model_bins.par_mean_pars,model_cdfs.par_mean_pars)= GetLogCdf(mdot.par_mean_pars.flatten()) 
    #(model_bins.par_med_pars,model_cdfs.par_med_pars)= GetLogCdf(mdot.par_med_pars.flatten()) 
    #(model_bins.perp_mean_pars,model_cdfs.perp_mean_pars)= GetLogCdf(mdot.perp_mean_pars.flatten()) 
    #(model_bins.perp_med_pars,model_cdfs.perp_med_pars)= GetLogCdf(mdot.perp_med_pars.flatten()) 
    (data_bins,data_cdf)= GetLogCdf(data,bins=15)
    fout=open(os.path.join(save_path,'data_models_CDFs.pickle'),"w")
    pickle.dump((model_bins,model_cdfs,data_bins,data_cdf),fout)
    fout.close()
   

def getVrms_weighted3DVrms(vx,vy,vz,rho):
    cs=1.
    vmag2= (vx**2+vy**2+vz**2)
    vrms_mw= np.sqrt(np.average(vmag2,weights=rho))
    return (np.sqrt(vmag2),vrms_mw/cs)

def normalize(x,y):
    coeff= 1./np.trapz(y=y,x=x)
    return coeff*y

def gauss_func(x,mu,std):
	pdf= np.exp(-(x-mu)**2/2/std**2)/std/np.sqrt(2*np.pi)
	return normalize(x,pdf)

def log_normal(x,Svar):
    return np.exp(-(x+Svar/2)**2/2/Svar)/np.sqrt(2*np.pi*Svar)

def get_chi2(o,e):
    chi2= (o-e)**2/e
    return chi2.sum()

def Print_dist_stats(mdot,data):
    print "PRINTING MDOTS:"
    print "data: mean=%.4f,med=%.4f" % (np.mean(data),np.median(data))
    for key in mdot.keys(): print "%s: mean=%.4f,med=%.4f" % (key,np.mean(mdot[key]),np.median(mdot[key]))

def Save_to_matlab_pickle(mdot,data,path):
    phi_mean={}
    phi_mean['data']= np.mean(data)
    for key in mdot.keys(): phi_mean[key]= np.mean(mdot[key])
    #median
    phi_med={}
    phi_med['data']= np.median(data)
    for key in mdot.keys(): phi_med[key]= np.median(mdot[key])

#    np.array([np.mean(data),np.mean(mdot.turb),np.mean(mdot.bh),np.mean(mdot.w),\
#        np.mean(mdot.med_reg_par),np.mean(mdot.med_rms_par),np.mean(mdot.mean_reg_par),np.mean(mdot.mean_rms_par),\
#        np.mean(mdot.med_reg_perp),np.mean(mdot.med_rms_perp),np.mean(mdot.mean_reg_perp),np.mean(mdot.mean_rms_perp),\
#        np.mean(mdot.combo_med_reg),np.mean(mdot.combo_med_rms),np.mean(mdot.combo_mean_reg),np.mean(mdot.combo_mean_rms)])
#    phi_med= np.array([np.median(data),np.median(mdot.turb),np.median(mdot.bh),np.median(mdot.w),\
#        np.median(mdot.med_reg_par),np.median(mdot.med_rms_par),np.median(mdot.mean_reg_par),np.median(mdot.mean_rms_par),\
#        np.median(mdot.med_reg_perp),np.median(mdot.med_rms_perp),np.median(mdot.mean_reg_perp),np.median(mdot.mean_rms_perp),\
#        np.median(mdot.combo_med_reg),np.median(mdot.combo_med_rms),np.median(mdot.combo_mean_reg),np.median(mdot.combo_mean_rms)])
#

    io.savemat(os.path.join(path,'data_and_phis.mat'), \
                {'data':data,'phi_mean':phi_mean,'phi_med':phi_med}) 

    fout=open(os.path.join(path,'data_and_phis.pickle'),"w")
    pickle.dump((data,phi_mean,phi_med),fout)
    fout.close()


def vortPDF(vort_flat,lbox,mach3D,cs,args):
    '''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
    vort_eff= mach3D*cs/lbox
    (w_pdf,w_bins,jnk)= plt.hist(vort_flat/vort_eff,bins=500,normed=True,align='mid',histtype='step',color="k")
    plt.close()
    ##now plot
    f,ax=plt.subplots()
    f.set_size_inches(10.,8.)
    x= (w_bins[1:]+w_bins[:-1])/2
    ax.plot(x,x*w_pdf)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(1e-2,1e3)
    ax.set_ylabel(r'dP($\tilde{\omega}$) / d ln $\tilde{\omega}$',fontsize='xx-large')
    ax.set_xlabel(r'$\tilde{\omega}$',fontsize='xx-large') 
    plt.savefig(os.path.join(args.path,'vort_pdf.png'),dpi=150) 
    plt.close()

def vortPDF_multi(args):
    '''s: ln(rho_x/rho0) array, vmag: velocity magnitude array'''
    f,ax=plt.subplots()
    f.set_size_inches(10.,8.)
    colors= ['k','k','b','b','r','g','y','m','c']
    lines= ['-','--','-','--','-']
    for fpick,label,color,ls in zip(args.pickles,args.labels,colors,lines):        
        fin=open(fpick,'r')
        (w_pdf,w_bins,w_median)= pickle.load(fin)
        fin.close()
        x= (w_bins[1:]+w_bins[:-1])/2
        ax.plot(x,x*w_pdf,c=color,ls=ls,lw=2,label=label+' med=%.1f' % w_median)
    ax.legend(loc=2,fontsize='medium')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(1e-2,1e3)
    ax.set_ylabel(r'dP($\tilde{\omega}$) / d ln $\tilde{\omega}$',fontsize='xx-large')
    ax.set_xlabel(r'$\tilde{\omega}$',fontsize='xx-large') 
    plt.savefig('./vort_pdf_multi.png',dpi=150) 
    plt.close()

def rand_sample_nstars(box_shape,nstars):
    maxint_excl= box_shape[0]
    return (randint(maxint_excl, size=(nstars)),
            randint(maxint_excl, size=(nstars)),
            randint(maxint_excl, size=(nstars)) )


#main
if args.pickles:
    vortPDF_multi(args)
    print "made multi-histogram, exiting"
    sys.exit(0)

cs=1.
#theoretical distributions
lev=0
ds=yt.load(os.path.join(args.path,args.turb_box))
lbox= float(ds.domain_width[0])
cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                  dims=ds.domain_dimensions)

print "gathering 3d data arrays"
#3D arrays
rho_x = np.array(cube["density"])
vx = np.array(cube["X-momentum"])/rho_x
vy = np.array(cube["Y-momentum"])/rho_x
vz = np.array(cube["Z-momentum"])/rho_x
bx = np.array(cube["X-magnfield"])*(4.*np.pi)**0.5
by = np.array(cube["Y-magnfield"])*(4.*np.pi)**0.5
bz = np.array(cube["Z-magnfield"])*(4.*np.pi)**0.5
Bmag= np.sqrt(bx**2+by**2+bz**2)
beta_x= 8*np.pi*rho_x*cs**2/Bmag**2
#del rho_x,bx,by,bz,Bmag
print "3d arrays extracted, now calculating vorticity and single numbers like 3D Mach"
#rho_inf= rho_x.flatten().mean()
(vmag_x,Mach_3D)= getVrms_weighted3DVrms(vx,vy,vz,rho_x)
(wmag_x,div_x)= Sim_curl.getCurl(vx,vy,vz,  ds) #3D array
#
print "---check mdotBH_interp() coded correctly---"
print "mdotBH_interp/mdotB for mach=5= 0.0073, code's value = ",mdotBH_interp_x(5.,1.e-2)/mdot_const_bondi()
#
nstars= 64
ncdfs= 10
mdot=dict(fit=np.zeros((ncdfs,nstars))-1, bh=np.zeros((ncdfs,nstars))-1)
for n in range(ncdfs):
    xyz= rand_sample_nstars(cube.shape,nstars)
    mdot['fit'][n,:]= mdotPar_x(vmag_x[xyz],beta_x[xyz],rho_x[xyz],A=1.,Bch=11.06,n=8.)/mdot_const_bondi()
    mdot['bh'][n,:]= mdotBH_interp_x(vmag_x[xyz],rho_x[xyz])/mdot_const_bondi()
fout=open(os.path.join(args.path,'%d_cdfs_%s_synthetic_stars.pickle'% (ncdfs,nstars)),'w')
pickle.dump(mdot,fout)
fout.close()

#fout=open(os.path.join('./','beta_100_indivSteadyMdots.pickle'),'w')
#pickle.dump(mdot['fit'][0,:],fout)
#fout.close()

print "saved ncdfs, exitting"
sys.exit(0)






#print "3d Mach= ",Mach_3D
mach=5.
cs=1.
vort_eff= mach*cs/lbox
(w_pdf,w_bins,jnk)= plt.hist(wmag_x.flatten()/vort_eff,bins=500,normed=True,align='mid',histtype='step',color="k")
plt.close()
w_median= np.median(wmag_x/vort_eff)
fout=open(os.path.join(args.path,'w_squig.pickle'),'w')
pickle.dump((w_pdf,w_bins,w_median),fout)
fout.close()
print "saved w_pdf,w_bins to pickle file, exitting"
sys.exit(0)

del vx,vy,vz,cube
#wsquig= wmag_x.flatten()*lbox/Mach_3D/cs
#calc mdot at every cells    
#class Objs():
#    pass
mdot= {}
print "calculating 3D Mdot arrays Mbh,Mw,Mturb"
#mdot every cells / const mdotBondi
mdot['fit']= mdotPar_x(vmag_x,beta_x,rho_x,A=1.,Bch=11.06,n=8.)/mdot_const_bondi()

#Print_dist_stats(mdot,data)
#save indiv_mdots and phi_mean,med to matlab
#Save_to_matlab_pickle(mdot,data,args.path)
#make hists
#hist_data_model(mdot,data,args.path)
#print "done"

