'''script that reads in all simulation pickle dumped data
tseries.dump
sink_data.pickle
analytic_mdots.pickle'''

class TimeSeries():
    def __init__(self,files):
        '''files: hdf5 file list'''
        self.Time= np.zeros(len(files)) -1
        self.Rho= self.Time.copy()
        self.MachS= self.Time.copy()
        self.Beta= self.Time.copy()
        self.MachA= self.Time.copy()
        self.Curl= self.Time.copy()
        self.Brms= self.Time.copy()

class SinkData():
    def __init__(self,NSinks,NSinkFiles):
        shape= (NSinkFiles,NSinks)
        self.time= np.zeros(NSinkFiles)-1
        self.mass= np.zeros(shape)-1
        self.x= np.zeros(shape)-1
        self.y= np.zeros(shape)-1
        self.z= np.zeros(shape)-1
        self.Mdot= np.zeros(shape)-1 #Mdot of sink particles
    def get_times(self,f_hdf5):
        for cnt,f in enumerate(f_hdf5):
            print "getting times: loading %s" % f
            pf=load(f)
            self.time[cnt]= pf.current_time
    def get_MassPosMdot(self,f_sink):
        '''f_sink: list of .sink files'''
        for cnt in range(len(f_sink)):
            print "getting sink props: loading %s" % f_sink[cnt]
            mass,x,y,z= np.loadtxt(f_sink[cnt],dtype='float',\
                                   skiprows=1,usecols=(0,1,2,3),unpack=True)
            self.mass[cnt,:]= mass
            self.x[cnt,:]= x
            self.y[cnt,:]= y
            self.z[cnt,:]= z
        #calc Mdot
        if self.time[0].astype('int') == -1:
            print "time not loaded"
            raise ValueError
        else:
            self.Mdot[0,:]=0.
            for cnt in np.arange(1,len(f_sink)):
                deltaM= self.mass[cnt,:] -self.mass[cnt-1,:] 
                deltaT= self.time[cnt]- self.time[cnt-1]
                self.Mdot[cnt,:]= deltaM/deltaT

class Mdots():
    def __init__(self,Msink,rho_inf,cs,f_star,MachS,lee_script_MBH,beta):
        self.K06= Mdots_K06(Msink,rho_inf,cs,f_star,MachS)
        self.Lee14= Mdots_Lee14(Msink,rho_inf,cs,lee_script_MBH,beta)

p_tseries= "./plots/tseries.dump"
p_sdata= "./smr/sinks/plots/sink_data.pickle"
# p_mdot= "./smr/sinks/plots/analytic_mdots.pickle"

fin=open(p_tseries,'r')
(ts,cs_iso,tB)=pickle.load(fin)
fin.close()
fin=open(p_sdata,'r')
sink=pickle.load(fin)
fin.close()




