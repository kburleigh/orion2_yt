import matplotlib
matplotlib.use('Agg')  #stampede, prevents: RuntimeError: Invalid DISPLAY variable 

from yt.mods import *
import matplotlib.pyplot as plt
from glob import glob as gl_glob
from pickle import dump

#Sink stuff
class SinkData():
    def __init__(self,NSinks,NSinkFiles):
        shape= (NSinkFiles,NSinks)
        self.time= np.zeros(NSinkFiles)-1
        self.mass= np.zeros(shape)-1
        self.x= np.zeros(shape)-1
        self.y= np.zeros(shape)-1
        self.z= np.zeros(shape)-1
        self.Mdot= np.zeros(shape)-1 #Mdot of sink particles
    def get_time(self,f_hdf5):
        for cnt,f in enumerate(f_hdf5):
            print "sink time: loading %s" % f
            pf=load(f)
            self.time[cnt]= pf.current_time
    def get_MassPosMdot(self,f_sink):
        '''f_sink: list of .sink files'''
        for cnt in range(len(f_sink)):
            print "sink MassPos: loading %s" % f_sink[cnt]
            mass,x,y,z= np.loadtxt(f_sink[cnt],dtype='float',\
                                   skiprows=1,usecols=(0,1,2,3),unpack=True)
            self.mass[cnt,:]= mass
            self.x[cnt,:]= x
            self.y[cnt,:]= y
            self.z[cnt,:]= z
        #calc Mdot
        if self.time[0].astype('int') == -1:
            print "time not loaded"
            raise ValueError
        else:
            self.Mdot[0,:]=0.
            for cnt in np.arange(1,len(f_sink)):
                deltaM= self.mass[cnt,:] -self.mass[cnt-1,:] 
                deltaT= self.time[cnt]- self.time[cnt-1]
                self.Mdot[cnt,:]= deltaM/deltaT

def PlotMdot4EachSink(sink,tB,f_save):
    '''sink: returned by CalcSinkQuantities()
    av: returned by CalcAvgQuantities
    tB: bondi time in units of simulation'''
    fig,ax= plt.subplots(figsize=(5,5))
    time= (sink.time - sink.time[0])/tB
    for cnt in range(sink.Mdot.shape[1]):
        ax.plot(time,np.log10(sink.Mdot[:,cnt]),label=str(cnt))
    ax.set_xlabel("t/tB")
    ax.set_ylabel("Mdot")
    ax.legend(loc=(0.1,1.01),ncol=2)
#     ax.set_ylim([-0.1*sink.MdotBHA_Avg.max(),1.1*sink.MdotBHA_Avg.max()])
    fig.savefig(f_save,dpi=200)

def PlotAvgMdot(sink,t_arr,tB,f_save):
    '''sink: returned by CalcSinkQuantities()
    av: returned by CalcAvgQuantities
    tB: bondi time in units of simulation'''
    fig,ax= plt.subplots(figsize=(5,5))
    time= (t_arr - t_arr[0])/tB
    ax.plot(time,sink.MdotAvg,"k-*",label="SinkAvg")
#     ax.plot(time,sink.MdotBHA_Avg,"b-*",label="BHA_Avg")
    ax.set_xlabel("t/tB")
    ax.set_ylabel("Mdot")
    ax.legend(loc=(0.1,1.01),ncol=2)
#     ax.set_ylim([-0.1*sink.MdotBHA_Avg.max(),1.1*sink.MdotBHA_Avg.max()])
    fig.savefig(f_save,dpi=200)

#Average Quantities stuff
#yt derived fields
def _xvel(field, data):
    return data['X-momentum']/data['density']
add_field("x-vel", function=_xvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _yvel(field, data):
    return data['Y-momentum']/data['density']
add_field("y-vel", function=_yvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _zvel(field, data):
    return data['Z-momentum']/data['density']
add_field("z-vel", function=_zvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bx(field, data):
    return (4.*fc.pi)**0.5 *data['X-magnfield']
add_field("Bx", function=_Bx, take_log=False,
          units=r'\rm{gauss}')

def _By(field, data):
    return (4.*fc.pi)**0.5 *data['Y-magnfield']
add_field("By", function=_By, take_log=False,
          units=r'\rm{gauss}')

def _Bz(field, data):
    return (4.*fc.pi)**0.5 *data['Z-magnfield']
add_field("Bz", function=_Bz, take_log=False,
          units=r'\rm{gauss}')

##
def _Vmag(field, data):
    return np.sqrt(data['x-vel']**2 + data['y-vel']**2 + data['z-vel']**2)
add_field("Vmag", function=_Vmag, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bmag(field, data):
    return np.sqrt( data['Bx']**2 + data['By']**2 + data['Bz']**2)
add_field("Bmag", function=_Bmag, take_log=False,
          units=r'\rm{gauss}')

def _Va(field, data):
    return data['Bmag'] / np.sqrt(4*np.pi* data['density'])
add_field("Va", function=_Va, take_log=False,
          units=r'\rm{cm/s}')

#hdf5 files: `calc avgerage density, magnetic field strength, velocity, va, and beta
class TypesOfAverages():
    def __init__(self,size):
        '''files: hdf5 file list'''
        self.Avg= np.zeros(size) -1
        self.MassWtAvg=np.zeros(size) -1

class AvgQuantities():
    def __init__(self,f_hdf5):
        '''files: hdf5 file list'''
        size=len(f_hdf5)
        self.Time= np.zeros(size) -1
        self.Dens= TypesOfAverages(size)
        self.Bmag= TypesOfAverages(size)
        self.Vmag= TypesOfAverages(size)
        self.Beta= TypesOfAverages(size)
        self.Va= TypesOfAverages(size)
    def get_AvgQs(self,f_hdf5,LevToCalcOn):
        '''f_hdf5: hdf5 file list
        LevToCalcOn: pf.h.covering_grid(level=LevToCalcOn, ...)'''
        for cnt,f in enumerate(f_hdf5):
            print "avg Q's: loading %s" % f
            pf=load(f)
            dims=pf.domain_dimensions*2**LevToCalcOn
            cube= pf.h.covering_grid(level=LevToCalcOn,\
                                      left_edge=pf.h.domain_left_edge,
                                      dims=dims)
            d = cube["density"]
            b= cube["Bmag"]
            v= cube["Vmag"]
            beta= d*1.**2*8*np.pi/b**2
            va= b/np.sqrt(4*np.pi*d)
            MassWt= d/d.max()
            self.Time[cnt]=pf.current_time
            self.Dens.Avg[cnt]= np.average(d)
            self.Dens.MassWtAvg[cnt]=np.average(d,weights=MassWt)
            self.Bmag.Avg[cnt]= np.average(b)
            self.Bmag.MassWtAvg[cnt]=np.average(b,weights=MassWt)
            self.Vmag.Avg[cnt]= np.average(v)
            self.Vmag.MassWtAvg[cnt]=np.average(v,weights=MassWt)
            self.Beta.Avg[cnt]= np.average(beta)
            self.Beta.MassWtAvg[cnt]=np.average(beta,weights=MassWt)
            self.Va.Avg[cnt]= np.average(va)
            self.Va.MassWtAvg[cnt]=np.average(va,weights=MassWt)

def PlotAvgQs(avgQ,tB,f_save):
    '''tB: bondi time in units of simulation'''
    f,axis= plt.subplots(2,3,figsize=(10,5))
    ax=axis.flatten()
    time= (avgQ.Time - avgQ.Time[0])/tB
    ax[0].plot(time,avgQ.Dens.Avg,"k-*",label="Avg")
    ax[0].plot(time,avgQ.Dens.MassWtAvg,"b-*",label="MassWtAvg")
    ax[1].plot(time,avgQ.Vmag.Avg,"k-*",label="Avg")
    ax[1].plot(time,avgQ.Vmag.MassWtAvg,"b-*",label="MassWtAvg")
    ax[2].plot(time,avgQ.Va.Avg,"k-*",label="Avg")
    ax[2].plot(time,avgQ.Va.MassWtAvg,"b-*",label="MassWtAvg")
    ax[3].plot(time,avgQ.Bmag.Avg,"k-*",label="Avg")
    ax[3].plot(time,avgQ.Bmag.MassWtAvg,"b-*",label="MassWtAvg")
    ax[4].plot(time,avgQ.Beta.Avg,"k-*",label="Avg")
    ax[4].plot(time,avgQ.Beta.MassWtAvg,"b-*",label="MassWtAvg")
    ylabs= ['Density',"RMS V","RMS Va","|B|",r"Magnetic $\beta$",'none']
    for i in range(6):
        ax[i].set_xlabel("t/tB")
        ax[i].set_ylabel(ylabs[i])
        ax[0].legend(loc=(1.1,1.1),ncol=2)#bbox_to_anchor = (0.,1.4))
    f.subplots_adjust(wspace=0.5,hspace=0.5)
    f.savefig(f_save,dpi=200)

#Curl in Vel field
def get_CurlMag(one_file_hdf5,level=0):
    print "computing curl magnitude for file: %s" % one_file_hdf5
    pf=load(one_file_hdf5)
    cube= pf.h.covering_grid(level=level,left_edge=pf.h.domain_left_edge,
                              dims=pf.domain_dimensions*2**level)
    Vx= cube["x-vel"]
    Vy= cube["y-vel"]
    Vz= cube["z-vel"]
    
    wid_cell= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**level
    dVy_dx= (Vy[:-1,:,:]-Vy[1:,:,:])/wid_cell
    dVz_dx= (Vz[:-1,:,:]-Vz[1:,:,:])/wid_cell
    dVx_dy= (Vx[:,:-1,:]-Vx[:,1:,:])/wid_cell
    dVz_dy= (Vz[:,:-1,:]-Vz[:,1:,:])/wid_cell
    dVx_dz= (Vx[:,:,:-1]-Vx[:,:,1:])/wid_cell
    dVy_dz= (Vy[:,:,:-1]-Vy[:,:,1:])/wid_cell
    #keep just regions that overlap
    dVy_dx= dVy_dx[:,:-1,:-1]
    dVz_dx= dVz_dx[:,:-1,:-1]
    dVx_dy= dVx_dy[:-1,:,:-1]
    dVz_dy= dVz_dy[:-1,:,:-1]
    dVx_dz= dVx_dz[:-1,:-1,:]
    dVy_dz= dVy_dz[:-1,:-1,:]
    xhat= dVz_dy- dVy_dz
    yhat= -(dVz_dx - dVx_dz)
    zhat= dVy_dx- dVx_dy
    curl_mag= (xhat**2+yhat**2+zhat**2)**0.5
    return curl_mag

def plot_hist_imshow(curl_mag,one_file_hdf5,level,f_save,out_dir):
    #histogram 
    f,ax=plt.subplots()
    h=ax.hist(curl_mag.flatten(),bins=100)
    ax.set_xlabel("Curl Mag (del cross vel)")
    f.savefig(out_dir+"hist_"+f_save,dpi=200)
    #imshow curl
    fig,ax= plt.subplots()
    index= int(curl_mag.shape[2]/2.)
    ax.imshow(np.log10(np.transpose(curl_mag[:,:,index])))
    ax.set_ylim(ax.get_ylim()[::-1])
    ax.set_title("Curl |Del X v|")
    fig.savefig(out_dir+"curl_"+f_save,dpi=200)
    #compare yt density slice with vel vectors
    pf=load(one_file_hdf5)
    cube= pf.h.covering_grid(level=level,left_edge=pf.h.domain_left_edge,
                              dims=pf.domain_dimensions*2**level)
    dens= cube["density"]
    slc= SlicePlot(pf,"z","density",center=pf.domain_center)
    slc.annotate_velocity()
    slc.save(out_dir+"dens_slc_"+f_save)

     

##
## Main
f_hdf5= gl_glob("./data.*.3d.hdf5")
f_hdf5=np.sort(f_hdf5)
f_sink= gl_glob('./data.*.3d.sink')
f_sink= np.sort(f_sink)
print "HDF5 files are: "
for f in f_hdf5: print f
print "SINK files are: "
for f in f_sink: print f

out_dir= "./plots/"
sink=SinkData(NSinks=16,NSinkFiles=len(f_sink))
sink.get_time(f_hdf5)
sink.get_MassPosMdot(f_sink)
PlotMdot4EachSink(sink,tB=1.,f_save=out_dir+'Mdot.png')
fout = open(out_dir+"Mdot.pickle", 'w')
dump(sink, fout)
fout.close()
print "Sink time,Mass,Pos,Mdot saved to pickle file"

avgQ=AvgQuantities(f_hdf5)
avgQ.get_AvgQs(f_hdf5,LevToCalcOn=0)
PlotAvgQs(avgQ,tB=1.,f_save=out_dir+"AvgQs.png")
fout = open(out_dir+"AvgQs.pickle", 'w')
dump(avgQ, fout)
fout.close()
print "saved AvgQs to pickle file"

#calc curl in velocity field at time insert sinks and at last output time
one_file= f_hdf5[0]
lev=0
curl_mag= get_CurlMag(one_file,level=lev) #get omega at time insert sinks
plot_hist_imshow(curl_mag,one_file,level=lev,f_save="curl_mag.png",out_dir=out_dir)
fout = open(out_dir+"CurlMag.pickle", 'w')
dump(curl_mag, fout)
fout.close()
print "saved CurlMag array to pickle file"


