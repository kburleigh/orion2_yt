'''Do density projection for Box and SinkSeparation for each data dump'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-NSinks",action="store",help='Number of sink particles')
parser.add_argument("-rBH",action="store",help='bondi hoyle radius in cm')
args = parser.parse_args()
if args.NSinks: Nsinks = int(args.NSinks)
else: raise ValueError
if args.rBH: rBH = float(args.rBH)
else: raise ValueError
if NSinks == 1: 
    print "scripts only works for > 1 sink"
    raise ValueError

SId= 8 #good defaults
MaxLev=2
path_hdf5_turb= "../"
path_hdf5_sinks= "./"
spath="./plots/"

f_hdf5_turb= glob.glob(path_hdf5_turb+"data.*.3d.hdf5")
f_hdf5_sinks= glob.glob(path_hdf5_sinks+"data.*.3d.hdf5")
f_sink_sinks= glob.glob(path_hdf5_sinks+"data.*.3d.sink")
f_hdf5_turb.sort()
f_hdf5_sinks.sort()
f_sink_sinks.sort()

#read initial .sink file to get sink positions
mass,x,y,z,sid= np.loadtxt(f_sink_sinks[0],dtype='float',\
                         skiprows=1,usecols=(0,1,2,3,10),unpack=True)
xyz0= (x[SId],y[SId],z[SId])

#make turb density projections
for f in f_hdf5_turb:
    print "loading: ", f
    pf=load(f)
    mass,x,y,z,sid= np.loadtxt(f_sink_sinks[0],dtype='float',\
                         skiprows=1,usecols=(0,1,2,3,10),unpack=True)
    d={"s":100.,"c":"black","alpha":1}
    #box
ProjectionPlot(pf,'z','density',center=pf.domain_center,width=pf.domain_width[0:2],axes_unit='cm')
    for cnt,tmp in enumerate(x):
        slc.annotate_marker((x[cnt],y[cnt],z[cnt]),marker="o",plot_args=d)
    slc.annotate_velocity()
    slc.annotate_grids(min_level=0,max_level=MaxLevel)
    title="%s Projection, Lbox= %.2g, rBH= %.2g, " % ('z',pf.domain_width[0],rBH)
    slc.annotate_title(title)
    dir="z"
    fsave=spath+ "RhoProj_%s_WidIsLbox_%s.png" % ('z',pf.basename[5:9])
    slc.save(name=fsave)

    #future sink location
    ProjectionPlot(pf,'z','density',center=xyz0,width=(60*rBH,60*rBH),axes_unit='cm')
    for cnt,tmp in enumerate(x):
        slc.annotate_marker((x[cnt],y[cnt],z[cnt]),marker="o",plot_args=d)
    slc.annotate_velocity()
    slc.annotate_grids(min_level=0,max_level=MaxLevel)
    title="%s Projection, Lbox= %.2g, rBH= %.2g, " % ('z',pf.domain_width[0],rBH)
    slc.annotate_title(title)
    fsave=spath+ "RhoProj_%s_WidIsSinkSep_60rBH_%s.png" % ('z',pf.basename[5:9])
    slc.save(name=fsave)
    print "sink density proj plotted for: ", f

#make sink density projections
for cnt,tmp in enumerate(f_hdf5_sinks):
    print "loading:", f_hdf5_sinks[cnt],f_sink_sinks[cnt]
    pf=load(f_hdf5_sinks[cnt])
    mass,x,y,z,sid= np.loadtxt(f_sink_sinks[cnt],dtype='float',\
                         skiprows=1,usecols=(0,1,2,3,10),unpack=True)
    d={"s":100.,"c":"black","alpha":1}
    #box
    ProjectionPlot(pf,'z','density',center=pf.domain_center,width=pf.domain_width[0:2],axes_unit='cm')
    for cnt,tmp in enumerate(x):
        slc.annotate_marker((x[cnt],y[cnt],z[cnt]),marker="o",plot_args=d)
    slc.annotate_velocity()
    slc.annotate_grids(min_level=0,max_level=MaxLevel)
    title="%s Projection, Lbox= %.2g, rBH= %.2g, " % ('z',pf.domain_width[0],rBH)
    slc.annotate_title(title)
    dir="z"
    fsave=spath+ "RhoProj_%s_WidIsLbox_%s.png" % ('z',pf.basename[5:9])
    slc.save(name=fsave)

    #future sink location
    ProjectionPlot(pf,'z','density',center=xyz0,width=(60*rBH,60*rBH),axes_unit='cm')
    for cnt,tmp in enumerate(x):
        slc.annotate_marker((x[cnt],y[cnt],z[cnt]),marker="o",plot_args=d)
    slc.annotate_velocity()
    slc.annotate_grids(min_level=0,max_level=MaxLevel)
    title="%s Projection, Lbox= %.2g, rBH= %.2g, " % ('z',pf.domain_width[0],rBH)
    slc.annotate_title(title)
    fsave=spath+ "RhoProj_%s_WidIsSinkSep_60rBH_%s.png" % ('z',pf.basename[5:9])
    slc.save(name=fsave)
    print "sink density proj plotted for: ",f_hdf5_sinks[cnt],f_sink_sinks[cnt]

