#!/bin/bash 
##arg 1: pout.0_*_processed to just sink data
##arg 2: output name 
nlines=`grep $1 -e "coarse time step" |wc -l`
echo "#number of coarse time steps:" > $2
echo "#npts: "$nlines >> $2
