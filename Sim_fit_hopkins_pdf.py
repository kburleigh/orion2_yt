import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-readfile",action="store",help='text file with single column of hdf5 files to include in time average pdf')
args = parser.parse_args()
#
import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq,curve_fit
import scipy.special 
import pickle

#fnames= np.loadtxt(args.readfile,dtype='string') 
#bins= 100. 
#hists= np.zeros([len(fnames),bins])-1
#std2= np.zeros(len(fnames))-1
#check_means=np.zeros(len(fnames)) #check that mean rho does not change
##load data 
#for cnt,f in enumerate(fnames):
#    lev=0
#    print "loading file: %s" % f
#    ds=yt.load(f)
#    lbox= float(ds.domain_width[0])
#    cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
#                      dims=ds.domain_dimensions)
#    ln_p = np.array(cube["density"])
#    check_means[cnt]=ln_p.flatten().mean()
#    ln_p= np.log( ln_p/ln_p.flatten().mean() )
#    std2[cnt]= np.var(ln_p.flatten())
#    if cnt == 0: 
#        file_insertSinks= f
#        (hists[cnt],const_bins,jnk)= plt.hist(ln_p.flatten(),bins=bins,normed=True,align='mid',histtype='step',label=str(cnt))
#    else: (hists[cnt],jnk2,jnk)= plt.hist(ln_p.flatten(),bins=const_bins,normed=True,align='mid',histtype='step',label=str(cnt))
#tol= (check_means.min() - check_means.max())/(check_means.min() + check_means.max())
#if tol > 1e-5: raise ValueError
##bin centers 
#xdata=(const_bins[1:]+const_bins[:-1])/2
##store pdf of each data file and variances in pickle file
#fout=open("avg_pdf.pickle","w")
#pickle.dump((xdata,hists,std2),fout)
#fout.close()
#print "saved avg_pdf.pickle"
fin=open('avg_pdf.pickle','r')
(xdata,hists,std2)= pickle.load(fin)
fin.close

#best fit hopkins13 params
def normalize(x,y):
    coeff= 1./np.trapz(y=y,x=x)
    return coeff*y

def log_normal(x,Svar):
    y= np.exp(-(x+Svar/2)**2/2/Svar)/np.sqrt(2*np.pi*Svar)
    return normalize(x,y)

def hopkins13(T,x,Svar):
    if T > 0.:
        pdf=np.zeros(len(x))-1
        lam=Svar/2/T**2
        for i in range(len(x)):
            u=lam/(1.+T)- x[i]/T
            if u < 0: pdf[i]=0.
            elif np.sqrt(lam*u) > 50: #avoid Nan from exp(HUGE)
                a=lam**0.25/u**0.75/(4*np.pi)**0.5
                b=np.exp(2*np.sqrt(lam*u)-lam-u)
                pdf[i]= a*b
            else: #compute directly
                pdf[i]= scipy.special.iv(1,2*np.sqrt(lam*u))*np.exp(-(lam+u))*np.sqrt(lam/u)
        return normalize(x,pdf)
    else: return log_normal(x,Svar)

def func_to_minimize(T,x,y,Svar):
    if T[0] < 0: return 100.
    else: return hopkins13(T,xdata,tot_var) - pdf

def gauss_func(x,mu,var):
    pdf= np.exp(-(x-mu)**2/2/var)/np.sqrt(2*np.pi*var)
    return normalize(x,pdf)

pdf= np.mean(hists,axis=0)
tot_var= std2.sum()/std2.shape[0]
#fit gaussian for reference
opt, cov = curve_fit(gauss_func, xdata, pdf)
print "variance from data= %f, variance from gaussian fit= %f" % (tot_var,opt[1])
#fit hopkins 2013 model
T=0.1  # Initial guess for the parameters
ntrials=10
Tvals=np.zeros(ntrials)-1
for i in range(10):
    res = leastsq(func=func_to_minimize, x0=T,args=(xdata,pdf,tot_var),full_output=True,ftol=1e-3)
    Tvals[i]= res[0][0]
    print "i=%d, T= %.3g" % (i,res[0][0])
delta_rms= ( np.power(pdf-hopkins13(Tvals.mean(),xdata,tot_var), 2).sum() )**0.5
print "using variance from data= %f, hopkins13 T= %.3g +/- %.3g, delta_rms for average T= %.3g" % (tot_var,Tvals.mean(),np.std(Tvals),delta_rms)

plt.plot(xdata,pdf,'k+',label='data')
plt.plot(xdata,gauss_func(xdata,opt[0],opt[1]),'k-',label='gauss')
plt.plot(xdata,hopkins13(res[0][0],xdata,tot_var),'b-',label='least squares')
plt.legend()
plt.yscale('log')
plt.savefig('avg_pdf.png',dpi=150)
