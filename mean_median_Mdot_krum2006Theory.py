import numpy as np
import matplotlib.pyplot as plt

user_rBL= [0.186,0.459,0.200,0.203,1.471]
user_rBL=np.array(user_rBL)

phi_v= 0.93
phi_w= 1.25
phi_u= 0.95
def mean_mdot(rB_div_L,mach):
    return 3/phi_u**3*(np.log(2*phi_u*mach)-1.)*np.power((1.+100*rB_div_L/mach),-0.68)

def median_mdot(rB_div_L,mach):
    inner= 10/phi_v**6*np.power((10*phi_w*rB_div_L/mach**2.3),1.8)
    return phi_v**-3/(1.+mach**2/4)**0.5*np.power((1.+inner),-0.5)

user_mean= mean_mdot(user_rBL,5.)
user_median= median_mdot(user_rBL,5.)
for i in range(len(user_rBL)):
	print "rB/L= %.3f, K06 mean= %.2f med= %.2f" % (user_rBL[i],user_mean[i],user_median[i])

rBL=np.linspace(-5,3,num=1000)
rBL=np.power(rBL,10)
fig,axis=plt.subplots(2,1,sharex=True,figsize=(5,10))
fig.subplots_adjust(hspace=0)
ax=axis.flatten()
ax[0].plot(rBL,mean_mdot(rBL,10),"k-",label="mach 10")
ax[0].plot(rBL,mean_mdot(rBL,5),"b-",label="mach 5")
ax[0].plot(rBL,mean_mdot(rBL,3),"r-",label="mach 3")
ax[0].set_ylabel(r'$\phi_{mean}$')
# ax[0].set_xlabel("rB / L")
ax[0].set_ylim(0.01,20)
ax[0].legend(loc=3)
ax[1].plot(rBL,median_mdot(rBL,10),"k-",label="mach 10")
ax[1].plot(rBL,median_mdot(rBL,5),"b-",label="mach 5")
ax[1].plot(rBL,median_mdot(rBL,3),"r-",label="mach 3")
ax[1].set_ylabel(r'$\phi_{median}$')
ax[1].set_xlabel("rB / L")
ax[1].set_ylim(0.01,2)
ax[1].legend(loc=3)
for myax in ax:
    myax.set_xscale('log')
    myax.set_yscale('log')
    myax.set_xlim(1e-5,30)
plt.savefig("mean_median.png")


