import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-f_hdf5",action="store",help='hdf5 file to get velocities for')
parser.add_argument("-cs",type=float,action="store",help='hdf5 file to get velocities for')
parser.add_argument("-msink",type=float,action="store",help='msink required for K06 setup')
#optional
parser.add_argument("-Geq1",action="store",help='set to 1 if G = 1 units')
parser.add_argument("-ytVersion2",action="store",help='set to 1 if only have yt version less than 3.0')
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
if args.ytVersion2: from yt.mods import *
else: import yt
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

import Sim_constants as sc
import Sim_curl

if args.Geq1: BIGG = 1.
else: BIGG = sc.G
class PhiVals():
    def __init__(self,phimean,phimed):
        self.phimean= phimean
        self.phimed= phimed

def MdotBH_x(rho_x,MachS_x,msinks,cs,Mach3d):
    lam= np.exp(1.5)/4
    paren= (lam**2+ MachS_x**2)/(1+MachS_x**2)**4
    #paren= (lam**2+ Mach3d**2)/(1+Mach3d**2)**4
    return 4*np.pi*rho_x*(BIGG*msinks)**2/cs**3*np.sqrt(paren)

def MdotW_x(rho_x,vort_x,cs,msinks):
    rB= BIGG*msinks/cs**2
    w_x= vort_x*rB/cs
    fstar_x= 1./(1.+w_x**0.9)
    return 4*np.pi*rho_x*(BIGG*msinks)**2/cs**3 *0.34*fstar_x

def MdotTurb_x(MdotW,MdotBH):
    return 1./np.sqrt(MdotBH**-2 + MdotW**-2)

def Mdot0(rho_inf,msink,mach_3d,cs):
    return 4*np.pi*rho_inf*(BIGG*msink)**2/(mach_3d*cs)**3

def GetLogHist(mdot_norm,bins=100):
    logw=np.log(mdot_norm)
    (logn,logbins,jnk)= plt.hist(logw,bins=bins,align='mid',normed=True,histtype='bar')
    plt.close()
    return logbins, logn

def fit_to_u(u, p1):
  return p1*u**3*np.exp(-1.5*u**1.7)


def PlotFig6(fsave,vrms_x,mach3d,args):
    #plot dp/d ln u for data
    (logbins,loghist)= GetLogHist((vrms_x/mach3d/args.cs).flatten())
    bins=(logbins[:-1]+logbins[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghist,'k*',label='u, Mach 5')
    #plot fit to data using krumholz 2006's functional form "fit_to_u" function
    guess= fit_to_u(loghist,1.).max()/loghist.max()
    p1 = curve_fit(fit_to_u, bins, loghist,p0=(guess))
    fit= fit_to_u(loghist,p1[0][0])
    plt.plot(bins,fit,'b-',label='fit')
    #finish annotating
    plt.xlim([1e-3,1e1])
    plt.ylim([5e-7,3])
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'u')
    plt.ylabel('dp(u) / d ln u')
    plt.savefig(fsave)

def PlotFig7(fsave,wmag_x,Lbox,mach3d,args):
    #plot dp/d ln u for data
    (logbins,loghist)= GetLogHist(wmag_x.flatten()*Lbox/mach3d/args.cs)
    bins=(logbins[:-1]+logbins[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghist,'k*',label=r'$\tilde{w}$, Mach 5')
    #finish annotating
    plt.xlim([1e-2,1e3])
    plt.ylim([1e-6,1])
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\tilde{w}$')
    plt.ylabel(r'dp($\tilde{w}$) / d ln $\tilde{w}$')
    plt.savefig(fsave)


def PlotFig4(fname,mdot):
    class Obj():
        pass
    logbins=Obj()
    loghists=Obj()
    (logbins.turb,loghists.turb)= GetLogHist(mdot.turb.flatten()/mdot.o)
    (logbins.bh,loghists.bh)= GetLogHist(mdot.bh.flatten()/mdot.o,bins=logbins.turb)
    (logbins.w,loghists.w)= GetLogHist(mdot.w.flatten()/mdot.o,bins=logbins.turb)
    bins=(logbins.turb[:-1]+logbins.turb[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghists.turb,'k-',label='turb')
    plt.plot(bins,loghists.bh,'k--',label='bh')
    plt.plot(bins,loghists.w,'k:',label='w')
    plt.xlim([1e-2,1e2])
    plt.ylim([0,0.5])
#     plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\dot{M}/\dot{M}_0$')
    plt.ylabel(r'dp/d ln (M/M0)')
    plt.legend(loc=0)
    plt.savefig(fname)
    plt.close()

#curl
lev=0
if args.ytVersion2: 
   ds=load(args.f_hdf5)
   lbox= float(ds.domain_width[0])
   cube= ds.h.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions) 
else:
    ds=yt.load(args.f_hdf5)
    lbox= float(ds.domain_width[0])
    cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions)
#3D arrays
rho_x = np.array(cube["density"])
vx = np.array(cube["X-momentum"])/rho_x
vy = np.array(cube["Y-momentum"])/rho_x
vz = np.array(cube["Z-momentum"])/rho_x
wmag_x= Sim_curl.getCurl(vx,vy,vz,  ds) #3D array
print "shapes of vx,wmag= ",vx.shape,wmag_x.shape
vrms2_x= (vx**2+vy**2+vz**2)
#free memory don't need anymore
del vx,vy,vz,cube
#single number paramaters
vrms2_MassWt= np.average(vrms2_x.flatten(),weights=rho_x.flatten())
Mach_3D= vrms2_MassWt**0.5/args.cs
rho_inf= rho_x.flatten().mean()

#have data to plot krumholz 2006 Fig. 6
name="fig6_K06_"+ds.basename[5:9]+".png"
PlotFig6(name,np.sqrt(vrms2_x),Mach_3D,args)
name="fig7_K06_"+ds.basename[5:9]+".png"
PlotFig7(name,wmag_x,lbox,Mach_3D,args)
med_u= np.median( np.sqrt(vrms2_x.flatten()) )/Mach_3D/args.cs
med_w= np.median( wmag_x.flatten() )*lbox/Mach_3D/args.cs
#if args.ytVersion2:
#    pass
#else: 
#    ad= ds.all_data()
#    rho_ad = np.array(ad["density"])
#    vx_ad = np.array(ad["X-momentum"])/rho_ad
#    vy_ad = np.array(ad["Y-momentum"])/rho_ad
#    vz_ad = np.array(ad["Z-momentum"])/rho_ad
#    med_u_ad= np.median(np.sqrt( vx_ad**2+vy_ad**2+vz_ad**2  ))/Mach_3D/args.cs
#    print "median using all_data() func is v/M0/cs= %.2f" % med_u_ad
wsquig= wmag_x*lbox/Mach_3D/args.cs
phi_w= np.median(wsquig)/10
phi_v= np.median(np.sqrt(vrms2_x))/Mach_3D/args.cs
print "med_u= %.2f, phi_v= %.2f, med_u= %.2f, phi_w= %.2f" % (med_u,phi_v,med_w,phi_w)
class Objs():
    pass
mdot= Objs()
##3D mdot arrays
mdot.bh= MdotBH_x(rho_x,np.sqrt(vrms2_x)/args.cs,args.msink,args.cs,Mach_3D)
mdot.w= MdotW_x(rho_x,wmag_x,args.cs,args.msink)
mdot.turb= MdotTurb_x(mdot.w,mdot.bh)
##free non-mdot_x arrays
del rho_x,vrms2_x,wmag_x
##0th order single number  mdot
mdot.o= Mdot0(rho_inf,args.msink,Mach_3D,args.cs)
#
PlotFig4('fig4_K06.png',mdot)
#
mod_med= np.median(mdot.turb.flatten())/mdot.o
mod_mean= np.mean(mdot.turb.flatten())/mdot.o
print "Model Prediction: phi_mean= %.4f, phi_med = %.4f" % (mod_mean,mod_med)
rB= BIGG*args.msink/args.cs**2
print "rB/L= %.4f, Log10 rB/L = %.4f" % (rB/lbox,np.log10(rB/lbox))
