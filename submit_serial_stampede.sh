#!/bin/bash
#SBATCH -p normal
#SBATCH -t 00:10:00  
#SBATCH -J python
#SBATCH -o std.o%j    
#SBATCH -e std.e%j    
#SBATCH -N 1                     # Total number of nodes requested (16 cores/node)
#SBATCH -n 1                     # Total number of tasks
#SBATCH --mail-user=kaylanb@berkeley.edu
#SBATCH --mail-type=end    # email me when the job finishes

export PATH=/home1/03048/kaylanb/anaconda/bin:$PATH
python perturbation.py --kmin=1 --kmax=2 --size=256 --alpha=2 --seed=2 --f_solenoidal=1
