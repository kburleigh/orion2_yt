from yt.mods import *
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import AxesGrid

import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import my_fund_const as fc
import init_cond as mic
import deriv_fields 

pars= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
ic = mic.calcIC(pars)
ic.prout()

path = "/clusterfs/henyey/kaylanb/Test_Prob/Turb/bondiTurb/quick/driving_t0_only/MHD/lev2/"
file = "data.0005.3d.hdf5"
pf = load(path+file) # load data

# ylab = (ic.Lbox/1.5e13, "au")
# zlab = (ic.Lbox/1.5e13, "au")
# p = SlicePlot(pf, 'x', "density", center=pf.domain_center)
# p.set_width(ylab,zlab)
# p.set_axes_unit("au")
# #p.set_zlim("density",1.e-8,1.e-4)
# p.set_cmap("density","winter")

p.annotate_title("Grids")
p.annotate_grids() 
#p.annotate_particles(1,p_size=4,col="r")
#Bargs ={}
#Bargs["color"]="black"
#p.annotate_velocity()
#p.annotate_steamlines("Y-magnfield","Z-magnfield",factor=200,plot_args=Bargs)

spath = path+"plots/" + "test.pdf"
p.save(spath)

