'''Given a 'sink_data_EveryTStep.pickle' file contianing sink Mdot data from simulation:
computes values of rB, rB/L, M0, from simulation data
plots Fig. 4 of Krumholz 2006: histogram of simulation Sink Mdot and Krumholz 2006 Analytic Mdots'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5_when_insert",action="store",help='.hdf5 data file when insert sinks')
parser.add_argument("-sink_when_insert",action="store",help='.sink data file when insert sinks')
parser.add_argument("-sinkdata_pickle",action="store",help='.pickle file with Mdots calculated for entire sink eveolution, usually titled "sink_data_EveryTStep.pickle"')
parser.add_argument("-cs",action="store",help='constant sound speed')
args = parser.parse_args()
if args.hdf5_when_insert and args.sink_when_insert and args.sinkdata_pickle and args.cs: 
    hdf5_when_insert= str(args.hdf5_when_insert)
    sink_when_insert= str(args.sink_when_insert)
    sinkdata_pickle= str(args.sinkdata_pickle)
    cs= float(args.cs)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import matplotlib.pyplot as plt
import pickle
import my_fund_const as fc

#so can load "sink_data_EveryTStep.pickle"
class SinkData():
    def __init__(self,NSinks,NSinkFiles):
        shape= (NSinkFiles,NSinks)
        self.time= np.zeros(NSinkFiles)-1
        self.sid= np.zeros(shape)-1
        self.mass= np.zeros(shape)-1
        self.x= np.zeros(shape)-1
        self.y= np.zeros(shape)-1
        self.z= np.zeros(shape)-1
    def OnlyGoodData(self,NSinkFiles_Good):
        self.time= self.time[0:NSinkFiles_Good]
        self.sid= self.sid[0:NSinkFiles_Good,:]
        self.mass= self.mass[0:NSinkFiles_Good,:]
        self.x= self.x[0:NSinkFiles_Good,:]
        self.y= self.y[0:NSinkFiles_Good,:]
        self.z= self.z[0:NSinkFiles_Good,:]           
    def MdotCoarseTstep(self):
        '''Mdot =delta Msink / delta time
        delta time coarse = 2^MaxLevel * delta time fine'''
        if self.time[-1].astype('int') == -1:
            print "time not loaded"
            raise ValueError
        else:
            mbin= sink.mass[::32,:]
            tbin= sink.time[::32]
            self.MdotCoarse= np.zeros(mbin.shape)-1
            self.MdotCoarse[0,:]=0.
            for cnt in np.arange(1,mbin.shape[0]):
                deltaM= mbin[cnt,:] -mbin[cnt-1,:]
                deltaT= tbin[cnt]- tbin[cnt-1]
                self.MdotCoarse[cnt,:]= deltaM/deltaT
    def MdotFineTstep(self):
        if self.time[-1].astype('int') == -1:
            print "time not loaded"
            raise ValueError
        else:
            self.MdotFine= np.zeros(self.mass.shape)-1
            self.MdotFine[0,:]=0.
            for cnt in np.arange(1,sink.mass.shape[0]):
                deltaM= self.mass[cnt,:] -self.mass[cnt-1,:]
                deltaT= self.time[cnt]- self.time[cnt-1]
                self.MdotFine[cnt,:]= deltaM/deltaT

def CurlCellDiff(pf,vx,vy,vz):
    #FIRST PART
    class CellFaces():
        class Vel():
            def __init__(self,vx,vy,vz):
                dim= np.array(vx.shape)+1  #vx,vy,vz all same size
                self.Vx= np.zeros(dim)
                self.Vy= self.Vx.copy()
                self.Vz= self.Vx.copy()
        def __init__(self,vx,vy,vz):
            '''vx,vy,vz are from vx=covering_grid['X-momentum']/covering_grid['density'], etc
            LR - Left/Right faces (x dir)
            FB - Front/Back faces (y dir)
            BT - Bottom/Top faces (z dir)
            '''
            self.LR= self.Vel(vx,vy,vz)
            self.TB= self.Vel(vx,vy,vz)
            self.FB= self.Vel(vx,vy,vz)

    Face= CellFaces(vx,vy,vz)
    first=0
    last=vx.shape[0]-1
    #iterate Left/Right faces first (eg. x direction)
    for i in range(vx.shape[0]):
        ind=i-1
        if i==first: ind=last
        Face.LR.Vx[i,:-1,:-1]= (vx[ind,:,:]+vx[i,:,:])/2  #:-1 keep dimensions same
        Face.LR.Vy[i,:-1,:-1]= (vy[ind,:,:]+vy[i,:,:])/2
        Face.LR.Vz[i,:-1,:-1]= (vz[ind,:,:]+vz[i,:,:])/2
    #fill that last index of face in x dir, it is the same as the first Face value
    Face.LR.Vx[-1,:,:]= Face.LR.Vx[0,:,:] #dim match so no -1
    Face.LR.Vy[-1,:,:]= Face.LR.Vy[0,:,:]
    Face.LR.Vz[-1,:,:]= Face.LR.Vz[0,:,:]
    #iterate Front/Back faces (eg. y direction)
    for j in range(vx.shape[0]):
        ind=j-1
        if j==first: ind=last
        Face.FB.Vx[:-1,j,:-1]= (vx[:,ind,:]+vx[:,j,:])/2
        Face.FB.Vy[:-1,j,:-1]= (vy[:,ind,:]+vy[:,j,:])/2
        Face.FB.Vz[:-1,j,:-1]= (vz[:,ind,:]+vz[:,j,:])/2
    Face.FB.Vx[:,-1,:]= Face.FB.Vx[:,0,:]
    Face.FB.Vy[:,-1,:]= Face.FB.Vy[:,0,:]
    Face.FB.Vz[:,-1,:]= Face.FB.Vz[:,0,:]
    #iterate Top/Bottom faces (eg. z direction)
    for k in range(vx.shape[0]):
        ind=k-1
        if k==first: ind=last
        Face.TB.Vx[:-1,:-1,k]= (vx[:,:,ind]+vx[:,:,k])/2
        Face.TB.Vy[:-1,:-1,k]= (vy[:,:,ind]+vy[:,:,k])/2
        Face.TB.Vz[:-1,:-1,k]= (vz[:,:,ind]+vz[:,:,k])/2
    Face.TB.Vx[:,:,-1]= Face.TB.Vx[:,:,0]
    Face.TB.Vy[:,:,-1]= Face.TB.Vy[:,:,0]
    Face.TB.Vz[:,:,-1]= Face.TB.Vz[:,:,0]
    #3 faces (1/2 of cell in corner [-1,-1,-1]) have not been filled, do so now
    Face.LR.Vx[-1,-1,-1]= Face.LR.Vx[0,-1,-1]
    Face.LR.Vy[-1,-1,-1]= Face.LR.Vy[0,-1,-1]
    Face.LR.Vz[-1,-1,-1]= Face.LR.Vz[0,-1,-1]
    #
    Face.FB.Vx[-1,-1,-1]= Face.FB.Vx[-1,0,-1]
    Face.FB.Vy[-1,-1,-1]= Face.FB.Vy[-1,0,-1]
    Face.FB.Vz[-1,-1,-1]= Face.FB.Vz[-1,0,-1]
    #
    Face.TB.Vx[-1,-1,-1]= Face.TB.Vx[-1,-1,0]
    Face.TB.Vy[-1,-1,-1]= Face.TB.Vy[-1,-1,0]
    Face.TB.Vz[-1,-1,-1]= Face.TB.Vz[-1,-1,0]
    #SECOND PART
    class Gradients():
        def __init__(self,vx):
            dim= np.array(vx.shape)
            #grad in x dir
            self.dVy_dx= np.zeros(dim)
            self.dVz_dx= self.dVy_dx.copy()
            #grad in y dir
            self.dVx_dy= self.dVy_dx.copy()
            self.dVz_dy= self.dVy_dx.copy()
            #grad in z dir
            self.dVx_dz= self.dVy_dx.copy()
            self.dVy_dz= self.dVy_dx.copy()
    Grad= Gradients(vx)
    #grad in x dir
    for i in range(vx.shape[0]):
        Grad.dVy_dx[i,:,:]= Face.LR.Vy[i+1,:-1,:-1]-Face.LR.Vy[i,:-1,:-1] #Grad arrays have Sim data dims
        Grad.dVz_dx[i,:,:]= Face.LR.Vz[i+1,:-1,:-1]-Face.LR.Vz[i,:-1,:-1]
    #grad in y dir
    for j in range(vx.shape[0]):
        Grad.dVx_dy[:,j,:]= Face.FB.Vx[:-1,j+1,:-1]-Face.FB.Vx[:-1,j,:-1]
        Grad.dVz_dy[:,j,:]= Face.FB.Vz[:-1,j+1,:-1]-Face.FB.Vz[:-1,j,:-1]
    #grad in z dir
    for k in range(vx.shape[0]):
        Grad.dVx_dz[:,:,k]= Face.TB.Vx[:-1,:-1,k+1]-Face.TB.Vx[:-1,:-1,k]
        Grad.dVy_dz[:,:,k]= Face.TB.Vy[:-1,:-1,k+1]-Face.TB.Vy[:-1,:-1,k]
    #don't forget to DIVIDE BY CELL WIDTH!
    lev=0
    wid_cell= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**lev
    Grad.dVy_dx /= wid_cell
    Grad.dVz_dx /= wid_cell
    #
    Grad.dVx_dy /= wid_cell
    Grad.dVz_dy /= wid_cell
    #
    Grad.dVx_dz /= wid_cell
    Grad.dVy_dz /= wid_cell
    #THIRD PART
    class CurlVector():
        def __init__(self,Grad):
            self.xhat= Grad.dVz_dy- Grad.dVy_dz
            self.yhat= -(Grad.dVz_dx- Grad.dVx_dz)
            self.zhat= Grad.dVy_dx- Grad.dVx_dy
            self.mag= np.sqrt(self.xhat**2+self.yhat**2+self.zhat**2)
    Curl= CurlVector(Grad)
    return Curl.mag

def MdotBH_x(rho_x,Msink,cs,MachS_x):
    one=4.*np.pi*rho_x*(fc.G*Msink)**2*cs**-3
    lam=1.1
    return one*(lam**2+MachS_x**2)/((1+MachS_x**2)**4)**0.5

def MdotW_x(Msink,rho_x,cs,vort_x):
    rB= fc.G*Msink/cs**2
    wstar_x= vort_x*rB/cs
    fstar_x= 1/(1+wstar_x**0.9)
    return 4*np.pi*rho_x*(fc.G*Msink)**2*cs**-3*0.34*fstar_x

def calc_M0ofVField(vx,vy,vz,rho,cs):
    vrms2= (vx**2+vy**2+vz**2)
    vrms2_MassWt= np.average(vrms2,weights=rho)
    return np.sqrt(vrms2_MassWt)/cs

class AnORSim():
    def __init__(self):
        self.Bins=-1
        self.Mdot=-1

class CumuOrPdf():
    def __init__(self):
        self.W= AnORSim()
        self.BH= AnORSim()
        self.Turb= AnORSim()
        self.Sink= AnORSim()

class HistData():
    def __init__(self):
        self.Cumu= CumuOrPdf()
        self.Pdf= CumuOrPdf()

#MAIN#
#rB from data
mass,x,y,z= np.loadtxt(sink_when_insert,dtype='float',\
                        skiprows=1,usecols=(0,1,2,3),unpack=True)
Msink= mass[0]
rB= fc.G*Msink/cs**2
rB_div_L= rB/ds.domain_width[0]
log_rB_L= np.log10(rB_div_L)
#velocities from data
ds=yt.load(hdf5_when_insert)
cgrid= ds.covering_grid(level=0,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions) 
rho_x=np.array(cgrid['density']) #_x means have value at every cell in grid
vx_x=np.array(cgrid['X-momentum']/rho_x)
vy_x=np.array(cgrid['Y-momentum']/rho_x)
vz_x=np.array(cgrid['Z-momentum']/rho_x)
vrms_x= np.sqrt(vx_x**2+vy_x**2+vz_x**2)
MachS_x= vrms_x/cs
vort_x= CurlCellDiff(ds,vx_x,vy_x,vz_x)
M0_data= calc_M0ofVField(vx_x,vy_x,vz_x,rho_x,cs)

Mbh_x= MdotBH_x(rho_x,Msink,cs,MachS_x)
Mw_x= MdotW_x(Msink,rho_x,cs,vort_x)
Mturb_x= (Mbh_x**-2 + Mw_x**-2)**-0.5
M0= 4.*np.pi*rho_x.mean()*(fc.G*Msink)**2/(M0_data*cs)**3
#Mdot histograms for velocity info only
data= HistData()
(data.Cumu.Turb.Mdot,CBins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True,cumulative=True)
(data.Cumu.BH.Mdot,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=CBins,normed=True,cumulative=True)
(data.Cumu.W.Mdot,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=CBins,normed=True,cumulative=True)
(data.Pdf.Turb.Mdot,PBins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True)#,stacked=True)
(data.Pdf.BH.Mdot,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=PBins,normed=True)#,stacked=True)
(data.Pdf.W.Mdot,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=PBins,normed=True)#,stacked=True)
CBins_avg= (CBins[:-1]+CBins[1:])/2
PBins_avg= (PBins[:-1]+PBins[1:])/2
data.Cumu.Turb.Bins= CBins_avg
data.Cumu.BH.Bins= CBins_avg
data.Cumu.W.Bins= CBins_avg
data.Pdf.Turb.Bins= PBins_avg
data.Pdf.BH.Bins= PBins_avg
data.Pdf.W.Bins= PBins_avg
#Mdot histogram for Sink Data
fin=open(sinkdata_pickle,'r')
sink=pickle.load(fin)
fin.close()
mdot_sinks=np.average(sink.MdotFine[-20:,:],axis=0)
(data.Cumu.Sink.Mdot,sink_cbins,p)=plt.hist(np.log10(mdot_sinks/M0),bins=100,normed=True,cumulative=True)
data.Cumu.Sink.Bins= (sink_cbins[:-1]+sink_cbins[1:])/2
(data.Pdf.Sink.Mdot,sink_pbins,p)=plt.hist(np.log10(mdot_sinks/M0),bins=10,normed=True)#,stacked=True
data.Pdf.Sink.Bins= (sink_pbins[:-1]+sink_pbins[1:])/2
#make Fig. 4
def plotFig4(data,fsave):
    fig,ax=plt.subplots(2,1,sharex=True)
    lab= [r'$\dot{M}_{Turb}$',r'$\dot{M}_{BH}$',r'$\dot{M}_{\omega}$',r'$\dot{M}_{Sinks}$',]
    bins= 10**data.Cumu.Turb.Bins
    ax[0].plot(bins,data.Cumu.Turb.Mdot,c='k',ls='-',label=lab[0])
    ax[0].plot(bins,data.Cumu.BH.Mdot,c='k',ls='--',label=lab[1])
    ax[0].plot(bins,data.Cumu.W.Mdot,c='k',ls=':',label=lab[2])
    ax[0].plot(10**data.Cumu.Sink.Bins,data.Cumu.Sink.Mdot,c='b',ls='-',label=lab[3])
    ax[0].set_ylabel(r'$P_M(<\dot{M}$)')
    ax[0].set_ylim(0,1)
    ax[0].set_xscale('log')
    ax[0].legend(loc=0)
    bins= 10**data.Pdf.Turb.Bins
    ax[1].plot(bins,data.Pdf.Turb.Mdot,c='k',ls='-',label=lab[0])
    ax[1].plot(bins,data.Pdf.BH.Mdot,c='k',ls='--',label=lab[1])
    ax[1].plot(bins,data.Pdf.W.Mdot,c='k',ls=':',label=lab[2])
    ax[1].plot(10**data.Pdf.Sink.Bins,data.Pdf.Sink.Mdot,c='b',ls='-',label=lab[3])
    ax[1].set_xlabel(r'$\dot{M}/\dot{M}_0$')
    ax[1].set_ylabel(r'PDF')
    ax[1].set_xlim(1.e-2,1.e2)
    ax[1].set_xscale('log')
    fig.subplots_adjust(hspace=0.)
    plt.savefig(fsave,dpi=150)
fsave=spath+"K06_Fig4_for_Hdf5Insert-%s_And-%s.png" % (hdf5_when_insert,sinkdata_pickle)
plotFig4(data,fsave)

