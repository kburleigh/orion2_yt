'''plots data,model pdfs and KS statistic for each'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-path",action="store",help='path to data')
parser.add_argument("-dist",action="store",help='final_distributions.pickle file')
parser.add_argument("-phi_data",nargs=2,type=float,action="store",help='mean median')
parser.add_argument("-phi_close",nargs=2,type=float,action="store",help='mean median')
parser.add_argument("-xlim",nargs=2,type=float,action="store",help='low hi',required=False)
args = parser.parse_args()

import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
from scipy import stats
from scipy.integrate import quad
import scipy as sp

def dp_dlnM(lnM,med,mean):
    std=(2*np.log(mean/med))**0.5
    return (2*np.pi*std**2)**-0.5*np.exp(-(lnM-np.log(med))**2/2/std**2)

def add_to_plot(ax,logbins,loghist,c,ls,label):
    bins=(logbins[:-1]+logbins[1:])/2
    bins=np.exp(bins)
    ax.plot(bins,loghist,c=c,ls=ls,label=label)

class Obj():
    pass
fin=open(os.path.join(args.path,args.dist),'r')
(logbins,loghists,databins,datahist)=pickle.load(fin)
fin.close()

fig,ax=plt.subplots(1,1)
# add_to_plot(ax, logbins.bh,loghists.bh,'g','-','bh')
# add_to_plot(ax, logbins.w,loghists.w,'m','-','w')
# add_to_plot(ax, logbins.par_mean_pars,loghists.par_mean_pars,'b','-',r'$\parallel$ mean pars')
# add_to_plot(ax, logbins.par_med_pars,loghists.par_med_pars,'b','--',r'$\parallel$ median pars')
# add_to_plot(ax, logbins.perp_mean_pars,loghists.perp_mean_pars,'y','-',r'$\perp$ mean pars')
# add_to_plot(ax, logbins.perp_med_pars,loghists.perp_med_pars,'y','--',r'$\perp$ median pars')
# add_to_plot(ax, logbins.perp_default,loghists.perp_default,'r','-',r'$\perp$ default')
# add_to_plot(ax, logbins.par_default,loghists.par_default,'r','--',r'$\parallel$ default')
bins=(databins[:-1]+databins[1:])/2
bins=np.exp(bins)
left=np.exp(databins[:-1])
right=np.exp(databins[1:])
ax.bar(bins,datahist,width=right-left,align='center',fill=False,color='k',label='data')
#plt.xlim([1e-2,1e2])
#plt.ylim([0,0.5])
#     plt.yscale('log')
M=np.exp(np.linspace(-10,3,num=100))
plt.plot(M,dp_dlnM(np.log(M),args.phi_data[1],args.phi_data[0]),c='b',label=r'mean,med from data')
plt.plot(M,dp_dlnM(np.log(M),args.phi_close[1],args.phi_close[0]),c='g',label='mean,med Predicted')
ax.set_xscale('log')
if args.xlim: ax.set_xlim(args.xlim[0],args.xlim[1])
ax.set_xlabel(r'$\dot{M}/\dot{M}_{B}$')
ax.set_ylabel(r'dp/d ln ($\dot{M}/\dot{M}_{B}$)')
ax.legend(loc=0,fontsize='small')
plt.savefig(os.path.join(args.path,'hist_model.png'))
plt.close()
#KS statistic for each model cdf
def normal_pdf(x,mu,std):
    return (2*np.pi*std**2)**-0.5*np.exp(-(x-mu)**2/2/std**2)

def normal_cdf(x,mu,std):
    x_sort=np.sort(x.copy())
    cdf=np.zeros(len(x_sort))-1
    pdf=cdf.copy()
    for i in range(len(x_sort)):
        pdf[i]=normal_pdf(x_sort[i],mu,std)
        cdf[i],err= quad(normal_pdf,-sp.inf,x_sort[i],args=(mu,std),epsabs=0,epsrel=1e-6)
    if len(np.where(cdf < 0)[0]) > 0: 
        print "cdf < 0 here: ",np.where(cdf < 0)[0]
        raise ValueError
    return pdf,cdf

def my_ks_test(data_cdf,model_cdf_eval_at_data_pts,N=64):
    '''N: sample size from which data_cdf is made, 64 sinks
    code taken from relevant parts in: scipy.stats.kstest() 
    since that code is needlessly complicated 
    and requires creation of a scipy.stats.rvs() object
    using deafult: alternative="two-sided", mode="approx"'''
    if len(data_cdf) != len(model_cdf_eval_at_data_pts): raise ValueError
    D = np.abs(data_cdf - model_cdf_eval_at_data_pts).max()
    #I use tabulated crical Dn values from: 
    #http://www.real-statistics.com/tests-normality-and-symmetry/statistical-tests-normality-symmetry/kolmogorov-smirnov-test/
    #http://www.real-statistics.com/statistics-tables/kolmogorov-smirnov-table/
#     pval_two = stats.kstwobign.sf(D*np.sqrt(N))
#     if N > 2666 or pval_two > 0.80 - N*0.3/1000.0:
#         return D, stats.kstwobign.sf(D*np.sqrt(N))
#     else:
#         return D, stats.ksone.sf(D,N)*2
    p_levels=np.array([0.01,0.05,0.1,0.15,0.2])
    if N > 50: 
		D_crit= np.array([1.63,1.36,1.22,1.14,1.07])/N**0.5
		print "D_crit= ",D_crit
		print "T/F: H0 can be rejected at the following percent levels"
		for p,tf in zip(p_levels,D > D_crit): print p,'->',tf
    else: 
        print "N<=50, for D_crit see http://www.real-statistics.com/statistics-tables/kolmogorov-smirnov-table/"
        raise ValueError   
    return D

#KS test: normal dist
mu,std=0,0.5
fig,axes=plt.subplots(2,2)
ax=axes.flatten()
np.random.seed(140)
N=1000
data=np.random.normal(mu,std , N)
print "mu,std= ",mu,std,"data mu,std= ",data.mean(),data.std()
(data_cdf,bins,junk)= ax[0].hist(data,bins=20,cumulative=True,normed=True,fill=False)
eval_cdf_at= bins[1:] #right side
mid_bins= (bins[1:]+bins[:-1])/2
dx_bins=bins[1:]-bins[:-1]
pdf,model_cdf=normal_cdf(eval_cdf_at,mu,std)
ax[1].plot(eval_cdf_at,model_cdf,mid_bins,pdf)
(data_pdf,junk1,junk)= ax[2].hist(data,bins=bins)
ax[3].bar(mid_bins,data_pdf,width=dx_bins,align='center',fill=False,color='k',label='data')
for i in range(4): ax[i].set_xlim(-1,1)
plt.savefig('KS_norm_test.png')
plt.close()
for d,m in zip(data_cdf,model_cdf): print "data,model,|diff|= ",d,m,np.abs(d-m)
print bins.min(),bins.max()
D=my_ks_test(data_cdf,model_cdf,N=N)
print "KS statistic= ",D
#KS B0.01 data CDF and model using mean,median from data
class Obj():
    pass
fin=open('b0.01/data_models_CDFs.pickle','r')
(model_bins,model_cdfs,data_bins,data_cdf)=pickle.load(fin)
fin.close()
def mdot_model_pdf(lnM,med,mean):
#     lnM=lnM/1.05 #beta0.01 "close" fits great with this
    std=(2*np.log(mean/med))**0.5
    return (2*np.pi*std**2)**-0.5*np.exp(-(lnM-np.log(med))**2/2/std**2)

def mdot_model_cdf(x,med,mean):
    x_sort=np.sort(x.copy())
    cdf=np.zeros(len(x_sort))-1
    for i in range(len(x_sort)):
        cdf[i],err= quad(mdot_model_pdf,-sp.inf,x_sort[i],args=(med,mean),epsabs=0,epsrel=1e-6)
    if len(np.where(cdf < 0)[0]) > 0: 
        print "cdf < 0 here: ",np.where(cdf < 0)[0]
        raise ValueError
    return cdf

eval_cdf_at= data_bins[1:]
#(mean,med)=(0.0013,0.0010) #directly from data
(mean,med)=(0.0018,0.0014) #predicted
model_cdf=mdot_model_cdf(eval_cdf_at,med,mean)
plt.bar(np.exp(eval_cdf_at),data_cdf,width=right-left,align='center',fill=False,color='k',label='data')
plt.plot(np.exp(eval_cdf_at),model_cdf,c='b')
plt.xscale('log')
plt.xlim(1e-4,1e-1)
plt.xlabel(r'$\dot{M}/\dot{M}_{0}$')
plt.ylabel(r'dp/d ln ($\dot{M}/\dot{M}_{0}$)')
plt.legend(loc=0)
plt.savefig('KS_b0.01.png')
plt.close()
for d,m in zip(data_cdf,model_cdf): print "data,model,|diff|= ",d,m,np.abs(d-m)
D=my_ks_test(data_cdf,model_cdf,N=64)
print "KS statistic= ",D
