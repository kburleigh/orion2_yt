#run from ipython, useful functions were tested in script mode then defined as fucntions

from yt.mods import * #yt.imods for yt notebook only
import glob

import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import my_fund_const as fc
import ic_dfields as myic      #intial conditions and all derived fields for simulation data

#Initial conditions for SIM
pars =myic.ParsNeed()
pars.temp=1.e6
pars.mach_s=1.41
pars.cells=32.
pars.levs =0
pars.Lbox=10.
pars.prout()
ic=myic.initCondData(pars)
ic.prout()
myic.add_ALL_fields(ic)
#######

path = "/clusterfs/henyey/kaylanb/Test_Prob/Sink/Lee14/quick/beta-10_M-1.41/lev0/"
spath = path+"plots/"
file = "data.0000.3d.hdf5"
#patt = "data.000*.3d.hdf5"
#files = glob.glob1(path, patt)

#testing, "rho-norm" with vel and B field vectors
ic.prout
rhomin= ic.rho0*ic.Lbox*1.e-1 #!!! multiply by Lbox b/c collpase box to square!!
rhomax= ic.rho0*ic.Lbox*1.e1
pf = load(path+file) #pf = param file
p = ProjectionPlot(pf, "x", "density")
p.zoom(1)
p.set_zlim("density",rhomin,rhomax)
p.annotate_velocity()
p.annotate_text((0.5, 0.75), 'hello', text_args={'size':'xx-large', 'color':'k'})
p.save(name=spath+"rho_wV_"+file[5:9]+".pdf")
#p = ProjectionPlot(pf, "x", "density")
#p.zoom(1)
#p.set_zlim(field="density",zmin=rhomin,zmax=rhomax)
#p.annotate_magnetic_field()
#p.save(name=spath+"rho_wB_"+file[5:9]+".pdf")


#def diff_zms(pf):
#	fields = ["Z-magnfield","energy-density","density"]
#	zms = [1,2,4,8]
#	for field in fields:
#		slc = SlicePlot(pf, "z", field,"c")
#		totZm = 1
#		for zm in zms:
#			slc.zoom(zm)
#			totZm *= zm
#			slc.save(name=save_path+field + "_slice_zm"+str(totZm)+".png")
#
##choose one field, loop over all data files plotting slice or projection of that field with fixed scale
#def proj_all_1field():
#	pf = load(path+files[0])
#	data = pf.h.all_data()
#	zmin = data["density"].min()
#	zmax = data["density"].max()
#	for f in files:
#		pf = load(path+f) #pf = param file
#		print "loading file: ",f
#		data = pf.h.all_data()
#		p = ProjectionPlot(pf, "z", "Density")
#		p.zoom(8)
#		p.set_zlim(field="all",zmin=zmin,zmax=zmax)
#		p.annotate_velocity()
#		p.save(name=save_path+"rho_proj_wVel_zm8"+f[5:9]+".png")
#		p = ProjectionPlot(pf, "z", "Density")
#		p.zoom(8)
#		p.set_zlim(field="all",zmin=zmin,zmax=zmax)
#		p.annotate_magnetic_field()
#		p.save(name=save_path+"rho_proj_wBfield_zm8"+f[5:9]+".png")
