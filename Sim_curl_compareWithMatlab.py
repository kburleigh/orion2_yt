'''run: 1) call "Sim_PrimVars2Matlab.py" then "vel2curl.m" to get matlab curlx,y,z
        2) run this script to calc my curl to compare against matlab'
'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-f_hdf5",action="store",help='hdf5 file to get velocities for')
parser.add_argument("-matcurl_PB",action="store",help='hdf5 file to get velocities for')
parser.add_argument("-matcurl_noPB",action="store",help='hdf5 file to get velocities for')
parser.add_argument("-mark_data",action="store",help='hdf5 file to get velocities for')
parser.add_argument("-lbox",action="store",help='hdf5 file to get velocities for')
parser.add_argument("-mach",action="store",help='hdf5 file to get velocities for')
parser.add_argument("-cs",action="store",help='hdf5 file to get velocities for')
args = parser.parse_args()
if args.f_hdf5:
    hdf5 = str(args.f_hdf5)
    matcurl_PB = str(args.matcurl_PB)
    matcurl_noPB = str(args.matcurl_noPB)
    mark_data = str(args.mark_data)
    lbox = float(args.lbox)
    mach = float(args.mach)
    cs = float(args.cs)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

def PeriodicBox(vx):
    vxn=np.zeros(np.array(vx.shape)+2)
    vxn[1:-1,1:-1,1:-1]= vx
    #copy opposite edge values to ghost zones
    vxn[0,1:-1,1:-1]=vx[-1,:,:]
    vxn[-1,1:-1,1:-1]=vx[0,:,:]
    vxn[1:-1,0,1:-1]=vx[:,-1,:]
    vxn[1:-1,-1,1:-1]=vx[:,0,:]
    vxn[1:-1,1:-1,0]=vx[:,:,-1]
    vxn[1:-1,1:-1,-1]=vx[:,:,0]
    return vxn

def KaylanCurl(wid_cell,Vx,Vy,Vz):
    #finite diff
    dVy_dx= Vy[:-2,1:-1,1:-1]-Vy[2:,1:-1,1:-1]
    dVz_dx= Vz[:-2,1:-1,1:-1]-Vz[2:,1:-1,1:-1]
    dVx_dy= Vx[1:-1,:-2,1:-1]-Vx[1:-1,2:,1:-1]
    dVz_dy= Vz[1:-1,:-2,1:-1]-Vz[1:-1,2:,1:-1]
    dVx_dz= Vx[1:-1,1:-1,:-2]-Vx[1:-1,1:-1,2:]
    dVy_dz= Vy[1:-1,1:-1,:-2]-Vy[1:-1,1:-1,2:]
    #compute curl
    xhat= (dVz_dy- dVy_dz)/2./wid_cell
    yhat= -(dVz_dx - dVx_dz)/2./wid_cell
    zhat= (dVy_dx- dVx_dy)/2./wid_cell
    return (-xhat,-yhat,-zhat)  #curl is defined differencing right -left, without minus sign i'm diff left-right

def WillCurl(BaseGridWidth,Vx,Vy,Vz):
    sl_left = slice(None, -2, None)
    sl_right = slice(2, None, None)
    sl_center = slice(1, -1, None)
    div_fac = 2.0*BaseGridWidth

    vortx = (Vz[sl_center,sl_right,sl_center] - Vz[sl_center,sl_left,sl_center] )/ div_fac - (Vy[sl_center,sl_center,sl_right]-Vy[sl_center,sl_center,sl_left])/div_fac
    
    vorty = (Vx[sl_center,sl_center,sl_right] - Vx[sl_center,sl_center,sl_left] )/ div_fac - (Vz[sl_right,sl_center,sl_center]-Vz[sl_left,sl_center,sl_center])/div_fac
    
    vortz = (Vy[sl_right,sl_center,sl_center] - Vy[sl_left,sl_center,sl_center] )/ div_fac - (Vx[sl_center,sl_right,sl_center]-Vx[sl_center,sl_left,sl_center])/div_fac
    return (vortx,vorty,vortz)


class CurlVec():
    pass
kay_PB= CurlVec()
kay_noPB= CurlVec()
#will= CurlVec()   #gives identical answer to kay curl
mat= CurlVec()

tmp= np.loadtxt(mark_data,dtype=np.string_,skiprows=1)
Fig7= CurlVec()
Fig7.w= np.array(tmp[:,0]).astype(np.float)
Fig7.dpdlnw= np.array(tmp[:,1]).astype(np.float)

ds=yt.load(hdf5)
lev= 0
cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions)
rho = np.array(cube["density"])
vx = np.array(cube["X-momentum"]/rho)
vy = np.array(cube["Y-momentum"]/rho)
vz = np.array(cube["Z-momentum"]/rho)
vxPB=PeriodicBox(vx)
vyPB=PeriodicBox(vy)
vzPB=PeriodicBox(vz)
width=1.
(kay_PB.x,kay_PB.y,kay_PB.z)= KaylanCurl(width,vxPB,vyPB,vzPB)
(kay_noPB.x,kay_noPB.y,kay_noPB.z)= KaylanCurl(width,vx,vy,vz)
#(will.x,will.y,will.z)= WillCurl(width,vxPB,vyPB,vzPB)

a = sio.loadmat(matcurl_PB)
mat.PB=a['mag']
b = sio.loadmat(matcurl_noPB)
mat.noPB=b['mag']

#linbins=CurlVec()
#linhists=CurlVec()
#def GetLinHist(vort_flat,bins=100):
#    (nhist,usebins,jnk)= plt.hist(vort_flat,bins=bins,normed=True,align='mid',histtype='bar')
#    plt.close()
#    return usebins,nhist
#(linbins.kay,linhists.kay)= GetLinHist(kay.z.flatten())
#(linbins.will,linhists.will)= GetLinHist(will.z.flatten(),bins=linbins.kay)
#(linbins.mat,linhists.mat)= GetLinHist(mat.z[1:-1,1:-1,1:-1].flatten(),bins=linbins.kay)
#bins=(linbins.kay[:-1]+linbins.kay[1:])/2
#plt.plot(bins,linhists.kay,'ro',ms=10.,label='Kay')
#plt.plot(bins,linhists.will,'o',ms=12.,mew=2.,mfc='none', mec='b',label='will')
#plt.plot(bins,linhists.mat,'o',ms=18.,mew=2.,mfc='none', mec='m',label='mat')
#
## plt.xlim([1e-2,1e3])
## plt.ylim([1e-6,1])
#plt.yscale('log')
#plt.xscale('log')
#plt.xlabel('w_x')
#plt.ylabel(r'dp/d ln w')
#plt.legend(loc=3)
#plt.savefig('wx.png')
#plt.close()

def GetLogHist(vort_flat,bins=100):
    logw=np.log(vort_flat)
    (logn,logbins,jnk)= plt.hist(logw,bins=bins,align='mid',normed=True,histtype='bar')
    plt.close()
    return logbins, logn

def PlotW_PB(fname,kay,mat,Fig7):
    class Obj():
        pass
    logbins=Obj()
    loghists=Obj()
    (logbins.kay,loghists.kay)= GetLogHist(kay.flatten())
    (logbins.mat,loghists.mat)= GetLogHist(mat.flatten(),bins=logbins.kay)
    (logbins.matin,loghists.matin)= GetLogHist(mat[1:-1,1:-1,1:-1].flatten(),bins=logbins.kay)
    bins=(logbins.kay[:-1]+logbins.kay[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghists.kay,'ro',ms=10.,label='Kaylan 512')
    plt.plot(bins,loghists.mat,'o',ms=14.,mew=2.,mfc='none', mec='b',label='Matlab 514')
    plt.plot(bins,loghists.matin,'m*',ms=10.,label='Matlab 512')
    plt.plot(Fig7.w,Fig7.dpdlnw,'^',ms=10.,mew=2.,mfc='none', mec='k',label='Mark 512')
    plt.xlim([1e-2,1e3])
    plt.ylim([1e-6,1])
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\tilde{\omega}= \omega L/(M_0c_s)$')
    plt.ylabel(r'dp/d ln w')
    plt.legend(loc=2)
    plt.savefig(fname)
    plt.close()

def PlotW_noPB(fname,kay,mat,Fig7):
    class Obj():
        pass
    logbins=Obj()
    loghists=Obj()
    (logbins.kay,loghists.kay)= GetLogHist(kay.flatten())
    (logbins.mat,loghists.mat)= GetLogHist(mat.flatten(),bins=logbins.kay)
    bins=(logbins.kay[:-1]+logbins.kay[1:])/2
    bins=np.exp(bins)
    plt.plot(bins,loghists.kay,'ro',ms=10.,label='Kaylan 510')
    plt.plot(bins,loghists.mat,'o',ms=14.,mew=2.,mfc='none', mec='b',label='Matlab 512')
    plt.plot(Fig7.w,Fig7.dpdlnw,'^',ms=10.,mew=2.,mfc='none', mec='k',label='Mark 512')
    plt.xlim([1e-2,1e3])
    plt.ylim([1e-6,1])
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\tilde{\omega}= \omega L/(M_0c_s)$')
    plt.ylabel(r'dp/d ln w')
    plt.legend(loc=2)
    plt.savefig(fname)
    plt.close()



cell_dx=float(ds.domain_width[0])/ds.domain_dimensions[0]/2**lev
wsquig= lbox/mach/cs
mat.PB=mat.PB/cell_dx*wsquig
mat.noPB=mat.noPB/cell_dx*wsquig
kay_PB.mag=np.sqrt(kay_PB.x**2+kay_PB.y**2+kay_PB.z**2)/cell_dx*wsquig
kay_noPB.mag=np.sqrt(kay_noPB.x**2+kay_noPB.y**2+kay_noPB.z**2)/cell_dx*wsquig
#will.mag=np.sqrt(will.y**2+will.x**2+will.z**2)/cell_dx*wsquig
PlotW_PB('Fig7_PB.png',kay_PB.mag,mat.PB,Fig7)
PlotW_noPB('Fig7_noPB.png',kay_noPB.mag,mat.noPB,Fig7)
