import argparse

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-problem",choices=['lee14','krum06'],action="store",help='problem name', required=True)
parser.add_argument("-answer",type=float,action="store",help='simulation result should get', required=True)
parser.add_argument("-path",action="store",help='hdf5 filename', required=True)
parser.add_argument("-den0",type=float,action="store",help='hdf5 filename', required=True)
parser.add_argument("-cs0",type=float,action="store",help='hdf5 filename', required=True)
parser.add_argument("-vxyz",nargs=3,metavar=('x','y','z'),type=float,action="store",help='x,y,z slice/proj center', required=True)
parser.add_argument("-bxyz",nargs=3,metavar=('x','y','z'),type=float,action="store",help='x,y,z slice/proj center', required=True)
#optional
parser.add_argument("-Geq1",action="store",help='1 or anything to use G = 1 units', required=False)
parser.add_argument("-LinearY",action="store",help='1 or anything to plot Linear y-axis', required=False)
parser.add_argument("-fname",action="store",help='prefix for mdot_sinkFiles.png', required=False)
parser.add_argument("-xlim",nargs=2,metavar=('min','max'),type=float,action="store",help='', required=False)
parser.add_argument("-ylim",nargs=2,metavar=('min','max'),type=float,action="store",help='', required=False)
args = parser.parse_args()

(vx0,vy0,vz0)= tuple(args.vxyz)
(bx0,by0,bz0)= tuple(args.bxyz)

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from math import *
import os
from yt.mods import *
import glob
import Sim_constants as sc

fontsize = 18
mpl.rcParams['font.size'] = fontsize
mpl.rcParams['xtick.labelsize'] = fontsize
mpl.rcParams['ytick.labelsize'] = fontsize
mpl.rcParams['axes.titlesize'] = fontsize
mpl.rcParams['axes.labelsize'] = fontsize
mpl.rcParams['legend.fontsize'] = fontsize
mpl.rcParams['axes.linewidth'] = 2.0
mpl.rcParams['lines.linewidth'] = 2.0
mpl.rcParams['lines.markeredgewidth'] = 1.0
#mpl.rcParams['text.usetex'] = True

P0 = args.den0*args.cs0**2
beta = 2.*P0/max(bx0**2+by0**2+bz0**2,1.e-300)
Mach = sqrt(vx0**2+vy0**2+vz0**2)/args.cs0
cos_angle = (bx0*vx0 + by0*vy0 + bz0*vz0) / sqrt((vx0**2+vy0**2+vz0**2)* max(bx0**2+by0**2+bz0**2,1.e-300))
if cos_angle == 1.0: dir='par'
if cos_angle == 0.0: dir='per'

if args.Geq1: bigG= 1.
else: bigG= sc.G
def mdotABH(Mach,msink,cs0,den0,dir):
    LAMBDA = 1.1204222675845161
    NB = 1.
    BETAB = 19.4
    mdotB = 4. * pi * den0 * LAMBDA * (bigG * msink)**2 /cs0**3
    MachABH = (((1.+Mach**4)**(1./3.)/(1.+(Mach/LAMBDA)**2)**(1./6.))**NB + (BETAB/beta)**(NB/2.))**(1./NB)
    MachBH = (1.+Mach**4)**(1./3.)/(1.+(Mach/LAMBDA)**2)**(1./6.)
    # parallel
    if dir=='par': mdot = mdotB/MachBH**2*(MachBH**NB+(BETAB/beta)**(NB/2.))**(-1./NB)
    # perpendicular
    if dir=='per': mdot = mdotB/MachBH*min(1./MachBH**2,1./MachABH)
    return mdot

def Mdot_BH(rho_inf,Msink,cs,MachS):
    lam=1.12
    head=4.*np.pi*rho_inf*(bigG*Msink)**2/cs**3
    tail= ((lam**2+MachS**2)/(1+MachS**2)**4)**0.5
    return head*tail

def Mdot_0(Msink,rho_inf,MachS,cs):
    return 4*np.pi*rho_inf*(bigG*Msink)**2/(MachS*cs)**3

def Mdot_B(Msink,rho_inf,cs):
    lam=1.12
    return 4.*np.pi*lam*(bigG*Msink)**2*rho_inf/cs**3

class Mdots():
    def __init__(self,Msink,rho_inf,cs,MachS,dir):
        self.B= Mdot_B(Msink,rho_inf,cs)
        self.BH= Mdot_BH(rho_inf,Msink,cs,MachS)
        self.M0= Mdot_0(Msink,rho_inf,MachS,cs)
        self.ABH= mdotABH(MachS,Msink,cs,rho_inf,dir)


m=[]
#mana = []
t=[]
fdata= glob.glob(args.path+'data.*.3d.hdf5')
fdata.sort()  #gets times consecutive
fsink=[]
for f in fdata: fsink.append( f[:-4]+'sink')
for fs,fd in zip(fsink,fdata):
    f = open(fs)
    line = f.readline()
    line = f.readline()
    f.close()
    m.append(float(line.strip(' \n').split(' ')[0]))
    pf = load(fd)
    t.append(pf.current_time)
    #mana.append(t[-1]*mdot)
m=np.array(m)
t= np.array(t)

mdot= Mdots(m[0],args.den0,args.cs0,Mach,dir)

s_mdot= np.zeros(m.shape)-1
s_mdot[0]=0.
deltaM= m[1:] -m[:-1]
deltaT= t[1:]- t[:-1]
s_mdot[1:]= deltaM/deltaT
#check that all times are consecutive, starting from t=0

if args.problem == 'lee14':
    print "steady state: s_mdotNorm[0]= %g, s_mdotNorm[-5:,0].mean()= %g" % \
            (s_mdot[0]/mdot.B, s_mdot[-5:].mean()/mdot.B)
elif args.problem == 'krum06':
    print "steady state: s_mdotNorm[0]= %g, s_mdotNorm[-5:,0].mean()= %g" % \
            (s_mdot[0]/mdot.M0,s_mdot[-5:].mean()/mdot.M0)
title= "steady state: mdotNorm[-5:,0].mean()= %.2f" % (s_mdot[-5:].mean()/mdot.B/args.answer)
 
if args.fname: name= "mdot_sinkFiles_" +args.fname+".png"
else: name= "mdot_sinkFiles.png"
if args.problem == 'lee14':
    tB=bigG*m[0]/(args.cs0)**3
    print "tB= %g, msink= %g" % (tB,m[0])
    time=t/tB 
    plt.figure().subplots_adjust(left=0.25)
    plt.plot(time,s_mdot/mdot.B/args.answer,'k-',label='Kaylan/Ramses')
    plt.gca().set_xlabel(r'$t/t_B$')
    plt.gca().set_ylabel(r'$\dot{M}/ \dot{M}_{B}$ / $\dot{M}_{Ramses}$ (Log Axis)')
    plt.gca().set_title(title)
    if args.LinearY == False: plt.gca().set_yscale("log")
    if args.xlim: plt.gca().set_xlim(args.xlim[0],args.xlim[1])
    if args.ylim: plt.gca().set_ylim(args.ylim[0],args.ylim[1])
    plt.savefig(name)
    plt.close()
elif args.problem == 'krum06':
    tBH=bigG*m[0]/(Mach*args.cs0)**3
    print "tBH= %g, msink= %g" % (tBH,m[0])
    plt.figure().subplots_adjust(left=0.25)
    plt.plot(t/tBH,s_mdot/mdot.M0)
    if args.LinearY == False: plt.gca().set_yscale("log")
    plt.gca().set_xlabel(r'$t/t_BH$')
    plt.gca().set_ylabel(r'$(\dot{M}/ \dot{M}_{0}$')
    plt.gca().set_title("Beta = %.1g, Mach= %.1f" % (beta,Mach))
    plt.savefig(name)
    plt.close()

   
#mass versus time
#m = m - m[0]
#
#plt.figure().subplots_adjust(left=0.25)
#plt.plot(t,m)
#plt.plot(t,mana)
#plt.gca().set_xlabel(r'$t/t_B$')
#plt.gca().set_ylabel(r'$(\dot{M} t -M_0)\,/\, M_0$')
#plt.gca().set_title(r'$\beta=$ %.3f'%beta+r',   Mach = '+str(Mach))
#plt.savefig('bondi.png')
#plt.close()
#
#mdot_code = (m[-1]-m[-2]) / (t[-1]-t[-2])
#error = abs(mdot_code-mdot)/mdot
#
## error criteria setup for beta=1.0, Mach=1.41, direction=parallel
## run to t = 1 t_B on a 32 r_B^3 domain with 32^3 cells and 2 AMR levels.
#print error
#if error < 0.15:
#    print 'Pass!'
#    writer=open('test_pass', 'w')
#    writer.close()
#else:
#    print 'FAIL!'
#
#mdot versus time


