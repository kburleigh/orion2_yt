#PBS -q serial
#PBS -l pvmem=35GB
#PBS -l walltime=00:15:00
#PBS -N origK06icDrive
#PBS -j oe
#PBS -A m231

cd $PBS_O_WORKDIR
python Sim_K06_Fig4_Fig8.py -f_hdf5 data.0079.3d.hdf5 -cs 1. -msink 10. -Geq1 1 -ytVersion2 1
