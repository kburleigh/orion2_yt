import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-curves",choices=['all','best'],action="store",help='all: predictions using best-fits for median, mean respecitvely. best: best predictions obtained for any combination of these',required=True)
args = parser.parse_args()

import matplotlib.pyplot as plt
import numpy as np
import os
import pickle

fig,axes=plt.subplots(2,1,sharex=True)
fig.set_size_inches(8.,10.)
plt.subplots_adjust(hspace=0)
ax=axes.flatten()

#initialize all_beta dict
fin=open('b0.01/data_and_phis.pickle','r')
(data,phi_mean,phi_med)=pickle.load(fin)
fin.close()
allb_phi_mean={}
allb_phi_med={}
allb_phi_mean['beta']=[]
allb_phi_med['beta']=[]
for key in phi_mean.keys(): 
    allb_phi_mean[key]=[]
    allb_phi_med[key]=[]

#get data
for bet in ['0.01','0.1','1','10','100']:
    fin=open('b%s/data_and_phis.pickle' % bet,'r')
    (data,phi_mean,phi_med)=pickle.load(fin)
    fin.close()
    allb_phi_mean['beta'].append(float(bet))
    allb_phi_med['beta'].append(float(bet))
    for key in phi_mean.keys(): 
        allb_phi_mean[key].append( phi_mean[key] )
        allb_phi_med[key].append( phi_med[key] )
#convert to np arrays
for key in allb_phi_mean.keys():
    allb_phi_mean[key]=np.array(allb_phi_mean[key])
    allb_phi_med[key]=np.array(allb_phi_med[key])
include_keys_all_mean=['turb','cunning12','lee14_par','lee14_perp',\
                'mean_reg_par','mean_rms_par','mean_reg_perp','mean_rms_perp']
include_keys_all_med=['turb','cunning12','lee14_par','lee14_perp',\
                'med_reg_par','med_rms_par','med_reg_perp','med_rms_perp']
include_keys_best_mean=['med_reg_perp','med_rms_perp','logavg_med_reg_rms_perp']
include_keys_best_med=['mean_rms_par']
if args.curves == 'all': mean_keys,med_keys= include_keys_all_mean,include_keys_all_med
else: mean_keys,med_keys= include_keys_best_mean,include_keys_best_med
# include_keys_best=['med_reg_perp','med_rms_perp','logavg_med_reg_rms_perp']
#plot
colors=['k','b','m','r','g','y','c','#00CCFF','#339933','0.8']
#MEAN
cnt=0
for key in mean_keys: 
    ax[0].plot(allb_phi_mean['beta'],allb_phi_mean[key]/allb_phi_mean['data'],\
               c=colors[cnt],lw=2,label=key)
    cnt+=1
ax[0].hlines(1.,allb_phi_mean['beta'].min()/2,allb_phi_mean['beta'].max()*2,color='k',linestyle='dashed',lw=2,label='Answer')
#MEDIAN
cnt=0
for key in med_keys: 
    ax[1].plot(allb_phi_med['beta'],allb_phi_med[key]/allb_phi_med['data'],\
               c=colors[cnt],lw=2,label=key)
    cnt+=1
#phi_mean actually predicts median in this case
if args.curves == 'best':
	ax[1].plot(allb_phi_med['beta'],allb_phi_mean['k06_lee14par_med_rms_perp']/allb_phi_med['data'],\
               c=colors[cnt],lw=2,label='k06_lee14par_med_rms_perp')
ax[1].hlines(1.,allb_phi_med['beta'].min()/2,allb_phi_med['beta'].max()*2,color='k',linestyle='dashed',lw=2,label='Answer')
#finish plot
for i in range(2):
    ax[i].legend(loc=(1.01,0),fontsize='small',handlelength=3)
    ax[i].set_xscale('log')
#     ax[i].set_yscale('log')
    ax[i].set_xlim(1e-2/2,1e2*2)
if args.curves == 'all':
	ax[0].set_ylim(2e-1,30)
	ax[1].set_ylim(2e-1,30)
else:
	ax[0].set_ylim(7e-1,2)
	ax[1].set_ylim(7e-1,2)
ax[0].text(0.5, 0.92,'Predicted Mean/Mean(data)', horizontalalignment='center',verticalalignment='center',transform=ax[0].transAxes,fontsize='large')
ax[1].text(0.5, 0.92,'Predicted Median/Median(data)', horizontalalignment='center',verticalalignment='center',transform=ax[1].transAxes,fontsize='large')
for i in range(2): ax[i].set_yscale('log')
#ax[0].set_title("Predictions for Mean, Median")
plt.savefig('predictions_mean_median.png',bbox_inches='tight')

if args.curves == 'best': #print best predictions
	print '#Mean prediction'
	print 'beta: ',allb_phi_mean['beta'] 
	for key in mean_keys: print '%s: ' % key,allb_phi_mean[key]
	print '#Median prediction'
	print 'beta: ',allb_phi_med['beta'] 
	for key in med_keys: print '%s: ' % key,allb_phi_med[key]
	print 'k06_lee14par_med_rms_perp: ',allb_phi_mean['k06_lee14par_med_rms_perp']
