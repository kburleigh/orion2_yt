#!/bin/bash
#if script.tot already exists rm it
tmp=`ls|grep '^script.tot$' -c`
if [ $tmp = 1 ]
then
  rm script.tot
fi
#output commands to file 'script.tot'
for i in $( ls ../../data.0*.3d.hdf5 )
do
# Sim_Slice.py
#    echo python Sim_Slice.py -fhdf5 ${i} -plotSlice True -dir z -center -7.534 0. 0. -pow10 17 0 0 -width 64 32 -msink 2.e33 -mach 3 -cs 0.188e5 -sinklev 4 -name box_rho -cbar 1. 18. -cbarPow -25 -25 >> script.tot
# Sim_Slice_and_1Dplot.py
  echo python Sim_Slice_and_1Dplot.py -fhdf5 ${i} -dir z -isBox False -center 7.534e17 0. -neg 1 0 -cs 0.188e5 -rho0 1.e-25 -name compare -rho_limits 1 1e3 -vel_limits -15 15 >> script.tot
done
