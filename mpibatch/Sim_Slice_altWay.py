'''alternative way to make Slice/Proj plots: convert yt's Fixed Resolution Buffer images into numpy arrays matplotlib can work with using SPECIAL REQUIRED yt function "slc.data_source.to_frb()" i previously did not know of. 

Also, allows easy multi-plotting... 

functionality not hooked up to MPI yet, just awaiting the NEED'''
#my MODIFIED version below, see
#http://yt-project.org/doc/cookbook/complex_plots.html  -> Multi-Plot Slice and Projections
#for original yt version
from yt.visualization.base_plot_types import get_multi_plot
import matplotlib.colorbar as cb
from matplotlib.colors import LogNorm

orient = 'horizontal'
ds = yt.load(f_drive_hdf5[-1]) # load data

# There's a lot in here:
#   From this we get a containing figure, a list-of-lists of axes into which we
#   can place plots, and some axes that we'll put colorbars.
# We feed it:
#   Number of plots on the x-axis, number of plots on the y-axis, and how we
#   want our colorbars oriented.  (This governs where they will go, too.
#   bw is the base-width in inches, but 4 is about right for most cases.
fig, axes, colorbars = get_multi_plot(2, 1, colorbar=orient, bw = 4)

slc = yt.SlicePlot(ds, 'z', fields=["density","velocity_magnitude"])
proj = yt.ProjectionPlot(ds, 'z', "density", weight_field="density")

slc_frb = slc.data_source.to_frb((1.0, "cm"), 512)
proj_frb = proj.data_source.to_frb((1.0, "cm"), 512)

print axes

dens_axes = axes[0][0]
vel_axes = axes[0][1]

dens_axes.xaxis.set_visible(False)
dens_axes.yaxis.set_visible(False)
vel_axes.xaxis.set_visible(False)
vel_axes.yaxis.set_visible(False)


# Converting our Fixed Resolution Buffers to numpy arrays so that matplotlib
# can render them

proj_dens = np.array(proj_frb['density'])
slc_vel = np.array(slc_frb['velocity_magnitude'])

plots = [dens_axes.imshow(proj_dens, origin='lower', norm=LogNorm()),
         vel_axes.imshow(slc_vel, origin='lower', norm=LogNorm())]
         
# plots[0].set_clim((1.0e-27,1.0e-25))
# plots[0].set_cmap("bds_highcontrast")
# plots[1].set_clim((1.0e-27,1.0e-25))
# plots[1].set_cmap("bds_highcontrast")

titles=[r'$\mathrm{Density}\ (\mathrm{g\ cm^{-3}})$', 
        r'$\mathrm{Velocity Magnitude}\ (\mathrm{cm\ s^{-1}})$']

cbar = fig.colorbar(plots[0], cax=colorbars[0], orientation=orient)
cbar.set_label(titles[0])
cbar = fig.colorbar(plots[1], cax=colorbars[1], orientation=orient)
cbar.set_label(titles[1])

# And now we're done! 
fig.savefig("image.png")
