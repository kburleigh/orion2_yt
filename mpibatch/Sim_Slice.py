'''Slice or Projection plots with width in Numbers of dxmin cells but xy ticks in units of user choice: rB, rBH, etc

 -- Fortan MPI (embarrasingly parallel) using Dr. Peter Nugent's mpibatch scripts
 -- serial also optional   

MPI: edit make_script.tot.sh
     then submit on however many cores need! 
Serial: run this script as normal
'''

import argparse
import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import glob
import Sim_constants as sc
#read in from command line
#required
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-fhdf5",action="store",help='hdf5 filename, if wildcard expression then will find all matches and loop over them', required=True)
parser.add_argument("-plotSlice",action="store",type=bool,help='True to plot a slice, False for Projection', required=True)
parser.add_argument("-field",choices=['density','velMag'],action="store",help='field to plot', required=True)
parser.add_argument("-vectors",choices=['vel','bfield'],action="store",help='field to plot', required=True)
parser.add_argument("-factor",type=int,action="store",help='integer multiple of 16 -- quiver plot, skip every "factor" data points. yt says 16 is good', required=True)
parser.add_argument("-center",nargs=3,metavar=('x','y','z'),type=float,action="store",help='usually location of sink particle',required=True)
parser.add_argument("-neg",nargs=3,metavar=('x','y','z'),type=int,action="store",help='1 if negative, 0 if positive', required=True)
parser.add_argument("-wid_Ndxmin",nargs=2,metavar=('horiz','vert'),type=int,action="store",help='horiz,vertical widths, in units of number of dx min celss', required=True)
parser.add_argument("-scale",choices=['rB','rBH'],action="store",help='label axis of 2D plot in units of scale', required=True)
parser.add_argument("-msink",action="store",type=float,help='mass sink when insert', required=True)
parser.add_argument("-mach",action="store",type=float,help='3D mach number when insert sink(s)', required=True)
parser.add_argument("-cs",action="store",type=float,help='sound speed', required=True)
parser.add_argument("-rho0",action="store",type=float,help='sound speed', required=True)
parser.add_argument("-sinklev",action="store",type=int,help='level sink is on', required=True)
parser.add_argument("-name",action="store",help='unique file name to append to front of default naming scheme', required=True)
#optional
parser.add_argument("-cbar",nargs=2,metavar=('min','max'),type=float,action="store",help='color bar and plot limits',required=False)
#get args
args = parser.parse_args()
if args.neg[0] == 1: args.center[0]= -1*args.center[0]
if args.neg[1] == 1: args.center[1]= -1*args.center[1]
if args.neg[2] == 1: args.center[2]= -1*args.center[2]

#everything else
rBH= sc.G*args.msink/args.cs**2/(1+args.mach**2)

def _KaysVx(field, data):
    return data["X-momentum"] / data["density"]
def _KaysVy(field, data):
    return data["Y-momentum"] / data["density"]
def _KaysVz(field, data):
    return data["Z-momentum"] / data["density"]
def _KaysVmag(field, data):
    return (data["KaysVx"]**2 + data["KaysVy"]**2 +data["KaysVz"]**2)**0.5
def _KaysBx(field, data):
    rt4pi= 3.5449
    return data["X-magnfield"]*rt4pi
def _KaysBy(field, data):
    rt4pi= 3.5449
    return data["Y-magnfield"]*rt4pi
def _KaysBz(field, data):
    rt4pi= 3.5449
    return data["Z-magnfield"]*rt4pi
#def _velxNorm(field, data):
#    return data["velx"] / data.get_field_parameter("cs")
#def _rhoNorm(field, data):
#    return data["density"]/data.get_field_parameter("rho0")

flist= glob.glob(args.fhdf5)
#for myf in flist:
ds1 = yt.load(flist[0])
ds1.add_field("KaysVx", function=_KaysVx, units="cm/s")
ds1.add_field("KaysVy", function=_KaysVy, units="cm/s")
ds1.add_field("KaysVz", function=_KaysVz, units="cm/s")
ds1.add_field("KaysVmag", function=_KaysVmag, units="cm/s")
ds1.add_field("KaysBx", function=_KaysBx, units="gauss")
ds1.add_field("KaysBy", function=_KaysBy, units="gauss")
ds1.add_field("KaysBz", function=_KaysBz, units="gauss")
#following not recognized by yt, not sure why
#ad = ds1.all_data()  
#ad.set_field_parameter("cs", args.cs)
#ad.set_field_parameter("rho0", args.rho0)
#ds1.add_field("velMagNorm", function=_velMagNorm, units="cm/s")
#ds1.add_field("velxNorm", function=_velMagNorm, units="cm/s")
#ds1.add_field("rhoNorm", function=_rhoNorm, units="g/cm**3")

dxmin= np.array(ds1.domain_width).max()/ds1.domain_dimensions.max()/2**args.sinklev
width= (args.wid_Ndxmin[0]*dxmin,args.wid_Ndxmin[1]*dxmin)
if width[0] >= np.array(ds1.domain_width).max() or width[1] >= np.array(ds1.domain_width).max():
    print 'WARNING: plot may wrap domain around to other side, requested width ~ domain width'
if args.scale == 'rBH':
    scale= sc.G*args.msink/args.cs**2/(1+args.mach**2)  
else:
    scale= sc.G*args.msink/args.cs**2  
ds1.unit_registry.add(args.scale,scale,yt.units.dimensions.length)
#x
dir='x'
if args.plotSlice: 
    px= yt.SlicePlot(ds1,dir,args.field, center=args.center,\
                width=((width[0],'cm'),(width[1],'cm')),axes_unit=args.scale)
else: 
    px= yt.ProjectionPlot(ds1,dir,args.field, center=args.center,\
                width=((width[0],'cm'),(width[1],'cm')), max_level=args.sinklev,axes_unit=args.scale)
px.annotate_grids(min_level=0,max_level=args.sinklev)
if args.vectors == 'vel': px.annotate_streamlines('KaysVy','KaysVz',factor=args.factor)
else: px.annotate_streamlines('KaysBy','KaysBz',factor=args.factor)
if args.cbar != None:
    px.set_zlim('density',args.cbar[0],args.cbar[1])
    print 'setting color bar limits to: (%g,%g)' % (args.cbar[0],args.cbar[1])
if args.plotSlice: px.save(args.name+'_Slice_%s_%s.png' % (dir,ds1.basename[5:9]))
else: px.save(args.name+'_Proj_%s_%s.png' % (dir,ds1.basename[5:9]))
#y
dir='y'
if args.plotSlice: 
    px= yt.SlicePlot(ds1,dir,args.field, center=args.center,\
                width=((width[0],'cm'),(width[1],'cm')),axes_unit=args.scale)
else: 
    px= yt.ProjectionPlot(ds1,dir,args.field, center=args.center,\
                width=((width[0],'cm'),(width[1],'cm')), max_level=args.sinklev,axes_unit=args.scale)
px.annotate_grids(min_level=0,max_level=args.sinklev)
#in a y projection, yt puts z on x-axis and x on y-axis 
if args.vectors == 'vel': px.annotate_streamlines('KaysVz','KaysVx',factor=args.factor)
else: px.annotate_streamlines('KaysBz','KaysBx',factor=args.factor)
if args.cbar != None:
    px.set_zlim('density',args.cbar[0],args.cbar[1])
    print 'setting color bar limits to: (%g,%g)' % (args.cbar[0],args.cbar[1])
if args.plotSlice: px.save(args.name+'_Slice_%s_%s.png' % (dir,ds1.basename[5:9]))
else: px.save(args.name+'_Proj_%s_%s.png' % (dir,ds1.basename[5:9]))
#z
dir='z'
if args.plotSlice: 
    px= yt.SlicePlot(ds1,dir,args.field, center=args.center,\
                width=((width[0],'cm'),(width[1],'cm')),axes_unit=args.scale)
else: 
    px= yt.ProjectionPlot(ds1,dir,args.field, center=args.center,\
                width=((width[0],'cm'),(width[1],'cm')), max_level=args.sinklev,axes_unit=args.scale)
px.annotate_grids(min_level=0,max_level=args.sinklev)
if args.vectors == 'vel': px.annotate_streamlines('KaysVx','KaysVy',factor=args.factor)
else: px.annotate_streamlines('KaysBx','KaysBy',factor=args.factor)
if args.cbar != None:
    px.set_zlim('density',args.cbar[0],args.cbar[1])
    print 'setting color bar limits to: (%g,%g)' % (args.cbar[0],args.cbar[1])
if args.plotSlice: px.save(args.name+'_Slice_%s_%s.png' % (dir,ds1.basename[5:9]))
else: px.save(args.name+'_Proj_%s_%s.png' % (dir,ds1.basename[5:9]))
