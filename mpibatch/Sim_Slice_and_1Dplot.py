'''make slice image of velocity (any combo of vx,vy,vz) and density, normalized by sound speed and mean density respectively, then plot 1D profile in horiz on vertical directions through the center, which is asumed to be the sink particle's location

ex run command: python Sim_Slice_and_1Dplot.py -fhdf5 ../../data.0005.3d.hdf5 -dir z -center 7.534e17 0. 0. -neg 1 0 0 -cs 0.188e5 -name test -cbar_rho 1e-25 1e-23 -cbar_vel 1 3

Slice plot with 1D profile in horizontal and vertical directions through center of slice plot
                -- Fortan MPI (embarrasingly parallel) using Dr. Peter Nugent's mpibatch scripts
                -- serial also optional   

MPI: edit make_script.tot.sh
     then submit on however many cores need! 
Serial: run this script as normal

Notes:
-center, -pow10 : these are a hack to get around not being able give argparse sci notation like 1e17
    ex) if sink is centered at (-1.345e17,0,2.e4)
        -center -1.345 0 2. -pow10 17 0 4 
Choose size image in units of coarse cell size. so width=64 would image entire box if 64^3 base grid. 
Labels x,y,z in units of rBH or whatever like'''

import argparse
import matplotlib as mpl
mpl.use('Agg')
fontsize = 25
mpl.rcParams['font.size'] = fontsize
mpl.rcParams['xtick.labelsize'] = fontsize
mpl.rcParams['ytick.labelsize'] = fontsize
mpl.rcParams['axes.titlesize'] = fontsize
mpl.rcParams['axes.labelsize'] = fontsize
mpl.rcParams['legend.fontsize'] = fontsize
mpl.rcParams['axes.linewidth'] = 2.0
mpl.rcParams['lines.linewidth'] = 2.0
mpl.rcParams['lines.markeredgewidth'] = 1.0
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import yt
import numpy as np
import Sim_constants as sc
#read in from command line
#required
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-fhdf5",action="store",help='hdf5 filename', required=True)
parser.add_argument("-dir",action="store",help='x,y, or z, slice direction', required=True)
parser.add_argument("-isBox",type=bool,action="store",help='True if domain is cube, False if rectangle', required=True)
parser.add_argument("-center",nargs=2,metavar=('x','y'),type=float,action="store",help='xy or xz or yz, MUST AGREE with dir given', required=True)
parser.add_argument("-neg",nargs=2,metavar=('x,y,z','x,y,z'),type=int,action="store",help='1 if negative, 0 if positive center value', required=True)
#parser.add_argument("-width",nargs=2,metavar=('horiz','vert'),type=int,action="store",help='horiz,vertical widths, in units of cell size on base level, so 64 if 64^3 base grid and want slice over entire box', required=True)
parser.add_argument("-cs",action="store",type=float,help='sound speed', required=True)
parser.add_argument("-rho0",action="store",type=float,help='sound speed', required=True)
parser.add_argument("-name",action="store",help='unique file name to append to front of default naming scheme', required=True)
#optional
parser.add_argument("-rho_limits",nargs=2,metavar=('min','max'),type=float,action="store",help='color bar and plot limits')
parser.add_argument("-vel_limits",nargs=2,metavar=('min','max'),type=float,action="store",help='color bar and plot limits')
#get args
args = parser.parse_args()
if args.neg[0] == 1: args.center[0]= -1*args.center[0]
if args.neg[1] == 1: args.center[1]= -1*args.center[1]


def _velx(field, data):
    return data["X-momentum"] / data["density"]
def _vely(field, data):
    return data["Y-momentum"] / data["density"]
def _velz(field, data):
    return data["Z-momentum"] / data["density"]
def _velMag(field, data):
    return (data["velx"]**2 + data["vely"]**2 +data["velz"]**2)**0.5

ds = yt.load(args.fhdf5)
ds.add_field("velx", function=_velx, units="cm/s")
ds.add_field("vely", function=_vely, units="cm/s")
ds.add_field("velz", function=_velz, units="cm/s")
ds.add_field("velMag", function=_velMag, units="cm/s")
slc = yt.SlicePlot(ds, args.dir, fields=["density"])
slcV = yt.SlicePlot(ds, args.dir, fields=["velx"])

slc_frb= slc.data_source.to_frb((ds.domain_width[0],"cm"),slc.buff_size[0])
rho= np.array(slc_frb["density"])/args.rho0
slcV_frb= slcV.data_source.to_frb((ds.domain_width[0],"cm"),slcV.buff_size[0])
velMag= np.array(slcV_frb["velx"])/args.cs

#if rectangular domain, assume horiz 2x length of vertical, extract
if args.isBox == False:
    rho=rho[200:600,:]
    velMag=velMag[200:600,:]

#convert center in simulation units to index units of np array which is the fixed res buff
#assume shape of rho == shape of velMag
if rho.shape != velMag.shape: raise ValueError('shapes not equal! rho is %s, velMag is %s' % (rho.shape,velMag.shape) )
frb_domain_x= np.array(slc.xlim) 
frb_domain_y= np.array(slc.ylim) 
#shape gives y,x sizes, so shape[1] for x direction
scale_x= float(rho.shape[1])/(frb_domain_x[1]-frb_domain_x[0])
scale_y= float(rho.shape[0])/(frb_domain_y[1]-frb_domain_y[0])
#now convert center location to an index in array
if frb_domain_x[0] < 0: #simulation domain symmetic about 0, but indices > 0, so shift center so its > 0
    shift_x= -1*frb_domain_x[0]
    shift_y= -1*frb_domain_y[0]
cent_index_x= np.round_((args.center[0]+shift_x)*scale_x,decimals=0).astype('int')
cent_index_y= np.round_((args.center[1]+shift_y)*scale_y,decimals=0).astype('int')


fig,axis= plt.subplots(3,2,figsize=(25,20))
ax= axis.flatten()
cax= ax[0].imshow(velMag,origin='lower') #, norm=LogNorm())
cax.set_clim(args.vel_limits[0],args.vel_limits[1])
cax.set_cmap("bds_highcontrast")
pos=ax[0].get_position()
# print pos.x0,pos.x1,pos.y0,pos.y1,pos.width,pos.height, pos
cax_pos = fig.add_axes([pos.x1,pos.y0,0.02, pos.height])
cbar=fig.colorbar(cax,cax=cax_pos,orientation='vertical')
ax[0].set_title("Vx / Cs")
#slice through center
ax[0].hlines(cent_index_y,ax[0].get_xlim()[0],ax[0].get_xlim()[1],\
            linestyles='dashed',colors='black')
ax[0].vlines(cent_index_x,ax[0].get_ylim()[0],ax[0].get_ylim()[1],\
            linestyles='dashed',colors='black')
ax[2].plot(range(velMag.shape[1]),velMag[cent_index_y,:]) #horiz slice 
ax[2].set_xlabel("horizontal slice")
ax[4].plot(range(velMag.shape[0]),velMag[:,cent_index_x]) #vert slice
ax[4].set_xlabel("vertical slice")
ax[2].set_ylabel("Vx/cs")
ax[4].set_ylabel("Vx/cs")
ax[2].set_ylim(args.vel_limits[0],args.vel_limits[1])
ax[4].set_ylim(args.vel_limits[0],args.vel_limits[1])

cax= ax[1].imshow(rho,origin='lower',norm=LogNorm())
cax.set_clim(args.rho_limits[0],args.rho_limits[1])
cax.set_cmap("bds_highcontrast")
pos=ax[1].get_position()
cax_pos = fig.add_axes([pos.x1,pos.y0,0.02, pos.height])
cbar=fig.colorbar(cax,cax=cax_pos,orientation='vertical')
ax[1].set_title("Density/ rho0")
ax[1].hlines(cent_index_y,ax[1].get_xlim()[1],ax[1].get_xlim()[1],\
            linestyles='dashed',colors='black')
ax[1].vlines(cent_index_x,ax[1].get_ylim()[1],ax[1].get_ylim()[1],\
            linestyles='dashed',colors='black')
ax[3].plot(range(rho.shape[1]),rho[cent_index_y,:])
ax[3].set_xlabel("horizontal slice")
ax[5].plot(range(rho.shape[0]),rho[:,cent_index_x])
ax[5].set_xlabel("vertical slice")
ax[3].set_yscale("log")
ax[5].set_yscale("log")
ax[3].set_ylabel("density")
ax[5].set_ylabel("density")
ax[3].set_ylim(args.rho_limits[0],args.rho_limits[1])
ax[5].set_ylim(args.rho_limits[0],args.rho_limits[1])
plt.subplots_adjust(hspace=0.3,wspace=0.3)
plt.savefig(args.name+'_Slice_%s_%s.png' % (args.dir,ds.basename[5:9]))



#dx_base= np.array(ds1.domain_width).max()/ds1.domain_dimensions.max()
#ds1.unit_registry.add('rBH',rBH,yt.units.dimensions.length)
#width= (args.width[0]*dx_base,args.width[1]*dx_base)
#if args.plotSlice: 
#    px= yt.SlicePlot(ds1,args.dir,"density", center=center,width=width,axes_unit='rBH')
#else: 
#    px= yt.ProjectionPlot(ds1,args.dir,"density", center=center,width=width, max_level=args.sinklev,axes_unit='rBH')
#px.annotate_grids(min_level=0,max_level=args.sinklev)
#if (args.cbar != None) and (args.cbarPow != None):
#    px.set_zlim('density',cbar[0],cbar[1])
#    print 'setting color bar limits to: (%g,%g)' % (cbar[0],cbar[1])
#if args.plotSlice: px.save(args.name+'_Slice_%s_%s.png' % (args.dir,ds1.basename[5:9]))
#else: px.save(args.name+'_Proj_%s_%s.png' % (args.dir,ds1.basename[5:9]))
