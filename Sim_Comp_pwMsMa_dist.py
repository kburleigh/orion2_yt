'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and plot vs. time'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-Lbox",action="store",help='length of box in cm')
parser.add_argument("-cs",action="store",help='adiabatic sound speed in cm/sec')
parser.add_argument("-tb",action="store",help='bondi time in sec')
args = parser.parse_args()
if args.Lbox: Lbox = float(args.Lbox)
else: raise ValueError
if args.cs: cs_ad = float(args.cs)
else: raise ValueError
if args.tb: tB = float(args.tb)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob
from pickle import dump as pdump
  
class QsOfInterest():
    def __init__(self,rho,vx,vy,vz,bx,by,bz,cs_iso,curl_mag):
#         vrms= (vx**2+vy**2+vz**2)**0.5
        vrms2= (vx**2+vy**2+vz**2)
        vrms2_MassWt= np.average(vrms2,weights=rho)
        self.MachS= vrms2_MassWt**0.5/cs_iso
        self.Rho= np.average(rho)
        self.Brms= np.average(bx**2+by**2+bz**2)**0.5
        self.Beta= 8*np.pi*self.Rho*cs_iso**2/self.Brms**2
        self.MachA= (self.Beta/2)**0.5*self.MachS
        self.Curl= np.average(curl_mag)

def CalcCurl(Vx,Vy,Vz, lev,domain_width_0,domain_dimensions_0):
    wid_cell= float(domain_width_0)/domain_dimensions_0/2**lev
    #finite diff
    dVy_dx= (Vy[:-1,:,:]-Vy[1:,:,:])/wid_cell
    dVz_dx= (Vz[:-1,:,:]-Vz[1:,:,:])/wid_cell
    dVx_dy= (Vx[:,:-1,:]-Vx[:,1:,:])/wid_cell
    dVz_dy= (Vz[:,:-1,:]-Vz[:,1:,:])/wid_cell
    dVx_dz= (Vx[:,:,:-1]-Vx[:,:,1:])/wid_cell
    dVy_dz= (Vy[:,:,:-1]-Vy[:,:,1:])/wid_cell
    #keep just regions that overlap
    dVy_dx= dVy_dx[:,:-1,:-1]
    dVz_dx= dVz_dx[:,:-1,:-1]
    dVx_dy= dVx_dy[:-1,:,:-1]
    dVz_dy= dVz_dy[:-1,:,:-1]
    dVx_dz= dVx_dz[:-1,:-1,:]
    dVy_dz= dVy_dz[:-1,:-1,:]
    #compute curl
    xhat= dVz_dy- dVy_dz
    yhat= -(dVz_dx - dVx_dz)
    zhat= dVy_dx- dVx_dy
    curl_mag= (xhat**2+yhat**2+zhat**2)**0.5
    return curl_mag

# def plot_TSeries_compare(fsave,ts,tB):
#     '''ts: returned by TimeSeries
#     tB: bondi time in units of simulation'''
#     f,axis= plt.subplots(2,3,figsize=(10,5))
#     ax=axis.flatten()
#     time= (ts.Time - ts.Time[0])/tB
#     ax[0].plot(time,ts.Rho,"k*")
#     ax[1].plot(time,ts.MachS,"k*")
#     ax[2].plot(time,ts.MachA,"k*")
#     ax[3].plot(time,ts.Beta,"k*",)
#     ax[4].plot(time,ts.Brms,"k*")
#     ax[5].plot(time,ts.Curl,"k*")
#     ylabs= ['Rho',"MachS","MachA","Beta","Brms",'|Curl|']
#     for i in range(6):
#         ax[i].set_xlabel("t/tB")
#         ax[i].set_ylabel(ylabs[i])
#     ax[0].legend(loc=(1.1,1.1),ncol=2)#bbox_to_anchor = (0.,1.4))
#     f.subplots_adjust(wspace=0.5,hspace=0.5)
#     f.savefig(fsave,dpi=150)   

class ValsInBox():
    def __init__(self):
        self.RhoDivRhobar=-1
        self.WSquig=-1
        self.MachS=-1
        self.MachA=-1

class DiffSims():
    def __init__(self):
        self.fsol1= ValsInBox()
        self.fsol067= ValsInBox()
        self.fsol033= ValsInBox()

def getHistVals(f_hdf5,cs_ad,Lbox):
    pf= load(f_hdf5)
    lev=0
    cube= pf.h.covering_grid(level=lev,\
                          left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions)
    rho = cube["density"]
    vx= cube["X-momentum"]/rho
    vy= cube["Y-momentum"]/rho
    vz= cube["Z-momentum"]/rho
    bx= (4*np.pi)**0.5*cube["X-magnfield"]
    by= (4*np.pi)**0.5*cube["Y-magnfield"]
    bz= (4*np.pi)**0.5*cube["Z-magnfield"]
    gamma=1.001
    cs_iso= cs_ad/gamma**0.5
    curl_mag= CalcCurl(vx,vy,vz, lev,pf.domain_width[0],pf.domain_dimensions[0])
    avgQ= QsOfInterest(rho,vx,vy,vz,bx,by,bz,cs_iso,curl_mag)   
    MachS=(vx**2+vy**2+vz**2)**0.5/cs_iso
    Bmag=(bx**2+by**2+bz**2)**0.5
    beta= 8*np.pi*rho*cs_iso**2/Bmag**2
    MachA=(beta/2)**0.5*MachS
    #
    vals=ValsInBox()
    vals.RhoDivRhobar= rho/np.average(rho)
    vals.WSquig= curl_mag*Lbox/avgQ.MachS/cs_iso
    vals.MachS= MachS
    vals.MachA= MachA
    return vals

def plot_hgrams(fsave,hists):
    f,axis=plt.subplots(2,2,figsize=(15,15))
    ax=axis.flatten()
    titles=['Density','Vorticity','Mach S','Mach A']
    xlab=[r'Log($\rho/\bar{\rho}$',r'Log$\tilde{\omega}$','r$M_s$','r$M_a$']
    ##
    y= np.log10(hists.fsol1.RhoDivRhobar.flatten())
    tup= ax[0].hist(y, bins=100,color="k",histtype='step',label="fsol= 1")
    y= np.log10(hists.fsol067.RhoDivRhobar.flatten())
    tup= ax[0].hist(y, bins=100,color="b",histtype='step',label="fsol= 2/3")
    y= np.log10(hists.fsol033.RhoDivRhobar.flatten())
    tup= ax[0].hist(y, bins=100,color="b",histtype='step',label="fsol= 1/3")
    ##
    y= np.log10(hists.fsol1.WSquig.flatten())
    tup= ax[1].hist(y, bins=100,color="k",histtype='step',label="fsol= 1")
    y= np.log10(hists.fsol067.WSquig.flatten())
    tup= ax[1].hist(y, bins=100,color="b",histtype='step',label="fsol= 2/3")
    y= np.log10(hists.fsol033.WSquig.flatten())
    tup= ax[1].hist(y, bins=100,color="b",histtype='step',label="fsol= 1/3")
    ##
    y= np.log10(hists.fsol1.MachS.flatten())
    tup= ax[2].hist(y, bins=100,color="k",histtype='step',label="fsol= 1")
    y= np.log10(hists.fsol067.MachS.flatten())
    tup= ax[2].hist(y, bins=100,color="b",histtype='step',label="fsol= 2/3")
    y= np.log10(hists.fsol033.MachS.flatten())
    tup= ax[2].hist(y, bins=100,color="b",histtype='step',label="fsol= 1/3")
    ##
    y= np.log10(hists.fsol1.MachA.flatten())
    tup= ax[3].hist(y, bins=100,color="k",histtype='step',label="fsol= 1")
    y= np.log10(hists.fsol067.MachA.flatten())
    tup= ax[3].hist(y, bins=100,color="b",histtype='step',label="fsol= 2/3")
    y= np.log10(hists.fsol033.MachA.flatten())
    tup= ax[3].hist(y, bins=100,color="b",histtype='step',label="fsol= 1/3")
    ##
    for cnt,axes in enumerate(ax):
        axes.set_xlabel(xlab[cnt])
        axes.set_title(title[cnt])
        axes.legend(loc=0)
    plt.savefig(fsave,dpi=150)

#MAIN
#3 dirs: fsol= 1, 0.67, 0.33
spath="./plots/"
few_files= ['data.0000.3d.hdf5','data.0016.3d.hdf5','data.0034.3d.hdf5']
path={}
path['fsol_1']='fsol_1/'
path['fsol_067']='./'
path['fsol_033']='fsol_0.33/'
fsol_1=[]
fsol_067=[]
fsol_033=[]
for name in few_files:
    fsol_1.append(path['fsol_1']+name)
    fsol_067.append(path['fsol_067']+name)
    fsol_033.append(path['fsol_033']+name)
fsol_1.sort()
fsol_067.sort()
fsol_033.sort()

for cnt in range(len(few_files)):
    print "comparing: %s AND %s AND %s" \
        % (fsol_1[cnt],fsol_067[cnt],fsol_033[cnt])
    hists=DiffSims()
    hists.fsol1= getHistVals(fsol_1[cnt],cs_ad,Lbox)
    hists.fsol067= getHistVals(fsol_067[cnt],cs_ad,Lbox)
    hists.fsol033= getHistVals(fsol_033[cnt],cs_ad,Lbox)
    #make plot
    fsave=spath+"hist_compare_"+few_files[cnt][5:9]+".png"
    plot_hgrams(fsave,hists)
    print "made histogram"
print "completed"



