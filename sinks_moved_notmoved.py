import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-init_sink",action="store",help='inital init.sink file',required=True)
parser.add_argument("-final_sink",action="store",help='final data.sink file',required=True)
parser.add_argument("-lbox",type=float,action="store",help='length of box is simulation units',required=True)
args = parser.parse_args()

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np

def read_init_sink(fname):
    '''fname: .sink file containins sink masses, locations, ids
	returns: tuple of masses,xyz positions, sink ids'''
    fin=open(fname,'r')
    lines=fin.readlines()
    fin.close
    nsinks= int(lines[0].strip().split(' ')[0])
    m=np.zeros(nsinks)-1
    x=m.copy()
    y=m.copy()
    z=m.copy()
    sid=m.copy()
    nsinks= int(lines[0].strip().split(' ')[0])
    m=np.zeros(nsinks)-1
    x=m.copy()
    y=m.copy()
    z=m.copy()
    sid=m.copy()
    for i,line in enumerate(lines[1:]):
        m[i]=float( line.strip().split(' ')[0] )
        x[i]=float( line.strip().split(' ')[1] )
        y[i]=float( line.strip().split(' ')[2] )
        z[i]=float( line.strip().split(' ')[3] )
        sid[i]=int( line.strip().split(' ')[-1] )
    return (m,x,y,z,sid)

#get sink data
(m1,x1,y1,z1,sid1) =read_init_sink(args.init_sink) 
(m2,x2,y2,z2,sid2) =read_init_sink(args.final_sink)
dx=x2-x1 #final - initial sink positions
dy=y2-y1
dz=z2-z1
distMoved= np.sqrt(dx**2+dy**2+dz**2)
fromCenter= np.sqrt(x1**2+y1**2+z1**2)
print x1
plt.scatter(fromCenter,distMoved/distMoved.max(),s=20)
plt.xlabel("distance from origin")
plt.ylabel("distance moved / max distance moved of any sink")
plt.show() 

print "dx shape",dx.shape

#text file listing sink ids that moved during 1st spike and those that did not
fin=open('sinks_moved_notmoved.txt','r')
lines=fin.readlines()
fin.close
imoved= np.array(lines[1].strip().split(' ')).astype(int)
istay= np.array(lines[3].strip().split(' ')).astype(int)
color= sid1.copy().astype(np.string_)


#yellow for sink ids that did NOT move, red for ids that MOVED
for i,sid in enumerate(sid1.astype(int)):
    itMoved=np.where(imoved == sid)[0]
    if len(itMoved) == 0: color[i]='y'  #not move
    else: color[i]='r' #moved
print 'color shape,color',color.shape,color

#3D plot
shape=(4,4,4)
fig = plt.figure()
ax = fig.gca(projection='3d')

ax.quiver(x1.reshape(shape), y1.reshape(shape), z1.reshape(shape), \
          dx.reshape(shape),dy.reshape(shape),dz.reshape(shape),\
          length=0.1,arrow_length_ratio=0.6)
ax.scatter(x1.reshape(shape)-0.1, y1.reshape(shape), z1.reshape(shape),\
           c=color.reshape(shape),s=60,depthshade=True)
ax.set_xlim(-args.lbox/2,args.lbox/2)
ax.set_ylim(-args.lbox/2,args.lbox/2)
ax.set_zlim(-args.lbox/2,args.lbox/2)
ax.set_title("red: MOVED, yellow:FIXED")
plt.show()
