import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5",action="store",help='hdf5 data file to calc Table from, ex: data.3d.0001.hdf5')
parser.add_argument("-cs",action="store",help='constant sound speed')
parser.add_argument("-Lbox",action="store",help='length simulation box, cm')
args = parser.parse_args()
if args.hdf5 and args.cs and args.Lbox: 
    hdf5= str(args.hdf5)
    cs= float(args.cs)
    Lbox = float(args.Lbox)
else: raise ValueError

import yt
yt.enable_parallelism()
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pickle
import my_fund_const as fc

class PhiVals():
    def __init__(self,phimean,phimed):
        self.phimean= phimean
        self.phimed= phimed

def MdotBH_x(rho_x,Msink,cs,MachS_x):
    one=4.*np.pi*rho_x*(fc.G*Msink)**2*cs**-3
    lam=1.1
    return one*(lam**2+MachS_x**2)/((1+MachS_x**2)**4)**0.5

def MdotW_x(Msink,rho_x,cs,vort_x):
    rB= fc.G*Msink/cs**2
    wstar_x= vort_x*rB/cs
    fstar_x= 1/(1+wstar_x**0.9)
    return 4*np.pi*rho_x*(fc.G*Msink)**2*cs**-3*0.34*fstar_x

def make_Fig4(fsave,Mturb_x,Mbh_x,Mw_x,M0):
    (cum_turb,cum_bins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True,cumulative=True)
    (cum_bh,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=cum_bins,normed=True,cumulative=True)
    (cum_w,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=cum_bins,normed=True,cumulative=True)
    (pdf_turb,pdf_bins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True)#,stacked=True)
    (pdf_bh,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=pdf_bins,normed=True)#,stacked=True)
    (pdf_w,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=pdf_bins,normed=True)#,stacked=True)
    fig,ax=plt.subplots(2,1,sharex=True)
    lab= [r'$\dot{M}_{Turb}$',r'$\dot{M}_{BH}$',r'$\dot{M}_{\omega}$']
    mybins= 10**((cum_bins[0:-1]+cum_bins[1:])/2)
    ax[0].plot(mybins,cum_turb,c='k',ls='-',label=lab[0])
    ax[0].plot(mybins,cum_bh,c='k',ls='--',label=lab[1])
    ax[0].plot(mybins,cum_w,c='k',ls=':',label=lab[2])
    # ax[0].set_xlabel(r'$\dot{M}/\dot{M}_0$')
    ax[0].set_ylabel(r'$P_M(<\dot{M}$)')
    ax[0].set_ylim(0,1)
    ax[0].set_xscale('log')
    ax[0].legend(loc=0)

    mybins= 10**((pdf_bins[0:-1]+pdf_bins[1:])/2)
    ax[1].plot(mybins,pdf_turb,c='k',ls='-',label=lab[0])
    ax[1].plot(mybins,pdf_bh,c='k',ls='--',label=lab[1])
    ax[1].plot(mybins,pdf_w,c='k',ls=':',label=lab[2])
    ax[1].set_xlabel(r'$\dot{M}/\dot{M}_0$')
    ax[1].set_ylabel(r'PDF')
    ax[1].set_xlim(1.e-2,1.e2)
    ax[1].set_xscale('log')
    fig.subplots_adjust(wspace=0.3,hspace=0.)
    # ax[1].legend(loc=0)
    plt.savefig(fsave,dpi=150)

#MAIN#
#store Table 1 vals
table= {}
#choice of rB sets Msink that you assume for Ansatz
log_rB_L= np.array([-5,-3,-1,-0.7,1])
Msink= 10.**log_rB_L*Lbox*cs**2/fc.G
#simulation props
spath='./plots/'
ds=yt.load(hdf5)
data=ds.all_data()
if yt.is_root(): print 'get rho,vel,Mach data'
rho_x=np.array(data['density']) #_x means have value at every cell in grid
vx_x=np.array(data['velocity_x'])
vy_x=np.array(data['velocity_y'])
vz_x=np.array(data['velocity_z'])
vrms2_x= np.average(vx_x**2+vy_x**2+vz_x**2,weights=rho_x)
MachS_x= vrms2_x**0.5/cs
MachS_0= np.average(MachS_x)
if yt.is_root(): print 'get Vorticity Mag'
vort_x= np.array(data[('gas', 'vorticity_magnitude')])
if yt.is_root(): print 'Done getting Vorticity Mag'
#loop over log(rB_L)
for rB_L,mass in zip(log_rB_L,Msink):
    # (rB_L,mass)=(log_rB_L[3],Msink[3])
    Mbh_x= MdotBH_x(rho_x,mass,cs,MachS_x)
    Mw_x= MdotW_x(mass,rho_x,cs,vort_x)
    Mturb_x= (Mbh_x**-2 + Mw_x**-2)**-0.5
    M0= 4.*np.pi*rho_x.mean()*(fc.G*mass)**2/(MachS_0*cs)**3
    phimean= np.mean(Mturb_x)/M0
    phimed= np.median(Mturb_x)/M0
    table[str(rB_L)]= PhiVals(phimean,phimed)
    # if str(rB_L) == '-0.7': 
#         fsave=spath+'K06_Fig4_Mach0fromData.png'
#         make_Fig4(fsave,Mturb_x,Mbh_x,Mw_x,M0)
if yt.is_root():
    print 'MachS= %f' % MachS_0
    print 'Log10(rB/L),\phi_{mean},\phi_{median}'
    for key in table.keys():
        print key, table[key].phimean,table[key].phimed
import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5",action="store",help='hdf5 data file to calc Table from, ex: data.3d.0001.hdf5')
parser.add_argument("-cs",action="store",help='constant sound speed')
parser.add_argument("-Lbox",action="store",help='length simulation box, cm')
args = parser.parse_args()
if args.hdf5 and args.cs and args.Lbox: 
    hdf5= str(args.hdf5)
    cs= float(args.cs)
    Lbox = float(args.Lbox)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import matplotlib.pyplot as plt
import pickle
import my_fund_const as fc


def CurlCellDiff(pf,vx,vy,vz):
    #FIRST PART
    class CellFaces():
        class Vel():
            def __init__(self,vx,vy,vz):
                dim= np.array(vx.shape)+1  #vx,vy,vz all same size
                self.Vx= np.zeros(dim)
                self.Vy= self.Vx.copy()
                self.Vz= self.Vx.copy()
        def __init__(self,vx,vy,vz):
            '''vx,vy,vz are from vx=covering_grid['X-momentum']/covering_grid['density'], etc
            LR - Left/Right faces (x dir)
            FB - Front/Back faces (y dir)
            BT - Bottom/Top faces (z dir)
            '''
            self.LR= self.Vel(vx,vy,vz)
            self.TB= self.Vel(vx,vy,vz)
            self.FB= self.Vel(vx,vy,vz)

    Face= CellFaces(vx,vy,vz)
    first=0
    last=vx.shape[0]-1
    #iterate Left/Right faces first (eg. x direction)
    for i in range(vx.shape[0]):
        ind=i-1
        if i==first: ind=last
        Face.LR.Vx[i,:-1,:-1]= (vx[ind,:,:]+vx[i,:,:])/2  #:-1 keep dimensions same
        Face.LR.Vy[i,:-1,:-1]= (vy[ind,:,:]+vy[i,:,:])/2
        Face.LR.Vz[i,:-1,:-1]= (vz[ind,:,:]+vz[i,:,:])/2
    #fill that last index of face in x dir, it is the same as the first Face value
    Face.LR.Vx[-1,:,:]= Face.LR.Vx[0,:,:] #dim match so no -1
    Face.LR.Vy[-1,:,:]= Face.LR.Vy[0,:,:] 
    Face.LR.Vz[-1,:,:]= Face.LR.Vz[0,:,:]
    #iterate Front/Back faces (eg. y direction)
    for j in range(vx.shape[0]):
        ind=j-1
        if j==first: ind=last
        Face.FB.Vx[:-1,j,:-1]= (vx[:,ind,:]+vx[:,j,:])/2
        Face.FB.Vy[:-1,j,:-1]= (vy[:,ind,:]+vy[:,j,:])/2
        Face.FB.Vz[:-1,j,:-1]= (vz[:,ind,:]+vz[:,j,:])/2
    Face.FB.Vx[:,-1,:]= Face.FB.Vx[:,0,:]  
    Face.FB.Vy[:,-1,:]= Face.FB.Vy[:,0,:]
    Face.FB.Vz[:,-1,:]= Face.FB.Vz[:,0,:]
    #iterate Top/Bottom faces (eg. z direction)
    for k in range(vx.shape[0]):
        ind=k-1
        if k==first: ind=last
        Face.TB.Vx[:-1,:-1,k]= (vx[:,:,ind]+vx[:,:,k])/2
        Face.TB.Vy[:-1,:-1,k]= (vy[:,:,ind]+vy[:,:,k])/2
        Face.TB.Vz[:-1,:-1,k]= (vz[:,:,ind]+vz[:,:,k])/2
    Face.TB.Vx[:,:,-1]= Face.TB.Vx[:,:,0] 
    Face.TB.Vy[:,:,-1]= Face.TB.Vy[:,:,0]
    Face.TB.Vz[:,:,-1]= Face.TB.Vz[:,:,0] 
    #3 faces (1/2 of cell in corner [-1,-1,-1]) have not been filled, do so now
    Face.LR.Vx[-1,-1,-1]= Face.LR.Vx[0,-1,-1]
    Face.LR.Vy[-1,-1,-1]= Face.LR.Vy[0,-1,-1]
    Face.LR.Vz[-1,-1,-1]= Face.LR.Vz[0,-1,-1]
    #
    Face.FB.Vx[-1,-1,-1]= Face.FB.Vx[-1,0,-1]
    Face.FB.Vy[-1,-1,-1]= Face.FB.Vy[-1,0,-1]
    Face.FB.Vz[-1,-1,-1]= Face.FB.Vz[-1,0,-1]
    #
    Face.TB.Vx[-1,-1,-1]= Face.TB.Vx[-1,-1,0]
    Face.TB.Vy[-1,-1,-1]= Face.TB.Vy[-1,-1,0]
    Face.TB.Vz[-1,-1,-1]= Face.TB.Vz[-1,-1,0]
    #SECOND PART
    class Gradients():
        def __init__(self,vx):
            dim= np.array(vx.shape)
            #grad in x dir
            self.dVy_dx= np.zeros(dim)
            self.dVz_dx= self.dVy_dx.copy()
            #grad in y dir
            self.dVx_dy= self.dVy_dx.copy()
            self.dVz_dy= self.dVy_dx.copy()
            #grad in z dir
            self.dVx_dz= self.dVy_dx.copy()
            self.dVy_dz= self.dVy_dx.copy()
    Grad= Gradients(vx)
    #grad in x dir
    for i in range(vx.shape[0]):
        Grad.dVy_dx[i,:,:]= Face.LR.Vy[i+1,:-1,:-1]-Face.LR.Vy[i,:-1,:-1] #Grad arrays have Sim data dims
        Grad.dVz_dx[i,:,:]= Face.LR.Vz[i+1,:-1,:-1]-Face.LR.Vz[i,:-1,:-1]
    #grad in y dir
    for j in range(vx.shape[0]):
        Grad.dVx_dy[:,j,:]= Face.FB.Vx[:-1,j+1,:-1]-Face.FB.Vx[:-1,j,:-1]
        Grad.dVz_dy[:,j,:]= Face.FB.Vz[:-1,j+1,:-1]-Face.FB.Vz[:-1,j,:-1]
    #grad in z dir
    for k in range(vx.shape[0]):
        Grad.dVx_dz[:,:,k]= Face.TB.Vx[:-1,:-1,k+1]-Face.TB.Vx[:-1,:-1,k]
        Grad.dVy_dz[:,:,k]= Face.TB.Vy[:-1,:-1,k+1]-Face.TB.Vy[:-1,:-1,k]
    #don't forget to DIVIDE BY CELL WIDTH!
    lev=0
    wid_cell= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**lev
    Grad.dVy_dx /= wid_cell
    Grad.dVz_dx /= wid_cell
    #
    Grad.dVx_dy /= wid_cell
    Grad.dVz_dy /= wid_cell
    #
    Grad.dVx_dz /= wid_cell
    Grad.dVy_dz /= wid_cell
    #THIRD PART
    class CurlVector():
        def __init__(self,Grad):
            self.xhat= Grad.dVz_dy- Grad.dVy_dz
            self.yhat= -(Grad.dVz_dx- Grad.dVx_dz)
            self.zhat= Grad.dVy_dx- Grad.dVx_dy
            self.mag= np.sqrt(self.xhat**2+self.yhat**2+self.zhat**2)
    Curl= CurlVector(Grad)
    return Curl.mag

class PhiVals():
    def __init__(self,phimean,phimed):
        self.phimean= phimean
        self.phimed= phimed

def MdotBH_x(rho_x,Msink,cs,MachS_x):
    one=4.*np.pi*rho_x*(fc.G*Msink)**2*cs**-3
    lam=1.1
    return one*(lam**2+MachS_x**2)/((1+MachS_x**2)**4)**0.5

def MdotW_x(Msink,rho_x,cs,vort_x):
    rB= fc.G*Msink/cs**2
    wstar_x= vort_x*rB/cs
    fstar_x= 1/(1+wstar_x**0.9)
    return 4*np.pi*rho_x*(fc.G*Msink)**2*cs**-3*0.34*fstar_x

def make_Fig4(fsave,Mturb_x,Mbh_x,Mw_x,M0):
    (cum_turb,cum_bins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True,cumulative=True)
    (cum_bh,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=cum_bins,normed=True,cumulative=True)
    (cum_w,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=cum_bins,normed=True,cumulative=True)
    (pdf_turb,pdf_bins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True)#,stacked=True)
    (pdf_bh,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=pdf_bins,normed=True)#,stacked=True)
    (pdf_w,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=pdf_bins,normed=True)#,stacked=True)
    fig,ax=plt.subplots(2,1,sharex=True)
    lab= [r'$\dot{M}_{Turb}$',r'$\dot{M}_{BH}$',r'$\dot{M}_{\omega}$']
    mybins= 10**((cum_bins[0:-1]+cum_bins[1:])/2)
    ax[0].plot(mybins,cum_turb,c='k',ls='-',label=lab[0])
    ax[0].plot(mybins,cum_bh,c='k',ls='--',label=lab[1])
    ax[0].plot(mybins,cum_w,c='k',ls=':',label=lab[2])
    # ax[0].set_xlabel(r'$\dot{M}/\dot{M}_0$')
    ax[0].set_ylabel(r'$P_M(<\dot{M}$)')
    ax[0].set_ylim(0,1)
    ax[0].set_xscale('log')
    ax[0].legend(loc=0)

    mybins= 10**((pdf_bins[0:-1]+pdf_bins[1:])/2)
    ax[1].plot(mybins,pdf_turb,c='k',ls='-',label=lab[0])
    ax[1].plot(mybins,pdf_bh,c='k',ls='--',label=lab[1])
    ax[1].plot(mybins,pdf_w,c='k',ls=':',label=lab[2])
    ax[1].set_xlabel(r'$\dot{M}/\dot{M}_0$')
    ax[1].set_ylabel(r'PDF')
    ax[1].set_xlim(1.e-2,1.e2)
    ax[1].set_xscale('log')
    fig.subplots_adjust(wspace=0.3,hspace=0.)
    # ax[1].legend(loc=0)
    plt.savefig(fsave,dpi=150)

#MAIN#
#store Table 1 vals
table= {}
#choice of rB sets Msink that you assume for Ansatz
log_rB_L= np.array([-5,-3,-1,-0.7,1])
Msink= 10.**log_rB_L*Lbox*cs**2/fc.G
#simulation props
spath='./plots/'
ds=yt.load(hdf5)
cgrid= ds.covering_grid(level=0,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions) 
rho_x=cgrid['density'] #_x means have value at every cell in grid
vx_x=cgrid['X-momentum']/rho_x
vy_x=cgrid['Y-momentum']/rho_x
vz_x=cgrid['Z-momentum']/rho_x
vrms2_x= np.average(vx_x**2+vy_x**2+vz_x**2,weights=rho_x)
MachS_x= vrms2_x**0.5/cs
MachS_0= np.average(MachS_x)
vort_x= CurlCellDiff(ds,vx_x,vy_x,vz_x)
#loop over log(rB_L)
for rB_L,mass in zip(log_rB_L,Msink):
    # (rB_L,mass)=(log_rB_L[3],Msink[3])
    Mbh_x= MdotBH_x(rho_x,mass,cs,MachS_x)
    Mw_x= MdotW_x(mass,rho_x,cs,vort_x)
    Mturb_x= (Mbh_x**-2 + Mw_x**-2)**-0.5
    M0= 4.*np.pi*rho_x.mean()*(fc.G*mass)**2/(MachS_0*cs)**3
    phimean= np.mean(Mturb_x)/M0
    phimed= np.median(Mturb_x)/M0
    table[str(rB_L)]= PhiVals(phimean,phimed)
    if str(rB_L) == '-0.7': 
        fsave=spath+'K06_Fig4_Mach0fromData.png'
        make_Fig4(fsave,Mturb_x,Mbh_x,Mw_x,M0)
print 'MachS= %f' % MachS_0
print 'Log10(rB/L),\phi_{mean},\phi_{median}'
for key in table.keys():
    print key, table[key].phimean,table[key].phimed
import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5",action="store",help='hdf5 data file to calc Table from, ex: data.3d.0001.hdf5')
parser.add_argument("-MachS_0",action="store",help='Mach # of simulation when sinks inserted')
parser.add_argument("-cs",action="store",help='constant sound speed')
parser.add_argument("-Lbox",action="store",help='length simulation box, cm')
args = parser.parse_args()
if args.hdf5 and args.MachS_0 and args.cs and args.Lbox: 
    hdf5= str(args.hdf5)
    MachS_0= float(args.MachS_0)
    cs= float(args.cs)
    Lbox = float(args.Lbox)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
import yt
yt.enable_parallelism()
import numpy as np
import matplotlib.pyplot as plt
import my_fund_const as fc

def CurlCellDiff(pf,vx,vy,vz):
    #FIRST PART
    class CellFaces():
        class Vel():
            def __init__(self,vx,vy,vz):
                dim= np.array(vx.shape)+1  #vx,vy,vz all same size
                self.Vx= np.zeros(dim)
                self.Vy= self.Vx.copy()
                self.Vz= self.Vx.copy()
        def __init__(self,vx,vy,vz):
            '''vx,vy,vz are from vx=covering_grid['X-momentum']/covering_grid['density'], etc
            LR - Left/Right faces (x dir)
            FB - Front/Back faces (y dir)
            BT - Bottom/Top faces (z dir)
            '''
            self.LR= self.Vel(vx,vy,vz)
            self.TB= self.Vel(vx,vy,vz)
            self.FB= self.Vel(vx,vy,vz)

    Face= CellFaces(vx,vy,vz)
    first=0
    last=vx.shape[0]-1
    #iterate Left/Right faces first (eg. x direction)
    for i in range(vx.shape[0]):
        ind=i-1
        if i==first: ind=last
        Face.LR.Vx[i,:-1,:-1]= (vx[ind,:,:]+vx[i,:,:])/2  #:-1 keep dimensions same
        Face.LR.Vy[i,:-1,:-1]= (vy[ind,:,:]+vy[i,:,:])/2
        Face.LR.Vz[i,:-1,:-1]= (vz[ind,:,:]+vz[i,:,:])/2
    #fill that last index of face in x dir, it is the same as the first Face value
    Face.LR.Vx[-1,:,:]= Face.LR.Vx[0,:,:] #dim match so no -1
    Face.LR.Vy[-1,:,:]= Face.LR.Vy[0,:,:] 
    Face.LR.Vz[-1,:,:]= Face.LR.Vz[0,:,:]
    #iterate Front/Back faces (eg. y direction)
    for j in range(vx.shape[0]):
        ind=j-1
        if j==first: ind=last
        Face.FB.Vx[:-1,j,:-1]= (vx[:,ind,:]+vx[:,j,:])/2
        Face.FB.Vy[:-1,j,:-1]= (vy[:,ind,:]+vy[:,j,:])/2
        Face.FB.Vz[:-1,j,:-1]= (vz[:,ind,:]+vz[:,j,:])/2
    Face.FB.Vx[:,-1,:]= Face.FB.Vx[:,0,:]  
    Face.FB.Vy[:,-1,:]= Face.FB.Vy[:,0,:]
    Face.FB.Vz[:,-1,:]= Face.FB.Vz[:,0,:]
    #iterate Top/Bottom faces (eg. z direction)
    for k in range(vx.shape[0]):
        ind=k-1
        if k==first: ind=last
        Face.TB.Vx[:-1,:-1,k]= (vx[:,:,ind]+vx[:,:,k])/2
        Face.TB.Vy[:-1,:-1,k]= (vy[:,:,ind]+vy[:,:,k])/2
        Face.TB.Vz[:-1,:-1,k]= (vz[:,:,ind]+vz[:,:,k])/2
    Face.TB.Vx[:,:,-1]= Face.TB.Vx[:,:,0] 
    Face.TB.Vy[:,:,-1]= Face.TB.Vy[:,:,0]
    Face.TB.Vz[:,:,-1]= Face.TB.Vz[:,:,0] 
    #3 faces (1/2 of cell in corner [-1,-1,-1]) have not been filled, do so now
    Face.LR.Vx[-1,-1,-1]= Face.LR.Vx[0,-1,-1]
    Face.LR.Vy[-1,-1,-1]= Face.LR.Vy[0,-1,-1]
    Face.LR.Vz[-1,-1,-1]= Face.LR.Vz[0,-1,-1]
    #
    Face.FB.Vx[-1,-1,-1]= Face.FB.Vx[-1,0,-1]
    Face.FB.Vy[-1,-1,-1]= Face.FB.Vy[-1,0,-1]
    Face.FB.Vz[-1,-1,-1]= Face.FB.Vz[-1,0,-1]
    #
    Face.TB.Vx[-1,-1,-1]= Face.TB.Vx[-1,-1,0]
    Face.TB.Vy[-1,-1,-1]= Face.TB.Vy[-1,-1,0]
    Face.TB.Vz[-1,-1,-1]= Face.TB.Vz[-1,-1,0]
    #SECOND PART
    class Gradients():
        def __init__(self,vx):
            dim= np.array(vx.shape)
            #grad in x dir
            self.dVy_dx= np.zeros(dim)
            self.dVz_dx= self.dVy_dx.copy()
            #grad in y dir
            self.dVx_dy= self.dVy_dx.copy()
            self.dVz_dy= self.dVy_dx.copy()
            #grad in z dir
            self.dVx_dz= self.dVy_dx.copy()
            self.dVy_dz= self.dVy_dx.copy()
    Grad= Gradients(vx)
    #grad in x dir
    for i in range(vx.shape[0]):
        Grad.dVy_dx[i,:,:]= Face.LR.Vy[i+1,:-1,:-1]-Face.LR.Vy[i,:-1,:-1] #Grad arrays have Sim data dims
        Grad.dVz_dx[i,:,:]= Face.LR.Vz[i+1,:-1,:-1]-Face.LR.Vz[i,:-1,:-1]
    #grad in y dir
    for j in range(vx.shape[0]):
        Grad.dVx_dy[:,j,:]= Face.FB.Vx[:-1,j+1,:-1]-Face.FB.Vx[:-1,j,:-1]
        Grad.dVz_dy[:,j,:]= Face.FB.Vz[:-1,j+1,:-1]-Face.FB.Vz[:-1,j,:-1]
    #grad in z dir
    for k in range(vx.shape[0]):
        Grad.dVx_dz[:,:,k]= Face.TB.Vx[:-1,:-1,k+1]-Face.TB.Vx[:-1,:-1,k]
        Grad.dVy_dz[:,:,k]= Face.TB.Vy[:-1,:-1,k+1]-Face.TB.Vy[:-1,:-1,k]
    #don't forget to DIVIDE BY CELL WIDTH!
    lev=0
    wid_cell= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**lev
    Grad.dVy_dx /= wid_cell
    Grad.dVz_dx /= wid_cell
    #
    Grad.dVx_dy /= wid_cell
    Grad.dVz_dy /= wid_cell
    #
    Grad.dVx_dz /= wid_cell
    Grad.dVy_dz /= wid_cell
    #THIRD PART
    class CurlVector():
        def __init__(self,Grad):
            self.xhat= Grad.dVz_dy- Grad.dVy_dz
            self.yhat= -(Grad.dVz_dx- Grad.dVx_dz)
            self.zhat= Grad.dVy_dx- Grad.dVx_dy
            self.mag= np.sqrt(self.xhat**2+self.yhat**2+self.zhat**2)
    Curl= CurlVector(Grad)
    return Curl.mag

class PhiVals():
    def __init__(self,phimean,phimed):
        self.phimean= phimean
        self.phimed= phimed

def MdotBH_x(rho_x,Msink,cs,MachS_x):
    one=4.*np.pi*rho_x*(fc.G*Msink)**2*cs**-3
    lam=1.1
    return one*(lam**2+MachS_x**2)/((1+MachS_x**2)**4)**0.5

def MdotW_x(Msink,rho_x,cs,vort_x):
    rB= fc.G*Msink/cs**2
    wstar_x= vort_x*rB/cs
    fstar_x= 1/(1+wstar_x**0.9)
    return 4*np.pi*rho_x*(fc.G*Msink)**2*cs**-3*0.34*fstar_x

def make_Fig4(fsave,Mturb_x,Mbh_x,Mw_x,M0):
    (cum_turb,cum_bins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True,cumulative=True)
    (cum_bh,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=cum_bins,normed=True,cumulative=True)
    (cum_w,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=cum_bins,normed=True,cumulative=True)
    (pdf_turb,pdf_bins,p)=plt.hist(np.log10(Mturb_x.flatten()/M0),bins=100,normed=True)#,stacked=True)
    (pdf_bh,jnk,p)=plt.hist(np.log10(Mbh_x.flatten()/M0),bins=pdf_bins,normed=True)#,stacked=True)
    (pdf_w,jnk,p)=plt.hist(np.log10(Mw_x.flatten()/M0),bins=pdf_bins,normed=True)#,stacked=True)

    fig,ax=plt.subplots(2,1,sharex=True)
    lab= [r'$\dot{M}_{Turb}$',r'$\dot{M}_{BH}$',r'$\dot{M}_{\omega}$']
    mybins= 10**((cum_bins[0:-1]+cum_bins[1:])/2)
    ax[0].plot(mybins,cum_turb,c='k',ls='-',label=lab[0])
    ax[0].plot(mybins,cum_bh,c='k',ls='--',label=lab[1])
    ax[0].plot(mybins,cum_w,c='k',ls=':',label=lab[2])
    # ax[0].set_xlabel(r'$\dot{M}/\dot{M}_0$')
    ax[0].set_ylabel(r'$P_M(<\dot{M}$)')
    ax[0].set_ylim(0,1)
    ax[0].set_xscale('log')
    ax[0].legend(loc=0)

    mybins= 10**((pdf_bins[0:-1]+pdf_bins[1:])/2)
    ax[1].plot(mybins,pdf_turb,c='k',ls='-',label=lab[0])
    ax[1].plot(mybins,pdf_bh,c='k',ls='--',label=lab[1])
    ax[1].plot(mybins,pdf_w,c='k',ls=':',label=lab[2])
    ax[1].set_xlabel(r'$\dot{M}/\dot{M}_0$')
    ax[1].set_ylabel(r'PDF')
    ax[1].set_xlim(1.e-2,1.e2)
    ax[1].set_xscale('log')
    fig.subplots_adjust(wspace=0.3,hspace=0.)
    # ax[1].legend(loc=0)
    plt.savefig(fsave,dpi=150)

#MAIN#
#store Table 1 vals
table= {}
#choice of rB sets Msink that you assume for Ansatz
log_rB_L= np.array([-5,-3,-1,-0.7,1])
Msink= 10.**log_rB_L*Lbox*cs**2/fc.G
#simulation props
spath='./plots/'
ds=yt.load(hdf5)
cgrid= ds.covering_grid(level=0,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions) 
rho_x=cgrid['density'] #_x means have value at every cell in grid
vx_x=cgrid['X-momentum']/rho_x
vy_x=cgrid['Y-momentum']/rho_x
vz_x=cgrid['Z-momentum']/rho_x
if yt.is_root():
    vrms2_x= np.average(vx_x**2+vy_x**2+vz_x**2,weights=rho_x)
    MachS_x= vrms2_x**0.5/cs
    vort_x= CurlCellDiff(ds,vx_x,vy_x,vz_x)
    #loop over log(rB_L)
    for rB_L,mass in zip(log_rB_L,Msink):
        # (rB_L,mass)=(log_rB_L[3],Msink[3])
        Mbh_x= MdotBH_x(rho_x,mass,cs,MachS_x)
        Mw_x= MdotW_x(mass,rho_x,cs,vort_x)
        Mturb_x= (Mbh_x**-2 + Mw_x**-2)**-0.5
        M0= 4.*np.pi*rho_x.mean()*(fc.G*mass)**2/(MachS_0*cs)**3
        phimean= np.mean(Mturb_x)/M0
        phimed= np.median(Mturb_x)/M0
        table[str(rB_L)]= PhiVals(phimean,phimed)
        if str(rB_L) == '-0.7': 
            fsave=spath+'K06_Fig4.png'
            make_Fig4(fsave,Mturb_x,Mbh_x,Mw_x,M0)
    print 'Log10(rB/L),\phi_{mean},\phi_{median}'
    for key in table.keys():
        print key, table[key].phimean,table[key].phimed
