from yt.mods import *
import matplotlib.pyplot as plt
import glob
import numpy as np

class Dat:
	def __init__(self):
		self.t = []
		self.sink=[]
		self.volavg_B2 = []
		self.rho_ray =[]
		self.vel_ray = []
		#following filled by calc_more() given data above
		self.s_mdot = [] #sink mdot
		self.beta = []
		self.mdot_B = [] #bondi accretion mdot, func of time
		self.mdot_Cun = [] #cunningham correction form for bondi mdot, func of time

den0=1.e-6
cs0 = 1.0
gam = 1.0001
lam_B = 4.5
grav_G = 6.67e-8 #cgs
m_sink = 1.
bx0=0.
by0=0.
bz0=1.4e-4
b_sqr = bx0**2 + by0**2 + bz0**2
beta_t0 = (den0*cs0**2) / (b_sqr/8./np.PI)
mdot_B_t0 = 4*np.PI*(grav_G*m_sink)**2*den0*lam_B/cs0**3
n_Cun = 0.42
bch_Cun = 5.
tmp = (bch_Cun/beta_t0)**(n_Cun/2)+1
mdot_Cun_t0 = mdot_B_t0*(temp)**(-1./n_Cun)

		
path = "/clusterfs/henyey/kaylanb/Test_Prob/Sink/BondiSubgrid/beta_1e4/"
save_path = path + "plots/"
dpatt = "data.000*.3d.hdf5"
files = glob.glob1(path, dpatt)

rayi = [-16.,0.,0.]
rayf = [16.,0.,0.]
dat = Dat()
for f in files:
	#read sink
	fsink= f[:-4] + "sink"
	rsink = open(fsink)
	line = rsink.readline()
	line = rsink.readline()
	rsink.close()
	d={} 
	keys = ["m","x","y","z","px","py","pz","Lx","Ly","Lz","id"]
	larr = line.strip(' \n').split(' ')
	larr.remove("")
	for i,key in enumerate(keys):
	    d[key] = float(larr[i])
	dat.sink.append(d)

	#read data
	pf = load(path+f)
	print pf.h.field_list
	dat.t.append(pf.current_time)
	data = pf.h.all_data()
	tmp = (data["X-magnfield"])**2 + (data["Y-magnfield"])**2+(data["Z-magnfield"])**2
	tmp /= 32.**3
	dat.volavg_B2.append(tmp)
	ray = pf.h.ray(rayi,rayf) #profile along any x,y,z dir in data cube or any diagonal direction too!
	dat.rho_ray.append(ray["density"])
	tmp = (ray["X-momentum"])**2 + (ray["Y-momentum"])**2 + (ray["Z-momentum"])**2
	tmp = np.sqrt( tmp / ray["density"])
	dat.vel_ray.append(tmp)

calc_more(dat)

def calc_more(dat):
	for i,t in enumerate(dat.t)
          if i==0:
               dat.s_mdot.append(0)
          else:
               dat.s_mdot.append( (dat.sink[i]["m"]-dat.sink[i-1]["m"])/ (dat.t[i]-dat.t[i-1]))


def plotAll(dat):
	f,ax = plt.subplots()
	x = np.array(dat.t)
	mdot =[]
	for i,t in enumerate(dat.t)
		if i==0:
			mdot.append(0)
		else:
			mdot.append( (dat.sink[i]["m"]-dat.sink[i-1]["m"])/ (dat.t[i]-dat.t[i-1]))
     ax.plot(x,y)
     plt.setp(ax.lines, linewidth=2)
     #tmp=d["m"]
     #ax.vlines(14.1,tmp.min(),tmp.max(),colors='r',linestyles='dashed',label="pc/pe = 14.1")
     ax.legend(loc=0)
     #ax.set_xscale('log')
     #ax.set_xlim([1,1e4])
	ax.set_xlabel("density")
     ax.set_ylabel("xgrid")
     f.show()

def myPlot(x,y):
     """x,y are numpy arrays, edit rest of myPlot() for plot desired"""
     f,ax = plt.subplots()
     ax.plot(x,y)
     plt.setp(ax.lines, linewidth=2)
     #tmp=d["m"]
     #ax.vlines(14.1,tmp.min(),tmp.max(),colors='r',linestyles='dashed',label="pc/pe = 14.1")
     ax.legend(loc=0)
     #ax.set_xscale('log')
     #ax.set_xlim([1,1e4])
     ax.set_xlabel("density")
     ax.set_ylabel("xgrid")
     f.show()
