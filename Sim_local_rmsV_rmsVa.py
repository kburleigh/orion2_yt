'''for one hdf5 dump, prints global rmsV, rmsVa for whole box, optionally the local rmsV,rmsVa around each sink can also be printed'''

import argparse
parser = argparse.ArgumentParser(description="test")
#required
parser.add_argument("-fhdf5",action="store",help='filename or file wildcard or path to filename and wild')
parser.add_argument("-nsinks",type=int,action="store",help='filename or file wildcard or path to filename and wild')
parser.add_argument("-region",choices=['box','sphere'],action="store",help='type of region around each sink particle within which to calc statistical properties')
parser.add_argument("-smrWide",type=int,action="store",help='width of box (region=box) or radius of sphere (region=sphere), in units of number of cells on chosen level. ex:region=box, smrWide=48 then calc local properties inside SMR level that is 48cubed')
parser.add_argument("-level",type=int,action="store",help='size of region= smrWide*width of a cell on this level')
#optional
parser.add_argument("-plot_local",action="store",help='set to 1 ONLY IF local.txt has been output, this will read in that file and plot local vrms, local rmsVa vs. time for each sink id')
args = parser.parse_args()

import numpy as np
import yt

class SinkParticles():
    def __init__(self,nsinks):
        self.sid= np.zeros(nsinks).astype(np.int)-1
        #print mass to 16 decimals (max decimals for double precision) so need float64
        self.mass= np.zeros(nsinks).astype(np.float64)-1
        self.x= self.mass.copy()
        self.y= self.mass.copy()
        self.z= self.mass.copy() 
        self.rmsV= self.mass.copy() 
        self.rmsVa= self.mass.copy()
        self.meanRho= self.mass.copy()

def rmsV_rmsVa_meanRho(ytregion):
    '''returns rms velocity and rms alfven velocity in region'''
    mass= np.array(ytregion[('gas', 'cell_mass')])
    #dx= np.array(ytregion[('index', 'dx')])
    #dy= np.array(ytregion[('index', 'dy')])
    #dz= np.array(ytregion[('index', 'dz')])
    rho= np.array(ytregion["density"])
    vx= np.array(ytregion["X-momentum"])/rho
    vy= np.array(ytregion["Y-momentum"])/rho
    vz= np.array(ytregion["Z-momentum"])/rho
    bx= np.array(ytregion["X-magnfield"])*(4*np.pi)**0.5
    by= np.array(ytregion["Y-magnfield"])*(4*np.pi)**0.5
    bz= np.array(ytregion["Z-magnfield"])*(4*np.pi)**0.5
    #TESTING
    #print "sum mass/rho= ",(mass/rho).sum(),", sum dx*dy*dz= ",(dx*dy*dz).sum()
    #print "sum mass= ",mass.sum(),", sum rho*dx*dy*dz= ",(rho*dx*dy*dz).sum()
    #mean rho
    meanRho= np.average(rho,weights=mass/rho) 
    #rmsV
    vmag2= (vx**2+vy**2+vz**2)
    rmsV= np.sqrt(np.average(vmag2,weights=mass))
    #rmsVa
    rmsB= np.sqrt( np.average(bx**2+by**2+bz**2,weights=mass/rho) ) 
    rmsVa= rmsB / np.sqrt(4*np.pi*meanRho)
    return (rmsV,rmsVa,meanRho)

def sink_sep(sink):
    dx=sink.x[1:]-sink.x[0]
    dx[np.where(dx == dx.min())[0]]= dx.max()+1 #dx min = 0 b/c many sinks have same x position
    return dx.min() #now dx min is the nearest sink distance

#MAIN
#read in .sink for sink locations which are our box centers
sink= SinkParticles(args.nsinks) 
f=open(args.fhdf5[:-4]+'sink','r')
lines= f.readlines()
f.close()
for i in range(1,len(lines)):
    info= np.around(np.float64(lines[i].split()),decimals=16)
    sink.sid[i-1]= info[-1].astype(int)
    sink.mass[i-1]= info[0]
    sink.x[i-1]= info[1]
    sink.y[i-1]= info[2]
    sink.z[i-1]= info[3]
#get sink separation, which is the width for all boxes
#width= sink_sep(sink)
#load hdf5 data and extract rmsV,rmsVa for local region around each sink
print "loading data: %s" % args.fhdf5
ds=yt.load(args.fhdf5)
#properties of simulation domain
base= ds.domain_dimensions[0]
lbox= float(ds.domain_width[0])
dx_lev= lbox/base/2.**args.level
width= args.smrWide*dx_lev
#global stats, always over entire domain
box= ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
(rmsV,rmsVa,meanRho)= rmsV_rmsVa_meanRho(box)
#write global stats
fin= open("global_rmsV_rmsVa.txt",'a')
fin.write("#filename time rmsV rmsVa meanRho\n")
fin.write("%s %.5g %.3f %.3f %.5f\n" % (ds.basename,float(ds.current_time), rmsV,rmsVa,meanRho) )
fin.close()
#local stats
#line will eventually be written to file, once contains all data
line="%s %.5g" % (ds.basename,float(ds.current_time))
#loop over local region centers
for i in range(args.nsinks):
	#extracting simulation data in box region and computing rmsV,Va
	c_arr=np.array([sink.x[i],sink.y[i],sink.z[i]]) 
	if args.region == 'box':
		l_arr= c_arr-width/2. 
		r_arr= c_arr+width/2.
		reg= ds.region(c_arr,l_arr,r_arr)
	elif args.region == 'sphere':
		reg= ds.sphere(c_arr,width)
	else: raise ValueError
	(sink.rmsV[i],sink.rmsVa[i],sink.meanRho[i])= rmsV_rmsVa_meanRho(reg)
	#line gets info for ith sink
	line= line+ " %d %.3f %.3f %.5f" % (sink.sid[i],sink.rmsV[i],sink.rmsVa[i],sink.meanRho[i])
#append to file
fin= open("local_rmsV_rmsVa.txt",'a')
head= "#region= %s, smrWide= %d, level= %d" % (args.region,args.smrWide,args.level)
fin.write(head+"; COLUMNS: filename time sid[i] rmsV[i] rmsVa[i] meanRho[i] sid[i+1] ... etc\n")
fin.write(line+"\n")
fin.close()

if args.plot_local:
    #prepare rmsV, rmsVa vs. time plot for local region of each sink
    f,axis=plt.subplots(2,1,sharex=True,figsize=(10,15))
    plt.subplots_adjust( hspace=0.2,wspace=0.2 )
    ax=axis.flatten()
    #get data
    data=np.loadtxt("local_rmsV_rmsVa.txt", dtype=np.string_)
    sort_ind= np.argsort(data[:,1]) #sorted index for time to increase monotonically
    sid_ind= np.arange(2,a.shape[1]-1,3) #index of each sink id, starts at 2, then every 3rd index after that
    time= data[:,1][sort_ind].astype(np.float128)
    for i in sid_ind:
        rmsV= data[:,i+1][sort_ind].astype(np.float128)
        rmsVa= data[:,i+2][sort_ind].astype(np.float128)
        ax[0].plot(time,rmsV,ls='-',label=data[:,i][0].astype(np.string_))
        ax[1].plot(time,rmsVa,ls='-',label=data[:,i][0].astype(np.string_))
    #finish plot
    ax[0].set_ylabel("rms V",fontweight='bold',fontsize='x-large')
    ax[1].set_ylabel("rms Va",fontweight='bold',fontsize='x-large')
    ax[1].set_xlabel(r'$t$',fontweight='bold',fontsize='x-large')
    ax[1].legend(loc=(1.05,0),fontsize='x-large',ncol=3)
    plt.savefig('local_rmsV_rmsVa.png')
   
