#!/bin/bash 
#vrms
grep $1 -e "vrms =" |cut -d ',' -f 2|cut -d ' ' -f 4 > ${1}_vrms.txt
#v max
grep $1 -e "vrms =" |cut -d ',' -f 4|cut -d ' ' -f 5 > ${1}_vmax.txt
#time, tstep
grep $1 -e "coarse time step" > ${1}_coarse.txt
