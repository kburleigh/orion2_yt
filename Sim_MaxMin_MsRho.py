'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and plot vs. time'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-cs",action="store",help='adiabatic sound speed in cm/sec')
args = parser.parse_args()
if args.cs: cs_ad = float(args.cs)
else: raise ValueError

#import matplotlib
#matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

def _vx(field, data):
    return data['X-momentum']/data['density']
add_field("vx", function=_vx, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _vy(field, data):
    return data['Y-momentum']/data['density']
add_field("vy", function=_vy, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _vz(field, data):
    return data['Z-momentum']/data['density']
add_field("vz", function=_vz, take_log=False,
        units=r'\rm{cm}/\rm{s}')

##
def _Vrms(field, data):
    return np.sqrt(data['vx']**2 + data['vy']**2 + data['vz']**2)
add_field("Vrms", function=_Vrms, take_log=False,
        units=r'\rm{cm}/\rm{s}')

#MAIN
spath="./plots/"
f_hdf5= glob.glob("./data.0045.3d.hdf5")
#f_hdf5= ["data.0001.3d.hdf5","data.0010.3d.hdf5"]
#f_hdf5= glob.glob("../../images/stampede/MHDMyMac/drive_MachS-5_MachA-1/maxRes-rBdiv128/data.00*.3d.hdf5")
#f_hdf5.append(f_hdf5[0])
f_hdf5.sort()
lev=0

print "hdf5 files are: ", f_hdf5
#cs= float(raw_input('enter cs in cm/sec: '))
#tB= float(raw_input('enter bondi time in sec: '))
for cnt,f in enumerate(f_hdf5):
    print "loading file: %s" % f
    pf= load(f)
    print 'calling covering grid'
    #my method
    cube= pf.h.covering_grid(level=lev,\
                          left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions)
    print 'rho,vx,etc from covering grid'
    rho = cube["density"]
    vx= cube["X-momentum"]/rho
    vy= cube["Y-momentum"]/rho
    vz= cube["Z-momentum"]/rho
    gamma=1.001
    cs_iso= cs_ad/gamma**0.5
    MachS=(vx**2+vy**2+vz**2)**0.5/cs_iso
    (x,y,z)=np.where(MachS > int(MachS.max()))
    print f
    print 'mean, min, max, stddev MachS= ', np.mean(MachS),np.min(MachS),np.max(MachS),np.std(MachS)
    print 'domain dim, domain width, MachS.shape:',pf.domain_dimensions, pf.domain_width, MachS.shape
    print 'lev0 cell location of max Vrms'
    for cnt,i in enumerate(x): print x[cnt],y[cnt],z[cnt] 
    print 'code units "cm" location of max Vrms'
    cm_cell=pf.domain_width[0]/pf.domain_dimensions[0]
    for cnt,i in enumerate(x): print float(x[cnt]-0.5)*cm_cell,float(y[cnt]-0.5)*cm_cell,float(z[cnt]-0.5)*cm_cell 
    print 'MachS,rho at location of max Vrms:',MachS[(x,y,z)],rho[(x,y,z)]
    #yt: full box
    alld= pf.h.all_data()
vx=alld['X-momentum']/alld['density']
vy=alld['Y-momentum']/alld['density']
vz=alld['Z-momentum']/alld['density']
vrms=(vx**2+vy**2+vz**2)**0.5
machs=vrms/cs_iso
yt_crit= np.where(machs > int(machs.max()))
print 'yt func: MachS,rho at location of max Vrms',machs[yt_crit],alld['density'][yt_crit]
alld['density'].min(),alld['density'].max(),alld['density'].mean(),alld['density'].std()
#yt: sphere around mach S ~ 200 pt
cm_cell=pf.domain_width[0]/pf.domain_dimensions[0]
cent= cm_cell*np.array([x[0]-0.5,y[0]-0.5,z[0]-0.5])
rad= 26*cm_cell
sp = pf.h.sphere(cent,(rad,'cm'))    
vx=sp['X-momentum']/sp['density']
vy=sp['Y-momentum']/sp['density']
vz=sp['Z-momentum']/sp['density']
vrms=(vx**2+vy**2+vz**2)**0.5
machs=vrms/cs_iso
print '26 cells around location: yt density min,max,mean,std',sp['density'].min(),sp['density'].max(),sp['density'].mean(),sp['density'].std()
print 'yt MachS 26 cell radius around max MachS:  min,max,mean,std of region:',machs.min(),machs.max(),machs.mean(),machs.std()
#above print DID NOT GIVE EXPECTE MACHS RESULT
#print 'yt MachS 26 cell radius around max MachS:  min,max,mean,std of region:',machs.min(),machs.max(),machs.mean(),machs.std()
#yt MachS 26 cell radius around max MachS:  min,max,mean,std of region: 0.0716320905622 cm**3*code_mass/(code_length**2*code_time*g) 36.6729586953 cm**3*code_mass/(code_length**2*code_time*g) 4.17344819071 cm**3*code_mass/(code_length**2*code_time*g) 1.7049525031 cm**3*code_mass/(code_length**2*code_time*g)
print 'finished'
