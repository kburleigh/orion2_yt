import fund_const as fc
import argparse
import numpy as np

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-beta",type= float,action="store",help= 'magnetic beta',required=True)
parser.add_argument("-vamax",type= float,action="store",help= 'alfven vel cap',required=True)
args = parser.parse_args()

mu= 2.05 #molecular hydrogen
temp= 10. 
msink= 2.e33 
baseGrid= 64
maxLevel=7
cs= (fc.kb*temp/mu/fc.mp)**0.5
rB= fc.G*msink/cs**2
Lbox= 50*rB
LboxHalf= Lbox/2
rho0 = 1e-8*msink/rB**3
dxmin= Lbox/baseGrid/2**maxLevel
mGas_mSink= rho0*Lbox**3/msink
print "cs[km/s]= %g, rB[AU]= %f,mGas/mSink= %g" % (cs/1e5,rB/1.5e13,mGas_mSink)
#paramaters
B= (rho0*cs**2*8*np.pi/args.beta)
Bcode= B/(4*np.pi)**0.5
tstep= 0.3*dxmin/5/cs
tstep_floor= 0.3*dxmin/args.vamax
print "Bcode = %g, expected tstep= %g, floor tstep= %g" % (Bcode, tstep, tstep_floor)
