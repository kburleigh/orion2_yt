'''NON-DIMENSIONAL units'''
import numpy as np
import fund_const as fc

def get_Lbox_xyzSinks(Nsinks,ss):
    if Nsinks==8:
        Lbox=np.array([2*ss,2*ss,2*ss]).astype('int')
        xs=np.array([-0.5*ss,0.5*ss])
        ys=np.array([-0.5*ss,0.5*ss])
        zs=np.array([-0.5*ss,0.5*ss])
    if Nsinks==32:
        Lbox=np.array([4*ss,4*ss,2*ss]).astype('int')
        xs=np.array([-1.5*ss,-0.5*ss,0.5*ss,1.5*ss])
        ys=np.array([-1.5*ss,-0.5*ss,0.5*ss,1.5*ss])
        zs=np.array([-0.5*ss,0.5*ss])
    else:
        Lbox,xs,ys,zs=-1.
    return Lbox,xs,ys,zs

class Vars():
    def __init__(self):
        #vars want
        self.Nsinks=8.
        self.rB=1.
        self.G= fc.G
        self.ss=20.*self.rB #separation between sinks in code units
        self.Lbox= 4*self.ss #length one side box, if 64 sinks, then box is (4*ss)^3
        self.Ncells_base=256
        self.cs=1.
        self.gamma=1.001
        self.MachS_initial= 15.
        self.MachS_when_insert=5.
        self.MachA_when_insert=1. #vs / va
        self.beta_initial=  100
        #vars calc given what want
        self.X1grid=[-self.Lbox/2.,self.Lbox/2.]
        self.beta_when_insert= 2.*(self.MachA_when_insert/self.MachS_when_insert)**2
        self.Msink= (self.rB*self.cs**2/self.G)*(1**2 + self.MachS_when_insert**2 + self.MachA_when_insert**2)
        self.rho0= (2./1e3)*self.Msink/self.ss**3  #Msink ~ 10^3*rho0*4pi/3*(ss/2)^3 ~ 0.5*10^3*rho0*ss^3
        self.B0_when_insert= np.sqrt(self.rho0*self.cs**2*8*np.pi/self.beta_when_insert)
        self.B0_given_beta0_want= np.sqrt(self.rho0*self.cs**2*8*np.pi/self.beta_initial)
        self.Va_given_beta0_want= np.sqrt(2./self.beta_initial)*self.cs
#         (self.Lbox,xs,ys,zs)= get_Lbox_xyzSinks(self.Nsinks,self.ss)
        self.Tcr_4Vs_initial_= self.Lbox/(self.MachS_initial*self.cs)
        self.Tcr_4Va_initial_= self.Lbox/(self.Va_given_beta0_want)
        self.Tcr_when_insert= self.Lbox/(self.MachS_when_insert*self.cs)
        self.T1st_4Vs= 0.5*(self.Lbox/self.Ncells_base)/(self.MachS_initial*self.cs)
        self.T1st_4Va= 0.5*(self.Lbox/self.Ncells_base)/(self.Va_given_beta0_want)
        # self.Tb_when_insert=
        self.T0 = self.cs**2*0.62*fc.mp/fc.kb
        self.small_dn = 1.e-12* self.rho0
        self.small_pr = self.small_dn* self.cs**2/ self.gamma
        self.ceil_va = 1.e2* self.B0_given_beta0_want/(4.*fc.pi*self.small_dn)**0.5
        self.floor_Tgas = 1.e-2 * self.T0 
    def printme(self):
            import pprint
            pprint.pprint(self.__dict__)

class SinkSep_ConvStudy():
    '''units: cgs'''
    def __init__(self):
        #vars want
        self.NLevs= 5.
        self.Nsinks=8.
        self.SinkSep_div_rBHA= 3.
        self.Msink=fc.msun
        self.cs= (fc.kb*Temp/0.62/fc.mp)**0.5
        self.MachS_estim_at_insert= 5.
        self.MachA_estim_at_insert= 1.
        self.ntags=14
        #driving params
        self.beta_drive = 10.
        #given esimates above, calc what expect for key quantities
        self.Vs_at_insert= self.MachS_estim_at_insert*self.cs
        self.Va_at_insert= self.Vs_at_insert/self.MachA_estim_at_insert
        self.rBHA= fc.G*self.Msink/max(self.cs,self.Vs_at_insert,self.Va_at_insert)**2
        self.Lbox= self.get_Lbox(self.Nsinks,self.SinkSep_div_rBHA,self.rBHA)
        self.X1cells= self.Lbox/self.rBHA
        self.dxMin_cgs= self.Lbox/self.X1cells/2**self.NLevs
        self.dxMin_div_rBHA = self.dxMin_cgs/self.rBHA 
        self.rho0= 1.e-3*self.Msink/self.Lbox**3
        tBHA_at_insert= self.rBHA/max(self.cs,self.Vs_at_insert,self.Va_at_insert)
        tCr_at_insert= self.Lbox/max(self.cs,self.Vs_at_insert,self.Va_at_insert)
        self.tBHA_div_tCr_estim_at_insert= tBHA_at_insert/ tCr_at_insert
        #orion2.ini params
        self.X1grid=[-self.Lbox/2.,self.Lbox/2.]
        self.beta_when_insert= 2.*(self.MachA_when_insert/self.MachS_when_insert)**2
        self.B0_at_insert= np.sqrt(self.rho0*self.cs**2*8*np.pi/self.beta_when_insert)
        self.B0_drive= np.sqrt(self.rho0*self.cs**2*8*np.pi/self.beta_drive)
        self.tCr_drive= self.Lbox/max(self.cs,self.Vs_at_insert,self.Va_at_insert)
        self.tstep_1st= 0.5*(self.Lbox/self.X1cells)/max(self.cs,self.Vs_at_insert,self.Va_at_insert)
        # self.Tb_when_insert=
        self.small_dn = 1.e-12* self.rho0
        self.small_pr = self.small_dn* self.cs**2/ self.gamma
        self.ceil_va = 1.e2* self.B0_drive/(4.*fc.pi*self.small_dn)**0.5
        T0= self.cs**2*0.6*fc.mp/fc.kb
        self.floor_Tgas = 1.e-2 *T0 
    def get_Lbox(Nsinks,SinkSep_div_rBHA,rBHA):
        if Nsinks == 8:
            return 2.*SinkSep_div_rBHA*rBHA
        if Nsinks == 64:
            return 4.*SinkSep_div_rBHA*rBHA
        else: 
            print "that number of sinks not yet supported"
            raise ValueError
    def printme(self):
            import pprint
            print "Warning simulation units are in cgs!, if this is not your case don't use these numbers!"
            pprint.pprint(self.__dict__)

a=Vars()
a.printme()
