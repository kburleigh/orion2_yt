import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-file",action="store",help='4 char string: 0004 would correspndong to data.3d.0004.hdf5 and *.sink')
parser.add_argument("-cs",action="store",help='sounds speed in cm/s')
parser.add_argument("-maxL",action="store",help='max level')
parser.add_argument("-Rad_div_dxmin",action="store",help='radius/dxmin around sink to compute Vr inside, eg 4 is accretion radius')
args = parser.parse_args()
if args.file: file = str(args.file)
else: raise ValueError
if args.cs: cs = float(args.cs)
else: raise ValueError
if args.maxL: maxL = int(args.maxL)
else: raise ValueError
if args.Rad_div_dxmin: Rad_div_dxmin = float(args.Rad_div_dxmin)
else: raise ValueError

path= "./"
spath="./plots/"

f_hdf5= path+'data.'+file+'.3d.hdf5'
f_sink= path+'data.'+file+'.3d.sink'
print 'f_hdf5, f_sink = ', f_hdf5, f_sink
#read initial .sink file to get sink positions

mass,x,y,z,sid= np.loadtxt(f_sink,dtype='float',\
                         skiprows=1,usecols=(0,1,2,3,10),unpack=True)
pf= load(f_hdf5)
dxmin= pf.domain_width[0]/pf.domain_dimensions[0]/2**maxL
Rad=Rad_div_dxmin*dxmin

for i,junk in enumerate(x):
    sphere0 = pf.h.sphere(center=(x[i],y[i],z[i]),radius=Rad,axes_unit='cm')
    rad_profile0 = BinnedProfile1D(sphere0, 100, 'RadiusCode', 0.0, Rad, log_space=False)
    rad_profile0.add_fields("RadialVelocity",weight='CellVolume')

    fig,ax = plt.subplots()
    ax.plot(rad_profile0["RadiusCode"]/dxmin, rad_profile0["RadialVelocity"]/cs)#,\
        #rad_profile1["RadiusCode"]/dxmin, rad_profile1["RadialVelocity"]/cs)
    ax.set_xlabel(r"$\mathrm{r/dxmin\ (cm)}$")
    ax.set_ylabel(r"$\mathrm{v_r/cs\ (cm/s)}$")
    ax.legend(loc=0)
    fsave= spath+ "Vrad_SinkId%s_data%s.png" % (sid[i],pf.basename[5:9])
    plt.savefig(fsave,dpi=150)
    print 'saved Vrad v. r plot for Sink ID %s' % sid[i]
print 'finished'