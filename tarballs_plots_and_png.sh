#!/bin/bash
#SBATCH -p normal
#SBATCH -t 00:20:00  
#SBATCH -J tarballs
#SBATCH -o std.o%j    
#SBATCH -e std.e%j    
#SBATCH -N 1                     # Total number of nodes requested (16 cores/node)
#SBATCH -n 1                     # Total number of tasks
#SBATCH --mail-user=kaylanb@berkeley.edu
#SBATCH --mail-type=end    # email me when the job finishes

#submit this script for dir where lev2,lev3,...,levN directories are
name=plots_and_png
find . -type d -name "plots" > ${name}.txt
find . -name "*.png" >> ${name}.txt
tar -zcvf ${name}.tar.gz -T ${name}.txt
