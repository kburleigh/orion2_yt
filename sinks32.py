'''script to make init.sink file
1. seperation b/w sink from edges of box is 1/2 seperation b/w sinks (ie periodic BCs)
2. seperation b/w sinks = Lbox / Nsinks == dX
3. position of given sink is -Lbox/2 + dX*(1/2 + index) ;index = np.arange(Nsinks)
so max(index)= Nsinks-1, so max(dX*(1/2 + index)) = 3.5*dX, Lbox = 4.*dX so works!
'''
#Star mass, x postion, y position, z position, x-momentum, y-momentum, z-momentum, x-angmom, y-angmom, z-angmom, particle id
#
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def scat3D(Lbox,x,y,z):
    '''3D plot to sanity check pos of sinks'''
    fig,ax = plt.subplots()
    ax = Axes3D(fig)
    ax.scatter(x,y,z)
    ax.set_xlim(-Lbox[0]/2.,Lbox[0]/2.)
    ax.set_ylim(-Lbox[1]/2.,Lbox[1]/2.)
    ax.set_zlim(-Lbox[2]/2.,Lbox[2]/2.)
    plt.show()

def get_Lbox_xyzSinks(Nsinks,ss):
    if Nsinks==8:
        Lbox=np.array([2*ss,2*ss,2*ss]).astype('int')
        xs=np.array([-0.5*ss,0.5*ss])
        ys=np.array([-0.5*ss,0.5*ss])
        zs=np.array([-0.5*ss,0.5*ss])
    elif Nsinks==32:
        Lbox=np.array([4*ss,4*ss,4*ss]).astype('int')
        xs=np.array([-1.5*ss,-0.5*ss,0.5*ss,1.5*ss])
        ys=np.array([-1.5*ss,-0.5*ss,0.5*ss,1.5*ss])
        zs=np.array([-0.5*ss,0.5*ss])
    elif Nsinks==64:
        Lbox=np.array([4*ss,4*ss,4*ss]).astype('int')
        xs=np.array([-1.5*ss,-0.5*ss,0.5*ss,1.5*ss])
        ys=np.array([-1.5*ss,-0.5*ss,0.5*ss,1.5*ss])
        zs=np.array([-1.5*ss,-0.5*ss,0.5*ss,1.5*ss])
    else:
        Lbox,xs,ys,zs=-1.
    return Lbox,xs,ys,zs

#cgs units!
rB_code=3.86e15
ss= 32*rB_code #sink separation
Nsinks=8
(Lbox,xs,ys,zs)= get_Lbox_xyzSinks(Nsinks,ss)
# v_when_insert=5.
# massless=True
# if massless:
#     max_lev=5
#     dx_min = 256./Lbox[0]/2**max_lev
#     vmin=0.1
#     m_code =1e-3*vmin**2*dx_min
# else: m_code=rB_code*v_when_insert**2
m_code= 2.e33


f=open("init.sink","w")
f.write("%d %d\n" % (Nsinks,Nsinks) )
cnt=0
xarr=np.zeros(Nsinks)-1
yarr= xarr.copy()
zarr= xarr.copy()
for x in xs:
    for y in ys:
        for z in zs:
            f.write("%1.2e %1.2e %1.2e %1.2e 0. 0. 0. 0. 0. 0. %d\n" % (m_code,x,y,z,cnt))
            xarr[cnt]=x
            yarr[cnt]=y
            zarr[cnt]=z
            cnt+=1
f.close()
scat3D(Lbox,xarr,yarr,zarr)
