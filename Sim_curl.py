'''import this file as module, call getCurl()
'''

import numpy as np

def PeriodicBox(vx):
    vxn=np.zeros(np.array(vx.shape)+2)
    vxn[1:-1,1:-1,1:-1]= vx
    #copy opposite edge values to ghost zones
    vxn[0,1:-1,1:-1]=vx[-1,:,:]
    vxn[-1,1:-1,1:-1]=vx[0,:,:]
    vxn[1:-1,0,1:-1]=vx[:,-1,:]
    vxn[1:-1,-1,1:-1]=vx[:,0,:]
    vxn[1:-1,1:-1,0]=vx[:,:,-1]
    vxn[1:-1,1:-1,-1]=vx[:,:,0]
    return vxn

def KaylanCurl(wid_cell,Vx,Vy,Vz):
    #finite diff
    dVy_dx= Vy[:-2,1:-1,1:-1]-Vy[2:,1:-1,1:-1]
    dVz_dx= Vz[:-2,1:-1,1:-1]-Vz[2:,1:-1,1:-1]
    dVx_dy= Vx[1:-1,:-2,1:-1]-Vx[1:-1,2:,1:-1]
    dVz_dy= Vz[1:-1,:-2,1:-1]-Vz[1:-1,2:,1:-1]
    dVx_dz= Vx[1:-1,1:-1,:-2]-Vx[1:-1,1:-1,2:]
    dVy_dz= Vy[1:-1,1:-1,:-2]-Vy[1:-1,1:-1,2:]
    #compute curl
    xhat= (dVz_dy- dVy_dz)/2./wid_cell
    yhat= -(dVz_dx - dVx_dz)/2./wid_cell
    zhat= (dVy_dx- dVx_dy)/2./wid_cell
    return (-xhat,-yhat,-zhat)  #curl is defined differencing right -left, without minus sign i'm diff left-right

def diverg_every_cell(wid_cell,Vx,Vy,Vz):
    #finite diff
    dVx_dx= Vx[:-2,1:-1,1:-1]-Vx[2:,1:-1,1:-1]
    dVy_dy= Vy[1:-1,:-2,1:-1]-Vy[1:-1,2:,1:-1]
    dVz_dz= Vz[1:-1,1:-1,:-2]-Vz[1:-1,1:-1,2:]
    return -(dVx_dx+dVy_dy+dVz_dz)/2./wid_cell #differencing right - left


def getCurl(vx,vy,vz,  ds,level=0):
    '''vx,vy,vz - are level 0 3D numpy arrays with vx,vy,vz data
    lbox,mach,cs - simulation units
    ds - ds = yt.load("data.hdf5")
    level - 0 assumed because turbulence generally ran with base grid only
    returns: curl magnitude using periodic boundary conditions to evaluate at all cells'''
    class CurlVec():
        pass
    kay_PB= CurlVec()
    vxPB=PeriodicBox(vx)
    vyPB=PeriodicBox(vy)
    vzPB=PeriodicBox(vz)
    width=1.
    (kay_PB.x,kay_PB.y,kay_PB.z)= KaylanCurl(width,vxPB,vyPB,vzPB)
    diverg= diverg_every_cell(width,vxPB,vyPB,vzPB)

    cell_dx=float(ds.domain_width[0])/ds.domain_dimensions[0]/2**level
    kay_PB.mag=np.sqrt(kay_PB.x**2+kay_PB.y**2+kay_PB.z**2)/cell_dx
    diverg=diverg/cell_dx
    return (kay_PB.mag,diverg)



