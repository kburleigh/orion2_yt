'''Use: import this file to use from ipython command line or python script

to import, put the following at top of python script and edit "Lib_path" if different:
import sys
Lib_path = "/global/home/users/kaylanb/myCode/ytScripts/Lib"
sys.path.append(Lib_path)
import deriv_fields
import perturbation_lib as pl

ex)
pl.plot_all_Pk_hist_rho_rmsV(args):
'''

#yt mods
from yt.mods import *

##
#***  import command REQUIRED above  ***

#general python mods
import h5py
import numpy as np
import numpy.fft
from math import *
from optparse import OptionParser

#my python mods
import init_cond as mic
p= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
ic = mic.calcIC(p)
ic.prout()


#class for storing vals give each "run()" call
class runPars(object):
    def __init__(self, size_box,kmin,kmax,f_sol,alpha):
        self.box = [size_box,size_box,size_box] #box dim
        self.kmin= kmin
        self.kmax = kmax
        self.f_sol = f_sol
        self.alpha = alpha
        self.seed = 0 #default
        self.dtype = np.float64 #default


def init_perturbations(n, kmin, kmax, alpha, dtype):
    kx = np.zeros(n, dtype=dtype)
    ky = np.zeros(n, dtype=dtype)
    kz = np.zeros(n, dtype=dtype)
    # perform fft k-ordering convention shifts
    for j in range(0,n[1]):
        for k in range(0,n[2]):
            kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
    for i in range(0,n[0]):
        for k in range(0,n[2]):
            ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
    for i in range(0,n[0]):
        for j in range(0,n[1]):
            kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
            
    kx = np.array(kx, dtype=dtype)
    ky = np.array(ky, dtype=dtype)
    kz = np.array(kz, dtype=dtype)
    k = np.sqrt(np.array(kx**2+ky**2+kz**2, dtype=dtype))

    # only use the positive frequencies
    inds = np.where(np.logical_and(k**2 >= kmin**2, k**2 < (kmax+1)**2))
    nr = len(inds[0])

    phasex = np.zeros(n, dtype=dtype)
    phasex[inds] = 2.*pi*np.random.uniform(size=nr)
    fx = np.zeros(n, dtype=dtype)
    fx[inds] = np.random.normal(size=nr)
    
    phasey = np.zeros(n, dtype=dtype)
    phasey[inds] = 2.*pi*np.random.uniform(size=nr)
    fy = np.zeros(n, dtype=dtype)
    fy[inds] = np.random.normal(size=nr)
    
    phasez = np.zeros(n, dtype=dtype)
    phasez[inds] = 2.*pi*np.random.uniform(size=nr)
    fz = np.zeros(n, dtype=dtype)
    fz[inds] = np.random.normal(size=nr)

    # rescale perturbation amplitude so that low number statistics
    # at low k do not throw off the desired power law scaling.
    for i in range(kmin, kmax+1):
        slice_inds = np.where(np.logical_and(k >= i, k < i+1))
        rescale = sqrt(np.sum(np.abs(fx[slice_inds])**2 + np.abs(fy[slice_inds])**2 + np.abs(fz[slice_inds])**2))
        fx[slice_inds] = fx[slice_inds]/rescale
        fy[slice_inds] = fy[slice_inds]/rescale
        fz[slice_inds] = fz[slice_inds]/rescale

    # set the power law behavior
    # wave number bins
    fx[inds] = fx[inds]*k[inds]**-(0.5*alpha)
    fy[inds] = fy[inds]*k[inds]**-(0.5*alpha)
    fz[inds] = fz[inds]*k[inds]**-(0.5*alpha)

    # add in phases
    fx = np.cos(phasex)*fx + 1j*np.sin(phasex)*fx
    fy = np.cos(phasey)*fy + 1j*np.sin(phasey)*fy
    fz = np.cos(phasez)*fz + 1j*np.sin(phasez)*fz

    return fx, fy, fz, kx, ky, kz


def normalize(fx, fy, fz, n):
    norm = np.sqrt(np.sum(fx**2 + fy**2 + fz**2)/np.product(n))
    fx = fx/norm
    fy = fy/norm
    fz = fz/norm
    return fx, fy, fz


def make_perturbations(pars):
    n = pars.box
    kmin = pars.kmin
    kmax = pars.kmax
    f_solenoidal = pars.f_sol
    alpha = pars.alpha
    seed = pars.seed 
    dtype = pars.dtype  
     
    fx, fy, fz, kx, ky, kz = init_perturbations(n, kmin, kmax, alpha, dtype)
    if f_solenoidal != None:
        k2 = kx**2+ky**2+kz**2
        # solenoidal part
        fxs = 0.; fys =0.; fzs = 0.
        if f_solenoidal != 0.0:
            fxs = fx - kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fys = fy - ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fzs = fz - kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            ind = np.where(k2 == 0)
            fxs[ind] = 0.; fys[ind] = 0.; fzs[ind] = 0.
        # compressive part
        # get a different random cube for the compressive part
        # so that we can target the RMS solenoidal fraction,
        # instead of setting a constant solenoidal fraction everywhere.
        fx, fy, fz, kx, ky, kz = init_perturbations(n, kmin, kmax, alpha, dtype)
        fxc = 0.; fyc =0.; fzc = 0.
        if f_solenoidal != 1.0:
            fxc = kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fyc = ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fzc = kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            ind = np.where(k2 == 0)
            fxc[ind] = 0.; fyc[ind] = 0.; fzc[ind] = 0.
    
        # back to real space
        pertx = np.real(np.fft.ifftn(f_solenoidal*fxs + (1.-f_solenoidal)*fxc))
        perty = np.real(np.fft.ifftn(f_solenoidal*fys + (1.-f_solenoidal)*fyc))
        pertz = np.real(np.fft.ifftn(f_solenoidal*fzs + (1.-f_solenoidal)*fzc))
    else:
        # just convert to real space
        pertx = np.real(np.fft.ifftn(fx))
        perty = np.real(np.fft.ifftn(fy))
        pertz = np.real(np.fft.ifftn(fz))
    
    # subtract off COM (assuming uniform density)
    pertx = pertx-np.average(pertx)
    perty = perty-np.average(perty)
    pertz = pertz-np.average(pertz)
    # scale RMS of perturbation cube to unity
    pertx, perty, pertz = normalize(pertx, perty, pertz, n)
    #pertx *= Vflow
    #perty *= Vflow
    #pertz *= Vflow
    return pertx, perty, pertz


def get_erot_ke_ratio(pertx, perty, pertz, n):
    x, y, z = np.mgrid[0:n[0], 0:n[1], 0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    r2 = x**2+y**2+z**2
    erot_ke_ratio = (np.sum(y*pertz-z*perty)**2 +
                     np.sum(z*pertx-x*pertz)**2 +
                     np.sum(x*perty-y*pertx)**2)/(np.sum(r2)*np.product(n)) 
    return erot_ke_ratio

def calc_Pk(pertx, perty, pertz, n, kmin, kmax):
    '''returns tuple of (k1d,power):
    k1d -- 1d array of k values
    power -- Pk for each k value'''
    fx = np.abs(np.fft.fftn(pertx))
    fy = np.abs(np.fft.fftn(perty))
    fz = np.abs(np.fft.fftn(pertz))
    fx = np.abs(fx)
    fy = np.abs(fy)
    fz = np.abs(fz)
    kx = np.zeros(n, dtype=np.float64)
    ky = np.zeros(n, dtype=np.float64)
    kz = np.zeros(n, dtype=np.float64)
    # perform fft k-ordering convention shifts
    for j in range(0,n[1]):
        for k in range(0,n[2]):
            kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
    for i in range(0,n[0]):
        for k in range(0,n[2]):
            ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
    for i in range(0,n[0]):
        for j in range(0,n[1]):
            kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
    k = np.sqrt(np.array(kx**2+ky**2+kz**2,dtype=np.float64))
    k1d = []
    power = []
    for i in range(kmin,kmax+1):
        slice_inds = np.where(np.logical_and(k >= i, k < i+1))
        k1d.append(i+0.5)
        power.append(np.sum(fx[slice_inds]**2 + fy[slice_inds]**2 + fz[slice_inds]**2))
        print i,power[-1]
    return k1d,power

def Pk_zdrv(path,f_zdrv,spath):
    '''plots 1D power spectrum for zdrv.hdf5
    f_zdrv: string, name of zdrv output file'''
    f = h5py.File(path+f_zdrv, 'r')
    pertx=f["pertx"]
    perty=f["perty"]
    pertz=f["pertz"]
    a = f.attrs

    x=pertx[:,:,:]
    n= list( x.shape )
    kmin = a["kmin"]
    kmax= a["kmax"] 
    #calc Pk
    k1d,Pk = calc_Pk(pertx, perty, pertz, n, kmin, kmax)   
    #plot Pk
    fname = spath + "Pk_zdrv_box%s_kmin%s_kmax%s_alpha%s_fsol%s.pdf" % (a["box"][0],a["kmin"],a["kmax"],a["alpha"],a["f_solenoidal"])
    py.loglog(k1d, Pk)
    py.title(r"$T/T_{\rmCross}$ = %g" % 0.)
    py.savefig(fname,format="pdf")
    py.close() 

def Pk_simData(path,f_simData, f_zdrv, spath, ic):
    '''plots 1D power spectrum for data.0*.3d.hdf5 (simulation data)'''
    import pylab as py
    #get pars from zdrv file
    f = h5py.File(path+f_zdrv, 'r')
    a = f.attrs
    n = list(a["box"])
    kmin = a["kmin"]
    kmax = a["kmax"]
    #get simData for Pk calc
    pf = load(path+f_simData)
    Tratio = pf.current_time / ic.tCr
    fields=["x-vel","y-vel","z-vel"]
    lev = 0
    dims= list(pf.domain_dimensions)
    if dims != n:
        raise Exception("sim cube and fourier cube not same")
    cube0= pf.h.covering_grid(level=lev,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    pertx=cube0["x-vel"]
    perty=cube0["y-vel"]
    pertz=cube0["z-vel"]
    #calc Pk
    k1d,Pk = calc_Pk(pertx, perty, pertz, n, kmin, kmax)
    #plot
    fname= spath + "Pk_simData.%s.lev%s.pdf" % (f_simData[5:9],lev)
    py.loglog(k1d, Pk)
    py.title(r"$T/T_{\rmCross}$ = %g" % Tratio)
    py.savefig(fname,format="pdf")
    py.close()    

def plot_hist_rmsV(Vx,Vy,Vz, fname, mach_f, tSim, ic):
    '''Vx, Vy, Vz -- 3D numpy arrays of each velocity component
    '''
    import pylab as py
    #calc rms vel and convert to rms Mach
    rmsV = (Vx**2+Vy**2+Vz**2)**0.5
    rmsV1d = np.reshape(rmsV, np.product(rmsV.shape))
    rmsM = rmsV1d / ic.cs
    logM= np.log10( rmsM )
    cnt,bins,patches = py.hist(logM, bins=100, color="blue")
    #plot quantities
    Tratio = tSim / ic.tCr
    mach_i = ic.v0 / ic.cs
    #plot
    py.vlines(np.log10( mach_i ), 0, cnt.max(), colors="red", linestyles='dashed',label=r"rms$\mathcal{M}_{i}$ = %2.1f" % mach_i)
    py.vlines(np.log10( mach_f ), 0, cnt.max(), colors="red", linestyles='solid',label=r"rms $\mathcal{M}$ = %2.1f" % mach_f)
    py.xlabel(r"$Log_{10}$ rms $\mathcal{M}_s$")
    py.ylabel("count")
    py.title(r"$T/T_{\rmCross}$ = %g" % Tratio)
    py.legend(loc=0, fontsize="small")
    py.savefig(fname,format="pdf")
    py.close()

def hist_rmsV_zdrv(path,f_zdrv, spath, mach_f,ic):
    '''f_zdrv: string, name of zdrv.hdf5
    Vst: initial rms velocity of turbulence'''
    import string as mys
    f = h5py.File(path+f_zdrv, 'r')
    x=f["pertx"]
    y=f["perty"]
    z=f["pertz"]
    a= f.attrs
    #get data cubes and make hist
    x=x[:,:,:]
    y=y[:,:,:]  
    z=z[:,:,:]
    fname = spath + "Hist_kmin%s_kmax%s_alpha%s_fsol%s_Vst%g.pdf" % (a["kmin"],a["kmax"],a["alpha"],a["f_solenoidal"],Vst)
    fname = mys.replace(fname,"+","") #remove '+' from name
    tSim=0.
    plot_hist_rmsV(x,y,z, fname, mach_f, tSim,ic)

def hist_rmsV_simData(path,f_simData, f_zdrv,spath, mach_f,ic):
    '''f_simData: string, name of simData
    mach_f: final rms sonic mach number desired
    '''
    import string as mys
    #get pars from zdrv file
    f = h5py.File(path+f_zdrv, 'r')
    a = f.attrs
    n = list(a["box"])
    kmin = a["kmin"]
    kmax = a["kmax"]
    #get simData for Pk calc
    pf = load(path+f_simData)
    tSim = pf.current_time 
    fields=["x-vel","y-vel","z-vel"]
    lev = 0
    dims= list(pf.domain_dimensions)
    if dims != n:
        raise Exception("sim cube and fourier cube not same")
    cube0= pf.h.covering_grid(level=lev,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    x=cube0["x-vel"]
    y=cube0["y-vel"]
    z=cube0["z-vel"]
    fname= spath + "Hist_rmsV_simData.%s.lev%s.pdf" % (f_simData[5:9],lev)
    fname = mys.replace(fname,"+","") #remove '+' from name
    plot_hist_rmsV(x,y,z, fname, mach_f, tSim, ic)

def plot_hist_rmsRho_Levs0(rho_L0, fname, tSim,ic):
    ''''''
    import pylab as py
    #make 6 hists
    rms = rho_L0
    lab = r"$\rho$, Lev 0"
    rms1d = np.reshape(rms, np.product(rms.shape))
    logrms= np.log10(rms1d)
    #plot quantities
    Tratio = tSim / ic.tCr
    #plot
    cnt,bins,patches = py.hist(logrms, bins=100, color="blue", alpha=0.5, label=lab)
    py.vlines(np.log10( ic.rho0 ), 0, cnt.max(), colors="black", linestyles='dashed',label=r"$\rho_{0}$ = %2.2g [g/cm^3]" % ic.rho0)
    #py.xlim([-13.,-9.])
    py.xlabel("Log10 Density [g/cm^3]")
    py.ylabel("count")
    py.title(r"$T/T_{\rmCross}$ = %g" % Tratio)
    py.legend(loc=0, fontsize="small")
    py.savefig(fname,format="pdf")
    py.close()

def hist_rmsRho_simData_Levs0(path,f_simData, f_zdrv, spath, ic):
    '''f_simData: string, name of simData
    rho0: initial density
    f_zdrv: only used to make sure dimensions cube match'''
    import string as mys
    #load zdrv cube to check dimensions
    f = h5py.File(path+f_zdrv, 'r')
    a = f.attrs
    n = list(a["box"])
    #get simData
    pf = load(path+f_simData)
    dims= list(pf.domain_dimensions)
    if dims != n:
        raise Exception("sim cube and fourier cube not same")
    tSim = pf.current_time 
    fields=["density"]
    #get data for each level
    lev = 0
    cube= pf.h.covering_grid(level=lev,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    rho_L0=cube["density"]
    #make hist
    fname= spath + "Hist_Rho_Lev0_simData.%s.pdf" % (f_simData[5:9],)
    fname = mys.replace(fname,"+","") #remove '+' from name
    plot_hist_rmsRho_Levs0(rho_L0, fname, tSim, ic)

def plot_hist_rmsRho_Levs012(rho_L0, rho_L1, rho_L2, tSim, fname):
    ''''''
    import pylab as py
    #make 6 hists
    rms = rho_L0
    lab = r"$\rho$, Lev 0"
    rms1d = np.reshape(rms, np.product(rms.shape))
    logrms= np.log10(rms1d)
    cnt,bins,patches = py.hist(logrms, bins=100, color="blue", alpha=0.5, label=lab)
    rms = rho_L1
    lab = r"$\rho$, Lev 1"
    rms1d = np.reshape(rms, np.product(rms.shape))
    logrms= np.log10(rms1d)
    cnt,bins,patches = py.hist(logrms, bins=100, color="red", alpha=0.5, label=lab)
    rms = rho_L2
    lab = r"$\rho$, Lev 2"
    rms1d = np.reshape(rms, np.product(rms.shape))
    logrms= np.log10(rms1d)
    #plot quantities
    Tratio = tSim / ic.tCr
    #plot
    cnt,bins,patches = py.hist(logrms, bins=100, color="green", alpha=0.5,label=lab)
    py.vlines(np.log10( ic.rho0 ), 0, cnt.max(), colors="black", linestyles='dashed',label=r"$\rho_{0}$ = %2.2g [g/cm^3]" % ic.rho0)
    #py.xlim([-13.,-9.])
    py.xlabel("Log10 Density [g/cm^3]")
    py.ylabel("count")
    py.title(r"$T/T_{\rmCross}$ = %g" % Tratio)
    py.legend(loc=0, fontsize="small")
    py.savefig(fname,format="pdf")
    py.close()

def hist_rmsRho_simData_Levs012(path,f_simData, f_zdrv, spath, ic):
    '''f_simData: string, name of simData
    f_zdrv: only used to make sure dimensions cube match'''
    import string as mys
    #load zdrv cube to check dimensions
    f = h5py.File(path+f_zdrv, 'r')
    a = f.attrs
    n = list(a["box"])
    #get simData
    pf = load(path+f_simData)
    dims= list(pf.domain_dimensions)
    if dims != n:
        raise Exception("sim cube and fourier cube not same")
    tSim = pf.current_time
    fields=["density"]
    #get data for each level
    lev = 0
    cube= pf.h.covering_grid(level=lev,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    rho_L0=cube["density"]
    lev = 1
    cube= pf.h.covering_grid(level=lev,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    rho_L1=cube["density"]
    lev = 2
    cube= pf.h.covering_grid(level=lev,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    rho_L2=cube["density"]
    #make hist
    fname= spath + "Hist_Rho_Lev012_simData.%s.pdf" % (f_simData[5:9],)
    fname = mys.replace(fname,"+","") #remove '+' from name
    plot_hist_rmsRho_Levs012(rho_L0, rho_L1, rho_L2, fname, tSim, ic)

def perturbation(size_box,kmin,kmax,f_sol,alpha, spath):
    '''size_box -- integer
    kmin, kmax -- integers
    f_sol -- float
    alpha -- float
    spath -- where save hdf5 perturbation cube'''
    pars = runPars(size_box,kmin,kmax,f_sol,alpha)
    n = pars.box
    kmin = pars.kmin
    kmax = pars.kmax
    f_solenoidal = pars.f_sol
    alpha = pars.alpha
    seed = pars.seed 
    dtype = pars.dtype   

    ###
    ###
    if kmin > kmax or kmin < 0 or kmax < 0:
        print "kmin must be < kmax, with kmin > 0, kmax > 0.  See --help."
        sys.exit(0)
    if kmax > floor(np.min(n))/2:
        print "kmax must be <= floor(size/2).  See --help."
        sys.exit(0)
    if f_solenoidal == "None" or f_solenoidal == "none":
        f_solenoidal = None
    else:
        f_solenoidal = float(f_solenoidal)
        if f_solenoidal > 1. or f_solenoidal < 0.:
            print "You must choose f_solenoidal.  See --help."
            sys.exit(0)
    if alpha==None:
        print "You must choose a power law slope, alpha.  See --help."
        sys.exit(0)
    if alpha < 0.:
        print "alpha is less than zero. Thats probably not what ou want.  See --help."
        sys.exit(0)
    # ratio of solenoidal to compressive components
    if f_solenoidal=="None" or f_solenoidal==None:
        f_solenoidal = None
    else:
        f_solenoidal = min(max(float(f_solenoidal), 0.), 1.)
    ###
    ###

    np.random.seed(seed=seed)
    pertx, perty, pertz = make_perturbations(pars)
    erot_ke_ratio = get_erot_ke_ratio(pertx, perty, pertz, n)
    print "erot_ke_ratio = ", erot_ke_ratio

    #plot_spectrum1D(pertx,perty,pertz,spath)

    #divV_rms = 0
    #for i in range(1,n[0]-1):
    #    for j in range(1,n[1]-1):
    #        for k in range(1,n[2]-1):
    #            divV_rms += ((pertx[i+1,j,k]-pertx[i-1,j,k])+(perty[i,j+1,k]-perty[i,j-1,k])+(pertz[i,j,k+1]-pertz[i,j,k-1]))**2
    #divV_rms = sqrt(divV_rms/np.product(n))
    #print divV_rms


    # hdf5 output
    f = h5py.File(spath+'zdrv.hdf5', 'w')
    ds = f['/'].create_dataset('pertx', n, dtype=np.float)
    ds[:] = pertx
    ds = f['/'].create_dataset('perty', n, dtype=np.float)
    ds[:] = perty
    ds = f['/'].create_dataset('pertz', n, dtype=np.float)
    ds[:] = pertz
    f['/'].attrs['box'] = n
    f['/'].attrs['kmin'] = kmin
    f['/'].attrs['kmax'] = kmax
    f['/'].attrs['alpha'] = alpha
    if f_solenoidal!=None: f['/'].attrs['f_solenoidal'] = f_solenoidal
    f['/'].attrs['erot_ke_ratio'] = erot_ke_ratio
    f['/'].attrs['seed'] = seed
    f.close()

##==================================
    
    
def Pk_all_data(path, ic):
    '''
    path: string, path to data
    ic: class calcIC() object, contains initial conditions for simulation'''
    spath = path + "plots/"
    patt = "data.00*.3d.hdf5"
    files = glob.glob1(path, patt)
    print "## READING FROM DIR: %s" % path
    for f in files:
        print "## READING ##: %s" % f
        Pk_simData(path,f, "zdrv.hdf5", spath,ic)

def hist_rmsV_all_data(path, mach_f, ic):
    '''
    path: string, path to data
    ic: class calcIC() object, contains initial conditions for simulation'''
    spath = path + "plots/"
    patt = "data.00*.3d.hdf5"
    files = glob.glob1(path, patt)
    print "## READING FROM DIR: %s" % path
    for f in files:
        print "## READING ##: %s" % f
        hist_rmsV_simData(path,f, "zdrv.hdf5", spath, mach_f, ic)


def hist_Rho_all_data(path,levs,ic):
    '''
    path: absolute path to simulation data
    levs: integer for highest resolution level, either 0 or 2 (unigrid is 0), edit "hist_rmsRho_simData_Levs012()" to add any level functionality 
    ic: class calcIC() object, contains initial conditions for simulation'''
    spath = path+"plots/"
    patt = "data.00*.3d.hdf5"
    files = glob.glob1(path, patt)
    print "## READING FROM DIR: %s" % path
    for f in files:
        print "## READING ##: %s" % f
        if levs == 0:
            hist_rmsRho_simData_Levs0(path,f, "zdrv.hdf5", spath, ic)
        else:
            hist_rmsRho_simData_Levs012(path,f, "zdrv.hdf5", spath, ic)

def plot_all_Pk_hist_rho_rmsV(path, levs, mach_f, ic):
    '''purpose: efficiently (open each data file only once) to make 3 plots for each data file: Pk, density histogram, rms velocity hist 
    path: absolute path to simulation data
    levs: integer for highest resolution level, either 0 or 2 (unigrid is 0), edit "hist_rmsRho_simData_Levs012()" to add any level functionality 
    mach_f: final rms sonic mach number desired
    ic: class calcIC() object, contains initial conditions for simulation
    '''
    spath = path + "plots/"
    patt = "data.00*.3d.hdf5"
    files = glob.glob1(path, patt)
    print "## READING FROM DIR: %s" % path
    for f in files:
        print "## READING ##: %s" % f
        Pk_simData(path,f, "zdrv.hdf5", spath, ic)
        hist_rmsV_simData(path,f, "zdrv.hdf5", spath, mach_f,ic)
        if levs == 0:
            hist_rmsRho_simData_Levs0(path,f, "zdrv.hdf5", spath, ic)
        else:
            hist_rmsRho_simData_Levs012(path,f, "zdrv.hdf5", spath, ic)
