'''
Purpose: for ORION2 sims, import file to use derived fields coming purley from density,mom,pressure data

How to use:  put the following at top of python script:
import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import deriv_fields
'''

'''
take_log = True   if want log plot for any yt plotting routine on field
'''

def _xvel(field, data):
    return data['X-momentum']/data['density']
add_field("x-vel", function=_xvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _yvel(field, data):
    return data['Y-momentum']/data['density']
add_field("y-vel", function=_yvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _zvel(field, data):
    return data['Z-momentum']/data['density']
add_field("z-vel", function=_zvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bx(field, data):
    return (4.*fc.pi)**0.5 *data['X-magnfield']
add_field("Bx", function=_Bx, take_log=False,
          units=r'\rm{gauss}')

def _By(field, data):
    return (4.*fc.pi)**0.5 *data['Y-magnfield']
add_field("By", function=_By, take_log=False,
          units=r'\rm{gauss}')

def _Bz(field, data):
    return (4.*fc.pi)**0.5 *data['Z-magnfield']
add_field("Bz", function=_Bz, take_log=False,
          units=r'\rm{gauss}')

##
def _Vmag(field, data):
    return np.sqrt(data['x-vel']**2 + data['y-vel']**2 + data['z-vel']**2)
add_field("Vmag", function=_Vmag, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bmag(field, data):
    return np.sqrt( data['Bx']**2 + data['By']**2 + data['Bz']**2)
add_field("Bmag", function=_Bmag, take_log=False,
          units=r'\rm{gauss}')

def _Va(field, data):
    return data['Bmag'] / np.sqrt(4*np.pi* data['density'])
add_field("Va", function=_Va, take_log=False,
          units=r'\rm{cm/s}')

		




