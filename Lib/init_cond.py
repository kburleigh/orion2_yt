'''
Purpose: import this library to calc initial conditions for simulation, store in "ic" class

How to use:  put the following at top of python script:
import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import init_cond as mic

ex)
p= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
ic = mic.calcIC(p)
ic.prout()
'''

import pprint
import numpy as np
import my_fund_const as fc

class ParsNeed(object):
    def __init__(self):
        self.gamma = 1.001
        self.temp=10. #kelvins
        self.mu = 0.62 #solar metalicity
        self.mach_s=40.
        self.beta=0.08
        self.Msink=2.e32
        self.Lbox=10. #units of rB
        self.cells=64
        self.levs=0
        self.Cnum=0.5

        print "PARAMATERS are:"
        self.prout()	
    def prout(self):
        print "PARAMATERS are:"
        pprint.pprint(self.__dict__)

class calcIC(object):
    def __init__(self, pars):
        '''pars is a ParsNeed() class '''
        self.cells = pars.cells
        self.gamma = pars.gamma
        self.temp = pars.temp
        self.beta = pars.beta
        self.mach_s = pars.mach_s
        self.Msink = pars.Msink
        self.cs = (pars.gamma*fc.kb*pars.temp/pars.mu/fc.mp)**0.5 #adiabatic sound speed
        self.v0 = pars.mach_s * self.cs
        self.va = (2./pars.beta)**0.5 * self.cs
        self.mach_a = self.v0 / self.va
        #lengths 
        self.rB = fc.G * pars.Msink/self.sumV2()
        self.Lbox = pars.Lbox* self.rB
        self.dx = self.Lbox/self.cells
        #dens
        self.rho0 = 1.e-3 * pars.Msink/self.Lbox**3
        self.B = self.va * (4.*fc.pi*self.rho0)**0.5
        #times
        self.tB = self.rB / self.sumV()
        self.tCr = self.Lbox/ self.maxV()
        self.tCN = pars.Cnum*self.dx/self.sumV() 
        #floors & ceiling
        self.small_dn = 1.e-12* self.rho0
        self.small_pr = self.small_dn*self.cs**2/self.gamma
        self.ceil_va = 1.e2* self.B/(4.*fc.pi*self.small_dn)**0.5
        self.floor_Tgas = self.temp / 10.
    def sumV(self):
       return self.cs + self.v0 + self.va
    def sumV2(self):
       return self.cs**2 + self.v0**2 + self.va**2
    def maxV(self):
        return np.max( (self.cs,self.v0,self.va) )
    def prout(self):
       print "UNITS: cgs"
       pprint.pprint(self.__dict__)

def MsinkNeeded(r_B,c_s,mach_s):
    return r_B * (c_s*mach_s)**2 / fc.G









