'''initial conditions for Production Run
'''

import numpy as np
import collections

import my_fund_const as fc


class ClassInfo:
    '''purpose: base class to print info about instances for any class
    how use: all my classes should inherit this class
    '''
    def printVars(self):
        import pprint
        pprint.pprint(self.__dict__)
    def printMethods(self):
	        print dir(self.__class__)
    def PrintAll(self):
        for key in self.__dict__.keys(): 
            print "METHOD: %s" % key
            self.__dict__[key].PrintMe()

class SimProp(ClassInfo):
    '''all properties describing my simulation'''  
    print "##\nBUG: NumsWant.SinkM should be 51./64 NOT 50.\n##"
    print "##\nPROBLEM: Is cs really constant? for \gamma = 1.001?\n##"

    def __init__(self):
        #add classes
        self.Turb = Turb()
        self.Sinks = Sinks()
    
class Turb(ClassInfo):
	'''properties upon turning on turbulence at time = 0''' 
	def __init__(self):
		self.LevelMax = 0
		self.MachS = 40.
		self.MachA = 200. #\beta = 0.1, other way gives \macha < 1 which do not want b/c va incr while v0 decr w/time: E_f = E_i -> rho_f*(v_sf^2 +v_af^2) ~ rho_i*(v_si^2+v_ai^2)
		self.cs = 1.
		self.v0 = self.cs*self.MachS
		self.va = self.v0/self.MachA
		self.rho0 = 1.
		self.gamma = 1.001
		self.G = 1.
		self.mu = 0.62 #mp = usual mp, kb = usual kb
		#
		#self.MassBox = ??
		self.Lbox = 2.
		self.NCells = 256.
		#
		self.T0 = self.cs**2 * self.mu *fc.mp / self.gamma / fc.kb
		self.B = self.va * np.sqrt(4*fc.pi* self.rho0)
		self.Beta = 2.*(self.MachA / self.MachS)**2
		#
		self.small_dn = 1.e-12* self.rho0
		self.small_pr = self.small_dn* self.cs**2/ self.gamma
		self.ceil_va = 1.e2* self.B/(4.*fc.pi*self.small_dn)**0.5
		self.floor_Tgas = 1.e-2 * self.T0
		#
		self.CFL = 0.5
		self.tstop = 5.*self.tCross()
		self.first_dt = 0.5*self.CFL*self.dxMin()/ self.maxV()
	def maxV(self):
		return np.max( (self.cs,self.v0,self.va) )
	def dxMin(self):
		return self.Lbox/self.NCells/2.**self.LevelMax
	def tCross(self):
		return self.Lbox/ self.maxV()

class Sinks(ClassInfo):
	'''properties upon inserting sinks, after statistical equil. reached'''
	def __init__(self):
		self.LevelMax = 2
		self.MachS = 5.
		self.MachA = 1.
		self.cs = 1. #approx
		self.v0 = self.cs*self.MachS
		self.va = self.v0/self.MachA
		self.Beta = 2.*(self.MachA / self.MachS)**2
		#    
		self.NSinks = 64
		self.SinkSep = 0.5 #simulation units
		self.SinkSepRbha = 32. #bondi radii units
		self.Rbha = self.SinkSep / self.SinkSepRbha
		self.SinkM = self.Rbha* self.cs**2*(1+ self.MachS**2 + self.MachS**2/self.MachA**2) / fc.G #should be: (2+25)/64 = 0.4218
	
		print "Sink Mass must be sufficienly large that can fit 4 sinks across simulation box. So require cs ~ 1, v_rms ~ 5, AND v_a <= v_rms at time insert sinks"
		print "CHECK SIMULATION DATA!" 

#class Units(ClassInfo):
#	def __init__(self, TurnOnTurb() ):
#		self.LengthBox=3.1e18 #in cm = 1 cp = R GMC
#		self.MassSinks = 1.e4* fc.msun  #in g = M GMC
#		self.MassOfSink = self.MassSinks / TurnOnTurb.NSinks #sinks constitute ~ 99.99% mass, sinks NOT STARS in this case b/c tooo massive!?
#		self.MassGas= 1.e-3 * self.MassSinks #self-gravity negligible, so all gas is only 1/1000 mass in sinks  
#		self.TempBox=10. #in K = Temp gmc
#		#
#		self.cs= np.sqrt(fc.kb*self.TBox / TurnOnTurb.mu/fc.mp)
#		
#	def printFullDims(self, TurnOnTurb() ):
#		pass
#
#print "what is mass in gas of box?"	

s=SimProp()		
