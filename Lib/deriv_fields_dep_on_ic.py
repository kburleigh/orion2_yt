'''
Purpose: derived fields that depend on initial condition values

How to use:  put the following at top of python script:
import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import init_cond as mic
import deriv_fields_dep_on_ic as dfic

ex)
pars= dfic.ParsNeed()  #then set pars.[pars] to new vals as needed
ic = dfic.calcIC(pars)
ic.prout()

dfic.add_fields_dep_ic(ic)
'''

from yt.mods import *

#derived fields
def add_fields_dep_ic(ic):
	'''defines and adds all fields, given ic
	some fields need initial condition values to be calculate
	take_log = True   if want log plot for any yt plotting routine on field
	'''
	def _temp(field, data):
	    '''temp of isothermal gas'''
	    mu = 0.5
	    cs=40631.0466
	    return data["density"]*0. + ic.cs**2 * mu*fc.mp/fc.kb
	add_field("temp", function=_temp, take_log=False,
			units=r'\rm{K}')

	def _xvel_norm(field, data):
	    '''v_flow/v_problem_rms'''
	    return data["x-vel"] /(ic.sumV2())**0.5
	add_field("x-vel-norm", function=_xvel_norm, take_log=False,
			units=r'\rm{unitless}')

	def _yvel_norm(field, data):
	    '''v_flow/v_problem_rms'''
	    return data["y-vel"] /(ic.sumV2())**0.5
	add_field("y-vel-norm", function=_yvel_norm, take_log=False,
			units=r'\rm{unitless}')

	def _zvel_norm(field, data):
	    '''v_flow/v_problem_rms'''
	    return data["z-vel"] /(ic.sumV2())**0.5
	add_field("z-vel-norm", function=_zvel_norm, take_log=False,
			units=r'\rm{unitless}')

	def _rho_norm(field, data):
	    '''density/rho0'''
	    return data["density"]/ic.rho0
	add_field("rho-norm", function=_rho_norm, take_log=False,
			units=r'\rm{unitless}')

