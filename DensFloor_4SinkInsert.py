import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

#yt derived fields
def _xvel(field, data):
    return data['X-momentum']/data['density']
add_field("x-vel", function=_xvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _yvel(field, data):
    return data['Y-momentum']/data['density']
add_field("y-vel", function=_yvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _zvel(field, data):
    return data['Z-momentum']/data['density']
add_field("z-vel", function=_zvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bx(field, data):
    return (4.*fc.pi)**0.5 *data['X-magnfield']
add_field("Bx", function=_Bx, take_log=False,
          units=r'\rm{gauss}')

def _By(field, data):
    return (4.*fc.pi)**0.5 *data['Y-magnfield']
add_field("By", function=_By, take_log=False,
          units=r'\rm{gauss}')

def _Bz(field, data):
    return (4.*fc.pi)**0.5 *data['Z-magnfield']
add_field("Bz", function=_Bz, take_log=False,
          units=r'\rm{gauss}')

def _Vmag(field, data):
    return np.sqrt(data['x-vel']**2 + data['y-vel']**2 + data['z-vel']**2)
add_field("Vmag", function=_Vmag, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bmag(field, data):
    return np.sqrt( data['Bx']**2 + data['By']**2 + data['Bz']**2)
add_field("Bmag", function=_Bmag, take_log=False,
          units=r'\rm{gauss}')

def _Va(field, data):
    return data['Bmag'] / np.sqrt(4*np.pi* data['density'])
add_field("Va", function=_Va, take_log=False,
          units=r'\rm{cm/s}')
#####################

path="/Users/kburleigh/GradSchool/Research/w_McKee/orion2_myFiles/images/stampede/MHDMyMac/drive_MachS-5_MachA-1/maxRes-rBdiv128/"
f_drive_hdf5= glob.glob1(path,"data.0011.3d.hdf5")[-1]
pf=load(path+f_drive_hdf5)

cube= pf.h.covering_grid(level=0,\
                          left_edge=pf.h.domain_left_edge,
                          dims=pf.domain_dimensions)
d = cube["density"]
Bmag = cube["Bmag"]
MassWt= d/d.max()
DensAvg=np.average(d)
DensWtAvg=np.average(d,weights=MassWt)
BmagAvg=np.average(Bmag)
BmagWtAvg=np.average(Bmag,weights=MassWt)

print "DensAvg: %g,DensWtAvg: %g,BmagAvg: %g,BmagWtAvg: %g" % (DensAvg,DensWtAvg,BmagAvg,BmagWtAvg)

vals=np.log10(d.flatten())
(n, bins, patches)=plt.hist(vals,bins=100)
plt.xlabel("Log10(Density)")
name=path+"plots/"+"hist_dens_%s.pdf" % f_drive_hdf5[-12:-8]
plt.savefig(name,dpi=200)

#dens_floor=1.e-2*10**-17.75
#va_ceil= 1.e2*BmagWtAvg/(4.*np.pi*dens_floor)**0.5