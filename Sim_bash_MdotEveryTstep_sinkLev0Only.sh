#!/bin/bash 
#input: 
#   standard out from simulation writing sink particle info every finest-level step (AMRLevelOrion.cpp, SinkParticleList.cpp print statements)
#output:
#   all finest-level sink data. it follows all lines containing "KJB:AMRLevelOrion.cpp: advance max level" 
#Run
#   1)run "Sim_SinkData_EveryTStep.py" instead, which calls this script
#   or 2) ./this_script concatenated_pout maxlevel nsinks output_file_name
pout=${1}
fout=${2}
echo "#number of sink info outputs:" > $fout
pts=$(grep $pout -e "KJB2: level 0" | wc -l) 
echo "#npts: "$pts >> $fout

