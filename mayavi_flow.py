"""
An example displaying the trajectories for the Lorenz system of
equations along with the z-nullcline.

The vector field of the Lorenz system flow is integrated to display
trajectories using mlab's flow function:
:func:`mayavi.mlab.flow`.

The z-nullcline is plotted by extracting the z component of the vector
field data source with the ExtractVectorComponent filter, and applying
an IsoSurface module on this scalar component.
"""
# Author: Prabhu Ramachandran <prabhu@aero.iitb.ac.in>
# Copyright (c) 2008-2009, Enthought, Inc.
# License: BSD Style.

import numpy as np
from mayavi import mlab
#import yt
import argparse
import os
#import glob 
import pickle
import sys

parser = argparse.ArgumentParser(description="test")
#parser.add_argument("-twod",choices=['slice','projection'],action="store",help='panels are slice or projection',required=True)
#parser.add_argument("-data_hdf5",action="store",help='wildcard for hdf5 files',required=True)
parser.add_argument("-bpickle",action="store",help='wildcard for hdf5 files',required=True)
args = parser.parse_args()


#def _KaysVx(field, data):
#    return data["X-momentum"] / data["density"]
#def _KaysVy(field, data):
#    return data["Y-momentum"] / data["density"]
#def _KaysVz(field, data):
#    return data["Z-momentum"] / data["density"]
#def _KaysBx(field, data):
#    rt4pi= 3.5449
#    return data["X-magnfield"]*rt4pi
#def _KaysBy(field, data):
#    rt4pi= 3.5449
#    return data["Y-magnfield"]*rt4pi
#def _KaysBz(field, data):
#    rt4pi= 3.5449
#    return data["Z-magnfield"]*rt4pi
#
#ds = yt.load(args.data_hdf5) # load data
#ds.add_field("KaysVx", function=_KaysVx, units="cm/s")
#ds.add_field("KaysVy", function=_KaysVy, units="cm/s")
#ds.add_field("KaysVz", function=_KaysVz, units="cm/s")
#ds.add_field("KaysBx", function=_KaysBx, units="gauss")
#ds.add_field("KaysBy", function=_KaysBy, units="gauss")
#ds.add_field("KaysBz", function=_KaysBz, units="gauss")
#
#center = [0,0,0]
#width = (float(ds.domain_width[0]), 'cm') 
#Npx= 1000
#res = [Npx, Npx] # create an image with 1000x1000 pixels
#proj = ds.slice(axis='z',coord=0.)
#frb = proj.to_frb(width=width, resolution=res, center=center)
#rho0=1e-2
#img= np.array(frb['density'])[::-1]/rho0
#Bhoriz,Bvert= np.array(frb['KaysBx'])[::-1], np.array(frb['KaysBy'])[::-1]
##im= ax[i].imshow(np.log10(img),cmap=cmaps[args.cmap],\
##				vmin=np.log10(args.clim[0]),vmax=np.log10(args.clim[1]))
##ax[i].autoscale(False)
##quiver plot
#X, Y = np.meshgrid(np.arange(0,Npx), np.arange(0,Npx))
#fout=open(os.path.join(os.path.dirname(args.data_hdf5),'xyBxBy.pickle'),'w')
#pickle.dump((X,Y,Bhoriz,Bvert),fout)
#fout.close()
#sys.exit()	

fin=open(args.bpickle,'r')
(x,y,Bx,By)= pickle.load(fin)
fin.close()
fig = mlab.figure(size=(500, 500), bgcolor=(0, 0, 0))
#
# Plot the flow of trajectories with suitable parameters.
Bz=np.zeros(Bx.shape)+1
f = mlab.flow(x,y,y,Bx,By,Bz, line_width=3, colormap='Paired')
f.module_manager.scalar_lut_manager.reverse_lut = True
f.stream_tracer.integration_direction = 'both'
f.stream_tracer.maximum_propagation = 200
# Uncomment the following line if you want to hide the seed:
#f.seed.widget.enabled = False
#
# A nice view of the plot.
#mlab.view(140, 120, 113, [0.65, 1.5, 27])
mlab.show()
