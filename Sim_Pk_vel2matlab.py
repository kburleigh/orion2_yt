import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-f_hdf5",action="store",help='hdf5 file to get velocities for')
args = parser.parse_args()

import yt
import numpy as np
import scipy.io as sio

#def PeriodicBox(vx):
#    vxn=np.zeros(np.array(vx.shape)+2)
#    vxn[1:-1,1:-1,1:-1]= vx
#    #copy opposite edge values to ghost zones
#    vxn[0,1:-1,1:-1]=vx[-1,:,:]
#    vxn[-1,1:-1,1:-1]=vx[0,:,:]
#    vxn[1:-1,0,1:-1]=vx[:,-1,:]
#    vxn[1:-1,-1,1:-1]=vx[:,0,:]
#    vxn[1:-1,1:-1,0]=vx[:,:,-1]
#    vxn[1:-1,1:-1,-1]=vx[:,:,0]
#    return vxn

ds=yt.load(args.f_hdf5)
lev= 0
cube= ds.covering_grid(level=lev,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions)
rho = np.array(cube["density"])
vx = np.array(cube["X-momentum"]/rho)
vy = np.array(cube["Y-momentum"]/rho)
vz = np.array(cube["Z-momentum"]/rho)
#for curl use Periodic BC's to increase size of 3d array to preserve dimensions
#but not necessary for power spectrum
sio.savemat('Vxyz.mat', {'v1':vx,'v2':vy,'v3':vz,'ndim':vx.shape[0]})
#vxPB=PeriodicBox(vx)
#vyPB=PeriodicBox(vy)
#vzPB=PeriodicBox(vz)
#sio.savemat('vels_PB.mat', {'v1':vxPB,'v2':vyPB,'v3':vzPB,'ndim':vxPB.shape[0]})
