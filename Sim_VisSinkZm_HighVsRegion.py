'''zoom in slice and overlay velocity vectors for each sink in x,z dir. 8 per sink, 2 per dir so 16 plots per time step'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-nsinks",action="store",help='Number of sink particles')
parser.add_argument("-rbh",action="store",help='bondi hoyle radius in cm')
parser.add_argument("-cs",action="store",help='sound speed in cm/s')
parser.add_argument("-dnflr",action="store",help='density floor in g cm^-3')
args = parser.parse_args()
if args.nsinks: Nsinks = int(args.nsinks)
else: raise ValueError
if args.rbh: rBH = float(args.rbh)
else: raise ValueError
if args.cs: cs = float(args.cs)
else: raise ValueError
if args.dnflr: DnFlr = float(args.dnflr)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

def _vx(field, data):
    return data['X-momentum']/data['density']
add_field("vx", function=_vx, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _vy(field, data):
    return data['Y-momentum']/data['density']
add_field("vy", function=_vy, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _vz(field, data):
    return data['Z-momentum']/data['density']
add_field("vz", function=_vz, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Vrms(field, data):
    return np.sqrt(data['vx']**2 + data['vy']**2 + data['vz']**2)
add_field("Vrms", function=_Vrms, take_log=False,
        units=r'\rm{cm}/\rm{s}')

class SinkInfo():
    def __init__(self,mass,x,y,z,id,icr):
        self.m= mass[icr]
        self.loc= (x[icr],y[icr],z[icr])
        self.id= id[icr]
        
def PlotSinkSlice(dir,fsave,pf,center,radius,rBH,cs,DnFlr,sink,icr,Dist):
    '''dir -- string like "x","y","z" to slice in that direction
    sink -- class containing all sinks data
    icr -- index in sink class corresponding to nearest sink to high vs cell
    Dist -- array of distances between high vs cell and each sink'''
    print 'pf.h.sphere to calc density, velocity properties'
    sp = pf.h.sphere(center=center,radius=(2*radius,'cm'))#fields=['Vrms','density']) NOT RECOGNIZED!?
    vx=sp['X-momentum']/sp['density']
    vy=sp['Y-momentum']/sp['density']
    vz=sp['Z-momentum']/sp['density']
    vrms=(vx**2+vy**2+vz**2)**0.5
    machs=vrms/cs
    print 'make slice plot'
    slc = SlicePlot(pf,dir,"density", center=center,axes_unit="cm",width=(2*radius,2*radius))
    d={"s":100.,"c":"black","alpha":1}
    slc.annotate_marker(sink.loc,marker="o",plot_args=d)
    slc.annotate_velocity()
    #slc.annotate_grids(min_level=MaxLevel, max_level=MaxLevel)
    title="%s Slice, rBH= %.2g cm" % \
          (dir,rBH)
    slc.annotate_title(title)
    text="Dens Floor = %0.2g\nDens Min,Max,Mean = %0.2g, %0.2g, %0.2g\nMachS Max = %0.2g\nNearest Sink ID, Dist/rBH= %0.2g, %0.2g" % \
          (DnFlr,sp['density'].min(),sp['density'].max(),sp['density'].mean(), machs.max(), sink.id,Dist[icr]/rBH) 
    slc.annotate_text(pos=(0.02,0.88),text=text,text_args={'bbox':{'facecolor':'white', 'alpha':0.5, 'pad':10}})
    slc.save(name=fsave)
    #slc.zoom(4)
    #name= "SinkID_%d_Slice__%s_Zm4x_%s.png" % (sink.id,dir,pf.basename[5:9])
    #slc.save(name=spath+name)

spath="./plots/"
path="./"

#Nsinks = 64
#SidUse = 2
#MaxLevel=5
#rBH = 4.4e15
#cs = 2.4e4


#f_hdf5= glob.glob(path+"data.004*.3d.hdf5")
#f_sink= glob.glob(path+"data.004*.3d.sink")
f_hdf5= "data.0045.3d.hdf5"
f_sink= "data.0045.3d.sink"
xyz=np.zeros((Nsinks,3))
pf = load(f_hdf5)
print 'reading sink file'
mass,x,y,z,sid= np.loadtxt(f_sink,dtype='float',\
					 skiprows=1,usecols=(0,1,2,3,10),unpack=True)
center= (4.51e17,2.23e17,1.76e17)
Dist=np.sqrt((x-center[0])**2+(y-center[1])**2+(z-center[2])**2)
icr= np.where(Dist == Dist.min())[0][0] 
sink=SinkInfo(mass,x,y,z,sid,icr) #sink now contains info for nearest sink to high vs cell
#slice of width 2*1.2*nearest sink distance
#x,z slice for each
print 'doing slice of width 2*1.2*nearest sink distance centered on crazy cell'
radius=1.2*Dist.min()
dir="x"
fsave=spath+ "HighVs_Width_1.2DistMin_Slice_%s_%s.png" % (dir,pf.basename[5:9])
PlotSinkSlice(dir,fsave,pf,center,radius,rBH,cs,DnFlr,sink,icr,Dist)
dir='z' 
fsave=spath+ "HighVs_Width_1.2DistMin_Slice_%s_%s.png" % (dir,pf.basename[5:9])
PlotSinkSlice(dir,fsave,pf,center,radius,rBH,cs,DnFlr,sink,icr,Dist)
print 'doing slice of width 2*rBH centered on crazy cell'
radius=rBH
dir="x"
fsave=spath+ "HighVs_Width_2rBH_Slice_%s_%s.png" % (dir,pf.basename[5:9])
PlotSinkSlice(dir,fsave,pf,center,radius,rBH,cs,DnFlr,sink,icr,Dist) 
dir="z"
fsave=spath+ "HighVs_Width_2rBH_Slice_%s_%s.png" % (dir,pf.basename[5:9])
PlotSinkSlice(dir,fsave,pf,center,radius,rBH,cs,DnFlr,sink,icr,Dist)
print 'slice for nearst sink to high vs cell'
radius=rBH
dir="x"
fsave=spath+ "HighVsNearestSink_Width_2rBH_Slice_%s_%s.png" % (dir,pf.basename[5:9])
PlotSinkSlice(dir,fsave,pf,sink.loc,radius,rBH,cs,DnFlr,sink,icr,Dist) 
dir="z"
fsave=spath+ "HighVsNearestSink_Width_2rBH_Slice_%s_%s.png" % (dir,pf.basename[5:9])
PlotSinkSlice(dir,fsave,pf,sink.loc,radius,rBH,cs,DnFlr,sink,icr,Dist) 
