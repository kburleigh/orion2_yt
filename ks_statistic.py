import matplotlib
matplotlib.use('Agg')
import argparse
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import scipy.io as io
from scipy.optimize import curve_fit
import subprocess

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-dirs",nargs=3,action="store",help='directories to each 64 sinks data and SAMs 256^3 cell models')
parser.add_argument("-betas",nargs=3,action="store",help='beta for each directory')
parser.add_argument("-sams",action="store",help='set to anything to plot Semi-Analytic Model CDFs in addition to that of Simulation')
args = parser.parse_args()

def add_cdfs(ax, cdf,bins):
    #colors=['k','b','r','g','y','m']*2
    #lines=['-']*(len(colors)/2)+['--']*(len(colors)/2)
    #assert(len(colors) >= len(cdf.keys()))
    #labs= dict(data='Simulation') #,w='Vorticity',fit='Magnetic',fit_w='Magn.+Vort.')
    color=dict(data='k',fit='b',bh='r') #,w='g',fit='r',fit_w='b')
    #for key,color,ls in zip(cdf.keys(),colors,lines):
    for key in cdf.keys():
        #if key in ['bh','turb','fit_w_bh']: continue
        print "key= ",key
        if key == 'data':
            mybin=np.exp(bins[key])
            centers=(mybin[:-1]+mybin[1:])/2
            left=mybin[:-1]
            right=mybin[1:]
            if key == 'data' or key == 'fit': 
                ax.step(centers, cdf[key], where='mid',color=color[key],lw=3,label=key)
        else:
            for n in range(cdf[key].shape[0]):
                mybin=np.exp(bins[key][n,:])
                centers=(mybin[:-1]+mybin[1:])/2
                left=mybin[:-1]
                right=mybin[1:]
                ax.step(centers, cdf[key][n,:], where='mid',lw=1,color=color[key])
        #else: 
        #	ax.plot(centers,cdf[key],color=c_key[key],ls=ls,lw=2,label=labs[key])


#define dict to hold all data
cdf,bins={},{}
for bet in args.betas: 
	cdf[bet]={}
	bins[bet]={}
#fill cdf,bin dicts
for cnt,path,beta in zip(range(len(args.dirs)),args.dirs,args.betas):
	#array of size 64 for 64 steady mdots
	fin=open(os.path.join(path,'beta_'+beta+'_indivSteadyMdots.pickle'),'r') # 'data_and_phis.pickle'),'r')
	data=pickle.load(fin)
	fin.close()
	print 'computing: Data CDF'
	(cdf[beta]['data'],bins[beta]['data'],junk)=plt.hist(np.log(data),bins=20,normed=True,cumulative=True)
	#256^3 array of mdots
	if args.sams:
		print 'reading in mdot each cell pickle file'
		fin=open(os.path.join(path,'10_cdfs_64_synthetic_stars.pickle'),'r')  #os.path.join(path,'cellMdot.pickle'),'r')
		(mdot)=pickle.load(fin)
		fin.close()
		print 'computing: Semi-Analytic CDFs'
		nbins=20
		for key in mdot.keys(): 
			cdf[beta][key]= np.zeros((mdot[key].shape[0],nbins))-1
			bins[beta][key]= np.zeros((mdot[key].shape[0],nbins+1))-1
			for n in range(mdot[key].shape[0]):
				(cdf[beta][key][n,:],bins[beta][key][n,:],junk)=plt.hist(np.log(mdot[key][n,:]),bins=nbins,normed=True,cumulative=True)
plt.close()
#now save to plot since all calls to plt.hist finished 
kwargs=dict(ylabargs=dict(fontsize=26,fontweight='bold'),\
			xlabargs=dict(fontsize=20,fontweight='bold'),\
			tickargs=dict(fontsize=24),\
			legargs=dict(fontsize=16))
matplotlib.rcParams['xtick.labelsize'] = kwargs['tickargs']['fontsize']
matplotlib.rcParams['ytick.labelsize'] = kwargs['tickargs']['fontsize']

fig,axes=plt.subplots(1,3,figsize=(24,8.5))
ax=axes.flatten()
plt.subplots_adjust(wspace=0 )
for cnt,beta in zip(range(len(args.betas)),args.betas):
	add_cdfs(ax[cnt],cdf[beta],bins[beta])

for i in range(len(args.betas)):
	ax[i].set_xlim(1e-5,1e-1) 
	ax[i].set_ylim(0,1) 
	ax[i].set_xlabel(r'$\mathbf{\dot{M}/\dot{M}_{B}}$',**kwargs['xlabargs'])
	ax[i].set_xscale('log')
	ax[i].set_title(r'$\mathbf{\beta = }$ %s' % args.betas[i],**kwargs['ylabargs'])
	if i > 0: ax[i].set_yticklabels([])
	if i < 2: ax[i].set_xticks(np.logspace(-5,-2,4))
ax[0].set_ylabel('Cumulative Probability Distribution',**kwargs['ylabargs'])
ax[2].legend(loc=4,**kwargs['legargs'])
plt.savefig(os.path.join('./','cdf.png'))
plt.close()
