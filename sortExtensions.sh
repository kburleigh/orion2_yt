#!/bin/bash
#how to use this file
#run it
#carefully go through junk4, mv files that don't make sense to a "junk" folder then delete their associated line in junk4d
#re-run and see if junk5 makes sense
#if so, uncomment "cat" lines at end and re-run a final time

#delete file name if it exists
for fname in junk junk2 junk3 junk4 junk5 junk6
do
if [ `ls|grep $fname -c` -ne 0 ]; then
    rm $fname
fi 
done
#output file extension to file
for i in `ls`
do
echo ${i##*.} >> junk 
done
#write sortted file extensions to file (since job #'s are time sequential)
cat junk|grep 'o[1-9]'|sort -n > junk2
#given sorted order, write the full file names to file
for i in `cat junk2`
do  
ls|grep $i >> junk3
done
#for each filename, write its first and last coarse time step number to file
for fname in `cat junk3`
do
first=`grep $fname -e "coarse time step"|head -n 1|cut -d " " -f 4`
last=`grep $fname -e "coarse time step"|tail -n 1|cut -d " " -f 4`
echo file,1stTstep,LastTstep $fname $first $last >> junk4
done


#BELOW NOT WORKING YET!!!!!!!  just prints to junk5 currently
#in future, could use junk3 to instead make all time sequential filenames have sequential coarse time step numbers, ie concatenate that list and give to Sim_SinkData_EveryStep.py
let count=0
for fname in `cat junk3`
do
if [ $count -eq 0 ]; then
  prev=$fname  
else
  echo prev=${prev} curr=${fname} >> junk5
  tline=`grep $fname -e "coarse time step"|tail -n 1|cut -d " " -f 4`
  echo tline=$tline >> junk5
  let tstep=$tline-1
  echo tstep=$tstep >> junk5
  lastline=`grep $prev -e "coarse time step $tstep" -n|cut -d ":" -f 1`
  echo lastline=$lastline >> junk5
  #head $prev -n $lastline > ${prev}_cat
  prev=$fname
fi
let count=count+1 
done

for out in `ls out*|sort -V`
do 
first=`grep $out -e "coarse time step"|head -n 1|cut -d " " -f 4`
last=`grep $out -e "coarse time step"|tail -n 1|cut -d " " -f 4`
echo file,1stTstep,LastTstep $out $first $last >> junk6
done

#concatenate the final files
#fout="out_concat"
#echo concatenated > $fout
#echo "concatenating files in this order:"
kstring='cat'
for out in `cat junk6|cut -d " " -f 2`
do
kstring="$kstring $out"
#cat $fout $out >> $fout
done
kstring="$kstring >> out_concat"
echo "this is the command:" $kstring
