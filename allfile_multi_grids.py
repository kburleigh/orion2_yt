import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("patt",type=np.str_,help='file pattern look for. Ex: data.00*.hdf5')
parser.add_argument("-n",action="store",default="",help='add string to default name (default: %(default)s)')
parser.add_argument("-p",action="store",default="./",help='use this path instead of default (default: %(default)s)')
parser.add_argument("-sp",action="store",default='/clusterfs/henyey/kaylanb/downloads/',help='save to this path instead of default (default: %(default)s)')
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import AxesGrid

######
def onefile_multi_plot(file,args):
	print "READING from file,path: %s, %s" % (file,args.p)
	pf= load(args.p+file)
	#plot
	fig = plt.figure()
	grid = AxesGrid(fig, (0.075,0.075,0.85,0.85),
			  nrows_ncols = (1,2),
			  axes_pad =1.5,
			  label_mode = "all",
			  share_all = False,
			  cbar_location="right",
			  cbar_mode="each",
			  cbar_size="3%",
			  cbar_pad="0%")
	#1
	p = ProjectionPlot(pf, 'x', "density", center=pf.domain_center,axes_unit="cm")
	p.annotate_grids(min_level=0, max_level=5)
	p.set_width(pf.domain_width[0],pf.domain_width[0])  #[0] = [1] = [2]
	p.annotate_title('Grids All levels')
	plot = p.plots[ "density" ]
	plot.figure = fig
	plot.axes = grid[0].axes
	plot.cax = grid.cbar_axes[0]
	p._setup_plots()

	#2 
	p = ProjectionPlot(pf, 'x', "density", center=pf.domain_center,axes_unit="cm")
	p.annotate_grids(min_level=5, max_level=5)
	p.set_width(pf.domain_width[0],pf.domain_width[0])  #[0] = [1] = [2]
	p.annotate_title('level 5 grids')
	plot = p.plots[ "density" ]
	plot.figure = fig
	plot.axes = grid[1].axes
	plot.cax = grid.cbar_axes[1]
	p._setup_plots()

#	#3
#	p = ProjectionPlot(pf, 'x', "density", center=pf.domain_center,axes_unit="cm")
#	p.annotate_grids(min_level=3, max_level=3)
#	p.set_width(pf.domain_width[0],pf.domain_width[0])  #[0] = [1] = [2]
#	p.annotate_title('level 3 grids')
#	plot = p.plots[ "density" ]
#	plot.figure = fig
#	plot.axes = grid[2].axes
#	plot.cax = grid.cbar_axes[2]
#	p._setup_plots()
#
#	#4
#	p = ProjectionPlot(pf, 'x', "density", center=pf.domain_center,axes_unit="cm")
#	p.annotate_grids(min_level=5, max_level=5)
#	p.set_width(pf.domain_width[0],pf.domain_width[0])  #[0] = [1] = [2]
#	p.annotate_title('level 5 grids')
#	plot = p.plots[ "density" ]
#	plot.figure = fig
#	plot.axes = grid[3].axes
#	plot.cax = grid.cbar_axes[3]
#	p._setup_plots()

	#output
	
	name = args.n + "grids_multi_%s.pdf" % file[5:9]
	spath = args.sp + name
	plt.savefig(spath)
#####
files = glob.glob1(args.p, args.patt)
for file in files:
	onefile_multi_plot(file,args)

