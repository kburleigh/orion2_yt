'''
dependecies: 
    Sim_SinkData_EveryTStep.py -- creates the "mdots*.pickle" file 
input:
    "mdots*.pickle" file containing sink particle time,mass,mdot on finest-level
    general simulation info
output:
    plot of mdot vs. time on finest-level or sampled with user-defined frequency (eg 0.2*bondi time)
'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-pickles",nargs=4,action="store",help='give relative paths to sinks.pickle files')
parser.add_argument("-names",nargs=4,action="store",help='name for pickle_a and pickle_b data', required=True)
parser.add_argument("-nsinks",type=int,action="store",help='number of sinks')
parser.add_argument("-cs",type=float,action="store",help='bondi hoyle time at instant insert sinks in simulation units')
parser.add_argument("-beta",choices= ['Infinity','100','10','1','0.1','0.01'],action="store",help='initialized beta, ie when begin driving')
#optional
parser.add_argument("-indivSinks",nargs=2,type=float,action="store",help="set to plot individual sink mdots, first number is lower time and 2nd number is upper time both in units of tBHA, each sink's mdot will be averaged over this time range, ranked highest to lowest, and plotted by this grouping for easier viewing", required=False)
parser.add_argument("-steadyDist",nargs=3,type=float,action="store",help="(how many intervals, what t/tBHA to tstart at, width/tBHA of interval) -- average each sink's mdot over each interval and plot distribution of mdots for all sinks average in this way",required=False)
parser.add_argument("-fname",action="store",help='prefix for mdot_sinkFiles.png', required=False)
parser.add_argument("-xlim",nargs=2,metavar=('low','hi'),type=float,action="store",help='mdot vs. time, time limits',required=False)
parser.add_argument("-ylim",nargs=2,metavar=('low','hi'),type=float,action="store",help='mdot vs. time, mdot normalized limits',required=False)
parser.add_argument("-freq_sample",type=float,action="store",help='mdot sampled on finest level data if not given, if given: sample time every freq_sample*tB, ex) freq_sample = 0.2 to calc mdot over 0.2tB time intervals',required=False)
parser.add_argument("-boxcar",type=float,action="store",help='use boxcar averaging, with width= args.boxcar in units of tBHA, ex) 0.02tB is in Cunningham 2012',required=False)
parser.add_argument("-Geq1",action="store",help='set this to anything, say 1 if units of G = 1', required=False)
parser.add_argument("-LinearY",action="store",help='set to 1 to plot Linear Y axis', required=False)
#get args
args = parser.parse_args()

import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pickle

import Sim_constants as sc
print "imported all modules"
if args.Geq1: bigG= 1.
else: bigG= sc.G


def mdot_bh(rho,msink,rmsV,args):
    return 4*np.pi*rho*np.power(bigG*msink,2)/np.power( np.power(rmsV,2)+args.cs**2, 1.5)

def lee_mach_bh(rmsV,args):
	lam=np.exp(1.5)/4
	return np.power( 1.+np.power(rmsV/args.cs,4), 1./3)/np.power( 1.+np.power(rmsV/args.cs/lam,2), 1./6)

def mdot_lee_const(sink,args):
	#Lee 2014 eqn 41, since table 1 par ~ perp and par > perp for large |B|
	bh= mdot_bh(sink.glob_meanRho[0],sink.mass[0,0],sink.glob_rmsV[0],args)
	mach_bh= lee_mach_bh(sink.glob_rmsV[0],args)
	inv_beta= sink.glob_rmsVa[0]**2/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_lee_ft(sink,args):
	bh= mdot_bh(sink.glob_meanRho,sink.mass[0,0],sink.glob_rmsV,args)
	mach_bh= lee_mach_bh(sink.glob_rmsV,args)
	inv_beta= sink.glob_rmsVa**2/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)
	
def mdot_lee_ft_environ(sink,args):
	bh= mdot_bh(sink.meanRho,sink.mass,sink.rmsV,args)
	mach_bh= lee_mach_bh(sink.rmsV,args)
	inv_beta= np.power(sink.rmsVa,2)/(2.*args.cs**2)  
	return bh*np.power( 1.+ 4.4*np.power(mach_bh,-1)*np.power(inv_beta,0.5), -1)

def mdot_0(rho,msink,rmsV):
	return 4*np.pi*rho*(msink)**2/(rmsV)**3

class Mdots():
    def __init__(self,sink,args):
		self.lee_const= mdot_lee_const(sink,args)
		self.lee_ft= mdot_lee_ft(sink,args)
		self.lee_ft_environ= mdot_lee_ft_environ(sink,args)
		self.krum06= mdot_0(args.rho,sink.mass[0,0],args.rmsV)

#define this class name so recognized when read in pickled data
class SinkData():
    pass 

def resampleMdot(sink,t_sample):
    '''sample finest level Mdot by t_sample'''
    sample= sink.time/t_sample
    imax=int(sample.max())
    imin=int(sample.min())
    ind=[]
    #if only have mdot info for some initial time > 0, must start from i = imin not i= 0 
    for i in (np.arange(imin,imax)+1):
        ind.append(np.where(sample < float(i))[0][-1])
    ind=np.array(ind)
    tbin= sink.time[ind]
    mbin= sink.mass[ind,:]
    mdotFine= np.zeros(mbin.shape)-1
    tFine= np.zeros(tbin.shape)-1
    newMdot[0,:]=0.
    tFine[0]= tbin[0]
    for cnt in np.arange(1,mbin.shape[0]):
        deltaM= mbin[cnt,:] -mbin[cnt-1,:] 
        deltaT= tbin[cnt]- tbin[cnt-1]
        mdotFine[cnt,:]= deltaM/deltaT
        tFine[cnt]= tbin[cnt-1]+deltaT/2.
    return (tFine,mdotFine)

def isMonotonic(t):
    '''t is array-like, raise error if t is not a monotoonly increasing'''
    ibad=np.where(t[1:]-t[:-1] <= 0.)[0]
    if ibad.size > 0: raise ValueError
    else: pass

def Boxcar(t,y,wid,units_tBHA=True):
	'''t,y are array-like. t is dependent variable, y is independent var we are taking boxcar average of.
	wid is width of boxcar average
	returns: tuplee of new boxcar averaged t,y '''
	print "computing a BoxCar average for Mdot info"
	isMonotonic(t)
	if not units_tBHA: #false (so not evaluated) unless units_tBHA set to False
		raise ValueError
	new_mdot=[]
	new_t= []
	low=t.min()
	hi=t.min()+wid
	cnt= 1
	while (low < t.max()):
		imin=np.where(t >= low)[0]
		imax=np.where(t < hi)[0]
		if len(imin) == 0 or len(imax) == 0: #did not find value between low and hi
			cnt+=1
			hi= t.min()+cnt*wid  #increase hi so can find value between original low and new hi
			if hi > t.max():  #no data in [end-small,end), okay if "small" is indeed small 
				break
		else: #found values continue as normal
			imin=imin.min()
			imax=imax.max()
			ind=np.arange(imin,imax+1,1)
			new_mdot.append( y[ind].mean() )
			new_t.append( (t[imin]+t[imax])/2 )
			cnt+=1
			low=hi
			hi= t.min()+cnt*wid
			if (low + wid >= t.max()): hi= t.max()
	return (np.array(new_t),np.array(new_mdot))


def plot_Mdots_lee14(simul,tBHA,mdot,args):
    fig,ax= plt.subplots() #1,2) #,figsize=(10,3))
    #ax= axis.flatten()
    time= simul.time/tBHA  
    lab=""
    if args.avgOver: lab +="mean= %.2f (%.1f-%.1f tB)" % (simul.avg/mdot.norm/args.answer,args.avgOver[0],args.avgOver[1])
    if args.boxcar: lab += " (boxcar %.1g tB)" % args.boxcar
    ax.plot(time,simul.mdot/mdot.norm/args.answer,"k-",label=lab)
    #can plot average mdot if > 1 sink particle 
    #ax.plot(time,np.average(simul.mdot,axis=1)/mdot.norm,"k-",label='K Mean')
    #ax.plot(time,np.median(simul.mdot,axis=1)/mdot.norm,"k--",label='K Median')
    ax.set_xlabel("t / %s" % args.tBHA)
    ylab= r"$\mathbf{\dot{M}}$"+" /%s/$\dot{M}_{Ramses}$" % args.mdotBHA
    #below doesn't work, not sure why
    if args.freq_sample: ylab += " (Sampled every %.1f tB)" % args.freq_sample
    ax.set_ylabel(ylab)
    ax.set_yscale('log')
    ax.legend(loc=4)
    if args.ylim: ax.set_ylim(args.ylim[0],args.ylim[1])
    if args.xlim: ax.set_xlim(args.xlim[0],args.xlim[1])
    if args.LinearY: ax.set_yscale('linear')
    #leg_box=ax.legend(loc=(1.01,0.01),ncol=1)
    if args.fname: fsave= "lee14_mdot_"+args.fname+".png"
    else: fsave="lee14_mdot.png"
    plt.savefig(fsave) #bbox_extra_artists=(leg_box,), bbox_inches='tight')
    plt.close()

def plot_krum06_avgMdot(sink,mdot,tBHA,args):
	#prep plot
	f,axis=plt.subplots(2,2,sharex=True,figsize=(15,10))
	plt.subplots_adjust( hspace=0,wspace=0 )
	ax=axis.flatten()
	#calc mean and median Mdot
	avg= np.average(sink.MdotFine/mdot.lee_const,axis=1)
	med= np.median(sink.MdotFine/mdot.lee_const,axis=1)
	avg_ft= np.average(sink.MdotFine,axis=1)/mdot.lee_ft #mdot.lee_ft has len = n time points
	avg_ft_environ= np.average(sink.MdotFine/mdot.lee_ft_environ,axis=1)
	med_ft= np.median(sink.MdotFine,axis=1)/mdot.lee_ft #mdot.lee_ft has len = n time points
	med_ft_environ= np.median(sink.MdotFine/mdot.lee_ft_environ,axis=1)
	time= (sink.time-sink.time[0])/tBHA
	if args.boxcar:
		#'time' returned for each below are all the same b/c input sink.time are the same 
		(time,avg)= Boxcar((sink.time-sink.time[0])/tBHA,avg,args.boxcar*tBHA,units_tBHA=True)
		(time,med)= Boxcar((sink.time-sink.time[0])/tBHA,med,args.boxcar*tBHA,units_tBHA=True)
		(time,avg_ft)= Boxcar((sink.time-sink.time[0])/tBHA,avg_ft,args.boxcar*tBHA,units_tBHA=True)
		(time,avg_ft_environ)= Boxcar((sink.time-sink.time[0])/tBHA,avg_ft_environ,args.boxcar*tBHA,units_tBHA=True)
		(time,med_ft)= Boxcar((sink.time-sink.time[0])/tBHA,med_ft,args.boxcar*tBHA,units_tBHA=True)
		(time,med_ft_environ)= Boxcar((sink.time-sink.time[0])/tBHA,med_ft_environ,args.boxcar*tBHA,units_tBHA=True)   
	#MEDIAN - right panels
	ax[0].plot(time,med,'k-',lw=2)    
	ax[0].plot(time,med_ft,'b-',lw=2)    
	ax[0].plot(time,med_ft_environ,'g-',lw=2)  
	ax[2].plot(time,med,'k-',lw=2)  
	ax[2].plot(time,med_ft,'b-',lw=2)
	ax[2].plot(time,med_ft_environ,'g-',lw=2)    
	#MEAN - left panels
	ax[1].plot(time,avg,'k-',lw=2)  
	ax[1].plot(time,avg_ft,'b-',lw=2)    
	ax[1].plot(time,avg_ft_environ,'g-',lw=2)    
	ax[3].plot(time,avg,'k-',lw=2,label=r"$\dot{M}=$ const")    
	ax[3].plot(time,avg_ft,'b-',lw=2,label=r"$\dot{M}=$ f(t)")    
	ax[3].plot(time,avg_ft_environ,'g-',lw=2,label=r"$\dot{M}=$ f(t,sink)") 
	#annotate plot
	ax[0].set_title("Median")
	ax[1].set_title("Mean")
	ax[0].set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_0 }$')
	ax[2].set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_0 }$')
	ax[2].set_xlabel(r"$\mathbf{ t/t_{BHA} }$")
	ax[3].set_xlabel(r"$\mathbf{ t/t_{BHA} }$")
	if args.boxcar: plt.subtitle("boxcar %.2g tBHA" % args.boxcar)
	ax[3].legend(loc=4,fontsize='medium')
	#scaling
	for i in [0,1]: ax[i].set_yscale('log')
	for i in [2,3]: ax[i].set_yscale('linear')
	for a in ax:
		if args.ylim: a.set_ylim(args.ylim[0],args.ylim[1])
		if args.xlim: a.set_xlim(args.xlim[0],args.xlim[1])
	if args.fname: fsave= "median_mean_mdot_"+args.fname+".png"
	else: fsave="median_mean_mdot.png"
	plt.savefig(fsave,dpi=150)
	plt.close() 

def plot_local_rmsV(sid_indices,fname, sink,mdot,tBHA,args):
	#set up plot
	fig = plt.figure(figsize=(15,10))
	fig.subplots_adjust(hspace=0,wspace=0) 
	ax1 = plt.subplot2grid((3,2), (0,0), colspan=1)
	ax2 = plt.subplot2grid((3,2), (1,0), colspan=1,sharex=ax1)
	ax3 = plt.subplot2grid((3,2), (2,0), colspan=1,sharex=ax1)
	ax4 = plt.subplot2grid((3,2), (0, 1), rowspan=2)	
	#
	colors=['k','b','g','r','c','m','y'] +['k']
	lw= 7*[2.]+[2.]
	#plot rmsV,rmsVa,meanRho on Left Vertical panels (ax1-ax3)
	#indiv sink mdots on Right Vertical Panels (ax4)
	time= (sink.time-sink.time[0])/tBHA
	for cnt,i in enumerate(sid_indices):
		ax1.plot(time,sink.meanRho[:,i],ls='-',c=colors[cnt],lw=lw[cnt])
		ax2.plot(time,sink.rmsV[:,i],ls='-',c=colors[cnt],lw=lw[cnt])
		ax3.plot(time,sink.rmsVa[:,i],ls='-',c=colors[cnt],lw=lw[cnt],label=str(sink.sid[:,i][0]))
		ax4.plot(time,sink.MdotFine[:,i]/mdot.lee_const,ls='-',c=colors[cnt],lw=lw[cnt])
		ax4.plot(time,sink.MdotFine[:,i]/mdot.lee_ft,ls='--',c=colors[cnt],lw=lw[cnt])
	#plot global averaged values
	ax1.plot(time,sink.glob_meanRho,'k-',lw=3.)
	ax2.plot(time,sink.glob_rmsV,'k-',lw=3.)
	ax3.plot(time,sink.glob_rmsVa,'k-',lw=3.,label="Full Domain")
	#finish plot
	ax1.set_ylabel(r"$\mathbf{ \bar{\rho} }$",fontweight='bold',fontsize='x-large')
	ax2.set_ylabel(r"$\mathbf{ V_{RMS} }$",fontweight='bold',fontsize='x-large')
	ax3.set_ylabel(r"$\mathbf{ V_{A, RMS} }$",fontweight='bold',fontsize='x-large')
	ax4.set_ylabel(r'$\mathbf{ \dot{M}/\dot{M}_0 }$',fontweight='bold',fontsize='x-large')
	ax4.set_xlabel(r'$\mathbf{ t/t_{\rm{BHA}} }$',fontweight='bold',fontsize='x-large')
	ax4.set_yscale('log')
	ax4.yaxis.set_label_position("right")
	ax4.yaxis.tick_right()
	ax3.legend(loc=(1.01,0),fontsize='medium',ncol=2)
	plt.savefig('local_rmsV_'+fname+'.png')
	plt.close()

def sink_ids_by_last_tBHA_mdot(sink,tBHA,args):
	'''returns numpy array of sink ids, sorted by highest to lowest mdot time averaged over the last tBHA of time data'''
	imin= np.where(sink.time/tBHA >= sink.time[-1]/tBHA -1.)[0]
	if len(imin) == 0: raise ValueError
	imin= imin[0]	
	mdots= np.average(sink.MdotFine[imin:,:],axis=0)
	return np.argsort(mdots)[::-1] #indices, highest to lowest


def add_sink_to_3x2_converg(ax,color,label, sink,mdot,tBHA,args):
	time= (sink.time-sink.time[0])/tBHA
	#MEDIAN - left panels
	ax[0].plot(time,np.median(sink.MdotFine/mdot.lee_const,axis=1),c=color,ls='-',lw=2.)
	ax[2].plot(time,np.median(sink.MdotFine,axis=1)/mdot.lee_ft,c=color,ls='-',lw=2.)
	ax[3].plot(time,np.median(sink.MdotFine/mdot.lee_ft_environ,axis=1),c=color,ls='-',lw=2.)
	#MEAN - right panels	
	ax[1].plot(time,np.mean(sink.MdotFine/mdot.lee_const,axis=1),c=color,ls='-',lw=2.)
	ax[3].plot(time,np.mean(sink.MdotFine,axis=1)/mdot.lee_ft,c=color,ls='-',lw=2.)
	ax[5].plot(time,np.mean(sink.MdotFine/mdot.lee_ft_environ,axis=1),c=color,ls='-',lw=2.,label=label)	

def plot_rmsM_v_time(sink,tBHA,args):
    norm= (sink.time.copy() -sink.time[0])/tBHA
    plt.plot(norm,sink.rmsM,"k-",label="rms Mach")
    plt.xlabel("$\mathbf{ t/t_{bh} }$")
    dylab= r"rms Mach number"
    plt.ylabel(ylab)
    plt.yscale('log')
    if args.ylim: plt.ylim(args.ylim[0],args.ylim[1])
    if args.xlim: plt.xlim(args.xlim[0],args.xlim[1])
    if args.LinearY: plt.yscale('linear')
    plt.legend(loc=2,fontsize='small')
    #save it
    if args.fname: fsave= "krum06_rmsMach.png_"+args.fname+".png"
    else: fsave="krum06_rmsMach.png"
    plt.savefig(fsave)
    plt.close() 


def plot_krum06_ExtremaMdot(simul,tBHA,mdot,args):
    #get average, median Mdot and do box car average if required
    fig,axis=plt.subplots(1,2,figsize=(20,5))
    ax=axis.flatten()
    maxMdot= np.max(simul.mdot,axis=1)/mdot.norm/args.answer
    minMdot= np.min(simul.mdot,axis=1)/mdot.norm/args.answer
    if args.boxcar: 
        (tMax,maxMdot)= Boxcar(simul.time,maxMdot,args.boxcar*tBHA)
        (tMin,minMdot)= Boxcar(simul.time,minMdot,args.boxcar*tBHA)
    else: (tMax,tMin)= (simul.time,simul.time)
    (tMax,tMin)= (tMax/tBHA,tMin/tBHA)
    #plot
    ax[0].plot(tMax,maxMdot,"k-")
    ax[0].set_title("MAX Mdot")
    ax[1].plot(tMin,minMdot,"k-")
    ax[1].set_title("MIN Mdot")
    plt.xlabel("$\mathbf{ t/t_{bh} }$")
    ylab= r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$"
    for myax in ax:
        if args.freq_sample: ylab += " (Sampled every %.1f tBH)" % args.freq_sample
        if args.boxcar: ylab += " (boxcar %.2g tBH)" % args.boxcar
        if args.vrms_vs_time: ylab += " vrms=f(t) corrected"
        myax.set_ylabel(ylab)
        myax.set_yscale('log')
        if args.ylim: myax.set_ylim(args.ylim[0],args.ylim[1])
        if args.xlim: myax.set_xlim(args.xlim[0],args.xlim[1])
        if args.LinearY: myax.set_yscale('linear')
        #leg_box=ax.legend(loc=(1.01,0.01),ncol=1)
        #     bbox=dict(facecolor='red', alpha=0.5)
        #plt.legend(loc=4,fontsize='small')
    #save it
    if args.fname: fsave= "krum06_ExtremaMdot_"+args.fname+".png"
    else: fsave="krum06_ExtremaMdot.png"
    plt.savefig(fsave)
    plt.close() 

  
def sort_SIDs_byTavgMdot(low,hi,simul,tBHA):
	'''returns numpy array of sink ids, sorted highest to lowest time averaged Mdot over low to hi range'''
	normed= simul.time.copy()/tBHA
	if (low > normed.max()) or (hi > normed.max()): raise ValueError
	imin=np.where(normed > low)[0].min()
	imax=np.where(normed < hi)[0].max()
	ind=np.arange(imin,imax+1,1)
	#average each sink's mdot over the interval
	if args.vrms_vs_time:  #mdot.norm[ind] has shape (size,1) but simul.mdot[ind,:] has shape (size,nsinks)
		tavg_mdot=simul.mdot[ind,:].copy()
		for sid in range(args.nsinks):
			tavg_mdot[:,sid]=tavg_mdot[:,sid]/ mdot.norm[ind]/args.answer
		tavg_mdot= np.mean(tavg_mdot,axis=0) #so shape is nsinks
	else:
		tavg_mdot= np.mean(simul.mdot[ind,:]/mdot.norm/args.answer,axis=0) #shape is nsinks
	isort=np.argsort(tavg_mdot) #indices that will sort tavg_mdot lowest to highest
	return isort[::-1] #highest to lowest

def plot_sids(sids, simul,mdot,tBHA,args,colors,lines,fsave):
    for i,sid in enumerate(sids):
        if args.boxcar:
            (boxTime,boxMdot)= Boxcar(simul.time,simul.mdot[:,sid]/mdot.norm/args.answer,args.boxcar*tBHA)
            plt.plot(boxTime/tBHA,boxMdot,c=colors[i],ls=lines[i],label=str(sid))
        else:
            plt.plot(simul.time/tBHA,simul.mdot[:,sid]/mdot.norm/args.answer,c=colors[i],ls=lines[i],label="%d" % sid)
    plt.xlabel("$\mathbf{ t/t_{bh} }$")
    ylab= r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$"
    if args.freq_sample: ylab += " (Sampled every %.1f tBH)" % args.freq_sample
    if args.boxcar: ylab += " (boxcar %.2g tBH)" % args.boxcar
    if args.vrms_vs_time: ylab += " vrms=f(t) corrected"
    plt.ylabel(ylab)
    plt.yscale('log')
    #if args.ylim: plt.ylim(args.ylim[0],args.ylim[1])
    #if args.xlim: plt.xlim(args.xlim[0],args.xlim[1])
    if args.LinearY: plt.yscale('linear')
    plt.legend(loc=(1.01,0.01),ncol=2,fontsize='xx-small')
    plt.tight_layout(rect=(0,0,0.85,1))
    plt.savefig(fsave) 
    plt.close()
 
def plot_indivSinks_byMdotTavg(Tlow,Thi, simul,mdot,tBHA,args):
	#prep plot
	sinks_per_plot=8
	colors=['k','b','g','r','c','m','y'] +['k']
	lines=7*['-']+['--']
	if args.nsinks % sinks_per_plot != 0: raise ValueError
	#get sink ID numbers in highest to lower time averaged mdot order
	isort= sort_SIDs_byTavgMdot(Tlow,Thi,simul,tBHA)
	#pass every sinks_per_plot sink IDs to plot function
	for i in range( int(args.nsinks/sinks_per_plot) ): 
		sids= isort[i*sinks_per_plot:(i+1)*sinks_per_plot]  #[0:8] is 8 sids to plot, next would be [8:16]
		fsave= "indivSinks_mdotRank_%s" % (i) #lower the "rank" higher the mdots in that group
		if args.fname: fsave=fsave+ "_"+args.fname
		plot_sids(sids, simul,mdot,tBHA,args,colors,lines,fsave+".png")


def plot_mdotdata_mdotsteadyline(simul,tnorm,mdot,args):
    '''1 plot per sink, so nsink plots
    this is a check that steady mdot calculation looks reasonable for each sink's mdot data'''
    time= simul.time/tBHA
    mdotBHA= simul.mdot.copy()/mdot.norm/args.answer
    for sid in range(8): #range(args.nsinks):
        lab=""
        if args.vrms_vs_time: lab= " vrms=f(t) corrected"
        plt.plot(time,mdotBHA,'k-',label='data'+lab)
        steady=simul.tavg_mdot[sid]/mdot.norm/args.answer
        plt.hlines(simul.tavg_mdot[sid]/mdot.norm/args.answer,time.min(),time.max(),\
            linestyles='dashed',colors='b',label="steady= %.2g" % steady)
        plt.xlabel("$\mathbf{ t/t_{bh} }$")
        ylab= r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$"
        ylab += r" (<$\mathbf{ \dot{M} }$> after t= %.2f tBH)" % args.tReachSteady
        plt.ylabel(ylab)
        plt.yscale('log')
    #     ax.legend(loc=4)
        if args.ylim: plt.ylim(args.ylim[0],args.ylim[1])
        if args.xlim: plt.xlim(args.xlim[0],args.xlim[1])
        if args.LinearY: plt.yscale('linear')
        plt.legend(loc=(1.01,0.01),ncol=2,fontsize='xx-small')
        fsave="mdot_%d.png" % sid
        plt.savefig(fsave) 
        plt.close()


def plot_testRange(simul,mdot,tBHA,stat_mean,stat_med,ind):
	normed= simul.time.copy()/tBHA
	avgMdot= np.average(simul.mdot,axis=1)/mdot.norm/args.answer
	medianMdot= np.median(simul.mdot,axis=1)/mdot.norm/args.answer
	plt.plot(normed,avgMdot,"k-",label="mean")
	plt.plot(normed,medianMdot,"b-",label="median")
	plt.vlines(normed[ind[0]],avgMdot.min(),avgMdot.max(),colors="r",linestyles='dashed')
	plt.vlines(normed[ind[-1]],avgMdot.min(),avgMdot.max(),colors="r",linestyles='dashed')
	plt.hlines(stat_mean,normed.min(),normed.max(),colors="m",linestyles='dashed')
	plt.hlines(stat_med,normed.min(),normed.max(),colors="m",linestyles='dashed')
	plt.xlabel('t/tBH')
	plt.ylabel('Mdot/Mdot0')
	fname='steadyDist_testRange_%.1f_%.1f.png' % (normed[ind[0]],normed[ind[-1]])
	plt.savefig(fname,dpi=150)
	plt.close()

def plot_steadyDist(lower,upper,simul,mdot,tBHA,args):
    normed= simul.time.copy()/tBHA
    if (lower > normed.max()) or (upper > normed.max()):
        print "args.steadyDist too large, max time have data for is t/tBHA = %.5f" % normed.max()
        raise ValueError
    imin=np.where(normed > lower)[0].min()
    imax=np.where(normed < upper)[0].max()
    ind=np.arange(imin,imax+1,1)
    print "in steadyDist: time/tBHA min=%f, max=%f" % (normed[ind[0]],normed[ind[-1]])
    #get average mdot for each sink over time interval
    if args.vrms_vs_time:  #mdot.norm[ind] has shape (size,1) but simul.mdot[ind,:] has shape (size,nsinks)
        mdot_norm=simul.mdot[ind,:].copy()
        for sid in range(args.nsinks):
            mdot_norm[:,sid]=mdot_norm[:,sid]/ mdot.norm[ind]/args.answer
        med_mdot= np.median(mdot_norm,axis=0)
        mean_mdot= np.mean(mdot_norm,axis=0)
    else:
        med_mdot= np.median(simul.mdot[ind,:]/mdot.norm/args.answer,axis=0)
        mean_mdot= np.mean(simul.mdot[ind,:]/mdot.norm/args.answer,axis=0)
    mean_of_med= np.mean(med_mdot)
    med_of_med= np.median(med_mdot)
    mean_of_mean= np.mean(mean_mdot)
    med_of_mean= np.median(mean_mdot)
    #test calc props correctly
    #plot_testRange(simul,mdot,tBHA,mean_of_med,med_of_med,ind)	
    #plot distribution
    f,axis=plt.subplots(2,2,figsize=(20,5)) #sharey=True
    plt.subplots_adjust( hspace=0.5,wspace=0.2 )
    ax=axis.flatten()
    #median mdot
    lab= "Median mdot over %.2f - %2.f tBH" % (normed[ind[0]],normed[ind[-1]])
    ax[0].set_title(lab)
    lab= "mean over sinks: %.2f \nmedian over sinks: %.2f" % (mean_of_med,med_of_med)
    ax[0].hist(med_mdot,bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[0].set_ylabel("dP/d M  (normalized to 1)")
    ax[0].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")   
    ax[0].legend(loc=1,ncol=1,fontsize='small') 
    ax[2].hist(np.log10(med_mdot),bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[2].set_ylabel("dP/d logM  (normalized to 1)")
    ax[2].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")  
    ax[2].set_xscale('log')
    #mean mdot
    lab= "Mean mdot over %.2f - %2.f tBH" % (normed[ind[0]],normed[ind[-1]])
    ax[1].set_title(lab)
    lab= "mean over sinks: %.2f \nmedian over sinks: %.2f" % (mean_of_mean,med_of_mean)
    ax[1].hist(mean_mdot,bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[1].set_ylabel("dP/d M  (normalized to 1)")
    ax[1].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")   
    ax[1].legend(loc=(0.,1.01),ncol=1,fontsize='small') 
    ax[3].hist(np.log10(mean_mdot),bins=20,normed=True,histtype='step',align='mid',color='b',label=lab)
    ax[3].set_ylabel("dP/d logM  (normalized to 1)")
    ax[3].set_xlabel(r"$\mathbf{ \dot{M} / \dot{M}_0 / \dot{M}_{ans}  }$")  
    ax[3].set_xscale('log')  
    ax[1].legend(loc=1,ncol=1,fontsize='small') 
    fname='steadyDist_%d_%d_%d_time_%.1f_%.1f.png' % \
        (int(args.steadyDist[0]),int(args.steadyDist[1]),int(args.steadyDist[2]),normed[ind[0]],normed[ind[-1]])
    plt.savefig(fname,dpi=150)
    plt.close()
    return (np.mean([lower,upper]),mean_of_med,med_of_med,mean_of_mean,med_of_mean)


#set up plot
fig,axis= plt.subplots(3,2,sharex=True)#,sharey=True)
fig.set_size_inches(18.5, 10.5)
ax=axis.flatten()
fig.subplots_adjust(hspace=0,wspace=0) 
colors=['k','b','g','r','c','m','y'] +['k']
#loop over all sink.pickle files
for i in range(len(args.pickles)):
	print "loading pickle data for %s" % args.pickles[i]
	fin=open(args.pickles[i],'r')
	sink=pickle.load(fin)
	fin.close()
	print "pickle data loaded"
	#process sink info
	#get tBHA and mdots to normalize by, const and func of time versions 
	mdot= Mdots(sink,args)
	#get tBHA using rmsV,rmsVa,msink when insert sinks
	tBHA = bigG*sink.mass[0,0]/(sink.glob_rmsVa[0]**2 +sink.glob_rmsV[0]**2+ args.cs**2)**(3./2)
	print "tBHA= ",tBHA
	#add sink info to plot
	add_sink_to_3x2_converg(ax,colors[i],args.names[i], sink,mdot,tBHA,args)
#finish plot
#x,y labels
ax[0].set_ylabel(r'$\mathbf{ \dot{M}/const }$',fontweight='bold',fontsize='xx-large')
ax[2].set_ylabel(r'$\mathbf{ \dot{M}/f(t) }$',fontweight='bold',fontsize='xx-large')
ax[4].set_ylabel(r'$\mathbf{ \dot{M}/f(t,sink) }$',fontweight='bold',fontsize='xx-large')
for i in [4,5]: ax[i].set_xlabel(r'$\mathbf{ t/t_{\rm{BHA}} }$',fontweight='bold',fontsize='xx-large')
#legends
ax[5].legend(loc=(1.01,0),fontsize='large',ncol=1)
#titles
ax[0].set_title('Median',fontweight='bold',fontsize='xx-large')
ax[1].set_title('Mean',fontweight='bold',fontsize='xx-large')
plt.suptitle('Beta = '+args.beta+", Convergence")
#scaling
for a in ax: a.set_yscale('log')
fname= 'convergence_3x2_log'
if args.fname: plt.savefig(fname+'_'+args.fname+'.png')
else: plt.savefig(fname)
plt.close()


