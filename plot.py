import numpy as np
import argparse
import os
from pickle import dump,load
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="test")
parser.add_argument("-data_file",action="store",help='path/to/beta_100.pickle',required=True)
args = parser.parse_args()

beta= os.path.basename(args.data_file).replace('.pickle','')

def all_rates(rate,time):
	'''plots accretion rate vs. time for each of 64 stars'''
	for i in range(rate.shape[0]):
		plt.plot(time,rate[i,:]) #ith star		
	plt.ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_B }$',fontweight='bold',fontsize='xx-large')
	plt.yscale('log')
	plt.xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',fontweight='bold',fontsize='xx-large')
	plt.title('Model: %s' % beta)
	plt.savefig('all_rates_%s.png' % beta)
	plt.close()

def mean_med_rates(rate,time): 
	#median and mean over the 64 stars
	plt.plot(time,np.median(rate,axis=0),'k-',label='median')
	plt.plot(time,np.mean(rate,axis=0),'b-',label='mean')		
	plt.legend(loc=4) 
	laba=dict(fontweight='bold',fontsize='xx-large')
	plt.ylabel(r'Log $\mathbf{ \dot{M}/\dot{M}_{\rm{B}} }$',**laba)
	plt.xlabel(r'$\mathbf{ t/t_{\rm{BH}} }$',**laba)
	plt.yscale('log')
	plt.title('Model: %s' % beta)
	plt.savefig('mean_med_rates_%s.png' % beta)
	plt.close()

def steady_rates(rate,time,avg_lo,avg_hi):
	'''returns 64 length array of time averaged accretion rates for each sink'''
	assert(rate.shape[0] == 64)
	#grab indices want to take time avg over
	imin=np.where(time >= avg_lo)[0].min()
	imax=np.where(time <= avg_hi)[0].max()
	ind=range(imin,imax+1)
	#our 'time avg' will be the 'median' over the time span
	return np.median(rate[:,ind],axis=1)


def pdf_mdots(rate,time,avg_lo,avg_hi):
	tavg_mdots= steady_rates(rate,time,avg_lo,avg_hi) #array of size 64, one time averaged accretion rate per star
	y=np.log10(tavg_mdots)
	lab='median: %.1f\nmean: %.1f\nstd: %.1f' %(np.median(y),np.mean(y),np.std(y)) #label with med,mean,std 
	plt.hist(y,bins=8,color='b',alpha=0.5,label=lab) #normed=True)
	plt.xlabel(r'Log10 $\dot{M}/\dot{M}_B$')
	plt.ylabel('PDF')
	plt.title('Model: %s' % beta)
	plt.legend(loc=2)
	plt.savefig('pdf_rates_%s.png' % beta)
	plt.close()


fin=open(args.data_file,'r')
rate,time =load(fin)
fin.close()
print 'max time=',time.max()
#plot 64 indiv rates vs. time
all_rates(rate,time)  
#plot median and mean rates vs. time
mean_med_rates(rate,time)  
#histogram/PDF
#time average between these two limits in our time units
if beta == 'bInf': avg_lo,avg_hi= 5.,6. #beta = Infinity (hydro)
elif beta == 'bInf_seed3': avg_lo,avg_hi= 5.,6. #different seed for random generator for turbulence, beta = Infinity
elif beta == 'b100': avg_lo,avg_hi= 22.,24. 
elif beta == 'b10': avg_lo,avg_hi= 20.,22. #sinks interact after t = 22
elif beta == 'b1': avg_lo,avg_hi= 15.,17.
elif beta == 'b1e-1': avg_lo,avg_hi= 10.5,12.5
elif beta == 'b1e-2': avg_lo,avg_hi= 4.5,5.
else: raise ValueError
pdf_mdots(rate,time,avg_lo,avg_hi)
