'''gets values for Lbox,M0, rB/L from user input
outputs my simulation versions of Krumholz 2006 Figs 6,7,8'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5_M0_10",action="store",help='hdf5 data file when M0 = 10 (ie would insert sinks here for M0=10 case')
parser.add_argument("-hdf5_M0_5",action="store",help='hdf5 data file when M0 = 5')
# parser.add_argument("-hdf5_M0_3",action="store",help='hdf5 data file when M0 = 3')
parser.add_argument("-cs",action="store",help='constant sound speed')
parser.add_argument("-Lbox",action="store",help='length simulation box, cm')
args = parser.parse_args()
if args.hdf5_M0_10 and args.hdf5_M0_5 and args.cs and args.Lbox: 
    hdf5_M0_10= str(args.hdf5_M0_10)
    hdf5_M0_5= str(args.hdf5_M0_5)
    cs= float(args.cs)
    Lbox = float(args.Lbox)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.optimize import curve_fit
import my_fund_const as fc

def CurlCellDiff(pf,vx,vy,vz):
    #FIRST PART
    class CellFaces():
        class Vel():
            def __init__(self,vx,vy,vz):
                dim= np.array(vx.shape)+1  #vx,vy,vz all same size
                self.Vx= np.zeros(dim)
                self.Vy= self.Vx.copy()
                self.Vz= self.Vx.copy()
        def __init__(self,vx,vy,vz):
            '''vx,vy,vz are from vx=covering_grid['X-momentum']/covering_grid['density'], etc
            LR - Left/Right faces (x dir)
            FB - Front/Back faces (y dir)
            BT - Bottom/Top faces (z dir)
            '''
            self.LR= self.Vel(vx,vy,vz)
            self.TB= self.Vel(vx,vy,vz)
            self.FB= self.Vel(vx,vy,vz)

    Face= CellFaces(vx,vy,vz)
    first=0
    last=vx.shape[0]-1
    #iterate Left/Right faces first (eg. x direction)
    for i in range(vx.shape[0]):
        ind=i-1
        if i==first: ind=last
        Face.LR.Vx[i,:-1,:-1]= (vx[ind,:,:]+vx[i,:,:])/2  #:-1 keep dimensions same
        Face.LR.Vy[i,:-1,:-1]= (vy[ind,:,:]+vy[i,:,:])/2
        Face.LR.Vz[i,:-1,:-1]= (vz[ind,:,:]+vz[i,:,:])/2
    #fill that last index of face in x dir, it is the same as the first Face value
    Face.LR.Vx[-1,:,:]= Face.LR.Vx[0,:,:] #dim match so no -1
    Face.LR.Vy[-1,:,:]= Face.LR.Vy[0,:,:] 
    Face.LR.Vz[-1,:,:]= Face.LR.Vz[0,:,:]
    #iterate Front/Back faces (eg. y direction)
    for j in range(vx.shape[0]):
        ind=j-1
        if j==first: ind=last
        Face.FB.Vx[:-1,j,:-1]= (vx[:,ind,:]+vx[:,j,:])/2
        Face.FB.Vy[:-1,j,:-1]= (vy[:,ind,:]+vy[:,j,:])/2
        Face.FB.Vz[:-1,j,:-1]= (vz[:,ind,:]+vz[:,j,:])/2
    Face.FB.Vx[:,-1,:]= Face.FB.Vx[:,0,:]  
    Face.FB.Vy[:,-1,:]= Face.FB.Vy[:,0,:]
    Face.FB.Vz[:,-1,:]= Face.FB.Vz[:,0,:]
    #iterate Top/Bottom faces (eg. z direction)
    for k in range(vx.shape[0]):
        ind=k-1
        if k==first: ind=last
        Face.TB.Vx[:-1,:-1,k]= (vx[:,:,ind]+vx[:,:,k])/2
        Face.TB.Vy[:-1,:-1,k]= (vy[:,:,ind]+vy[:,:,k])/2
        Face.TB.Vz[:-1,:-1,k]= (vz[:,:,ind]+vz[:,:,k])/2
    Face.TB.Vx[:,:,-1]= Face.TB.Vx[:,:,0] 
    Face.TB.Vy[:,:,-1]= Face.TB.Vy[:,:,0]
    Face.TB.Vz[:,:,-1]= Face.TB.Vz[:,:,0] 
    #3 faces (1/2 of cell in corner [-1,-1,-1]) have not been filled, do so now
    Face.LR.Vx[-1,-1,-1]= Face.LR.Vx[0,-1,-1]
    Face.LR.Vy[-1,-1,-1]= Face.LR.Vy[0,-1,-1]
    Face.LR.Vz[-1,-1,-1]= Face.LR.Vz[0,-1,-1]
    #
    Face.FB.Vx[-1,-1,-1]= Face.FB.Vx[-1,0,-1]
    Face.FB.Vy[-1,-1,-1]= Face.FB.Vy[-1,0,-1]
    Face.FB.Vz[-1,-1,-1]= Face.FB.Vz[-1,0,-1]
    #
    Face.TB.Vx[-1,-1,-1]= Face.TB.Vx[-1,-1,0]
    Face.TB.Vy[-1,-1,-1]= Face.TB.Vy[-1,-1,0]
    Face.TB.Vz[-1,-1,-1]= Face.TB.Vz[-1,-1,0]
    #SECOND PART
    class Gradients():
        def __init__(self,vx):
            dim= np.array(vx.shape)
            #grad in x dir
            self.dVy_dx= np.zeros(dim)
            self.dVz_dx= self.dVy_dx.copy()
            #grad in y dir
            self.dVx_dy= self.dVy_dx.copy()
            self.dVz_dy= self.dVy_dx.copy()
            #grad in z dir
            self.dVx_dz= self.dVy_dx.copy()
            self.dVy_dz= self.dVy_dx.copy()
    Grad= Gradients(vx)
    #grad in x dir
    for i in range(vx.shape[0]):
        Grad.dVy_dx[i,:,:]= Face.LR.Vy[i+1,:-1,:-1]-Face.LR.Vy[i,:-1,:-1] #Grad arrays have Sim data dims
        Grad.dVz_dx[i,:,:]= Face.LR.Vz[i+1,:-1,:-1]-Face.LR.Vz[i,:-1,:-1]
    #grad in y dir
    for j in range(vx.shape[0]):
        Grad.dVx_dy[:,j,:]= Face.FB.Vx[:-1,j+1,:-1]-Face.FB.Vx[:-1,j,:-1]
        Grad.dVz_dy[:,j,:]= Face.FB.Vz[:-1,j+1,:-1]-Face.FB.Vz[:-1,j,:-1]
    #grad in z dir
    for k in range(vx.shape[0]):
        Grad.dVx_dz[:,:,k]= Face.TB.Vx[:-1,:-1,k+1]-Face.TB.Vx[:-1,:-1,k]
        Grad.dVy_dz[:,:,k]= Face.TB.Vy[:-1,:-1,k+1]-Face.TB.Vy[:-1,:-1,k]
    #don't forget to DIVIDE BY CELL WIDTH!
    lev=0
    wid_cell= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**lev
    Grad.dVy_dx /= wid_cell
    Grad.dVz_dx /= wid_cell
    #
    Grad.dVx_dy /= wid_cell
    Grad.dVz_dy /= wid_cell
    #
    Grad.dVx_dz /= wid_cell
    Grad.dVy_dz /= wid_cell
    #THIRD PART
    class CurlVector():
        def __init__(self,Grad):
            self.xhat= Grad.dVz_dy- Grad.dVy_dz
            self.yhat= -(Grad.dVz_dx- Grad.dVx_dz)
            self.zhat= Grad.dVy_dx- Grad.dVx_dy
            self.mag= np.sqrt(self.xhat**2+self.yhat**2+self.zhat**2)
    Curl= CurlVector(Grad)
    return Curl.mag

def MdotBH_x(rho_x,Msink,cs,MachS_x):
    one=4.*np.pi*rho_x*(fc.G*Msink)**2*cs**-3
    lam=1.1
    return one*(lam**2+MachS_x**2)/((1+MachS_x**2)**4)**0.5

def MdotW_x(Msink,rho_x,cs,vort_x):
    rB= fc.G*Msink/cs**2
    wstar_x= vort_x*rB/cs
    fstar_x= 1/(1+wstar_x**0.9)
    return 4*np.pi*rho_x*(fc.G*Msink)**2*cs**-3*0.34*fstar_x

class Fig6Data():
    def __init__(self):
        self.uBins= -1
        self.uFit= -1
        self.uPDF= -1
class Fig7Data():
    def __init__(self):
        self.wBins= -1
        self.wPDF= -1

class AnORSim():
    def __init__(self):
        self.rB_L=-1
        self.phi_mean= -1
        self.phi_med= -1 

class Fig8Data():
    def __init__(self):
        self.An= AnORSim()
        self.Sim= AnORSim()

class FigureData():              
    def __init__(self):
        self.Fig6= Fig6Data()
        self.Fig7= Fig7Data()
        self.Fig8= Fig8Data()

class MachData():
    def __init__(self):
        self.M10= FigureData()
        self.M5= FigureData()
        #self.M3= FigureData

def pdf_u(u, p1):
  return p1*u**3*np.exp(-1.5*u**1.7)

def calc_M0ofVField(vx,vy,vz,rho,cs):
    vrms2= (vx**2+vy**2+vz**2)
    vrms2_MassWt= np.average(vrms2,weights=rho)
    return np.sqrt(vrms2_MassWt)/cs


#MAIN#
spath='./plots/'
data= MachData() #stores all computed values
#choice of rB sets Msink that you assume for Ansatz
log_rB_L= np.array([-5,-3,-1,-0.7,1])
Msink= 10.**log_rB_L*Lbox*cs**2/fc.G
#####
#M0=10 data
#####
M0_box= 10.
ds=yt.load(hdf5_M0_10)
cgrid= ds.covering_grid(level=0,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions) 
rho_x=np.array(cgrid['density']) #_x means have value at every cell in grid
vx_x=np.array(cgrid['X-momentum']/rho_x)
vy_x=np.array(cgrid['Y-momentum']/rho_x)
vz_x=np.array(cgrid['Z-momentum']/rho_x)
vrms_x= np.sqrt(vx_x**2+vy_x**2+vz_x**2)
MachS_x= vrms_x/cs
vort_x= CurlCellDiff(ds,vx_x,vy_x,vz_x)
#Fig 6
u= vrms_x/M0_box/cs
y=np.log10(u)
(u_num,u_bins,jnk)= plt.hist(y.flatten(),bins=100,normed=True,align='mid',histtype='bar')
u_avgbins= (u_bins[:-1]+u_bins[1:])/2

xdata = u_avgbins
ydata = u_num

guess= pdf_u(ydata,1.).max()/ydata.max()
p1 = curve_fit(pdf_u, xdata, ydata,p0=(guess))
u_fit= pdf_u(ydata,p1[0][0])
data.M10.Fig6.uBins= u_avgbins
data.M10.Fig6.uFit= u_fit
data.M10.Fig6.uPDF= u_num
#Fig 7
w_squig= vort_x*Lbox/M0_box/cs
y=np.log10(w_squig)
(w_num,w_bins,jnk)= plt.hist(y.flatten(),bins=100,normed=True,align='mid',histtype='bar')
plt.close()
w_avgbins= (w_bins[:-1]+w_bins[1:])/2
data.M10.Fig7.wBins= w_avgbins
data.M10.Fig7.wPDF= w_num
#Fig 8
rB_L= 10**np.arange(-6,2,1).astype(np.float16)
phi_u=0.95
phi_v= 0.93
phi_w= 1.25
InBrack= 1+(10./phi_v**6)*(10.*phi_w*rB_L/M0_box**2.3)**1.8
phi_med= (phi_v**-3/np.sqrt(1+M0_box**2/4))*InBrack**-0.5
phi_mean= (3./phi_u**3)*(np.log(2*phi_u*M0_box)-1)*(1+100.*rB_L/M0_box)**-0.68
data.M10.Fig8.An.rB_L= rB_L
data.M10.Fig8.An.phi_mean= phi_mean
data.M10.Fig8.An.phi_med= phi_med
#loop over log(rB_L)
for logrBL,mass in zip(log_rB_L,Msink):
    Mbh_x= MdotBH_x(rho_x,mass,cs,MachS_x)
    Mw_x= MdotW_x(mass,rho_x,cs,vort_x)
    Mturb_x= (Mbh_x**-2 + Mw_x**-2)**-0.5
    M0= 4.*np.pi*rho_x.mean()*(fc.G*mass)**2/(M0_box*cs)**3
    data.M10.Fig8.Sim.rB_L= 10**logrBL
    data.M10.Fig8.Sim.phi_mean= np.mean(Mturb_x)/M0
    data.M10.Fig8.Sim.phi_med= np.median(Mturb_x)/M0
#####
#M0= 5 data
#####
M0_box= 5.
ds=yt.load(hdf5_M0_5)
cgrid= ds.covering_grid(level=0,left_edge=ds.domain_left_edge,
                          dims=ds.domain_dimensions) 
rho_x=np.array(cgrid['density']) #_x means have value at every cell in grid
vx_x=np.array(cgrid['X-momentum']/rho_x)
vy_x=np.array(cgrid['Y-momentum']/rho_x)
vz_x=np.array(cgrid['Z-momentum']/rho_x)
vrms_x= np.sqrt(vx_x**2+vy_x**2+vz_x**2)
MachS_x= vrms_x/cs
vort_x= CurlCellDiff(ds,vx_x,vy_x,vz_x)
#Fig 6
u= vrms_x/M0_box/cs
y=np.log10(u)
(u_num,u_bins,jnk)= plt.hist(y.flatten(),bins=100,normed=True,align='mid',histtype='bar')
plt.close()
u_avgbins= (u_bins[:-1]+u_bins[1:])/2

xdata = u_avgbins
ydata = u_num

guess= np.median(pdf_u(ydata,1.))/np.median(ydata.mean())
p1 = curve_fit(pdf_u, xdata, ydata,p0=(guess))
u_fit= pdf_u(ydata,p1[0][0])
data.M5.Fig6.uBins= u_avgbins
data.M5.Fig6.uFit= u_fit
data.M5.Fig6.uPDF= u_num
#Fig 7
w_squig= vort_x*Lbox/M0_box/cs
y=np.log10(w_squig)
(w_num,w_bins,jnk)= plt.hist(y.flatten(),bins=100,normed=True,align='mid',histtype='bar')
plt.close()
w_avgbins= (w_bins[:-1]+w_bins[1:])/2
data.M5.Fig7.wBins= w_avgbins
data.M5.Fig7.wPDF= w_num
#Fig 8
rB_L= 10**np.arange(-6,2,1).astype(np.float16)
phi_u=0.95
phi_v= 0.93
phi_w= 1.25
InBrack= 1+(10./phi_v**6)*(10.*phi_w*rB_L/M0_box**2.3)**1.8
phi_med= (phi_v**-3/np.sqrt(1+M0_box**2/4))*InBrack**-0.5
phi_mean= (3./phi_u**3)*(np.log(2*phi_u*M0_box)-1)*(1+100.*rB_L/M0_box)**-0.68
data.M5.Fig8.An.rB_L= rB_L
data.M5.Fig8.An.phi_mean= phi_mean
data.M5.Fig8.An.phi_med= phi_med
#loop over log(rB_L)
for logrBL,mass in zip(log_rB_L,Msink):
    Mbh_x= MdotBH_x(rho_x,mass,cs,MachS_x)
    Mw_x= MdotW_x(mass,rho_x,cs,vort_x)
    Mturb_x= (Mbh_x**-2 + Mw_x**-2)**-0.5
    M0= 4.*np.pi*rho_x.mean()*(fc.G*mass)**2/(M0_box*cs)**3
    data.M5.Fig8.Sim.rB_L= 10**logrBL
    data.M5.Fig8.Sim.phi_mean= np.mean(Mturb_x)/M0
    data.M5.Fig8.Sim.phi_med= np.median(Mturb_x)/M0

#make Figs 6-8
def PlotFig6(data,fsave):
    plt.plot(data.M10.Fig6.uBins,data.M10.Fig6.uPDF,'b*',label="M10 PDF")
    plt.plot(data.M10.Fig6.uBins,data.M10.Fig6.uFit,'b-',label="K06 M10 Fit")
    plt.plot(data.M5.Fig6.uBins,data.M5.Fig6.uPDF,'k*',label="M5 PDF")
    plt.plot(data.M5.Fig6.uBins,data.M5.Fig6.uFit,'k-',label="K06 M5 Fit")
    plt.xlim([1e-3,1e1])
    plt.ylim([5e-7,3])
    plt.yscale('log')
    plt.xscale('log')
    plt.title('K06 Fig. 6')
    plt.xlabel(r'u $= v_{rms}/M_0c_s$')
    plt.ylabel('Normalized Histogram of u')
    plt.savefig(fsave,dpi=150)

def PlotFig7(data,fsave):
    plt.plot(data.M10.Fig7.wBins,data.M10.Fig7.wPDF,'b*',label="M10 PDF")
    plt.plot(data.M5.Fig7.wBins,data.M5.Fig7.wPDF,'k*',label="M5 PDF")
    plt.xlim([1e-2,1e3])
    plt.ylim([5e-7,3])
    plt.yscale('log')
    plt.xscale('log')
    plt.title('K06 Fig. 7')
    plt.xlabel(r'$\tilde{\omega}= \omega L/(M_0c_s)$')
    plt.ylabel(r'Normalized Histogram of $\tilde{\omega}$')
    plt.savefig(fsave,dpi=150)


def PlotFig8(data,fsave):
    f,axes= plt.subplots(2,1,sharex=True)
    ax= axes.flatten()
    ax[0].plot(data.M10.Fig8.An.rB_L,data.M10.Fig8.An.phi_mean,'k-')
    ax[0].plot(data.M10.Fig8.Sim.rB_L,data.M10.Fig8.Sim.phi_mean,'^-')
    ax[0].plot(data.M5.Fig8.An.rB_L,data.M5.Fig8.An.phi_mean,'b-')
    ax[0].plot(data.M5.Fig8.Sim.rB_L,data.M5.Fig8.Sim.phi_mean,'*-')
    #ax[0].set_ylim([1e-2,20])
    ax[0].set_ylabel(r'$\phi_{mean}$')
    ax[0].plot(data.M10.Fig8.An.rB_L,data.M10.Fig8.An.phi_med,'k-')
    ax[0].plot(data.M10.Fig8.Sim.rB_L,data.M10.Fig8.Sim.phi_med,'^-')
    ax[0].plot(data.M5.Fig8.An.rB_L,data.M5.Fig8.An.phi_med,'b-')
    ax[0].plot(data.M5.Fig8.Sim.rB_L,data.M5.Fig8.Sim.phi_med,'*-')
    #ax[1].set_ylim([1e-2,2])
    ax[1].set_xlabel(r'$r_B/l$')
    ax[1].set_ylabel(r'$\phi_{med}$')
    for a in ax:
        a.set_xscale('log')
        a.set_yscale('log')
    f.subplots_adjust(hspace=0)
    plt.savefig(fsave,dpi=150)

fsave=spath+"K06_Fig6_for_M10-%s_M5-%s.png" % (hdf5_M0_10,hdf5_M0_5)
PlotFig6(data,fsave)
fsave=spath+"K06_Fig7_for_M10-%s_M5-%s.png" % (hdf5_M0_10,hdf5_M0_5)
PlotFig7(data,fsave)
fsave=spath+"K06_Fig8_for_M10-%s_M5-%s.png" % (hdf5_M0_10,hdf5_M0_5)
PlotFig8(data,fsave)
