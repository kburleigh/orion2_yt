#show sink locations relative to some location in grid
#
import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-Lbox",action="store",help='Lbox in cm')
args = parser.parse_args()
if args.Lbox: Lbox= float(args.Lbox)
else: raise ValueError

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
    

def scat3D(x,y,z,Lbox,xyz_pt):
    '''3D plot to sanity check pos of sinks'''
    fig,ax = plt.subplots()
    ax = Axes3D(fig)
    ax.scatter(x,y,z)
    ax.scatter(xyz_pt[0],xyz_pt[1],xyz_pt[2],c='r',marker='*')
#     ax.scatter(0,0,0) #c='r',marker='*')
    ax.set_xlim(-Lbox/2.,Lbox/2.)
    ax.set_ylim(-Lbox/2.,Lbox/2.)
    ax.set_zlim(-Lbox/2.,Lbox/2.)
    plt.show()


##main
xyz_pt= (4.51e17,2.23e17,1.76e17) #in cm
fsink="data.0045.3d.sink"
mass,x,y,z= np.loadtxt(fsink,dtype='float',\
                                   skiprows=1,usecols=(0,1,2,3),unpack=True)
scat3D(x,y,z,Lbox,xyz_pt)

