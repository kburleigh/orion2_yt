'''use: Call this, then call "curl2py.m"
read in velocities from hdf5 data cube and cell size, output to matlabl readable file so can compute curl with matlab'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-hdf5",action="store",help='hdf5 file to get velocities for')
args = parser.parse_args()
if args.hdf5: hdf5 = str(args.hdf5)
else: raise ValueError

import yt
import scipy.io as sio

pf=yt.load(hdf5)
lev= 0
cube= pf.covering_grid(level=lev,left_edge=pf.domain_left_edge,
                  dims=pf.domain_dimensions)
rho = np.array(cube["density"])
vx = np.array(cube["X-momentum"]/rho)
vy = np.array(cube["Y-momentum"]/rho)
vz = np.array(cube["Z-momentum"]/rho)
dx_size= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**lev
fname='vels_%s.mat' % pf.basename[5:9]
sio.savemat('./plots/'+fname, {'vx':vx,'vy':vy,'vz':vz,'dx_size':dx_size})