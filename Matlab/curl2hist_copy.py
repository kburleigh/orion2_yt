'''use: Call this, then call "curl2py.m"
read in velocities from matcurl data cube and cell size, output to matlabl readable file so can compute curl with matlab'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-matcurl",action="store",help='matlab .mat file containing flattened curl array')
parser.add_argument("-hdf5",action="store",help='hdf5 data dump again')
args = parser.parse_args()
if args.matcurl and args.hdf5: 
    matcurl = str(args.matcurl)
    hdf5= str(args.hdf5)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
import yt
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

class Curls():
    pass
curl= Curls()

#matlab's curl
a = sio.loadmat(matcurl)
curl.matlab=a['flat']
#my 2 curl functions and Will's
def CalcCurl(ds,Vx,Vy,Vz):
    wid_cell= float(ds.domain_width[0])/ds.domain_dimensions[0]/2**lev
    #finite diff
    dVy_dx= (Vy[:-1,:,:]-Vy[1:,:,:])/wid_cell
    dVz_dx= (Vz[:-1,:,:]-Vz[1:,:,:])/wid_cell
    dVx_dy= (Vx[:,:-1,:]-Vx[:,1:,:])/wid_cell
    dVz_dy= (Vz[:,:-1,:]-Vz[:,1:,:])/wid_cell
    dVx_dz= (Vx[:,:,:-1]-Vx[:,:,1:])/wid_cell
    dVy_dz= (Vy[:,:,:-1]-Vy[:,:,1:])/wid_cell
    #keep just regions that overlap
    dVy_dx= dVy_dx[:,:-1,:-1]
    dVz_dx= dVz_dx[:,:-1,:-1]
    dVx_dy= dVx_dy[:-1,:,:-1]
    dVz_dy= dVz_dy[:-1,:,:-1]
    dVx_dz= dVx_dz[:-1,:-1,:]
    dVy_dz= dVy_dz[:-1,:-1,:]
    #compute curl
    xhat= dVz_dy- dVy_dz
    yhat= -(dVz_dx - dVx_dz)
    zhat= dVy_dx- dVx_dy
    curl_mag= (xhat**2+yhat**2+zhat**2)**0.5
    return curl_mag

def CurlAtAllCells(pf,vx,vy,vz):
    #FIRST PART
    class CellFaces():
        class Vel():
            def __init__(self,vx,vy,vz):
                dim= np.array(vx.shape)+1  #vx,vy,vz all same size
                self.Vx= np.zeros(dim)
                self.Vy= self.Vx.copy()
                self.Vz= self.Vx.copy()
        def __init__(self,vx,vy,vz):
            '''vx,vy,vz are from vx=covering_grid['X-momentum']/covering_grid['density'], etc
            LR - Left/Right faces (x dir)
            FB - Front/Back faces (y dir)
            BT - Bottom/Top faces (z dir)
            '''
            self.LR= self.Vel(vx,vy,vz)
            self.TB= self.Vel(vx,vy,vz)
            self.FB= self.Vel(vx,vy,vz)

    Face= CellFaces(vx,vy,vz)
    first=0
    last=vx.shape[0]-1
    #iterate Left/Right faces first (eg. x direction)
    for i in range(vx.shape[0]):
        ind=i-1
        if i==first: ind=last
        Face.LR.Vx[i,:-1,:-1]= (vx[ind,:,:]+vx[i,:,:])/2  #:-1 keep dimensions same
        Face.LR.Vy[i,:-1,:-1]= (vy[ind,:,:]+vy[i,:,:])/2
        Face.LR.Vz[i,:-1,:-1]= (vz[ind,:,:]+vz[i,:,:])/2
    #fill that last index of face in x dir, it is the same as the first Face value
    Face.LR.Vx[-1,:,:]= Face.LR.Vx[0,:,:] #dim match so no -1
    Face.LR.Vy[-1,:,:]= Face.LR.Vy[0,:,:]
    Face.LR.Vz[-1,:,:]= Face.LR.Vz[0,:,:]
    #iterate Front/Back faces (eg. y direction)
    for j in range(vx.shape[0]):
        ind=j-1
        if j==first: ind=last
        Face.FB.Vx[:-1,j,:-1]= (vx[:,ind,:]+vx[:,j,:])/2
        Face.FB.Vy[:-1,j,:-1]= (vy[:,ind,:]+vy[:,j,:])/2
        Face.FB.Vz[:-1,j,:-1]= (vz[:,ind,:]+vz[:,j,:])/2
    Face.FB.Vx[:,-1,:]= Face.FB.Vx[:,0,:]
    Face.FB.Vy[:,-1,:]= Face.FB.Vy[:,0,:]
    Face.FB.Vz[:,-1,:]= Face.FB.Vz[:,0,:]
    #iterate Top/Bottom faces (eg. z direction)
    for k in range(vx.shape[0]):
        ind=k-1
        if k==first: ind=last
        Face.TB.Vx[:-1,:-1,k]= (vx[:,:,ind]+vx[:,:,k])/2
        Face.TB.Vy[:-1,:-1,k]= (vy[:,:,ind]+vy[:,:,k])/2
        Face.TB.Vz[:-1,:-1,k]= (vz[:,:,ind]+vz[:,:,k])/2
    Face.TB.Vx[:,:,-1]= Face.TB.Vx[:,:,0]
    Face.TB.Vy[:,:,-1]= Face.TB.Vy[:,:,0]
    Face.TB.Vz[:,:,-1]= Face.TB.Vz[:,:,0]
    #3 faces (1/2 of cell in corner [-1,-1,-1]) have not been filled, do so now
    Face.LR.Vx[-1,-1,-1]= Face.LR.Vx[0,-1,-1]
    Face.LR.Vy[-1,-1,-1]= Face.LR.Vy[0,-1,-1]
    Face.LR.Vz[-1,-1,-1]= Face.LR.Vz[0,-1,-1]
    #
    Face.FB.Vx[-1,-1,-1]= Face.FB.Vx[-1,0,-1]
    Face.FB.Vy[-1,-1,-1]= Face.FB.Vy[-1,0,-1]
    Face.FB.Vz[-1,-1,-1]= Face.FB.Vz[-1,0,-1]
    #
    Face.TB.Vx[-1,-1,-1]= Face.TB.Vx[-1,-1,0]
    Face.TB.Vy[-1,-1,-1]= Face.TB.Vy[-1,-1,0]
    Face.TB.Vz[-1,-1,-1]= Face.TB.Vz[-1,-1,0]
    #SECOND PART
    class Gradients():
        def __init__(self,vx):
            dim= np.array(vx.shape)
            #grad in x dir
            self.dVy_dx= np.zeros(dim)
            self.dVz_dx= self.dVy_dx.copy()
            #grad in y dir
            self.dVx_dy= self.dVy_dx.copy()
            self.dVz_dy= self.dVy_dx.copy()
            #grad in z dir
            self.dVx_dz= self.dVy_dx.copy()
            self.dVy_dz= self.dVy_dx.copy()
    Grad= Gradients(vx)
    #grad in x dir
    for i in range(vx.shape[0]):
        Grad.dVy_dx[i,:,:]= Face.LR.Vy[i+1,:-1,:-1]-Face.LR.Vy[i,:-1,:-1] #Grad arrays have Sim data dims
        Grad.dVz_dx[i,:,:]= Face.LR.Vz[i+1,:-1,:-1]-Face.LR.Vz[i,:-1,:-1]
    #grad in y dir
    for j in range(vx.shape[0]):
        Grad.dVx_dy[:,j,:]= Face.FB.Vx[:-1,j+1,:-1]-Face.FB.Vx[:-1,j,:-1]
        Grad.dVz_dy[:,j,:]= Face.FB.Vz[:-1,j+1,:-1]-Face.FB.Vz[:-1,j,:-1]
    #grad in z dir
    for k in range(vx.shape[0]):
        Grad.dVx_dz[:,:,k]= Face.TB.Vx[:-1,:-1,k+1]-Face.TB.Vx[:-1,:-1,k]
        Grad.dVy_dz[:,:,k]= Face.TB.Vy[:-1,:-1,k+1]-Face.TB.Vy[:-1,:-1,k]
    #don't forget to DIVIDE BY CELL WIDTH!
    lev=0
    wid_cell= float(pf.domain_width[0])/pf.domain_dimensions[0]/2**lev
    Grad.dVy_dx /= wid_cell
    Grad.dVz_dx /= wid_cell
    #
    Grad.dVx_dy /= wid_cell
    Grad.dVz_dy /= wid_cell
    #
    Grad.dVx_dz /= wid_cell
    Grad.dVy_dz /= wid_cell
    #THIRD PART
    class CurlVector():
        def __init__(self,Grad):
            self.xhat= Grad.dVz_dy- Grad.dVy_dz
            self.yhat= -(Grad.dVz_dx- Grad.dVx_dz)
            self.zhat= Grad.dVy_dx- Grad.dVx_dy
            self.mag= np.sqrt(self.xhat**2+self.yhat**2+self.zhat**2)
    Curl= CurlVector(Grad)
    return Curl.mag

def WillCurl(Vx,Vy,Vz,dx,dy,dz):
    sl_left = slice(None, -2, None)
    sl_right = slice(2, None, None)
    sl_center = slice(1, -1, None)
    div_fac = 2.0

    vortx = (Vz[sl_center,sl_right,sl_center] - Vz[sl_center,sl_left,sl_center] )/ (div_fac*just_one(dy)) - (Vy[sl_center,sl_center,sl_right]-Vy[sl_center,sl_center,sl_left])/(div_fac*just_one(dz))

    vorty = (Vx[sl_center,sl_center,sl_right] - Vx[sl_center,sl_center,sl_left] )/ (div_fac*just_one(dz)) - (Vz[sl_right,sl_center,sl_center]-Vz[sl_left,sl_center,sl_center])/(div_fac*just_one(dx))

    vortz = (Vy[sl_right,sl_center,sl_center] - Vy[sl_left,sl_center,sl_center] )/ (div_fac*just_one(dx)) - (Vx[sl_center,sl_right,sl_center]-Vx[sl_center,sl_left,sl_center])/(div_fac*just_one(dy))

    vmag = np.sqrt(vortx**2 + vorty**2 + vortz**2 )
    return vmag


ds=yt.load(hdf5)
cube= ds.covering_grid(level=0,left_edge=ds.domain_left_edge,
                      dims=ds.domain_dimensions)
rho = np.array(cube["density"])
vx = np.array(cube["X-momentum"]/rho)
vy = np.array(cube["Y-momentum"]/rho)
vz = np.array(cube["Z-momentum"]/rho)
dx = np.array(cube['dx'])
dy = np.array(cube['dy'])
dz = np.array(cube['dz'])
curl.k1= CalcCurl(ds,vx,vy,vz)
curl.k2= CurlAtAllCells(ds,vx,vy,vz)
curl.will= WillCurl(vx,vy,vz,dx,dy,dz)


fig,axis = plt.subplots()
ax= axis.flatten()
(ln_hist,lnbins,jnk)= ax[0].hist(np.log(curl.will).flatten(),bins=100,normed=True,align='mid',histtype='step',color='b')
(ln_hist,lnbins,jnk)= ax[1].hist(np.log(curl.k1).flatten(),bins=100,normed=True,align='mid',histtype='step',color='k')
(ln_hist,lnbins,jnk)= ax[2].hist(np.log(curl.k2).flatten(),bins=100,normed=True,align='mid',histtype='step',color='r')
(ln_hist,lnbins,jnk)= ax[3].hist(np.log(curl.matlab),bins=100,normed=True,align='mid',histtype='step',color='g')
fname='curlhist_%s.png' % ds.basename[5:9]
plt.savefig('./plots/'+fname)