'''use: read in velocities from hdf5 data cube and cell size, output to matlabl readable file so can compute curl with matlab'''
load ./plots/vels_%s.mat
[m,n,p] = size(vx);
[X Y Z] = meshgrid(1:n,1:m,1:p);
[curlx,curly,curlz,cav] = curl(X,Y,Z,vx,vy,vz);
curlx= curlx/dx_size;
curly= curly/dx_size;
curlz= curlz/dx_size;
curl_sq= power(curlx,2)+power(curly,2)+power(curlz,2);
curl_mag= power(curl_sq,0.5);
flat= reshape(curl_mag,[m*n*p 1 1]);
mean(flat),min(flat),max(flat),median(flat)
save flatcurl.mat flat