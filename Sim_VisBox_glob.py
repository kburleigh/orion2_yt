'''proj of entire box in x and z dir showing sink locations as black dots and velocity vectors. 2 dirs so 2 plots per time step'''

import argparse
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-fhdf5",action="store",help='OPTIONAL, if not given will use GLOB')
parser.add_argument("-xc",action="store",help='Length of box in cm')
parser.add_argument("-yc",action="store",help='Length of box in cm')
parser.add_argument("-width",action="store",help='Length of box in cm')
parser.add_argument("-rBH",action="store",help='Length of box in cm')
parser.add_argument("-maxlev",action="store",help='Length of box in cm')
parser.add_argument("-tcross",action="store",help='Length of box in cm')
args = parser.parse_args()
if args.xc and args.yc and args.width and args.rBH and args.maxlev and args.tcross:
    xc=float(args.xc)
    yc=float(args.yc)
    width=float(args.width)
    rBH=float(args.rBH)
    maxlev=int(args.maxlev)
    tcross=float(args.tcross)
else: raise ValueError

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

if args.fhdf5: fil= str(args.fhdf5)
else: 
    f_sink= 'data.0099.3d.sink'
    fil=glob.glob('./data.*.hdf5')
    fil.sort()

for f in fil:
    ds1 = load(f)
    dxmin= ds1.domain_width[0]/ds1.domain_dimensions[0]/2.**maxlev
    class SinkProps():
        pass
    sink=SinkProps()
    #f_sink= hdf5[:-4]+'sink'  #referencing sink file upon insert (so after driving/decay)
    sink.m,sink.x,sink.y,sink.z= np.loadtxt(f_sink,dtype='float',skiprows=1,usecols=(0,1,2,3),unpack=True)
    px= ProjectionPlot(ds1,'z',"density", center=(xc,yc),width=width,axes_unit="cm",\
                      max_level=maxlev)
    # px.annotate_grids(min_level=maxlev)
    # px = yt.ProjectionPlot(ds1, 'x', "density")
    px.set_axes_unit('cm')
    plot = px.plots[('density')]
    fig = plot.figure
    ax = plot.axes

    rBH_size = np.diff(ax.transData.transform((0,rBH)))
    accrad_size = np.diff(ax.transData.transform((4.*dxmin,0)))
    # if ntag_finest <= 7:
    #     r_div_dxmin= 20.
    # r_size = np.diff(ax.transData.transform((20*dxmin,0)))
    for x,y in zip(sink.x,sink.y):
        ax.scatter(x,y, s= np.pi*(rBH_size**2.), marker = 'o', facecolors = 'none', edgecolors = 'w')
        ax.scatter(x,y, s= np.pi*(accrad_size**2.), marker = 'o', facecolors = 'none', edgecolors = 'w')
	tnorm= float(ds1.current_time/tcross)
    ax.set_title("t/tcross= %.2e" % tnorm) 
    px.save('plots/zproj_Box_%s.png' %ds1.basename[5:9])
