'''to run: python this_script_name.py -- copy this script to sim produciton dir, run it from submit script
purpose: calc average Qs (magnetic beta, B field, V RMS, Va RMS) and Mdot values for 64 sinks'''

import matplotlib
matplotlib.use('Agg')
from yt.mods import *
import numpy as np
import matplotlib.pyplot as plt
import glob

#yt derived fields
def _xvel(field, data):
    return data['X-momentum']/data['density']
add_field("x-vel", function=_xvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _yvel(field, data):
    return data['Y-momentum']/data['density']
add_field("y-vel", function=_yvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _zvel(field, data):
    return data['Z-momentum']/data['density']
add_field("z-vel", function=_zvel, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bx(field, data):
    return (4.*fc.pi)**0.5 *data['X-magnfield']
add_field("Bx", function=_Bx, take_log=False,
          units=r'\rm{gauss}')

def _By(field, data):
    return (4.*fc.pi)**0.5 *data['Y-magnfield']
add_field("By", function=_By, take_log=False,
          units=r'\rm{gauss}')

def _Bz(field, data):
    return (4.*fc.pi)**0.5 *data['Z-magnfield']
add_field("Bz", function=_Bz, take_log=False,
          units=r'\rm{gauss}')

##
def _Vmag(field, data):
    return np.sqrt(data['x-vel']**2 + data['y-vel']**2 + data['z-vel']**2)
add_field("Vmag", function=_Vmag, take_log=False,
        units=r'\rm{cm}/\rm{s}')

def _Bmag(field, data):
    return np.sqrt( data['Bx']**2 + data['By']**2 + data['Bz']**2)
add_field("Bmag", function=_Bmag, take_log=False,
          units=r'\rm{gauss}')

def _Va(field, data):
    return data['Bmag'] / np.sqrt(4*np.pi* data['density'])
add_field("Va", function=_Va, take_log=False,
          units=r'\rm{cm/s}')


#hdf5 files: `calc avgerage density, magnetic field strength, velocity, va, and beta
class TypesOfAverages():
    def __init__(self,files):
        '''files: hdf5 file list'''
        self.Avg= np.zeros(len(files)) -1
        self.MassWtAvg=np.zeros(len(files)) -1

class AvgQuantities():
    def __init__(self,files):
        '''files: hdf5 file list'''
        self.Time= np.zeros(len(files)) -1
        self.Dens= TypesOfAverages(files)
        self.Bmag= TypesOfAverages(files)
        self.Vmag= TypesOfAverages(files)
        self.Beta= TypesOfAverages(files)
        self.Va= TypesOfAverages(files)

def CalcAvgQuantities(f_hdf5,LevToCalcOn):
    '''f_hdf5: hdf5 file list
    LevToCalcOn: pf.h.covering_grid(level=LevToCalcOn, ...)'''
    av= AvgQuantities(f_hdf5)
    for cnt,f in enumerate(f_hdf5):
        print "loading: %s" % f
        pf=load(f)
        # dims=pf.domain_dimensions*2**LevToCalcOn
#         cube= pf.h.covering_grid(level=LevToCalcOn,\
#                                   left_edge=pf.h.domain_left_edge,
#                                   dims=dims)
#         d = cube["density"]
#         b= cube["Bmag"]
#         v= cube["Vmag"]
#         beta= d*1.**2*8*np.pi/b**2
#         va= b/np.sqrt(4*np.pi*d)
#         MassWt= d/d.max()
        av.Time[cnt]=pf.current_time
        # av.Dens.Avg[cnt]= np.average(d)
#         av.Dens.MassWtAvg[cnt]=np.average(d,weights=MassWt)
#         av.Bmag.Avg[cnt]= np.average(b)
#         av.Bmag.MassWtAvg[cnt]=np.average(b,weights=MassWt)
#         av.Vmag.Avg[cnt]= np.average(v)
#         av.Vmag.MassWtAvg[cnt]=np.average(v,weights=MassWt)
#         av.Beta.Avg[cnt]= np.average(beta)
#         av.Beta.MassWtAvg[cnt]=np.average(beta,weights=MassWt)
#         av.Va.Avg[cnt]= np.average(va)
#         av.Va.MassWtAvg[cnt]=np.average(va,weights=MassWt)
    return av

#.sink files: load in Sink data and calc mdot
class SinkData():
    def __init__(self,NSinks,NSinkFiles):
        shape= (NSinkFiles,NSinks)
        self.mass= np.zeros(shape)-1
        self.x= np.zeros(shape)-1
        self.y= np.zeros(shape)-1
        self.z= np.zeros(shape)-1
        self.MdotBHA= np.zeros(shape)-1
        self.MdotBHA_Avg= np.zeros(NSinkFiles)
        self.Mdot= np.zeros(shape)-1 #Mdot of sink particles
        self.MdotAvg= np.zeros(NSinkFiles) #avg mdot of sinks at each time
        

def Mdot_bha_iso(Mstar,rho_inf,cs,vmag,va):
    lam= 1.12
    v2= (cs**2+vmag**2+va**2)
    return 4*np.pi*lam*Mstar**2*rho_inf/v2**1.5

def CalcSinkQuantities(f_sink,av,NSinks):
    '''f_sink: list of .sink files
    av: returned by CalcAvgQuantities()'''
    sink= SinkData(NSinks,len(f_sink))
    for cnt in range(len(f_sink)):   
        mass,x,y,z= np.loadtxt(f_sink[cnt],dtype='float',\
                               skiprows=1,usecols=(0,1,2,3),unpack=True)
        sink.mass[cnt,:]= mass
        sink.x[cnt,:]= x
        sink.y[cnt,:]= y
        sink.z[cnt,:]= z

    #calc Mdot
    sink.Mdot[0,:]=0.
    sink.MdotAvg[0]= np.average(sink.Mdot[0,:])
    sink.MdotBHA[0,:]=0.
    sink.MdotBHA_Avg[0]=np.average(sink.MdotBHA[0,:]) 
    for cnt in np.arange(1,len(f_sink)):
        deltaM= sink.mass[cnt,:] -sink.mass[cnt-1,:] 
        deltaT= av.Time[cnt]- av.Time[cnt-1]
        sink.Mdot[cnt,:]= deltaM/deltaT
        sink.MdotAvg[cnt]= np.average(sink.Mdot[cnt,:])
        #analytic
        # cs=1.
        sink.MdotBHA[cnt,:]= Mdot_bha_iso(sink.mass[cnt,:],av.Dens.Avg[cnt],
#                                     cs,av.Vmag.Avg[cnt],av.Va.Avg[cnt]) 
        sink.MdotBHA_Avg[cnt] = np.average(sink.MdotBHA[cnt,:])
    return sink

#plotting funcs
def PlotAvgQs(av,tB):
    '''av: returned by CalcAvgQuantities
    tB: bondi time in units of simulation'''
    f,axis= plt.subplots(2,3,figsize=(10,5))
    ax=axis.flatten()
    time= (av.Time - av.Time[0])/tB
    ax[0].plot(time,av.Dens.Avg,"k-*",label="Avg")
    ax[0].plot(time,av.Dens.MassWtAvg,"b-*",label="MassWtAvg")
    ax[1].plot(time,av.Vmag.Avg,"k-*",label="Avg")
    ax[1].plot(time,av.Vmag.MassWtAvg,"b-*",label="MassWtAvg")
    ax[2].plot(time,av.Va.Avg,"k-*",label="Avg")
    ax[2].plot(time,av.Va.MassWtAvg,"b-*",label="MassWtAvg")
    ax[3].plot(time,av.Bmag.Avg,"k-*",label="Avg")
    ax[3].plot(time,av.Bmag.MassWtAvg,"b-*",label="MassWtAvg")
    ax[4].plot(time,av.Beta.Avg,"k-*",label="Avg")
    ax[4].plot(time,av.Beta.MassWtAvg,"b-*",label="MassWtAvg")
    ylabs= ['Density',"RMS V","RMS Va","|B|",r"Magnetic $\beta$",'none']
    for i in range(6):
        ax[i].set_xlabel("t/tB")
        ax[i].set_ylabel(ylabs[i])
    ax[0].legend(loc=(1.1,1.1),ncol=2)#bbox_to_anchor = (0.,1.4))
    f.subplots_adjust(wspace=0.5,hspace=0.5)
    f.savefig("AvgQs.png",dpi=300)


def PlotMdot(sink,av,tB,f_save):
    '''sink: returned by CalcSinkQuantities()
    av: returned by CalcAvgQuantities
    tB: bondi time in units of simulation'''
    fig,ax= plt.subplots(figsize=(5,5))
    time= (av.Time - av.Time[0])/tB
    ax.plot(time,sink.MdotAvg,"k-*",label="SinkAvg")
    ax.plot(time,sink.MdotBHA_Avg,"b-*",label="BHA_Avg")
    ax.set_xlabel("t/tB")
    ax.set_ylabel("Mdot")
    ax.legend(loc=(0.1,1.01),ncol=2)
    ax.set_ylim([-0.1*sink.MdotBHA_Avg.max(),1.1*sink.MdotBHA_Avg.max()])
    fig.savefig(fsave,dpi=200)


#MAIN
path = "sinks/"
f_hdf5= glob.glob(path+"data.00*.3d.hdf5")
f_sink= glob.glob(path+"data.00*.3d.sink")
tB= ####

LevToCalcOn= 3 #KEY!
print "in CalcAvgQuantities()"
av=CalcAvgQuantities(f_hdf5,LevToCalcOn)
print "in CalcSinkQuantities()"
sink=CalcSinkQuantities(f_sink,av,NSinks=16)

#approx tB
# vrms= 5.
# r_bh=sink.mass[0,0]/vrms**2
# tB = r_bh/vrms
#plot
print "Plotting"
# PlotAvgQs(av,tB)
PlotMdot(sink,av,tB)
