from yt.mods import *
import pylab as py
import numpy as np

import sys
sys.path.append("/global/home/users/kaylanb/myCode/ytScripts/Lib")
import my_fund_const as fc
import ic_dfields as myic


class readData(object):
    def __init__(self, ic):
        self.mdot_anB = mdot_analyticB(
                                ic.Msink,ic.rho0,ic.getV2() )
        self.mdot_anFit = mdot_analyticFit(
                                self.mdot_anB, ic.mach_s, ic.beta)
        self.sink = []
        self.t = []
        self.ms = []
        self.mdot_sim = []
        

def mdot_analyticB(Msink,rhoInf,vtot2):
    v3 = vtot2**1.5
    return 4*np.pi*rhoInf*(fc.G*Msink)**2/v3

def mdot_analyticFit(mdot_anB, mach_s, beta):
    lam = 1.12
    Mbh = (1.+ mach_s**4)**(1./3)
    Mbh /= (1.+ (mach_s/lam)**2)**(1./6)
    denom = 1 + 4.4/Mbh/beta**0.5
    return mdot_anB/ denom


#Initial conditions for SIM
pars =myic.ParsNeed()
pars.temp=1.e6
pars.mach_s=1.41
pars.cells=32.
pars.levs =0
pars.Lbox=10.
pars.prout()
ic=myic.initCondData(pars)
ic.prout()
myic.add_ALL_fields(ic)
#######

# look at the mass of the particle to determine if the test passes or not.
path="/clusterfs/henyey/kaylanb/Test_Prob/Sink/Lee14/quick/beta-10_M-1.41/lev2/"
spath=path+"plots/"
dpatt = "data.00*.3d.hdf5"
files = glob.glob1(path, dpatt)
print "printing files: ",files

#get sink evol
rd = readData(ic)

for f in files:
	#read sink
	fsink= f[:-4] + "sink"
	print "fsink opening: %s" % fsink
	rsink = open(path+fsink)
	line = rsink.readline()
	line = rsink.readline()
	rsink.close()
	d = {}
	keys = ["m","x","y","z","px","py","pz","lx","ly","lz","id"]
	larr = line.strip(' \n').split(' ')
	larr.remove("")
	for i,key in enumerate(keys):
	    d[key] = float(larr[i])
	rd.sink.append(d)

#sink data
for ind,f in enumerate(files):
	print "loading file: %s" % (f,)
	pf=load(path+f)
	rd.t.append(pf.current_time)
	rd.ms.append(rd.sink[ind]["m"])
	if ind == 0:
		rd.mdot_sim.append(0.)
	if ind >= 1:
		rd.mdot_sim.append( (rd.ms[ind]-rd.ms[ind-1])/(rd.t[ind]-rd.t[ind-1])  )

rd.t = np.array(rd.t)
rd.ms= np.array(rd.ms)
rd.mdot_sim = np.array(rd.mdot_sim)

#plotting
x=rd.t/ic.tB
y=rd.mdot_sim*fc.secyr/fc.msun
py.plot(x,y,"b-",label="sink")
y=rd.mdot_anB*fc.secyr/fc.msun
py.hlines(y,x.min(),x.max(),colors='k',linestyles='solid',label="BHL analytic")
y=rd.mdot_anFit*fc.secyr/fc.msun
py.hlines(y,x.min(),x.max(),colors='k',linestyles='dashed',label="Lee14 Fit analytic")
py.legend(loc=0)
ax = py.gca()
ax.set_yscale('log')
ax.set_ylabel('Mdot [Msun / Yr]')
ax.set_xlabel('t/tB')
py.savefig(spath+"mdot_v_time.pdf",format="pdf")
py.close()

py.plot(rd.t/ic.tB,rd.ms/fc.msun,"k-")
ax = py.gca()
ax.set_ylim([0.1,2])
ax.set_yscale('log')
ax.set_ylabel('Mass of Sink [Msun]')
ax.set_xlabel('t/tB')
py.savefig(spath+"ms_v_time.pdf",format="pdf")
py.close()


# pc = PlotCollection(pf, center = [0, 0, 0])
# bin_sphere = pc.add_profile_sphere(3.2e13, "cm", ['Radius', 'density',"tot-vel"], weight='CellVolume', x_bounds = (0.1*ic.rB, 10*ic.rB))
# x = bin_sphere.data['Radius'] /ic.rB
# y = bin_sphere.data['density'] /ic.rho0
# y = bin_sphere.data['tot-vel'] /ic.cs
# y = bin_sphere.data['tot-vel'] /ic.va
# py.plot()



