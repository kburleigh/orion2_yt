import argparse
import numpy as np

parser = argparse.ArgumentParser(description="make 2D density image for all ORION2 data")
parser.add_argument("patt",type=np.str_,help='file pattern look for. Ex: data.00*.hdf5')
parser.add_argument("-n",action="store",default="",help='add string to default name (default: %(default)s)')
parser.add_argument("-p",action="store",help='path to data files',default="./")
parser.add_argument("-sp",action="store",help='dir save output image to',default="/global/home/users/kaylanb/downloads/")
args = parser.parse_args()

##########
from yt.mods import *
import matplotlib.pyplot as plt
import glob

def oneFilePlot(file,args):
	print "READING from file,path: %s, %s" % (file,args.p)
	pf = load(args.p + file)
	p = ProjectionPlot(pf,"x","density", center=pf.domain_center)
	p.annotate_grids()
	spath=args.sp+ args.n + "mov%s.pdf" % file[5:9] 
	p.save(spath)

files = glob.glob1(args.p, args.patt)
for f in files:
	oneFilePlot(f, args)

	
