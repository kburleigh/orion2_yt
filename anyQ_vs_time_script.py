from yt.mods import *
import numpy as np
import numpy.random
import numpy.ma as ma
import pylab as py
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


import sys
Lib_path = "/global/home/users/kaylanb/myCode/ytScripts/Lib" 
sys.path.append(Lib_path)
import deriv_fields
import init_cond as mic

class TimeProfQs(object):
    def __init__(self):
        self.t = [] #time
        self.maxRho = []
        self.minRho = []
        self.avgRho = [] 
        self.rmsV = [] 
        self.rmsV_lrg = []  #only for cells where: Rho > Rho_ic_t0
        self.avgRho_HiPa = []
        self.frac = []   #fraction cells: Rho > Rho_ic_t0 
    def convert(self):
        self.t = np.array(self.t)
        self.maxRho = np.array(self.maxRho)
        self.minRho = np.array(self.minRho)
        self.avgRho = np.array(self.avgRho) 
        self.rmsV = np.array(self.rmsV) 
        self.rmsV_lrg = np.array(self.rmsV_lrg)
        self.avgRho_HiPa = np.array(self.avgRho_HiPa)
        self.frac = np.array(self.frac)


#funcs
def getData(lev, pf, ic, level):
    '''
    lev --  TimeProfQs() object that append data to
    level -- integer, AMR grid
    pf -- returned by load("data.hdf5")
    ic -- returned by mic.calcIC(params)
    '''
    #easy data
    lev.t.append( pf.current_time )
    #med data
    fields=["density","rms-vel"]
    dims= list(pf.domain_dimensions)
    cube= pf.h.covering_grid(level=level,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
    rho = cube["density"]
    rmsV = cube["rms-vel"]
    lev.maxRho.append( rho.max() )
    lev.minRho.append( rho.min() )
    lev.avgRho.append( rho.mean() )
    lev.rmsV.append( rmsV.mean() )
    #harder data
    m_rho = ma.masked_where(rho <= ic.rho0, rho) 
    if pf.basename == "data.0000.3d.hdf5":
        lev.avgRho_HiPa.append( rho.mean() )
    else:
        lev.avgRho_HiPa.append( m_rho.mean() )   
    
    #rmsV= cube["rms-vel"]

    # m_rho = ma.masked_where(rho <= ic.rho0, rho)
#     m_rmsV = ma.masked_where(rho <= ic.rho0, rmsV)
#     iLrg= np.where(m_rho == False) #multi array so np.where returns 3 arrays, one per dim
#     cnt_iLrg = float( len(iLrg[0]) ) #num elem meeting np.where condition 
# 
#     lev.t.append( pf.current_time )
#     lev.rmsV.append( rmsV.mean() )
#     lev.avgRho.append( rho.mean() )
#     if pf.basename == "data.0000.3d.hdf5":
#         lev.rmsV_lrg.append( rmsV.mean() )  #don't know what other val use
#         lev.avgRho_lrg.append( rho.mean() )
#         lev.frac.append( 0. ) 
#     else:
#         lev.rmsV_lrg.append( m_rmsV.mean() )
#         lev.avgRho_lrg.append( m_rho.mean() )
#         lev.frac.append( cnt_iLrg / m_rho.size ) 

def plot_max_avg_Rho_lev0(lev0, ic, spath):
    '''lev -- TimeProfQs() object, whose lev.convert() attribute has been called'''
    #avgRho
    Tratio = lev0.t / ic.tCr
    fig, ax = plt.subplots()
    plt.plot(Tratio,lev0.maxRho,ls="-",c="black",lw=2.,label="max")
    plt.plot(Tratio,lev0.minRho,ls="-",c="green",lw=2.,label="min")
    plt.plot(Tratio,lev0.avgRho,ls="-",c="blue",lw=2.,label="avg")
    plt.plot(Tratio,lev0.avgRho_HiPa,ls="-",c="red",lw=2.,label=r"avg ($\rho > \rho_0$)")
    plt.hlines(ic.rho0, Tratio.min(),Tratio.max(), colors="magenta", linestyles='dashed',label="initial")
    #finish
    plt.yscale("log")
    plt.xlabel("t / t_cross")
    plt.ylabel("Densities [g/cm^3]")
    plt.title(r"Max & Avg Densities, Lev0")
    py.legend(loc=4, fontsize="small")
    name = spath+"max_avg_Rho_Lev0.pdf"
    plt.savefig(name,format="pdf")
    plt.close()

def plot_max_avg_Rho_lev012(lev0,lev1,lev2, ic, spath):
    '''lev -- TimeProfQs() object, whose lev.convert() attribute has been called'''
    #avgRho
    Tratio = lev0.t / ic.tCr
    fig, ax = plt.subplots()
    #initial
    plt.hlines(ic.rho0, Tratio.min(),Tratio.max(), colors="magenta", linestyles='dashed',label="initial")
    #lev0
    plt.plot(Tratio,lev0.maxRho,ls="-",c="black",lw=2.,label="max Lev0")
    plt.plot(Tratio,lev0.minRho,ls="-",c="green",lw=2.,label="min Lev0")
    plt.plot(Tratio,lev0.avgRho,ls="-",c="blue",lw=2.,label="avg Lev0")
    plt.plot(Tratio,lev0.avgRho_HiPa,ls="-",c="red",lw=2.,label=r"avg ($\rho > \rho_0$)")
    #lev1
    plt.plot(Tratio,lev1.maxRho,ls="--",c="black",lw=2.,label="max Lev1")
    plt.plot(Tratio,lev1.minRho,ls="--",c="green",lw=2.,label="min Lev1")
    plt.plot(Tratio,lev1.avgRho,ls="--",c="blue",lw=2.,label="avg Lev1")
    plt.plot(Tratio,lev1.avgRho_HiPa,ls="--",c="red",lw=2.,label=r"avg Lev1 ($\rho > \rho_0$)")
    #lev2
    plt.plot(Tratio,lev2.maxRho,ls=":",c="black",lw=2.,label="max Lev2")
    plt.plot(Tratio,lev2.minRho,ls=":",c="green",lw=2.,label="min Lev2")
    plt.plot(Tratio,lev2.avgRho,ls=":",c="blue",lw=2.,label="avg Lev2")
    plt.plot(Tratio,lev0.avgRho_HiPa,ls=":",c="red",lw=2.,label=r"avg Lev1 ($\rho > \rho_0$)")
    #finish
    plt.yscale("log")
    plt.xlabel("t / t_cross")
    plt.ylabel("Densities [g/cm^3]")
    plt.title(r"Max & Avg Densities, Lev0")
    py.legend(loc=4, fontsize="small")
    name = spath+"max_avg_Rho_Lev012.pdf"
    plt.savefig(name,format="pdf")
    plt.close()
##

def ind_1st_3_maxima(nparr):
    '''nparr is numpy array of times'''
    ind = [0]*3  
    ind[0] =np.where(nparr == nparr.max())[0][0]
    nparr[ ind[0] ] = 0.
    ind[1] =np.where(nparr == nparr.max())[0][0]
    nparr[ ind[1] ] = 0.
    ind[2] =np.where(nparr == nparr.max())[0][0]
    return ind

### not working plotting scripts for adding color bar showing percent cube data in rho > rho_crit per data point# 

##
# def plot_lev0_Tprof_Qs(lev, ic, spath):
#     '''lev -- TimeProfQs() object, whose lev.convert() attribute has been called'''
#     #avgRho
#     Tratio = lev.t / ic.tCr
#     size = np.zeros( len(Tratio) ) + 100.
#     c_code = lev.frac 
#     ###EDIT TO USE MY OWN COLOR MAP: arg: CMAP=???
#     fig, ax = plt.subplots()
#     plt.scatter(Tratio,lev.avgRho,s=size, c="black",marker="o",label="avgRho")
#     #c= color means no color mapping, c = float means color map used
#     plt.scatter(Tratio,lev.avgRho_lrg,s=size,cmap=py.get_cmap("winter"), c=c_code,marker="o",label="avgRho_lrg")
#     plt.hlines(ic.rho0, Tratio.min(), Tratio.max(), colors="black", linestyles='dashed',label="rho0")
#     #colorbar
#     data = np.clip(np.random.randn(250, 250), 1, 10)
#     cax = ax.imshow(data, interpolation='nearest', cmap=plt.get_cmap("winter"), norm=LogNorm())
#     cbar = fig.colorbar(cax, orientation='vertical')
#     #finish
#     plt.xlabel("t / t_cross")
#     plt.ylabel("Density [g/cm^3]")
#     #plt.yscale("log")
#     plt.title(r"Average Density, Lev0")
#     plt.legend(loc=0)
#     name = spath+"avgRho_Lev0.pdf"
#     plt.savefig(name,format="pdf")
#     #plt.close()
#    
# def plot_lev012_Tprof_Qs(lev0,lev1,lev2, ic, spath):
#     '''lev -- TimeProfQs() object, whose lev.convert() attribute has been called'''
#     #avgRho
#     Tratio = lev0.t / ic.tCr  #same for all levs: lev0.t = lev1.t = lev2.t ...
#     size = np.zeros( len(Tratio) ) + 2. 
#     ###EDIT TO USE MY OWN COLOR MAP: arg: CMAP=???  
#     #plot each level
#     #
#     flt_col = lev0.frac
#     py.scatter(Tratio,lev0.avgRho,s=size,c="black",marker="o",label="Lev0 avgRho")
#     py.scatter(Tratio,lev0.avgRho_lrg,s=size,cmap=py.get_cmap("winter"), c=c_code,marker="o",label="Lev0 avgRho_lrg")
#     #
#     flt_col = lev1.frac
#     py.scatter(Tratio,lev1.avgRho,s=size,c="blue",marker="^",label="Lev1 avgRho")
#     py.scatter(Tratio,lev1.avgRho_lrg,s=size,cmap=py.get_cmap("winter"), c=c_code,marker="o",label="Lev1 avgRho_lrg")
#     #
#     flt_col = lev2.frac
#     py.scatter(Tratio,lev2.avgRho,s=size,c="red",marker="D",label="Lev2 avgRho")
#     py.scatter(Tratio,lev2.avgRho_lrg,s=size,cmap=py.get_cmap("winter"), c=c_code,marker="o",label="Lev2 avgRho_lrg")
#     #
#     py.hlines(ic.rho0, Tratio.min(), Tratio.max(), colors="black", linestyles='dashed',label="rho0")
#     py.xlabel("t / t_cross")
#     py.ylabel("Density [g/cm^3]")
#     py.yscale("log")
#     py.title(r"Average Density, Levs0,1,2")
#     py.legend(loc=0, fontsize="small")
#     name = spath+"avgRho_Lev012.pdf"
#     py.savefig(name,format="pdf")
#     py.close()
    


#===============

# f = "data.0000.3d.hdf5"
# path = "/Users/kburleigh/GradSchool/Research/w_McKee/orion2/test_data/Turb/driving_t0_only/MHD/"
# spath = path+"plots/"
# patt = "data.00*.3d.hdf5"
# lev0 = TimeProfQs()
# 
# p= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
# ic = mic.calcIC(p)
# ic.prout()
# 
# pf = load(path+f)
# fields=["density","rms-vel"]
# dims= list(pf.domain_dimensions)
# cube= pf.h.covering_grid(level=0,left_edge=pf.h.domain_left_edge,dims=dims,fields=fields)
# rho = cube["density"]
# rmsV= cube["rms-vel"]
# 
# m_rho = ma.masked_where(rho <= ic.rho0, rho)
# m_rmsV = ma.masked_where(rho <= ic.rho0, rmsV)
# iLrg= np.where(m_rho == False) #multi array so np.where returns 3 arrays, one per dim
# cnt_iLrg = float( len(iLrg[0]) ) #num elem meeting np.where condition 
# test = np.where(rho <= ic.rho0)
# 
# lev0.t.append( pf.current_time )
# lev0.rmsV.append( rmsV.mean() )
# lev0.avgRho.append( rho.mean() )
# if f == "data.0000.3d.hdf5":
#     lev0.rmsV_lrg.append( rmsV.mean() )  #don't know what other val use
#     lev0.avgRho_lrg.append( rho.mean() )
#     lev0.frac.append( 0. ) 
# else:
#     lev0.rmsV_lrg.append( m_rmsV.mean() )
#     lev0.avgRho_lrg.append( m_rho.mean() )
#     lev0.frac.append( cnt_iLrg / m_rho.size ) 


levels = 2 #even if AMR simulation, level 0 is identical to unigrid only simulation data...
path = "/clusterfs/henyey/kaylanb/Test_Prob/Turb/bondiTurb/quick/driving_t0_only/MHD/lev2/"
spath = path+"plots/"
patt = "data.00*.3d.hdf5"

lev0 = TimeProfQs()
lev1 = TimeProfQs()
lev2 = TimeProfQs()

p= mic.ParsNeed()  #then set pars.[pars] to new vals as needed
ic = mic.calcIC(p)
ic.prout()

files = glob.glob1(path, patt)
print "## READING from DIR: %s" % path
# files = files[0:3]
for f in files:
    print "#####\n\nloading file: %s \n\n#######\n" % f
    pf = load(path+ f)
    getData(lev0, pf, ic, level=0) #data appended onto lev0 for each f
    if levels > 0:
        getData(lev1, pf, ic, level=1) 
        getData(lev2, pf, ic, level=2) 

lev0.convert()
lev1.convert()
lev2.convert()

plot_max_avg_Rho_lev0(lev0, ic, spath) #plot lev0 every time, as less cluttered
if levels > 0:
    plot_max_avg_Rho_lev012(lev0, lev1, lev2, ic, spath)
# plot_lev0_Tprof_Qs(lev0, ic, spath)
# if levels > 0:
#     plot_lev012_Tprof_Qs(lev0,lev1,lev2, ic, spath)

#diagnostic printing
print "from path: %s" % path
tCr = lev0.t / ic.tCr
mach = lev0.rmsV / ic.cs
cp = np.copy( lev0.maxRho )
ind_maxRho = ind_1st_3_maxima( cp )
print "t/t_cross,v/cs at maxRho 1st,2nd,3rd maxima:"
for ind in ind_maxRho: print "%f, %f" % (tCr[ind], mach[ind])

cp = np.copy( lev0.avgRho_HiPa )
ind_avgRho_HiPa = ind_1st_3_maxima( cp )
print "t/t_cross,v/cs at avgRho_HiPa 1st,2nd,3rd maxima"
for ind in ind_avgRho_HiPa: print "%f, %f" % (tCr[ind], mach[ind])

