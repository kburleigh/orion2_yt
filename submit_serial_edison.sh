#PBS -q serial
#PBS -l walltime=00:05:00
#PBS -l vmem=1GB
#PBS -N python
#PBS -A m2218

cd $PBS_O_WORKDIR
#python Sim_SinkData_EveryTStep.py -pout edit1 -init_sink ../chk.0032.3d.sink -time0 0.76796 -sinklevel 1 -NSinks 27
python Sim_Mdot_HD_EveryTStep.py -mdots_pickle sinks.pickle -nsinks 27 -MachS 5. -cs 1. -rho0 1e-2 -problem krum06 -answer 1. -Geq1 1 -LinearY 1 -boxcar 0.08 
